package com.javabi.sizeof.definition.type;

import com.javabi.sizeof.IMemoryCalculator;
import com.javabi.sizeof.definition.ClassDefinition;

/**
 * A Float Array Definition.
 */
public final class FloatArrayDefinition extends ClassDefinition<float[]> {

	@Override
	public long sizeOf(float[] array, IMemoryCalculator calculator) {
		return calculator.getArchitecture().sizeOfFloatArray(array.length);
	}

}
