package com.javabi.sizeof.definition.type;

import com.javabi.sizeof.IMemoryCalculator;
import com.javabi.sizeof.definition.ClassDefinition;

/**
 * A Byte Array Definition.
 */
public final class ByteArrayDefinition extends ClassDefinition<byte[]> {

	@Override
	public long sizeOf(byte[] array, IMemoryCalculator calculator) {
		return calculator.getArchitecture().sizeOfByteArray(array.length);
	}

}
