package com.javabi.sizeof.definition.type;

import com.javabi.sizeof.IMemoryCalculator;
import com.javabi.sizeof.definition.ClassDefinition;

/**
 * A Boolean Array Definition.
 */
public final class BooleanArrayDefinition extends ClassDefinition<boolean[]> {

	@Override
	public long sizeOf(boolean[] array, IMemoryCalculator calculator) {
		return calculator.getArchitecture().sizeOfBooleanArray(array.length);
	}
}
