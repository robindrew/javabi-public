package com.javabi.sizeof.definition.type;

import com.javabi.sizeof.IMemoryCalculator;
import com.javabi.sizeof.definition.ClassDefinition;

/**
 * A Double Array Definition.
 */
public final class DoubleArrayDefinition extends ClassDefinition<double[]> {

	@Override
	public long sizeOf(double[] array, IMemoryCalculator calculator) {
		return calculator.getArchitecture().sizeOfDoubleArray(array.length);
	}

}
