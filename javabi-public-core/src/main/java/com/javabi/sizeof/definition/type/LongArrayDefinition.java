package com.javabi.sizeof.definition.type;

import com.javabi.sizeof.IMemoryCalculator;
import com.javabi.sizeof.definition.ClassDefinition;

/**
 * A Long Array Definition.
 */
public final class LongArrayDefinition extends ClassDefinition<long[]> {

	@Override
	public long sizeOf(long[] array, IMemoryCalculator calculator) {
		return calculator.getArchitecture().sizeOfLongArray(array.length);
	}

}
