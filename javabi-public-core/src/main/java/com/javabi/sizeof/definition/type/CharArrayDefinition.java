package com.javabi.sizeof.definition.type;

import com.javabi.sizeof.IMemoryCalculator;
import com.javabi.sizeof.definition.ClassDefinition;

/**
 * A Char Array Definition.
 */
public final class CharArrayDefinition extends ClassDefinition<char[]> {

    @Override
	public long sizeOf(char[] array, IMemoryCalculator calculator) {
        return calculator.getArchitecture().sizeOfCharArray(array.length);
    }

}
