package com.javabi.sizeof.definition.type;

import com.javabi.sizeof.IMemoryCalculator;
import com.javabi.sizeof.definition.ClassDefinition;

/**
 * A Short Array Definition.
 */
public final class ShortArrayDefinition extends ClassDefinition<short[]> {

	@Override
	public long sizeOf(short[] array, IMemoryCalculator calculator) {
		return calculator.getArchitecture().sizeOfShortArray(array.length);
	}

}
