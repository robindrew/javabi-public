package com.javabi.sizeof.definition.type;

import com.javabi.sizeof.IMemoryCalculator;
import com.javabi.sizeof.definition.ClassDefinition;

/**
 * A Int Array Definition.
 */
public final class IntArrayDefinition extends ClassDefinition<int[]> {

	@Override
	public long sizeOf(int[] array, IMemoryCalculator calculator) {
		return calculator.getArchitecture().sizeOfIntArray(array.length);
	}

}
