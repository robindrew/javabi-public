package com.javabi.sizeof.definition.type;

import com.javabi.sizeof.IMemoryCalculator;
import com.javabi.sizeof.definition.ClassDefinition;

/**
 * An Ignore Definition.
 */
public final class IgnoreDefinition<T> extends ClassDefinition<T> {

	@Override
	public long sizeOf(T instance, IMemoryCalculator calculator) {
		return 0;
	}

}
