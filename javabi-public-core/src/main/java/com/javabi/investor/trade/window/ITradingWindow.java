package com.javabi.investor.trade.window;

import java.time.LocalDateTime;

import com.javabi.investor.price.candle.IPriceCandle;

public interface ITradingWindow {

	boolean contains(LocalDateTime date);

	boolean contains(IPriceCandle candle);

}
