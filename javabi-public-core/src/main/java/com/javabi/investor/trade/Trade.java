package com.javabi.investor.trade;

import java.math.BigDecimal;

import com.javabi.investor.platform.ITradingInstrument;
import com.javabi.investor.price.pounds.IPounds;
import com.javabi.investor.price.pounds.Pounds;
import com.javabi.investor.trade.stop.StopLoss;
import com.javabi.investor.trade.stop.StopProfit;

@Deprecated
public class Trade implements ITrade {

	private final ITradingInstrument instrument;
	private final double openPrice;
	private final IPounds poundsPerPip;
	private final StopLoss stopLoss;
	private final StopProfit stopProfit;
	private final TradeDirection direction;

	public Trade(ITradingInstrument instrument, double openPrice, IPounds poundsPerPip, StopLoss stopLoss,
			StopProfit stopProfit, TradeDirection direction) {
		this.instrument = instrument;
		this.openPrice = openPrice;
		this.poundsPerPip = poundsPerPip;
		this.stopLoss = stopLoss;
		this.stopProfit = stopProfit;
		this.direction = direction;
	}

	@Override
	public ITradingInstrument getInstrument() {
		return instrument;
	}

	@Override
	public double getOpenPrice() {
		return openPrice;
	}

	@Override
	public StopLoss getStopLoss() {
		return stopLoss;
	}

	@Override
	public StopProfit getStopProfit() {
		return stopProfit;
	}

	@Override
	public TradeDirection getDirection() {
		return direction;
	}

	@Override
	public String toString() {
		return openPrice + " (" + direction + ")";
	}

	@Override
	public IPounds close(double closePrice) {
		double pips = getPipDistance(closePrice);
		return new Pounds(new BigDecimal(pips), false).multiply(poundsPerPip);
	}

	public double getDistance(double price) {
		if (getDirection().isBuy()) {
			return price - getOpenPrice();
		} else {
			return getOpenPrice() - price;
		}
	}

	public double getPipDistance(double closePrice) {
		return getDistance(closePrice) / instrument.getPipSize();
	}
}
