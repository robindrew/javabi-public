package com.javabi.investor.trade;

import static com.javabi.common.date.Dates.toLocalDateTime;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.pounds.IPounds;

public class Trades {

	private static final Logger log = LoggerFactory.getLogger(Trades.class);

	public static void close(ITrade trade, Funds funds, double closePrice) {
		IPounds cost = trade.close(closePrice);
		if (cost.isPositive()) {
			funds.add(cost);
			log.info("Profit: " + cost);
		} else {
			cost = cost.negate();
			funds.remove(cost);
			log.info("Loss: " + cost);
		}
	}

	protected static LocalDate getStartDate(IPriceCandle candle) {
		return toLocalDateTime(candle.getOpenTime()).toLocalDate();
	}

	public static boolean isOvernight(IPriceCandle previous, IPriceCandle current) {
		if (previous == null || current == null) {
			return false;
		}
		LocalDate previousDate = getStartDate(previous);
		LocalDate currentDate = getStartDate(current);
		return !previousDate.equals(currentDate);
	}

}
