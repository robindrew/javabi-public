package com.javabi.investor.trade;

import com.javabi.investor.price.pounds.IPounds;
import com.javabi.investor.price.pounds.Pounds;

public class Funds {

	private IPounds pounds;

	public Funds(IPounds pounds) {
		this.pounds = new Pounds(pounds, true);
	}

	public IPounds getPounds() {
		return pounds;
	}

	public void add(IPounds pounds) {
		this.pounds = getPounds().add(pounds);
	}

	public void remove(IPounds pounds) {
		this.pounds = getPounds().remove(pounds);
	}

	@Override
	public String toString() {
		return pounds.toString();
	}

}
