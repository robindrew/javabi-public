package com.javabi.investor.trade.stop;

import com.javabi.investor.trade.TradeDirection;

public class StopLoss extends Stop {

	public StopLoss(int tradePrice, int stopPrice, TradeDirection direction) {
		super(tradePrice, stopPrice, direction);
	}

	public boolean isTriggered(int latestPrice) {
		if (getDirection().isBuy()) {
			return latestPrice <= getStopPrice();
		} else {
			return latestPrice >= getStopPrice();
		}
	}

	public int getLoss() {
		int loss;
		if (getDirection().isBuy()) {
			loss = getTradePrice() - getStopPrice();
		} else {
			loss = getStopPrice() - getTradePrice();
		}
		if (loss < 0.0) {
			throw new IllegalArgumentException("tradePrice=" + getTradePrice() + ", stopPrice=" + getStopPrice() + ", direction=" + getDirection());
		}
		return loss;
	}

}
