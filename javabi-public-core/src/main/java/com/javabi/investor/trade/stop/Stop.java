package com.javabi.investor.trade.stop;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.trade.TradeDirection;

public abstract class Stop {

	private final int tradePrice;
	private final int stopPrice;
	private final TradeDirection direction;

	protected Stop(int tradePrice, int stopPrice, TradeDirection direction) {
		if (direction == null) {
			throw new NullPointerException("direction");
		}
		this.tradePrice = tradePrice;
		this.stopPrice = stopPrice;
		this.direction = direction;
	}

	public int getTradePrice() {
		return tradePrice;
	}

	public int getStopPrice() {
		return stopPrice;
	}

	public TradeDirection getDirection() {
		return direction;
	}

	public abstract boolean isTriggered(int latestPrice);

	public boolean isTriggered(IPriceCandle candle) {
		return isTriggered(candle.getHighPrice()) || isTriggered(candle.getLowPrice());
	}

}
