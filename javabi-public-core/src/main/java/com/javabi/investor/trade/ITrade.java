package com.javabi.investor.trade;

import com.javabi.investor.platform.ITradingInstrument;
import com.javabi.investor.price.pounds.IPounds;
import com.javabi.investor.trade.stop.StopLoss;
import com.javabi.investor.trade.stop.StopProfit;

public interface ITrade {

	ITradingInstrument getInstrument();

	double getOpenPrice();

	StopLoss getStopLoss();

	StopProfit getStopProfit();
	
	TradeDirection getDirection();
	
	IPounds close(double closePrice);

}
