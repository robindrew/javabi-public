package com.javabi.investor.trade.stop;

import com.javabi.investor.trade.TradeDirection;

public class StopProfit extends Stop {

	public StopProfit(int tradePrice, int stopPrice, TradeDirection direction) {
		super(tradePrice, stopPrice, direction);
	}

	public boolean isTriggered(int latestPrice) {
		if (getDirection().isBuy()) {
			return latestPrice >= getStopPrice();
		} else {
			return latestPrice <= getStopPrice();
		}
	}

	public int getProfit() {
		int profit;
		if (getDirection().isBuy()) {
			profit = getStopPrice() - getTradePrice();
		} else {
			profit = getTradePrice() - getStopPrice();
		}
		if (profit < 0.0) {
			throw new IllegalArgumentException("tradePrice=" + getTradePrice() + ", stopPrice=" + getStopPrice() + ", direction=" + getDirection());
		}
		return profit;
	}

}
