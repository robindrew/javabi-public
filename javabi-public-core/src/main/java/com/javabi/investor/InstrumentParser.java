package com.javabi.investor;

import com.javabi.investor.currency.CurrencyPair;

public class InstrumentParser implements IInstrumentParser {

	@Override
	public IInstrument parse(String text) {
		return CurrencyPair.valueOf(text);
	}

}
