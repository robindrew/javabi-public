package com.javabi.investor;

public class InstrumentNotSupportedException extends RuntimeException {

	private static final long serialVersionUID = -2170509427750254097L;

	public InstrumentNotSupportedException(IInstrument instrument) {
		super(instrument.toString());
	}

}
