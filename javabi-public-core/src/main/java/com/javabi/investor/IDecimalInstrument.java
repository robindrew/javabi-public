package com.javabi.investor;

public interface IDecimalInstrument {

	IInstrument getInstrument();

	int getDecimalPlaces();

	int getMinPrice();

	int getMaxPrice();

}
