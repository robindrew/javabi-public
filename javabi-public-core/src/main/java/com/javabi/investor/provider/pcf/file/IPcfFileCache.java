package com.javabi.investor.provider.pcf.file;

import com.javabi.investor.IInstrument;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.stream.source.IPriceCandleStreamSource;
import com.javabi.investor.provider.pcf.IPcfTimeWindow;

public interface IPcfFileCache {

	IInstrument getInstrument();

	IPcfFile getFile(IPriceCandle candle);

	IPriceCandleStreamSource getStreamSource(IPcfTimeWindow window);

}
