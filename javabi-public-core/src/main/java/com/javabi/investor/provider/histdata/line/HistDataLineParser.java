package com.javabi.investor.provider.histdata.line;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import com.javabi.common.date.Dates;
import com.javabi.common.text.tokenizer.CharDelimiters;
import com.javabi.investor.price.candle.line.parser.IPriceCandleLineParser;

public abstract class HistDataLineParser implements IPriceCandleLineParser {

	public static final CharDelimiters DELIMITERS = new CharDelimiters().whitespace().character(',');

	public static final ZoneOffset EST = ZoneOffset.of("-05:00");

	public static long toMillis(LocalDateTime date) {
		return Dates.toMillis(date, EST);
	}

}
