package com.javabi.investor.provider.histdata;

import java.io.File;
import java.io.IOException;

import com.javabi.investor.price.candle.line.filter.ILineFilter;
import com.javabi.investor.price.candle.line.parser.IPriceCandleLineParser;
import com.javabi.investor.provider.histdata.line.HistDataLineFilter;
import com.javabi.investor.provider.histdata.line.HistDataM1LineParser;
import com.javabi.investor.provider.histdata.line.HistDataTickLineParser;
import com.javabi.investor.provider.pcf.PcfFileLineConverter;

public class HistDataPcfLineConverter extends PcfFileLineConverter {

	public static void main(String[] args) throws IOException {
		File root = new File("M:/sync/Data/Market Data/HISTDATA/");

		for (HistDataInstrument instrument : HistDataInstrument.values()) {

			File from = new File(root, "M1/" + instrument + "/");
			File to = new File(root, "PCF/");

			createM1(instrument).convert(instrument.getInstrument(), from, to);
		}
	}

	public static HistDataPcfLineConverter createM1(HistDataInstrument instrument) {
		IPriceCandleLineParser parser = new HistDataM1LineParser(instrument);
		ILineFilter filter = new HistDataLineFilter();
		HistDataPcfLineConverter converter = new HistDataPcfLineConverter(parser, filter);
		converter.setMinPrice(instrument.getMinPrice());
		converter.setMaxPrice(instrument.getMaxPrice());
		return converter;
	}

	public static HistDataPcfLineConverter createTick(HistDataInstrument instrument) {
		IPriceCandleLineParser parser = new HistDataTickLineParser(instrument);
		ILineFilter filter = new HistDataLineFilter();
		return new HistDataPcfLineConverter(parser, filter);
	}

	public HistDataPcfLineConverter(IPriceCandleLineParser parser, ILineFilter filter) {
		super(parser, filter);
	}

}
