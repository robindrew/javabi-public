package com.javabi.investor.provider.pcf.file;

import java.io.File;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.javabi.common.io.file.Files;
import com.javabi.investor.IInstrument;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.stream.source.IPriceCandleStreamSource;
import com.javabi.investor.provider.pcf.IPcfTimeWindow;
import com.javabi.investor.provider.pcf.PcfDirectoryStreamSource;
import com.javabi.investor.provider.pcf.PcfFormat;

public class PcfFileCache implements IPcfFileCache {

	private final Map<LocalDate, IPcfFile> fileCache = new HashMap<>();
	private final IInstrument instrument;
	private final File directory;

	public PcfFileCache(IInstrument instrument, File directory) {
		if (instrument == null) {
			throw new NullPointerException("instrument");
		}
		this.instrument = instrument;
		this.directory = Files.existingDirectory(directory);
	}

	public IPcfFile getFile(IPriceCandle candle) {
		LocalDate month = PcfFormat.getMonth(candle);

		// For efficiency we cache mappings
		IPcfFile file = fileCache.get(month);
		if (file == null) {
			file = newFile(candle, month);
			fileCache.put(month, file);
		}
		return file;
	}

	public IPcfFile getFile(Collection<? extends IPriceCandle> candles) {
		if (candles.isEmpty()) {
			throw new IllegalArgumentException("candles is empty");
		}

		IPcfFile file = null;
		for (IPriceCandle candle : candles) {
			if (file == null) {
				file = getFile(candle);
			} else {
				if (!file.equals(getFile(candle))) {
					throw new IllegalArgumentException("candle=" + candle + ", expected file=" + file);
				}
			}
		}
		return file;
	}

	private IPcfFile newFile(IPriceCandle candle, LocalDate month) {

		File dir = new File(directory, instrument.name());
		Files.existingDirectory(dir, true);

		String filename = PcfFormat.getFilename(month);
		return new PcfFile(new File(dir, filename), month);
	}

	@Override
	public IInstrument getInstrument() {
		return instrument;
	}

	@Override
	public IPriceCandleStreamSource getStreamSource(IPcfTimeWindow window) {
		File parent = new File(directory, instrument.name());
		return new PcfDirectoryStreamSource(parent, window);
	}

}
