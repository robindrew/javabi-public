package com.javabi.investor.provider.pcf.locator;

import static com.javabi.investor.provider.pcf.PcfFormat.isPcfFile;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.javabi.common.io.file.Files;
import com.javabi.investor.IInstrument;
import com.javabi.investor.IInstrumentParser;
import com.javabi.investor.provider.DataProvider;
import com.javabi.investor.provider.pcf.file.IPcfFile;
import com.javabi.investor.provider.pcf.file.PcfFile;

public class PcfFileLocator implements IPcfFileLocator {

	private final File directory;
	private final IInstrumentParser parser;

	public PcfFileLocator(File directory, IInstrumentParser parser) {
		if (parser == null) {
			throw new NullPointerException("parser");
		}
		this.directory = Files.existingDirectory(directory);
		this.parser = parser;
	}

	@Override
	public Set<DataProvider> getDataProviders() {
		List<File> contents = Files.listContents(directory);
		Set<DataProvider> set = new TreeSet<>();
		for (File file : contents) {
			if (file.isDirectory()) {
				DataProvider provider = DataProvider.valueOf(file.getName());
				set.add(provider);
			}
		}
		return set;
	}

	@Override
	public Set<IInstrument> getInstruments(DataProvider provider) {
		File providerDir = getDirectory(provider);

		List<File> contents = Files.listContents(providerDir);
		Set<IInstrument> set = new LinkedHashSet<>();
		for (File file : contents) {
			if (file.isDirectory()) {
				IInstrument instrument = parser.parse(file.getName());
				set.add(instrument);
			}
		}
		return set;
	}

	private File getDirectory(DataProvider provider) {
		return new File(new File(directory, provider.name()), "PCF");
	}

	@Override
	public File getDirectory(DataProvider provider, IInstrument instrument) {
		return new File(getDirectory(provider), instrument.name());
	}

	@Override
	public Set<IPcfFile> getFiles(DataProvider provider, IInstrument instrument) {
		File instrumentDir = getDirectory(provider, instrument);

		List<File> contents = Files.listContents(instrumentDir, file -> isPcfFile(file));
		Set<IPcfFile> set = new TreeSet<>();
		for (File file : contents) {
			set.add(new PcfFile(file));
		}
		return set;
	}

}
