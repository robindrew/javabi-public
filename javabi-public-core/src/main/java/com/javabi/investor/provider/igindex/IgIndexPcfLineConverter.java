package com.javabi.investor.provider.igindex;

import java.io.File;
import java.io.IOException;

import com.javabi.investor.price.candle.line.filter.ILineFilter;
import com.javabi.investor.price.candle.line.filter.LineFilter;
import com.javabi.investor.price.candle.line.parser.IPriceCandleLineParser;
import com.javabi.investor.price.candle.line.parser.PriceCandleLineDecimalParser;
import com.javabi.investor.provider.pcf.PcfFileLineConverter;

public class IgIndexPcfLineConverter extends PcfFileLineConverter {

	public static void main(String[] args) throws IOException {
		File root = new File("M:/sync/Data/Market Data/IGINDEX/");

		for (IgInstrument instrument : IgInstrument.values()) {
			if (!instrument.name().startsWith("FX")) {
				continue;
			}

			File from = new File(root, "TICK/" + instrument.getInstrument() + "/");
			File to = new File(root, "PCF/");

			createTick(instrument).convert(instrument.getInstrument(), from, to);
		}
	}

	public static IgIndexPcfLineConverter createTick(IgInstrument instrument) {
		IPriceCandleLineParser parser = new PriceCandleLineDecimalParser(instrument.getDecimalPlaces(), false);
		ILineFilter filter = new LineFilter();
		return new IgIndexPcfLineConverter(parser, filter);
	}

	public IgIndexPcfLineConverter(IPriceCandleLineParser parser, ILineFilter filter) {
		super(parser, filter);
	}

}
