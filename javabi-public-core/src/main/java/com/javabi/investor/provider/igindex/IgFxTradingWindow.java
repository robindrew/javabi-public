package com.javabi.investor.provider.igindex;

import static com.javabi.investor.trade.window.TradingHours.openUntil;
import static java.time.DayOfWeek.FRIDAY;

import java.time.LocalTime;

import com.javabi.investor.trade.window.TradingWindow;

public class IgFxTradingWindow extends TradingWindow {

	public IgFxTradingWindow() {
		closedAtWeekends();
		setOpeningHours(FRIDAY, openUntil(LocalTime.of(22, 0)));
	}
}
