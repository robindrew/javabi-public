package com.javabi.investor.provider.openexchangerates.currencies;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.javabi.common.io.RuntimeIOException;

public class CurrenciesJson {

	public static CurrenciesJson readFrom(File file) {
		try (FileReader reader = new FileReader(file)) {
			return readFrom(reader);
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}

	public static CurrenciesJson readFrom(Reader reader) {
		JsonParser parser = new JsonParser();
		JsonElement json = parser.parse(reader);
		JsonObject object = json.getAsJsonObject();
		return new CurrenciesJson(object);
	}

	private final Map<String, String> codeToDescriptionMap = new LinkedHashMap<>();

	public CurrenciesJson(JsonObject object) {
		for (Entry<String, JsonElement> entry : object.entrySet()) {
			String code = entry.getKey();
			String description = entry.getValue().getAsString();
			codeToDescriptionMap.put(code, description);
		}
	}

	public Map<String, String> getCodeToDescriptionMap() {
		return ImmutableMap.copyOf(codeToDescriptionMap);
	}

}
