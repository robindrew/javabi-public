package com.javabi.investor.provider.openexchangerates;

public abstract class UrlBuilder {

	public static final String API_URL = "https://openexchangerates.org/api/";
	public static final String PARAM_APP_ID = "?app_id=";

	private final String appId;

	protected UrlBuilder(String appId) {
		this.appId = appId;
	}

	public String getAppId() {
		return appId;
	}

	public abstract String build();

}
