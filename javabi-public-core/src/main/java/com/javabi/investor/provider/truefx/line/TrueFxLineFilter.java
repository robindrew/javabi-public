package com.javabi.investor.provider.truefx.line;

import com.javabi.investor.price.candle.line.filter.DeclineSetFilter;

public class TrueFxLineFilter extends DeclineSetFilter {

	public TrueFxLineFilter() {
		add("USD/CAD,20140930 03:41:08.795");
	}
}
