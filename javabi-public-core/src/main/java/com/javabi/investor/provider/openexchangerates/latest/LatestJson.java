package com.javabi.investor.provider.openexchangerates.latest;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.javabi.common.io.RuntimeIOException;
import com.javabi.common.locale.CurrencyCode;

public class LatestJson {

	public static LatestJson readFromText(String text) {
		return readFromReader(new StringReader(text));
	}

	public static LatestJson readFromFile(File file) {
		try (FileReader reader = new FileReader(file)) {
			return readFromReader(reader);
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}

	public static LatestJson readFromReader(Reader reader) {
		JsonParser parser = new JsonParser();
		JsonElement json = parser.parse(reader);
		JsonObject object = json.getAsJsonObject();
		return new LatestJson(object);
	}

	private final long timestamp;
	private final CurrencyCode base;
	private final Map<CurrencyCode, Float> rateMap = new LinkedHashMap<>();

	public LatestJson(JsonObject json) {

		// Timestamp
		this.timestamp = json.get("timestamp").getAsLong();

		// Base currency
		this.base = CurrencyCode.valueOf(json.get("base").getAsString());

		// Exchange rates
		JsonObject jsonRates = json.get("rates").getAsJsonObject();
		for (Entry<String, JsonElement> entry : jsonRates.entrySet()) {
			CurrencyCode code = CurrencyCode.valueOf(entry.getKey());
			float rate = entry.getValue().getAsFloat();
			rateMap.put(code, rate);
		}
	}

	public long getTimestamp() {
		return timestamp;
	}

	public LocalDateTime getDate(boolean normalise) {
		LocalDateTime dateTime = getDate();
		if (normalise) {
			LocalDate date = dateTime.toLocalDate();
			LocalTime time = dateTime.toLocalTime();

			// Remove the nanos and seconds
			time = time.withNano(0);
			time = time.withSecond(0);

			dateTime = LocalDateTime.of(date, time);
		}
		return dateTime;
	}

	public LocalDateTime getDate() {
		Instant instant = Instant.ofEpochSecond(getTimestamp());
		LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
		return dateTime;
	}

	public CurrencyCode getBase() {
		return base;
	}

	public Map<CurrencyCode, Float> getRateMap() {
		return ImmutableMap.copyOf(rateMap);
	}

}
