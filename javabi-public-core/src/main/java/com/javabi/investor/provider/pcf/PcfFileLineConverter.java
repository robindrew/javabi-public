package com.javabi.investor.provider.pcf;

import static java.util.concurrent.TimeUnit.MINUTES;

import java.io.File;
import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.io.file.Files;
import com.javabi.investor.IInstrument;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.PriceCandleDateComparator;
import com.javabi.investor.price.candle.checker.PriceCandleRangeChecker;
import com.javabi.investor.price.candle.filter.PriceCandleConsecutiveFilter;
import com.javabi.investor.price.candle.interval.ChronoUnitInterval;
import com.javabi.investor.price.candle.interval.IPriceCandleInterval;
import com.javabi.investor.price.candle.interval.TimeUnitInterval;
import com.javabi.investor.price.candle.io.list.filter.PriceCandleListDuplicateFilter;
import com.javabi.investor.price.candle.io.list.source.IPriceCandleListSource;
import com.javabi.investor.price.candle.io.stream.source.IPriceCandleStreamSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleCheckerStreamSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleDirectoryStreamSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleFilteredStreamSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleIntervalStreamSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleIntervalStreamToListSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleLoggedStreamSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleModifierStreamSource;
import com.javabi.investor.price.candle.line.filter.ILineFilter;
import com.javabi.investor.price.candle.line.parser.IPriceCandleLineParser;
import com.javabi.investor.price.candle.modifier.PriceCandleMultiplyModifier;
import com.javabi.investor.provider.pcf.file.IPcfFile;
import com.javabi.investor.provider.pcf.file.PcfFileCache;

public class PcfFileLineConverter {

	private static final Logger log = LoggerFactory.getLogger(PcfFileLineConverter.class);

	private final IPriceCandleLineParser parser;
	private final ILineFilter filter;
	private boolean verify = true;
	private int loggingFrequency = 1000;
	private int multiplier = 0;
	private int minPrice = 1;
	private int maxPrice = Integer.MAX_VALUE;

	public PcfFileLineConverter(IPriceCandleLineParser parser, ILineFilter filter) {
		if (parser == null) {
			throw new NullPointerException("parser");
		}
		if (filter == null) {
			throw new NullPointerException("filter");
		}
		this.parser = parser;
		this.filter = filter;
	}

	public int getLoggingFrequency() {
		return loggingFrequency;
	}

	public void setVerify(boolean verify) {
		this.verify = verify;
	}

	public void setLoggingFrequency(int loggingFrequency) {
		this.loggingFrequency = loggingFrequency;
	}

	public void setMinPrice(int minPrice) {
		this.minPrice = minPrice;
	}

	public void setMaxPrice(int maxPrice) {
		this.maxPrice = maxPrice;
	}

	public void setMultiplier(int multiplier) {
		this.multiplier = multiplier;
	}

	public void convert(IInstrument instrument, File fromDirectory, File toDirectory) throws IOException {
		Files.existingDirectory(fromDirectory);
		Files.existingDirectory(toDirectory);

		PcfFileCache fileCache = new PcfFileCache(instrument, toDirectory);

		IPriceCandleInterval interval = new ChronoUnitInterval(ChronoUnit.MONTHS);
		try (IPriceCandleListSource source = createSource(fromDirectory, interval)) {
			while (true) {

				// 1. Read all the candles in to one big list!
				List<IPriceCandle> candles = source.getNextCandles();
				if (candles.isEmpty()) {
					break;
				}

				// 2. Sort the candles
				candles = new PriceCandleListDuplicateFilter().filter(candles);
				Collections.sort(candles, new PriceCandleDateComparator());

				// 3. Write the PCF candle file
				IPcfFile file = fileCache.getFile(candles);
				if (file.getFile().exists()) {
					log.warn("File already exists: " + file.getFile());
					continue;
				}
				file.write(candles);

				// 4. Verify the PCF candle file
				if (verify) {
					List<IPriceCandle> written = file.read();
					if (!candles.equals(written)) {
						throw new IllegalStateException("Candles do not match!!");
					}
				}
			}
		}
	}

	private PriceCandleIntervalStreamToListSource createSource(File fromDirectory, IPriceCandleInterval interval) {
		IPriceCandleStreamSource source = new PriceCandleDirectoryStreamSource(fromDirectory, parser, filter);
		source = new PriceCandleFilteredStreamSource(source, new PriceCandleConsecutiveFilter(1000));
		source = new PriceCandleIntervalStreamSource(source, new TimeUnitInterval(1, MINUTES));
		source = new PriceCandleModifierStreamSource(source, new PriceCandleMultiplyModifier(multiplier));
		source = new PriceCandleLoggedStreamSource(source, loggingFrequency);
		source = new PriceCandleCheckerStreamSource(source, new PriceCandleRangeChecker(minPrice, maxPrice));
		return new PriceCandleIntervalStreamToListSource(source, interval);
	}

}
