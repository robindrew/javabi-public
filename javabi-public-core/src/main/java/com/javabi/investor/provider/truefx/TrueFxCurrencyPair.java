package com.javabi.investor.provider.truefx;

import com.javabi.investor.IDecimalInstrument;
import com.javabi.investor.IInstrument;
import com.javabi.investor.currency.CurrencyPair;

public enum TrueFxCurrencyPair implements IDecimalInstrument {

	AUDUSD(5), EURUSD(5), USDCAD(5), USDCHF(5), USDJPY(5);

	private final int decimalPlaces;
	private final CurrencyPair pair;

	private TrueFxCurrencyPair(int decimalPlaces) {
		if (decimalPlaces < 0) {
			throw new IllegalArgumentException("decimalPlaces=" + decimalPlaces);
		}
		this.decimalPlaces = decimalPlaces;
		this.pair = CurrencyPair.valueOf(name());
	}

	@Override
	public int getDecimalPlaces() {
		return decimalPlaces;
	}

	@Override
	public CurrencyPair getInstrument() {
		return pair;
	}

	public static TrueFxCurrencyPair valueOf(IInstrument instrument) {
		for (TrueFxCurrencyPair pair : values()) {
			if (pair.getInstrument().equals(instrument)) {
				return pair;
			}
		}
		throw new IllegalArgumentException("Instrument not supported: " + instrument);
	}

	@Override
	public int getMinPrice() {
		throw new IllegalStateException("Not implemented");
	}

	@Override
	public int getMaxPrice() {
		throw new IllegalStateException("Not implemented");
	}

}
