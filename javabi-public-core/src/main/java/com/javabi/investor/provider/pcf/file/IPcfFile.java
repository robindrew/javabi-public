package com.javabi.investor.provider.pcf.file;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import com.javabi.investor.price.candle.IPriceCandle;

public interface IPcfFile extends Comparable<IPcfFile> {

	LocalDate getMonth();

	void write(List<IPriceCandle> candles) throws IOException;

	List<IPriceCandle> read() throws IOException;

	File getFile();

}
