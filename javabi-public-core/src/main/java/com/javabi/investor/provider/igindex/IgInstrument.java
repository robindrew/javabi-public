package com.javabi.investor.provider.igindex;

import com.javabi.investor.IDecimalInstrument;
import com.javabi.investor.IInstrument;
import com.javabi.investor.currency.CurrencyPair;
import com.javabi.investor.platform.ITradingInstrument;
import com.javabi.investor.share.index.ShareIndex;
import com.javabi.investor.trade.window.ITradingWindow;

public enum IgInstrument implements IDecimalInstrument, ITradingInstrument {

	/** AUD/USD. */
	FX_AUD_USD(CurrencyPair.AUDUSD, "CS.D.AUDUSD.TODAY.IP", 2, true),
	/** EUR/JPY. */
	FX_EUR_JPY(CurrencyPair.EURJPY, "CS.D.EURJPY.TODAY.IP", 2, true),
	/** EUR/USD. */
	FX_EUR_USD(CurrencyPair.EURUSD, "CS.D.EURUSD.TODAY.IP", 2, true),
	/** GBP/USD. */
	FX_GBP_USD(CurrencyPair.GBPUSD, "CS.D.GBPUSD.TODAY.IP", 2, true),
	/** USD/CHF. */
	FX_USD_CHF(CurrencyPair.USDCHF, "CS.D.USDCHF.TODAY.IP", 2, false),
	/** USD/JPY. */
	FX_USD_JPY(CurrencyPair.USDJPY, "CS.D.USDJPY.TODAY.IP", 2, true),

	/** FTSE 100. */
	INDEX_FTSE_100(ShareIndex.FTSE_100, "IX.D.FTSE.DAILY.IP", 1, true),
	/** FTSE 100. */
	INDEX_DAX(ShareIndex.DAX, "IX.D.DAX.DAILY.IP", 1, true),
	/** S&P 500. */
	INDEX_SP_500(ShareIndex.SP_500, "IX.D.SPTRD.DAILY.IP", 2, true),
	/** DOW JONES. */
	INDEX_DOW_JONES(ShareIndex.DJIA, "IX.D.DOW.DAILY.IP", 1, true);

	private final IInstrument instrument;
	private final String epic;
	private final boolean guaranteedStop;
	private final int decimalPlaces;

	private IgInstrument(IInstrument instrument, String epic, int decimalPlaces, boolean guaranteedStop) {
		this.instrument = instrument;
		this.epic = epic;
		this.decimalPlaces = decimalPlaces;
		this.guaranteedStop = guaranteedStop;
	}

	public String getEpic() {
		return epic;
	}

	public IInstrument getInstrument() {
		return instrument;
	}

	public boolean hasGuaranteedStop() {
		return guaranteedStop;
	}

	public static IgInstrument forEpic(String epic) {
		for (IgInstrument instrument : values()) {
			if (instrument.getEpic().equals(epic)) {
				return instrument;
			}
		}
		throw new IllegalArgumentException("Unknown epic: '" + epic + "'");
	}

	@Override
	public int getDecimalPlaces() {
		return decimalPlaces;
	}

	@Override
	public int getMinPrice() {
		throw new IllegalStateException("Not implemented");
	}

	@Override
	public int getMaxPrice() {
		throw new IllegalStateException("Not implemented");
	}

	@Override
	public double getPipSize() {
		throw new IllegalStateException("Not implemented");
	}

	@Override
	public double getSpreadSize() {
		throw new IllegalStateException("Not implemented");
	}

	@Override
	public ITradingWindow getWindow() {
		throw new IllegalStateException("Not implemented");
	}

}
