package com.javabi.investor.provider.histdata.line;

import static java.time.format.DateTimeFormatter.ofPattern;
import static java.time.temporal.ChronoUnit.MINUTES;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import com.javabi.common.text.tokenizer.CharTokenizer;
import com.javabi.investor.IInstrument;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.PriceCandle;
import com.javabi.investor.provider.histdata.HistDataInstrument;
import com.javabi.investor.provider.pcf.FloatingPoint;

public class HistDataM1LineParser extends HistDataLineParser {

	private static DateTimeFormatter DATE_FORMAT = ofPattern("yyyy.MM.dd");

	private final HistDataInstrument instrument;

	public HistDataM1LineParser(HistDataInstrument instrument) {
		if (instrument == null) {
			throw new NullPointerException("instrument");
		}
		this.instrument = instrument;
	}

	public HistDataM1LineParser(IInstrument instrument) {
		this(HistDataInstrument.valueOf(instrument));
	}

	@Override
	public IPriceCandle parseCandle(String line) {
		CharTokenizer tokenizer = new CharTokenizer(line, DELIMITERS);
		int decimalPlaces = instrument.getDecimalPlaces();

		// Dates
		LocalDate date = LocalDate.parse(tokenizer.next(false), DATE_FORMAT);
		LocalTime openTime = LocalTime.parse(tokenizer.next(false));
		LocalDateTime openDate = LocalDateTime.of(date, openTime);
		LocalDateTime closeDate = openDate.plus(1, MINUTES);

		// Prices
		int open = FloatingPoint.toBigInt(tokenizer.next(false), decimalPlaces);
		int high = FloatingPoint.toBigInt(tokenizer.next(false), decimalPlaces);
		int low = FloatingPoint.toBigInt(tokenizer.next(false), decimalPlaces);
		int close = FloatingPoint.toBigInt(tokenizer.next(false), decimalPlaces);

		return new PriceCandle(open, high, low, close, toMillis(openDate), toMillis(closeDate), decimalPlaces);
	}
}
