package com.javabi.investor.provider.openexchangerates.latest;

import com.javabi.investor.provider.openexchangerates.UrlBuilder;

public class LatestUrlBuilder extends UrlBuilder {

	public static final String FILENAME = "latest.json";

	public LatestUrlBuilder(String appId) {
		super(appId);
	}

	@Override
	public String build() {
		StringBuilder url = new StringBuilder();
		url.append(API_URL);
		url.append(FILENAME);
		url.append(PARAM_APP_ID);
		url.append(getAppId());
		return url.toString();
	}
}
