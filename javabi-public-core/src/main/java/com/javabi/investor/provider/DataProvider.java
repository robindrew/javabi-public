package com.javabi.investor.provider;

public enum DataProvider {

	HISTDATA, TRUEFX, OPENEXCHANGERATES, IGINDEX;

}
