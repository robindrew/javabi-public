package com.javabi.investor.provider.pcf;

import static com.javabi.common.text.StringFormats.bytes;
import static com.javabi.investor.provider.pcf.PcfFormat.getMonth;
import static com.javabi.investor.provider.pcf.PcfFormat.isPcfFile;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.io.file.Files;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.line.source.FileLineSource;
import com.javabi.investor.price.candle.io.line.source.FilteredLineSource;
import com.javabi.investor.price.candle.io.line.source.ILineSource;
import com.javabi.investor.price.candle.io.stream.source.IPriceCandleStreamSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleListBackedStreamSource;
import com.javabi.investor.price.candle.line.filter.ILineFilter;
import com.javabi.investor.provider.pcf.file.IPcfFile;
import com.javabi.investor.provider.pcf.file.PcfFile;

public class PcfDirectoryStreamSource implements IPriceCandleStreamSource {

	private static final Logger log = LoggerFactory.getLogger(PcfDirectoryStreamSource.class);

	private final File directory;
	private final IPcfTimeWindow window;
	private final LinkedList<IPcfFile> files = new LinkedList<>();

	private IPriceCandleStreamSource currentSource = null;

	public PcfDirectoryStreamSource(File directory, IPcfTimeWindow window) {
		if (directory == null) {
			throw new NullPointerException("directory");
		}
		if (window == null) {
			throw new NullPointerException("window");
		}

		this.directory = directory;
		this.window = window;

		files.addAll(listContents(directory));
	}

	private List<IPcfFile> listContents(File directory) {
		List<File> contents = Files.listContents(directory);
		if (contents.isEmpty()) {
			throw new IllegalArgumentException("Directory is empty: " + directory);
		}

		List<IPcfFile> files = new ArrayList<>();
		for (File file : contents) {
			if (isPcfFile(file)) {
				files.add(new PcfFile(file, getMonth(file)));
			}
		}

		if (files.isEmpty()) {
			throw new IllegalArgumentException("Directory contains no PCF files: " + directory);
		}

		// Sort the files (by month)
		Collections.sort(files);
		return files;
	}

	@Override
	public String getName() {
		return directory.getAbsolutePath();
	}

	@Override
	public void close() throws IOException {
		files.clear();
		if (currentSource != null) {
			currentSource.close();
			currentSource = null;
		}
	}

	@Override
	public IPriceCandle getNextCandle() throws IOException {

		// Is there a source?
		if (currentSource != null) {
			IPriceCandle candle = currentSource.getNextCandle();
			if (candle != null) {
				return candle;
			}
			currentSource = null;
		}

		// No more files?
		while (true) {
			if (files.isEmpty()) {
				return null;
			}

			// Next file
			IPcfFile file = files.removeFirst();
			if (!window.contains(file.getMonth())) {
				continue;
			}
			log.info("Source File: " + file + " (" + bytes(file.getFile().length()) + ")");

			List<IPriceCandle> candles = file.read();
			currentSource = new PriceCandleListBackedStreamSource(candles);

			// Each file should contain at least one candle!
			IPriceCandle candle = currentSource.getNextCandle();
			if (candle == null) {
				throw new IllegalStateException("File does not contain any candles: " + file);
			}
			return candle;
		}
	}

	protected ILineSource createLineSource(File file, ILineFilter filter, Charset charset) throws IOException {
		return new FilteredLineSource(new FileLineSource(file, charset), filter);
	}

}
