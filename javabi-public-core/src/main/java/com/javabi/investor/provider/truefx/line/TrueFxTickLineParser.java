package com.javabi.investor.provider.truefx.line;

import static com.javabi.common.date.Dates.toMillis;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import com.javabi.common.text.tokenizer.CharDelimiters;
import com.javabi.common.text.tokenizer.CharTokenizer;
import com.javabi.investor.currency.CurrencyPair;
import com.javabi.investor.price.Mid;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.PriceCandleInstant;
import com.javabi.investor.price.candle.line.parser.IPriceCandleLineParser;
import com.javabi.investor.provider.pcf.FloatingPoint;
import com.javabi.investor.provider.truefx.TrueFxCurrencyPair;

public class TrueFxTickLineParser implements IPriceCandleLineParser {

	private static final CharDelimiters DELIMITERS = new CharDelimiters().whitespace().character(',');

	private static DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd");
	private static DateTimeFormatter TIME_FORMAT = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");

	private final String pairToken;
	private final TrueFxCurrencyPair pair;

	public TrueFxTickLineParser(CurrencyPair pair) {
		this(TrueFxCurrencyPair.valueOf(pair));
	}

	public TrueFxTickLineParser(TrueFxCurrencyPair pair) {
		if (pair == null) {
			throw new NullPointerException("pair");
		}
		this.pair = pair;
		this.pairToken = pair.getInstrument().toString("/");
	}

	@Override
	public IPriceCandle parseCandle(String line) {
		CharTokenizer tokenizer = new CharTokenizer(line, DELIMITERS);
		int decimalPlaces = pair.getDecimalPlaces();

		// Currency Pair
		if (!pairToken.equals(tokenizer.next(false))) {
			throw new IllegalArgumentException("line missing currency pair: " + pair + ", line: '" + line + "'");
		}

		// Dates
		LocalDate date = LocalDate.parse(tokenizer.next(false), DATE_FORMAT);
		LocalTime time = LocalTime.parse(tokenizer.next(false), TIME_FORMAT);

		// Prices
		BigDecimal bid = new BigDecimal(tokenizer.next(false));
		BigDecimal ask = new BigDecimal(tokenizer.next(false));
		// long volume = Long.parseLong(tokenizer.next(false));
		BigDecimal mid = Mid.getMid(bid, ask);

		return new PriceCandleInstant(FloatingPoint.toBigInt(mid, decimalPlaces), toMillis(LocalDateTime.of(date, time)), decimalPlaces);
	}

}
