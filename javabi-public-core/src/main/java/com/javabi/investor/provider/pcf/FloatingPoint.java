package com.javabi.investor.provider.pcf;

import static java.lang.Math.max;

import java.math.BigDecimal;
import java.util.Collection;

import com.javabi.investor.price.candle.IPriceCandle;

public class FloatingPoint {

	private static final BigDecimal TEN = new BigDecimal(10);

	public static int getDecimalPlaces(Collection<? extends IPriceCandle> candles) {
		int places = 0;
		for (IPriceCandle candle : candles) {
			places = max(places, getDecimalPlaces(candle));
		}
		return places;
	}

	public static int getDecimalPlaces(IPriceCandle candle) {
		int open = getDecimalPlaces(candle.getOpenPrice());
		int high = getDecimalPlaces(candle.getHighPrice());
		int low = getDecimalPlaces(candle.getLowPrice());
		int close = getDecimalPlaces(candle.getClosePrice());
		return max(max(max(open, close), high), low);
	}

	public static int getDecimalPlaces(double value) {
		return getDecimalPlaces(Double.toString(value));
	}

	public static int getDecimalPlaces(String text) {
		boolean decimal = false;
		int places = 0;
		for (int i = text.length() - 1; i >= 0; i--) {
			char c = text.charAt(i);
			if (c == '.') {
				decimal = true;
				break;
			}
			if (c == '0' && places == 0) {
				continue;
			}
			places++;
		}
		if (!decimal) {
			return 0;
		}
		return places;
	}

	public static int toInt(String text, int decimalPlaces) {
		return toInt(text, decimalPlaces, true);
	}

	public static int toInt(String text, int decimalPlaces, boolean checkPlaces) {
		double value = Double.parseDouble(text);
		return toInt(value, decimalPlaces, checkPlaces);
	}

	public static int toInt(double value, int decimalPlaces) {
		return toInt(value, decimalPlaces, true);
	}

	public static int toInt(double value, int decimalPlaces, boolean checkPlaces) {
		double multiplyDouble = value;
		for (int i = 0; i < decimalPlaces; i++) {
			multiplyDouble *= 10;
		}
		long roundedLong = Math.round(multiplyDouble);

		// Validate all decimal places have been removed
		if (checkPlaces) {
			double checkDouble = multiplyDouble;
			long checkLong = roundedLong;
			for (int i = 0; i < 2; i++) {
				checkDouble *= 10;
				checkLong *= 10;
				if (checkLong != Math.round(checkDouble)) {
					throw new IllegalArgumentException("value=" + value + ", decimalPlaces=" + decimalPlaces);
				}
			}
		}

		// Truncate to integer
		int roundedInt = (int) roundedLong;
		if (roundedInt != roundedLong) {
			throw new IllegalArgumentException("value=" + value + ", decimalPlaces=" + decimalPlaces);
		}
		return roundedInt;
	}

	public static int toBigInt(String text, int decimalPlaces) {
		return toBigInt(text, decimalPlaces, true);
	}

	public static int toBigInt(String text, int decimalPlaces, boolean checkPlaces) {
		BigDecimal value = new BigDecimal(text);
		return toBigInt(value, decimalPlaces, checkPlaces);
	}

	public static int toBigInt(BigDecimal value, int decimalPlaces) {
		return toBigInt(value, decimalPlaces, true);
	}

	public static int toBigInt(BigDecimal value, int decimalPlaces, boolean checkPlaces) {
		BigDecimal multiplyDouble = value;
		for (int i = 0; i < decimalPlaces; i++) {
			multiplyDouble = multiplyDouble.multiply(TEN);
		}

		long roundedLong = multiplyDouble.toBigInteger().longValue();

		// Truncate to integer
		int roundedInt = (int) roundedLong;
		if (roundedInt != roundedLong) {
			throw new IllegalArgumentException("value=" + value + ", decimalPlaces=" + decimalPlaces);
		}
		return roundedInt;
	}

}
