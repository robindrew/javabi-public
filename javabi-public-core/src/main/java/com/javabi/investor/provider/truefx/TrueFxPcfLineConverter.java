package com.javabi.investor.provider.truefx;

import java.io.File;
import java.io.IOException;

import com.javabi.investor.currency.CurrencyPair;
import com.javabi.investor.price.candle.line.filter.ILineFilter;
import com.javabi.investor.price.candle.line.parser.IPriceCandleLineParser;
import com.javabi.investor.provider.pcf.PcfFileLineConverter;
import com.javabi.investor.provider.truefx.line.TrueFxLineFilter;
import com.javabi.investor.provider.truefx.line.TrueFxTickLineParser;

public class TrueFxPcfLineConverter extends PcfFileLineConverter {

	public static void main(String[] args) throws IOException {
		File root = new File("M:/sync/Data/Market Data/TRUEFX/");

		CurrencyPair pair = CurrencyPair.AUDUSD;
		File from = new File(root, "Tick/" + pair.toString("") + "/");
		File to = new File(root, "PCF/");

		createTick(pair).convert(pair, from, to);
	}

	public static TrueFxPcfLineConverter createTick(CurrencyPair pair) {
		IPriceCandleLineParser parser = new TrueFxTickLineParser(pair);
		ILineFilter filter = new TrueFxLineFilter();
		return new TrueFxPcfLineConverter(parser, filter);
	}

	public TrueFxPcfLineConverter(IPriceCandleLineParser parser, ILineFilter filter) {
		super(parser, filter);
	}

}
