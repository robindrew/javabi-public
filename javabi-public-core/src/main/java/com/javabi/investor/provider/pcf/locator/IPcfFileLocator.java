package com.javabi.investor.provider.pcf.locator;

import java.io.File;
import java.util.Set;

import com.javabi.investor.IInstrument;
import com.javabi.investor.provider.DataProvider;
import com.javabi.investor.provider.pcf.file.IPcfFile;

public interface IPcfFileLocator {

	Set<DataProvider> getDataProviders();

	Set<IInstrument> getInstruments(DataProvider provider);

	Set<IPcfFile> getFiles(DataProvider provider, IInstrument instrument);

	File getDirectory(DataProvider provider, IInstrument instrumentValue);

}
