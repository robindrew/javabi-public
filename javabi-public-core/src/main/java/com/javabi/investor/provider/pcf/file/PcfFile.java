package com.javabi.investor.provider.pcf.file;

import static com.javabi.common.text.StringFormats.number;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.date.timer.ITimer;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.common.io.data.DataReader;
import com.javabi.common.io.data.DataWriter;
import com.javabi.common.io.data.IDataReader;
import com.javabi.common.io.data.IDataSerializer;
import com.javabi.common.io.data.IDataWriter;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.filter.IPriceCandleFilter;
import com.javabi.investor.price.candle.filter.PriceCandleDateFilter;
import com.javabi.investor.price.candle.io.line.source.FileLineSource;
import com.javabi.investor.price.candle.io.line.source.ILineSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleFilteredStreamSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleLineStreamSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleStreamToListSource;
import com.javabi.investor.provider.histdata.HistDataInstrument;
import com.javabi.investor.provider.histdata.line.HistDataM1LineParser;
import com.javabi.investor.provider.pcf.PcfDataSerializer;
import com.javabi.investor.provider.pcf.PcfFormat;

public class PcfFile implements IPcfFile {

	private static final Logger log = LoggerFactory.getLogger(PcfFile.class);

	public static void main(String[] args) throws IOException {

		HistDataInstrument instrument = HistDataInstrument.AUDUSD;
		for (int i = 1; i < 10; i++) {
			File zip = new File("M:/sync/Data/Market Data/HistData/M1/AUDUSD/HISTDATA_COM_MT_AUDUSD_M120160" + i + ".zip");

			int year = 2016;
			int month = i;

			ILineSource lines = new FileLineSource(zip);
			IPriceCandleFilter filter = new PriceCandleDateFilter(LocalDate.of(year, month, 01), LocalDate.of(year, month, 28));
			PriceCandleLineStreamSource lineSource = new PriceCandleLineStreamSource(lines, new HistDataM1LineParser(instrument));
			PriceCandleFilteredStreamSource filterSource = new PriceCandleFilteredStreamSource(lineSource, filter);
			try (PriceCandleStreamToListSource source = new PriceCandleStreamToListSource(filterSource)) {
				List<IPriceCandle> candles1 = source.getNextCandles();

				File file = new File("c:/temp/AUDUSD-20160" + i + ".pcf");

				LocalDate date = LocalDate.of(year, month, 01);
				PcfFile pcf = new PcfFile(file, date);

				// pcf.write(candles1);
				ITimer timer = NanoTimer.startTimer();
				List<IPriceCandle> candles2 = pcf.read();
				timer.stop();
				System.out.println("Loaded in " + timer);

				System.out.println(candles1.equals(candles2) + " in " + timer);
			}
		}
	}

	private final File file;
	private final LocalDate month;
	private final IDataSerializer<List<IPriceCandle>> serializer = new PcfDataSerializer();

	public PcfFile(File file, LocalDate month) {
		if (file == null) {
			throw new NullPointerException("file");
		}
		if (month == null) {
			throw new NullPointerException("month");
		}
		if (month.getDayOfMonth() != 1) {
			throw new IllegalArgumentException("month=" + month);
		}

		this.file = file;
		this.month = month;
	}

	public PcfFile(File file) {
		this(file, PcfFormat.getMonth(file));
	}

	@Override
	public LocalDate getMonth() {
		return month;
	}

	@Override
	public File getFile() {
		return file;
	}

	@Override
	public int compareTo(IPcfFile file) {
		return getMonth().compareTo(file.getMonth());
	}

	@Override
	public void write(List<IPriceCandle> candles) throws IOException {
		if (candles.isEmpty()) {
			throw new IllegalArgumentException("candles is empty");
		}
		checkMonth(candles);

		log.info("Writing PCF file: " + file);
		ITimer timer = NanoTimer.startTimer();
		try (IDataWriter writer = new DataWriter(new BufferedOutputStream(new FileOutputStream(file)))) {
			serializer.writeObject(writer, candles);
		}
		timer.stop();
		log.info("Written PCF file: " + file + ", " + number(candles) + " candles in " + timer);
	}

	private void checkMonth(List<IPriceCandle> candles) {
		for (IPriceCandle candle : candles) {
			checkMonth(candle);
		}
	}

	private void checkMonth(IPriceCandle candle) {
		if (!month.equals(PcfFormat.getMonth(candle))) {
			throw new IllegalArgumentException("Incorrect month for candle: " + candle + ", month=" + month);
		}
	}

	@Override
	public String toString() {
		return file.toString();
	}

	@Override
	public List<IPriceCandle> read() throws IOException {
		final List<IPriceCandle> candles;

		log.info("Reading PCF file: " + file);
		ITimer timer = NanoTimer.startTimer();
		try (IDataReader reader = new DataReader(new BufferedInputStream(new FileInputStream(file)))) {
			candles = serializer.readObject(reader);
		}
		timer.stop();
		log.info("Read from PCF file: " + file + ", " + number(candles) + " candles in " + timer);

		return candles;
	}

}
