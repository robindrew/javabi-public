package com.javabi.investor.provider.histdata;

import com.javabi.investor.IDecimalInstrument;
import com.javabi.investor.IInstrument;
import com.javabi.investor.currency.CurrencyPair;

public enum HistDataInstrument implements IDecimalInstrument {

	/** AUD/USD */
	AUDUSD(CurrencyPair.AUDUSD, 5, 40000, 120000),
	/** EUR/USD */
	EURUSD(CurrencyPair.EURUSD, 5, 80000, 170000),
	/** USD/CAD */
	USDCAD(CurrencyPair.USDCAD, 5, 90000, 170000),
	/** USD/CHF */
	USDCHF(CurrencyPair.USDCHF, 5, 70000, 190000),
	/** USD/JPY */
	USDJPY(CurrencyPair.USDJPY, 3, 70000, 140000);

	private final IInstrument instrument;
	private final int decimalPlaces;
	private final int minPrice;
	private final int maxPrice;

	private HistDataInstrument(IInstrument instrument, int decimalPlaces, int minPrice, int maxPrice) {
		if (decimalPlaces < 0) {
			throw new IllegalArgumentException("decimalPlaces=" + decimalPlaces);
		}
		if (minPrice < 1) {
			throw new IllegalArgumentException("minPrice=" + minPrice);
		}
		if (maxPrice <= minPrice) {
			throw new IllegalArgumentException("minPrice=" + minPrice + ", maxPrice=" + maxPrice);
		}

		this.instrument = instrument;
		this.decimalPlaces = decimalPlaces;
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
	}

	@Override
	public int getDecimalPlaces() {
		return decimalPlaces;
	}

	@Override
	public IInstrument getInstrument() {
		return instrument;
	}

	public static HistDataInstrument valueOf(IInstrument instrument) {
		for (HistDataInstrument data : values()) {
			if (data.getInstrument().equals(instrument)) {
				return data;
			}
		}
		throw new IllegalArgumentException("Instrument not supported: " + instrument);
	}

	@Override
	public int getMinPrice() {
		return minPrice;
	}

	@Override
	public int getMaxPrice() {
		return maxPrice;
	}

}
