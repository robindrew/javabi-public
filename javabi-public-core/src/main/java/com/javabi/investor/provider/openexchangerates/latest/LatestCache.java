package com.javabi.investor.provider.openexchangerates.latest;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Map.Entry;

import com.javabi.common.io.file.Files;
import com.javabi.common.io.file.datetime.DateTimeFileStorage;
import com.javabi.common.io.file.datetime.IDateTimeFileStorage;

public class LatestCache {

	public static void main(String[] args) {
		LatestCache cache = new LatestCache(new File("src/test/resources/cache"));
		Map<LocalDateTime, File> files = cache.getAll();
		for (Entry<LocalDateTime, File> entry : files.entrySet()) {

			LocalDateTime date = entry.getKey();
			File file = entry.getValue();

			LatestJson json = LatestJson.readFromFile(file);

			System.out.println(date.equals(json.getDate(true)));
			System.out.println(json.getDate());
		}
	}

	private static final String FILE_EXTENSION = ".json";

	private final IDateTimeFileStorage storage;

	public LatestCache(File directory) {
		this.storage = new DateTimeFileStorage(directory, FILE_EXTENSION);
	}

	public Map<LocalDateTime, File> getAll() {
		return storage.getAllFiles();
	}

	public void cache(LocalDateTime date, String contents) {
		File file = storage.getFile(date, true);
		Files.writeFromString(file, contents);
	}

}
