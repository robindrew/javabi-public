package com.javabi.investor.provider.openexchangerates;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.javabi.common.io.RuntimeIOException;

public class OpenExchangeRateCurrency {

	public static List<OpenExchangeRateCurrency> readCurrencyList(File file) {
		try (FileReader reader = new FileReader(file)) {
			return readCurrencyList(reader);
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}

	private static List<OpenExchangeRateCurrency> readCurrencyList(FileReader reader) {
		JsonParser parser = new JsonParser();
		JsonElement json = parser.parse(reader);
		JsonObject object = json.getAsJsonObject();

		List<OpenExchangeRateCurrency> list = new ArrayList<>();
		for (Entry<String, JsonElement> entry : object.entrySet()) {
			String code = entry.getKey();
			String description = entry.getValue().getAsString();
			list.add(new OpenExchangeRateCurrency(code, description));
		}
		return list;
	}

	private final String code;
	private final String description;

	public OpenExchangeRateCurrency(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

}
