package com.javabi.investor.platform;

import com.javabi.investor.IInstrument;
import com.javabi.investor.trade.window.ITradingWindow;

public class TradingInstrument implements ITradingInstrument {

	private final IInstrument instrument;
	private final double pipSize;
	private final double spreadSize;
	private final ITradingWindow window;

	public TradingInstrument(IInstrument instrument, double pipSize, double spreadSize, ITradingWindow window) {
		if (instrument == null) {
			throw new NullPointerException("instrument");
		}
		if (window == null) {
			throw new NullPointerException("window");
		}
		if (pipSize <= 0.0) {
			throw new IllegalArgumentException("pipSize=" + pipSize);
		}
		if (spreadSize <= 0.0) {
			throw new IllegalArgumentException("spreadSize=" + spreadSize);
		}

		this.instrument = instrument;
		this.pipSize = pipSize;
		this.spreadSize = spreadSize;
		this.window = window;
	}

	@Override
	public IInstrument getInstrument() {
		return instrument;
	}

	@Override
	public double getPipSize() {
		return pipSize;
	}

	@Override
	public double getSpreadSize() {
		return spreadSize;
	}

	@Override
	public ITradingWindow getWindow() {
		return window;
	}

}
