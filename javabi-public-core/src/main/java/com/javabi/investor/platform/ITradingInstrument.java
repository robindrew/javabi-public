package com.javabi.investor.platform;

import com.javabi.investor.IInstrument;
import com.javabi.investor.trade.window.ITradingWindow;

public interface ITradingInstrument {

	IInstrument getInstrument();

	double getPipSize();

	double getSpreadSize();

	ITradingWindow getWindow();

}
