package com.javabi.investor.platform.position;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.javabi.common.locale.CurrencyCode;
import com.javabi.investor.IInstrument;
import com.javabi.investor.trade.TradeDirection;

public interface IPosition {

	String getId();

	IInstrument getInstrument();

	TradeDirection getDirection();

	LocalDateTime getOpenDate();

	CurrencyCode getTradeCurrency();

	BigDecimal getOpenPrice();

	BigDecimal getTradeSize();

}
