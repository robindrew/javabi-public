package com.javabi.investor.platform;

import java.util.Collection;
import java.util.List;

import com.javabi.investor.IInstrument;
import com.javabi.investor.platform.position.IPosition;
import com.javabi.investor.price.candle.io.stream.sink.subscriber.IPriceCandleSubscriberStreamSinkProvider;
import com.javabi.investor.trade.Funds;

public interface ITradingPlatform extends IPriceCandleSubscriberStreamSinkProvider {

	boolean supports(IInstrument instrument);

	ITradingInstrument getInstrument(IInstrument instrument);

	List<IPosition> getAllPositions();

	List<IPosition> getPositions(IInstrument instrument);

	void closePosition(IPosition position);

	void closePositions(Collection<? extends IPosition> positions);
	
	Funds getAvailableFunds();
}
