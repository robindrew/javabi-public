package com.javabi.investor.platform;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.javabi.investor.IInstrument;
import com.javabi.investor.platform.position.IPosition;
import com.javabi.investor.price.candle.io.stream.sink.subscriber.IPriceCandleSubscriberStreamSink;

public abstract class TradingPlatform implements ITradingPlatform {

	private final ConcurrentMap<IInstrument, ITradingInstrument> instrumentMap = new ConcurrentHashMap<>();
	private final ConcurrentMap<IInstrument, IPriceCandleSubscriberStreamSink> sinkMap = new ConcurrentHashMap<>();

	@Override
	public boolean supports(IInstrument instrument) {
		if (instrument == null) {
			throw new NullPointerException("instrument");
		}
		return instrumentMap.containsKey(instrument);
	}

	@Override
	public ITradingInstrument getInstrument(IInstrument instrument) {
		if (instrument == null) {
			throw new NullPointerException("instrument");
		}

		ITradingInstrument value = instrumentMap.get(instrument);
		if (value == null) {
			throw new IllegalArgumentException("Instrument not registered: " + instrument);
		}
		return value;
	}

	public boolean register(ITradingInstrument instrument) {
		if (instrument == null) {
			throw new NullPointerException("instrument");
		}
		return instrumentMap.putIfAbsent(instrument.getInstrument(), instrument) == null;
	}

	public boolean register(IPriceCandleSubscriberStreamSink sink) {
		if (sink == null) {
			throw new NullPointerException("sink");
		}
		return sinkMap.putIfAbsent(sink.getInstrument(), sink) == null;
	}

	@Override
	public IPriceCandleSubscriberStreamSink getSubscriber(IInstrument instrument) {

		// Verify the instrument is registered
		getInstrument(instrument);

		// Lookup the sink
		IPriceCandleSubscriberStreamSink sink = sinkMap.get(instrument);
		if (sink == null) {
			throw new IllegalArgumentException("No sink registered for instrument: " + instrument);
		}
		return sink;
	}

	@Override
	public void closePositions(Collection<? extends IPosition> positions) {
		for (IPosition position : positions) {
			closePosition(position);
		}
	}

	@Override
	public List<IPosition> getPositions(IInstrument instrument) {
		List<IPosition> positions = new ArrayList<>();
		for (IPosition position : getAllPositions()) {
			if (position.getInstrument().equals(instrument)) {
				positions.add(position);
			}
		}
		return positions;
	}
}
