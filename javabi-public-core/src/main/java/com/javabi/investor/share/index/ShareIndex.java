package com.javabi.investor.share.index;

import com.javabi.common.locale.Country;
import com.javabi.investor.IInstrument;

public enum ShareIndex implements IInstrument {

	/** FTSE 100. */
	FTSE_100("FTSE 100", 100, Country.UNITED_KINGDOM),
	/** Dow Jones Industrial Average. */
	DJIA("Dow Jones Industrial Average", 30, Country.UNITED_STATES),
	/** S&P 500. */
	SP_500("S&P 500", 500, Country.UNITED_STATES),
	/** DAX. */
	DAX("DAX", 30, Country.GERMANY),
	/** CAC 40. */
	CAC_40("CAC 40", 40, Country.FRANCE);

	private final String name;
	private final int constituents;
	private final Country country;

	private ShareIndex(String name, int constituents, Country country) {
		this.name = name;
		this.constituents = constituents;
		this.country = country;
	}

	public String getName() {
		return name;
	}

	public int getConstituents() {
		return constituents;
	}

	public Country getCountry() {
		return country;
	}

}
