package com.javabi.investor.simulator;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.investor.platform.ITradingInstrument;
import com.javabi.investor.platform.ITradingPlatform;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.stream.sink.IPriceCandleStreamSink;
import com.javabi.investor.price.candle.io.stream.sink.subscriber.IPriceCandleSubscriberStreamSink;
import com.javabi.investor.trade.Funds;

public abstract class TradingSimulator implements ITradingSimulator {

	private static final Logger log = LoggerFactory.getLogger(TradingSimulator.class);

	private final ITradingInstrument instrument;
	private final ITradingPlatform platform;
	private final Funds funds;

	protected TradingSimulator(ITradingInstrument instrument, ITradingPlatform platform, Funds funds) {
		if (instrument == null) {
			throw new NullPointerException("instrument");
		}
		if (funds == null) {
			throw new NullPointerException("funds");
		}
		this.instrument = instrument;
		this.platform = platform;
		this.funds = funds;
	}

	public ITradingPlatform getPlatform() {
		return platform;
	}

	public Funds getFunds() {
		return funds;
	}

	@Override
	public void run() {
		try {

			// Get the subscriber for the instrument we are trading
			IPriceCandleSubscriberStreamSink subscriber = platform.getSubscriber(instrument.getInstrument());

			// Register the simulated sink with the subscriber
			SimulatorSink sink = new SimulatorSink(getName() + "Sink");
			subscriber.subscribe(sink);
			
			// Wait until complete ...
			sink.waitUntilClosed();

		} catch (Exception e) {
			log.warn("Simulator Crashed! instrument=" + instrument + ", funds=" + funds, e);
		}
	}

	private class SimulatorSink implements IPriceCandleStreamSink {

		private final String name;
		private final CountDownLatch closed = new CountDownLatch(1);
		private IPriceCandle previous = null;

		public SimulatorSink(String name) {
			if (name.isEmpty()) {
				throw new IllegalArgumentException("name is empty");
			}
			this.name = name;
		}

		public void waitUntilClosed() throws InterruptedException {
			closed.await();
		}

		@Override
		public void close() throws IOException {
			closed.countDown();
		}

		@Override
		public void putNextCandle(IPriceCandle current) throws IOException {

			// No more candles?
			if (current == null) {
				if (previous == null) {
					log.warn("No ticks received");
				} else {
					lastTick(previous);
				}
				close();
				return;
			}

			// Handle current candle
			if (previous == null) {
				firstTick(current);
			} else {
				tick(previous, current);
			}

			// Store previous candle
			previous = current;
		}

		@Override
		public String getName() {
			return name;
		}
	}

}
