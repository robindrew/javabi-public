package com.javabi.investor.simulator;

import com.javabi.investor.price.candle.IPriceCandle;

public interface ITradingSimulator extends Runnable {

	String getName();

	void firstTick(IPriceCandle current);

	void tick(IPriceCandle previous, IPriceCandle current);

	void lastTick(IPriceCandle previous);

}
