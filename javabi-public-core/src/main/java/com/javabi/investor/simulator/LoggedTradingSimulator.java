package com.javabi.investor.simulator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.investor.platform.ITradingInstrument;
import com.javabi.investor.platform.ITradingPlatform;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.trade.Funds;

public class LoggedTradingSimulator extends SingleTradeSimulator {

	private static final Logger log = LoggerFactory.getLogger(LoggedTradingSimulator.class);

	public LoggedTradingSimulator(ITradingInstrument instrument, ITradingPlatform platform, Funds funds) {
		super(instrument, platform, funds);
	}

	@Override
	public String getName() {
		return "LoggedTradingSimulator";
	}

	@Override
	public void firstTick(IPriceCandle current) {
		log.info("[First Tick] " + current);
	}

	@Override
	public void tick(IPriceCandle previous, IPriceCandle current) {
		log.info("[Tick] " + current);
	}

	@Override
	public void lastTick(IPriceCandle previous) {
		log.info("[Last Tick] " + previous);
	}

}
