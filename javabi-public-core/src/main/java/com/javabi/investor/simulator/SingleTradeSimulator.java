package com.javabi.investor.simulator;

import java.util.concurrent.atomic.AtomicReference;

import com.javabi.investor.platform.ITradingInstrument;
import com.javabi.investor.platform.ITradingPlatform;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.trade.Funds;
import com.javabi.investor.trade.ITrade;
import com.javabi.investor.trade.Trades;

public abstract class SingleTradeSimulator extends TradingSimulator {

	private final AtomicReference<ITrade> openTrade = new AtomicReference<>();

	protected SingleTradeSimulator(ITradingInstrument instrument, ITradingPlatform platform, Funds funds) {
		super(instrument, platform, funds);
	}

	public boolean hasOpenTrade() {
		return openTrade.get() != null;
	}

	public ITrade getOpenTrade() {
		ITrade trade = openTrade.get();
		if (trade == null) {
			throw new IllegalStateException("Trade does not exist");
		}
		return trade;
	}

	public void openTrade(ITrade trade) {
		if (trade == null) {
			throw new NullPointerException("trade");
		}
		if (!openTrade.compareAndSet(null, trade)) {
			throw new IllegalArgumentException("Trade already exists");
		}
	}

	public void closeTrade(IPriceCandle candle) {
		closeTrade(candle.getClosePrice());
	}

	public void closeTrade(double closePrice) {
		ITrade trade = openTrade.get();
		if (trade != null && openTrade.compareAndSet(trade, null)) {
			Trades.close(trade, getFunds(), closePrice);
		}
	}

}
