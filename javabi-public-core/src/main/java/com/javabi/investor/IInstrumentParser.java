package com.javabi.investor;

import com.javabi.common.text.parser.IStringParser;

public interface IInstrumentParser extends IStringParser<IInstrument> {

}
