package com.javabi.investor.price.candle.analysis;

import java.io.IOException;
import java.util.List;

import com.javabi.investor.IInstrument;
import com.javabi.investor.price.candle.IPriceCandle;

public interface IPriceCandleAnalysis {

	void performAnalysis(IInstrument instrument, List<IPriceCandle> candles) throws IOException;

}
