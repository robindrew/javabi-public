package com.javabi.investor.price.candle.io;

import static com.javabi.common.text.StringFormats.number;
import static com.javabi.investor.price.candle.io.stream.sink.PriceCandleStreamDataSink.createFileDataSink;
import static com.javabi.investor.price.candle.io.stream.source.PriceCandleDataStreamSource.createLineSource;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.date.timer.ITimer;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.list.filter.PriceCandleListSortedFilter;
import com.javabi.investor.price.candle.io.list.sink.IPriceCandleListSink;
import com.javabi.investor.price.candle.io.list.sink.PriceCandleListToStreamSink;
import com.javabi.investor.price.candle.io.list.source.IPriceCandleListSource;
import com.javabi.investor.price.candle.io.list.source.PriceCandleListFilteredSource;
import com.javabi.investor.price.candle.io.stream.source.IPriceCandleStreamSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleDataStreamSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleStreamToListSource;
import com.javabi.investor.price.candle.line.formatter.IPriceCandleLineFormatter;
import com.javabi.investor.price.candle.line.formatter.PriceCandleLineFormatter;

public class PriceCandleSources {

	private static final Logger log = LoggerFactory.getLogger(PriceCandleSources.class);

	public static void writeCandlesToLineWriter(IPriceCandleStreamSource source, Writer writer) throws IOException {
		IPriceCandleLineFormatter formatter = new PriceCandleLineFormatter();
		while (true) {
			IPriceCandle candle = source.getNextCandle();
			if (candle == null) {
				break;
			}

			// Build and write the line
			String line = formatter.formatCandle(candle, true);
			writer.write(line);
		}
	}

	public static void writeCandlesToLineFile(IPriceCandleStreamSource source, File file) throws IOException {
	}

	public static void writeCandlesToDataFile(File toFile, List<IPriceCandle> candleList) throws IOException {
		if (candleList.isEmpty()) {
			throw new IllegalArgumentException("candleList is empty");
		}
		int decimalPlaces = candleList.get(0).getDecimalPlaces();

		log.info("Writing " + number(candleList) + " candles to file " + toFile);
		ITimer timer = NanoTimer.startTimer();

		try (IPriceCandleListSink sink = new PriceCandleListToStreamSink(createFileDataSink(toFile, new PriceCandleDataSerializer(decimalPlaces)))) {
			sink.putNextCandles(candleList);
		}

		timer.stop();
		log.info("Written " + number(candleList) + " candles to file " + toFile + " in " + timer);
	}

	public static List<IPriceCandle> readStreamToSortedList(IPriceCandleStreamSource stream) throws IOException {
		log.info("Reading candles from " + stream.getName());
		ITimer timer = NanoTimer.startTimer();

		final List<IPriceCandle> candleList;
		try (IPriceCandleListSource list = new PriceCandleListFilteredSource(new PriceCandleStreamToListSource(stream), new PriceCandleListSortedFilter())) {
			candleList = list.getNextCandles();
		}

		timer.stop();
		log.info("Read " + number(candleList) + " candles from " + stream.getName() + " in " + timer);
		return candleList;
	}

	public static List<IPriceCandle> readCandlesFromDataFile(File fromFile, int decimalPlaces) throws IOException {
		try (PriceCandleDataStreamSource stream = createLineSource(fromFile, new PriceCandleDataSerializer(decimalPlaces))) {
			return PriceCandleSources.readStreamToSortedList(stream);
		}
	}

}
