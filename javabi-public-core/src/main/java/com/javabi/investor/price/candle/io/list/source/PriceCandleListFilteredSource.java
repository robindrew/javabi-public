package com.javabi.investor.price.candle.io.list.source;

import java.io.IOException;
import java.util.List;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.list.filter.IPriceCandleListFilter;

public class PriceCandleListFilteredSource implements IPriceCandleListSource {

	private final IPriceCandleListSource source;
	private final IPriceCandleListFilter filter;

	public PriceCandleListFilteredSource(IPriceCandleListSource source, IPriceCandleListFilter filter) {
		if (source == null) {
			throw new NullPointerException("source");
		}
		if (filter == null) {
			throw new NullPointerException("filter");
		}
		this.source = source;
		this.filter = filter;
	}

	@Override
	public String getName() {
		return source.getName();
	}

	@Override
	public void close() throws IOException {
		source.close();
	}

	@Override
	public List<IPriceCandle> getNextCandles() throws IOException {
		List<IPriceCandle> candles = source.getNextCandles();
		return filter.filter(candles);
	}

}
