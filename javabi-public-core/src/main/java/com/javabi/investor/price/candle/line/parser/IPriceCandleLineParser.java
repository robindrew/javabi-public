package com.javabi.investor.price.candle.line.parser;

import com.javabi.investor.price.candle.IPriceCandle;

public interface IPriceCandleLineParser {

	IPriceCandle parseCandle(String line);
	
}
