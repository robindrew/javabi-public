package com.javabi.investor.price.candle.io.stream.source;

import static com.javabi.investor.price.candle.PriceCandles.merge;

import java.io.IOException;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.interval.IPriceCandleInterval;

/**
 * Note that this does NOT support monthly intervals or above, as months have different lengths, and years are subject
 * to leap year loss.
 */
public class PriceCandleIntervalStreamSource implements IPriceCandleStreamSource {

	private final IPriceCandleStreamSource source;
	private final IPriceCandleInterval interval;

	private IPriceCandle current = null;
	private boolean finished = false;

	public PriceCandleIntervalStreamSource(IPriceCandleStreamSource source, IPriceCandleInterval interval) {
		if (source == null) {
			throw new NullPointerException("source");
		}
		if (interval == null) {
			throw new NullPointerException("interval");
		}
		this.source = source;
		this.interval = interval;
	}

	public IPriceCandleInterval getInterval() {
		return interval;
	}

	@Override
	public String getName() {
		return source.getName();
	}

	@Override
	public void close() throws IOException {
		source.close();
	}

	@Override
	public IPriceCandle getNextCandle() throws IOException {
		if (finished) {
			return null;
		}

		if (current == null) {
			current = source.getNextCandle();
		}
		if (current == null) {
			finished = true;
			return null;
		}

		// Merge candles in the same interval
		IPriceCandle merged = current;
		long timePeriod = interval.getTimePeriod(current);
		while (true) {
			current = source.getNextCandle();
			if (current == null) {
				finished = true;
				break;
			}

			if (timePeriod != interval.getTimePeriod(current)) {
				break;
			}

			merged = merge(merged, current);
		}

		return merged;
	}

}
