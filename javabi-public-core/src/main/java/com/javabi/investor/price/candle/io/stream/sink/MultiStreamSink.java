package com.javabi.investor.price.candle.io.stream.sink;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import com.javabi.investor.price.candle.IPriceCandle;

public class MultiStreamSink implements IPriceCandleStreamSink {

	private final String name;
	private final Set<IPriceCandleStreamSink> sinks = new CopyOnWriteArraySet<>();

	public MultiStreamSink(String name) {
		if (name == null) {
			throw new NullPointerException("name");
		}
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void close() throws IOException {
		for (IPriceCandleStreamSink sink : sinks) {
			sink.close();
		}
		sinks.clear();
	}

	@Override
	public void putNextCandle(IPriceCandle candle) throws IOException {
		for (IPriceCandleStreamSink sink : sinks) {
			sink.putNextCandle(candle);
		}
	}
}
