package com.javabi.investor.price.candle.analysis;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ListMultimap;
import com.javabi.investor.IInstrument;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.interval.ChronoUnitInterval;
import com.javabi.investor.price.candle.interval.IPriceCandleInterval;
import com.javabi.investor.price.candle.interval.PriceCandleIntervalPartitioner;

public class ConsecutiveCandleAnalysis implements IPriceCandleAnalysis {

	private static final Logger log = LoggerFactory.getLogger(ConsecutiveCandleAnalysis.class);

	@Override
	public void performAnalysis(IInstrument instrument, List<IPriceCandle> candles) throws IOException {
		IPriceCandleInterval interval = new ChronoUnitInterval(ChronoUnit.HOURS);

		ListMultimap<LocalDateTime, IPriceCandle> partitions = new PriceCandleIntervalPartitioner(interval).partition(candles);

		for (LocalDateTime date : new TreeSet<>(partitions.keySet())) {
			List<IPriceCandle> list = partitions.get(date);

			long total = 0;
			int count = 0;
			for (IPriceCandle candle : list) {
				count++;
				total += candle.getHighLowRange();
			}

			log.info("[Date] " + date + " -> " + (total / count));
		}

	}

}
