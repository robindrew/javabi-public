package com.javabi.investor.price.candle.interval;

import java.time.LocalDateTime;

import com.javabi.investor.price.candle.IPriceCandle;

public interface IPriceCandleInterval {

	long getTimePeriod(IPriceCandle candle);

	LocalDateTime getDateTime(IPriceCandle candle);

}