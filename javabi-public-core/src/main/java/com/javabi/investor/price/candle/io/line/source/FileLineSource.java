package com.javabi.investor.price.candle.io.line.source;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import com.google.common.base.Charsets;
import com.javabi.common.io.file.FileByteSource;

public class FileLineSource extends ByteLineSource {

	private final File file;

	public FileLineSource(File file, Charset charset) throws IOException {
		super(file.getName(), new FileByteSource(file), charset);
		this.file = file;
	}

	public FileLineSource(File file) throws IOException {
		this(file, Charsets.UTF_8);
	}

	public File getFile() {
		return file;
	}

}
