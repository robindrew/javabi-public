package com.javabi.investor.price.candle.filter;

import com.javabi.investor.price.candle.IPriceCandle;

@FunctionalInterface
public interface IPriceCandleFilter {

	boolean accept(IPriceCandle candle);

}
