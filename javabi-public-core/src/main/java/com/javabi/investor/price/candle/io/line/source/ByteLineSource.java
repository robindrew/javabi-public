package com.javabi.investor.price.candle.io.line.source;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import com.google.common.io.ByteSource;

public class ByteLineSource implements ILineSource {

	private final String name;
	private final BufferedReader reader;
	private boolean closed = false;

	public ByteLineSource(String name, ByteSource source, Charset charset) throws IOException {
		if (name.isEmpty()) {
			throw new IllegalArgumentException("name is empty");
		}
		if (source == null) {
			throw new NullPointerException("source");
		}
		if (charset == null) {
			throw new NullPointerException("charset");
		}
		this.name = name;

		// Create the reader
		this.reader = new BufferedReader(new InputStreamReader(source.openStream(), charset));
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void close() throws IOException {
		closed = true;
		reader.close();
	}

	@Override
	public String getNextLine() throws IOException {
		if (closed) {
			throw new IllegalStateException("Source is closed: " + getName());
		}

		try {
			return reader.readLine();
		} catch (IOException e) {
			throw e;
		}
	}

}
