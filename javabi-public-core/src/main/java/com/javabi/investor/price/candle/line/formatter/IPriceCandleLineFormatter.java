package com.javabi.investor.price.candle.line.formatter;

import com.javabi.investor.price.candle.IPriceCandle;

public interface IPriceCandleLineFormatter {

	String formatCandle(IPriceCandle candle, boolean includeEndOfLine);

}
