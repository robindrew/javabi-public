package com.javabi.investor.price.candle.io.list.sink;

import java.io.IOException;
import java.util.List;

import com.javabi.common.io.INamedCloseable;
import com.javabi.investor.price.candle.IPriceCandle;

public interface IPriceCandleListSink extends INamedCloseable {

	void putNextCandles(List<IPriceCandle> candles) throws IOException;

}
