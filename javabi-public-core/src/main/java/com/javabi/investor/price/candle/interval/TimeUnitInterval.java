package com.javabi.investor.price.candle.interval;

import static java.util.concurrent.TimeUnit.DAYS;
import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import com.javabi.common.date.Dates;
import com.javabi.common.date.UnitTime;
import com.javabi.investor.price.candle.IPriceCandle;

public class TimeUnitInterval implements IPriceCandleInterval {

	public static final TimeUnitInterval ONE_SECOND = new TimeUnitInterval(1, SECONDS);
	public static final TimeUnitInterval ONE_MINUTE = new TimeUnitInterval(1, MINUTES);
	public static final TimeUnitInterval ONE_HOUR = new TimeUnitInterval(1, HOURS);
	public static final TimeUnitInterval ONE_DAY = new TimeUnitInterval(1, DAYS);

	public static final TimeUnitInterval hours(int number) {
		return new TimeUnitInterval(number, HOURS);
	}

	public static final TimeUnitInterval minutes(int number) {
		return new TimeUnitInterval(number, MINUTES);
	}

	public static final TimeUnitInterval days(int number) {
		return new TimeUnitInterval(number, DAYS);
	}

	private final long intervalInMillis;
	private final UnitTime interval;

	public TimeUnitInterval(UnitTime interval) {
		if (interval == null) {
			throw new NullPointerException("interval");
		}
		this.interval = interval;
		this.intervalInMillis = interval.getTime(MILLISECONDS);
	}

	public TimeUnitInterval(long interval, TimeUnit unit) {
		this(new UnitTime(interval, unit));
	}

	public long getIntervalInMillis() {
		return intervalInMillis;
	}

	public UnitTime getInterval() {
		return interval;
	}

	@Override
	public long getTimePeriod(IPriceCandle candle) {
		return candle.getOpenTime() / intervalInMillis;
	}

	@Override
	public LocalDateTime getDateTime(IPriceCandle candle) {
		long timeInMillis = getTimePeriod(candle);
		return Dates.toLocalDateTime(timeInMillis);
	}

}
