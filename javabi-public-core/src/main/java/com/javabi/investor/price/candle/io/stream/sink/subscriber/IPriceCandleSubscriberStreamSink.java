package com.javabi.investor.price.candle.io.stream.sink.subscriber;

import com.javabi.investor.IInstrument;
import com.javabi.investor.price.candle.io.stream.sink.IPriceCandleStreamSink;

public interface IPriceCandleSubscriberStreamSink extends IPriceCandleStreamSink {

	IInstrument getInstrument();

	boolean subscribe(IPriceCandleStreamSink sink);

	boolean unsubscribe(IPriceCandleStreamSink sink);

}
