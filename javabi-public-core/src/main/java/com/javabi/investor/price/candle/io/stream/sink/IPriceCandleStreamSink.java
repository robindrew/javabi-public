package com.javabi.investor.price.candle.io.stream.sink;

import java.io.IOException;

import com.javabi.common.io.INamedCloseable;
import com.javabi.investor.price.candle.IPriceCandle;

public interface IPriceCandleStreamSink extends INamedCloseable {

	void putNextCandle(IPriceCandle candle) throws IOException;
	
}
