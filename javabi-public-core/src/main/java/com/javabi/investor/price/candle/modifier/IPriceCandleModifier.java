package com.javabi.investor.price.candle.modifier;

import com.javabi.investor.price.candle.IPriceCandle;

public interface IPriceCandleModifier {

	IPriceCandle modify(IPriceCandle candle);

}
