package com.javabi.investor.price.candle;

import com.javabi.investor.price.close.IClosePrice;

public interface IPriceCandle extends IClosePrice {

	int getOpenPrice();

	int getHighPrice();

	int getLowPrice();

	@Override
	int getClosePrice();

	int getDecimalPlaces();

	long getOpenTime();

	long getCloseTime();

	long getCloseAmount();

	long getHighLowRange();

	long getOpenCloseRange();

	IPriceCandle mergeWith(IPriceCandle candle);

	boolean isInstant();

	boolean hasClosedUp();

	double getMedian();

	double getTypical();

	double getWeighted();

	boolean after(IPriceCandle candle);

	boolean before(IPriceCandle candle);

	boolean containsPrice(int price);

}