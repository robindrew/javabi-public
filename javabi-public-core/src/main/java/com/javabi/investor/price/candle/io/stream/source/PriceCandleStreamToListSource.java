package com.javabi.investor.price.candle.io.stream.source;

import static com.javabi.common.text.StringFormats.number;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.date.timer.ITimer;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.list.source.IPriceCandleListSource;

public class PriceCandleStreamToListSource implements IPriceCandleListSource {

	private static final Logger log = LoggerFactory.getLogger(PriceCandleStreamToListSource.class);

	private final IPriceCandleStreamSource source;

	public PriceCandleStreamToListSource(IPriceCandleStreamSource source) {
		if (source == null) {
			throw new NullPointerException("source");
		}
		this.source = source;
	}

	@Override
	public String getName() {
		return source.getName();
	}

	@Override
	public void close() throws IOException {
		source.close();
	}

	@Override
	public List<IPriceCandle> getNextCandles() throws IOException {
		log.info("Reading all candles from " + getName());
		ITimer timer = NanoTimer.startTimer();

		List<IPriceCandle> list = new ArrayList<>();
		while (true) {
			IPriceCandle candle = source.getNextCandle();
			if (candle == null) {
				break;
			}
			list.add(candle);
		}

		timer.stop();
		log.info("Read " + number(list) + " candles from " + getName() + " in " + timer);
		return list;
	}

}
