package com.javabi.investor.price.close;

public interface IClosePrice {

	int getClosePrice();

}
