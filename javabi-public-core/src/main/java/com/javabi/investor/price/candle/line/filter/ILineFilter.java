package com.javabi.investor.price.candle.line.filter;

public interface ILineFilter {

	String filter(String line);
	
}
