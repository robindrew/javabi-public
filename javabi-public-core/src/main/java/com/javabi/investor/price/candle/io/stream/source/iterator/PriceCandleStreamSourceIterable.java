package com.javabi.investor.price.candle.io.stream.source.iterator;

import java.util.Iterator;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.stream.source.IPriceCandleStreamSource;

public class PriceCandleStreamSourceIterable implements Iterable<IPriceCandle> {

	public static PriceCandleStreamSourceIterable iterator(IPriceCandleStreamSource source) {
		return new PriceCandleStreamSourceIterable(source);
	}

	private final IPriceCandleStreamSource source;

	public PriceCandleStreamSourceIterable(IPriceCandleStreamSource source) {
		if (source == null) {
			throw new NullPointerException("source");
		}
		this.source = source;
	}

	@Override
	public Iterator<IPriceCandle> iterator() {
		return PriceCandleStreamSourceIterator.iterator(source);
	}

}
