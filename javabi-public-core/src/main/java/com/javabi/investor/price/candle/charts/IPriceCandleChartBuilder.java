package com.javabi.investor.price.candle.charts;

import java.util.List;
import java.util.function.Supplier;

import com.javabi.investor.IInstrument;
import com.javabi.investor.price.candle.IPriceCandle;

public interface IPriceCandleChartBuilder extends Supplier<IPriceCandleChart> {

	IPriceCandleChartBuilder setInstrument(IInstrument instrument);

	IPriceCandleChartBuilder setCandles(List<IPriceCandle> candles);

	IPriceCandleChartBuilder setDimensions(int width, int height);

	IPriceCandleChartBuilder setDateAxisLabel(String label);

	IPriceCandleChartBuilder setPriceAxisLabel(String label);

}
