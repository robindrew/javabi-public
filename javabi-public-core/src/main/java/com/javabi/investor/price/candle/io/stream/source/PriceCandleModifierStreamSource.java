package com.javabi.investor.price.candle.io.stream.source;

import java.io.IOException;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.modifier.IPriceCandleModifier;

public class PriceCandleModifierStreamSource implements IPriceCandleStreamSource {

	private final IPriceCandleStreamSource source;
	private final IPriceCandleModifier modifier;

	public PriceCandleModifierStreamSource(IPriceCandleStreamSource source, IPriceCandleModifier modifier) {
		if (source == null) {
			throw new NullPointerException("source");
		}
		if (modifier == null) {
			throw new NullPointerException("modifier");
		}
		this.source = source;
		this.modifier = modifier;
	}

	@Override
	public String getName() {
		return source.getName();
	}

	@Override
	public void close() throws IOException {
		source.close();
	}

	@Override
	public IPriceCandle getNextCandle() throws IOException {
		IPriceCandle next = source.getNextCandle();
		if (next == null) {
			return next;
		}

		// Apply modifier
		return modifier.modify(next);
	}

}
