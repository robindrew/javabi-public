package com.javabi.investor.price.candle.io.stream.sink;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.concurrent.Threads;
import com.javabi.common.date.UnitTime;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.list.sink.IPriceCandleListSink;

public class QueuedPriceCandleSink extends Thread implements IPriceCandleStreamSink, IPriceCandleListSink {

	private static final Logger log = LoggerFactory.getLogger(QueuedPriceCandleSink.class);

	private final IPriceCandleListSink delegate;
	private final BlockingDeque<IPriceCandle> queue = new LinkedBlockingDeque<>();
	private final AtomicBoolean shutdown = new AtomicBoolean(false);
	private volatile UnitTime frequency = new UnitTime(50, MILLISECONDS);

	public QueuedPriceCandleSink(IPriceCandleListSink delegate) {
		this.delegate = delegate;
	}

	public void setFrequency(UnitTime frequency) {
		if (frequency == null) {
			throw new NullPointerException("frequency");
		}
		if (frequency.getTime(MILLISECONDS) < 1) {
			throw new IllegalArgumentException("frequency=" + frequency);
		}
		this.frequency = frequency;
	}

	@Override
	public void run() {
		try {
			while (!shutdown.get()) {

				// Throttling
				Threads.sleep(frequency);

				// Consume ticks
				consumeTicks();
			}
		} catch (Exception e) {
			log.error("Unexpected queue failure", e);
		}
	}

	private void consumeTicks() throws IOException {
		List<IPriceCandle> candleList = new ArrayList<>();
		queue.drainTo(candleList);
		delegate.putNextCandles(candleList);
	}

	public void shutdown() {
		shutdown.get();
	}

	@Override
	public void close() throws IOException {
		delegate.close();
	}

	@Override
	public void putNextCandle(IPriceCandle candle) throws IOException {
		queue.add(candle);
	}

	@Override
	public void putNextCandles(List<IPriceCandle> candles) throws IOException {
		queue.addAll(candles);
	}

}
