package com.javabi.investor.price.candle.io.list.sink;

import static com.google.common.base.Charsets.UTF_8;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.date.Dates;
import com.javabi.common.io.file.Files;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.line.formatter.IPriceCandleLineFormatter;

public class DailyFilePriceCandleListConsumer implements IPriceCandleListSink {

	private static final Logger log = LoggerFactory.getLogger(DailyFilePriceCandleListConsumer.class);

	private final File directory;
	private final String filename;
	private final IPriceCandleLineFormatter formatter;

	public DailyFilePriceCandleListConsumer(File directory, String name, IPriceCandleLineFormatter formatter) {
		if (directory == null) {
			throw new NullPointerException("directory");
		}
		if (name.isEmpty()) {
			throw new IllegalArgumentException("name is empty");
		}
		if (formatter == null) {
			throw new NullPointerException("formatter");
		}

		this.directory = Files.existingDirectory(directory);
		this.filename = name;
		this.formatter = formatter;
	}

	@Override
	public void putNextCandles(List<IPriceCandle> candles) throws IOException {
		if (candles.isEmpty()) {
			return;
		}

		LocalDate date = getDate(candles);
		writeCandles(candles, date);
	}

	private LocalDate getDate(List<IPriceCandle> candles) {
		IPriceCandle first = candles.get(0);
		return Dates.toLocalDateTime(first.getOpenTime()).toLocalDate();
	}

	private void writeCandles(List<IPriceCandle> candles, LocalDate date) {
		String lines = toLines(candles);

		// We push all the lines out in one single write
		// Hopefully this will minimise write time and avoid partial writes if
		// the application crashes
		File file = getFile(date);
		try {
			try (FileOutputStream output = new FileOutputStream(file, true)) {
				output.write(lines.getBytes(UTF_8));
			}
		} catch (Exception e) {
			log.warn("Failed to write candles to file: " + filename);
		}
	}

	private File getFile(LocalDate date) {
		return new File(directory, filename + "." + date + ".txt");
	}

	private String toLines(List<IPriceCandle> candles) {
		StringBuilder lines = new StringBuilder();
		for (IPriceCandle candle : candles) {
			lines.append(formatter.formatCandle(candle, true));
		}
		return lines.toString();
	}

	@Override
	public String getName() {
		return filename;
	}

	@Override
	public void close() throws IOException {
		// Nothing to do
	}

}
