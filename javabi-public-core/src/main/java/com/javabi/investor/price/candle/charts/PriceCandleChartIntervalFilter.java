package com.javabi.investor.price.candle.charts;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.interval.IPriceCandleInterval;
import com.javabi.investor.price.candle.io.stream.source.IPriceCandleStreamSourceBuilder;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleStreamSourceBuilder;

public class PriceCandleChartIntervalFilter {

	private static final LocalDateTime THE_FUTURE = LocalDateTime.of(3000, 01, 01, 0, 0, 0);

	private final LocalDateTime fromDate;
	private final IPriceCandleInterval interval;
	private final int maxCandles;

	public PriceCandleChartIntervalFilter(LocalDateTime fromDate, IPriceCandleInterval interval, int maxCandles) {
		if (fromDate == null) {
			throw new NullPointerException("fromDate");
		}
		if (interval == null) {
			throw new NullPointerException("interval");
		}
		if (maxCandles < 1) {
			throw new IllegalArgumentException("maxCandles=" + maxCandles);
		}
		this.fromDate = fromDate;
		this.interval = interval;
		this.maxCandles = maxCandles;
	}

	public List<IPriceCandle> filterCandles(String directory) throws IOException {
		IPriceCandleStreamSourceBuilder builder = new PriceCandleStreamSourceBuilder();
		builder.setPcfDirectory(directory);
		builder.setInterval(interval);
		builder.between(fromDate, THE_FUTURE);
		builder.limit(maxCandles);
		return builder.getList();
	}

}
