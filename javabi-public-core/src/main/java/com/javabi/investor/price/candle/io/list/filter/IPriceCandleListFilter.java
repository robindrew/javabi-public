package com.javabi.investor.price.candle.io.list.filter;

import java.util.List;

import com.javabi.investor.price.candle.IPriceCandle;

@FunctionalInterface
public interface IPriceCandleListFilter {

	List<IPriceCandle> filter(List<IPriceCandle> candles);

}
