package com.javabi.investor.price.candle.io.stream.sink;

import java.io.File;
import java.io.IOException;

import com.google.common.io.ByteSink;
import com.javabi.common.io.data.DataWriter;
import com.javabi.common.io.data.IDataSerializer;
import com.javabi.common.io.data.IDataWriter;
import com.javabi.common.io.file.FileByteSink;
import com.javabi.investor.price.candle.IPriceCandle;

public class PriceCandleStreamDataSink implements IPriceCandleStreamSink {

	public static PriceCandleStreamDataSink createFileDataSink(File file, IDataSerializer<IPriceCandle> serializer) throws IOException {
		String name = file.getName();
		ByteSink sink = new FileByteSink(file);
		return new PriceCandleStreamDataSink(name, sink, serializer);
	}

	private final String name;
	private final IDataWriter writer;
	private final IDataSerializer<IPriceCandle> serializer;
	private boolean closed = false;

	public PriceCandleStreamDataSink(String name, ByteSink sink, IDataSerializer<IPriceCandle> serializer) throws IOException {
		if (name.isEmpty()) {
			throw new IllegalArgumentException("name is empty");
		}
		if (sink == null) {
			throw new NullPointerException("sink");
		}
		if (serializer == null) {
			throw new NullPointerException("serializer");
		}
		this.name = name;
		this.serializer = serializer;

		// Create the writer
		writer = new DataWriter(sink.openBufferedStream());
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void close() throws IOException {
		closed = true;
		writer.close();
	}

	@Override
	public void putNextCandle(IPriceCandle candle) throws IOException {
		if (candle == null) {
			throw new NullPointerException("candle");
		}
		if (closed) {
			throw new IllegalStateException("Sink is closed: " + name);
		}

		try {
			writer.writeObject(candle, false, serializer);
		} catch (IOException e) {
			throw new IOException("Failed to write next candle to: " + getName() + ", candle: " + candle);
		}

	}

}
