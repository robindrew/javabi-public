package com.javabi.investor.price.candle.io.stream.sink.subscriber;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import com.javabi.investor.IInstrument;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.stream.sink.IPriceCandleStreamSink;

public class PriceCandleSubscriberStreamSink implements IPriceCandleSubscriberStreamSink {

	private final IInstrument instrument;
	private final Set<IPriceCandleStreamSink> subscriberSet = new CopyOnWriteArraySet<>();

	public PriceCandleSubscriberStreamSink(IInstrument instrument) {
		if (instrument == null) {
			throw new NullPointerException("instrument");
		}
		this.instrument = instrument;
	}

	@Override
	public void putNextCandle(IPriceCandle candle) throws IOException {
		for (IPriceCandleStreamSink subscriber : subscriberSet) {
			subscriber.putNextCandle(candle);
		}
	}

	@Override
	public String getName() {
		return "Subscribers" + subscriberSet;
	}

	@Override
	public void close() throws IOException {
		for (IPriceCandleStreamSink subscriber : subscriberSet) {
			subscriber.close();
		}
		subscriberSet.clear();
	}

	@Override
	public IInstrument getInstrument() {
		return instrument;
	}

	@Override
	public boolean subscribe(IPriceCandleStreamSink sink) {
		return subscriberSet.add(sink);
	}

	@Override
	public boolean unsubscribe(IPriceCandleStreamSink sink) {
		return subscriberSet.remove(sink);
	}

}
