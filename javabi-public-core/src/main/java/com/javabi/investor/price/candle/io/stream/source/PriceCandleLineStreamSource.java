package com.javabi.investor.price.candle.io.stream.source;

import java.io.IOException;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.line.source.ILineSource;
import com.javabi.investor.price.candle.line.parser.IPriceCandleLineParser;

public class PriceCandleLineStreamSource implements IPriceCandleStreamSource {

	private final ILineSource lineSource;
	private final IPriceCandleLineParser parser;

	public PriceCandleLineStreamSource(ILineSource lineSource, IPriceCandleLineParser parser) {
		if (lineSource == null) {
			throw new NullPointerException("lineSource");
		}
		if (parser == null) {
			throw new NullPointerException("parser");
		}
		this.lineSource = lineSource;
		this.parser = parser;
	}

	@Override
	public String getName() {
		return lineSource.getName();
	}

	@Override
	public void close() throws IOException {
		lineSource.close();
	}

	@Override
	public IPriceCandle getNextCandle() throws IOException {
		String line = nextLine();

		// No more candles?
		if (line == null) {
			return null;
		}
		return parseCandle(line);
	}

	private IPriceCandle parseCandle(String line) throws IOException {
		try {
			return parser.parseCandle(line);
		} catch (Exception e) {
			throw new IOException("Failed to parse line from: " + lineSource.getName() + ", line: '" + line + "'", e);
		}
	}

	private String nextLine() throws IOException {
		try {
			return lineSource.getNextLine();
		} catch (Exception e) {
			throw new IOException("Failed to read next line from: " + lineSource.getName(), e);
		}
	}

}
