package com.javabi.investor.price.candle.io.stream;

import java.io.IOException;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.stream.sink.IPriceCandleStreamSink;
import com.javabi.investor.price.candle.io.stream.source.IPriceCandleStreamSource;

public class PriceCandleStreamPipe {

	private final IPriceCandleStreamSource source;
	private final IPriceCandleStreamSink sink;

	public PriceCandleStreamPipe(IPriceCandleStreamSource source, IPriceCandleStreamSink sink) {
		if (source == null) {
			throw new NullPointerException("source");
		}
		if (sink == null) {
			throw new NullPointerException("sink");
		}
		this.source = source;
		this.sink = sink;
	}

	public void pipe() throws IOException {
		while (true) {
			IPriceCandle candle = source.getNextCandle();
			if (candle == null) {
				break;
			}
			sink.putNextCandle(candle);
		}
	}

}
