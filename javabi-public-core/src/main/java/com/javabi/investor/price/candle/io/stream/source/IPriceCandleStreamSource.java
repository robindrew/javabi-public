package com.javabi.investor.price.candle.io.stream.source;

import java.io.IOException;

import com.javabi.common.io.INamedCloseable;
import com.javabi.investor.price.candle.IPriceCandle;

public interface IPriceCandleStreamSource extends INamedCloseable {

	IPriceCandle getNextCandle() throws IOException;

}
