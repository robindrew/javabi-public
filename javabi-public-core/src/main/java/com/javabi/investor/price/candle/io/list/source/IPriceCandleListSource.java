package com.javabi.investor.price.candle.io.list.source;

import java.io.IOException;
import java.util.List;

import com.javabi.common.io.INamedCloseable;
import com.javabi.investor.price.candle.IPriceCandle;

public interface IPriceCandleListSource extends INamedCloseable {

	List<IPriceCandle> getNextCandles() throws IOException;

}
