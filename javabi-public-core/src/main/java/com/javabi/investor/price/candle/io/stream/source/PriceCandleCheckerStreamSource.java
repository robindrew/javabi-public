package com.javabi.investor.price.candle.io.stream.source;

import java.io.IOException;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.checker.IPriceCandleChecker;

public class PriceCandleCheckerStreamSource implements IPriceCandleStreamSource {

	private final IPriceCandleStreamSource source;
	private final IPriceCandleChecker checker;
	private IPriceCandle previous = null;

	public PriceCandleCheckerStreamSource(IPriceCandleStreamSource source, IPriceCandleChecker checker) {
		if (source == null) {
			throw new NullPointerException("source");
		}
		if (checker == null) {
			throw new NullPointerException("checker");
		}
		this.source = source;
		this.checker = checker;
	}

	@Override
	public String getName() {
		return source.getName();
	}

	@Override
	public void close() throws IOException {
		source.close();
	}

	@Override
	public IPriceCandle getNextCandle() throws IOException {
		IPriceCandle next = source.getNextCandle();
		if (next == null) {
			return next;
		}

		// Apply the checker if we have a previous candle to check against
		if (previous != null) {
			checker.check(previous, next);
		}

		previous = next;
		return next;
	}

}
