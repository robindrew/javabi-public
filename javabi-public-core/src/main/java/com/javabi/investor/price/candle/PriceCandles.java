package com.javabi.investor.price.candle;

import com.javabi.investor.price.candle.io.stream.source.IPriceCandleStreamSource;
import com.javabi.investor.price.candle.io.stream.source.iterator.PriceCandleStreamSourceIterable;
import com.javabi.investor.price.close.IClosePrice;

public class PriceCandles {

	public static IPriceCandle merge(IPriceCandle candle1, IPriceCandle candle2) {
		if (candle1.getDecimalPlaces() != candle2.getDecimalPlaces()) {
			throw new IllegalArgumentException("Unable to merge candles with different precision");
		}

		long openTime = Math.min(candle1.getOpenTime(), candle2.getOpenTime());
		long closeTime = Math.max(candle1.getCloseTime(), candle2.getCloseTime());

		int openPrice = candle1.getOpenTime() <= candle2.getOpenTime() ? candle1.getOpenPrice() : candle2.getOpenPrice();
		int closePrice = candle1.getCloseTime() >= candle2.getCloseTime() ? candle1.getClosePrice() : candle2.getClosePrice();
		int highPrice = Math.max(candle1.getHighPrice(), candle2.getHighPrice());
		int lowPrice = Math.min(candle1.getLowPrice(), candle2.getLowPrice());

		return new PriceCandle(openPrice, highPrice, lowPrice, closePrice, openTime, closeTime, candle1.getDecimalPlaces());
	}

	public static double getAverage(double total, int count) {
		return (count <= 1) ? total : (total / count);
	}

	public static double getSmoothingConstant(int periods) {
		if (periods < 1) {
			throw new IllegalArgumentException("periods=" + periods);
		}
		return 2.0f / (periods + 1);
	}

	public static int getChange(IClosePrice previous, IClosePrice current) {
		return current.getClosePrice() - previous.getClosePrice();
	}

	public static double getPercentDifference(double price1, double price2) {
		double diff = (price1 > price2) ? price1 / price2 : price2 / price1;
		// Convert to a percentage ...
		return (diff * 100.0) - 100.0;
	}

	public static Iterable<IPriceCandle> iterable(IPriceCandleStreamSource source) {
		return new PriceCandleStreamSourceIterable(source);
	}

}
