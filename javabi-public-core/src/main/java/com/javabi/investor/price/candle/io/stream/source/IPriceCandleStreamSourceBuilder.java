package com.javabi.investor.price.candle.io.stream.source;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;

import com.javabi.investor.IInstrument;
import com.javabi.investor.currency.CurrencyPair;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.checker.IPriceCandleChecker;
import com.javabi.investor.price.candle.filter.IPriceCandleFilter;
import com.javabi.investor.price.candle.interval.IPriceCandleInterval;
import com.javabi.investor.price.candle.io.list.source.IPriceCandleListSourceBuilder;
import com.javabi.investor.price.candle.modifier.IPriceCandleModifier;
import com.javabi.investor.provider.pcf.IPcfTimeWindow;
import com.javabi.investor.provider.pcf.file.IPcfFile;

public interface IPriceCandleStreamSourceBuilder extends Supplier<IPriceCandleStreamSource> {

	IPriceCandleStreamSourceBuilder setBaseSource(IPriceCandleStreamSource source);

	IPriceCandleStreamSourceBuilder setHistDataM1Directory(IInstrument instrument, String directory);

	IPriceCandleStreamSourceBuilder setHistDataTickDirectory(IInstrument instrument, String directory);

	IPriceCandleStreamSourceBuilder setTrueFxTickDirectory(CurrencyPair pair, String directory);

	IPriceCandleStreamSourceBuilder setPcfDirectory(String directory);

	IPriceCandleStreamSourceBuilder setPcfDirectory(String directory, IPcfTimeWindow window);

	IPriceCandleStreamSourceBuilder setPcfFile(String filename) throws IOException;

	IPriceCandleStreamSourceBuilder setPcfFile(IPcfFile file) throws IOException;

	IPriceCandleStreamSourceBuilder setCandles(List<IPriceCandle> candles);

	IPriceCandleStreamSourceBuilder filterBy(IPriceCandleFilter filter);

	IPriceCandleStreamSourceBuilder between(LocalDateTime from, LocalDateTime to);

	IPriceCandleStreamSourceBuilder between(LocalDate from, LocalDate to);

	IPriceCandleStreamSourceBuilder limit(int maxCandles);

	IPriceCandleStreamSourceBuilder modifyWith(IPriceCandleModifier modifier);

	IPriceCandleStreamSourceBuilder multiplyBy(int multiplier);

	IPriceCandleStreamSourceBuilder divideBy(int divisor);

	IPriceCandleStreamSourceBuilder checkWith(IPriceCandleChecker checker);

	IPriceCandleStreamSourceBuilder checkSortedByDate();

	IPriceCandleStreamSourceBuilder checkSortedBy(Comparator<IPriceCandle> comparator);

	IPriceCandleStreamSourceBuilder checkSanity(double maxPercentDiff);

	IPriceCandleStreamSourceBuilder filterConsecutive(int logThreshold);

	IPriceCandleStreamSourceBuilder setInterval(IPriceCandleInterval interval);

	IPriceCandleListSourceBuilder asListSourceBuilder();

	List<IPriceCandle> getList() throws IOException;

}