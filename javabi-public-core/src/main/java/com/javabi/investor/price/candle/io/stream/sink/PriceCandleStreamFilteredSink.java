package com.javabi.investor.price.candle.io.stream.sink;

import java.io.IOException;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.filter.IPriceCandleFilter;

public class PriceCandleStreamFilteredSink implements IPriceCandleStreamSink {

	private final IPriceCandleStreamSink sink;
	private final IPriceCandleFilter filter;

	public PriceCandleStreamFilteredSink(IPriceCandleStreamSink sink, IPriceCandleFilter filter) {
		if (sink == null) {
			throw new NullPointerException("sink");
		}
		if (filter == null) {
			throw new NullPointerException("filter");
		}
		this.sink = sink;
		this.filter = filter;
	}

	@Override
	public String getName() {
		return sink.getName();
	}

	@Override
	public void close() throws IOException {
		sink.close();
	}

	@Override
	public void putNextCandle(IPriceCandle candle) throws IOException {
		if (filter.accept(candle)) {
			sink.putNextCandle(candle);
		}
	}

}
