package com.javabi.investor.price.candle.checker;

import com.javabi.investor.price.candle.IPriceCandle;

public interface IPriceCandleChecker {

	boolean check(IPriceCandle previous, IPriceCandle current);

}
