package com.javabi.investor.price.candle.interval;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.time.temporal.ChronoUnit.MONTHS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.time.temporal.ChronoUnit.YEARS;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Set;

import com.google.common.collect.Sets;
import com.javabi.common.date.Dates;
import com.javabi.investor.price.candle.IPriceCandle;

public class ChronoUnitInterval implements IPriceCandleInterval {

	private static final Set<ChronoUnit> SUPPORTED = Sets.newHashSet(YEARS, MONTHS, DAYS, HOURS, MINUTES, SECONDS);

	private final ChronoUnit unit;

	public ChronoUnitInterval(ChronoUnit unit) {
		if (unit == null) {
			throw new NullPointerException("unit");
		}
		if (!SUPPORTED.contains(unit)) {
			throw new IllegalStateException("Unit not supported: " + unit);
		}
		this.unit = unit;
	}

	public TemporalUnit getUnit() {
		return unit;
	}

	@Override
	public long getTimePeriod(IPriceCandle candle) {
		return Dates.toMillis(getDateTime(candle.getOpenTime()));
	}

	public long getTimePeriod(long timeInMillis) {
		return Dates.toMillis(getDateTime(timeInMillis));
	}

	public LocalDateTime getDateTime(IPriceCandle candle) {
		return getDateTime(candle.getOpenTime());
	}

	private LocalDateTime getDateTime(long timeInMillis) {
		return getDateTime(Dates.toLocalDateTime(timeInMillis));
	}

	private LocalDateTime getDateTime(LocalDateTime date) {
		int year = date.getYear();
		int month = date.getMonthValue();
		int day = date.getDayOfMonth();
		int hour = date.getHour();
		int minute = date.getMinute();
		int second = date.getSecond();

		switch (unit) {
			case YEARS:
				return LocalDateTime.of(year, 1, 1, 0, 0, 0, 0);
			case MONTHS:
				return LocalDateTime.of(year, month, 1, 0, 0, 0, 0);
			case DAYS:
				return LocalDateTime.of(year, month, day, 0, 0, 0, 0);
			case HOURS:
				return LocalDateTime.of(year, month, day, hour, 0, 0, 0);
			case MINUTES:
				return LocalDateTime.of(year, month, day, hour, minute, 0, 0);
			case SECONDS:
				return LocalDateTime.of(year, month, day, hour, minute, second, 0);
			default:
				throw new IllegalStateException("Unit not supported: " + unit);
		}
	}
}
