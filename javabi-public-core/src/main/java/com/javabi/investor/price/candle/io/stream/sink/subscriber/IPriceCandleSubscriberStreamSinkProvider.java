package com.javabi.investor.price.candle.io.stream.sink.subscriber;

import com.javabi.investor.IInstrument;

public interface IPriceCandleSubscriberStreamSinkProvider {

	IPriceCandleSubscriberStreamSink getSubscriber(IInstrument instrument);

}
