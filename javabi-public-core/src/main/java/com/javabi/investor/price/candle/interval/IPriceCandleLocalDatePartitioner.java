package com.javabi.investor.price.candle.interval;

import java.time.LocalDateTime;
import java.util.List;

import com.google.common.collect.ListMultimap;
import com.javabi.investor.price.candle.IPriceCandle;

public interface IPriceCandleLocalDatePartitioner {

	ListMultimap<LocalDateTime, IPriceCandle> partition(List<IPriceCandle> candles);

}
