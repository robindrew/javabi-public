package com.javabi.investor.price.close.calc;

import com.javabi.investor.price.close.IClosePrice;

public interface IClosePriceStreamCalc {

	int getMinPrices();

	int getMaxPrices();

	void next(IClosePrice price, int index);

}
