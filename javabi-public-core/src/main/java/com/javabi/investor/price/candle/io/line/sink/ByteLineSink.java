package com.javabi.investor.price.candle.io.line.sink;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

import com.google.common.io.ByteSink;
import com.javabi.common.io.file.FileByteSink;

public class ByteLineSink implements ILineSink {

	public static ByteLineSink createLineSink(File file, Charset charset) throws IOException {
		String name = file.getName();
		FileByteSink sink = new FileByteSink(file);
		return new ByteLineSink(name, sink, charset);
	}

	private final String name;
	private final BufferedWriter writer;
	private boolean closed = false;

	public ByteLineSink(String name, ByteSink sink, Charset charset) throws IOException {
		if (name.isEmpty()) {
			throw new IllegalArgumentException("name is empty");
		}
		if (sink == null) {
			throw new NullPointerException("sink");
		}
		this.name = name;

		// Create the writer
		this.writer = new BufferedWriter(new OutputStreamWriter(sink.openStream(), charset));
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void close() throws IOException {
		closed = true;
		writer.close();
	}

	@Override
	public void putNextLine(String line) throws IOException {
		if (closed) {
			throw new IllegalStateException("Sink is closed: " + getName());
		}
		writer.write(line);
	}

}
