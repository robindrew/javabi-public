package com.javabi.investor.price.candle.io.line.sink;

import java.io.IOException;

import com.javabi.common.io.INamedCloseable;

public interface ILineSink extends INamedCloseable {

	void putNextLine(String line) throws IOException;

}
