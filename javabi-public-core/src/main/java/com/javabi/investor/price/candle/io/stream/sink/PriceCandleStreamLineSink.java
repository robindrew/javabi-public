package com.javabi.investor.price.candle.io.stream.sink;

import java.io.IOException;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.line.sink.ILineSink;
import com.javabi.investor.price.candle.line.formatter.IPriceCandleLineFormatter;
import com.javabi.investor.price.candle.line.formatter.PriceCandleLineFormatter;

public class PriceCandleStreamLineSink implements IPriceCandleStreamSink {

	private final ILineSink lineSink;
	private final IPriceCandleLineFormatter formatter;

	public PriceCandleStreamLineSink(ILineSink lineSink, IPriceCandleLineFormatter formatter) {
		if (lineSink == null) {
			throw new NullPointerException("lineSink");
		}
		if (formatter == null) {
			throw new NullPointerException("formatter");
		}
		this.lineSink = lineSink;
		this.formatter = formatter;
	}

	public PriceCandleStreamLineSink(ILineSink lineSink) {
		this(lineSink, new PriceCandleLineFormatter());
	}

	@Override
	public String getName() {
		return lineSink.getName();
	}

	@Override
	public void close() throws IOException {
		lineSink.close();
	}

	@Override
	public void putNextCandle(IPriceCandle candle) throws IOException {
		String line = formatter.formatCandle(candle, true);
		try {
			lineSink.putNextLine(line);
		} catch (Exception e) {
			throw new IOException("Failed to write next line to: " + lineSink.getName() + ", line: '" + line + "'", e);
		}
	}

}
