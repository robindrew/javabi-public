package com.javabi.investor.price.candle.io.line.sink;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import com.google.common.base.Charsets;
import com.javabi.common.io.file.FileByteSink;

public class FileLineSink extends ByteLineSink {

	private final File file;

	public FileLineSink(File file, Charset charset) throws IOException {
		super(file.getName(), new FileByteSink(file), charset);
		this.file = file;
	}

	public FileLineSink(File file) throws IOException {
		this(file, Charsets.UTF_8);
	}

	public File getFile() {
		return file;
	}

}
