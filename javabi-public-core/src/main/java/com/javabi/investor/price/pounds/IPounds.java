package com.javabi.investor.price.pounds;

import java.math.BigDecimal;

public interface IPounds {

	BigDecimal get();

	boolean isPositive();

	IPounds multiply(BigDecimal amount);

	IPounds divide(BigDecimal amount);

	IPounds add(BigDecimal amount);

	IPounds remove(BigDecimal amount);

	IPounds multiply(IPounds pounds);

	IPounds divide(IPounds pounds);

	IPounds add(IPounds pounds);

	IPounds remove(IPounds pounds);

	IPounds negate();

}
