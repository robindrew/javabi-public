package com.javabi.investor.price.candle.io;

import java.io.IOException;

import com.javabi.common.io.data.IDataReader;
import com.javabi.common.io.data.IDataSerializer;
import com.javabi.common.io.data.IDataWriter;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.PriceCandle;

/**
 * Non-optimal implementation, but still far more efficient than line parsing/formatting.
 */
public class PriceCandleDataSerializer implements IDataSerializer<IPriceCandle> {

	private final int decimalPlaces;

	public PriceCandleDataSerializer(int decimalPlaces) {
		this.decimalPlaces = decimalPlaces;
	}

	@Override
	public IPriceCandle readObject(IDataReader reader) throws IOException {
		try {

			long openTime = reader.readPositiveLong();
			long closeTime = reader.readPositiveLong();

			int openPrice = reader.readPositiveInt();
			int highPrice = reader.readPositiveInt();
			int lowPrice = reader.readPositiveInt();
			int closePrice = reader.readPositiveInt();

			return new PriceCandle(openPrice, highPrice, lowPrice, closePrice, openTime, closeTime, decimalPlaces);

		} catch (IOException ioe) {

			// Hack to allow for final candle, may need improvement
			if (isEndOfStream(ioe)) {
				return null;
			}
			throw ioe;
		}
	}

	protected boolean isEndOfStream(IOException ioe) {
		return "End of stream".equals(ioe.getMessage());
	}

	@Override
	public void writeObject(IDataWriter writer, IPriceCandle candle) throws IOException {
		if (candle.getDecimalPlaces() != decimalPlaces) {
			throw new IllegalArgumentException("candle=" + candle + " expected decimalPlaces=" + decimalPlaces);
		}

		writer.writePositiveLong(candle.getOpenTime());
		writer.writePositiveLong(candle.getCloseTime());

		writer.writePositiveInt(candle.getOpenPrice());
		writer.writePositiveInt(candle.getHighPrice());
		writer.writePositiveInt(candle.getLowPrice());
		writer.writePositiveInt(candle.getClosePrice());
	}
}
