package com.javabi.investor.price.candle.io.list.source;

import java.util.List;
import java.util.function.Supplier;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.checker.IPriceCandleChecker;
import com.javabi.investor.price.candle.io.list.filter.IPriceCandleListFilter;
import com.javabi.investor.price.candle.io.stream.source.IPriceCandleStreamSource;

public interface IPriceCandleListSourceBuilder extends Supplier<IPriceCandleListSource> {

	IPriceCandleListSourceBuilder setBaseSource(IPriceCandleListSource source);

	IPriceCandleListSourceBuilder setStreamSource(IPriceCandleStreamSource source);

	IPriceCandleListSourceBuilder filterBy(IPriceCandleListFilter filter);

	IPriceCandleListSourceBuilder checkWith(IPriceCandleChecker checker);

	IPriceCandleListSourceBuilder setCandles(List<IPriceCandle> candles);

}
