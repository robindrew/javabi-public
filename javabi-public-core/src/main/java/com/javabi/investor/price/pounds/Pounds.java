package com.javabi.investor.price.pounds;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Pounds implements IPounds {

	private static final DecimalFormat FORMAT = new DecimalFormat("0.00");

	private final BigDecimal pounds;
	private final boolean positiveOnly;

	public Pounds(BigDecimal pounds, boolean positiveOnly) {
		this.pounds = pounds;
		this.positiveOnly = positiveOnly;

		if (positiveOnly && !isPositive()) {
			throw new IllegalArgumentException("pounds is negative: " + pounds);
		}
	}

	public Pounds(IPounds pounds, boolean positiveOnly) {
		this(pounds.get(), positiveOnly);
	}

	@Override
	public String toString() {
		return FORMAT.format(pounds);
	}

	@Override
	public BigDecimal get() {
		return pounds;
	}

	@Override
	public boolean isPositive() {
		return pounds.doubleValue() >= 0.0;
	}

	@Override
	public IPounds multiply(BigDecimal amount) {
		if (amount.doubleValue() <= 0.0) {
			throw new IllegalArgumentException("amount is negative: " + amount);
		}
		return new Pounds(pounds.multiply(amount), positiveOnly);
	}

	@Override
	public IPounds divide(BigDecimal amount) {
		if (amount.doubleValue() <= 0.0) {
			throw new IllegalArgumentException("amount is negative: " + amount);
		}
		return new Pounds(pounds.divide(amount), positiveOnly);
	}

	@Override
	public IPounds add(BigDecimal amount) {
		if (amount.doubleValue() < 0.0) {
			throw new IllegalArgumentException("amount is negative: " + amount);
		}
		return new Pounds(pounds.add(amount), positiveOnly);
	}

	@Override
	public IPounds remove(BigDecimal amount) {
		if (amount.doubleValue() < 0.0) {
			throw new IllegalArgumentException("amount is negative: " + amount);
		}
		return new Pounds(pounds.subtract(amount), positiveOnly);
	}

	@Override
	public IPounds multiply(IPounds pounds) {
		return multiply(pounds.get());
	}

	@Override
	public IPounds divide(IPounds pounds) {
		return divide(pounds.get());
	}

	@Override
	public IPounds add(IPounds pounds) {
		return add(pounds.get());
	}

	@Override
	public IPounds remove(IPounds pounds) {
		return remove(pounds.get());
	}

	@Override
	public IPounds negate() {
		return new Pounds(pounds.negate(), positiveOnly);
	}

}
