package com.javabi.investor.price.candle.charts;

import java.io.IOException;

import com.google.common.io.ByteSink;
import com.javabi.common.image.ImageFormat;

public interface IPriceCandleChart {

	void writeTo(ByteSink sink, ImageFormat format) throws IOException;

}
