package com.javabi.investor.price.candle.interval;

import static java.util.concurrent.TimeUnit.MINUTES;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.investor.price.candle.checker.PriceCandleSanityChecker;
import com.javabi.investor.price.candle.io.line.sink.FileLineSink;
import com.javabi.investor.price.candle.io.stream.PriceCandleStreamPipe;
import com.javabi.investor.price.candle.io.stream.sink.IPriceCandleStreamSink;
import com.javabi.investor.price.candle.io.stream.sink.PriceCandleStreamLineSink;
import com.javabi.investor.price.candle.io.stream.source.IPriceCandleStreamSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleDirectoryStreamSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleCheckerStreamSource;
import com.javabi.investor.price.candle.io.stream.source.PriceCandleIntervalStreamSource;
import com.javabi.investor.price.candle.line.filter.ILineFilter;
import com.javabi.investor.price.candle.line.parser.IPriceCandleLineParser;

public class PriceCandleIntervalConverter {

	private static final Logger log = LoggerFactory.getLogger(PriceCandleIntervalConverter.class);

	private final IPriceCandleLineParser parser;
	private final ILineFilter filter;
	private IPriceCandleInterval interval = new TimeUnitInterval(1, MINUTES);

	public PriceCandleIntervalConverter(IPriceCandleLineParser parser, ILineFilter filter) {
		if (parser == null) {
			throw new NullPointerException("parser");
		}
		if (filter == null) {
			throw new NullPointerException("filter");
		}
		this.parser = parser;
		this.filter = filter;
	}

	public boolean convert(File sourceDirectory, File destinationFile, double maxPercentDiff) throws IOException {
		if (destinationFile.exists()) {
			log.warn("File already exists: " + destinationFile);
			return false;
		}

		// Convert!
		try (IPriceCandleStreamSource files = new PriceCandleDirectoryStreamSource(sourceDirectory, parser, filter)) {
			try (IPriceCandleStreamSource intervalSource = new PriceCandleIntervalStreamSource(files, interval)) {
				try (IPriceCandleStreamSource checkerSource = new PriceCandleCheckerStreamSource(intervalSource, new PriceCandleSanityChecker(maxPercentDiff))) {
					try (IPriceCandleStreamSink sink = new PriceCandleStreamLineSink(new FileLineSink(destinationFile))) {
						new PriceCandleStreamPipe(checkerSource, sink).pipe();
						return true;
					}
				}
			}
		}

		// Attempt to clean away partially written files
		catch (Exception e) {
			if (destinationFile.exists()) {
				destinationFile.delete();
			}
			throw Throwables.propagate(e);
		}
	}
}
