package com.javabi.investor.price.candle.io.list;

import java.io.IOException;
import java.util.List;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.list.sink.IPriceCandleListSink;
import com.javabi.investor.price.candle.io.list.source.IPriceCandleListSource;

public class PriceCandleListPipe {

	private final IPriceCandleListSource source;
	private final IPriceCandleListSink sink;

	public PriceCandleListPipe(IPriceCandleListSource source, IPriceCandleListSink sink) {
		if (source == null) {
			throw new NullPointerException("source");
		}
		if (sink == null) {
			throw new NullPointerException("sink");
		}
		this.source = source;
		this.sink = sink;
	}

	public void pipe() throws IOException {
		while (true) {
			List<IPriceCandle> candles = source.getNextCandles();
			if (candles.isEmpty()) {
				break;
			}
			sink.putNextCandles(candles);
		}
	}

}
