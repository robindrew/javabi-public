package com.javabi.investor.price.candle.io.stream.source;

import java.io.File;
import java.io.IOException;

import com.google.common.io.ByteSource;
import com.javabi.common.io.data.DataReader;
import com.javabi.common.io.data.IDataReader;
import com.javabi.common.io.data.IDataSerializer;
import com.javabi.common.io.file.FileByteSource;
import com.javabi.investor.price.candle.IPriceCandle;

public class PriceCandleDataStreamSource implements IPriceCandleStreamSource {

	public static PriceCandleDataStreamSource createLineSource(File file, IDataSerializer<IPriceCandle> serializer) throws IOException {
		String name = file.getName();
		ByteSource source = new FileByteSource(file);
		return new PriceCandleDataStreamSource(name, source, serializer);
	}

	private final String name;
	private final IDataReader reader;
	private final IDataSerializer<IPriceCandle> serializer;
	private boolean closed = false;

	public PriceCandleDataStreamSource(String name, ByteSource source, IDataSerializer<IPriceCandle> serializer) throws IOException {
		if (name.isEmpty()) {
			throw new IllegalArgumentException("name is empty");
		}
		if (source == null) {
			throw new NullPointerException("source");
		}
		if (serializer == null) {
			throw new NullPointerException("serializer");
		}
		this.name = name;
		this.serializer = serializer;

		// Create the reader
		this.reader = new DataReader(source.openBufferedStream());
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void close() throws IOException {
		closed = true;
		reader.close();
	}

	@Override
	public IPriceCandle getNextCandle() throws IOException {
		try {

			if (closed) {
				throw new IllegalStateException("Source is closed: " + name);
			}

			IPriceCandle candle = reader.readObject(false, serializer);

			// No more candles?
			if (candle == null) {
				return null;
			}

			return candle;

		} catch (IOException e) {
			throw new IOException("Failed to read next candle from: " + getName());
		}

	}

}
