package com.javabi.investor.price.candle.io.line.source;

import java.io.IOException;

import com.javabi.common.io.INamedCloseable;

public interface ILineSource extends INamedCloseable {

	String getNextLine() throws IOException;

}
