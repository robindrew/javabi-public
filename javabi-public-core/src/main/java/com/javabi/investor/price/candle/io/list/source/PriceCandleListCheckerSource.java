package com.javabi.investor.price.candle.io.list.source;

import java.io.IOException;
import java.util.List;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.checker.IPriceCandleChecker;

public class PriceCandleListCheckerSource implements IPriceCandleListSource {

	private final IPriceCandleListSource source;
	private final IPriceCandleChecker checker;

	public PriceCandleListCheckerSource(IPriceCandleListSource source, IPriceCandleChecker checker) {
		if (source == null) {
			throw new NullPointerException("source");
		}
		if (checker == null) {
			throw new NullPointerException("checker");
		}
		this.source = source;
		this.checker = checker;
	}

	@Override
	public String getName() {
		return source.getName();
	}

	@Override
	public void close() throws IOException {
		source.close();
	}

	@Override
	public List<IPriceCandle> getNextCandles() throws IOException {
		List<IPriceCandle> candles = source.getNextCandles();

		IPriceCandle previous = null;
		for (IPriceCandle next : candles) {
			if (previous != null) {
				checker.check(previous, next);
			}
			previous = next;
		}
		return candles;
	}

}
