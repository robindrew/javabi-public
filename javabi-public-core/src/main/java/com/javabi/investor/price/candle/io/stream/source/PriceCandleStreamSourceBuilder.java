package com.javabi.investor.price.candle.io.stream.source;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.javabi.investor.IInstrument;
import com.javabi.investor.currency.CurrencyPair;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.PriceCandleDateComparator;
import com.javabi.investor.price.candle.checker.IPriceCandleChecker;
import com.javabi.investor.price.candle.checker.PriceCandleSanityChecker;
import com.javabi.investor.price.candle.checker.PriceCandleSortedChecker;
import com.javabi.investor.price.candle.filter.IPriceCandleFilter;
import com.javabi.investor.price.candle.filter.PriceCandleConsecutiveFilter;
import com.javabi.investor.price.candle.filter.PriceCandleCountFilter;
import com.javabi.investor.price.candle.filter.PriceCandleDateFilter;
import com.javabi.investor.price.candle.filter.PriceCandleDateTimeFilter;
import com.javabi.investor.price.candle.interval.IPriceCandleInterval;
import com.javabi.investor.price.candle.io.list.source.IPriceCandleListSourceBuilder;
import com.javabi.investor.price.candle.io.list.source.PriceCandleListSourceBuilder;
import com.javabi.investor.price.candle.line.filter.ILineFilter;
import com.javabi.investor.price.candle.line.parser.IPriceCandleLineParser;
import com.javabi.investor.price.candle.modifier.IPriceCandleModifier;
import com.javabi.investor.price.candle.modifier.PriceCandleDivideModifier;
import com.javabi.investor.price.candle.modifier.PriceCandleMultiplyModifier;
import com.javabi.investor.provider.histdata.line.HistDataLineFilter;
import com.javabi.investor.provider.histdata.line.HistDataM1LineParser;
import com.javabi.investor.provider.histdata.line.HistDataTickLineParser;
import com.javabi.investor.provider.pcf.IPcfTimeWindow;
import com.javabi.investor.provider.pcf.PcfDirectoryStreamSource;
import com.javabi.investor.provider.pcf.PcfTimeWindow;
import com.javabi.investor.provider.pcf.file.IPcfFile;
import com.javabi.investor.provider.pcf.file.PcfFile;
import com.javabi.investor.provider.truefx.line.TrueFxLineFilter;
import com.javabi.investor.provider.truefx.line.TrueFxTickLineParser;

public class PriceCandleStreamSourceBuilder implements IPriceCandleStreamSourceBuilder {

	private IPriceCandleStreamSource base;
	private IPriceCandleInterval candleInterval;
	private final Set<IPriceCandleFilter> filterSet = new LinkedHashSet<>();
	private final Set<IPriceCandleChecker> checkerSet = new LinkedHashSet<>();
	private final Set<IPriceCandleModifier> modifierSet = new LinkedHashSet<>();

	@Override
	public IPriceCandleStreamSourceBuilder setBaseSource(IPriceCandleStreamSource source) {
		if (source == null) {
			throw new NullPointerException("source");
		}
		if (base != null) {
			throw new IllegalStateException("Base source already initialised");
		}
		base = source;
		return this;
	}

	@Override
	public IPriceCandleStreamSourceBuilder setPcfFile(String filename) throws IOException {
		File file = new File(filename);
		IPcfFile pcf = new PcfFile(file);
		return setPcfFile(pcf);
	}

	@Override
	public IPriceCandleStreamSourceBuilder setPcfFile(IPcfFile file) throws IOException {
		List<IPriceCandle> candles = file.read();
		return setBaseSource(new PriceCandleListBackedStreamSource(candles));
	}

	@Override
	public IPriceCandleStreamSourceBuilder setHistDataM1Directory(IInstrument instrument, String directory) {
		IPriceCandleLineParser parser = new HistDataM1LineParser(instrument);
		ILineFilter filter = new HistDataLineFilter();
		return setBaseSource(new PriceCandleDirectoryStreamSource(new File(directory), parser, filter));
	}

	@Override
	public IPriceCandleStreamSourceBuilder setHistDataTickDirectory(IInstrument instrument, String directory) {
		IPriceCandleLineParser parser = new HistDataTickLineParser(instrument);
		ILineFilter filter = new HistDataLineFilter();
		return setBaseSource(new PriceCandleDirectoryStreamSource(new File(directory), parser, filter));
	}

	@Override
	public IPriceCandleStreamSourceBuilder setTrueFxTickDirectory(CurrencyPair pair, String directory) {
		IPriceCandleLineParser parser = new TrueFxTickLineParser(pair);
		ILineFilter filter = new TrueFxLineFilter();
		return setBaseSource(new PriceCandleDirectoryStreamSource(new File(directory), parser, filter));
	}

	@Override
	public IPriceCandleStreamSourceBuilder setPcfDirectory(String directory) {
		return setPcfDirectory(directory, new PcfTimeWindow());
	}

	@Override
	public IPriceCandleStreamSourceBuilder setPcfDirectory(String directory, IPcfTimeWindow timeWindow) {
		return setBaseSource(new PcfDirectoryStreamSource(new File(directory), timeWindow));
	}

	@Override
	public IPriceCandleStreamSourceBuilder setCandles(List<IPriceCandle> candles) {
		return setBaseSource(new PriceCandleListBackedStreamSource(candles));
	}

	@Override
	public IPriceCandleStreamSourceBuilder checkSortedByDate() {
		return checkSortedBy(new PriceCandleDateComparator());
	}

	@Override
	public IPriceCandleStreamSourceBuilder filterBy(IPriceCandleFilter filter) {
		if (filter == null) {
			throw new NullPointerException("filter");
		}
		filterSet.add(filter);
		return this;
	}

	@Override
	public IPriceCandleStreamSourceBuilder limit(int maxCandles) {
		IPriceCandleFilter filter = new PriceCandleCountFilter(maxCandles);
		return filterBy(filter);
	}

	@Override
	public IPriceCandleStreamSourceBuilder between(LocalDateTime from, LocalDateTime to) {
		IPriceCandleFilter filter = new PriceCandleDateTimeFilter(from, to);
		return filterBy(filter);
	}

	@Override
	public IPriceCandleStreamSourceBuilder between(LocalDate from, LocalDate to) {
		IPriceCandleFilter filter = new PriceCandleDateFilter(from, to);
		return filterBy(filter);
	}

	@Override
	public IPriceCandleStreamSourceBuilder modifyWith(IPriceCandleModifier modifier) {
		if (modifier == null) {
			throw new NullPointerException("modifier");
		}
		modifierSet.add(modifier);
		return this;
	}

	@Override
	public IPriceCandleStreamSourceBuilder checkWith(IPriceCandleChecker checker) {
		if (checker == null) {
			throw new NullPointerException("checker");
		}
		checkerSet.add(checker);
		return this;
	}

	@Override
	public IPriceCandleStreamSourceBuilder checkSortedBy(Comparator<IPriceCandle> comparator) {
		return checkWith(new PriceCandleSortedChecker(comparator));
	}

	@Override
	public IPriceCandleStreamSourceBuilder checkSanity(double maxPercentDiff) {
		return checkWith(new PriceCandleSanityChecker(maxPercentDiff));
	}

	@Override
	public IPriceCandleStreamSourceBuilder filterConsecutive(int logThreshold) {
		return filterBy(new PriceCandleConsecutiveFilter(logThreshold));
	}

	@Override
	public IPriceCandleStreamSourceBuilder setInterval(IPriceCandleInterval interval) {
		if (interval == null) {
			throw new NullPointerException("interval");
		}
		candleInterval = interval;
		return this;
	}

	@Override
	public IPriceCandleStreamSourceBuilder multiplyBy(int multiplier) {
		return modifyWith(new PriceCandleMultiplyModifier(multiplier));
	}

	@Override
	public IPriceCandleStreamSourceBuilder divideBy(int divisor) {
		return modifyWith(new PriceCandleDivideModifier(divisor));
	}

	private IPriceCandleStreamSource getBaseSource() {
		if (base == null) {
			throw new IllegalStateException("No base source initialised (directory, file, etc)");
		}
		return base;
	}

	@Override
	public IPriceCandleStreamSource get() {
		IPriceCandleStreamSource source = getBaseSource();

		// Aggregate by inverval?
		if (candleInterval != null) {
			source = new PriceCandleIntervalStreamSource(source, candleInterval);
		}

		// Apply filters
		for (IPriceCandleFilter filter : filterSet) {
			source = new PriceCandleFilteredStreamSource(source, filter);
		}

		// Apply modifiers
		for (IPriceCandleModifier modifier : modifierSet) {
			source = new PriceCandleModifierStreamSource(source, modifier);
		}

		// Apply checkers
		for (IPriceCandleChecker checker : checkerSet) {
			source = new PriceCandleCheckerStreamSource(source, checker);
		}

		return source;
	}

	@Override
	public IPriceCandleListSourceBuilder asListSourceBuilder() {
		return new PriceCandleListSourceBuilder().setStreamSource(get());
	}

	@Override
	public List<IPriceCandle> getList() throws IOException {
		try (PriceCandleStreamToListSource source = new PriceCandleStreamToListSource(get())) {
			return source.getNextCandles();
		}
	}

}
