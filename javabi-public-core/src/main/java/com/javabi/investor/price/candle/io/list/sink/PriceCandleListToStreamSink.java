package com.javabi.investor.price.candle.io.list.sink;

import java.io.IOException;
import java.util.List;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.stream.sink.IPriceCandleStreamSink;

public class PriceCandleListToStreamSink implements IPriceCandleListSink {

	private final IPriceCandleStreamSink sink;

	public PriceCandleListToStreamSink(IPriceCandleStreamSink sink) {
		if (sink == null) {
			throw new NullPointerException("sink");
		}
		this.sink = sink;
	}

	@Override
	public String getName() {
		return sink.getName();
	}

	@Override
	public void close() throws IOException {
		sink.close();
	}

	@Override
	public void putNextCandles(List<IPriceCandle> candles) throws IOException {
		for (IPriceCandle candle : candles) {
			sink.putNextCandle(candle);
		}
	}

}
