package com.javabi.template;

import java.util.Map;

import com.javabi.template.table.IDataTable;

public interface ITemplateData {

	Object get(String key);

	void set(String key, Object value);

	void set(String key, Object value, Object defaultValue);

	boolean contains(String key);

	void remove(String key);

	Map<String, Object> toMap();

	IDataTable table(String name);

}
