package com.javabi.template.velocity.map;

import java.nio.charset.Charset;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.log.LogChute;

import com.google.common.base.Charsets;
import com.google.common.base.Supplier;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.velocity.Slf4jLogChute;
import com.javabi.template.velocity.VelocityTemplateLocator;

public class VelocityMapTemplateLocatorSupplier implements Supplier<ITemplateLocator> {

	private Charset inputEncoding = Charsets.UTF_8;
	private Charset outputEncoding = Charsets.UTF_8;
	private String extension = "template";
	private IVelocityMap map = new VelocityMap();
	private LogChute logChute = new Slf4jLogChute();
	private boolean strict = false;

	public void setMap(IVelocityMap map) {
		if (map == null) {
			throw new NullPointerException("map");
		}
		this.map = map;
	}

	public IVelocityMap getMap() {
		return map;
	}

	public String getExtension() {
		return extension;
	}

	public Charset getInputEncoding() {
		return inputEncoding;
	}

	public Charset getOutputEncoding() {
		return outputEncoding;
	}

	public LogChute getLogChute() {
		return logChute;
	}

	public VelocityMapTemplateLocatorSupplier setLogChute(LogChute logChute) {
		if (logChute == null) {
			throw new NullPointerException("logChute");
		}
		this.logChute = logChute;
		return this;
	}

	public VelocityMapTemplateLocatorSupplier setExtension(String extension) {
		if (extension.isEmpty()) {
			throw new IllegalArgumentException("extension is empty");
		}
		this.extension = extension;
		return this;
	}

	public VelocityMapTemplateLocatorSupplier setInputEncoding(Charset charset) {
		if (charset == null) {
			throw new NullPointerException("charset");
		}
		inputEncoding = charset;
		return this;
	}

	public VelocityMapTemplateLocatorSupplier setOutputEncoding(Charset charset) {
		if (charset == null) {
			throw new NullPointerException("charset");
		}
		outputEncoding = charset;
		return this;
	}

	@Override
	public ITemplateLocator get() {
		VelocityEngine engine = new VelocityEngine();
		engine.setProperty(RuntimeConstants.INPUT_ENCODING, inputEncoding.name());
		engine.setProperty(RuntimeConstants.OUTPUT_ENCODING, outputEncoding.name());
		engine.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM, getLogChute());
		engine.setProperty(RuntimeConstants.RESOURCE_LOADER, "map");
		engine.setProperty("map.resource.loader.class", VelocityMapResourceLoader.class.getName());
		engine.setProperty("map.resource.loader.map", map);
		if (strict) {
			engine.setProperty("runtime.references.strict", strict);
		}
		engine.init();
		return new VelocityTemplateLocator(engine);
	}

	// public static void main(String[] args) {
	// IVelocityMap map = new VelocityMap();
	// map.put("fred", "jim");
	//
	// VelocityMapTemplateLocatorSupplier supplier = new VelocityMapTemplateLocatorSupplier();
	// supplier.setMap(map);
	// ITemplateLocator locator = supplier.get();
	//
	// ITemplate template = locator.getTemplate("fred");
	// String text = template.execute(new HashMap<String, String>());
	// System.out.println(text);
	// }

}
