package com.javabi.template.velocity;

import java.io.InputStream;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.Resource;
import org.apache.velocity.runtime.resource.loader.ResourceLoader;

import com.javabi.common.lang.IResource;

public class VelocityResourceLoader extends ResourceLoader {

	private volatile String path = "";
	private volatile String extension = "template";
	private volatile boolean caching = false;

	public boolean isCaching() {
		return caching;
	}

	@Override
	public long getLastModified(Resource resource) {
		return 0;
	}

	@Override
	public boolean isSourceModified(Resource resource) {
		return false;
	}

	@Override
	public void init(ExtendedProperties properties) {
		path = properties.getString("path");
		extension = properties.getString("extension");
		caching = properties.getBoolean("caching");
	}

	@Override
	public InputStream getResourceStream(String name) throws ResourceNotFoundException {
		if (path != null && !name.startsWith(path)) {
			name = path + "/" + name;
		}
		if (extension != null && !name.endsWith(extension)) {
			name = name + "." + extension;
		}
		IResource resource = new com.javabi.common.lang.Resource(name);
		if (!resource.exists()) {
			return null;
		}
		return resource.newInputStream();
	}

}
