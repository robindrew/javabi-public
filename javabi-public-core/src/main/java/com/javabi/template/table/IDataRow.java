package com.javabi.template.table;

import java.util.Map;

public interface IDataRow extends Map<String, Object> {

	int columns();

	void set(String key, Object value);

}
