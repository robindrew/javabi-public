package com.javabi.template.table;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DataRow implements IDataRow {

	private final Map<String, Object> map = new HashMap<String, Object>();

	@Override
	public int columns() {
		return map.size();
	}

	@Override
	public String toString() {
		return map.toString();
	}

	@Override
	public void set(String key, Object value) {
		put(key, value);
	}

	@Override
	public Object put(String key, Object value) {
		return map.put(key, value);
	}

	@Override
	public Object get(Object key) {
		return map.get(key.toString());
	}

	@Override
	public int size() {
		return columns();
	}

	@Override
	public boolean isEmpty() {
		return size() > 0;
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return map.containsValue(value);
	}

	@Override
	public String remove(Object key) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Set<String> keySet() {
		return map.keySet();
	}

	@Override
	public Collection<Object> values() {
		return map.values();
	}

	@Override
	public Set<java.util.Map.Entry<String, Object>> entrySet() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> map) {
		this.map.putAll(map);
	}

}
