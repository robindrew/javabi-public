package com.javabi.template.table;

import java.util.List;

public interface IDataTable extends List<IDataRow> {

	List<IDataRow> getRows();

	int rows();

	IDataRow row();

	@Override
	IDataRow get(int rowIndex);

	Object get(int rowIndex, String columnKey);

}
