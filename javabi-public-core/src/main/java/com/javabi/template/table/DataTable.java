package com.javabi.template.table;

import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class DataTable implements IDataTable {

	private final List<IDataRow> rows = new ArrayList<IDataRow>();

	@Override
	public int rows() {
		return rows.size();
	}

	@Override
	public IDataRow row() {
		IDataRow row = new DataRow();
		rows.add(row);
		return row;
	}

	@Override
	public IDataRow get(int index) {
		return rows.get(index);
	}

	@Override
	public Object get(int rowIndex, String columnKey) {
		IDataRow row = get(rowIndex);
		return row.get(columnKey);
	}

	@Override
	public List<IDataRow> getRows() {
		return unmodifiableList(rows);
	}

	@Override
	public int size() {
		return rows();
	}

	@Override
	public boolean isEmpty() {
		return rows() == 0;
	}

	@Override
	public void clear() {
		rows.clear();
	}

	@Override
	public Iterator<IDataRow> iterator() {
		return getRows().iterator();
	}

	@Override
	public Object[] toArray() {
		return getRows().toArray();
	}

	@Override
	public <T> T[] toArray(T[] array) {
		return getRows().toArray(array);
	}

	@Override
	public boolean contains(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean add(IDataRow e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(Collection<? extends IDataRow> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(int index, Collection<? extends IDataRow> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IDataRow set(int index, IDataRow element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void add(int index, IDataRow element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IDataRow remove(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int indexOf(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int lastIndexOf(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<IDataRow> listIterator() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<IDataRow> listIterator(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<IDataRow> subList(int fromIndex, int toIndex) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString() {
		return rows.toString();
	}

	public void sortRows(Comparator<IDataRow> comparator) {
		Collections.sort(rows, comparator);
	}

}
