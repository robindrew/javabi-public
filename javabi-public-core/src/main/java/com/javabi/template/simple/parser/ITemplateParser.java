package com.javabi.template.simple.parser;

import com.javabi.template.simple.element.IRootElement;

public interface ITemplateParser {

	IRootElement parse(String text);

}
