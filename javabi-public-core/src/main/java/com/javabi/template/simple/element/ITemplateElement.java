package com.javabi.template.simple.element;

import com.javabi.common.io.writer.IWriter;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;

public interface ITemplateElement {

	String getText();

	void process(ITemplateLocator locator, ITemplateData data, IWriter writer);

}
