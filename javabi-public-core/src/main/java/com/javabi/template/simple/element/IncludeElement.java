package com.javabi.template.simple.element;

import java.util.regex.Pattern;

import com.javabi.common.io.writer.IWriter;
import com.javabi.template.ITemplate;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;

public class IncludeElement extends TemplateElement implements ITemplateElement {

	private static final Pattern PATTERN = Pattern.compile("include " + FILENAME);

	public static boolean matches(String content) {
		return PATTERN.matcher(content).matches();
	}

	private final String templateName;

	public IncludeElement(String text, String content) {
		super(text);
		int index = content.indexOf(' ');
		this.templateName = content.substring(index + 1);
	}

	@Override
	public void process(ITemplateLocator locator, ITemplateData data, IWriter writer) {
		ITemplate template = locator.getTemplate(templateName);
		template.execute(data, writer);
	}

}
