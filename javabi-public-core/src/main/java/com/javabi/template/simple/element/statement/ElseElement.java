package com.javabi.template.simple.element.statement;

import com.javabi.common.io.writer.IWriter;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.simple.element.TemplateElement;

public class ElseElement extends TemplateElement implements IElseElement {

	public static boolean matches(String content) {
		return "else".equals(content);
	}

	public ElseElement(String text) {
		super(text);
	}

	@Override
	public void process(ITemplateLocator locator, ITemplateData data, IWriter writer) {
		throw new IllegalStateException("else does not process, and must be in an if statement");
	}

}
