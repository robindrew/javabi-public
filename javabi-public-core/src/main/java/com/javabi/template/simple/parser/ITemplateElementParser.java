package com.javabi.template.simple.parser;

import com.javabi.template.simple.element.ITemplateElement;

public interface ITemplateElementParser {

	ITemplateElement parse(String text, String content);

}
