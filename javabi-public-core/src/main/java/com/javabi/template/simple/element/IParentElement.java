package com.javabi.template.simple.element;

public interface IParentElement extends ITemplateElement {

	void addChild(ITemplateElement element);

	boolean isEmpty();

}
