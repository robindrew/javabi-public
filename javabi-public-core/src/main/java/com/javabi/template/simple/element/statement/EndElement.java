package com.javabi.template.simple.element.statement;

import com.javabi.common.io.writer.IWriter;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.simple.element.TemplateElement;

public class EndElement extends TemplateElement implements IEndElement {

	public static boolean matches(String content) {
		return "end".equals(content);
	}

	public EndElement(String text) {
		super(text);
	}

	@Override
	public void process(ITemplateLocator locator, ITemplateData data, IWriter writer) {
		throw new UnsupportedOperationException();
	}

}
