package com.javabi.template.simple.element;

import com.javabi.common.io.writer.IWriter;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;

public class TextElement extends TemplateElement implements ITemplateElement {

	private String[] parts;

	public TextElement(String text, String[] parts) {
		super(text);
		this.parts = parts;
	}

	@Override
	public void process(ITemplateLocator locator, ITemplateData data, IWriter writer) {
		for (String part : parts) {
			writer.append(part);
		}
	}

}
