package com.javabi.template.simple.variable;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

import com.google.common.base.Objects;
import com.javabi.template.TemplateException;

public class TemplateVariable implements ITemplateVariable {

	private Object getValue(Object variable) {
		Object value = ((TemplateVariable) variable).value;
		if (value instanceof TemplateVariable) {
			value = getValue(value);
		}
		return value;
	}

	private final Object value;

	public TemplateVariable(Object value) {
		if (value == null) {
			throw new NullPointerException();
		}
		if (value instanceof TemplateVariable) {
			value = getValue(value);
		}
		this.value = value;
	}

	@Override
	public Object get() {
		return value;
	}

	@Override
	public Class<?> getType() {
		return value.getClass();
	}

	@Override
	public long size() {
		if (isCollection()) {
			return ((Collection<?>) value).size();
		}
		if (isMap()) {
			return ((Map<?, ?>) value).size();
		}
		if (isArray()) {
			return Array.getLength(value);
		}

		Long number = toLong();
		if (number != null) {
			return number.intValue();
		}
		throw new TemplateException("Unable to evaluate size from type: " + value.getClass() + ", value=" + value);
	}

	@Override
	public boolean isBoolean() {
		return value instanceof Boolean;
	}

	@Override
	public boolean isLong() {
		return value instanceof Long || value instanceof Integer || value instanceof Short || value instanceof Byte;
	}

	@Override
	public boolean isDouble() {
		return value instanceof Double || value instanceof Float;
	}

	@Override
	public boolean isString() {
		return value instanceof Number;
	}

	@Override
	public boolean isCollection() {
		return value instanceof Collection;
	}

	@Override
	public boolean isMap() {
		return value instanceof Map;
	}

	@Override
	public boolean isArray() {
		return value.getClass().isArray();
	}

	@Override
	public String toString() {
		return value.toString();
	}

	@Override
	public Boolean toBoolean() {
		if (value instanceof Boolean) {
			return (Boolean) value;
		}
		if (value instanceof Number) {
			return ((Number) value).longValue() != 0;
		}
		if (value instanceof String) {
			String string = (String) value;
			if (string.equalsIgnoreCase("true")) {
				return true;
			}
			if (string.equalsIgnoreCase("false")) {
				return false;
			}
		}
		return null;
	}

	@Override
	public Long toLong() {
		if (value instanceof Long) {
			return (Long) value;
		}
		if (value instanceof Integer) {
			return new Long((Integer) value);
		}
		if (value instanceof Short) {
			return new Long((Short) value);
		}
		if (value instanceof Byte) {
			return new Long((Byte) value);
		}

		// Text
		if (value instanceof String) {
			String string = (String) value;
			try {
				return Long.parseLong(string);
			} catch (Exception e) {
			}
		}

		return null;
	}

	@Override
	public Double toDouble() {
		if (value instanceof Double) {
			return (Double) value;
		}
		if (value instanceof Float) {
			return new Double((Float) value);
		}

		// Text
		if (value instanceof String) {
			String string = (String) value;
			try {
				return Double.parseDouble(string);
			} catch (Exception e) {
			}
		}

		return null;
	}

	@Override
	public boolean equals(Object value) {
		if (!(value instanceof TemplateVariable)) {
			value = new TemplateVariable(value);
		}
		return equals(this, (TemplateVariable) value);
	}

	private boolean equals(TemplateVariable value1, TemplateVariable value2) {

		// Same type equality
		if (value1.getType().equals(value2.getType())) {
			return value1.get().equals(value2.get());
		}

		// Boolean equality
		if (value1.isBoolean() || value2.isBoolean()) {
			return Objects.equal(value1.toBoolean(), value2.toBoolean());
		}

		// Numeric equality
		if (value1.isLong() || value2.isLong()) {
			return Objects.equal(value1.toLong(), value2.toLong());
		}
		if (value1.isDouble() || value2.isDouble()) {
			return Objects.equal(value1.toDouble(), value2.toDouble());
		}

		// String equality
		return value1.toString().equals(value2.toString());
	}

}
