package com.javabi.template.simple.locator;

import java.util.HashMap;
import java.util.Map;

import com.javabi.template.ITemplate;
import com.javabi.template.ITemplateLocator;

public class SimpleLocatorMap implements ITemplateLocator {

	private final Map<String, ITemplate> nameToTemplateMap = new HashMap<String, ITemplate>();

	public void setTemplate(ITemplate template) {
		nameToTemplateMap.put(template.getName(), template);
	}

	@Override
	public ITemplate getTemplate(String name) {
		ITemplate template = nameToTemplateMap.get(name);
		if (template == null) {
			throw new IllegalArgumentException("Template not found: '" + name + "'");
		}
		return template;
	}

}
