package com.javabi.template.simple.element;

import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.io.writer.IWriter;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.TemplateException;

public class DebugElement extends TemplateElement implements ITemplateElement {

	private static final Logger log = LoggerFactory.getLogger(DebugElement.class);

	private static final Pattern PATTERN = Pattern.compile("debug [a-zA-Z0-9\\.\\-\\[\\]]+");

	public static boolean matches(String content) {
		return PATTERN.matcher(content).matches();
	}

	private final String variableName;

	public DebugElement(String text, String content) {
		super(text);

		// Parse key and value variable names
		int index1 = content.indexOf(' ', 0);
		this.variableName = content.substring(index1 + 1);
	}

	@Override
	public void process(ITemplateLocator locator, ITemplateData data, IWriter writer) {
		try {

			// If the variable does not exist, just ignore
			boolean exists = data.contains(variableName);
			if (!exists) {
				log.debug("Variable: " + variableName + " does not exist");
			} else {
				Object value = data.get(variableName);
				log.debug("Variable: " + variableName + " = " + value);
			}

		} catch (Exception e) {
			throw new TemplateException("Element processing failed: " + getText(), e);
		}
	}

}
