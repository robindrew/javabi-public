package com.javabi.template.simple;

import java.util.Map;

import com.javabi.common.io.writer.IWriter;
import com.javabi.common.io.writer.StringBuilderWriter;
import com.javabi.template.ITemplate;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.TemplateData;
import com.javabi.template.TemplateException;
import com.javabi.template.simple.element.IRootElement;

public class SimpleTemplate implements ITemplate {

	private final String name;
	private final ITemplateLocator locator;
	private final IRootElement root;

	public SimpleTemplate(String name, ITemplateLocator locator, IRootElement root) {
		if (name == null) {
			throw new NullPointerException("name");
		}
		if (locator == null) {
			throw new NullPointerException("locator");
		}
		if (root == null) {
			throw new NullPointerException("root");
		}
		this.name = name;
		this.locator = locator;
		this.root = root;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ITemplateLocator getLocator() {
		return locator;
	}

	@Override
	public String execute(Map<String, ?> map) {
		return execute(new TemplateData(map));
	}

	@Override
	public String execute(ITemplateData data) {
		IWriter writer = new StringBuilderWriter();
		execute(data, writer);
		return writer.toString();
	}

	@Override
	public void execute(ITemplateData data, IWriter writer) {
		try {
			root.process(locator, data, writer);
		} catch (Exception e) {
			throw new TemplateException("Template execution failed: " + name, e);
		}
	}

}
