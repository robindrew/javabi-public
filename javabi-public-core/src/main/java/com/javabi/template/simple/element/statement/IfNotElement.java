package com.javabi.template.simple.element.statement;

import java.util.regex.Pattern;

import com.javabi.template.ITemplateData;
import com.javabi.template.simple.variable.TemplateVariable;

public class IfNotElement extends ConditionalElement {

	private static final Pattern PATTERN = Pattern.compile("ifnot " + VARIABLE + "( .+)?");

	public static boolean matches(String content) {
		return PATTERN.matcher(content).matches();
	}

	private final String variableName;
	private final TemplateVariable variableValue;

	public IfNotElement(String text, String content) {
		super(text);

		// Parse variable name
		int index1 = content.indexOf(' ', 0);
		int index2 = content.indexOf(' ', index1 + 1);
		if (index2 == -1) {
			this.variableName = content.substring(index1 + 1);
			this.variableValue = new TemplateVariable(true);
		} else {
			this.variableName = content.substring(index1 + 1, index2);
			this.variableValue = new TemplateVariable(content.substring(index2 + 1));
		}
	}

	@Override
	protected boolean evaluate(ITemplateData data) {
		Object value = data.get(variableName);
		return !variableValue.equals(new TemplateVariable(value));
	}

}
