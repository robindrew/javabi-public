package com.javabi.template.simple;

import java.io.Reader;
import java.io.Writer;
import java.util.Map;

import com.google.common.base.Throwables;
import com.google.common.io.CharStreams;
import com.javabi.template.ITemplate;
import com.javabi.template.evaluator.AbstractTemplateEvaluator;
import com.javabi.template.simple.element.IRootElement;
import com.javabi.template.simple.locator.SimpleLocatorMap;
import com.javabi.template.simple.parser.ITemplateParser;
import com.javabi.template.simple.parser.TemplateElementParser;
import com.javabi.template.simple.parser.TemplateParser;

public class SimpleEvaluator extends AbstractTemplateEvaluator {

	@Override
	public String evaluate(String text, Map<String, Object> parameters) {
		SimpleLocatorMap locator = new SimpleLocatorMap();
		ITemplateParser parser = new TemplateParser(new TemplateElementParser());
		IRootElement root = parser.parse(text);
		ITemplate template = new SimpleTemplate("SimpleEvaluator", locator, root);
		locator.setTemplate(template);
		return template.execute(parameters);
	}

	@Override
	public void evaluate(Reader reader, Writer writer, Map<String, Object> parameters) {
		try {
			String text = CharStreams.toString(reader);
			String result = evaluate(text, parameters);
			writer.write(result);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public String evaluate(Reader reader, Map<String, Object> parameters) {
		try {
			String text = CharStreams.toString(reader);
			return evaluate(text, parameters);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}
}
