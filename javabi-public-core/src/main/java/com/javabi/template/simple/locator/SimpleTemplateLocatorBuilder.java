package com.javabi.template.simple.locator;

import com.google.common.base.Supplier;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.simple.parser.ITemplateElementParser;
import com.javabi.template.simple.parser.ITemplateParser;
import com.javabi.template.simple.parser.TemplateElementParser;
import com.javabi.template.simple.parser.TemplateParser;

public class SimpleTemplateLocatorBuilder implements Supplier<ITemplateLocator> {

	private String path = "";
	private String extension = "template";
	private boolean caching = false;
	private String delimiterStart = "${";
	private String delimiterFinish = "}";

	public SimpleTemplateLocatorBuilder setPath(String path) {
		if (path.isEmpty()) {
			throw new IllegalArgumentException("path is empty");
		}
		this.path = path;
		return this;
	}

	public SimpleTemplateLocatorBuilder setExtension(String extension) {
		this.extension = extension;
		return this;
	}

	public SimpleTemplateLocatorBuilder setCaching(boolean caching) {
		this.caching = caching;
		return this;
	}

	public SimpleTemplateLocatorBuilder setDelimiterStart(String delimiterStart) {
		this.delimiterStart = delimiterStart;
		return this;
	}

	public SimpleTemplateLocatorBuilder setDelimiterFinish(String delimiterFinish) {
		this.delimiterFinish = delimiterFinish;
		return this;
	}

	public ITemplateLocator get() {
		if (path.isEmpty()) {
			throw new IllegalStateException("path is empty");
		}

		// Template locator
		ITemplateElementParser elementParser = new TemplateElementParser();
		ITemplateParser parser = new TemplateParser(elementParser, delimiterStart, delimiterFinish);
		SimpleLocatorPath locator = new SimpleLocatorPath(path, extension, parser);
		locator.setCaching(caching);
		return locator;
	}

}
