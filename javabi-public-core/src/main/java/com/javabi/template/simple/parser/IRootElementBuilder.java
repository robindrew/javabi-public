package com.javabi.template.simple.parser;

import com.javabi.template.simple.element.IRootElement;
import com.javabi.template.simple.element.ITemplateElement;

public interface IRootElementBuilder {

	void add(ITemplateElement element);

	IRootElement build();

}
