package com.javabi.template.simple.parser;

import java.util.ArrayList;
import java.util.List;

import com.javabi.template.TemplateException;
import com.javabi.template.simple.element.IParentElement;
import com.javabi.template.simple.element.IRootElement;
import com.javabi.template.simple.element.ITemplateElement;
import com.javabi.template.simple.element.RootElement;
import com.javabi.template.simple.element.statement.IBreakElement;
import com.javabi.template.simple.element.statement.IConditionalElement;
import com.javabi.template.simple.element.statement.IElseElement;
import com.javabi.template.simple.element.statement.IEndElement;
import com.javabi.template.simple.element.statement.ILoopElement;
import com.javabi.template.simple.element.statement.IStatementElement;

public class RootElementBuilder implements IRootElementBuilder {

	private final IRootElement root = new RootElement();
	private final List<IParentElement> stack = new ArrayList<IParentElement>();

	public RootElementBuilder() {
		stack.add(root);
	}

	private IParentElement pop() {
		int size = stack.size();
		return stack.remove(size - 1);
	}

	private IParentElement peek() {
		int size = stack.size();
		return stack.get(size - 1);
	}

	private void push(IParentElement element) {
		stack.add(element);
	}

	@Override
	public IRootElement build() {
		if (stack.size() > 1) {
			ITemplateElement element = pop();
			throw new TemplateException("Missing 'end' to element: " + element);
		}
		return root;
	}

	@Override
	public void add(ITemplateElement element) {
		if (element == null) {
			throw new NullPointerException("element");
		}

		IParentElement parent = peek();
		if (element instanceof IStatementElement) {
			addStatement(parent, (IStatementElement) element);
		} else {
			parent.addChild(element);
		}
	}

	private void addStatement(IParentElement parent, IStatementElement child) {

		// End
		if (child instanceof IEndElement) {
			if (parent instanceof IRootElement) {
				throw new IllegalStateException("Unexpected " + child);
			}
			if (parent.isEmpty()) {
				throw new IllegalStateException("Element ended without any children: " + parent);
			}
			pop();
			return;
		}

		// Else
		if (child instanceof IElseElement) {
			if (!(parent instanceof IConditionalElement)) {
				throw new IllegalStateException("Unexpected " + child + " in element: " + parent);
			}
			IConditionalElement conditional = (IConditionalElement) parent;
			if (!conditional.startElse()) {
				throw new IllegalStateException("Multiple " + child + " found in element: " + parent);
			}
			return;
		}

		// Parent
		if (child instanceof IParentElement) {
			push((IParentElement) child);
			parent.addChild(child);
			return;
		}

		// Break
		if (child instanceof IBreakElement) {
			boolean loop = false;
			for (IParentElement element : stack) {
				if (element instanceof ILoopElement) {
					loop = true;
					break;
				}
			}
			if (!loop) {
				throw new IllegalArgumentException("Unexpected " + child + " in " + stack);
			}
			parent.addChild(child);
		}

		throw new IllegalStateException("Statement element not handled: " + child + " in " + stack);
	}
}
