package com.javabi.template.simple.locator;

import java.util.HashMap;
import java.util.Map;

import com.javabi.common.lang.Resource;
import com.javabi.template.ITemplate;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.TemplateException;
import com.javabi.template.simple.SimpleTemplate;
import com.javabi.template.simple.element.IRootElement;
import com.javabi.template.simple.parser.ITemplateParser;

public class SimpleLocatorPath implements ITemplateLocator {

	private final String path;
	private final String extension;
	private final Map<String, ITemplate> nameToTemplateMap = new HashMap<String, ITemplate>();
	private final ITemplateParser parser;
	private boolean caching = true;

	public SimpleLocatorPath(String path, String extension, ITemplateParser parser) {
		if (path == null) {
			throw new NullPointerException("path");
		}
		if (parser == null) {
			throw new NullPointerException("parser");
		}
		if (extension.isEmpty()) {
			throw new IllegalArgumentException("extension is empty");
		}
		if (path.endsWith("/")) {
			throw new IllegalArgumentException("path ends with /: '" + path + "'");
		}
		if (!new Resource(path).exists()) {
			throw new IllegalArgumentException("path does not exist: '" + path + "'");
		}
		this.path = path;
		this.extension = extension;
		this.parser = parser;
	}

	@Override
	public ITemplate getTemplate(String name) {
		ITemplate template = nameToTemplateMap.get(name);
		if (template == null || !caching) {
			template = loadTemplate(name);
			nameToTemplateMap.put(name, template);
		}
		return template;
	}

	public boolean isCaching() {
		return caching;
	}

	public void setCaching(boolean enabled) {
		this.caching = enabled;
	}

	private ITemplate loadTemplate(String name) {

		// File
		if (!name.endsWith("." + extension)) {
			name = name + "." + extension;
		}
		Resource resource = new Resource(path + "/" + name);
		if (!resource.exists()) {
			throw new TemplateException("Failed to load template: '" + name + "', resource not found: " + resource);
		}

		// Load
		String text;
		try {
			text = resource.readToString();;
		} catch (Exception e) {
			throw new TemplateException("Unable to read template file: " + resource, e);
		}

		// Parse
		try {
			IRootElement root = parser.parse(text);
			return new SimpleTemplate(name, this, root);
		} catch (Exception e) {
			throw new TemplateException("Error parsing template file: " + resource, e);
		}
	}

}
