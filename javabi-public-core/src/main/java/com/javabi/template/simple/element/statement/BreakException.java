package com.javabi.template.simple.element.statement;

public class BreakException extends RuntimeException {

	public BreakException() {
		super("break");
	}

}
