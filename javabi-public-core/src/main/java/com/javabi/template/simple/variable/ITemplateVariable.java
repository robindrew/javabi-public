package com.javabi.template.simple.variable;

public interface ITemplateVariable {

	long size();

	Object get();

	Class<?> getType();

	boolean isBoolean();

	boolean isLong();

	boolean isDouble();

	boolean isString();

	boolean isCollection();

	boolean isMap();

	boolean isArray();

	Boolean toBoolean();

	Long toLong();

	Double toDouble();

	String toString();

}
