package com.javabi.template.simple.element.statement;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.javabi.common.io.writer.IWriter;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.TemplateException;
import com.javabi.template.simple.element.ITemplateElement;
import com.javabi.template.simple.element.TemplateElement;
import com.javabi.template.simple.variable.TemplateVariable;

public class ForElement extends TemplateElement implements ILoopElement {

	private static final Pattern PATTERN = Pattern.compile("for " + VARIABLE);

	public static boolean matches(String content) {
		return PATTERN.matcher(content).matches();
	}

	private final String variableName;
	private List<ITemplateElement> forList = new ArrayList<ITemplateElement>();

	public ForElement(String text, String content) {
		super(text);

		// Parse variable name
		String[] parts = content.split(" ");
		this.variableName = parts[1];
	}

	@Override
	public boolean isEmpty() {
		return forList.isEmpty();
	}

	@Override
	public void addChild(ITemplateElement element) {
		if (element == null) {
			throw new NullPointerException("element");
		}
		forList.add(element);
	}

	@Override
	public void process(ITemplateLocator locator, ITemplateData data, IWriter writer) {
		try {
			processLoop(locator, data, writer);
		} catch (BreakException e) {
			// Break can be swallowed
		} catch (Exception e) {
			throw new TemplateException("Element processing failed: " + getText(), e);
		}
	}

	private void processLoop(ITemplateLocator locator, ITemplateData data, IWriter writer) {
		Object value = data.get(variableName);

		long size = new TemplateVariable(value).size();
		if (size < 0) {
			throw new TemplateException("Invalid size from type: " + value.getClass() + ", value=" + value);
		}

		for (long i = 0; i < size; i++) {
			for (ITemplateElement element : forList) {
				element.process(locator, data, writer);
			}
		}
	}

}
