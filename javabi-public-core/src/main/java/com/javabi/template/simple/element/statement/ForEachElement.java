package com.javabi.template.simple.element.statement;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import com.javabi.common.io.writer.IWriter;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.TemplateException;
import com.javabi.template.simple.element.ITemplateElement;
import com.javabi.template.simple.element.TemplateElement;

public class ForEachElement extends TemplateElement implements ILoopElement {

	private static final Pattern PATTERN = Pattern.compile("foreach " + VARIABLE + " " + VARIABLE);

	public static boolean matches(String content) {
		return PATTERN.matcher(content).matches();
	}

	private final String elementName;
	private final String loopName;
	private List<ITemplateElement> forList = new ArrayList<ITemplateElement>();

	public ForEachElement(String text, String content) {
		super(text);

		// Parse variable name
		String[] parts = content.split(" ");
		this.elementName = parts[1];
		this.loopName = parts[2];
	}

	@Override
	public boolean isEmpty() {
		return forList.isEmpty();
	}

	@Override
	public void addChild(ITemplateElement element) {
		if (element == null) {
			throw new NullPointerException("element");
		}
		forList.add(element);
	}

	@Override
	public void process(ITemplateLocator locator, ITemplateData data, IWriter writer) {
		try {
			processLoop(locator, data, writer);
		} catch (BreakException e) {
			// Break must be swallowed
		} catch (Exception e) {
			throw new TemplateException("Element processing failed: " + getText(), e);
		}
	}

	private void processLoop(ITemplateLocator locator, ITemplateData data, IWriter writer) {
		Object value = data.get(loopName);

		// Collection
		if (value instanceof Collection) {
			int index = 0;
			String elementIndex = elementName + "Index";
			Collection<?> collection = (Collection<?>) value;
			for (Object elementValue : collection) {
				data.set(elementName, elementValue);
				data.set(elementIndex, index++);

				for (ITemplateElement element : forList) {
					element.process(locator, data, writer);
				}
			}
			data.remove(elementName);
			data.remove(elementIndex);
			return;
		}

		// Array
		if (value.getClass().isArray()) {
			int length = Array.getLength(value);
			String elementIndex = elementName + "Index";
			for (int i = 0; i < length; i++) {
				Object elementValue = Array.get(value, i);
				data.set(elementName, elementValue);
				data.set(elementIndex, i);

				for (ITemplateElement element : forList) {
					element.process(locator, data, writer);
				}
			}
			data.remove(elementName);
			data.remove(elementIndex);
			return;
		}

		throw new TemplateException("Unable to parse foreach variable from " + loopName + " = " + value);
	}
}
