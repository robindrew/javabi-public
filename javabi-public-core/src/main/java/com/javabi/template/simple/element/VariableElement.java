package com.javabi.template.simple.element;

import java.util.regex.Pattern;

import com.javabi.common.io.writer.IWriter;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.TemplateException;

public class VariableElement extends TemplateElement implements ITemplateElement {

	private static final Pattern PATTERN = Pattern.compile(VARIABLE + "\\??");

	public static boolean matches(String content) {
		return PATTERN.matcher(content).matches();
	}

	private final String variableName;
	private final boolean optional;

	public VariableElement(String text, String content) {
		super(text);
		optional = content.endsWith("?");
		if (optional) {
			content = content.substring(0, content.length() - 1);
		}
		this.variableName = content;
	}

	@Override
	public void process(ITemplateLocator locator, ITemplateData data, IWriter writer) {
		try {

			// If the variable does not exist, just ignore
			boolean exists = data.contains(variableName);
			if (optional && !exists) {
				return;
			}
			if (!exists) {
				writer.append(getText());
			} else {
				Object value = data.get(variableName);
				writer.append(value.toString());
			}

		} catch (Exception e) {
			throw new TemplateException("Element processing failed: " + getText(), e);
		}
	}

}
