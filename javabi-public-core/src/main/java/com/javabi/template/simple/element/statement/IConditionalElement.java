package com.javabi.template.simple.element.statement;

import com.javabi.template.simple.element.IParentElement;

public interface IConditionalElement extends IStatementElement, IParentElement {

	boolean startElse();

}
