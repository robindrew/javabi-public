package com.javabi.template.simple.element.statement;

import com.javabi.common.io.writer.IWriter;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.simple.element.TemplateElement;

public class BreakElement extends TemplateElement implements IBreakElement {

	public static boolean matches(String content) {
		return "break".equals(content);
	}

	public BreakElement(String text) {
		super(text);
	}

	@Override
	public void process(ITemplateLocator locator, ITemplateData data, IWriter writer) {
		throw new BreakException();
	}

}
