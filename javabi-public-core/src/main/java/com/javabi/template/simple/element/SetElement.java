package com.javabi.template.simple.element;

import java.util.regex.Pattern;

import com.javabi.common.io.writer.IWriter;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.simple.variable.TemplateVariable;

public class SetElement extends TemplateElement implements ITemplateElement {

	private static final Pattern PATTERN = Pattern.compile("set " + VARIABLE + " .+");

	public static boolean matches(String content) {
		return PATTERN.matcher(content).matches();
	}

	private final String key;
	private final TemplateVariable value;

	public SetElement(String text, String content) {
		super(text);

		// Parse key and value variable names
		int index1 = content.indexOf(' ', 0);
		int index2 = content.indexOf(' ', index1 + 1);
		this.key = content.substring(index1 + 1, index2);
		this.value = new TemplateVariable(content.substring(index2 + 1));
	}

	@Override
	public void process(ITemplateLocator locator, ITemplateData data, IWriter writer) {
		data.set(key, value);
	}

}
