package com.javabi.template.simple.element;

public abstract class TemplateElement implements ITemplateElement {

	protected static final String VARIABLE = "[a-zA-Z0-9\\.\\-\\_\\[\\]]+";
	protected static final String FILENAME = "[a-zA-Z0-9\\.\\-\\_\\/]+";

	private String text;

	protected TemplateElement(String text) {
		if (text == null) {
			throw new NullPointerException("text");
		}
		this.text = text;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

}
