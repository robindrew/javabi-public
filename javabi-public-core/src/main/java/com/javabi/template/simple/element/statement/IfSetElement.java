package com.javabi.template.simple.element.statement;

import java.util.regex.Pattern;

import com.javabi.template.ITemplateData;

public class IfSetElement extends ConditionalElement {

	private static final Pattern PATTERN = Pattern.compile("ifset " + VARIABLE);

	public static boolean matches(String content) {
		return PATTERN.matcher(content).matches();
	}

	private final String variableName;

	public IfSetElement(String text, String content) {
		super(text);

		// Parse variable name
		String[] parts = content.split(" ");
		this.variableName = parts[1];
	}

	@Override
	protected boolean evaluate(ITemplateData data) {
		return data.contains(variableName);
	}

}
