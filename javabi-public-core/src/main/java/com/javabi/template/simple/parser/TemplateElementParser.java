package com.javabi.template.simple.parser;

import com.javabi.template.simple.element.DebugElement;
import com.javabi.template.simple.element.ITemplateElement;
import com.javabi.template.simple.element.IncludeElement;
import com.javabi.template.simple.element.SetElement;
import com.javabi.template.simple.element.VariableElement;
import com.javabi.template.simple.element.statement.BreakElement;
import com.javabi.template.simple.element.statement.ElseElement;
import com.javabi.template.simple.element.statement.EndElement;
import com.javabi.template.simple.element.statement.ForEachElement;
import com.javabi.template.simple.element.statement.ForElement;
import com.javabi.template.simple.element.statement.IfElement;
import com.javabi.template.simple.element.statement.IfNotElement;
import com.javabi.template.simple.element.statement.IfSetElement;

public class TemplateElementParser implements ITemplateElementParser {

	public ITemplateElement parse(String text, String content) {

		// End
		if (EndElement.matches(content)) {
			return new EndElement(text);
		}

		// Break
		if (BreakElement.matches(content)) {
			return new BreakElement(text);
		}

		// Else
		if (ElseElement.matches(content)) {
			return new ElseElement(text);
		}

		// Variable
		if (VariableElement.matches(content)) {
			return new VariableElement(text, content);
		}

		// Debug
		if (DebugElement.matches(content)) {
			return new DebugElement(text, content);
		}

		// If
		if (IfElement.matches(content)) {
			return new IfElement(text, content);
		}

		// If Not
		if (IfNotElement.matches(content)) {
			return new IfNotElement(text, content);
		}

		// If Set
		if (IfSetElement.matches(content)) {
			return new IfSetElement(text, content);
		}

		// For
		if (ForElement.matches(content)) {
			return new ForElement(text, content);
		}

		// For Each
		if (ForEachElement.matches(content)) {
			return new ForEachElement(text, content);
		}

		// Include
		if (IncludeElement.matches(content)) {
			return new IncludeElement(text, content);
		}

		// Set
		if (SetElement.matches(content)) {
			return new SetElement(text, content);
		}

		// Failed!
		return null;
	}

}
