package com.javabi.template.simple.element.statement;

import java.util.ArrayList;
import java.util.List;

import com.javabi.common.io.writer.IWriter;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.TemplateException;
import com.javabi.template.simple.element.ITemplateElement;
import com.javabi.template.simple.element.TemplateElement;

public abstract class ConditionalElement extends TemplateElement implements IConditionalElement {

	private final List<ITemplateElement> ifList = new ArrayList<ITemplateElement>();
	private final List<ITemplateElement> elseList = new ArrayList<ITemplateElement>();
	private boolean elseElement = false;

	protected ConditionalElement(String text) {
		super(text);
	}

	@Override
	public void addChild(ITemplateElement element) {
		if (element == null) {
			throw new NullPointerException("element");
		}

		// add child to if or else?
		if (elseElement) {
			elseList.add(element);
		} else {
			ifList.add(element);
		}
	}

	@Override
	public boolean isEmpty() {
		return ifList.isEmpty() || (elseElement && elseList.isEmpty());
	}

	@Override
	public boolean startElse() {
		if (elseElement) {
			return false;
		}
		elseElement = true;
		return true;
	}

	@Override
	public void process(ITemplateLocator locator, ITemplateData data, IWriter writer) {
		try {
			if (evaluate(data)) {
				for (ITemplateElement element : ifList) {
					element.process(locator, data, writer);
				}
			} else {
				for (ITemplateElement element : elseList) {
					element.process(locator, data, writer);
				}
			}
		} catch (Exception e) {
			throw new TemplateException("Element processing failed: " + getText(), e);
		}
	}

	protected abstract boolean evaluate(ITemplateData data);

}
