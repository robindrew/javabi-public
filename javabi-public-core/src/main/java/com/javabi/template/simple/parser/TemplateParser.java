package com.javabi.template.simple.parser;

import java.util.ArrayList;
import java.util.List;

import com.javabi.template.TemplateException;
import com.javabi.template.simple.element.IRootElement;
import com.javabi.template.simple.element.ITemplateElement;
import com.javabi.template.simple.element.TextElement;

public class TemplateParser implements ITemplateParser {

	private static final char ESCAPE = '\\';
	private static final String DEFAULT_START = "${";
	private static final String DEFAULT_FINISH = "}";

	private final ITemplateElementParser parser;
	private final String startDelimiter;
	private final String finishDelimiter;

	public TemplateParser(ITemplateElementParser parser, String startDelimiter, String finishDelimiter) {
		if (parser == null) {
			throw new NullPointerException("parser");
		}
		this.parser = parser;
		this.startDelimiter = startDelimiter;
		this.finishDelimiter = finishDelimiter;
	}

	public TemplateParser(ITemplateElementParser parser) {
		this(parser, DEFAULT_START, DEFAULT_FINISH);
	}

	public TemplateParser() {
		this(new TemplateElementParser(), DEFAULT_START, DEFAULT_FINISH);
	}

	@Override
	public IRootElement parse(String text) {
		IRootElementBuilder root = new RootElementBuilder();

		int textIndex = 0;
		int searchIndex = 0;
		List<Integer> escapedList = new ArrayList<Integer>();
		while (true) {

			// Find start of next element
			int beginIndex = text.indexOf(startDelimiter, searchIndex);

			// End of template
			if (beginIndex == -1) {
				String block = text.substring(textIndex);
				String[] parts = getParts(block, escapedList, textIndex);
				ITemplateElement element = new TextElement(block, parts);
				root.add(element);
				break;
			}

			// Escaped, skip and try again
			if (isEscaped(text, beginIndex)) {
				escapedList.add(beginIndex - 1);
				searchIndex = beginIndex + 1;
				continue;
			}

			// Text element
			if (textIndex < beginIndex) {
				String block = text.substring(textIndex, beginIndex);
				String[] parts = getParts(block, escapedList, textIndex);
				ITemplateElement element = new TextElement(block, parts);
				root.add(element);
			}

			// Find end of element (can not be escaped)
			int endIndex = text.indexOf(finishDelimiter, beginIndex + 1);
			if (endIndex == -1) {
				throw new TemplateException("Element started but not ended at index: " + beginIndex + ", text: " + text.substring(beginIndex));
			}

			// Parse element
			String elementText = text.substring(beginIndex, endIndex + finishDelimiter.length());
			String elementContent = text.substring(beginIndex + startDelimiter.length(), endIndex);
			ITemplateElement element = parser.parse(elementText, elementContent);
			if (element == null) {
				throw new TemplateException("Unable to parse element from: '" + elementText + "' in " + text);
			}
			root.add(element);

			textIndex = endIndex + 1;
			searchIndex = endIndex + 1;
		}

		return root.build();
	}

	private String[] getParts(String text, List<Integer> list, int offset) {
		int[] escaped = toIntArray(list, offset);
		return split(text, escaped);
	}

	private String[] split(String text, int[] escaped) {
		if (escaped == null || escaped.length == 0) {
			return new String[] { text };
		}
		String[] parts = new String[escaped.length + 1];
		int index = -1;
		for (int i = 0; i < escaped.length; i++) {
			parts[i] = text.substring(index + 1, escaped[i]);
			index = escaped[i];
		}
		parts[escaped.length] = text.substring(index + 1);
		return parts;
	}

	private int[] toIntArray(List<Integer> list, int offset) {
		if (list.isEmpty()) {
			return null;
		}
		int[] array = new int[list.size()];
		for (int i = 0; i < list.size(); i++) {
			array[i] = list.get(i) - offset;
		}
		list.clear();
		return array;
	}

	private boolean isEscaped(String text, int index) {
		return index > 0 && text.charAt(index - 1) == ESCAPE;
	}

}
