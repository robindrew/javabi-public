package com.javabi.template.simple.element;

import java.util.ArrayList;
import java.util.List;

import com.javabi.common.io.writer.IWriter;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;

public class RootElement implements IRootElement {

	private final List<ITemplateElement> elements = new ArrayList<ITemplateElement>();

	@Override
	public String getText() {
		return "";
	}

	@Override
	public void addChild(ITemplateElement element) {
		if (element == null) {
			throw new NullPointerException("element");
		}
		elements.add(element);
	}

	@Override
	public void process(ITemplateLocator locator, ITemplateData data, IWriter writer) {
		for (ITemplateElement element : elements) {
			element.process(locator, data, writer);
		}
	}

	@Override
	public boolean isEmpty() {
		return elements.isEmpty();
	}

}
