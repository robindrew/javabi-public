package com.javabi.template.simple.element.statement;

import com.javabi.template.simple.element.IParentElement;

public interface ILoopElement extends IStatementElement, IParentElement {

}
