package com.javabi.template.stringtemplate;

import java.io.Reader;
import java.io.Writer;
import java.util.Map;
import java.util.Map.Entry;

import org.stringtemplate.v4.ST;

import com.google.common.base.Throwables;
import com.google.common.io.CharStreams;
import com.javabi.template.evaluator.AbstractTemplateEvaluator;

public class StringTemplateEvaluator extends AbstractTemplateEvaluator {

	@Override
	public String evaluate(String text, Map<String, Object> parameters) {
		ST template = new ST(text);
		for (Entry<String, Object> parameter : parameters.entrySet()) {
			template.add(parameter.getKey(), parameter.getValue());
		}
		return template.render();
	}

	@Override
	public void evaluate(Reader reader, Writer writer, Map<String, Object> parameters) {
		try {
			String text = CharStreams.toString(reader);
			String result = evaluate(text, parameters);
			writer.write(result);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public String evaluate(Reader reader, Map<String, Object> parameters) {
		try {
			String text = CharStreams.toString(reader);
			return evaluate(text, parameters);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}
}
