package com.javabi.template;

public interface ITemplateLocator {

	ITemplate getTemplate(String name);

}
