package com.javabi.template;

public class TemplateException extends RuntimeException {

	public TemplateException(String message, Throwable t) {
		super(message, t);
	}

	public TemplateException(String message) {
		super(message);
	}
}
