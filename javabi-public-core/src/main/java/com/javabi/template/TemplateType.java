package com.javabi.template;

public enum TemplateType {

	SIMPLE, VELOCITY;
}
