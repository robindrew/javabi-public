package com.javabi.template;

import java.util.Map;

import com.javabi.common.io.writer.IWriter;

public interface ITemplate {

	String getName();

	ITemplateLocator getLocator();

	String execute(Map<String, ?> map);

	String execute(ITemplateData data);

	void execute(ITemplateData data, IWriter writer);

}
