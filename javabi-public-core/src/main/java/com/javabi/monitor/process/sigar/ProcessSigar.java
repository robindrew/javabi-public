package com.javabi.monitor.process.sigar;

import static com.javabi.common.lang.Variables.notNull;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.hyperic.sigar.Cpu;
import org.hyperic.sigar.ProcCpu;
import org.hyperic.sigar.ProcExe;
import org.hyperic.sigar.ProcMem;
import org.hyperic.sigar.ProcState;
import org.hyperic.sigar.ProcTime;
import org.hyperic.sigar.Sigar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.monitor.process.IProcess;

public class ProcessSigar implements IProcess {

	private static final Logger log = LoggerFactory.getLogger(ProcessSigar.class);

	private static Cpu[] getCpus(Sigar sigar, long pid) {
		try {
			return sigar.getCpuList();
		} catch (Exception e) {
			log.warn("[ProcExe not available");
			return null;
		}
	}

	private static ProcExe getProcExe(Sigar sigar, long pid) {
		try {
			return sigar.getProcExe(pid);
		} catch (Exception e) {
			log.warn("[ProcExe not available");
			return null;
		}
	}

	private static ProcCpu getProcCpu(Sigar sigar, long pid) {
		try {
			return sigar.getProcCpu(pid);
		} catch (Exception e) {
			log.warn("[ProcCpu not available");
			return null;
		}
	}

	private static ProcMem getProcMem(Sigar sigar, long pid) {
		try {
			return sigar.getProcMem(pid);
		} catch (Exception e) {
			log.warn("[ProcMem not available");
			return null;
		}
	}

	private static ProcTime getProcTime(Sigar sigar, long pid) {
		try {
			return sigar.getProcTime(pid);
		} catch (Exception e) {
			log.warn("[ProcTime not available");
			return null;
		}
	}

	private static ProcState getProcState(Sigar sigar, long pid) {
		try {
			return sigar.getProcState(pid);
		} catch (Exception e) {
			log.warn("[ProcState not available");
			return null;
		}
	}

	private static String[] getProcArgs(Sigar sigar, long pid) {
		try {
			return sigar.getProcArgs(pid);
		} catch (Exception e) {
			log.warn("[ProcArgs not available");
			return null;
		}
	}

	private static Map<?, ?> getProcEnv(Sigar sigar, long pid) {
		try {
			return sigar.getProcEnv(pid);
		} catch (Exception e) {
			log.warn("[ProcEnv not available");
			return null;
		}
	}

	private static List<?> getProcModules(Sigar sigar, long pid) {
		try {
			return sigar.getProcModules(pid);
		} catch (Exception e) {
			log.warn("[ProcModules not available");
			return null;
		}
	}

	private final Sigar sigar;
	private final long pid;

	private final ProcExe exe;
	private final ProcMem mem;
	private final ProcTime time;
	private final ProcState state;
	private final String[] args;
	private final Map<?, ?> env;
	private final List<?> modules;
	private final Cpu[] cpus;
	private final SigarProcessCpu cpu;

	public ProcessSigar() {
		this(new Sigar());
	}

	public ProcessSigar(Sigar sigar) {
		this(sigar, sigar.getPid());
	}

	public ProcessSigar(Sigar sigar, long pid) {
		this.sigar = notNull("[sigar", sigar);
		this.pid = pid;

		this.exe = getProcExe(sigar, pid);
		this.cpu = new SigarProcessCpu(sigar, pid);
		this.mem = getProcMem(sigar, pid);
		this.time = getProcTime(sigar, pid);
		this.state = getProcState(sigar, pid);
		this.args = getProcArgs(sigar, pid);
		this.env = getProcEnv(sigar, pid);
		this.modules = getProcModules(sigar, pid);
		this.cpus = getCpus(sigar, pid);

		log.info("[Pid] " + pid);
		log.info("[Exe] " + exe);
		log.info("[Cpu] " + cpu);
		log.info("[Mem] " + mem);
		log.info("[Env] " + env);
		log.info("[Time] " + time);
		log.info("[Args] " + Arrays.toString(args));
		log.info("[State] " + state);
		log.info("[Modules] " + modules);
	}

	public Sigar getSigar() {
		return sigar;
	}

	public long getProcessId() {
		return pid;
	}

	public double getCpu() {
		if (cpu == null) {
			return Double.NaN;
		}
		try {
			cpu.gather();
		} catch (Exception e) {
			log.warn("Error updating CPU information using SIGAR", e);
		}
		return cpu.getCpu();
	}

	public long getMem() {
		if (mem == null) {
			return -1;
		}
		try {
			mem.gather(sigar, pid);
		} catch (Exception e) {
			log.warn("Error updating memory information using SIGAR", e);
		}
		return mem.getResident();
	}

	public Cpu[] getCpus() {
		return cpus;
	}

}
