package com.javabi.monitor.process;

public interface IProcessIdFile {

	void check(IProcess process);

	void check(long processId);

}
