package com.javabi.monitor.process;

public interface IProcess {

	long getProcessId();

	double getCpu();

	long getMem();

}
