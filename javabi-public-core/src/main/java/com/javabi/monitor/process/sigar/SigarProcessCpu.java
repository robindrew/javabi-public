package com.javabi.monitor.process.sigar;

import org.hyperic.sigar.ProcCpu;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;

import com.google.common.base.Throwables;

public class SigarProcessCpu {

	private final Sigar sigar;
	private final ProcCpu process;
	private final long pid;
	private final int cpuCount;

	private double cpuPercent = Double.NaN;

	private long previousTime = 0;
	private long previousTotal = 0;
	private long duration;

	public SigarProcessCpu(Sigar sigar, long pid) {
		try {

			// Fields
			this.sigar = sigar;
			this.pid = pid;
			this.cpuCount = sigar.getCpuList().length;
			this.process = new ProcCpu();

			// Initial gather
			process.gather(sigar, pid);
			previousTime = process.getLastTime();
			previousTotal = process.getTotal();

		} catch (SigarException e) {
			throw Throwables.propagate(e);
		}
	}

	public SigarProcessCpu(Sigar sigar) {
		this(sigar, sigar.getPid());
	}

	public long getPid() {
		return pid;
	}

	public long getDuration() {
		return duration;
	}

	public double getCpu() {
		return cpuPercent;
	}

	public boolean isValid() {
		return !Double.isNaN(cpuPercent) && cpuPercent >= 0.0;
	}

	public boolean gather() {
		try {
			process.gather(sigar, pid);

			long currentTime = process.getLastTime();
			long currentTotal = process.getTotal();
			long deltaTime = currentTime - previousTime;
			long deltaTotal = currentTotal - previousTotal;

			// Negative or zero CPU activity recorded?
			if (deltaTotal <= 0) {
				return false;
			}

			// Calculation
			cpuPercent = (100.0 * deltaTotal) / deltaTime / cpuCount;

			// Next
			duration = deltaTime;
			previousTime = currentTime;
			previousTotal = currentTotal;

			return true;
		} catch (SigarException e) {
			throw Throwables.propagate(e);
		}
	}

}
