package com.javabi.monitor.process.simple;

import com.javabi.common.lang.Java;
import com.javabi.monitor.process.IProcess;

public class SimpleProcess implements IProcess {

	private final long processId;

	public SimpleProcess() {
		this.processId = Java.getProcessId();
	}

	@Override
	public long getProcessId() {
		return processId;
	}

	@Override
	public double getCpu() {
		return Double.NaN;
	}

	@Override
	public long getMem() {
		return -1;
	}

}
