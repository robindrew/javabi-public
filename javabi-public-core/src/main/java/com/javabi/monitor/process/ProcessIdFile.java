package com.javabi.monitor.process;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.io.file.FileInput;
import com.javabi.common.io.file.FileOutput;
import com.javabi.common.io.file.IFileInput;
import com.javabi.common.io.file.IFileOutput;
import com.javabi.common.lang.properties.PropertyUtil;

public class ProcessIdFile implements IProcessIdFile {

	private static final Logger log = LoggerFactory.getLogger(ProcessIdFile.class);

	private static final String PID_FILENAME = "pid.filename";

	public long readProcessId(File file) {
		try {
			log.info("Reading process id from file: " + file);
			IFileInput reader = new FileInput(file);
			String pid = reader.readToString();
			return Long.parseLong(pid);
		} catch (Exception e) {
			return -1;
		}
	}

	public void writeProcessId(File file, long pid) {
		log.info("Writing process id to file: " + file + " (pid=" + pid + ")");
		IFileOutput writer = new FileOutput(file);
		writer.writeFromString(String.valueOf(pid));
	}

	@Override
	public void check(IProcess process) {
		check(process.getProcessId());
	}

	@Override
	public void check(long processId) {
		File file = getFile(PID_FILENAME);
		if (file == null) {
			return;
		}
		if (file.exists()) {
			throw new IllegalStateException("Process id file exists: " + file + " with process id: " + readProcessId(file));
		}

		// Write id
		writeProcessId(file, processId);
		file.deleteOnExit();
	}

	private File getFile(String key) {
		String value = PropertyUtil.get(key);
		if (value == null) {
			log.warn("Property not found: '" + key + "'");
			return null;
		}
		return new File(value);
	}

}
