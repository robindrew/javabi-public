package com.javabi.monitor.system.simple;

import com.javabi.monitor.system.ILoadAverage;
import com.javabi.monitor.system.LoadAveragePeriod;

public class SimpleLoadAverage implements ILoadAverage {

	@Override
	public boolean isAvailable() {
		return false;
	}

	@Override
	public double getFifteenMinute() {
		throw new UnsupportedOperationException();
	}

	@Override
	public double getFiveMinute() {
		throw new UnsupportedOperationException();
	}

	@Override
	public double getOneMinute() {
		throw new UnsupportedOperationException();
	}

	@Override
	public double get(LoadAveragePeriod period) {
		throw new UnsupportedOperationException();
	}

}
