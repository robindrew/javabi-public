package com.javabi.monitor.system.summary;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Throwables;
import com.javabi.common.exec.stream.LineReaderHandler;

public class ProcessSummaryListReader extends LineReaderHandler {

	private final ProcessSummaryParser parser = new ProcessSummaryParser();
	private final List<IProcessSummary> list = new ArrayList<IProcessSummary>();

	@Override
	protected void handleException(Exception e) {
		throw Throwables.propagate(e);
	}

	@Override
	protected boolean handleLine(String line) {
		line = line.trim();
		if (line.isEmpty()) {
			return true;
		}
		if (line.startsWith("USER")) {
			return true;
		}
		IProcessSummary summary = parser.parse(line);
		list.add(summary);
		return true;
	}

	public List<IProcessSummary> getList() {
		return list;
	}

}
