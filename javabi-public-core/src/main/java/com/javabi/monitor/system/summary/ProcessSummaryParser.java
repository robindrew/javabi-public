package com.javabi.monitor.system.summary;

import com.javabi.common.text.TokenParser;
import com.javabi.common.text.parser.IStringParser;

public class ProcessSummaryParser implements IStringParser<IProcessSummary> {

	@SuppressWarnings("unused")
	@Override
	public IProcessSummary parse(String line) {
		try {
			TokenParser parser = new TokenParser(line);

			String user = parser.readToken();
			String pid = parser.readToken();
			String cpu = parser.readToken();
			String mem = parser.readToken();
			String vsz = parser.readToken();
			String rss = parser.readToken();
			String tty = parser.readToken();
			String stat = parser.readToken();
			String start = parser.readToken();
			String time = parser.readToken();
			String command = parser.readLastToken();

			ProcessSummary summary = new ProcessSummary();
			summary.setUser(user);
			summary.setPid(Long.parseLong(pid));
			summary.setCpuPercent(Double.parseDouble(cpu));
			summary.setMemoryPercent(Double.parseDouble(mem));
			summary.setMemory(Long.parseLong(rss) * 1000);
			summary.setCommand(command);
			return summary;

		} catch (Exception e) {
			throw new IllegalArgumentException("Badly formatted process summary: '" + line + "'");
		}
	}
}
