package com.javabi.monitor.system.diskio;

import com.google.common.base.Throwables;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.javabi.common.exec.stream.LineReaderHandler;

public class DiskIoMapReader extends LineReaderHandler {

	private int device = 0;
	private final DiskIoParser parser = new DiskIoParser();
	private final Multimap<String, IDiskIo> map = ArrayListMultimap.create();
	private final int iterations;

	public DiskIoMapReader(int iterations) {
		if (iterations < 1) {
			throw new IllegalArgumentException("iterations=" + iterations);
		}
		this.iterations = iterations;
	}

	@Override
	protected void handleException(Exception e) {
		throw Throwables.propagate(e);
	}

	@Override
	protected boolean handleLine(String line) {
		line = line.trim();
		if (line.isEmpty()) {
			return true;
		}
		if (line.startsWith("Device:")) {
			device++;
			return true;
		}
		// We are not interested in the first device printout
		// as it is an average since bootup
		if (device < 2) {
			return true;
		}

		IDiskIo io = parser.parse(line);
		map.put(io.getDeviceName(), io);
		if (device - 1 == iterations) {
			destroy();
			return false;
		}
		return true;
	}

	public Multimap<String, IDiskIo> getMap() {
		return map;
	}

}
