package com.javabi.monitor.system.sigar;

import java.util.ArrayList;
import java.util.List;

import org.hyperic.sigar.Cpu;
import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.FileSystem;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Multimap;
import com.javabi.monitor.system.IFileSystemRoot;
import com.javabi.monitor.system.ILoadAverage;
import com.javabi.monitor.system.ISystem;
import com.javabi.monitor.system.diskio.DiskIoMapParser;
import com.javabi.monitor.system.diskio.IDiskIo;
import com.javabi.monitor.system.summary.IProcessSummary;
import com.javabi.monitor.system.summary.ProcessSummaryListParser;

public class SystemSigar implements ISystem {

	private static final Logger log = LoggerFactory.getLogger(SystemSigar.class);

	private final Sigar sigar;
	private final ILoadAverage loadAverage;

	public SystemSigar(Sigar sigar) {
		if (sigar == null) {
			throw new NullPointerException("sigar");
		}
		this.sigar = sigar;
		this.loadAverage = new SigarLoadAverage(sigar);
	}

	public SystemSigar() {
		this(new Sigar());
	}

	@Override
	public ILoadAverage getLoadAverage() {
		return loadAverage;
	}

	@Override
	public List<IFileSystemRoot> getFileSystemRoots() {
		List<IFileSystemRoot> list = new ArrayList<IFileSystemRoot>();
		try {
			FileSystem[] systems = sigar.getFileSystemList();
			for (FileSystem system : systems) {
				IFileSystemRoot root = new SigarFileSystemRoot(sigar, system);
				list.add(root);
			}
		} catch (SigarException se) {
			log.warn("Error getting SIGAR file systems", se);
		}
		return list;
	}

	@Override
	public double getCpu() {
		try {
			CpuPerc cpu = sigar.getCpuPerc();
			double percent = cpu.getCombined() * 100.0;
			if (percent > 10000.0) {
				return Double.NaN;
			}
			return percent;
		} catch (Exception e) {
			log.warn("Error getting SIGAR cpu usage");
		}
		return Double.NaN;
	}

	@Override
	public int getCpuCount() {
		Cpu[] cpus = null;
		try {
			cpus = sigar.getCpuList();
		} catch (Exception e) {
			log.warn("Error getting SIGAR cpu usage");
		}
		if (cpus != null) {
			return cpus.length;
		}
		return Runtime.getRuntime().availableProcessors();
	}

	@Override
	public List<IProcessSummary> getProcessSummary() {
		ProcessSummaryListParser parser = new ProcessSummaryListParser();
		return parser.parse();
	}

	@Override
	public Multimap<String, IDiskIo> getDiskIo(int iterations) {
		DiskIoMapParser parser = new DiskIoMapParser(iterations);
		return parser.parse();
	}

}
