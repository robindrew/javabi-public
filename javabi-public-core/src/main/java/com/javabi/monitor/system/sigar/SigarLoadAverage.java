package com.javabi.monitor.system.sigar;

import static com.javabi.monitor.system.LoadAveragePeriod.FIFTEEN_MINUTE;
import static com.javabi.monitor.system.LoadAveragePeriod.FIVE_MINUTE;
import static com.javabi.monitor.system.LoadAveragePeriod.ONE_MINUTE;

import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;
import org.hyperic.sigar.SigarNotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.monitor.system.ILoadAverage;
import com.javabi.monitor.system.LoadAveragePeriod;

public class SigarLoadAverage implements ILoadAverage {

	private static final Logger log = LoggerFactory.getLogger(SigarLoadAverage.class);

	private final Sigar sigar;
	private final boolean available;

	public SigarLoadAverage(Sigar sigar) {
		if (sigar == null) {
			throw new NullPointerException("sigar");
		}
		this.sigar = sigar;
		this.available = checkAvailable();
	}

	@Override
	public boolean isAvailable() {
		return available;
	}

	private boolean checkAvailable() {
		try {
			sigar.getLoadAverage();
		} catch (SigarNotImplementedException ni) {
			return false;
		} catch (SigarException e) {
			log.warn("Error obtaining SIGAR load average", e);
		}
		return true;
	}

	private double get(int index) {
		try {
			double[] averages = sigar.getLoadAverage();
			if (averages != null && averages.length == 3) {
				return averages[index];
			}
		} catch (SigarNotImplementedException ni) {
			// Rethrow - should have been avoided by checking isAvailable()
			throw Throwables.propagate(ni);
		} catch (SigarException e) {
			log.warn("Error obtaining SIGAR load average", e);
		}
		return Double.NaN;
	}

	@Override
	public double get(LoadAveragePeriod period) {
		return get(period.ordinal());
	}

	@Override
	public double getFifteenMinute() {
		return get(FIFTEEN_MINUTE);
	}

	@Override
	public double getFiveMinute() {
		return get(FIVE_MINUTE);
	}

	@Override
	public double getOneMinute() {
		return get(ONE_MINUTE);
	}

	@Override
	public String toString() {
		return getOneMinute() + ", " + getFiveMinute() + ", " + getFifteenMinute();
	}

}
