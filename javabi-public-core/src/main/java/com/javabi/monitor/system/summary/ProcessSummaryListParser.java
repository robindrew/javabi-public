package com.javabi.monitor.system.summary;

import java.util.List;

import com.google.common.base.Throwables;
import com.javabi.common.exec.ExecHandler;
import com.javabi.common.exec.IExecProcess;

public class ProcessSummaryListParser extends ExecHandler {

	private static final String[] COMMAND = new String[] { "ps", "auxwww" };

	public List<IProcessSummary> parse() {
		try {
			ProcessSummaryListReader reader = new ProcessSummaryListReader();
			setInputHandler(reader);

			// Execute
			IExecProcess process = execute();
			process.waitFor();

			return reader.getList();
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public String[] getCommand() {
		return COMMAND;
	}

}
