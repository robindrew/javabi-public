package com.javabi.monitor.system.summary;

import com.javabi.common.lang.Java;

public class ProcessSummary implements IProcessSummary {

	private String user;
	private long pid;
	private double cpuPercent;
	private double memoryPercent;
	private long memory;
	private String command;

	public String getUser() {
		return user;
	}

	public long getPid() {
		return pid;
	}

	public double getCpuPercent() {
		return cpuPercent;
	}

	public double getMemoryPercent() {
		return memoryPercent;
	}

	public long getMemory() {
		return memory;
	}

	public String getCommand() {
		return command;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setPid(long pid) {
		this.pid = pid;
	}

	public void setCpuPercent(double cpuPercent) {
		this.cpuPercent = cpuPercent;
	}

	public void setMemoryPercent(double memoryPercent) {
		this.memoryPercent = memoryPercent;
	}

	public void setMemory(long memory) {
		this.memory = memory;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

}
