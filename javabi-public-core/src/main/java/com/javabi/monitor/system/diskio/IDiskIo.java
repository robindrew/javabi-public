package com.javabi.monitor.system.diskio;

public interface IDiskIo {

	String getDeviceName();

	double getTotalTime();

	double getServiceTime();

	double getWaitTime();

	double getWaitPercent();

	double getUtil();
}
