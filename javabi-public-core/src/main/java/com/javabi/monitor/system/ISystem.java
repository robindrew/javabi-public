package com.javabi.monitor.system;

import java.util.List;

import com.google.common.collect.Multimap;
import com.javabi.monitor.system.diskio.IDiskIo;
import com.javabi.monitor.system.summary.IProcessSummary;

public interface ISystem {

	ILoadAverage getLoadAverage();

	List<IFileSystemRoot> getFileSystemRoots();

	List<IProcessSummary> getProcessSummary();

	Multimap<String, IDiskIo> getDiskIo(int iterations);

	double getCpu();

	int getCpuCount();

}
