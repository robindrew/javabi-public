package com.javabi.monitor.system;

public enum LoadAveragePeriod {

	ONE_MINUTE, FIVE_MINUTE, FIFTEEN_MINUTE;

}
