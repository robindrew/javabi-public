package com.javabi.monitor.system;

public interface IFileSystemRoot {

	String getName();

	String getMountName();

	double getPercentUsed();

	double getPercentFree();

	long getCapacity();

	long getUsed();

	long getFree();

}
