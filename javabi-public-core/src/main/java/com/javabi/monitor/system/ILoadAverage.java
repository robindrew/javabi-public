package com.javabi.monitor.system;

public interface ILoadAverage {

	boolean isAvailable();

	double get(LoadAveragePeriod period);

	double getFifteenMinute();

	double getFiveMinute();

	double getOneMinute();

}
