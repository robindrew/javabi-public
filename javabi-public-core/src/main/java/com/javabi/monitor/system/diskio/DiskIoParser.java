package com.javabi.monitor.system.diskio;

import static com.javabi.common.text.StringFormats.decimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.text.TokenParser;
import com.javabi.common.text.parser.IStringParser;

public class DiskIoParser implements IStringParser<IDiskIo> {

	private static final Logger log = LoggerFactory.getLogger(DiskIoParser.class);

	@SuppressWarnings("unused")
	@Override
	public IDiskIo parse(String line) {
		try {
			TokenParser parser = new TokenParser(line);

			// The device name
			String device = parser.readToken();

			// The number of read requests merged per second that were issued to the device
			String rrqms = parser.readToken();

			// The number of write requests merged per second that were issued to the device
			String wrqms = parser.readToken();

			// The number of read requests that were issued to the device per second
			String rs = parser.readToken();

			// The number of write requests that were issued to the device per second
			String ws = parser.readToken();

			// The number of sectors read from the device per second
			String rsecs = parser.readToken();

			// The number of sectors written to the device per second
			String wsecs = parser.readToken();

			// The average size (in sectors) of the requests that were issued to the device
			String avgrqsz = parser.readToken();

			// The average queue length of the requests that were issued to the device
			String avgqusz = parser.readToken();

			// The average total time (in milliseconds) for I/O requests issued to the device to be served
			String await = parser.readToken();

			// The average service time (in milliseconds) for I/O requests that were issued to the device
			String svctm = parser.readToken();

			// Percentage of CPU time during which I/O requests were issued to the device
			String util = parser.readLastToken();

			DiskIo io = new DiskIo();
			io.setDeviceName(device);
			io.setTotalTime(Double.parseDouble(await));
			io.setServiceTime(Double.parseDouble(svctm));
			io.setUtil(Double.parseDouble(util));
			log.info("Disk I/O: [" + device + "] " + format(io.getUtil()) + "% util, " + format(io.getWaitPercent()) + "% wait");
			return io;

		} catch (Exception e) {
			throw new IllegalArgumentException("Badly formatted process summary: '" + line + "'");
		}
	}

	private String format(double number) {
		return decimal(number, 2);
	}

}
