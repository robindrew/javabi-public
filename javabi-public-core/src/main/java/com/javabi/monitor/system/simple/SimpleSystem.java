package com.javabi.monitor.system.simple;

import static java.util.Collections.emptyList;

import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.javabi.monitor.system.IFileSystemRoot;
import com.javabi.monitor.system.ILoadAverage;
import com.javabi.monitor.system.ISystem;
import com.javabi.monitor.system.diskio.IDiskIo;
import com.javabi.monitor.system.summary.IProcessSummary;

public class SimpleSystem implements ISystem {

	private final SimpleLoadAverage load = new SimpleLoadAverage();

	@Override
	public ILoadAverage getLoadAverage() {
		return load;
	}

	@Override
	public double getCpu() {
		return Double.NaN;
	}

	@Override
	public int getCpuCount() {
		return Runtime.getRuntime().availableProcessors();
	}

	@Override
	public List<IFileSystemRoot> getFileSystemRoots() {
		return emptyList();
	}

	@Override
	public List<IProcessSummary> getProcessSummary() {
		return emptyList();
	}

	@Override
	public Multimap<String, IDiskIo> getDiskIo(int iterations) {
		return ArrayListMultimap.create();
	}

}
