package com.javabi.monitor.system.summary;

public interface IProcessSummary {

	String getUser();

	long getPid();

	double getCpuPercent();

	double getMemoryPercent();

	long getMemory();

	String getCommand();
}
