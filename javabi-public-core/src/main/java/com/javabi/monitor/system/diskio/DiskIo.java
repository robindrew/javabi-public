package com.javabi.monitor.system.diskio;

import com.javabi.common.lang.Java;

public class DiskIo implements IDiskIo {

	private String deviceName;
	private double totalTime;
	private double serviceTime;
	private double util;

	@Override
	public double getUtil() {
		return util;
	}

	@Override
	public String getDeviceName() {
		return deviceName;
	}

	@Override
	public double getTotalTime() {
		return totalTime;
	}

	@Override
	public double getServiceTime() {
		return serviceTime;
	}

	public void setUtil(double util) {
		this.util = util;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public void setTotalTime(double totalTime) {
		this.totalTime = totalTime;
	}

	public void setServiceTime(double serviceTime) {
		this.serviceTime = serviceTime;
	}

	@Override
	public double getWaitTime() {
		return totalTime - serviceTime;
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

	@Override
	public double getWaitPercent() {
		if (totalTime <= serviceTime) {
			return 0.0;
		}
		if (serviceTime <= 0.0) {
			return 100.0;
		}
		double waitTime = totalTime - serviceTime;
		return (waitTime * 100.0) / totalTime;
	}

}
