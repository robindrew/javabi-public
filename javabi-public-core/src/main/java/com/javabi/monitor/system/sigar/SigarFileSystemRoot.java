package com.javabi.monitor.system.sigar;

import org.hyperic.sigar.FileSystem;
import org.hyperic.sigar.FileSystemUsage;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;

import com.google.common.base.Throwables;
import com.javabi.monitor.system.IFileSystemRoot;

public class SigarFileSystemRoot implements IFileSystemRoot {

	private final Sigar sigar;
	private final FileSystem system;

	public SigarFileSystemRoot(Sigar sigar, FileSystem system) {
		if (sigar == null) {
			throw new NullPointerException("sigar");
		}
		if (system == null) {
			throw new NullPointerException("system");
		}
		this.sigar = sigar;
		this.system = system;
	}

	private FileSystemUsage getUsage() {
		try {
			return sigar.getFileSystemUsage(system.getDirName());
		} catch (SigarException e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public String getName() {
		return system.getDevName();
	}

	@Override
	public String getMountName() {
		return system.getDirName();
	}

	@Override
	public double getPercentUsed() {
		return getUsage().getUsePercent() * 100.0;
	}

	@Override
	public double getPercentFree() {
		double used = getPercentUsed();
		if (used >= 100.0) {
			return 0.0;
		}
		return 100.0 - used;
	}

	@Override
	public long getCapacity() {
		return getUsage().getTotal() * 1000;
	}

	@Override
	public long getUsed() {
		return getUsage().getUsed() * 1000;
	}

	@Override
	public long getFree() {
		return getUsage().getFree() * 1000;
	}

}
