package com.javabi.monitor.system.diskio;

import com.google.common.base.Throwables;
import com.google.common.collect.Multimap;
import com.javabi.common.exec.ExecHandler;
import com.javabi.common.exec.IExecProcess;

public class DiskIoMapParser extends ExecHandler {

	private static final String[] COMMAND = new String[] { "iostat", "-dx", "1" };

	private final int iterations;

	public DiskIoMapParser(int iterations) {
		if (iterations < 1) {
			throw new IllegalArgumentException("iterations=" + iterations);
		}
		this.iterations = iterations;
	}

	public Multimap<String, IDiskIo> parse() {
		try {
			DiskIoMapReader reader = new DiskIoMapReader(iterations);
			setInputHandler(reader);

			// Execute
			IExecProcess process = execute();
			process.waitFor();

			return reader.getMap();
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public String[] getCommand() {
		return COMMAND;
	}

}
