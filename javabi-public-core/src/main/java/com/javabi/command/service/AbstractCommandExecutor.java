package com.javabi.command.service;

import static com.javabi.common.dependency.DependencyFactory.getDependency;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.command.IExecutableCommand;
import com.javabi.command.IExecutableCommandResponse;
import com.javabi.command.errorcode.ErrorCodeException;
import com.javabi.command.errorcode.ErrorCodeSet;
import com.javabi.command.errorcode.IErrorCode;
import com.javabi.common.date.timer.ITimer;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.common.lang.IThrowableHandler;
import com.javabi.common.lang.Java;

public abstract class AbstractCommandExecutor<R, C extends IExecutableCommand<R>> implements ICommandExecutor<R, C> {

	private static final Logger log = LoggerFactory.getLogger(AbstractCommandExecutor.class);

	public static final AtomicLong globalNumber = new AtomicLong(1);

	private final long number;
	private long executionLimit = SECONDS.toNanos(1);

	protected AbstractCommandExecutor() {
		this.number = globalNumber.getAndAdd(1);
	}

	public long getNumber() {
		return number;
	}

	public long getExecutionLimit() {
		return executionLimit;
	}

	public void setExecutionLimit(long limit) {
		if (limit < 1) {
			throw new IllegalArgumentException("limit=" + limit);
		}
		this.executionLimit = SECONDS.toNanos(limit);
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

	@Override
	public IExecutableCommandResponse<R> executeCommand(C command) {
		ITimer timer = new NanoTimer();
		timer.start();
		short commandId = command.getCommandId();
		try {
			log.info("[Command #" + getNumber() + "] Request: " + command);

			R returnValue = execute(command);
			timer.stop();

			IExecutableCommandResponse<R> response = newResponse(command);
			response.setCommandId(commandId);
			response.setReturnValue(returnValue);

			// Log response
			log(command, response, timer);

			return response;

		} catch (ErrorCodeException ece) {
			timer.stop();

			IExecutableCommandResponse<R> response = newResponse(command);
			response.setCommandId(commandId);
			response.setErrors(ece.getErrors());
			if (log.isDebugEnabled()) {
				log.debug("[Command #" + getNumber() + "] Response: " + response + " in " + timer);
			} else {
				log.info("[Command #" + getNumber() + "] Request: " + command + ", Errors: " + ece.getErrors() + " in " + timer);
			}
			return response;

		} catch (Throwable t) {
			timer.stop();

			IThrowableHandler handler = getDependency(IThrowableHandler.class);
			handler.handle(t);

			// Internal ServerControl Error!
			// TODO: Handle error
			// IInternalServerErrorHandler handler = getDependency(IInternalServerErrorHandler.class);
			// long errorId = handler.handle(command, t);

			IExecutableCommandResponse<R> response = newResponse(command);
			response.setCommandId(commandId);
			response.setErrors(new ErrorCodeSet(getInternalServerErrorCode()));
			log.error("[Command #" + getNumber() + "] Request: " + command + " Failed in " + timer, t);

			return response;
		}
	}

	private void log(C command, IExecutableCommandResponse<R> response, ITimer timer) {
		try {

			// Exceeded execution limit?
			if (timer.elapsed() > getExecutionLimit()) {
				if (command.isDebuggable()) {
					log.info("[Command #" + getNumber() + "] Response: " + response + " in " + timer);
				} else {
					log.info("[Command #" + getNumber() + "] Request: " + command + " in " + timer);
				}
				return;
			}

			// Debug enabled?
			if (log.isDebugEnabled()) {
				if (command.isDebuggable()) {
					log.debug("[Command #" + getNumber() + "] Response: " + response + " in " + timer);
				} else {
					log.debug("[Command #" + getNumber() + "] Request: " + command + " in " + timer);
				}
				return;
			}
		} catch (Exception e) {
			log.error("[Command #" + getNumber() + "] Request: " + command + " Error debugging response!", e);
		}
	}

	@SuppressWarnings("unchecked")
	public IExecutableCommandResponse<R> newResponse(C command) {
		try {
			String name = command.getClass().getName() + "Response";
			Class<?> type = Class.forName(name);
			return (IExecutableCommandResponse<R>) type.newInstance();
		} catch (Exception e) {
			log.warn("Failed to resolve response for command: " + command);
			throw Throwables.propagate(e);
		}
	}

	protected abstract IErrorCode getInternalServerErrorCode();

	public abstract R execute(C command);

}
