package com.javabi.command.service;

import static com.javabi.common.dependency.DependencyFactory.getDependency;
import static com.javabi.common.text.StringFormats.bytes;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.google.common.io.CountingInputStream;
import com.google.common.io.CountingOutputStream;
import com.javabi.command.ICommandRequestList;
import com.javabi.command.ICommandResponseList;
import com.javabi.command.IExecutableCommand;
import com.javabi.command.IExecutableCommandResponse;
import com.javabi.command.response.CommandRequestList;
import com.javabi.command.serializer.stream.ICommandStream;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.common.io.connection.INetworkAddress;
import com.javabi.common.io.connection.NetworkAddress;
import com.javabi.common.net.ISocketConnector;

public class CommandStreamClient implements ICommandExecutorService {

	private static final Logger log = LoggerFactory.getLogger(CommandStreamClient.class);

	private static final int DEFAULT_CONNECT_TIMEOUT_SECONDS = 5;
	private static final int DEFAULT_BLOCKING_TIMEOUT_SECONDS = 60;

	private final ICommandStream stream;
	private final INetworkAddress address;

	private final AtomicInteger connectTimeout = new AtomicInteger(DEFAULT_CONNECT_TIMEOUT_SECONDS);
	private final AtomicInteger blockingTimeout = new AtomicInteger(DEFAULT_BLOCKING_TIMEOUT_SECONDS);

	public CommandStreamClient(INetworkAddress address) {
		if (address == null) {
			throw new NullPointerException("address");
		}
		this.address = address;
		this.stream = getDependency(ICommandStream.class);
	}

	public CommandStreamClient(String address, int port) {
		this(new NetworkAddress(address, port));
	}

	@Override
	public String toString() {
		// Optimization - address.getHostname() takes time!
		return "CommandStreamClient[" + address + "]";
	}

	public void setConnectTimeoutInSeconds(int seconds) {
		if (seconds < 1) {
			throw new IllegalArgumentException("seconds=" + seconds);
		}
		this.connectTimeout.set(seconds);
	}

	public void setBlockingTimeoutInSeconds(int seconds) {
		if (seconds < 1) {
			throw new IllegalArgumentException("seconds=" + seconds);
		}
		this.blockingTimeout.set(seconds);
	}

	private int getConnectTimeoutInMillis() {
		return (int) SECONDS.toMillis(connectTimeout.get());
	}

	private int getBlockingTimeoutInMillis() {
		return (int) SECONDS.toMillis(blockingTimeout.get());
	}

	@Override
	@SuppressWarnings("unchecked")
	public <R> IExecutableCommandResponse<R> executeCommand(IExecutableCommand<R> command) {
		ICommandRequestList requestList = new CommandRequestList(command);
		ICommandResponseList responseList = execute(requestList);
		return responseList.get(0);
	}

	@Override
	public ICommandResponseList execute(ICommandRequestList requestList) {
		if (requestList == null) {
			throw new NullPointerException("requestList");
		}
		try {
			NanoTimer timer = new NanoTimer();
			timer.start();

			ISocketConnector connector = getDependency(ISocketConnector.class);
			Socket socket = connector.connect(address.toSocketAddress(), getConnectTimeoutInMillis());
			socket.setSoTimeout(getBlockingTimeoutInMillis());

			// Write request
			CountingOutputStream countingOutput = writeRequest(requestList, socket);

			// Read response
			return readResponse(requestList, timer, socket, countingOutput);

		} catch (Throwable t) {
			log.error("Command Failed: " + requestList + ", server=" + address);
			throw handleException(t);
		}
	}

	private CountingOutputStream writeRequest(ICommandRequestList requestList, Socket socket) throws IOException {
		if (log.isDebugEnabled()) {
			String xml = stream.toString(requestList);
			log.debug("Request:\n" + xml);
		}
		OutputStream output = socket.getOutputStream();
		CountingOutputStream countingOutput = new CountingOutputStream(new BufferedOutputStream(output));
		stream.writeRequestList(requestList, countingOutput);
		countingOutput.flush();
		return countingOutput;
	}

	private ICommandResponseList readResponse(ICommandRequestList requestList, NanoTimer timer, Socket socket, CountingOutputStream countingOutput) throws IOException {
		InputStream input = socket.getInputStream();
		CountingInputStream countingInput = new CountingInputStream(new BufferedInputStream(input));
		ICommandResponseList responseList = stream.readResponseList(countingInput);
		timer.stop();
		if (log.isDebugEnabled()) {
			String written = bytes(countingOutput.getCount());
			String read = bytes(countingInput.getCount());
			boolean debuggable = isDebuggable(requestList);
			if (debuggable) {
				String text = stream.toString(responseList);
				log.debug("Response (" + written + " written, " + read + " read in " + timer + ")\n" + text);
			} else {
				log.debug("Response (" + written + " written, " + read + " read in " + timer + ")");
			}
		}
		responseList.setRequestList(requestList);
		return responseList;
	}

	protected RuntimeException handleException(Throwable t) {
		throw Throwables.propagate(t);
	}

	private boolean isDebuggable(ICommandRequestList requestList) {
		for (IExecutableCommand<?> command : requestList) {
			if (!command.isDebuggable()) {
				return false;
			}
		}
		return true;
	}
}
