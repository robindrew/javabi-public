package com.javabi.command.service;

import com.javabi.command.IExecutableCommand;
import com.javabi.command.IExecutableCommandResponse;

public interface ICommandExecutor<R, C extends IExecutableCommand<R>> {

	IExecutableCommandResponse<R> executeCommand(C command);

}
