package com.javabi.command.service;

import com.google.common.base.Throwables;
import com.javabi.command.IExecutableCommand;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class CommandExecutorLookup implements ICommandExecutorLookup {

	protected String getExecutorClassName(Class<? extends IExecutableCommand<?>> command) {
		StringBuilder name = new StringBuilder();
		name.append(command.getPackage().getName());
		name.append(".").append(command.getSimpleName()).append("Executor");
		return name.toString();
	}

	@Override
	public Class<? extends ICommandExecutor<?, ?>> getExecutorClass(IExecutableCommand<?> command) {
		return getExecutorClass((Class) command.getClass());
	}

	@Override
	public Class<? extends ICommandExecutor<?, ?>> getExecutorClass(Class<? extends IExecutableCommand<?>> command) {
		String className = getExecutorClassName(command);
		try {
			return (Class) Class.forName(className);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

}
