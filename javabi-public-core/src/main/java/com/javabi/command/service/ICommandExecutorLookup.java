package com.javabi.command.service;

import com.javabi.command.IExecutableCommand;

public interface ICommandExecutorLookup {

	Class<? extends ICommandExecutor<?, ?>> getExecutorClass(IExecutableCommand<?> command);

	Class<? extends ICommandExecutor<?, ?>> getExecutorClass(Class<? extends IExecutableCommand<?>> command);

}
