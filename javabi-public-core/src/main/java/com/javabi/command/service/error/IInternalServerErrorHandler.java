package com.javabi.command.service.error;

import com.javabi.command.IExecutableCommand;

public interface IInternalServerErrorHandler {

	long handle(IExecutableCommand<?> command, Throwable t);

}
