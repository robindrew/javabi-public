package com.javabi.command.service;

import static com.javabi.common.dependency.DependencyFactory.getDependency;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.command.IExecutableCommand;

public class CommandExecutorService extends AbstractCommandExecutorService {

	private static final Logger log = LoggerFactory.getLogger(CommandExecutorService.class);

	private final ICommandExecutorLookup lookup;
	private final Map<Class<?>, Class<?>> commandToExecutorMap = new ConcurrentHashMap<Class<?>, Class<?>>();

	public CommandExecutorService(ICommandExecutorLookup lookup) {
		if (lookup == null) {
			throw new NullPointerException("lookup");
		}
		this.lookup = lookup;
	}

	public CommandExecutorService() {
		this(getDependency(ICommandExecutorLookup.class));
	}

	private Class<?> getClass(IExecutableCommand<?> command) throws Exception {
		Class<?> key = command.getClass();
		Class<?> value = commandToExecutorMap.get(key);
		if (value == null) {
			value = lookup.getExecutorClass(command);
			commandToExecutorMap.put(key, value);
			log.info("Command: " + key.getSimpleName() + " -> Executor: " + value.getSimpleName());
		}
		return value;
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected <R> ICommandExecutor<R, IExecutableCommand<R>> getExecutor(IExecutableCommand<R> command) {
		try {
			Class<?> clazz = getClass(command);
			return (ICommandExecutor) clazz.newInstance();
		} catch (Exception e) {
			log.warn("Failed to resolve executor for command: " + command);
			throw Throwables.propagate(e);
		}
	}

}
