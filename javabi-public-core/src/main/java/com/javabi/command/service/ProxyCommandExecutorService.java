package com.javabi.command.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.javabi.command.ICommandRequestList;
import com.javabi.command.ICommandResponseList;
import com.javabi.command.IExecutableCommand;
import com.javabi.command.IExecutableCommandResponse;
import com.javabi.command.response.CommandResponseList;

public class ProxyCommandExecutorService implements ICommandExecutorService {

	private final Map<Class<?>, ICommandExecutorService> commandToExecutorMap = new ConcurrentHashMap<Class<?>, ICommandExecutorService>();

	public void redirect(Class<? extends IExecutableCommand<?>> command, ICommandExecutorService service) {
		if (command == null) {
			throw new NullPointerException("command");
		}
		if (service == null) {
			throw new NullPointerException("service");
		}
		commandToExecutorMap.put(command, service);
	}

	@Override
	public <R> IExecutableCommandResponse<R> executeCommand(IExecutableCommand<R> command) {
		Class<?> type = command.getClass();
		ICommandExecutorService executor = commandToExecutorMap.get(type);
		if (executor == null) {
			throw new IllegalArgumentException("No proxy executor configured to handle command: " + type.getSimpleName());
		}
		return executor.executeCommand(command);
	}

	@Override
	public ICommandResponseList execute(ICommandRequestList requestList) {
		if (requestList == null) {
			throw new NullPointerException("requestList");
		}
		CommandResponseList responseList = new CommandResponseList();
		for (IExecutableCommand<?> command : requestList) {
			IExecutableCommandResponse<?> response = executeCommand(command);
			responseList.add(response);
		}
		return responseList;
	}

}
