package com.javabi.command.service;

import com.google.common.base.Supplier;
import com.javabi.common.io.connection.INetworkAddress;
import com.javabi.common.io.connection.NetworkAddress;

public class CommandStreamClientSupplier implements Supplier<CommandStreamClient> {

	private String host;
	private int port;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	@Override
	public CommandStreamClient get() {
		if (host.isEmpty()) {
			throw new IllegalStateException("host is empty");
		}
		if (port < 1 || port > NetworkAddress.MAXIMUM_PORT) {
			throw new IllegalStateException("port=" + port);
		}

		INetworkAddress address = new NetworkAddress(host, port);
		return new CommandStreamClient(address);
	}

}
