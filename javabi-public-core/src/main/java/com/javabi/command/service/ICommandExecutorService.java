package com.javabi.command.service;

import com.javabi.command.ICommandRequestList;
import com.javabi.command.ICommandResponseList;
import com.javabi.command.IExecutableCommand;
import com.javabi.command.IExecutableCommandResponse;

/**
 * A Command Executor Service.
 */
public interface ICommandExecutorService {

	/**
	 * Execute the given command.
	 * @param <R> the return type.
	 * @param command the command.
	 * @return the return value.
	 */
	<R> IExecutableCommandResponse<R> executeCommand(IExecutableCommand<R> command);

	/**
	 * Execute a list of commands.
	 * @param requestList the request list.
	 * @return the response.
	 */
	ICommandResponseList execute(ICommandRequestList requestList);
}
