package com.javabi.command.service;

import com.javabi.command.ICommandRequestList;
import com.javabi.command.ICommandResponseList;
import com.javabi.command.IExecutableCommand;
import com.javabi.command.IExecutableCommandResponse;
import com.javabi.command.response.CommandResponseList;

public abstract class AbstractCommandExecutorService implements ICommandExecutorService {

	@Override
	public final <R> IExecutableCommandResponse<R> executeCommand(IExecutableCommand<R> command) {
		ICommandExecutor<R, IExecutableCommand<R>> executor = getExecutor(command);
		return executor.executeCommand(command);
	}

	@Override
	public ICommandResponseList execute(ICommandRequestList requestList) {
		if (requestList == null) {
			throw new NullPointerException("requestList");
		}
		CommandResponseList responseList = new CommandResponseList();
		for (IExecutableCommand<?> command : requestList) {
			IExecutableCommandResponse<?> response = executeCommand(command);
			responseList.add(response);
		}
		return responseList;
	}

	protected abstract <R> ICommandExecutor<R, IExecutableCommand<R>> getExecutor(IExecutableCommand<R> command);

}
