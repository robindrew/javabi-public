package com.javabi.command;

import java.io.Serializable;

/**
 * An Executable Command.
 * @param <R> the return type.
 */
public interface IExecutableCommand<R> extends Serializable {

	short getCommandId();

	boolean isDebuggable();
}
