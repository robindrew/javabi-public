package com.javabi.command.object;

import com.javabi.common.lang.reflect.method.IMethod;
import com.javabi.common.text.parser.IStringParser;

public interface ICommandParameter<T> {

	IMethod getSetter();

	String getName();

	Class<T> getType();

	IStringParser<T> getParser();

}
