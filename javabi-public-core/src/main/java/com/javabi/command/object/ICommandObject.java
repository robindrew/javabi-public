package com.javabi.command.object;

import java.util.List;

import com.javabi.command.IExecutableCommand;

public interface ICommandObject {

	String getName();

	Class<? extends IExecutableCommand<?>> getType();

	ICommandParameter<?> getParameter(String name);

	List<ICommandParameter<?>> getParameterList();

	IExecutableCommand<?> newInstance();

}
