package com.javabi.command.object;

import static com.javabi.common.lang.reflect.method.IMethodLister.Type.SETTER;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.javabi.command.IExecutableCommand;
import com.javabi.common.lang.clazz.Classes;
import com.javabi.common.lang.reflect.method.IMethod;
import com.javabi.common.lang.reflect.method.IMethodLister;
import com.javabi.common.lang.reflect.method.MethodLister;
import com.javabi.common.text.parser.IStringParserMap;

public class CommandObject implements ICommandObject {

	private final Map<String, ICommandParameter<?>> parameterMap = new LinkedHashMap<>();
	private final String name;
	private final Class<? extends IExecutableCommand<?>> type;

	@SuppressWarnings("rawtypes")
	public CommandObject(Class<? extends IExecutableCommand<?>> type, IStringParserMap parserMap) {
		this.name = type.getSimpleName();
		this.type = type;

		IMethodLister lister = new MethodLister();
		lister.setType(SETTER);
		List<IMethod> setterList = lister.getMethodList(type);
		for (IMethod setter : setterList) {
			ICommandParameter<?> parameter = new CommandParameter(setter, parserMap);
			parameterMap.put(parameter.getName(), parameter);
		}
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Class<? extends IExecutableCommand<?>> getType() {
		return type;
	}

	@Override
	public List<ICommandParameter<?>> getParameterList() {
		return new ArrayList<>(parameterMap.values());
	}

	@Override
	public ICommandParameter<?> getParameter(String name) {
		return parameterMap.get(name);
	}

	@Override
	public IExecutableCommand<?> newInstance() {
		return Classes.newInstance(type);
	}

}
