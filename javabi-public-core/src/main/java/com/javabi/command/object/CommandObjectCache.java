package com.javabi.command.object;

import java.util.LinkedHashMap;
import java.util.Map;

import com.javabi.command.IExecutableCommand;
import com.javabi.common.lang.clazz.ClassRegistry;
import com.javabi.common.text.parser.IStringParserMap;

public class CommandObjectCache implements ICommandObjectCache {

	private final IStringParserMap parserMap;
	private final Map<String, ICommandObject> commandMap = new LinkedHashMap<>();

	public CommandObjectCache(IStringParserMap parserMap) {
		if (parserMap == null) {
			throw new IllegalArgumentException("parserMap");
		}
		this.parserMap = parserMap;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void cache(ClassRegistry<? extends IExecutableCommand> registry) {
		for (Class type : registry) {
			cache(type);
		}
	}

	@Override
	public void cache(Class<? extends IExecutableCommand<?>> type) {
		ICommandObject object = new CommandObject(type, parserMap);
		commandMap.put(object.getName(), object);
	}

	@Override
	public ICommandObject getObject(String name) {
		ICommandObject object = commandMap.get(name);
		if (object == null) {
			throw new IllegalArgumentException("Unknown command: " + name);
		}
		return object;
	}

}
