package com.javabi.command.object;

import com.javabi.command.IExecutableCommand;
import com.javabi.common.lang.clazz.ClassRegistry;

public interface ICommandObjectCache {

	@SuppressWarnings("rawtypes")
	void cache(ClassRegistry<? extends IExecutableCommand> registry);

	void cache(Class<? extends IExecutableCommand<?>> type);

	ICommandObject getObject(String commandName);

}
