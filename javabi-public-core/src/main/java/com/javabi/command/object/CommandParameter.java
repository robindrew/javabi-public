package com.javabi.command.object;

import com.javabi.common.lang.reflect.method.IMethod;
import com.javabi.common.text.parser.IStringParser;
import com.javabi.common.text.parser.IStringParserMap;

public class CommandParameter<T> implements ICommandParameter<T> {

	private final IMethod setter;
	private final String name;
	private final Class<T> type;
	private final IStringParser<T> parser;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public CommandParameter(IMethod setter, IStringParser<T> parser) {
		this.setter = setter;
		this.name = setter.getFieldName();
		this.type = (Class) setter.getParameterTypes().get(0);
		this.parser = parser;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public CommandParameter(IMethod setter, IStringParserMap parserMap) {
		this.setter = setter;
		this.name = setter.getFieldName();
		this.type = (Class) setter.getParameterTypes().get(0);
		this.parser = parserMap.getParser(type);
	}

	public IMethod getSetter() {
		return setter;
	}

	public String getName() {
		return name;
	}

	public Class<T> getType() {
		return type;
	}

	@Override
	public IStringParser<T> getParser() {
		return parser;
	}

}
