package com.javabi.command.executor;

import com.javabi.common.io.connection.IConnection;
import com.javabi.server.connection.IConnectionHandler;
import com.javabi.server.connection.IConnectionHandlerFactory;

public class CommandStreamConnectionHandlerFactory implements IConnectionHandlerFactory {

	@Override
	public IConnectionHandler newConnection(IConnection connection, long number) {
		return new CommandStreamConnectionHandler(connection, number);
	}

}
