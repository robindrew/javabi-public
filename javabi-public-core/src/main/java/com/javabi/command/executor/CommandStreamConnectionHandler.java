package com.javabi.command.executor;

import static com.javabi.common.dependency.DependencyFactory.getDependency;
import static com.javabi.common.text.StringFormats.bytes;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.google.common.io.CountingInputStream;
import com.google.common.io.CountingOutputStream;
import com.javabi.command.ICommandRequestList;
import com.javabi.command.ICommandResponseList;
import com.javabi.command.IExecutableCommand;
import com.javabi.command.IExecutableCommandResponse;
import com.javabi.command.response.CommandResponseList;
import com.javabi.command.serializer.stream.ICommandStream;
import com.javabi.command.service.ICommandExecutorService;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.common.io.connection.IConnection;
import com.javabi.common.text.Trimmer;
import com.javabi.server.AbstractConnectionHandler;

public class CommandStreamConnectionHandler extends AbstractConnectionHandler {

	private static final Logger log = LoggerFactory.getLogger(CommandStreamConnectionHandler.class);

	private static final int WARN_LOG_SECONDS = 1;
	private static final int XML_MAXIMUM_LENGTH = 500;

	private static final long WARN_BYTES_WRITTEN = Short.MAX_VALUE;

	private final ICommandStream stream;

	public CommandStreamConnectionHandler(IConnection connection, long number) {
		super(connection, number);
		this.stream = getDependency(ICommandStream.class);
	}

	protected String trim(String xml) {
		return new Trimmer(XML_MAXIMUM_LENGTH, true).trim(xml);
	}

	@Override
	protected void handleConnection() throws IOException {
		CountingInputStream input = new CountingInputStream(getConnection().getInput());
		CountingOutputStream output = new CountingOutputStream(getConnection().getOutput());

		// Request
		ICommandRequestList requestList = stream.readRequestList(input);
		if (log.isDebugEnabled()) {
			String read = bytes(input.getCount());
			String xml = stream.toString(requestList);
			log.debug("[CommandList #" + getNumber() + "] " + read + " read\n" + xml);
		}

		// Execute
		NanoTimer timer = new NanoTimer();
		timer.start();
		ICommandResponseList responseList = execute(requestList);
		timer.stop();

		// Response
		if (log.isDebugEnabled()) {
			boolean debuggable = isDebuggable(requestList);
			if (debuggable) {
				String xml = stream.toString(responseList);
				log.debug("[CommandList #" + getNumber() + "] execution took " + timer + "\n" + trim(xml));
			} else {
				log.debug("[CommandList #" + getNumber() + "] execution took " + timer);
			}
		}

		timer = new NanoTimer();
		timer.start();
		stream.writeResponseList(responseList, output);
		timer.stop();

		// Warning - the command write took too long
		if (timer.hasExceeded(WARN_LOG_SECONDS, SECONDS) || output.getCount() > WARN_BYTES_WRITTEN) {
			String read = bytes(input.getCount());
			String written = bytes(output.getCount());
			String xml = stream.toString(requestList);
			log.warn("[CommandList #" + getNumber() + "] " + read + " read, " + written + " bytes written in " + timer + "\n" + xml);
		}
	}

	private boolean isDebuggable(ICommandRequestList requestList) {
		for (IExecutableCommand<?> command : requestList) {
			if (!command.isDebuggable()) {
				return false;
			}
		}
		return true;
	}

	private ICommandResponseList execute(ICommandRequestList requestList) {
		CommandResponseList responseList = new CommandResponseList();
		for (IExecutableCommand<?> request : requestList) {
			IExecutableCommandResponse<?> response = execute(request);
			responseList.add(response);
		}
		return responseList;
	}

	private IExecutableCommandResponse<?> execute(IExecutableCommand<?> request) {
		ICommandExecutorService service = getDependency(ICommandExecutorService.class);

		NanoTimer timer = new NanoTimer();
		timer.start();
		final IExecutableCommandResponse<?> response;
		try {

			// Execute the command
			response = service.executeCommand(request);
			if (response.hasErrors()) {
				log.warn("[Request #" + getNumber() + "] has errors: " + response.getErrors() + "\n" + stream.toString(request));
			}

		} catch (Throwable t) {

			// Error - the command failed
			timer.stop();
			log.error("[Request #" + getNumber() + "] failed after " + timer + "\n" + stream.toString(request), t);

			throw Throwables.propagate(t);
		}
		timer.stop();

		// Warning - the command execution took too long
		if (timer.hasExceeded(WARN_LOG_SECONDS, SECONDS)) {
			log.warn("[Request #" + getNumber() + "] took " + timer + "\n" + stream.toString(request));
			if (request.isDebuggable()) {
				log.warn("[Response #" + getNumber() + "] took " + timer + ")\n" + stream.toString(response));
			}
		}
		return response;
	}
}
