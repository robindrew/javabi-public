package com.javabi.command.errorcode.validator;

import static com.javabi.command.errorcode.ErrorCodeException.error;

import java.util.regex.Pattern;

import com.javabi.command.errorcode.IErrorCode;

public class StringValidator extends ErrorCodeValdiator<String> {

	private boolean autoTrim = false;
	private int minimumLength;
	private int maximumLength;
	private Pattern pattern;

	private IErrorCode notNullError = null;
	private IErrorCode minimumLengthError = null;
	private IErrorCode maximumLengthError = null;
	private IErrorCode patternError = null;

	@Override
	public String validate(String value) {

		// Check if value is and can be null
		if (value == null) {
			if (notNullError != null) {
				error(minimumLengthError);
			}
			return value;
		}

		// Auto trim?
		if (autoTrim) {
			value = value.trim();
		}

		// Check minimum length
		if (minimumLengthError != null) {
			if (value.length() < minimumLength) {
				error(minimumLengthError);
			}
		}

		// Check maximum length
		if (maximumLengthError != null) {
			if (value.length() > maximumLength) {
				error(maximumLengthError);
			}
		}

		// Check pattern
		if (patternError != null) {
			if (!pattern.matcher(value).matches()) {
				error(patternError);
			}
		}

		return value;
	}

	public StringValidator setAutoTrim(boolean trim) {
		this.autoTrim = trim;
		return this;
	}

	public StringValidator setNotNull(IErrorCode error) {
		if (error == null) {
			throw new NullPointerException("error");
		}
		this.notNullError = error;
		return this;
	}

	public StringValidator setMinimumLength(int length, IErrorCode error) {
		if (error == null) {
			throw new NullPointerException("error");
		}
		if (length < 1) {
			throw new IllegalArgumentException("length=" + length);
		}
		this.minimumLength = length;
		this.minimumLengthError = error;
		return this;
	}

	public StringValidator setMaximumLength(int length, IErrorCode error) {
		if (error == null) {
			throw new NullPointerException("error");
		}
		if (length < 1) {
			throw new IllegalArgumentException("length=" + length);
		}
		this.maximumLength = length;
		this.maximumLengthError = error;
		return this;
	}

	public StringValidator setPattern(String pattern, IErrorCode error) {
		if (error == null) {
			throw new NullPointerException("error");
		}
		this.pattern = Pattern.compile(pattern);
		this.patternError = error;
		return this;
	}
}
