package com.javabi.command.errorcode.gwt;

import com.google.common.annotations.GwtCompatible;
import com.javabi.command.errorcode.ErrorCodeSet;
import com.javabi.command.errorcode.IErrorCode;
import com.javabi.command.errorcode.IErrorCodeException;
import com.javabi.command.errorcode.IErrorCodeSet;
import com.javabi.command.errorcode.IErrorCodeSetContainer;

@GwtCompatible
public class GwtErrorCodeException extends Exception implements IErrorCodeException {

	private static final long serialVersionUID = 530927858169252071L;

	private IErrorCodeSet errors;

	public GwtErrorCodeException(IErrorCode error) {
		super(error.name());
		this.errors = new ErrorCodeSet();
		this.errors.add(error);
	}

	public GwtErrorCodeException(IErrorCodeSet errors) {
		super(errors.getFirstError().name());
		this.errors = errors;
	}

	public GwtErrorCodeException(IErrorCodeSetContainer container) {
		this(container.getErrors());
	}

	public GwtErrorCodeException() {
	}

	public IErrorCodeSet getErrors() {
		return errors;
	}

}
