package com.javabi.command.errorcode.validator;

public interface IErrorCodeValidator<V> {

	V validate(V value);

}
