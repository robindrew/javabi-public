package com.javabi.command.errorcode;

import java.io.Serializable;

/**
 * The I Validator Error Code.
 */
public interface IErrorCode extends Serializable {

	String name();
}