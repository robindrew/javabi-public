package com.javabi.command.errorcode.data;

import com.javabi.command.errorcode.IErrorCodeSet;
import com.javabi.common.io.data.IDataSerializer;

public interface IErrorCodeSetSerializer extends IDataSerializer<IErrorCodeSet> {

}
