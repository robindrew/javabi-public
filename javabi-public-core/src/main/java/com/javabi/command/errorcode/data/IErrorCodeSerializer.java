package com.javabi.command.errorcode.data;

import com.javabi.command.errorcode.IErrorCode;
import com.javabi.common.io.data.IDataSerializer;

public interface IErrorCodeSerializer extends IDataSerializer<IErrorCode> {

}
