package com.javabi.command.errorcode;

import java.io.Serializable;
import java.util.Set;

public interface IErrorCodeAttributes extends Serializable {

	int size();

	IErrorCodeAttributes set(String key, String value);

	Set<String> keySet();

	String getValue(String key);

}
