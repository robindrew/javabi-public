package com.javabi.command.errorcode;

import java.io.Serializable;

public interface IErrorCodeSetContainer extends Serializable {

	IErrorCodeSet getErrors();

}
