package com.javabi.command.errorcode;

import java.io.Serializable;

public interface IErrorCodeException extends IErrorCodeSetContainer, Serializable {

}
