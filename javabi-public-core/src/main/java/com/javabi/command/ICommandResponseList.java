package com.javabi.command;

import java.io.Serializable;

@SuppressWarnings("rawtypes")
public interface ICommandResponseList extends Iterable<IExecutableCommandResponse>, Serializable {

	int size();

	IExecutableCommandResponse get(int index);

	<R> IExecutableCommandResponse<R> getResponse(IExecutableCommand<R> request);

	void setRequestList(ICommandRequestList requestList);

}
