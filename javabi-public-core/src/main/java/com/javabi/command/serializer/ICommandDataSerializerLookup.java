package com.javabi.command.serializer;

public interface ICommandDataSerializerLookup {

	ICommandDataSerializer<?> getSerializer(short commandId);
}
