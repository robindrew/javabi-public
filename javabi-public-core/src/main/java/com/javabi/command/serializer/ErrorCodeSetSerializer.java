package com.javabi.command.serializer;

import static com.javabi.common.dependency.DependencyFactory.getDependency;

import java.io.IOException;
import java.util.Set;

import com.javabi.command.errorcode.ErrorCodeSet;
import com.javabi.command.errorcode.IErrorCode;
import com.javabi.command.errorcode.IErrorCodeSet;
import com.javabi.command.errorcode.data.IErrorCodeSerializer;
import com.javabi.command.errorcode.data.IErrorCodeSetSerializer;
import com.javabi.common.io.data.IDataReader;
import com.javabi.common.io.data.IDataWriter;

public class ErrorCodeSetSerializer implements IErrorCodeSetSerializer {

	private IErrorCodeSerializer serializer = getDependency(IErrorCodeSerializer.class);

	@Override
	public void writeObject(IDataWriter writer, IErrorCodeSet errors) throws IOException {
		Set<IErrorCode> set = errors.getCodeSet();
		int size = set.size();
		writer.writePositiveInt(size);

		for (IErrorCode error : set) {
			serializer.writeObject(writer, error);
		}
	}

	@Override
	public IErrorCodeSet readObject(IDataReader reader) throws IOException {
		int size = reader.readPositiveInt();
		IErrorCodeSet errors = new ErrorCodeSet();
		for (int i = 0; i < size; i++) {
			IErrorCode error = serializer.readObject(reader);
			errors.add(error);
		}
		return errors;
	}

}
