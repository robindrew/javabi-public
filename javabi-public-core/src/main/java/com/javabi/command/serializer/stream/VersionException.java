package com.javabi.command.serializer.stream;


public class VersionException extends RuntimeException {

	public VersionException(String message, Throwable t) {
		super(message, t);
	}

}
