package com.javabi.command.serializer.stream;

import java.io.InputStream;
import java.io.OutputStream;

import com.javabi.command.ICommandRequestList;
import com.javabi.command.ICommandResponseList;
import com.javabi.command.IExecutableCommand;
import com.javabi.command.IExecutableCommandResponse;

public interface ICommandStream {

	String toString(IExecutableCommand<?> request);

	String toString(IExecutableCommandResponse<?> response);

	String toString(ICommandRequestList requestList);

	String toString(ICommandResponseList responseList);

	void writeRequestList(ICommandRequestList requestList, OutputStream output);

	void writeResponseList(ICommandResponseList responseList, OutputStream output);

	ICommandResponseList readResponseList(InputStream input);

	ICommandRequestList readRequestList(InputStream input);

}
