package com.javabi.command.serializer;

import java.io.IOException;

import com.javabi.command.IExecutableCommand;
import com.javabi.command.IExecutableCommandResponse;
import com.javabi.common.io.data.IDataReader;
import com.javabi.common.io.data.IDataWriter;

public interface ICommandDataSerializer<R> {

	short getCommandId();

	R readReturnValue(IDataReader reader) throws IOException;

	IExecutableCommandResponse<R> readResponse(IDataReader reader) throws IOException;

	IExecutableCommand<R> readRequest(IDataReader reader) throws IOException;

	void writeReturnValue(IDataWriter writer, R returnValue) throws IOException;

	void writeRequest(IDataWriter writer, IExecutableCommand<R> request) throws IOException;

	void writeResponse(IDataWriter writer, IExecutableCommandResponse<R> response) throws IOException;

}
