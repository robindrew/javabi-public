package com.javabi.command.serializer.stream;

import java.io.InputStream;
import java.io.OutputStream;

import com.javabi.command.ICommandRequestList;
import com.javabi.command.ICommandResponseList;
import com.javabi.command.IExecutableCommand;
import com.javabi.command.IExecutableCommandResponse;
import com.javabi.command.errorcode.ErrorCodeAttributes;
import com.javabi.command.errorcode.ErrorCodeSet;
import com.javabi.command.response.CommandRequestList;
import com.javabi.command.response.CommandResponseList;
import com.javabi.common.xml.xstream.ConcurrentXStream;
import com.javabi.common.xml.xstream.IXStream;

public class CommandXStream implements ICommandStream {

	private final IXStream xstream;

	public CommandXStream() {
		xstream = new ConcurrentXStream();
		xstream.alias(CommandResponseList.class);
		xstream.alias(CommandRequestList.class);
		xstream.alias(ErrorCodeSet.class);
		xstream.alias(ErrorCodeAttributes.class);
	}

	@Override
	public void writeRequestList(ICommandRequestList requestList, OutputStream output) {
		xstream.toXml(requestList, output);
	}

	@Override
	public ICommandResponseList readResponseList(InputStream input) {
		return (ICommandResponseList) xstream.fromXml(input);
	}

	@Override
	public void writeResponseList(ICommandResponseList responseList, OutputStream output) {
		xstream.toXml(responseList, output);
	}

	@Override
	public ICommandRequestList readRequestList(InputStream input) {
		return (ICommandRequestList) xstream.fromXml(input);
	}

	@Override
	public String toString(ICommandRequestList requestList) {
		return xstream.toXml(requestList);
	}

	@Override
	public String toString(ICommandResponseList responseList) {
		return xstream.toXml(responseList);
	}

	@Override
	public String toString(IExecutableCommand<?> request) {
		return xstream.toXml(request);
	}

	@Override
	public String toString(IExecutableCommandResponse<?> response) {
		return xstream.toXml(response);
	}

}
