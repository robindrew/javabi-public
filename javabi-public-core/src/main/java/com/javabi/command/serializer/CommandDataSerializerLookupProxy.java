package com.javabi.command.serializer;

import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class CommandDataSerializerLookupProxy implements ICommandDataSerializerLookup {

	private static final Logger log = LoggerFactory.getLogger(CommandDataSerializerLookupProxy.class);

	private final Map<Short, ICommandDataSerializerLookup> commandIdToLookupMap = Maps.newConcurrentMap();
	private final Set<ICommandDataSerializerLookup> lookupSet = Sets.newCopyOnWriteArraySet();

	public void add(ICommandDataSerializerLookup lookup) {
		lookupSet.add(lookup);
	}

	@Override
	public ICommandDataSerializer<?> getSerializer(short commandId) {
		if (lookupSet.isEmpty()) {
			throw new IllegalStateException("Proxy not initialised");
		}

		// Already mapped?
		ICommandDataSerializerLookup lookup = commandIdToLookupMap.get(commandId);
		if (lookup != null) {
			return lookup.getSerializer(commandId);
		}

		// Find the correct lookup
		return findSerializer(commandId);
	}

	private ICommandDataSerializer<?> findSerializer(short commandId) {
		for (ICommandDataSerializerLookup lookup : lookupSet) {
			try {
				ICommandDataSerializer<?> serializer = lookup.getSerializer(commandId);
				if (serializer != null) {
					commandIdToLookupMap.put(commandId, lookup);
					return serializer;
				}
			} catch (Exception e) {
				log.info("lookup: " + lookup + " does not handle command " + commandId + " (" + e.getMessage() + ")");
			}
		}
		throw new IllegalArgumentException("no lookups are able to handle commandId=" + commandId);
	}
}
