package com.javabi.command.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.google.common.annotations.GwtCompatible;
import com.javabi.command.ICommandRequestList;
import com.javabi.command.IExecutableCommand;

@GwtCompatible
public class CommandRequestList implements ICommandRequestList, Serializable {

	private static final long serialVersionUID = 6768090821964845707L;

	private List<IExecutableCommand<?>> requestList = new ArrayList<IExecutableCommand<?>>();

	public CommandRequestList() {
	}

	public CommandRequestList(IExecutableCommand<?> command) {
		add(command);
	}

	public CommandRequestList(IExecutableCommand<?>... commands) {
		addAll(commands);
	}

	public CommandRequestList(Collection<IExecutableCommand<?>> commands) {
		addAll(commands);
	}

	public void add(IExecutableCommand<?> command) {
		if (command == null) {
			throw new NullPointerException("command");
		}
		requestList.add(command);
	}

	public void addAll(IExecutableCommand<?>... commands) {
		for (IExecutableCommand<?> command : commands) {
			add(command);
		}
	}

	public void addAll(Collection<IExecutableCommand<?>> commands) {
		for (IExecutableCommand<?> command : commands) {
			add(command);
		}
	}

	@Override
	public Iterator<IExecutableCommand<?>> iterator() {
		return requestList.iterator();
	}

	@Override
	public int size() {
		return requestList.size();
	}

	@Override
	@SuppressWarnings("unchecked")
	public <R> IExecutableCommand<R> get(int index) {
		return (IExecutableCommand<R>) requestList.get(index);
	}

	@Override
	public int indexOf(IExecutableCommand<?> command) {
		return requestList.indexOf(command);
	}

	@Override
	public String toString() {
		ToStringBuilder string = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
		string.append("requestList", requestList);
		return string.toString();
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object instanceof CommandRequestList) {
			CommandRequestList compare = (CommandRequestList) object;
			EqualsBuilder equals = new EqualsBuilder();
			equals.append(this.requestList, compare.requestList);
			return equals.isEquals();
		}
		return false;
	}

}
