package com.javabi.command.response;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.google.common.annotations.GwtCompatible;
import com.javabi.command.IExecutableCommandResponse;
import com.javabi.command.errorcode.IErrorCodeSet;

@GwtCompatible
public abstract class AbstractCommandResponse<R> implements IExecutableCommandResponse<R>, Serializable {

	private static final long serialVersionUID = -5980768958647074106L;

	private short commandId = -1;
	private IErrorCodeSet errors = null;

	public AbstractCommandResponse() {
	}

	public AbstractCommandResponse(short commandId) {
		if (commandId < 0) {
			throw new IllegalArgumentException("commandId=" + commandId);
		}
		this.commandId = commandId;
	}

	public AbstractCommandResponse(short commandId, IErrorCodeSet errors) {
		if (commandId < 0) {
			throw new IllegalArgumentException("commandId=" + commandId);
		}
		if (errors == null) {
			throw new NullPointerException("errors");
		}
		if (errors.isEmpty()) {
			throw new IllegalArgumentException("errors is empty");
		}
		this.commandId = commandId;
		this.errors = errors;
	}

	@Override
	public short getCommandId() {
		return commandId;
	}

	@Override
	public void setCommandId(short commandId) {
		if (commandId < 0) {
			throw new IllegalArgumentException("commandId=" + commandId);
		}
		this.commandId = commandId;
	}

	@Override
	public void setErrors(IErrorCodeSet errors) {
		this.errors = errors;
	}

	@Override
	public IErrorCodeSet getErrors() {
		return errors;
	}

	@Override
	public boolean hasErrors() {
		return getErrors() != null;
	}

	@Override
	public String toString() {
		ToStringBuilder string = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
		string.append("commandId", commandId);
		if (getReturnValue() != null) {
			string.append("returnValue", getReturnValue());
		}
		if (getErrors() != null) {
			string.append("errors", getErrors());
		}
		return string.toString();
	}

	@Override
	@SuppressWarnings("rawtypes")
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object instanceof AbstractCommandResponse) {
			AbstractCommandResponse compare = (AbstractCommandResponse) object;
			EqualsBuilder equals = new EqualsBuilder();
			equals.append(this.getCommandId(), compare.getCommandId());
			equals.append(this.getReturnValue(), compare.getReturnValue());
			equals.append(this.getErrors(), compare.getErrors());
			return equals.isEquals();
		}
		return false;
	}

}
