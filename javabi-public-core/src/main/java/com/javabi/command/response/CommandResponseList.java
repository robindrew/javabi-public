package com.javabi.command.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.google.common.annotations.GwtCompatible;
import com.javabi.command.ICommandRequestList;
import com.javabi.command.ICommandResponseList;
import com.javabi.command.IExecutableCommand;
import com.javabi.command.IExecutableCommandResponse;

@GwtCompatible
@SuppressWarnings("rawtypes")
public class CommandResponseList implements ICommandResponseList, Serializable {

	private static final long serialVersionUID = -1961489586530247553L;

	private List<IExecutableCommandResponse> responseList = new ArrayList<IExecutableCommandResponse>();
	private ICommandRequestList requestList = null;

	public CommandResponseList() {
	}

	public CommandResponseList(IExecutableCommandResponse<?> command) {
		add(command);
	}

	public CommandResponseList(IExecutableCommandResponse<?>... commands) {
		addAll(commands);
	}

	public CommandResponseList(Collection<IExecutableCommandResponse<?>> commands) {
		addAll(commands);
	}

	public void add(IExecutableCommandResponse<?> command) {
		if (command == null) {
			throw new NullPointerException("command");
		}
		responseList.add(command);
	}

	public void addAll(IExecutableCommandResponse<?>... commands) {
		for (IExecutableCommandResponse<?> command : commands) {
			add(command);
		}
	}

	public void addAll(Collection<IExecutableCommandResponse<?>> commands) {
		for (IExecutableCommandResponse<?> command : commands) {
			add(command);
		}
	}

	@Override
	public void setRequestList(ICommandRequestList requestList) {
		this.requestList = requestList;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <R> IExecutableCommandResponse<R> getResponse(IExecutableCommand<R> request) {
		int index = indexOf(request);
		if (index == -1) {
			throw new IllegalStateException("request not found in responseList: " + request);
		}
		return responseList.get(index);
	}

	private <R> int indexOf(IExecutableCommand<R> request) {
		if (requestList == null) {
			throw new IllegalStateException("requestList not set");
		}
		for (int i = 0; i < requestList.size(); i++) {
			if (request == requestList.get(i)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public Iterator<IExecutableCommandResponse> iterator() {
		return responseList.iterator();
	}

	@Override
	public int size() {
		return responseList.size();
	}

	@Override
	public IExecutableCommandResponse get(int index) {
		return responseList.get(index);
	}

	@Override
	public String toString() {
		ToStringBuilder string = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
		string.append("responseList", responseList);
		return string.toString();
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object instanceof CommandResponseList) {
			CommandResponseList compare = (CommandResponseList) object;
			EqualsBuilder equals = new EqualsBuilder();
			equals.append(this.responseList, compare.responseList);
			equals.append(this.requestList, compare.requestList);
			return equals.isEquals();
		}
		return false;
	}

}
