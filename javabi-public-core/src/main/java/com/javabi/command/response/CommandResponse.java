package com.javabi.command.response;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.google.common.annotations.GwtCompatible;
import com.javabi.command.IExecutableCommandResponse;
import com.javabi.command.errorcode.IErrorCodeSet;

@GwtCompatible
public class CommandResponse<R> implements IExecutableCommandResponse<R>, Serializable {

	private static final long serialVersionUID = -5980768958647074106L;

	private short commandId = -1;
	private R returnValue = null;
	private IErrorCodeSet errors = null;

	public CommandResponse() {
	}

	public CommandResponse(short commandId, R returnValue) {
		if (commandId < 0) {
			throw new IllegalArgumentException("commandId=" + commandId);
		}
		this.commandId = commandId;
		this.returnValue = returnValue;
	}

	public CommandResponse(short commandId, IErrorCodeSet errors) {
		if (commandId < 0) {
			throw new IllegalArgumentException("commandId=" + commandId);
		}
		if (errors == null) {
			throw new NullPointerException("errors");
		}
		if (errors.isEmpty()) {
			throw new IllegalArgumentException("errors is empty");
		}
		this.commandId = commandId;
		this.errors = errors;
	}

	@Override
	public short getCommandId() {
		return commandId;
	}

	public void setCommandId(short commandId) {
		if (commandId < 0) {
			throw new IllegalArgumentException("commandId=" + commandId);
		}
		this.commandId = commandId;
	}

	public void setReturnValue(R returnValue) {
		this.returnValue = returnValue;
	}

	public void setErrors(IErrorCodeSet errors) {
		this.errors = errors;
	}

	@Override
	public R getReturnValue() {
		return returnValue;
	}

	@Override
	public IErrorCodeSet getErrors() {
		return errors;
	}

	@Override
	public boolean hasErrors() {
		return errors != null;
	}

	@Override
	public String toString() {
		ToStringBuilder string = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
		string.append("commandId", commandId);
		if (returnValue != null) {
			string.append("returnValue", returnValue);
		}
		if (errors != null) {
			string.append("errors", errors);
		}
		return string.toString();
	}

	@Override
	@SuppressWarnings("rawtypes")
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object instanceof CommandResponse) {
			CommandResponse compare = (CommandResponse) object;
			EqualsBuilder equals = new EqualsBuilder();
			equals.append(this.commandId, compare.commandId);
			equals.append(this.returnValue, compare.returnValue);
			equals.append(this.errors, compare.errors);
			return equals.isEquals();
		}
		return false;
	}

}
