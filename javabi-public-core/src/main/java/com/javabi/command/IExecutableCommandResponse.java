package com.javabi.command;

import java.io.Serializable;

import com.javabi.command.errorcode.IErrorCodeSet;
import com.javabi.command.errorcode.IErrorCodeSetContainer;

/**
 * An Executable Command Response.
 * @param <R> the return type.
 */
public interface IExecutableCommandResponse<R> extends IErrorCodeSetContainer, Serializable {

	short getCommandId();

	R getReturnValue();

	boolean hasErrors();

	void setCommandId(short commandId);

	void setReturnValue(R returnValue);

	void setErrors(IErrorCodeSet errors);

}
