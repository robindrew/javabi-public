package com.javabi.command;

import java.io.Serializable;

public interface ICommandRequestList extends Iterable<IExecutableCommand<?>>, Serializable {

	int size();

	int indexOf(IExecutableCommand<?> command);

	<R> IExecutableCommand<R> get(int index);

}
