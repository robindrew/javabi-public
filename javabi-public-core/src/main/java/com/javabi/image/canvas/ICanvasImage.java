package com.javabi.image.canvas;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import com.javabi.common.image.ImageFormat;

public interface ICanvasImage {

	BufferedImage getBufferedImage();

	Graphics2D getGraphics();

	int getWidth();

	int getHeight();

	ICanvasImage copy();

	ICanvasImage crop(int x, int y, int width, int height);

	ICanvasImage resize(int width, int height);

	boolean ensureSize(int maxWidth, int maxHeight);

	ICanvasImage greyscale();

	ImageFormat getImageFormat();

	void drawRectangle(Color color, int x, int y, int width, int height);

	void drawRectangle(Color color, int x, int y, int width, int height, float alpha);

	void drawImage(ICanvasImage image, int x, int y);

	void dispose();

	byte[] toByteArray(ImageFormat format, float quality);

	byte[] toByteArray(ImageFormat format);

	void writeToFile(File file);

	void writeToFile(File file, ImageFormat format);

	void writeToFile(File file, ImageFormat format, float quality);

	boolean hasAlpha();

}
