package com.javabi.image.canvas;

import static java.awt.RenderingHints.KEY_ANTIALIASING;
import static java.awt.RenderingHints.KEY_INTERPOLATION;
import static java.awt.RenderingHints.KEY_RENDERING;
import static java.awt.RenderingHints.VALUE_ANTIALIAS_ON;
import static java.awt.RenderingHints.VALUE_INTERPOLATION_BILINEAR;
import static java.awt.RenderingHints.VALUE_RENDER_QUALITY;
import static java.awt.image.BufferedImage.TYPE_3BYTE_BGR;
import static java.awt.image.BufferedImage.TYPE_4BYTE_ABGR;
import static java.awt.image.BufferedImage.TYPE_4BYTE_ABGR_PRE;
import static java.awt.image.BufferedImage.TYPE_BYTE_BINARY;
import static java.awt.image.BufferedImage.TYPE_BYTE_GRAY;
import static java.awt.image.BufferedImage.TYPE_BYTE_INDEXED;
import static java.awt.image.BufferedImage.TYPE_CUSTOM;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import static java.awt.image.BufferedImage.TYPE_INT_BGR;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;

import com.javabi.common.image.ImageFormat;
import com.javabi.image.graphics.Graphics;

public abstract class AbstractCanvasImage implements ICanvasImage {

	protected AlphaComposite getComposite(float alpha) {
		int type = AlphaComposite.SRC_OVER;
		return (AlphaComposite.getInstance(type, alpha));
	}

	protected Graphics2D createGraphics(BufferedImage image) {
		Graphics2D graphics = image.createGraphics();
		// For resizing
		graphics.setRenderingHint(KEY_INTERPOLATION, VALUE_INTERPOLATION_BILINEAR);
		// Does anything?
		graphics.setRenderingHint(KEY_RENDERING, VALUE_RENDER_QUALITY);
		// Does anything?
		graphics.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_ON);
		return graphics;
	}

	protected ImageFormat getImageFormat(int type) {
		switch (type) {
			case TYPE_BYTE_GRAY:
			case TYPE_BYTE_BINARY:
			case TYPE_INT_BGR:
			case TYPE_3BYTE_BGR:
			case TYPE_INT_RGB:
				return ImageFormat.JPG;
			case TYPE_4BYTE_ABGR:
			case TYPE_4BYTE_ABGR_PRE:
			case TYPE_INT_ARGB:
			case TYPE_BYTE_INDEXED:
				return ImageFormat.PNG;
			default:
				throw new IllegalStateException("Type not mapped: " + type);
		}
	}

	protected int getType(BufferedImage image) {
		int type = image.getType();
		// We need to translate the unknown type (custom)
		if (type == TYPE_CUSTOM) {
			return TYPE_INT_ARGB;
		}
		return type;
	}

	@Override
	public void drawRectangle(Color color, int x, int y, int width, int height) {
		Graphics2D graphics = getGraphics();

		// Mark
		Color oldColor = graphics.getColor();

		// Draw
		graphics.setColor(color);
		graphics.fillRect(x, y, width, height);

		// Reset
		graphics.setColor(oldColor);
	}

	@Override
	public void drawRectangle(Color color, int x, int y, int width, int height, float alpha) {
		Graphics2D graphics = getGraphics();

		// Mark
		Composite oldComposite = graphics.getComposite();
		Color oldColor = graphics.getColor();

		// Draw
		graphics.setComposite(getComposite(alpha));
		graphics.setColor(color);
		graphics.fillRect(x, y, width, height);

		// Reset
		graphics.setComposite(oldComposite);
		graphics.setColor(oldColor);
	}

	@Override
	public void drawImage(ICanvasImage image, int x, int y) {
		Graphics2D graphics = getGraphics();
		graphics.drawImage(image.getBufferedImage(), x, y, null);
	}

	@Override
	public boolean hasAlpha() {
		int type = getType(getBufferedImage());
		switch (type) {
			case TYPE_4BYTE_ABGR:
			case TYPE_4BYTE_ABGR_PRE:
			case TYPE_INT_ARGB:
			case TYPE_BYTE_INDEXED:
				return true;
			default:
				return false;
		}
	}

	@Override
	public boolean ensureSize(int maxWidth, int maxHeight) {
		int width = getWidth();
		int height = getHeight();

		// Already within bounds?
		if (width <= maxWidth && height <= maxHeight) {
			return false;
		}

		// Resize
		double widthFactor = (double) width / (double) maxWidth;
		double heightFactor = (double) height / (double) maxHeight;
		if (widthFactor > heightFactor) {
			height = height * maxWidth / width;
			width = maxWidth;
		} else {
			width = width * maxHeight / height;
			height = maxHeight;
		}
		resize(width, height);
		return true;
	}

	@Override
	public ImageFormat getImageFormat() {
		return getImageFormat(getType(getBufferedImage()));
	}

	@Override
	public ICanvasImage greyscale() {
		if (hasAlpha()) {
			return getGreyScaleAlpha();
		}
		int width = getWidth();
		int height = getHeight();
		BufferedImage greyImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		new Graphics(greyImage).drawImage(getBufferedImage()).dispose();
		return new CanvasImage(greyImage);
	}

	private ICanvasImage getGreyScaleAlpha() {

		// Create new image
		int width = getWidth();
		int height = getHeight();
		BufferedImage greyImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics graphics = new Graphics(greyImage);
		graphics.drawImage(getBufferedImage());
		graphics.dispose();

		// Grey scale pixels
		final Raster raster = greyImage.getData();
		final DataBuffer buffer = raster.getDataBuffer();
		final int[] argb = new int[4];
		for (int i = 0; i < buffer.getSize(); i++) {
			int pixel = buffer.getElem(i);
			pixel = getGreyScalePixel(pixel, argb);
			buffer.setElem(i, pixel);
		}
		greyImage.setData(raster);
		return new CanvasImage(greyImage);
	}

	private int getGreyScalePixel(int pixel, int[] argb) {
		argb[0] = (pixel & 0xFF000000) >>> 24; // Alpha
		argb[1] = (pixel & 0x00FF0000) >>> 16; // Red
		argb[2] = (pixel & 0x0000FF00) >>> 8; // Green
		argb[3] = (pixel & 0x000000FF); // Blue

		int grey = (argb[1] / 3) + (argb[2] / 3) + (argb[3] / 3);
		// a little bit brighter
		// grey = grey / 2 + 127;

		argb[1] = grey;
		argb[2] = grey;
		argb[3] = grey;

		return (argb[0] << 24 | argb[1] << 16 | argb[2] << 8 | argb[3]);
	}

}
