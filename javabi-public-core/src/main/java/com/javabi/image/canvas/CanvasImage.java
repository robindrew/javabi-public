package com.javabi.image.canvas;

import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;

import com.google.common.base.Throwables;
import com.javabi.common.image.IImageOutput;
import com.javabi.common.image.ImageFormat;
import com.javabi.common.image.ImageOutput;
import com.javabi.image.buffered.BufferedImageReader;

public class CanvasImage extends AbstractCanvasImage {

	public static ICanvasImage readFromFile(File file) {
		try {
			BufferedImage image = new BufferedImageReader().readFromFile(file);
			return new CanvasImage(image);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	public static ICanvasImage readFromStream(InputStream input) {
		try {
			BufferedImage image = new BufferedImageReader().readFromStream(input);
			return new CanvasImage(image);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	private final BufferedImage image;
	private Graphics2D graphics = null;

	public CanvasImage(BufferedImage image) {
		if (image == null) {
			throw new NullPointerException("image");
		}
		this.image = image;
	}

	public CanvasImage(int width, int height) {
		this(width, height, false);
	}

	public CanvasImage(int width, int height, boolean alpha) {
		if (width < 0) {
			throw new IllegalArgumentException("width=" + width);
		}
		if (height < 0) {
			throw new IllegalArgumentException("height=" + height);
		}
		this.image = new BufferedImage(width, height, alpha ? TYPE_INT_ARGB : TYPE_INT_RGB);
	}

	@Override
	public BufferedImage getBufferedImage() {
		return image;
	}

	@Override
	public int getWidth() {
		return image.getWidth();
	}

	@Override
	public int getHeight() {
		return image.getHeight();
	}

	@Override
	public Graphics2D getGraphics() {
		if (graphics == null) {
			graphics = createGraphics(image);
		}
		return graphics;
	}

	@Override
	public ICanvasImage crop(int x, int y, int width, int height) {
		if (getWidth() == width && getHeight() == height) {
			return copy();
		}
		BufferedImage newImage = new BufferedImage(width, height, getType(getBufferedImage()));
		Graphics2D graphics = createGraphics(newImage);
		graphics.drawImage(getBufferedImage(), -x, -y, null);
		return new CanvasImage(newImage);
	}

	@Override
	public ICanvasImage resize(int width, int height) {
		// NOTE: This method uses an algorithm that produces
		// much better scaling but at a cost on performance

		// Examine old image
		BufferedImage oldImage = getBufferedImage();
		int type = getType(oldImage);
		int oldWidth = oldImage.getWidth();
		int oldHeight = oldImage.getHeight();

		// Same size, just do straight copy
		if (oldWidth == width && oldHeight == height) {
			return copy();
		}

		while (true) {

			// Calculate new dimensions (half each time)
			int newWidth = oldWidth / 2;
			if (newWidth < width) {
				newWidth = width;
			}
			int newHeight = oldHeight / 2;
			if (newHeight < height) {
				newHeight = height;
			}

			boolean finished = (newWidth == width && newHeight == height);

			// Resize
			BufferedImage newImage = new BufferedImage(newWidth, newHeight, type);
			Graphics2D graphics = createGraphics(newImage);
			graphics.drawImage(oldImage, 0, 0, newWidth, newHeight, null);
			graphics.dispose();

			// Finished?
			if (finished) {
				return new CanvasImage(newImage);
			}

			oldImage = newImage;
			oldWidth = newImage.getWidth();
			oldHeight = newImage.getHeight();
		}
	}

	@Override
	public ICanvasImage copy() {
		BufferedImage newImage = new BufferedImage(getWidth(), getHeight(), getType(getBufferedImage()));
		Graphics2D graphics = createGraphics(newImage);
		graphics.drawImage(getBufferedImage(), 0, 0, null);
		return new CanvasImage(newImage);
	}

	@Override
	public void dispose() {
		if (graphics != null) {
			Graphics2D dispose = graphics;
			graphics = null;
			dispose.dispose();
		}
	}

	@Override
	public byte[] toByteArray(ImageFormat format, float quality) {
		IImageOutput output = new ImageOutput(getBufferedImage(), format, quality);
		return output.writeToByteArray();
	}

	@Override
	public byte[] toByteArray(ImageFormat format) {
		IImageOutput output = new ImageOutput(getBufferedImage(), format);
		return output.writeToByteArray();
	}

	@Override
	public void writeToFile(File file) {
		int type = getType(getBufferedImage());
		ImageFormat format = getImageFormat(type);
		writeToFile(file, format);
	}

	@Override
	public void writeToFile(File file, ImageFormat format) {
		IImageOutput output = new ImageOutput(getBufferedImage(), format);
		output.writeToFile(file);
	}

	@Override
	public void writeToFile(File file, ImageFormat format, float quality) {
		IImageOutput output = new ImageOutput(getBufferedImage(), format, quality);
		output.writeToFile(file);
	}

}
