package com.javabi.image.canvas;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.google.common.base.Throwables;
import com.javabi.common.image.ImageFormat;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;

public class ImageJCanvasImage extends AbstractCanvasImage {

	public static ICanvasImage readFromFile(File file) {
		try {
			ImagePlus image = IJ.openImage(file.getAbsolutePath());
			return new ImageJCanvasImage(image);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	private final ImagePlus image;
	private Graphics2D graphics = null;

	public ImageJCanvasImage(ImagePlus image) {
		if (image == null) {
			throw new NullPointerException("image");
		}
		this.image = image;
	}

	private ICanvasImage newImage(ImageProcessor processor) {
		return new ImageJCanvasImage(new ImagePlus(image.getShortTitle(), processor));
	}

	@Override
	public BufferedImage getBufferedImage() {
		return image.getBufferedImage();
	}

	@Override
	public Graphics2D getGraphics() {
		if (graphics == null) {
			graphics = (Graphics2D) image.getImage().getGraphics();
		}
		return graphics;
	}

	@Override
	public int getWidth() {
		return image.getWidth();
	}

	@Override
	public int getHeight() {
		return image.getHeight();
	}

	@Override
	public ICanvasImage copy() {
		return new ImageJCanvasImage((ImagePlus) image.clone());
	}

	@Override
	public ICanvasImage crop(int x, int y, int width, int height) {
		ImageProcessor processor = image.getProcessor();
		processor.setInterpolationMethod(ImageProcessor.BILINEAR);
		processor.setRoi(x, y, width, height);
		processor = processor.crop();
		return newImage(processor);
	}

	@Override
	public ICanvasImage resize(int width, int height) {
		ImageProcessor processor = image.getProcessor();
		processor.setInterpolationMethod(ImageProcessor.BILINEAR);
		processor = processor.resize(width, height);
		return newImage(processor);
	}

	@Override
	public void dispose() {
		if (graphics != null) {
			Graphics2D dispose = graphics;
			graphics = null;
			dispose.dispose();
		}
	}

	@Override
	public byte[] toByteArray(ImageFormat format, float quality) {
		throw new UnsupportedOperationException();
	}

	@Override
	public byte[] toByteArray(ImageFormat format) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void writeToFile(File file) {
		try {
			BufferedImage buffered = getBufferedImage();
			ImageFormat format = getImageFormat(getType(buffered));
			ImageIO.write(buffered, format.name(), file);
		} catch (IOException e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public void writeToFile(File file, ImageFormat format) {
		try {
			ImageIO.write((RenderedImage) image.getImage(), format.name(), file);
		} catch (IOException e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public void writeToFile(File file, ImageFormat format, float quality) {
		try {
			ImageIO.write((RenderedImage) image.getImage(), format.name(), file);
		} catch (IOException e) {
			throw Throwables.propagate(e);
		}
	}

}
