package com.javabi.image.graphics;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

import com.javabi.common.text.parser.ColorParser;
import com.javabi.common.text.parser.IStringParser;
import com.javabi.image.shape.IRectangle;

public class Graphics implements IGraphics {

	private final IStringParser<Color> parser = new ColorParser();
	private final Graphics2D graphics;

	public Graphics(java.awt.Graphics graphics) {
		if (graphics == null) {
			throw new NullPointerException("graphics");
		}
		this.graphics = (Graphics2D) graphics;
	}

	public Graphics(Image image) {
		this(image.getGraphics());
	}

	@Override
	public Graphics2D getGraphics() {
		return graphics;
	}

	@Override
	public IGraphics rectangle(Color color, int border, int x, int y, int width, int height) {
		graphics.setColor(color);
		if (border < 1) {
			graphics.fillRect(x, y, width, height);
		} else {
			for (int i = 0; i < border; i++) {
				graphics.drawRect(x + i, y + i, width - (1 + i * 2), height - (1 + i * 2));
			}
		}
		return this;
	}

	@Override
	public IGraphics rectangle(String color, int border, int x, int y, int width, int height) {
		rectangle(parser.parse(color), x, y, width, height, border);
		return this;
	}

	@Override
	public IGraphics rectangle(Color color, int border, IRectangle rectangle) {
		rectangle(color, border, rectangle.getX(), rectangle.getY(), rectangle.getWidth(), rectangle.getHeight());
		return this;
	}

	@Override
	public IGraphics rectangle(String color, int border, IRectangle rectangle) {
		rectangle(parser.parse(color), border, rectangle);
		return this;
	}

	@Override
	public IGraphics transparent(int x, int y, int width, int height) {
		graphics.setColor(ColorParser.TRANSPARENT);
		graphics.fillRect(x, y, width, height);
		return this;
	}

	@Override
	public IGraphics transparent(IRectangle rectangle) {
		transparent(rectangle.getX(), rectangle.getY(), rectangle.getWidth(), rectangle.getHeight());
		return this;
	}

	@Override
	public IGraphics gradient(int angle, int border, String from, String to, IRectangle rectangle) {
		Color fromColor = parser.parse(from);
		Color toColor = parser.parse(to);
		gradient(angle, border, fromColor, toColor, rectangle);
		return this;
	}

	@Override
	public IGraphics gradient(int angle, int border, Color from, Color to, IRectangle rectangle) {
		Paint paint = graphics.getPaint();

		// Calculate points for gradient
		GradientCalculator calculator = new GradientCalculator();
		calculator.calculatePoints(angle, rectangle);
		Point point1 = calculator.getPoint1();
		Point point2 = calculator.getPoint2();

		// Render gradient
		GradientPaint gradient = new GradientPaint(point1, from, point2, to, false);
		graphics.setPaint(gradient);

		int x = rectangle.getX();
		int y = rectangle.getY();
		int width = rectangle.getWidth();
		int height = rectangle.getHeight();
		if (border < 1) {
			graphics.fillRect(x, y, width, height);
		} else {
			for (int i = 0; i < border; i++) {
				graphics.drawRect(x + i, y + i, width - (1 + i * 2), height - (1 + i * 2));
			}
		}

		// Reset
		graphics.setPaint(paint);
		return this;
	}

	@Override
	public IGraphics dispose() {
		graphics.dispose();
		return this;
	}

	@Override
	public IGraphics drawImage(BufferedImage image, int x, int y, ImageObserver observer) {
		graphics.drawImage(image, x, y, observer);
		return this;
	}

	@Override
	public IGraphics drawImage(BufferedImage image, int x, int y) {
		return drawImage(image, x, y, null);
	}

	@Override
	public IGraphics drawImage(BufferedImage image) {
		return drawImage(image, 0, 0, null);
	}

}
