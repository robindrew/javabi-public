package com.javabi.image.graphics;

import java.awt.Point;

import com.javabi.image.shape.IRectangle;

public class GradientCalculator {

	private final Point point1 = new Point();
	private final Point point2 = new Point();

	public Point getPoint1() {
		return point1;
	}

	public Point getPoint2() {
		return point2;
	}

	public void calculatePoints(int angle, IRectangle rectangle) {
		if (angle < 0 || angle > 360) {
			throw new IllegalArgumentException("angle=" + angle);
		}

		int x = rectangle.getX();
		int y = rectangle.getY();
		int width = rectangle.getWidth();
		int height = rectangle.getHeight();

		// Quick cases
		switch (angle) {
			case 0:
			case 360:
				point1.setLocation(x, y);
				point2.setLocation(x, y + height);
				return;
			case 90:
				point1.setLocation(x + width, y);
				point2.setLocation(x, y);
				return;
			case 180:
				point1.setLocation(x, y + height);
				point2.setLocation(x, y);
				return;
			case 270:
				point1.setLocation(x, y);
				point2.setLocation(x + width, y);
				return;
			default:
				break;
		}

		// 0-90 degrees
		if (angle > 0 && angle < 90) {
			Point reference = getPoint(90 - getAngle(angle), x, y, x + width, y + height);
			point1.setLocation(x + width, y);
			reference.x = point1.x - (reference.x - x);
			point2.setLocation(reference);
			return;
		}

		// 90-180 degrees
		if (angle > 90 && angle < 180) {
			Point reference = getPoint(getAngle(angle), x, y, x + width, y + height);
			point1.setLocation(x + width, y + height);
			reference.x = point1.x - (reference.x - x);
			reference.y = point1.y - (reference.y - y);
			point2.setLocation(reference);
			return;
		}

		// 180-270 degrees
		if (angle > 180 && angle < 270) {
			Point reference = getPoint(90 - getAngle(angle), x, y, x + width, y + height);
			point1.setLocation(x, y + height);
			reference.y = point1.y - (reference.y - y);
			point2.setLocation(reference);
			return;
		}

		// 270-360 degrees
		if (angle > 270 && angle < 360) {
			Point reference = getPoint(getAngle(angle), x, y, x + width, y + height);
			point1.setLocation(x, y);
			point2.setLocation(reference);
			return;
		}

		throw new IllegalArgumentException("angle=" + angle);
	}

	private int getAngle(int angle) {
		while (angle > 90) {
			angle -= 90;
		}
		return angle;
	}

	public Point getPoint(int angle, int x1, int y1, int x2, int y2) {
		return getPoint(angle, new Point(x1, y1), new Point(x2, y2));
	}

	public Point getPoint(int angle, Point point1, Point point2) {
		double radians = Math.toRadians(angle);
		double length1 = point1.distance(point2);
		double length2 = length1 * Math.cos(radians);
		double length3 = Math.sqrt((length1 * length1) - (length2 * length2));
		return new Point(point1.x + (int) length2, point1.y + (int) length3);
	}

}
