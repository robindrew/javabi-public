package com.javabi.image.graphics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

import com.javabi.image.shape.IRectangle;

public interface IGraphics {

	Graphics2D getGraphics();

	IGraphics rectangle(Color color, int border, int x, int y, int width, int height);

	IGraphics rectangle(String color, int border, int x, int y, int width, int height);

	IGraphics rectangle(Color color, int border, IRectangle rectangle);

	IGraphics rectangle(String color, int border, IRectangle rectangle);

	IGraphics transparent(int x, int y, int width, int height);

	IGraphics transparent(IRectangle rectangle);

	IGraphics gradient(int angle, int border, Color from, Color to, IRectangle rectangle);

	IGraphics gradient(int angle, int border, String from, String to, IRectangle rectangle);

	IGraphics dispose();

	IGraphics drawImage(BufferedImage image, int x, int y, ImageObserver observer);

	IGraphics drawImage(BufferedImage image, int x, int y);

	IGraphics drawImage(BufferedImage image);

}
