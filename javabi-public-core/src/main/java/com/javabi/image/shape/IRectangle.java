package com.javabi.image.shape;

public interface IRectangle {

	int getX();

	int getY();

	int getWidth();

	int getHeight();

}
