package com.javabi.image.buffered;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;

import javax.imageio.ImageIO;

import com.google.common.base.Throwables;

public class BufferedImageReader {

	// private static final Logger log = LoggerFactory.getLogger(BufferedImageReader.class);

	public BufferedImage readFromStream(InputStream input) {
		try {
			BufferedImage bufferedImage = ImageIO.read(input);
			return bufferedImage;
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	public BufferedImage readFromFile(File file) {
		try {
			// NanoTimer timer = new NanoTimer();
			// timer.start();
			BufferedImage bufferedImage = ImageIO.read(file);
			return bufferedImage;
			// int type = bufferedImage.getType();
			// bufferedImage = null;
			//
			// // Why not just use the above line, why use the toolkit below??
			// // Because unfortunately a small number of images simply do not read correctly ...
			// // The colour is completely messed up! (java 6)
			//
			// Image image = Toolkit.getDefaultToolkit().getImage(file.getAbsolutePath());
			// bufferedImage = readFromImage(image, type);
			// timer.stop();
			// log.info("Image loaded from " + file + " in " + timer);
			// return bufferedImage;
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	public BufferedImage readFromImage(Image image, int type) {
		if (image instanceof BufferedImage) {
			return (BufferedImage) image;
		}

		// The toolkit has not actually loaded the image at this point so we have to use this unnecessary observer code
		WaitingImageObserver observer = new WaitingImageObserver(image);
		observer.waitUntilImageLoaded();

		BufferedImage newImage = new BufferedImage(image.getWidth(observer), image.getHeight(observer), type);
		Graphics2D graphics = newImage.createGraphics();
		graphics.drawImage(image, 0, 0, observer);
		graphics.dispose();
		return newImage;
	}
}
