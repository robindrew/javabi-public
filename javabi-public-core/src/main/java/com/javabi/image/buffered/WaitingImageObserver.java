package com.javabi.image.buffered;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

/**
 * This code exists to force an Image to actually load its contents completely.
 */
public class WaitingImageObserver implements ImageObserver {

	private boolean lock;
	private Image image;
	private boolean error;

	public WaitingImageObserver(final Image image) {
		if (image == null) {
			throw new NullPointerException();
		}
		this.image = image;
		this.lock = true;
	}

	@Override
	public synchronized boolean imageUpdate(final Image img, final int infoflags, final int x, final int y, final int width, final int height) {

		// Success?
		if ((infoflags & ImageObserver.ALLBITS) == ImageObserver.ALLBITS) {
			this.lock = false;
			this.error = false;
			notifyAll();
			return false;
		} else {

			// Aborted or Error ...
			if ((infoflags & ImageObserver.ABORT) == ImageObserver.ABORT || (infoflags & ImageObserver.ERROR) == ImageObserver.ERROR) {
				this.lock = false;
				this.error = true;
				notifyAll();
				return false;
			}
		}
		return true;
	}

	public synchronized void waitUntilImageLoaded() {
		if (lock == false) {
			return;
		}

		BufferedImage tinyImage = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
		Graphics graphics = tinyImage.getGraphics();

		// Wait for dimensions
		int width = -1;
		int height = -1;
		while (width == -1 || height == -1) {
			width = tinyImage.getWidth(this);
			height = tinyImage.getHeight(this);
		}

		// Wait for image
		while (this.lock) {
			if (graphics.drawImage(this.image, 0, 0, width, height, this)) {
				graphics.dispose();
				return;
			}
			try {
				wait(200);
			} catch (InterruptedException e) {
			}
		}
	}

	public boolean isLoadingComplete() {
		return lock == false;
	}

	public boolean isError() {
		return error;
	}
}
