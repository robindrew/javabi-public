package com.javabi.igindex;

import static com.javabi.igindex.rest.executor.getaccounts.AccountType.SPREADBET;

import java.util.ArrayList;
import java.util.List;

import com.javabi.igindex.rest.IRestService;
import com.javabi.igindex.rest.executor.getaccounts.Account;
import com.javabi.igindex.rest.executor.getaccounts.AccountType;
import com.javabi.igindex.streaming.IStreamingService;
import com.javabi.igindex.streaming.subscription.charttick.ChartTickSubscription;
import com.javabi.igindex.streaming.subscription.charttick.ChartTickSubscriptionListener;
import com.javabi.investor.platform.TradingPlatform;
import com.javabi.investor.platform.position.IPosition;
import com.javabi.investor.price.candle.io.stream.sink.subscriber.IPriceCandleSubscriberStreamSink;
import com.javabi.investor.price.pounds.IPounds;
import com.javabi.investor.price.pounds.Pounds;
import com.javabi.investor.provider.igindex.IgInstrument;
import com.javabi.investor.trade.Funds;

public class IgTradingPlatform extends TradingPlatform {

	private final AccountType accountType;
	private final IRestService service;

	public IgTradingPlatform(IRestService service) {
		this.service = service;
		this.accountType = SPREADBET;
	}

	public IStreamingService getStreamingService() {
		return getRestService().getStreamingService();
	}

	public IRestService getRestService() {
		return service;
	}

	public IPriceCandleSubscriberStreamSink register(IgInstrument instrument) {

		// If we can register this instrument ...
		if (super.register(instrument)) {

			// Subscribe for TICK updates
			ChartTickSubscriptionListener listener = new ChartTickSubscriptionListener(instrument);
			getStreamingService().addSubscription(new ChartTickSubscription(instrument, listener));

			// Register the listener so that updates can be subscribed for
			// through the platform
			register(listener);

			return listener;
		} else {

			return getSubscriber(instrument.getInstrument());
		}
	}

	@Override
	public List<IPosition> getAllPositions() {
		List<IPosition> positions = new ArrayList<>();
		for (IPosition position : service.getPositions()) {
			positions.add(position);
		}
		return positions;
	}

	@Override
	public void closePosition(IPosition position) {
		service.closePosition(position);

	}

	@Override
	public Funds getAvailableFunds() {
		Account account = service.getAccount(accountType);
		IPounds available = new Pounds(account.getBalance().getAvailable(), false);
		return new Funds(available);
	}

}
