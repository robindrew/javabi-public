package com.javabi.igindex;

public class IgException extends RuntimeException {

	public IgException(String message) {
		super(message);
	}

	public IgException(Throwable cause) {
		super(cause);
	}

	public IgException(String message, Throwable cause) {
		super(message, cause);
	}

}
