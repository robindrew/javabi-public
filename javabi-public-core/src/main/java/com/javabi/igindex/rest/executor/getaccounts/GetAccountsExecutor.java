package com.javabi.igindex.rest.executor.getaccounts;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;

import com.javabi.common.xml.gson.IJson;
import com.javabi.igindex.rest.IRestService;
import com.javabi.igindex.rest.executor.HttpJsonRestExecutor;

public class GetAccountsExecutor extends HttpJsonRestExecutor<GetAccountsResponse> {

	public GetAccountsExecutor(IRestService service) {
		super(service);
	}

	@Override
	public int getRequestVersion() {
		return 1;
	}

	@Override
	public HttpUriRequest createRequest() {
		HttpGet request = new HttpGet(getUrl("/accounts"));
		addHeaders(request);
		return request;
	}

	@Override
	public GetAccountsResponse createResponse(IJson json) {
		return new GetAccountsResponse(json);
	}

}
