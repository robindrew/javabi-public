package com.javabi.igindex.rest.executor.getmarketdetails;

import com.javabi.common.xml.gson.IJson;

public class GetMarketDetailsResponse {

	private final MarketDetails marketDetails;

	public GetMarketDetailsResponse(IJson object) {
		marketDetails = new MarketDetails(object);
	}

	public MarketDetails getMarketDetails() {
		return marketDetails;
	}
}
