package com.javabi.igindex.rest.executor.getmarketdetails;

public enum MarketOrderPreference {

	AVAILABLE_DEFAULT_OFF, AVAILABLE_DEFAULT_ON, NOT_AVAILABLE
}
