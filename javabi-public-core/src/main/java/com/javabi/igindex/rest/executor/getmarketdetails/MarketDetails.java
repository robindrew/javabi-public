package com.javabi.igindex.rest.executor.getmarketdetails;

import com.javabi.common.lang.Java;
import com.javabi.common.xml.gson.IJson;

public class MarketDetails {

	private final Instrument instrument;
	private final DealingRules dealingRules;
	private final Snapshot snapshot;

	public MarketDetails(IJson object) {
		instrument = new Instrument(object.getObject("instrument"));
		dealingRules = new DealingRules(object.getObject("dealingRules"));
		snapshot = new Snapshot(object.getObject("snapshot"));
	}

	public Instrument getInstrument() {
		return instrument;
	}

	public DealingRules getDealingRules() {
		return dealingRules;
	}

	public Snapshot getSnapshot() {
		return snapshot;
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

}
