package com.javabi.igindex.rest.executor.getpositions;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;

import com.javabi.common.xml.gson.IJson;
import com.javabi.igindex.rest.IRestService;
import com.javabi.igindex.rest.executor.HttpJsonRestExecutor;

public class GetPositionsExecutor extends HttpJsonRestExecutor<GetPositionsResponse> {

	public GetPositionsExecutor(IRestService service) {
		super(service);
	}

	@Override
	public int getRequestVersion() {
		return 2;
	}

	@Override
	public HttpUriRequest createRequest() {
		HttpGet request = new HttpGet(getUrl("/positions"));
		addHeaders(request);
		return request;
	}

	@Override
	public GetPositionsResponse createResponse(IJson json) {
		return new GetPositionsResponse(json);
	}

}
