package com.javabi.igindex.rest.executor.getpositions;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.javabi.common.xml.gson.IJson;

public class GetPositionsResponse {

	private final List<MarketPosition> marketPositions = new ArrayList<>();

	public GetPositionsResponse(IJson object) {
		for (IJson element : object.getObjectList("positions")) {
			marketPositions.add(new MarketPosition(element));
		}
	}

	public List<MarketPosition> getMarketPositions() {
		return ImmutableList.copyOf(marketPositions);
	}

}
