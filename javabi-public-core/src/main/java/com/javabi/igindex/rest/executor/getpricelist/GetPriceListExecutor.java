package com.javabi.igindex.rest.executor.getpricelist;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;

import com.javabi.common.xml.gson.IJson;
import com.javabi.igindex.rest.IRestService;
import com.javabi.igindex.rest.executor.HttpJsonRestExecutor;
import com.javabi.investor.provider.igindex.IgInstrument;

public class GetPriceListExecutor extends HttpJsonRestExecutor<GetPriceListResponse> {

	private final GetPriceListRequest request;

	public GetPriceListExecutor(IRestService service, GetPriceListRequest request) {
		super(service);
		if (request == null) {
			throw new NullPointerException("instrument");
		}
		this.request = request;
	}

	@Override
	public int getRequestVersion() {
		return 3;
	}

	@Override
	public HttpUriRequest createRequest() {
		IgInstrument instrument = request.getInstrument();

		HttpGet request = new HttpGet(getUrl("/prices/") + instrument.getEpic() + getQuery());
		addHeaders(request);
		return request;
	}

	private String getQuery() {
		QueryBuilder query = new QueryBuilder();
		if (request.getMax() > 0) {
			query.append("resolution", request.getResolution());
		}
		if (request.getMax() > 0) {
			query.append("max", request.getMax());
		}
		if (request.getPageSize() > 0) {
			query.append("pageSize", request.getPageSize());
		}
		if (request.getPageNumber() > 0) {
			query.append("pageNumber", request.getPageNumber());
		}
		if (request.getFrom() != null) {
			query.append("from", request.getFrom());
		}
		if (request.getTo() != null) {
			query.append("to", request.getTo());
		}
		return query.toString();
	}

	@Override
	public GetPriceListResponse createResponse(IJson json) {
		return new GetPriceListResponse(json);
	}

}
