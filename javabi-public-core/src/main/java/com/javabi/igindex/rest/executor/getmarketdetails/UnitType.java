package com.javabi.igindex.rest.executor.getmarketdetails;

public enum UnitType {

	POINTS, PERCENTAGE;
}
