package com.javabi.igindex.rest.executor.getpositions;

public enum MarketStatus {

	CLOSED, EDITS_ONLY, OFFLINE, ON_AUCTION, ON_AUCTION_NO_EDITS, SUSPENDED, TRADEABLE
}
