package com.javabi.igindex.rest.executor.getaccounts;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.javabi.common.xml.gson.IJson;

public class GetAccountsResponse {

	private final List<Account> accounts = new ArrayList<>();

	public GetAccountsResponse(IJson object) {
		for (IJson element : object.getObjectList("accounts")) {
			accounts.add(new Account(element));
		}
	}

	public List<Account> getAccounts() {
		return ImmutableList.copyOf(accounts);
	}

}
