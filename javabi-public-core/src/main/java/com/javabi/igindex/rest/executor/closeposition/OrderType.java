package com.javabi.igindex.rest.executor.closeposition;

public enum OrderType {

	MARKET, LIMIT, QUOTE;
}
