package com.javabi.igindex.rest.executor.getmarketdetails;

import java.math.BigDecimal;

import com.javabi.common.lang.Java;
import com.javabi.common.locale.CurrencyCode;
import com.javabi.common.xml.gson.IJson;

public class MarginDepositBand {

	private final BigDecimal min;
	private final BigDecimal max;
	private final BigDecimal margin;
	private final CurrencyCode currency;

	public MarginDepositBand(IJson object) {
		min = object.getBigDecimal("min");
		max = object.getBigDecimal("max", true);
		margin = object.getBigDecimal("margin");
		currency = object.getEnum("currency", CurrencyCode.class);
	}

	public BigDecimal getMin() {
		return min;
	}

	public BigDecimal getMax() {
		return max;
	}

	public BigDecimal getMargin() {
		return margin;
	}

	public CurrencyCode getCurrency() {
		return currency;
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

}
