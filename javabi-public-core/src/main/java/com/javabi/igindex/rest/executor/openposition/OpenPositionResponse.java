package com.javabi.igindex.rest.executor.openposition;

import com.javabi.common.xml.gson.IJson;

public class OpenPositionResponse {

	private final String dealReference;

	public OpenPositionResponse(IJson object) {
		dealReference = object.get("dealReference");
	}

	public String getDealReference() {
		return dealReference;
	}

}
