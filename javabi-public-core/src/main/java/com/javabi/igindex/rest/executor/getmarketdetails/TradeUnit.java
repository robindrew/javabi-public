package com.javabi.igindex.rest.executor.getmarketdetails;

public enum TradeUnit {

	AMOUNT, CONTRACTS, SHARES
}
