package com.javabi.igindex.rest.executor.getpositions;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.javabi.common.lang.Java;
import com.javabi.common.locale.CurrencyCode;
import com.javabi.common.xml.gson.IJson;
import com.javabi.investor.trade.TradeDirection;

public final class Position {

	private final BigDecimal contractSize;
	private final LocalDateTime createdDateUTC; // "createdDateUTC":"2016-12-21T22:31:42",
	private final String dealId;
	private final String dealReference;
	private final BigDecimal size;
	private final TradeDirection direction;
	private final BigDecimal limitLevel;
	private final BigDecimal level;
	private final CurrencyCode currency;
	private final boolean controlledRisk;
	private final BigDecimal stopLevel;

	// "createdDate":"2016/12/21 22:31:42:000",
	// "trailingStep":null,
	// "trailingStopDistance":null

	public Position(IJson object) {
		contractSize = object.getBigDecimal("contractSize");
		createdDateUTC = object.getLocalDateTime("createdDateUTC");
		dealId = object.get("dealId");
		dealReference = object.get("dealReference");
		size = object.getBigDecimal("size");
		direction = object.getEnum("direction", TradeDirection.class);
		limitLevel = object.getBigDecimal("limitLevel", true);
		level = object.getBigDecimal("level");
		currency = object.getEnum("currency", CurrencyCode.class);
		controlledRisk = object.getBoolean("currency");
		stopLevel = object.getBigDecimal("stopLevel", true);
	}

	public BigDecimal getContractSize() {
		return contractSize;
	}

	public LocalDateTime getCreatedDateUTC() {
		return createdDateUTC;
	}

	public String getDealId() {
		return dealId;
	}

	public String getDealReference() {
		return dealReference;
	}

	public BigDecimal getSize() {
		return size;
	}

	public TradeDirection getDirection() {
		return direction;
	}

	public BigDecimal getLimitLevel() {
		return limitLevel;
	}

	public BigDecimal getLevel() {
		return level;
	}

	public CurrencyCode getCurrency() {
		return currency;
	}

	public boolean isControlledRisk() {
		return controlledRisk;
	}

	public BigDecimal getStopLevel() {
		return stopLevel;
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

}
