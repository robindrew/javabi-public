package com.javabi.igindex.rest.executor.getmarketdetails;

import java.math.BigDecimal;

import com.javabi.common.lang.Java;
import com.javabi.common.xml.gson.IJson;

public class UnitValue {

	private final UnitType unit;
	private final BigDecimal value;

	public UnitValue(IJson object) {
		unit = object.getEnum("unit", UnitType.class);
		value = object.getBigDecimal("value");
	}

	public UnitType getUnit() {
		return unit;
	}

	public BigDecimal getValue() {
		return value;
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

}
