package com.javabi.igindex.rest.executor;

import org.apache.http.client.methods.HttpUriRequest;

import com.javabi.common.xml.gson.IJson;

public interface IHttpJsonRestExecutor<R> extends IRestExecutor<R> {

	HttpUriRequest createRequest();

	R createResponse(IJson json);

}
