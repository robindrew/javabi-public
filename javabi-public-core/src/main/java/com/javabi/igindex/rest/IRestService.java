package com.javabi.igindex.rest;

import java.time.temporal.ChronoUnit;
import java.util.List;

import com.javabi.igindex.IgSession;
import com.javabi.igindex.rest.executor.getaccounts.Account;
import com.javabi.igindex.rest.executor.getaccounts.AccountType;
import com.javabi.igindex.rest.executor.getmarketdetails.MarketDetails;
import com.javabi.igindex.rest.executor.getpositions.MarketPosition;
import com.javabi.igindex.rest.executor.getpricelist.PriceList;
import com.javabi.igindex.streaming.IStreamingService;
import com.javabi.investor.platform.position.IPosition;
import com.javabi.investor.provider.igindex.IgInstrument;

public interface IRestService {

	IgSession getSession();

	void login();

	PriceList getPriceList(IgInstrument instrument, ChronoUnit unit, int size);

	List<Account> getAccounts();

	Account getAccount(AccountType type);

	List<MarketPosition> getPositions();

	void closePosition(IPosition position);

	void closeAllPositions();

	IStreamingService getStreamingService();

	MarketDetails getMarketDetails(IgInstrument instrument);
}
