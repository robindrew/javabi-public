package com.javabi.igindex.rest.executor.getmarketdetails;

public enum Availability {

	AVAILABLE, NOT_AVAILABLE;
}
