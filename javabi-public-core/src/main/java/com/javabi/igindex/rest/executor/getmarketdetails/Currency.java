package com.javabi.igindex.rest.executor.getmarketdetails;

import java.math.BigDecimal;

import com.javabi.common.lang.Java;
import com.javabi.common.locale.CurrencyCode;
import com.javabi.common.xml.gson.IJson;

public class Currency {

	private final CurrencyCode code;
	private final String symbol;
	private final BigDecimal baseExchangeRate;
	private final BigDecimal exchangeRate;
	private final boolean isDefault;

	public Currency(IJson object) {
		code = object.getEnum("code", CurrencyCode.class);
		symbol = object.get("symbol");
		baseExchangeRate = object.getBigDecimal("baseExchangeRate");
		exchangeRate = object.getBigDecimal("exchangeRate");
		isDefault = object.getBoolean("isDefault");
	}

	public CurrencyCode getCode() {
		return code;
	}

	public String getSymbol() {
		return symbol;
	}

	public BigDecimal getBaseExchangeRate() {
		return baseExchangeRate;
	}

	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public boolean isDefault() {
		return isDefault;
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

}
