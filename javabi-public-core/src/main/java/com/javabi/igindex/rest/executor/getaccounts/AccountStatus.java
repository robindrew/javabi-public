package com.javabi.igindex.rest.executor.getaccounts;

public enum AccountStatus {

	ENABLED, DISABLED, SUSPENDED_FROM_DEALING;
}
