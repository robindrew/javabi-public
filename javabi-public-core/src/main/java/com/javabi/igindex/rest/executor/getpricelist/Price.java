package com.javabi.igindex.rest.executor.getpricelist;

import java.math.BigDecimal;

import com.javabi.common.lang.Java;
import com.javabi.common.xml.gson.IJson;

public class Price {

	private final BigDecimal bid;
	private final BigDecimal ask;
	private final BigDecimal lastTraded;

	public Price(IJson object) {
		bid = object.getBigDecimal("bid", true);
		ask = object.getBigDecimal("ask", true);
		lastTraded = object.getBigDecimal("lastTraded", true);
	}

	public BigDecimal getBid() {
		return bid;
	}

	public BigDecimal getAsk() {
		return ask;
	}

	public BigDecimal getLastTraded() {
		return lastTraded;
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

}
