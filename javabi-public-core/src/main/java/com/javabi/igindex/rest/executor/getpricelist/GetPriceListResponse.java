package com.javabi.igindex.rest.executor.getpricelist;

import com.javabi.common.xml.gson.IJson;

public class GetPriceListResponse {

	private final PriceList priceList;

	public GetPriceListResponse(IJson object) {
		priceList = new PriceList(object);
	}

	public PriceList getPriceList() {
		return priceList;
	}

}
