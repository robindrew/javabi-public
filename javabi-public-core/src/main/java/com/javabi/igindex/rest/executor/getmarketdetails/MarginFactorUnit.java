package com.javabi.igindex.rest.executor.getmarketdetails;

public enum MarginFactorUnit {

	PERCENTAGE, POINTS;
}
