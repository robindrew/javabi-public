package com.javabi.igindex.rest.executor.getaccounts;

public enum AccountType {

	CFD, PHYSICAL, SPREADBET
}
