package com.javabi.igindex.rest.executor.getwatchlists;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;

import com.javabi.common.xml.gson.IJson;
import com.javabi.igindex.rest.IRestService;
import com.javabi.igindex.rest.executor.HttpJsonRestExecutor;

public class GetWatchlistsExecutor extends HttpJsonRestExecutor<Boolean> {

	public GetWatchlistsExecutor(IRestService platform) {
		super(platform);
	}

	@Override
	public int getRequestVersion() {
		return 1;
	}

	@Override
	public HttpUriRequest createRequest() {
		HttpGet request = new HttpGet(getUrl("/watchlists"));
		addHeaders(request);
		return request;
	}

	@Override
	public Boolean createResponse(IJson json) {
		return true;
	}

}
