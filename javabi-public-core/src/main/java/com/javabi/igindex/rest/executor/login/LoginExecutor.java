package com.javabi.igindex.rest.executor.login;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.xml.gson.IJson;
import com.javabi.igindex.rest.IRestService;
import com.javabi.igindex.rest.executor.HttpJsonRestExecutor;

public class LoginExecutor extends HttpJsonRestExecutor<LoginResponse> {

	private static final Logger log = LoggerFactory.getLogger(LoginExecutor.class);

	public LoginExecutor(IRestService platform) {
		super(platform);
	}

	@Override
	public int getRequestVersion() {
		return 2;
	}

	@Override
	protected boolean isLoginAttempt() {
		return true;
	}

	@Override
	public HttpUriRequest createRequest() {

		// JSON
		String username = getSession().getCredentials().getUsername();
		String password = getSession().getCredentials().getPassword();
		LoginRequest jsonRequest = new LoginRequest(username, password);

		// HTTP
		HttpPost request = new HttpPost(getUrl("/session"));
		addHeaders(request);
		setRequestContent(request, jsonRequest);
		return request;
	}

	@Override
	public LoginResponse createResponse(IJson json) {
		LoginResponse response = new LoginResponse(json);

		log.info("clientId: " + response.getClientId());
		log.info("accountType: " + response.getAccountType());
		log.info("timezoneOffset: " + response.getTimezoneOffset());
		log.info("dealingEnabled: " + response.getDealingEnabled());
		log.info("currencySymbol: " + response.getCurrencySymbol());
		log.info("currencyIsoCode: " + response.getCurrencyIsoCode());
		log.info("currentAccountId: " + response.getCurrentAccountId());
		log.info("trailingStopsEnabled: " + response.getTrailingStopsEnabled());
		log.info("hasActiveDemoAccounts: " + response.getHasActiveDemoAccounts());
		log.info("hasActiveLiveAccounts: " + response.getHasActiveLiveAccounts());
		log.info("lightstreamerEndpoint: " + response.getLightstreamerEndpoint());

		// Lightstreamer Endpoint
		getSession().setLightstreamerEndpoint(response.getLightstreamerEndpoint());

		return response;
	}
}
