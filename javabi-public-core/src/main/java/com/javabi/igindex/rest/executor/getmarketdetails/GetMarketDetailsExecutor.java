package com.javabi.igindex.rest.executor.getmarketdetails;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;

import com.javabi.common.xml.gson.IJson;
import com.javabi.igindex.rest.IRestService;
import com.javabi.igindex.rest.executor.HttpJsonRestExecutor;
import com.javabi.investor.provider.igindex.IgInstrument;

public class GetMarketDetailsExecutor extends HttpJsonRestExecutor<GetMarketDetailsResponse> {

	private final IgInstrument instrument;

	public GetMarketDetailsExecutor(IRestService service, IgInstrument instrument) {
		super(service);
		if (instrument == null) {
			throw new NullPointerException("instrument");
		}
		this.instrument = instrument;
	}

	@Override
	public int getRequestVersion() {
		return 3;
	}

	@Override
	public HttpUriRequest createRequest() {
		HttpGet request = new HttpGet(getUrl("/markets/" + instrument.getEpic()));
		addHeaders(request);
		return request;
	}

	@Override
	public GetMarketDetailsResponse createResponse(IJson json) {
		return new GetMarketDetailsResponse(json);
	}

}
