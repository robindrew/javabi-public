package com.javabi.igindex.rest.executor.getpricelist;

import com.javabi.common.lang.Java;
import com.javabi.common.xml.gson.IJson;

public class Metadata {

	private final Allowance allowance;
	private final int size;
	private final PageData pageData;

	public Metadata(IJson object) {
		allowance = new Allowance(object.getObject("allowance"));
		size = object.getInt("size");
		pageData = new PageData(object.getObject("pageData"));
	}

	public Allowance getAllowance() {
		return allowance;
	}

	public int getSize() {
		return size;
	}

	public PageData getPageData() {
		return pageData;
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

	public class Allowance {

		private final int remainingAllowance;
		private final int totalAllowance;
		private final int allowanceExpiry;

		private Allowance(IJson object) {
			remainingAllowance = object.getInt("remainingAllowance");
			totalAllowance = object.getInt("totalAllowance");
			allowanceExpiry = object.getInt("allowanceExpiry");
		}

		public int getRemainingAllowance() {
			return remainingAllowance;
		}

		public int getTotalAllowance() {
			return totalAllowance;
		}

		public int getAllowanceExpiry() {
			return allowanceExpiry;
		}

		@Override
		public String toString() {
			return Java.toString(this);
		}
	}

	public class PageData {

		private final int pageSize;
		private final int pageNumber;
		private final int totalPages;

		private PageData(IJson object) {
			pageSize = object.getInt("pageSize");
			pageNumber = object.getInt("pageNumber");
			totalPages = object.getInt("totalPages");
		}

		public int getPageSize() {
			return pageSize;
		}

		public int getPageNumber() {
			return pageNumber;
		}

		public int getTotalPages() {
			return totalPages;
		}

		@Override
		public String toString() {
			return Java.toString(this);
		}
	}

}
