package com.javabi.igindex.rest;

import java.time.temporal.ChronoUnit;
import java.util.List;

import com.javabi.igindex.IgSession;
import com.javabi.igindex.rest.executor.closeposition.ClosePositionExecutor;
import com.javabi.igindex.rest.executor.closeposition.ClosePositionRequest;
import com.javabi.igindex.rest.executor.getaccounts.Account;
import com.javabi.igindex.rest.executor.getaccounts.AccountType;
import com.javabi.igindex.rest.executor.getaccounts.GetAccountsExecutor;
import com.javabi.igindex.rest.executor.getaccounts.GetAccountsResponse;
import com.javabi.igindex.rest.executor.getmarketdetails.GetMarketDetailsExecutor;
import com.javabi.igindex.rest.executor.getmarketdetails.GetMarketDetailsResponse;
import com.javabi.igindex.rest.executor.getmarketdetails.MarketDetails;
import com.javabi.igindex.rest.executor.getpositions.GetPositionsExecutor;
import com.javabi.igindex.rest.executor.getpositions.GetPositionsResponse;
import com.javabi.igindex.rest.executor.getpositions.MarketPosition;
import com.javabi.igindex.rest.executor.getpricelist.GetPriceListExecutor;
import com.javabi.igindex.rest.executor.getpricelist.GetPriceListRequest;
import com.javabi.igindex.rest.executor.getpricelist.GetPriceListResponse;
import com.javabi.igindex.rest.executor.getpricelist.PriceList;
import com.javabi.igindex.rest.executor.getpricelist.PriceResolution;
import com.javabi.igindex.rest.executor.login.LoginExecutor;
import com.javabi.igindex.streaming.IStreamingService;
import com.javabi.igindex.streaming.StreamingService;
import com.javabi.investor.platform.position.IPosition;
import com.javabi.investor.provider.igindex.IgInstrument;

public class RestService implements IRestService {

	private final IgSession session;

	public RestService(IgSession session) {
		if (session == null) {
			throw new NullPointerException("session");
		}
		this.session = session;
	}

	@Override
	public IgSession getSession() {
		return session;
	}

	@Override
	public synchronized void login() {
		new LoginExecutor(this).execute();
	}

	@Override
	public synchronized List<MarketPosition> getPositions() {
		GetPositionsResponse response = new GetPositionsExecutor(this).execute();
		return response.getMarketPositions();
	}

	@Override
	public synchronized void closePosition(IPosition position) {
		ClosePositionRequest request = ClosePositionRequest.forPosition(position);
		new ClosePositionExecutor(this, request).execute();
	}

	@Override
	public synchronized List<Account> getAccounts() {
		GetAccountsResponse response = new GetAccountsExecutor(this).execute();
		return response.getAccounts();
	}

	@Override
	public synchronized void closeAllPositions() {

		// List open positions
		List<MarketPosition> positions = getPositions();

		// Close all open positions!
		for (MarketPosition position : positions) {
			closePosition(position);
		}
	}

	@Override
	public synchronized Account getAccount(AccountType type) {
		for (Account account : getAccounts()) {
			if (account.getAccountType().equals(type)) {
				return account;
			}
		}
		throw new IllegalArgumentException("Account not found with type: " + type);
	}

	@Override
	public IStreamingService getStreamingService() {
		return new StreamingService(session);
	}

	@Override
	public synchronized MarketDetails getMarketDetails(IgInstrument instrument) {
		GetMarketDetailsResponse response = new GetMarketDetailsExecutor(this, instrument).execute();
		return response.getMarketDetails();
	}

	@Override
	public PriceList getPriceList(IgInstrument instrument, ChronoUnit unit, int size) {
		if (size < 1) {
			throw new IllegalArgumentException("size=" + size);
		}

		PriceResolution resolution = PriceResolution.valueOf(unit);
		GetPriceListRequest request = new GetPriceListRequest(instrument, resolution);
		request.setMax(size);

		GetPriceListResponse response = new GetPriceListExecutor(this, request).execute();
		return response.getPriceList();
	}

}
