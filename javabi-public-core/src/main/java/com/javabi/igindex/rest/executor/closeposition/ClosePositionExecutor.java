package com.javabi.igindex.rest.executor.closeposition;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;

import com.javabi.common.xml.gson.IJson;
import com.javabi.igindex.rest.IRestService;
import com.javabi.igindex.rest.executor.HttpJsonRestExecutor;

public class ClosePositionExecutor extends HttpJsonRestExecutor<Boolean> {

	private final ClosePositionRequest jsonRequest;

	public ClosePositionExecutor(IRestService service, ClosePositionRequest jsonRequest) {
		super(service);
		if (jsonRequest == null) {
			throw new NullPointerException("jsonRequest");
		}
		this.jsonRequest = jsonRequest;
	}

	@Override
	public int getRequestVersion() {
		return 1;
	}

	@Override
	public HttpUriRequest createRequest() {
		HttpPost request = new HttpPost(getUrl("/positions/otc"));
		addDeleteHeader(request);
		addHeaders(request);
		setRequestContent(request, jsonRequest);
		return request;
	}

	@Override
	public Boolean createResponse(IJson json) {
		return true;
	}
}
