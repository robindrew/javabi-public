package com.javabi.igindex.rest.executor.getaccounts;

import java.math.BigDecimal;

import com.javabi.common.lang.Java;
import com.javabi.common.xml.gson.IJson;

public class Balance {

	private final BigDecimal balance;
	private final BigDecimal deposit;
	private final BigDecimal profitLoss;
	private final BigDecimal available;

	public Balance(IJson object) {
		balance = object.getBigDecimal("balance");
		deposit = object.getBigDecimal("deposit");
		profitLoss = object.getBigDecimal("profitLoss");
		available = object.getBigDecimal("available");
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public BigDecimal getDeposit() {
		return deposit;
	}

	public BigDecimal getProfitLoss() {
		return profitLoss;
	}

	public BigDecimal getAvailable() {
		return available;
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}
}
