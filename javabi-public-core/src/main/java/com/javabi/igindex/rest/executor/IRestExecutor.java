package com.javabi.igindex.rest.executor;

import com.javabi.igindex.IgSession;

public interface IRestExecutor<R> {

	IgSession getSession();

	int getRequestVersion();

	R execute();

}
