package com.javabi.igindex.rest.executor.getpositions;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.javabi.common.lang.Java;
import com.javabi.common.locale.CurrencyCode;
import com.javabi.common.xml.gson.IJson;
import com.javabi.investor.IInstrument;
import com.javabi.investor.platform.position.IPosition;
import com.javabi.investor.trade.TradeDirection;

public class MarketPosition implements IPosition {

	private final Position position;
	private final Market market;

	public MarketPosition(IJson object) {
		this.position = new Position(object.getObject("position"));
		this.market = new Market(object.getObject("market"));
	}

	public Position getPosition() {
		return position;
	}

	public Market getMarket() {
		return market;
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

	@Override
	public String getId() {
		return getPosition().getDealId();
	}

	@Override
	public IInstrument getInstrument() {
		return getMarket().getInstrument().getInstrument();
	}

	@Override
	public TradeDirection getDirection() {
		return getPosition().getDirection();
	}

	@Override
	public LocalDateTime getOpenDate() {
		return getPosition().getCreatedDateUTC();
	}

	@Override
	public CurrencyCode getTradeCurrency() {
		return getPosition().getCurrency();
	}

	@Override
	public BigDecimal getOpenPrice() {
		return getPosition().getLevel();
	}

	@Override
	public BigDecimal getTradeSize() {
		// return getPosition().getContractSize();
		// return getPosition().getSize();
		throw new UnsupportedOperationException();
	}

}
