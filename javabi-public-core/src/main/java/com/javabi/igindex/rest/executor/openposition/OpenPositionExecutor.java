package com.javabi.igindex.rest.executor.openposition;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;

import com.javabi.common.xml.gson.IJson;
import com.javabi.igindex.rest.IRestService;
import com.javabi.igindex.rest.executor.HttpJsonRestExecutor;

public class OpenPositionExecutor extends HttpJsonRestExecutor<OpenPositionResponse> {

	private final OpenPositionRequest jsonRequest;

	public OpenPositionExecutor(IRestService platform, OpenPositionRequest jsonRequest) {
		super(platform);
		if (jsonRequest == null) {
			throw new NullPointerException("jsonRequest");
		}
		this.jsonRequest = jsonRequest;
	}

	@Override
	public int getRequestVersion() {
		return 2;
	}

	@Override
	public HttpUriRequest createRequest() {
		HttpPost request = new HttpPost(getUrl("/positions/otc"));
		addHeaders(request);
		setRequestContent(request, jsonRequest);
		return request;
	}

	@Override
	public OpenPositionResponse createResponse(IJson json) {
		return new OpenPositionResponse(json);
	}

}
