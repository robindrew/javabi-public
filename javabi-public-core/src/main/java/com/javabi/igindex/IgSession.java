package com.javabi.igindex;

public class IgSession {

	private final IgCredentials credentials;
	private final IgEnvironment environment;

	private String clientSecurityToken;
	private String accountSecurityToken;
	private String lightstreamerEndpoint;

	public IgSession(IgCredentials credentials, IgEnvironment environment) {
		this.credentials = credentials;
		this.environment = environment;
	}

	public IgCredentials getCredentials() {
		return credentials;
	}

	public IgEnvironment getEnvironment() {
		return environment;
	}

	public String getClientSecurityToken() {
		return clientSecurityToken;
	}

	public String getAccountSecurityToken() {
		return accountSecurityToken;
	}

	public String getLightstreamerEndpoint() {
		return lightstreamerEndpoint;
	}

	public void setClientSecurityToken(String token) {
		if (token.isEmpty()) {
			throw new IllegalArgumentException("token is empty");
		}
		this.clientSecurityToken = token;
	}

	public void setAccountSecurityToken(String token) {
		if (token.isEmpty()) {
			throw new IllegalArgumentException("token is empty");
		}
		this.accountSecurityToken = token;
	}

	public void setLightstreamerEndpoint(String endpoint) {
		if (endpoint.isEmpty()) {
			throw new IllegalArgumentException("token is empty");
		}
		this.lightstreamerEndpoint = endpoint;
	}

	@Override
	public String toString() {
		return credentials + "/" + environment;
	}

}
