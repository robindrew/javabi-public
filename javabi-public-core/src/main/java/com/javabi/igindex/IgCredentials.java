package com.javabi.igindex;

public class IgCredentials {

	private final String apiKey;
	private final String username;
	private final String password;

	public IgCredentials(String apiKey, String username, String password) {
		this.apiKey = apiKey;
		this.username = username;
		this.password = password;
	}

	public String getApiKey() {
		return apiKey;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return username + "/" + apiKey;
	}

}
