package com.javabi.igindex.streaming.subscription.charttick;

import static com.javabi.common.collect.PopulatingMap.createConcurrentMap;
import static com.javabi.common.text.StringFormats.number;
import static com.javabi.igindex.streaming.subscription.charttick.ChartTickSubscription.FIELD_BID;
import static com.javabi.igindex.streaming.subscription.charttick.ChartTickSubscription.FIELD_OFR;
import static com.javabi.igindex.streaming.subscription.charttick.ChartTickSubscription.FIELD_UTM;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.collect.PopulatingMap;
import com.javabi.common.date.Delay;
import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.io.stream.sink.subscriber.PriceCandleSubscriberStreamSink;
import com.javabi.investor.provider.igindex.IgInstrument;
import com.lightstreamer.ls_client.HandyTableListener;
import com.lightstreamer.ls_client.UpdateInfo;

public class ChartTickSubscriptionListener extends PriceCandleSubscriberStreamSink implements HandyTableListener {

	private static final Logger log = LoggerFactory.getLogger(ChartTickSubscriptionListener.class);

	private static final PopulatingMap<String, AtomicLong> countMap = createConcurrentMap(key -> new AtomicLong(0));

	private final AtomicBoolean active = new AtomicBoolean(true);
	private Delay loggingDelay = new Delay(1, TimeUnit.MINUTES);

	private final IgInstrument instrument;

	public ChartTickSubscriptionListener(IgInstrument instrument) {
		super(instrument.getInstrument());
		this.instrument = instrument;
	}

	@Override
	public void onUpdate(int itemPos, String itemName, UpdateInfo updateInfo) {
		long count = increment(itemName);

		// Extract the information we are interested in
		String timestamp = updateInfo.getNewValue(FIELD_UTM);
		String bid = updateInfo.getNewValue(FIELD_BID);
		String offer = updateInfo.getNewValue(FIELD_OFR);

		boolean bidInvalid = isInvalid(bid);
		boolean offerInvalid = isInvalid(offer);

		// Invalid update?
		if (isInvalid(timestamp) || (bidInvalid && offerInvalid)) {
			log.debug("[Invalid Tick] - onUpdate(" + itemPos + ", " + itemName + ", " + updateInfo + ")");
			return;
		}

		if (bidInvalid) {
			bid = updateInfo.getOldValue(FIELD_BID);
			if (isInvalid(bid)) {
				log.debug("[Invalid Tick] - onUpdate(" + itemPos + ", " + itemName + ", " + updateInfo + ")");
				return;
			}
		}
		if (offerInvalid) {
			offer = updateInfo.getOldValue(FIELD_OFR);
			if (isInvalid(offer)) {
				log.debug("[Invalid Tick] - onUpdate(" + itemPos + ", " + itemName + ", " + updateInfo + ")");
				return;
			}
		}

		// Enqueue tick ...
		final IChartTick tick;
		try {
			tick = new ChartTick(itemName, timestamp, bid, offer);
		} catch (Exception e) {
			log.warn("[Invalid Tick] - onUpdate(" + itemPos + ", " + itemName + ", " + updateInfo + ")", e);
			return;
		}

		// Verify instrument ...
		if (!getIgInstrument().equals(tick.getInstrument())) {
			log.warn("[Invalid Tick] - onUpdate(" + itemPos + ", " + itemName + ", " + updateInfo + ") expected=" + getInstrument() + ", actual=" + tick.getInstrument());
			return;
		}

		// Consume tick
		try {
			IPriceCandle candle = tick.toPriceCandle();
			putNextCandle(candle);
		} catch (IOException e) {
			log.error("Error consuming tick", e);
		}

		// As these are ticks (spam!) only log after a period of time
		if (loggingDelay.expired(true)) {
			log.info("onUpdate(" + itemName + ") called " + number(count) + " times");
		}
	}

	private long increment(String key) {
		AtomicLong count = countMap.get(key, true);
		return count.incrementAndGet();
	}

	private boolean isInvalid(String text) {
		return text == null || text.isEmpty() || text.equals("null") || text.equals("NULL");
	}

	@Override
	public void onSnapshotEnd(int itemPos, String itemName) {
		log.info("onSnapshotEnd(" + itemPos + ", " + itemName + ")");
	}

	@Override
	public void onRawUpdatesLost(int itemPos, String itemName, int lostUpdates) {
		log.info("onRawUpdatesLost(" + itemPos + ", " + itemName + ", lostUpdates=" + lostUpdates + ")");
	}

	@Override
	public void onUnsubscr(int itemPos, String itemName) {
		log.info("onUnsubscr(" + itemPos + ", " + itemName + ")");
		active.set(false);
	}

	@Override
	public void onUnsubscrAll() {
		log.info("onUnsubscrAll()");
		active.set(false);
	}

	public boolean isActive() {
		return active.get();
	}

	public IgInstrument getIgInstrument() {
		return instrument;
	}

}
