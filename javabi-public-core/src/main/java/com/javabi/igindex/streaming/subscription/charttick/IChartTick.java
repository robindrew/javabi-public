package com.javabi.igindex.streaming.subscription.charttick;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.tick.IPriceTick;
import com.javabi.investor.provider.igindex.IgInstrument;

public interface IChartTick extends IPriceTick {

	IgInstrument getInstrument();

	IPriceCandle toPriceCandle();

}