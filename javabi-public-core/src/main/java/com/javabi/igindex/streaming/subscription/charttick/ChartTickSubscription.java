package com.javabi.igindex.streaming.subscription.charttick;

import java.util.List;

import com.google.common.collect.Lists;
import com.javabi.igindex.IgException;
import com.javabi.igindex.streaming.subscription.FieldSet;
import com.javabi.igindex.streaming.subscription.IStreamingSubscription;
import com.javabi.investor.provider.igindex.IgInstrument;
import com.lightstreamer.ls_client.ExtendedTableInfo;
import com.lightstreamer.ls_client.HandyTableListener;
import com.lightstreamer.ls_client.SubscrException;

public class ChartTickSubscription implements IStreamingSubscription {

	public static final String FIELD_BID = "BID";
	public static final String FIELD_OFR = "OFR";
	public static final String FIELD_LTP = "LTP";
	public static final String FIELD_LTV = "LTV";
	public static final String FIELD_UTM = "UTM";
	public static final String FIELD_DAY_OPEN_MID = "DAY_OPEN_MID";
	public static final String FIELD_DAY_PERC_CHG_MID = "DAY_PERC_CHG_MID";
	public static final String FIELD_DAY_HIGH = "DAY_HIGH";
	public static final String FIELD_DAY_LOW = "DAY_LOW";

	private final IgInstrument instrument;
	private final FieldSet fields;
	private final ChartTickSubscriptionListener listener;

	public ChartTickSubscription(IgInstrument instrument, ChartTickSubscriptionListener listener, List<String> fields) {
		if (instrument == null) {
			throw new NullPointerException("instrument");
		}
		if (listener == null) {
			throw new NullPointerException("listener");
		}
		if (fields.isEmpty()) {
			throw new IllegalArgumentException("fields is empty");
		}
		this.instrument = instrument;
		this.listener = listener;
		this.fields = new FieldSet(fields);

	}

	public ChartTickSubscription(IgInstrument instrument, ChartTickSubscriptionListener listener) {
		this(instrument, listener, Lists.newArrayList(FIELD_UTM, FIELD_BID, FIELD_OFR));
	}

	public String getSubscriptionKey(IgInstrument instrument) {
		return "CHART:" + instrument.getEpic() + ":TICK";
	}

	public String getType() {
		return "DISTINCT";
	}

	public ExtendedTableInfo getTableInfo() {
		try {
			String subscriptionKey = getSubscriptionKey(instrument);
			return new ExtendedTableInfo(new String[] { subscriptionKey }, "DISTINCT", fields.toArray(), true);
		} catch (SubscrException e) {
			throw new IgException(e);
		}
	}

	@Override
	public HandyTableListener getListener() {
		return listener;
	}

}
