package com.javabi.igindex.streaming;

import com.javabi.igindex.IgException;
import com.javabi.igindex.IgSession;
import com.javabi.igindex.streaming.subscription.IStreamingSubscription;
import com.lightstreamer.ls_client.ConnectionInfo;
import com.lightstreamer.ls_client.ConnectionListener;
import com.lightstreamer.ls_client.ExtendedTableInfo;
import com.lightstreamer.ls_client.HandyTableListener;
import com.lightstreamer.ls_client.LSClient;

public class StreamingServiceConnection {

	private final IgSession session;
	private final LSClient client = new LSClient();

	public StreamingServiceConnection(IgSession session) {
		if (session == null) {
			throw new NullPointerException("session");
		}
		this.session = session;
	}

	public ConnectionInfo getInfo() {
		ConnectionInfo info = new ConnectionInfo();
		info.user = session.getCredentials().getUsername();
		info.password = "CST-" + session.getClientSecurityToken() + "|XST-" + session.getAccountSecurityToken();
		info.pushServerUrl = session.getLightstreamerEndpoint();
		return info;
	}

	public void connect(ConnectionListener listener) {
		try {
			ConnectionInfo info = getInfo();
			client.openConnection(info, listener);
		} catch (Exception e) {
			throw new IgException(e);
		}
	}

	public void disconnect() {
		client.closeConnection();
	}

	public void subscribe(IStreamingSubscription subscription) {
		try {
			ExtendedTableInfo tableInfo = subscription.getTableInfo();
			HandyTableListener listener = subscription.getListener();
			client.subscribeTable(tableInfo, listener, false);
		} catch (Exception e) {
			throw new IgException(e);
		}
	}
}
