package com.javabi.igindex.streaming.subscription.charttick;

import com.javabi.investor.provider.igindex.IgInstrument;

public class ChartTickItemName {

	private static final String PREFIX = "CHART:";
	private static final String POSTFIX = ":TICK";

	private final String itemName;

	public ChartTickItemName(String itemName) {
		if (!itemName.startsWith(PREFIX)) {
			throw new IllegalArgumentException("itemName: '" + itemName + "'");
		}
		if (!itemName.endsWith(POSTFIX)) {
			throw new IllegalArgumentException("itemName: '" + itemName + "'");
		}
		this.itemName = itemName;
	}

	public String getItemName() {
		return itemName;
	}

	public IgInstrument getInstrument() {
		int index1 = PREFIX.length();
		int index2 = itemName.length() - POSTFIX.length();
		String epic = itemName.substring(index1, index2);
		return IgInstrument.forEpic(epic);
	}

}
