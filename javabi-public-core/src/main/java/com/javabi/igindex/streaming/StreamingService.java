package com.javabi.igindex.streaming;

import static com.javabi.common.concurrent.Threads.sleepUntil;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.igindex.IgSession;
import com.javabi.igindex.streaming.subscription.IStreamingSubscription;
import com.lightstreamer.ls_client.ConnectionInfo;
import com.lightstreamer.ls_client.ConnectionListener;
import com.lightstreamer.ls_client.PushConnException;
import com.lightstreamer.ls_client.PushServerException;

public class StreamingService implements IStreamingService {

	private static final Logger log = LoggerFactory.getLogger(StreamingService.class);

	private final IgSession session;
	private final Set<IStreamingSubscription> subscriptions = new CopyOnWriteArraySet<>();
	private final AtomicBoolean retry = new AtomicBoolean(false);

	public StreamingService(IgSession session) {
		if (session == null) {
			throw new NullPointerException("session");
		}
		this.session = session;
	}

	public void addSubscription(IStreamingSubscription subscription) {
		if (subscription == null) {
			throw new NullPointerException("subscription");
		}
		subscriptions.add(subscription);
	}

	@Override
	public void run() {
		retry.set(false);

		// Connect the client
		connectClient();

		// Sleep until the connection fails ...
		sleepUntil(retry, 10, SECONDS);
	}

	private void connectClient() {
		try {

			// Connect client
			StreamingServiceConnection client = new StreamingServiceConnection(session);
			client.connect(new Listener(client.getInfo()));

			// Create client
			for (IStreamingSubscription subscription : subscriptions) {
				client.subscribe(subscription);
			}

		} catch (Exception e) {
			log.warn("Failed to connect client", e);
			retry.set(true);
		}
	}

	private class Listener implements ConnectionListener {

		private final ConnectionInfo info;

		public Listener(ConnectionInfo info) {
			this.info = info;
		}

		@Override
		public void onConnectionEstablished() {
			log.info("Connection Established: " + info.user + "@" + info.pushServerUrl);
		}

		@Override
		public void onSessionStarted(boolean isPolling) {
			log.info("Session Started (" + (isPolling ? "Polling" : "Non-Polling") + ")");
		}

		@Override
		public void onNewBytes(long bytes) {
			log.trace("onNewBytes(" + bytes + ")");
		}

		@Override
		public void onDataError(PushServerException e) {
			log.warn("Data Error", e);
		}

		@Override
		public void onActivityWarning(boolean warningOn) {
			log.warn("Activity Warning (" + (warningOn ? "Started" : "Finished") + ")");
		}

		@Override
		public void onClose() {
			log.info("Connection Closed");
			retry.set(true);
		}

		@Override
		public void onEnd(int cause) {
			log.warn("onEnd(" + cause + ")");
		}

		@Override
		public void onFailure(PushServerException e) {
			log.error("onFailure()", e);
		}

		@Override
		public void onFailure(PushConnException e) {
			log.error("onFailure()", e);
		}
	}
}
