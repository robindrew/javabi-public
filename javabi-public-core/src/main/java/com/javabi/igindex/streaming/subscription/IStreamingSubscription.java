package com.javabi.igindex.streaming.subscription;

import com.lightstreamer.ls_client.ExtendedTableInfo;
import com.lightstreamer.ls_client.HandyTableListener;

public interface IStreamingSubscription {

	ExtendedTableInfo getTableInfo();

	HandyTableListener getListener();

}
