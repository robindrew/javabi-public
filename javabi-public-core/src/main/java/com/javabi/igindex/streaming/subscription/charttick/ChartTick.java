package com.javabi.igindex.streaming.subscription.charttick;

import java.math.BigDecimal;

import com.javabi.investor.price.candle.IPriceCandle;
import com.javabi.investor.price.candle.PriceCandleInstant;
import com.javabi.investor.price.tick.PriceTick;
import com.javabi.investor.provider.igindex.IgInstrument;
import com.javabi.investor.provider.pcf.FloatingPoint;

public class ChartTick extends PriceTick implements IChartTick {

	private final IgInstrument instrument;

	public ChartTick(String itemName, String timestamp, String bid, String ask) {
		super(Long.parseLong(timestamp), new BigDecimal(bid), new BigDecimal(ask));
		this.instrument = new ChartTickItemName(itemName).getInstrument();
	}

	@Override
	public IgInstrument getInstrument() {
		return instrument;
	}

	@Override
	public IPriceCandle toPriceCandle() {
		BigDecimal mid = getMid();
		int decimalPlaces = instrument.getDecimalPlaces();
		int price = FloatingPoint.toBigInt(mid, decimalPlaces);
		return new PriceCandleInstant(price, getTimestamp(), decimalPlaces);
	}

}
