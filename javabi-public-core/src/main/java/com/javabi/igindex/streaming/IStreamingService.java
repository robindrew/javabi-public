package com.javabi.igindex.streaming;

import com.javabi.igindex.streaming.subscription.IStreamingSubscription;

public interface IStreamingService extends Runnable {

	void addSubscription(IStreamingSubscription subscription);

}
