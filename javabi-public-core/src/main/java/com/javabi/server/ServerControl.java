package com.javabi.server;

import static com.javabi.common.dependency.DependencyFactory.getDependency;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.javabi.common.collect.SizeLimitedList;
import com.javabi.common.concurrent.thread.ILoopingThreadControl;
import com.javabi.common.concurrent.thread.LoopingThreadControl;
import com.javabi.common.io.connection.IConnection;
import com.javabi.common.io.connection.INetworkAddress;
import com.javabi.common.io.connection.SocketConnection;
import com.javabi.common.lang.Quietly;
import com.javabi.common.service.control.Control;
import com.javabi.server.connection.IConnectionHandler;
import com.javabi.server.connection.IConnectionHandlerFactory;

public class ServerControl extends Control implements IServerControl {

	private static final Logger log = LoggerFactory.getLogger(ServerControl.class);

	private final AtomicLong connectionCount = new AtomicLong(0);
	private final ServerSocket server;
	private final ExecutorService connectionThreadPool;
	private final ILoopingThreadControl acceptThread;
	private final INetworkAddress address;
	private final SizeLimitedList<NumberedConnection> latestConnectionList = new SizeLimitedList<NumberedConnection>(50);

	public ServerControl(String threadName, INetworkAddress address, int threads, ThreadFactory factory) {
		this(threadName, address, Executors.newFixedThreadPool(threads, factory));
	}

	public ServerControl(String threadName, INetworkAddress address, int threads) {
		this(threadName, address, Executors.newFixedThreadPool(threads));
	}

	public ServerControl(String threadName, INetworkAddress address, ExecutorService threadPool) {
		super(threadName);
		if (address == null) {
			throw new NullPointerException("address");
		}
		if (threadPool == null) {
			throw new NullPointerException("threadPool");
		}
		this.address = address;

		// Connection handling thread pool
		this.connectionThreadPool = threadPool;

		// Accept thread
		ThreadFactory factory = new ThreadFactoryBuilder().setDaemon(false).setNameFormat(threadName).build();
		this.acceptThread = new LoopingThreadControl(threadName + "Loop", new ServerRunnable(), factory);

		try {

			log.info("Opening server socket: " + address);
			server = new ServerSocket();
			server.bind(address.toSocketAddress());

		} catch (Exception e) {
			throw new ServerException("Error binding server to: " + address, e);
		}
	}

	@Override
	public long getConnectionCount() {
		return connectionCount.get();
	}

	@Override
	public List<NumberedConnection> getLatestConnectionList() {
		synchronized (latestConnectionList) {
			return new ArrayList<NumberedConnection>(latestConnectionList);
		}
	}

	private void nextConnection() {
		Socket socket = null;
		try {
			socket = server.accept();
			long number = connectionCount.addAndGet(1);
			NumberedConnection numbered = new NumberedConnection(number, socket);
			synchronized (latestConnectionList) {
				latestConnectionList.add(numbered);
			}
			log.info("New connection " + numbered);

			IConnection connection = new SocketConnection(socket);
			IConnectionHandlerFactory handlerFactory = getConnectionHandlerFactory();
			IConnectionHandler handler = handlerFactory.newConnection(connection, number);
			connectionThreadPool.execute(handler);

		} catch (Exception e) {
			Quietly.close(socket);

			// Socket closed?
			if (e instanceof SocketException) {
				if ("socket closed".equals(e.getMessage())) {
					log.warn("ServerControl socket closed");
					return;
				}
			}

			// Unexpected error
			log.error("Error accepting client connection", e);
		}
	}

	protected IConnectionHandlerFactory getConnectionHandlerFactory() {
		return getDependency(IConnectionHandlerFactory.class);
	}

	@Override
	public void startupControl() {
		acceptThread.startup();
	}

	@Override
	public void shutdownControl() {
		log.warn("ServerControl shutting down");

		// Shutdown accept thread
		try {
			log.info("Shutting down server accept thread");
			acceptThread.shutdown();
		} catch (Exception e) {
			log.error("Error shutting down server", e);
		}

		// Shutdown connection thread pool
		try {
			log.info("Shutting down server thread pool");
			if (!connectionThreadPool.isShutdown()) {
				connectionThreadPool.shutdown();
			}
		} catch (Exception e) {
			log.error("Error shutting down server", e);
		}

		// Shutdown server
		try {
			log.info("Closing server socket: " + server);
			server.close();
		} catch (Exception e) {
			log.error("Error shutting down server", e);
		}
	}

	private class ServerRunnable implements Runnable {

		@Override
		public void run() {
			nextConnection();
		}

	}

	@Override
	public INetworkAddress getAddress() {
		return address;
	}

}
