package com.javabi.server;

public class ServerException extends RuntimeException {

	private static final long serialVersionUID = 7452294540820957981L;

	private static Throwable getCause(Throwable cause) {
		while (cause instanceof ServerException) {
			ServerException exception = (ServerException) cause;
			if (!exception.isWrapped()) {
				break;
			}
			cause = exception.getCause();
		}
		return cause;
	}

	private boolean wrapped = false;

	public ServerException() {
	}

	public ServerException(String message) {
		super(message);
	}

	public ServerException(Throwable cause) {
		super(getCause(cause));
		this.wrapped = true;
	}

	public ServerException(String message, Throwable cause) {
		super(message, cause);
	}

	public boolean isWrapped() {
		return wrapped;
	}

}
