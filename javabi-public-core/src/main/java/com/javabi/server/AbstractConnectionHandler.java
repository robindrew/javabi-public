package com.javabi.server;

import static com.javabi.common.lang.Variables.min;
import static com.javabi.common.lang.Variables.notNull;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.common.io.connection.IConnection;
import com.javabi.common.lang.Quietly;
import com.javabi.server.connection.IConnectionHandler;

public abstract class AbstractConnectionHandler implements IConnectionHandler {

	private static final Logger log = LoggerFactory.getLogger(AbstractConnectionHandler.class);

	private final IConnection connection;
	private final long number;

	protected AbstractConnectionHandler(IConnection connection, long number) {
		this.connection = notNull("connection", connection);
		this.number = min("number", number, 1);
	}

	@Override
	public long getNumber() {
		return number;
	}

	@Override
	public IConnection getConnection() {
		return connection;
	}

	@Override
	public String toString() {
		return "#" + number + "/" + connection;
	}

	@Override
	public void run() {
		try {
			handle();
		} finally {
			handled();
		}
	}

	public void handle() {
		NanoTimer timer = new NanoTimer();
		timer.start();
		try {

			// Handle!
			handleConnection();

			// Flush (necessary)
			getConnection().getOutput().flush();

		} catch (Throwable t) {
			handleError(t);

		} finally {
			timer.stop();
			log.info("Connection Completed: " + this + " in " + timer);
		}
	}

	protected void handleError(Throwable t) {
		t = Throwables.getRootCause(t);
		log.error("Connection Error: " + this, t);
	}

	protected void handled() {
		Quietly.close(connection);
	}

	protected abstract void handleConnection() throws IOException;
}
