package com.javabi.server;

import java.net.Socket;

public interface INumberedConnection {

	long getNumber();

	Socket getSocket();

}
