package com.javabi.server.connection;

import com.javabi.common.io.connection.IConnection;

public interface IConnectionHandler extends Runnable {

	IConnection getConnection();

	long getNumber();

}
