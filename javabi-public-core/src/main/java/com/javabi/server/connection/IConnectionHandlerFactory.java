package com.javabi.server.connection;

import com.javabi.common.io.connection.IConnection;

public interface IConnectionHandlerFactory {

	IConnectionHandler newConnection(IConnection connection, long number);

}
