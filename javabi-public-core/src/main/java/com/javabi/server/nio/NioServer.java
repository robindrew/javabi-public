package com.javabi.server.nio;

import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.lang.Quietly;
import com.javabi.server.nio.connection.INioConnectionHandlerFactory;

public class NioServer implements INioServer, Runnable {

	private static final Logger log = LoggerFactory.getLogger(NioServer.class);

	private final AtomicBoolean shutdown = new AtomicBoolean(false);
	private final ServerSocketChannel server;
	private final INioSelector selector;
	private final Thread thread;
	private int timeout = 50;

	public NioServer(String name, String host, int port, ExecutorService executor, INioConnectionHandlerFactory factory) {
		InetSocketAddress address = new InetSocketAddress(host, port);
		try {

			this.thread = new Thread(this, name);
			this.server = ServerSocketChannel.open();
			this.server.socket().bind(address);
			this.server.configureBlocking(false);
			this.selector = new NioSelector(server, executor, factory);

		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public void run() {
		try {
			while (!shutdown.get()) {
				if (!selector.select(timeout)) {
					// Why is this sleep necessary?
					// Without it reads don't seem to work ... weird.
					Thread.sleep(timeout);
				}
			}
		} catch (Exception e) {
			log.error("Error accepting connections", e);
		} finally {
			shutdown.set(true);
		}
	}

	@Override
	public void startup() {
		thread.start();
	}

	@Override
	public void shutdown() {
		Quietly.close(server);
	}
}
