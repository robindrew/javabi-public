package com.javabi.server.nio;

import com.javabi.common.service.IShutdownable;
import com.javabi.common.service.IStartupable;

public interface INioServer extends IShutdownable, IStartupable {

}
