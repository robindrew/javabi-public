package com.javabi.server.nio.connection;

import java.nio.channels.SocketChannel;

import com.javabi.common.io.connection.IConnection;

public interface INioConnection extends IConnection {

	SocketChannel getConnection();

}
