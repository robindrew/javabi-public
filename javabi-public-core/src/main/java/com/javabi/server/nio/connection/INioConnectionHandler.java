package com.javabi.server.nio.connection;

public interface INioConnectionHandler extends Runnable {

	INioConnection getConnection();

}
