package com.javabi.server.nio.connection;

public interface INioConnectionHandlerFactory {

	INioConnectionHandler newConnectionHandler(INioConnection connection);

}
