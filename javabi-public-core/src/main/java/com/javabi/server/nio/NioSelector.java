package com.javabi.server.nio;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.server.nio.connection.INioConnection;
import com.javabi.server.nio.connection.INioConnectionHandler;
import com.javabi.server.nio.connection.INioConnectionHandlerFactory;
import com.javabi.server.nio.connection.NioConnection;

public class NioSelector implements INioSelector {

	private static final Logger log = LoggerFactory.getLogger(NioSelector.class);

	private final ServerSocketChannel server;
	private final Selector selector;
	private final ExecutorService handlerExecutor;
	private final INioConnectionHandlerFactory handlerFactory;

	public NioSelector(ServerSocketChannel server, ExecutorService handlerService, INioConnectionHandlerFactory handlerFactory) throws IOException {
		this.server = server;
		this.selector = Selector.open();
		this.handlerExecutor = handlerService;
		this.handlerFactory = handlerFactory;

		// Register the server for accept
		server.register(selector, SelectionKey.OP_ACCEPT);
	}

	@Override
	public boolean select(long timeout) throws IOException {
		if (selector.select(timeout) > 0) {
			selectKeys();
			return true;
		}
		return false;
	}

	private void selectKeys() {
		Iterator<SelectionKey> keys = selector.selectedKeys().iterator();
		while (keys.hasNext()) {
			SelectionKey key = keys.next();
			keys.remove();
			selectKey(key);
		}
	}

	private boolean selectKey(SelectionKey key) {
		if (!key.isValid()) {
			log.debug("Selected key cancelled");
			// Key cancelled
			return false;
		}
		if (key.isAcceptable()) {
			log.debug("Selected key acceptable");
			acceptKey(key);
			return true;
		}
		if (key.isReadable()) {
			log.debug("Selected key readable");
			readKey(key);
			return true;
		}
		return false;
	}

	private void readKey(SelectionKey key) {
		NioConnection connection = (NioConnection) key.attachment();
		connection.read();

		// Asynchronous handler
		if (connection.handle()) {
			INioConnectionHandler handler = handlerFactory.newConnectionHandler(connection);
			handlerExecutor.submit(handler);
		}
	}

	private void acceptKey(SelectionKey key) {
		try {
			SocketChannel client = server.accept();
			log.info("New Connection " + client);

			client.configureBlocking(false);
			client.socket().setTcpNoDelay(true);

			// Register the client for accept
			INioConnection connection = new NioConnection(client);
			client.register(selector, SelectionKey.OP_READ, connection);

		} catch (Exception e) {
			log.error("Error accepting connection", e);
		}
	}

}
