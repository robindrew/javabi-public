package com.javabi.server.nio;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;

import com.javabi.server.nio.connection.INioConnection;
import com.javabi.server.nio.connection.INioConnectionHandler;
import com.javabi.server.nio.connection.INioConnectionHandlerFactory;

public class TestHandler implements INioConnectionHandler, INioConnectionHandlerFactory {

	private static final AtomicInteger connectionNumber = new AtomicInteger(0);

	private final int number;
	private final INioConnection connection;

	public TestHandler() {
		number = 0;
		connection = null;
	}

	public TestHandler(INioConnection connection) {
		this.connection = connection;
		this.number = connectionNumber.addAndGet(1);
	}

	@Override
	public INioConnectionHandler newConnectionHandler(INioConnection connection) {
		return new TestHandler(connection);
	}

	@Override
	public INioConnection getConnection() {
		return connection;
	}

	@Override
	public void run() {
		System.err.println(number + " ALIVE");
		InputStream input = connection.getInput();
		try {
			while (true) {
				int read = input.read();
				if (read == -1) {
					break;
				}
				System.err.print(number);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.err.println(number + " DEAD");
	}

}
