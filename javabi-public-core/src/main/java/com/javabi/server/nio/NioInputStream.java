package com.javabi.server.nio;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class NioInputStream extends InputStream {

	private static final byte[] EMPTY = new byte[0];

	private int writeLength = 0;
	private byte[] writeBuffer = EMPTY;

	private volatile int readOffset = 0;
	private volatile int readLength = 0;
	private volatile byte[] readBuffer = EMPTY;

	private volatile boolean closed = false;

	public synchronized void write(ByteBuffer buffer) {

		// Empty write buffer
		if (writeLength == 0) {
			int limit = buffer.limit();
			writeBuffer = new byte[limit * 2];
			buffer.get(writeBuffer, 0, limit);
			writeLength = limit;
			return;
		}

		// writeBuffer = append(buffer, writeBuffer);

		notify();
	}

	public synchronized void close() {
		closed = true;
		notify();
	}

	@Override
	public int read() throws IOException {
		if (closed) {
			return -1;
		}
		if (readOffset == readLength) {
			if (!blockUntilBytesAvailable()) {
				return -1;
			}
		}
		return readBuffer[readOffset++];
	}

	@Override
	public int read(byte[] array, int offset, int length) throws IOException {
		if (closed) {
			return -1;
		}
		if (readOffset == readLength) {
			if (!blockUntilBytesAvailable()) {
				return -1;
			}
		}
		int available = readLength - readOffset;
		if (length > available) {
			length = available;
		}
		System.arraycopy(readBuffer, readOffset, array, offset, length);
		readOffset += length;
		return length;
	}

	private synchronized boolean blockUntilBytesAvailable() {

		// Wait until write buffer has some data
		while (writeLength == 0 && !closed) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}

		// Transfer data from write buffer to read buffer
		return transfer();
	}

	private boolean transfer() {
		if (closed) {
			return false;
		}

		// Transfer to read buffer
		readOffset = 0;
		readLength = writeLength;
		readBuffer = writeBuffer;

		// Reset write buffer
		writeLength = 0;
		writeBuffer = EMPTY;

		return true;
	}

}
