package com.javabi.server.nio.connection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.io.connection.INetworkAddress;
import com.javabi.common.lang.Quietly;
import com.javabi.server.nio.NioInputStream;

public class NioConnection implements INioConnection {

	private static final Logger log = LoggerFactory.getLogger(NioConnection.class);

	private final SocketChannel connection;
	private volatile ByteBuffer readBuffer = ByteBuffer.allocate(10000);
	private volatile NioInputStream input = new NioInputStream();
	private final AtomicBoolean handled = new AtomicBoolean(false);

	public NioConnection(SocketChannel connection) {
		this.connection = connection;
	}

	@Override
	public SocketChannel getConnection() {
		return connection;
	}

	public boolean handle() {
		return handled.compareAndSet(false, true);
	}

	public void read() {
		if (!isConnected()) {
			return;
		}
		try {
			int read = connection.read(readBuffer);

			// Closed
			if (read == -1) {
				log.debug("Connection closed: " + connection);
				close();
				return;
			}
			log.debug("Connection read " + read + " bytes: " + connection);

			// Bytes read?
			if (read > 0) {
				flushBufferToInput();
			}

		} catch (IOException e) {
			log.error("Error reading from connection" + connection, e);
			close();
		}
	}

	private void flushBufferToInput() {
		readBuffer.flip();
		input.write(readBuffer);
		readBuffer.clear();
	}

	@Override
	public String toString() {
		return connection.toString();
	}

	@Override
	public INetworkAddress getRemoteAddress() {
		throw new UnsupportedOperationException();
	}

	@Override
	public INetworkAddress getLocalAddress() {
		throw new UnsupportedOperationException();
	}
	@Override
	public InputStream getInput() {
		return input;
	}

	@Override
	public OutputStream getOutput() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isConnected() {
		return readBuffer != null;
	}

	@Override
	public void disconnect() {
		close();
	}

	@Override
	public void close() {
		readBuffer = null;
		Quietly.close(input);
		Quietly.close(connection);
	}

}
