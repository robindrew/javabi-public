package com.javabi.server.nio;

import java.io.IOException;

public interface INioSelector {

	boolean select(long timeout) throws IOException;

}
