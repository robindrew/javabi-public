package com.javabi.server;

import java.net.Socket;

public class NumberedConnection implements INumberedConnection {

	private final long number;
	private final Socket socket;

	public NumberedConnection(long number, Socket socket) {
		this.number = number;
		this.socket = socket;
	}

	public long getNumber() {
		return number;
	}

	public Socket getSocket() {
		return socket;
	}

	@Override
	public String toString() {
		return "#" + number + " " + socket;
	}
}
