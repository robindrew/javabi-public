package com.javabi.server;

import java.util.List;

import com.javabi.common.io.connection.INetworkAddress;
import com.javabi.common.service.control.IControl;

public interface IServerControl extends IControl {

	long getConnectionCount();

	List<NumberedConnection> getLatestConnectionList();

	INetworkAddress getAddress();
}
