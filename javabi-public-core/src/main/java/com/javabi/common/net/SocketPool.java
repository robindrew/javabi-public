package com.javabi.common.net;

import java.io.IOException;
import java.net.Socket;

/**
 * A Socket Pool.
 */
public interface SocketPool {

	/**
	 * Lock and return a socket.
	 * @return the socket.
	 */
	Socket lockSocket() throws IOException;

	/**
	 * Unlock the given socket.
	 * @param socket the socket.
	 * @param reconnect true to reconnect the socket.
	 */
	void unlockSocket(Socket socket, boolean reconnect) throws IOException;

}
