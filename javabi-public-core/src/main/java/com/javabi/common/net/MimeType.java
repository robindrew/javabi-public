package com.javabi.common.net;

import java.util.HashMap;
import java.util.Map;

/**
 * MIME Types.
 */
public enum MimeType {

	/** The PLAIN mime type. */
	TEXT_PLAIN("text", "plain", "txt", "text"),
	/** The HTML mime type. */
	TEXT_HTML("text", "html", "htm"),
	/** The XML mime type. */
	TEXT_XML("text", "xml"),
	/** The CSS mime type. */
	TEXT_CSS("text", "css", "cs"),
	/** The CSV mime type. */
	TEXT_CSV("text", "csv", "csv"),
	/** The RTF mime type. */
	TEXT_RTF("text", "rtf", "rtf"),

	/** The MP3 mime type. */
	AUDIO_MPEG("audio", "mpeg", "mp3"),
	/** The WAV mime type. */
	AUDIO_X_WAV("audio", "x-wav", "wav"),
	/** The realplayer mime type. */
	AUDIO_X_PN_REALAUDIO("audio", "x-pn-realaudio", "ra", "ram"),

	/** The AVI mime type. */
	VIDEO_X_MSVIDEO("video", "x-msvideo", "avi"),
	/** The MPEG mime type. */
	VIDEO_MPEG("video", "mpeg", "mpg"),
	/** The quicktime mime type. */
	VIDEO_QUICKTIME("video", "quicktime", "mov", "qt"),

	/** The JPEG mime type. */
	IMAGE_JPEG("image", "jpeg", "jpg"),
	/** The PNG mime type. */
	IMAGE_PNG("image", "png", "x-png"),
	/** The BMP mime type. */
	IMAGE_BMP("image", "bmp", "x-bmp"),
	/** The GIF mime type. */
	IMAGE_GIF("image", "gif"),
	/** The TIFF mime type. */
	IMAGE_TIFF("image", "tiff", "tif"),
	/** The TIFF mime type. */
	IMAGE_WBMP("image", "vnd.wap.wbmp", "wbmp"),

	/** The HTTP mime type. */
	APPLICATION_HTTP("application", "http", "http"),
	/** The ZIP mime type. */
	APPLICATION_ZIP("application", "zip", "zip"),
	/** The GZIP mime type. */
	APPLICATION_X_GZIP("application", "x-gzip", "gz", "gzip"),
	/** The TAR GZIP mime type. */
	APPLICATION_X_COMPRESSED("application", "x-compressed", "tgz"),
	/** The TAR mime type. */
	APPLICATION_X_TAR("application", "x-tar", "tar"),
	/** The JavaScript mime type. */
	APPLICATION_JAVASCRIPT("application", "javascript", "js"),
	/** The JAR type. */
	APPLICATION_JAVA_ARCHIVE("application", "java-archive", "jar"),
	/** The octet stream mime type. */
	APPLICATION_OCTET_STREAM("application", "octet-stream"),
	/** The URL encoded mime type. */
	APPLICATION_X_WWW_FORM_URLENCODED("application", "x-www-form-urlencoded"),
	/** The shockwave flash mime type. */
	APPLICATION_X_SHOCKWAVE_FLASH("application", "x-shockwave-flash"),

	/** The multipart form data type. */
	MULTIPART_FORM_DATA("multipart", "form-data"),
	/** The multipart mixed. */
	MULTIPART_MIXED("multipart", "mixed");

	/** The type map. */
	private static final Map<String, MimeType> fullNameToTypeMap = new HashMap<String, MimeType>();

	/**
	 * Parse and return a MIME type from the given string.
	 * @param type the type string.
	 * @return the MIME type.
	 */
	public static MimeType parseMimeType(String type) {
		return parseMimeType(type, true);
	}

	/**
	 * Parse and return a MIME type from the given string.
	 * @param type the type string.
	 * @param throwException true to throw an exception if not found.
	 * @return the MIME type.
	 */
	private static MimeType parseMimeType(String type, boolean throwException) {
		synchronized (fullNameToTypeMap) {
			if (fullNameToTypeMap.isEmpty()) {
				for (MimeType mimeType : values()) {
					for (String fullType : mimeType.getTypes()) {
						fullNameToTypeMap.put(fullType, mimeType);
					}
				}
			}
			MimeType mimeType = fullNameToTypeMap.get(type.toLowerCase());
			if (mimeType == null && throwException) {
				throw new IllegalArgumentException("unknown MIME type: '" + type + "'");
			}
			return mimeType;
		}
	}

	/** The full type. */
	private final String fullType;
	/** The full types. */
	private final String[] fullTypes;
	/** The super type. */
	private final String superType;
	/** The sub types. */
	private final String[] subTypes;

	/**
	 * Returns true if this is a text type.
	 * @return true if this is a text type.
	 */
	public boolean isText() {
		return superType.equals("text");
	}

	/**
	 * Returns true if this is an image type.
	 * @return true if this is an image type.
	 */
	public boolean isImage() {
		return superType.equals("image");
	}

	/**
	 * Returns true if this is a video type.
	 * @return true if this is a video type.
	 */
	public boolean isVideo() {
		return superType.equals("video");
	}

	/**
	 * Returns true if this is an audio type.
	 * @return true if this is an audio type.
	 */
	public boolean isAudio() {
		return superType.startsWith("audio");
	}

	/**
	 * Returns true if this is an application type.
	 * @return true if this is an application type.
	 */
	public boolean isApplication() {
		return superType.startsWith("application");
	}

	/**
	 * Returns this type.
	 * @return this type.
	 */
	public String get() {
		return fullType;
	}

	/**
	 * Returns the full types.
	 * @return the full types.
	 */
	public String[] getTypes() {
		return fullTypes.clone();
	}

	/**
	 * Returns the sub-types.
	 * @return the sub-types.
	 */
	public String[] getSubTypes() {
		return subTypes.clone();
	}

	/**
	 * Returns true if this is the given type.
	 * @param type the type.
	 * @return true if this is the given type.
	 */
	public boolean isType(String type) {
		return parseMimeType(type, false) == this;
	}

	/**
	 * Returns this as a string.
	 * @return this as a string.
	 */
	public String toString() {
		return fullType;
	}

	/**
	 * Creates a new MimeType.
	 * @param superType the super type.
	 * @param subTypes the sub types.
	 */
	MimeType(String superType, String... subTypes) {
		if (superType == null) {
			throw new NullPointerException();
		}
		this.superType = superType;
		this.subTypes = subTypes.clone();
		this.fullTypes = new String[subTypes.length];
		for (int i = 0; i < subTypes.length; i++) {
			fullTypes[i] = (superType + '/' + subTypes[i]).toString().intern();
		}
		this.fullType = fullTypes[0];
	}

}