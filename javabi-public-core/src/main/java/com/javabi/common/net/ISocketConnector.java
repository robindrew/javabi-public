package com.javabi.common.net;

import java.net.Socket;
import java.net.SocketAddress;

public interface ISocketConnector {

	Socket connect(SocketAddress address, int connectTimeoutInMillis);

}
