package com.javabi.common.net;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.util.SimpleResourcePool;

/**
 * A Simple Socket Pool.
 */
public class SimpleSocketPool extends SimpleResourcePool<Socket, IOException> implements SocketPool {

	/** The log. */
	private static final Logger log = LoggerFactory.getLogger(SimpleSocketPool.class);

	/** The address. */
	private final InetAddress address;
	/** The port. */
	private final int port;

	/**
	 * Creates a new socket pool.
	 * @param address the address.
	 * @param port the port.
	 * @param size the size.
	 */
	public SimpleSocketPool(InetAddress address, int port, int size) {
		super(size);
		if (address == null) {
			throw new NullPointerException();
		}
		if (port < 0) {
			throw new IllegalArgumentException("port=" + port);
		}
		this.address = address;
		this.port = port;
	}

	/**
	 * Handle the given SQL exception.
	 * @param exception the exception.
	 */
	protected void handleIOException(IOException exception) {
		exception.printStackTrace();
	}

	@Override
	protected boolean isClosed(Socket socket) {
		return socket.isClosed();
	}

	@Override
	protected void close(Socket socket) {
		try {
			socket.close();
		} catch (IOException e) {
			handleIOException(e);
		}
	}

	@Override
	protected Socket newResource() throws IOException {
		log.info("Connecting New Socket: " + address + ":" + port);

		Socket socket = Sockets.connect(address, port);
		// TODO: Check if keep alive and linger are ok?
		// socket.setKeepAlive(true);
		// socket.setSoLinger(false, 0);
		socket.setSoTimeout(0);
		return socket;
	}

	@Override
	public final Socket lockSocket() throws IOException {
		return lock();
	}

	@Override
	public final void unlockSocket(Socket socket, boolean reconnect) throws IOException {
		unlock(socket, reconnect);
	}

}
