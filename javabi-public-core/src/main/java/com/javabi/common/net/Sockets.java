package com.javabi.common.net;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;

public final class Sockets {

	public static Socket connect(String host, int port) throws IOException {
		try {
			return new Socket(host, port);
		} catch (Exception e) {
			throw new IOException("Failed to connect to " + host + ":" + port, e);
		}
	}

	public static Socket connect(SocketAddress address, int connectTimeoutInMillis) throws IOException {
		try {
			Socket socket = new Socket();
			socket.connect(address, connectTimeoutInMillis);
			return socket;
		} catch (Exception e) {
			throw new IOException("Failed to connect to " + address, e);
		}
	}

	public static Socket connect(InetAddress address, int port) throws IOException {
		try {
			return new Socket(address, port);
		} catch (Exception e) {
			throw new IOException("Failed to connect to " + address + ":" + port, e);
		}
	}

	private Sockets() {
	}

}
