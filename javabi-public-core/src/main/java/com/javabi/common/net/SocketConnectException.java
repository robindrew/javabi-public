package com.javabi.common.net;

public class SocketConnectException extends RuntimeException {

	public SocketConnectException(String message, Exception e) {
		super(message, e);
	}
}
