package com.javabi.common.net;

import java.net.URLEncoder;
import java.nio.charset.Charset;

import com.google.common.base.Charsets;
import com.google.common.base.Throwables;

public class UrlBuilder implements IUrlBuilder {

	private Charset encoding = Charsets.UTF_8;
	private StringBuilder url = new StringBuilder();
	private int parameters = 0;

	public UrlBuilder(String path) {
		if (!path.startsWith("http://") && !path.startsWith("https://")) {
			throw new IllegalArgumentException("path: '" + path + "'");
		}
		url.append(path);
	}

	@Override
	public void setEncoding(Charset encoding) {
		if (encoding == null) {
			throw new NullPointerException("encoding");
		}
		this.encoding = encoding;
	}

	@Override
	public String get() {
		return url.toString();
	}

	@Override
	public String toString() {
		return url.toString();
	}

	@Override
	public IUrlBuilder param(String key, Object value) {
		return param(key, value, true);
	}

	@Override
	public IUrlBuilder param(String key, Object value, boolean encode) {
		String text = value.toString();

		// Encode?
		try {
			key = URLEncoder.encode(key, encoding.name());
			text = URLEncoder.encode(text, encoding.name());
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}

		// Append
		if (parameters == 0) {
			url.append('?');
		} else {
			url.append('&');
		}
		url.append(key).append('=').append(text);
		parameters++;

		return this;
	}

}
