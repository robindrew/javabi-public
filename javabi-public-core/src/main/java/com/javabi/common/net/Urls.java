package com.javabi.common.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

import com.google.common.base.Throwables;
import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;

public class Urls {

	public static URL create(String url) {
		try {
			return new URL(url);
		} catch (MalformedURLException e) {
			throw Throwables.propagate(e);
		}
	}

	public static byte[] downloadToBytes(URL url) {
		try (InputStream input = url.openStream()) {
			return ByteStreams.toByteArray(input);
		} catch (IOException e) {
			throw Throwables.propagate(e);
		}
	}

	public static String downloadToString(URL url, Charset charset) {
		try (Reader reader = new InputStreamReader(url.openStream(), charset)) {
			return CharStreams.toString(reader);
		} catch (IOException e) {
			throw Throwables.propagate(e);
		}
	}
}
