package com.javabi.common.net;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

import com.google.common.base.Throwables;

/**
 * A collection of useful network address utility methods.
 */
public final class Inet4AddressUtil {

	/** The loopback address parts. */
	public static final byte[] LOOPBACK_PARTS = new byte[] { 127, 0, 0, 1 };
	/** The loopback address code. */
	public static final int LOOPBACK_CODE = getCode(LOOPBACK_PARTS);

	/**
	 * Inaccessible constructor
	 */
	private Inet4AddressUtil() {
	}

	public static InetAddress getByName(String host) {
		if (host.isEmpty()) {
			throw new IllegalArgumentException("host is empty");
		}
		try {
			return InetAddress.getByName(host);
		} catch (UnknownHostException e) {
			throw Throwables.propagate(e);
		}
	}

	public static int getCode(InetAddress address) {
		if (address == null) {
			throw new NullPointerException();
		}
		if (address instanceof Inet4Address) {
			return ((Inet4Address) address).hashCode();
		}
		throw new UnsupportedOperationException("unable to obtain code from address class " + address.getClass() + " (" + address + ")");
	}

	/**
	 * Returns the code for the parts.
	 * @param parts the four parts.
	 * @return the code.
	 * @see Inet4Address#getByAddress(byte[])
	 */
	public static int getCode(byte[] parts) {
		if (parts.length != 4) {
			throw new IllegalArgumentException("there must be exactly 4 parts");
		}
		// ESCA-JAVA0076: Magic numbers allowed
		int code = parts[3] & 0xFF;
		code |= ((parts[2] << 8) & 0xFF00);
		code |= ((parts[1] << 16) & 0xFF0000);
		code |= ((parts[0] << 24) & 0xFF000000);
		return code;
	}

	public static int getCode(String address) {
		if (address.length() < 7 || address.length() > 15) {
			throw new IllegalArgumentException("address: '" + address + "'");
		}

		// Find the dots ...
		int index1 = address.indexOf('.', 0);
		int index2 = address.indexOf('.', index1 + 1);
		int index3 = address.indexOf('.', index2 + 1);
		if (index1 < 1 || index2 < 1 || index3 < 1) {
			throw new IllegalArgumentException("address: '" + address + "'");
		}

		// Parse parts
		byte[] parts = new byte[4];
		parts[0] = (byte) Integer.parseInt(address.substring(0, index1));
		parts[1] = (byte) Integer.parseInt(address.substring(index1 + 1, index2));
		parts[2] = (byte) Integer.parseInt(address.substring(index2 + 1, index3));
		parts[3] = (byte) Integer.parseInt(address.substring(index3 + 1));
		return getCode(parts);
	}

	public static byte[] toByteArray(int code) {
		byte[] parts = new byte[4];
		parts[0] = (byte) ((code >>> 24) & 0xFF);
		parts[1] = (byte) ((code >>> 16) & 0xFF);
		parts[2] = (byte) ((code >>> 8) & 0xFF);
		parts[3] = (byte) (code & 0xFF);
		return parts;
	}

	public static String toString(byte[] parts) {
		StringBuilder text = new StringBuilder();
		text.append(toInt(parts[0]));
		text.append('.');
		text.append(toInt(parts[1]));
		text.append('.');
		text.append(toInt(parts[2]));
		text.append('.');
		text.append(toInt(parts[3]));
		return text.toString();
	}

	private static int toInt(int part) {
		if (part >= 0) {
			return part;
		}
		return part + 256;
	}

	public static String toString(int code) {
		byte[] parts = toByteArray(code);
		return toString(parts);
	}

	public static void main(String[] args) {
		System.out.println(getCode("213.141.21.218"));
	}

}
