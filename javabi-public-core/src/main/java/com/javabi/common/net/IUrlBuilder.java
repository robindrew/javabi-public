package com.javabi.common.net;

import java.nio.charset.Charset;

import com.google.common.base.Supplier;

public interface IUrlBuilder extends Supplier<String> {

	void setEncoding(Charset encoding);

	IUrlBuilder param(String key, Object value);

	IUrlBuilder param(String key, Object value, boolean encode);

}
