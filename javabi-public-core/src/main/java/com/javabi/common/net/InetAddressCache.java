package com.javabi.common.net;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Map;

/**
 * A Internet Address Cache
 * <p>
 * Thread safe cache for host name resolved addresses.
 */
public final class InetAddressCache {

	/** The singleton. */
	private static final InetAddressCache SINGLETON = new InetAddressCache();

	/**
	 * Returns the address cache.
	 * @return the address cache.
	 */
	public static InetAddressCache getInetAddressCache() {
		return SINGLETON;
	}

	/** The resolved address map. */
	private final Map<InetSocketAddress, InetSocketAddress> addressMap = new Hashtable<InetSocketAddress, InetSocketAddress>();
	/** The resolved map. */
	private final Map<String, InetAddress> resolvedHostMap = new Hashtable<String, InetAddress>();

	/**
	 * Singleton constructor.
	 */
	private InetAddressCache() {
	}

	/**
	 * Clear all cached resolved hosts.
	 */
	public void clear() {
		resolvedHostMap.clear();
	}

	/**
	 * Resolves the given host to an address.
	 * @param host the host.
	 * @param port the port.
	 * @return the address.
	 */
	public InetSocketAddress resolve(String host, int port) throws UnknownHostException {
		InetAddress address = resolve(host);
		InetSocketAddress socketAddress = new InetSocketAddress(address, port);
		InetSocketAddress cachedAddress = addressMap.get(socketAddress);
		if (cachedAddress == null) {
			addressMap.put(socketAddress, socketAddress);
			cachedAddress = socketAddress;
		}
		return cachedAddress;
	}

	/**
	 * Resolves the given host to an address.
	 * @param host the host.
	 * @return the address.
	 */
	public InetAddress resolve(String host) throws UnknownHostException {
		host = host.toLowerCase();
		InetAddress address = null;
		address = resolvedHostMap.get(host);
		if (address == null) {
			host = host.intern();
			synchronized (host) {
				address = resolvedHostMap.get(host);
				if (address == null) {
					address = InetAddress.getByName(host);
					resolvedHostMap.put(host, address);
				}
			}
		}
		return address;
	}
}
