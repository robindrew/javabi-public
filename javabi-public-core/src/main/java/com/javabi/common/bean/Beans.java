package com.javabi.common.bean;

import java.lang.reflect.Method;

import com.google.common.base.Throwables;

public class Beans {

	public static Method getMethod(Class<?> type, String prefix, String postfix) {
		String methodName = prefix + Character.toUpperCase(postfix.charAt(0)) + postfix.substring(1);
		for (Method method : type.getDeclaredMethods()) {
			if (method.getName().equals(methodName)) {
				return method;
			}
		}
		throw new IllegalStateException("method not found: " + methodName);
	}

	public static void set(Object type, String fieldName, Object value) {
		Method method = getMethod(type.getClass(), "set", fieldName);
		try {
			method.invoke(type, value);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	@SuppressWarnings("unchecked")
	public static <V> V get(Object type, String fieldName) {
		Method method = getMethod(type.getClass(), "get", fieldName);
		try {
			return (V) method.invoke(type);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

}
