package com.javabi.common.concurrent;

import static java.util.Collections.synchronizedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import com.google.common.base.Supplier;
import com.google.common.base.Throwables;

public class FailFastExecutor {

	public void executeAll(Supplier<ExecutorService> supplier, Collection<? extends Runnable> tasks) {
		ExecutorService service = supplier.get();
		Session session = new Session(tasks.size());
		try {
			for (Runnable task : tasks) {
				session.throwFailure();
				service.submit(() -> runTask(task, session));
			}
			session.awaitCompletion();
		} finally {
			service.shutdown();
		}
	}

	public <T> List<T> executeAndReturn(Supplier<ExecutorService> supplier, Collection<? extends Callable<T>> tasks) {
		ExecutorService service = supplier.get();
		Session session = new Session(tasks.size());
		try {
			List<T> results = synchronizedList(new ArrayList<>());
			for (Callable<T> task : tasks) {
				session.throwFailure();
				service.submit(() -> runTask(task, session, results));
			}
			session.awaitCompletion();
			return new ArrayList<T>(results);
		} finally {
			service.shutdown();
		}
	}

	private void runTask(Runnable task, Session session) {
		if (session.hasFailed()) {
			return;
		}
		try {
			task.run();
		} catch (Throwable t) {
			session.failed(t);
			Throwables.propagate(t);
		} finally {
			session.completed();
		}
	}

	private <T> T runTask(Callable<T> task, Session session, List<T> results) throws Exception {
		if (session.hasFailed()) {
			return null;
		}
		try {
			T result = task.call();
			results.add(result);
			return result;
		} catch (Exception e) {
			session.failed(e);
			throw e;
		} catch (Throwable t) {
			session.failed(t);
			throw Throwables.propagate(t);
		} finally {
			session.completed();
		}
	}

	private static class Session {

		private final AtomicInteger remainingTasks = new AtomicInteger();
		private final AtomicReference<Throwable> firstThrowable = new AtomicReference<Throwable>();

		private Session(int taskCount) {
			remainingTasks.set(taskCount);
		}

		public void awaitCompletion() {
			synchronized (this) {
				while (!hasFinished()) {
					Threads.await(this);
				}
			}
			throwFailure();
		}

		public boolean hasFinished() {
			return hasFailed() || remainingTasks.get() == 0;
		}

		public boolean hasFailed() {
			return firstThrowable.get() != null;
		}

		public void throwFailure() {
			Throwable failure = firstThrowable.get();
			if (failure != null) {
				Throwables.propagate(failure);
			}
		}

		public void failed(Throwable t) {
			firstThrowable.compareAndSet(null, t);
			synchronized (this) {
				notifyAll();
			}
		}

		public void completed() {
			remainingTasks.decrementAndGet();
			synchronized (this) {
				notifyAll();
			}
		}

	}
}
