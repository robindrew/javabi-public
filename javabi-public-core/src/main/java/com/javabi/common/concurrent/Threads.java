package com.javabi.common.concurrent;

import java.lang.management.ThreadInfo;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import com.javabi.common.date.UnitTime;

public class Threads {

	private static volatile IThreads instance = new ThreadsImpl();

	public static IThreads getThreads() {
		return instance;
	}

	public static void setThreads(IThreads threads) {
		if (threads == null) {
			throw new NullPointerException("threads");
		}
		instance = threads;
	}

	private Threads() {
		// Inaccessible constructor
	}

	public static int getThreadCount() {
		return instance.getThreadCount();
	}

	public static int getPeakThreadCount() {
		return instance.getPeakThreadCount();
	}

	public static ThreadInfo getThreadInfo(long id) {
		return instance.getThreadInfo(id);
	}

	public static Set<Thread> getAllThreads() {
		return instance.getAllThreads();
	}

	public static Map<Thread, StackTraceElement[]> getAllStackTraces() {
		return instance.getAllStackTraces();
	}

	public static void sleep(long millis) {
		instance.sleep(millis);
	}

	public static void sleep(long millis, int nanos) {
		instance.sleep(millis, nanos);
	}

	public static void sleep(long amount, TimeUnit unit) {
		instance.sleep(amount, unit);
	}

	public static void sleep(UnitTime time) {
		instance.sleep(time);
	}

	public static void sleepUntil(long timeInMillis) {
		instance.sleepUntil(timeInMillis);
	}

	public static void sleepForever() {
		instance.sleepForever();
	}

	public static void sleepUntil(AtomicBoolean wakeUp, long interval) {
		instance.sleepUntil(wakeUp, interval);
	}

	public static void sleepUntil(AtomicBoolean wakeUp, long interval, TimeUnit unit) {
		instance.sleepUntil(wakeUp, interval, unit);
	}

	public static void sleepUntil(AtomicBoolean wakeUp, UnitTime interval) {
		instance.sleepUntil(wakeUp, interval);
	}

	public static void await(Object object) {
		instance.await(object);
	}

	public static void terminate(ExecutorService service) {
		instance.terminate(service);
	}

	public static ThreadFactory newThreadFactory(String nameFormat) {
		return instance.newThreadFactory(nameFormat);
	}

	public static ThreadFactory newThreadFactory(String nameFormat, boolean daemon) {
		return instance.newThreadFactory(nameFormat, daemon);
	}

	public static ExecutorService newFixedThreadPoolt(String nameFormat, int threads) {
		return instance.newFixedThreadPoolt(nameFormat, threads);
	}

	public static ExecutorService newFixedThreadPool(ThreadFactory factory, int threads) {
		return instance.newFixedThreadPool(factory, threads);
	}

	public static ExecutorService newCachedThreadPool(String nameFormat) {
		return instance.newCachedThreadPool(nameFormat);
	}

	public static ExecutorService newCachedThreadPool(ThreadFactory factory) {
		return instance.newCachedThreadPool(factory);
	}

	public static Callable<?> callable(Runnable runnable) {
		return instance.callable(runnable);
	}

	public static <R> Callable<R> callable(Runnable runnable, R returnValue) {
		return instance.callable(runnable, returnValue);
	}

	public static void executeAll(String nameFormat, int threads, Collection<? extends Runnable> tasks) {
		instance.executeAll(nameFormat, threads, tasks);
	}

	public static <T> List<T> executeAndReturn(String nameFormat, int threads, Collection<? extends Callable<T>> tasks) {
		return instance.executeAndReturn(nameFormat, threads, tasks);
	}

}
