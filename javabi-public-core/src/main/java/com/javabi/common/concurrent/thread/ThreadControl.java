package com.javabi.common.concurrent.thread;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicReference;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.javabi.common.service.Status;
import com.javabi.common.service.control.Control;

public abstract class ThreadControl extends Control implements IThreadControl {

	private final AtomicReference<Thread> thread = new AtomicReference<Thread>();
	private final ThreadFactory factory;

	public ThreadControl(String name) {
		super(name);
		this.factory = new ThreadFactoryBuilder().setNameFormat(name).build();
	}

	public ThreadControl(String name, ThreadFactory factory) {
		super(name);
		if (factory == null) {
			throw new NullPointerException("factory");
		}
		this.factory = factory;
	}

	@Override
	protected void startupControl() throws Throwable {
		Thread startThread = factory.newThread(getRunnable());
		if (!thread.compareAndSet(null, startThread)) {
			throw new IllegalStateException("Thread already set");
		}
		startThread.start();
	}

	@Override
	public boolean isRunning() {
		return hasStatus(Status.STARTED_UP);
	}

	@Override
	public void interrupt() {
		Thread interruptThread = thread.get();
		if (interruptThread != null) {
			interruptThread.interrupt();
		}
	}

}
