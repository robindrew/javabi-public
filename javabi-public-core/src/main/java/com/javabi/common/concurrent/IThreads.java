package com.javabi.common.concurrent;

import java.lang.management.ThreadInfo;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import com.javabi.common.date.UnitTime;

public interface IThreads {

	int getThreadCount();

	int getPeakThreadCount();

	ThreadInfo getThreadInfo(long id);

	Set<Thread> getAllThreads();

	Map<Thread, StackTraceElement[]> getAllStackTraces();

	void sleep(long millis);

	void sleep(long millis, int nanos);

	void sleep(long amount, TimeUnit unit);

	void sleep(UnitTime time);

	void sleepUntil(long timeInMillis);

	void sleepForever();

	void sleepUntil(AtomicBoolean wakeUp, long interval);

	void sleepUntil(AtomicBoolean wakeUp, long interval, TimeUnit unit);

	void sleepUntil(AtomicBoolean wakeUp, UnitTime interval);

	void await(Object object);

	void terminate(ExecutorService service);

	ThreadFactory newThreadFactory(String nameFormat);

	ThreadFactory newThreadFactory(String nameFormat, boolean daemon);

	ExecutorService newFixedThreadPoolt(String nameFormat, int threads);

	ExecutorService newFixedThreadPool(ThreadFactory factory, int threads);

	ExecutorService newCachedThreadPool(String nameFormat);

	ExecutorService newCachedThreadPool(ThreadFactory factory);

	Callable<?> callable(Runnable runnable);

	<R> Callable<R> callable(Runnable runnable, R returnValue);

	void executeAll(String nameFormat, int threads, Collection<? extends Runnable> tasks);

	<T> List<T> executeAndReturn(String nameFormat, int threads, Collection<? extends Callable<T>> tasks);

}
