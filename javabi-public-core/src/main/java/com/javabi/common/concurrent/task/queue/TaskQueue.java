package com.javabi.common.concurrent.task.queue;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.concurrent.task.Task;

/**
 * A Task Queue.
 * @param <T> the queue element type.
 */
public abstract class TaskQueue<T extends Task<?>> implements Runnable {

	/** The logger. */
	private static final Logger log = LoggerFactory.getLogger(TaskQueue.class);

	/** Execute monitor. */
	private final Object executeMonitor = new Object();
	/** Queue monitor. */
	private final Object queueMonitor = new Object();

	/** The currently executing task. */
	private final AtomicReference<T> executingTask = new AtomicReference<T>();
	/** The number of tasks currently executing. */
	private final AtomicInteger executing = new AtomicInteger(0);
	/** The total number of tasks executing. */
	private final AtomicLong executed = new AtomicLong(0);
	/** The number of failed. */
	private final AtomicLong failed = new AtomicLong(0);
	/** The number of succeeded. */
	private final AtomicLong succeeded = new AtomicLong(0);
	/** The throttle time in milliseconds. */
	private final AtomicLong throttleMillis = new AtomicLong(0);

	/** Indicates if shutdown. */
	private final AtomicBoolean shutdown = new AtomicBoolean(false);
	/** Indicates if paused. */
	private final AtomicBoolean paused = new AtomicBoolean(false);

	/**
	 * Flush the queue.
	 */
	public void flush() {
		run();
	}

	/**
	 * Shutdown this queue.
	 */
	public void shutdown() {
		shutdown.set(true);
	}

	/**
	 * Returns true if this is shutting down.
	 * @return true if this is shutting down.
	 */
	public boolean isShutdown() {
		return shutdown.get();
	}

	/**
	 * Returns true if paused.
	 * @return true if paused.
	 */
	public boolean isPaused() {
		return paused.get();
	}

	/**
	 * Pause this queue.
	 */
	public void pause() {
		paused.set(true);
	}

	/**
	 * Resume this queue.
	 */
	public void resume() {
		paused.set(false);
		synchronized (paused) {
			paused.notify();
		}
	}

	/**
	 * Returns the total number of tasks failed.
	 * @return the total number of tasks failed.
	 */
	public long failed() {
		return failed.get();
	}

	/**
	 * Returns the total number of tasks succeeded.
	 * @return the total number of tasks succeeded.
	 */
	public long succeeded() {
		return succeeded.get();
	}

	/**
	 * Returns the currently executing task.
	 * @return the currently executing task.
	 */
	public T getExecutingTask() {
		return executingTask.get();
	}

	/**
	 * Returns the execute monitor.
	 * @return the execute monitor.
	 */
	protected final Object getExecuteMonitor() {
		return executeMonitor;
	}

	/**
	 * Returns the queue monitor.
	 * @return the queue monitor.
	 */
	protected final Object getQueueMonitor() {
		return queueMonitor;
	}

	/**
	 * Run this queue.
	 */
	public void run() {
		try {
			while (!shutdown.get()) {
				runTasks();
			}

		} catch (InterruptedException ie) {
			log.warn("Queue Interrupted: " + this, ie);
		}
	}

	private void runTasks() throws InterruptedException {
		if (waiting() == 0) {
			return;
		}

		// Drain Queue
		List<T> taskList = drain();

		// Run Elements
		runTasks(taskList);

	}

	private List<T> drain() throws InterruptedException {
		synchronized (getQueueMonitor()) {
			List<T> executeList = drainQueue();
			while (executeList.isEmpty()) {
				getQueueMonitor().wait();
				executeList = drainQueue();
			}
			return executeList;
		}
	}

	private void runTasks(List<T> taskList) throws InterruptedException {
		synchronized (getExecuteMonitor()) {
			executing.set(taskList.size());
			try {
				for (T task : taskList) {

					// Paused
					if (paused.get()) {
						waitForResume();
					}

					// Execute
					executingTask.set(task);
					task.runTask();
					if (task.hasSucceeded()) {
						succeeded.addAndGet(1);
					} else {
						failed.addAndGet(1);
					}

					// Cleanup
					executingTask.set(null);
					executed.addAndGet(1);
					executing.addAndGet(-1);

					// Throttle
					throttle();
				}
			} finally {
				executing.set(0);
				getExecuteMonitor().notify();
			}
		}
	}

	private void throttle() throws InterruptedException {
		long sleep = throttleMillis.get();
		if (sleep > 0) {
			Thread.sleep(sleep);
		} else {
			if (Thread.interrupted()) {
				throw new InterruptedException();
			}
		}
	}

	private void waitForResume() throws InterruptedException {
		synchronized (paused) {
			while (paused.get()) {
				paused.wait();
			}
		}
	}

	/**
	 * Clear this queue.
	 */
	public abstract void clear();

	/**
	 * Returns the number of tasks waiting.
	 * @return the number of tasks waiting.
	 */
	public abstract int waiting();

	/**
	 * Enqueue the given element.
	 * @param element the element.
	 * @return false if the queue is full.
	 */
	public abstract boolean enqueue(T element);

	/**
	 * Handle a failed element execution.
	 * @param e the exception.
	 */
	protected abstract void handleFailure(Exception e);

	/**
	 * Drain the queue.
	 * @return the contents of the queue.
	 */
	protected abstract List<T> drainQueue();

}
