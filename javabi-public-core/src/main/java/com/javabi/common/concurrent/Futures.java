package com.javabi.common.concurrent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Future;

import com.google.common.base.Throwables;

public class Futures {

	public static <T> List<T> await(Collection<Future<T>> futures) {
		try {
			List<T> list = new ArrayList<T>();
			for (Future<T> future : futures) {
				list.add(future.get());
			}
			return list;
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	private Futures() {
	}
}
