package com.javabi.common.concurrent.lock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public interface ILockSet {

	Lock getLock(Object object);

	Condition getCondition(Object object);

}
