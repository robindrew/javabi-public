package com.javabi.common.concurrent;

public interface IRunStatus {

	boolean isStarted();

	boolean isRunning();

	boolean isShutdown();

	boolean shutdown();

}
