package com.javabi.common.concurrent.task;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * An Abstract Task.
 * @param <R> the return value.
 */
public abstract class Task<R> implements Callable<R> {

	/** The start time (0 if not started). */
	private final AtomicLong startTime = new AtomicLong(0);
	/** The finish time (0 if not finished). */
	private final AtomicLong finishTime = new AtomicLong(0);

	/** The return value. */
	private final AtomicReference<R> returnValue = new AtomicReference<R>();
	/** The throwable. */
	private final AtomicReference<Throwable> throwable = new AtomicReference<Throwable>();

	/**
	 * Returns true if this task has started.
	 * @return true if this task has started.
	 */
	public boolean isStarted() {
		return startTime.get() > 0;
	}

	/**
	 * Returns true if this task has finished.
	 * @return true if this task has finished.
	 */
	public boolean isFinished() {
		return finishTime.get() > 0;
	}

	/**
	 * Returns the return value.
	 * @return the return value.
	 */
	public R getReturnValue() {
		return returnValue.get();
	}

	/**
	 * Returns the throwable thrown.
	 * @return the throwable thrown.
	 */
	public Throwable getThrowable() {
		return throwable.get();
	}

	/**
	 * Returns true if this task has succeeded.
	 * @return true if this task has succeeded.
	 */
	public boolean hasSucceeded() {
		return !hasFailed();
	}

	/**
	 * Returns true if this task has failed.
	 * @return true if this task has failed.
	 */
	public boolean hasFailed() {
		return throwable.get() != null;
	}

	/**
	 * Returns the time the task started.
	 * @return the time the task started.
	 */
	public long getStartTime() {
		return startTime.get();
	}

	/**
	 * Returns the time the task finished.
	 * @return the time the task finished.
	 */
	public long getFinishTime() {
		return finishTime.get();
	}

	/**
	 * Returns the duration of the task.
	 * @return the duration of the task.
	 */
	public long getDuration() {
		if (!isStarted()) {
			return -1;
		}
		if (!isFinished()) {
			long currentTime = System.currentTimeMillis();
			return currentTime - startTime.get();
		}
		return finishTime.get() - startTime.get();
	}

	/**
	 * Run the task.
	 */
	public final void runTask() {
		try {
			startTime.set(System.currentTimeMillis());
			R result = call();
			if (result != null) {
				this.returnValue.set(result);
			}
		} catch (Throwable t) {
			throwable.set(t);
		} finally {
			finishTime.set(System.currentTimeMillis());
		}
	}

}
