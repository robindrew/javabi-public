package com.javabi.common.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CountDownExecutor extends ExecutorServiceDelegate {

	private final CountDown countDown = new CountDown();

	public CountDownExecutor(ExecutorService service) {
		super(service);
	}

	public CountDownExecutor(int threads) {
		super(Executors.newFixedThreadPool(threads));
	}

	public CountDown getCountDown() {
		return countDown;
	}

	public void waitFor() {
		countDown.waitFor();
	}

	@Override
	public <T> Future<T> submit(Callable<T> task) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> Future<T> submit(Runnable task, T result) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Future<?> submit(Runnable task) {
		task = wrap(task);
		return super.submit(task);
	}

	@Override
	public void execute(Runnable command) {
		command = wrap(command);
		super.execute(command);
	}

	private Runnable wrap(final Runnable runnable) {
		countDown.increment();
		return new Runnable() {

			@Override
			public void run() {
				try {
					runnable.run();
				} finally {
					countDown.decrement();
				}
			}
		};
	}

}
