package com.javabi.common.concurrent;

import static java.lang.System.currentTimeMillis;

import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;

public class Monitor implements IMonitor {

	private final Object monitor;

	public Monitor(Object monitor) {
		if (monitor == null) {
			throw new NullPointerException("monitor");
		}
		this.monitor = monitor;
	}

	public Monitor() {
		this.monitor = new Object();
	}

	@Override
	public void await() {
		try {
			synchronized (monitor) {
				monitor.wait();
			}
		} catch (InterruptedException ie) {
			throw new RuntimeInterruptedException(ie);
		}
	}

	@Override
	public void await(long time, TimeUnit unit) {
		long timeout = unit.toMillis(time);
		if (timeout <= 0) {
			throw new IllegalArgumentException("Invalid timeout: " + time + " " + unit);
		}

		final Object monitor = this.monitor;
		try {
			synchronized (monitor) {
				monitor.wait(timeout);
			}
		} catch (InterruptedException ie) {
			throw new RuntimeInterruptedException(ie);
		}
	}

	@Override
	public void await(BooleanSupplier condition) {
		final Object monitor = this.monitor;
		try {
			synchronized (monitor) {
				while (!condition.getAsBoolean()) {
					monitor.wait();
				}
			}
		} catch (InterruptedException ie) {
			throw new RuntimeInterruptedException(ie);
		}
	}

	@Override
	public void await(BooleanSupplier condition, long time, TimeUnit unit) {
		long now = currentTimeMillis();
		long timeout = unit.toMillis(time);
		if (timeout <= 0) {
			throw new IllegalArgumentException("Invalid timeout: " + time + " " + unit);
		}

		final Object monitor = this.monitor;
		final long futureTime = now + timeout;
		try {
			synchronized (monitor) {
				while (!condition.getAsBoolean()) {
					long millis = futureTime - currentTimeMillis();
					if (millis <= 0) {
						break;
					}
					monitor.wait(millis);
				}
			}
		} catch (InterruptedException ie) {
			throw new RuntimeInterruptedException(ie);
		}
	}

	@Override
	public void signal() {
		synchronized (monitor) {
			monitor.notify();
		}
	}

	@Override
	public void signalAll() {
		synchronized (monitor) {
			monitor.notifyAll();
		}
	}

}
