package com.javabi.common.concurrent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A map that records updated by key.
 */
public class UpdateMap<K, V> extends SynchronizedMap<K, V> {

	/** The set of keys that have changed. */
	private final Set<K> updatedKeys;

	/**
	 * Creates a new update map.
	 * @param map the map.
	 * @param set the set.
	 */
	protected UpdateMap(Map<K, V> map, Set<K> set) {
		super(map);
		if (set == null) {
			throw new NullPointerException();
		}
		this.updatedKeys = set;
	}

	/**
	 * Creates a new update map.
	 */
	public UpdateMap() {
		this(new HashMap<K, V>(), new HashSet<K>());
	}

	/**
	 * Returns the list of updated keys.
	 * @param clear true to clear the updates.
	 * @param changes the changes.
	 * @return the list of updated keys.
	 */
	public <C extends Collection<K>> C getUpdatedKeys(boolean clear, C changes) {
		synchronized (this) {
			if (!updatedKeys.isEmpty()) {
				changes.addAll(updatedKeys);
				if (clear) {
					updatedKeys.clear();
				}
			}
		}
		return changes;
	}

	/**
	 * Returns the number of keys updated.
	 * @return the number of keys updated.
	 */
	public int updated() {
		synchronized (this) {
			return updatedKeys.size();
		}
	}

	/**
	 * Returns the list of updated keys.
	 * @param clear true to clear the updates.
	 * @return the list of updated keys.
	 */
	public List<K> getUpdatedKeys(boolean clear) {
		synchronized (this) {
			if (updatedKeys.isEmpty()) {
				return Collections.emptyList();
			}
			return getUpdatedKeys(clear, new ArrayList<K>(updatedKeys.size()));
		}
	}

	/**
	 * Returns a map of updated keys and their values.
	 * @param clear true to clear the updates.
	 * @param updatedMap the map of updates.
	 * @return the map of updates.
	 */
	public <M extends Map<K, V>> M getUpdatedMap(boolean clear, M updatedMap) {
		synchronized (this) {
			for (K key : updatedKeys) {
				V value = get(key);
				updatedMap.put(key, value);
			}
			if (clear) {
				updatedKeys.clear();
			}
		}
		return updatedMap;
	}

	/**
	 * Returns a map of updated keys and their values.
	 * @param clear true to clear the updates.
	 * @return the map of updates.
	 */
	public Map<K, V> getUpdatedMap(boolean clear) {
		return getUpdatedMap(clear, new HashMap<K, V>());
	}

	@Override
	public V put(K key, V value) {
		if (key == null || value == null) {
			throw new NullPointerException();
		}
		synchronized (this) {
			V oldValue = getUnderlyingMap().put(key, value);
			if (oldValue == null || !value.equals(oldValue)) {
				updatedKeys.add(key);
			}
			return oldValue;
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public V remove(Object key) {
		synchronized (this) {
			V value = getUnderlyingMap().remove(key);
			if (value != null) {
				updatedKeys.add((K) key);
			}
			return value;
		}
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> map) {
		synchronized (this) {
			for (Entry<? extends K, ? extends V> entry : map.entrySet()) {
				put(entry.getKey(), entry.getValue());
			}
		}
	}

}
