package com.javabi.common.concurrent;

import java.util.concurrent.atomic.AtomicBoolean;

public class Flag implements IFlag {

	private final AtomicBoolean flag = new AtomicBoolean(false);
	private final IMonitor monitor = new Monitor(this);

	@Override
	public void waitUntilValue(boolean value) {
		monitor.await(() -> flag.get() == value);
	}

	@Override
	public boolean hasValue(boolean value) {
		return flag.get() == value;
	}

	@Override
	public boolean setValue(boolean value) {
		if (flag.compareAndSet(!value, value)) {
			monitor.notifyAll();
			return true;
		}
		return false;
	}

}
