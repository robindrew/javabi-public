package com.javabi.common.concurrent.task.queue;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.concurrent.task.Task;

/**
 * A Map Task Queue.
 * @param <T> the task type.
 */
public class MapTaskQueue<T extends Task<?>> extends TaskQueue<T> {

	/** The logger. */
	private static final Logger log = LoggerFactory.getLogger(MapTaskQueue.class);

	/** The queue map. */
	private final Map<T, T> queueMap;

	/**
	 * Creates a new map task queue.
	 * @param queueMap the queue map.
	 */
	public MapTaskQueue(Map<T, T> queueMap) {
		if (queueMap == null) {
			throw new NullPointerException();
		}
		this.queueMap = queueMap;
	}

	/**
	 * Creates a new map task queue.
	 */
	public MapTaskQueue() {
		this(new LinkedHashMap<T, T>());
	}

	@Override
	public void clear() {
		synchronized (getQueueMonitor()) {
			queueMap.clear();
		}
	}

	@Override
	protected List<T> drainQueue() {
		List<T> list = new ArrayList<T>(queueMap.size());
		list.addAll(queueMap.keySet());
		queueMap.clear();
		return list;
	}

	@Override
	public boolean enqueue(T element) {
		synchronized (getQueueMonitor()) {

			// Replace previous element if present.
			queueMap.put(element, element);
		}
		return true;
	}

	@Override
	protected void handleFailure(Exception e) {
		log.warn("Unexpected exception caught", e);
	}

	@Override
	public int waiting() {
		synchronized (getQueueMonitor()) {
			return queueMap.size();
		}
	}

}
