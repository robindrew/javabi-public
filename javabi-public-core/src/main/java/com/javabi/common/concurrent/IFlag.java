package com.javabi.common.concurrent;

public interface IFlag {

	/**
	 * Block on this method until the flag is set to the given value.
	 */
	void waitUntilValue(boolean value);

	/**
	 * Returns true if the flag is set to the given value.
	 */
	boolean hasValue(boolean value);

	/**
	 * Sets this flag to the given value.
	 * @return true if the flag was changed by this call.
	 */
	boolean setValue(boolean value);

}
