package com.javabi.common.concurrent;

import static java.util.concurrent.TimeUnit.MICROSECONDS;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import com.google.common.base.Supplier;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.javabi.common.date.UnitTime;

public class ThreadsImpl implements IThreads {

	@Override
	public int getThreadCount() {
		return getAllStackTraces().size();
	}

	@Override
	public int getPeakThreadCount() {
		return ManagementFactory.getThreadMXBean().getPeakThreadCount();
	}

	@Override
	public ThreadInfo getThreadInfo(long id) {
		return ManagementFactory.getThreadMXBean().getThreadInfo(id);
	}

	@Override
	public Set<Thread> getAllThreads() {
		return getAllStackTraces().keySet();
	}

	@Override
	public Map<Thread, StackTraceElement[]> getAllStackTraces() {
		return Thread.getAllStackTraces();
	}

	@Override
	public void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			throw new RuntimeInterruptedException(e);
		}
	}

	@Override
	public void sleep(long millis, int nanos) {
		try {
			Thread.sleep(millis, nanos);
		} catch (InterruptedException e) {
			throw new RuntimeInterruptedException(e);
		}
	}

	@Override
	public void sleep(UnitTime time) {
		TimeUnit unit = time.getUnit();

		// Nanosecond precision?
		if (unit.equals(NANOSECONDS) || unit.equals(MICROSECONDS)) {
			int nanos = (int) (time.getTime(NANOSECONDS) % 1000000);
			long millis = time.getTime(MILLISECONDS);
			sleep(millis, nanos);
		}

		// Millisecond precision
		else {
			long millis = time.getTime(MILLISECONDS);
			sleep(millis);
		}
	}

	@Override
	public void sleep(long duration, TimeUnit unit) {
		sleep(new UnitTime(duration, unit));
	}

	@Override
	public void sleepUntil(long timeInMillis) {
		// N.B. timeInMillis should be in the future :)
		while (true) {
			long millis = timeInMillis - System.currentTimeMillis();
			if (millis <= 0) {
				break;
			}

			// To avoid clock slippage, we only sleep for 3/4 of the duration
			// until we reach 1 second ...
			if (millis <= 1000) {
				sleep(millis);
			} else {
				sleep((millis * 3) / 4);
			}
		}
	}

	/**
	 * Sleep forever (why would you ever use this?!)
	 */
	@Override
	public void sleepForever() {
		sleep(Long.MAX_VALUE);
	}

	/**
	 * Looping sleep that wakes up at a given interval to check whether to wake up.
	 * @param wakeUp the wake up indicator.
	 * @param interval the interval in milliseconds.
	 */
	@Override
	public void sleepUntil(AtomicBoolean wakeUp, long interval) {
		while (!wakeUp.get()) {
			sleep(interval);
		}
	}

	/**
	 * Looping sleep that wakes up at a given interval to check whether to wake up.
	 * @param wakeUp the wake up indicator.
	 * @param interval the interval in the given time unit.
	 * @param unit the time unit.
	 */
	@Override
	public void sleepUntil(AtomicBoolean wakeUp, long interval, TimeUnit unit) {
		sleepUntil(wakeUp, new UnitTime(interval, unit));
	}

	/**
	 * Looping sleep that wakes up at a given interval to check whether to wake up.
	 * @param wakeUp the wake up indicator.
	 * @param interval the interval.
	 */
	@Override
	public void sleepUntil(AtomicBoolean wakeUp, UnitTime interval) {
		sleepUntil(wakeUp, interval.getTime(MILLISECONDS));
	}

	@Override
	public void await(Object object) {
		try {
			object.wait();
		} catch (InterruptedException e) {
			throw new RuntimeInterruptedException(e);
		}
	}

	@Override
	public void terminate(ExecutorService service) {
		try {
			service.shutdown();
			service.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			throw new RuntimeInterruptedException(e);
		}
	}

	private String checkNameFormat(String nameFormat) {
		if (!nameFormat.contains("%d")) {
			nameFormat = nameFormat + "-%d";
		}
		return nameFormat;
	}

	@Override
	public ThreadFactory newThreadFactory(String nameFormat) {
		ThreadFactoryBuilder builder = new ThreadFactoryBuilder();
		builder.setNameFormat(checkNameFormat(nameFormat));
		return builder.build();
	}

	@Override
	public ThreadFactory newThreadFactory(String nameFormat, boolean daemon) {
		ThreadFactoryBuilder builder = new ThreadFactoryBuilder();
		builder.setNameFormat(checkNameFormat(nameFormat));
		builder.setDaemon(daemon);
		return builder.build();
	}

	@Override
	public ExecutorService newFixedThreadPoolt(String nameFormat, int threads) {
		ThreadFactory factory = newThreadFactory(nameFormat);
		return newFixedThreadPool(factory, threads);
	}

	@Override
	public ExecutorService newFixedThreadPool(ThreadFactory factory, int threads) {
		return Executors.newFixedThreadPool(threads, factory);
	}

	@Override
	public ExecutorService newCachedThreadPool(String nameFormat) {
		ThreadFactory factory = newThreadFactory(nameFormat);
		return newCachedThreadPool(factory);
	}

	@Override
	public ExecutorService newCachedThreadPool(ThreadFactory factory) {
		return Executors.newCachedThreadPool(factory);
	}

	@Override
	public Callable<?> callable(Runnable runnable) {
		return Executors.callable(runnable);
	}

	@Override
	public <R> Callable<R> callable(Runnable runnable, R returnValue) {
		return Executors.callable(runnable, returnValue);
	}

	@Override
	public void executeAll(String nameFormat, int threads, Collection<? extends Runnable> tasks) {
		Supplier<ExecutorService> supplier = new ExecutorServiceSupplier(nameFormat, threads);
		new FailFastExecutor().executeAll(supplier, tasks);
	}

	@Override
	public <T> List<T> executeAndReturn(String nameFormat, int threads, Collection<? extends Callable<T>> tasks) {
		Supplier<ExecutorService> supplier = new ExecutorServiceSupplier(nameFormat, threads);
		return new FailFastExecutor().executeAndReturn(supplier, tasks);
	}

}
