package com.javabi.common.concurrent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

/**
 * A Synchronized {@link ConcurrentMap} wrapper for existing maps.
 * @param <K> the key.
 * @param <V> the value.
 */
public class SynchronizedMap<K, V> implements ConcurrentMap<K, V> {

	/** The underlying map. */
	private final Map<K, V> underlyingMap;

	/**
	 * Creates a new synchronized map.
	 * @param map the map.
	 */
	public SynchronizedMap(Map<K, V> map) {
		if (map == null) {
			throw new NullPointerException();
		}
		this.underlyingMap = map;
	}

	/**
	 * Returns the underlying map.
	 * @return the underlying map.
	 */
	protected final Map<K, V> getUnderlyingMap() {
		return underlyingMap;
	}

	public Set<K> keySet() {
		synchronized (this) {
			return new HashSet<K>(getUnderlyingMap().keySet());
		}
	}

	public Collection<V> values() {
		synchronized (this) {
			return new ArrayList<V>(getUnderlyingMap().values());
		}
	}

	public void clear() {
		synchronized (this) {
			underlyingMap.clear();
		}
	}

	public boolean containsKey(Object key) {
		synchronized (this) {
			return underlyingMap.containsKey(key);
		}
	}

	public boolean containsValue(Object value) {
		synchronized (this) {
			return underlyingMap.containsValue(value);
		}
	}

	public Set<java.util.Map.Entry<K, V>> entrySet() {
		synchronized (this) {
			return underlyingMap.entrySet();
		}
	}

	public V get(Object key) {
		synchronized (this) {
			return underlyingMap.get(key);
		}
	}

	public boolean isEmpty() {
		synchronized (this) {
			return underlyingMap.isEmpty();
		}
	}

	public void putAll(Map<? extends K, ? extends V> map) {
		synchronized (this) {
			underlyingMap.putAll(map);
		}
	}

	public int size() {
		synchronized (this) {
			return underlyingMap.size();
		}
	}

	public V put(K key, V value) {
		if (key == null || value == null) {
			throw new NullPointerException();
		}
		synchronized (this) {
			return underlyingMap.put(key, value);
		}
	}

	public V remove(Object key) {
		synchronized (this) {
			return underlyingMap.remove(key);
		}
	}

	public V putIfAbsent(K key, V value) {
		if (key == null || value == null) {
			throw new NullPointerException();
		}
		synchronized (this) {
			if (!underlyingMap.containsKey(key)) {
				return underlyingMap.put(key, value);
			}
			return underlyingMap.get(key);
		}
	}

	public boolean remove(Object key, Object value) {
		if (key == null || value == null) {
			throw new NullPointerException();
		}
		synchronized (this) {
			if (underlyingMap.containsKey(key) && underlyingMap.get(key).equals(value)) {
				underlyingMap.remove(key);
				return true;
			}
			return false;
		}
	}

	public V replace(K key, V value) {
		if (key == null || value == null) {
			throw new NullPointerException();
		}
		synchronized (this) {
			if (underlyingMap.containsKey(key)) {
				return underlyingMap.put(key, value);
			}
			return null;
		}
	}

	public boolean replace(K key, V oldValue, V newValue) {
		if (key == null || oldValue == null || newValue == null) {
			throw new NullPointerException();
		}
		synchronized (this) {
			if (underlyingMap.containsKey(key) && underlyingMap.get(key).equals(oldValue)) {
				underlyingMap.put(key, newValue);
				return true;
			}
			return false;
		}
	}
}
