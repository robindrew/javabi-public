package com.javabi.common.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import com.google.common.base.Supplier;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

public class ExecutorServiceSupplier implements Supplier<ExecutorService> {

	private int threads = 0;
	private String nameFormat = null;
	private boolean daemon = false;

	public ExecutorServiceSupplier(String nameFormat) {
		setNameFormat(nameFormat);
	}

	public ExecutorServiceSupplier(String nameFormat, int threads) {
		setNameFormat(nameFormat);
		setThreadCount(threads);
	}

	public ExecutorServiceSupplier setNameFormat(String nameFormat) {
		if (nameFormat.isEmpty()) {
			throw new IllegalArgumentException("nameFormat is empty");
		}
		this.nameFormat = nameFormat;
		return this;
	}

	public ExecutorServiceSupplier setThreadCount(int threads) {
		if (threads < 0) {
			throw new IllegalArgumentException("threads=" + threads);
		}
		this.threads = threads;
		return this;
	}

	public ExecutorServiceSupplier setDaemon(boolean daemon) {
		this.daemon = daemon;
		return this;
	}

	private ThreadFactory getFactory() {
		ThreadFactoryBuilder builder = new ThreadFactoryBuilder();
		if (nameFormat != null) {
			builder.setNameFormat(nameFormat);
		}
		builder.setDaemon(daemon);
		return builder.build();
	}

	@Override
	public ExecutorService get() {
		ThreadFactory factory = getFactory();
		if (threads > 0) {
			return Executors.newFixedThreadPool(threads, factory);
		} else {
			return Executors.newCachedThreadPool(factory);
		}
	}

}
