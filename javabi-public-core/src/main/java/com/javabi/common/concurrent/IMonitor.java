package com.javabi.common.concurrent;

import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;

public interface IMonitor {

	void await();

	void await(long time, TimeUnit unit);

	void await(BooleanSupplier condition);

	void await(BooleanSupplier condition, long time, TimeUnit unit);

	void signal();

	void signalAll();

}
