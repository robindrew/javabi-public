package com.javabi.common.concurrent.task.queue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.concurrent.task.Task;

/**
 * A Blocking Task Queue.
 * @param <T> the task type.
 */
public class BlockingTaskQueue<T extends Task<?>> extends TaskQueue<T> {

	/** The logger. */
	private static final Logger log = LoggerFactory.getLogger(BlockingTaskQueue.class);

	/** The underlying queue. */
	private final BlockingQueue<T> queue;

	/**
	 * Creates a new blocking task queue.
	 * @param queue the underlying queue.
	 */
	public BlockingTaskQueue(BlockingQueue<T> queue) {
		if (queue == null) {
			throw new NullPointerException();
		}
		this.queue = queue;
	}

	/**
	 * Creates a new blocking task queue.
	 */
	public BlockingTaskQueue() {
		this(new LinkedBlockingQueue<T>());
	}

	@Override
	public void clear() {
		queue.clear();
	}

	@Override
	protected List<T> drainQueue() {
		List<T> list = new ArrayList<T>(queue.size());
		queue.drainTo(list);
		return list;
	}

	@Override
	public boolean enqueue(T element) {
		return queue.add(element);
	}

	@Override
	protected void handleFailure(Exception e) {
		log.warn("Unexpected exception caught", e);
	}

	@Override
	public int waiting() {
		return queue.size();
	}

}
