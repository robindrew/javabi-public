package com.javabi.common.concurrent.thread;

import static com.javabi.common.service.Status.STARTED_UP;
import static com.javabi.common.service.Status.STARTING_UP;

import java.util.concurrent.ThreadFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.concurrent.Threads;
import com.javabi.common.service.ShutdownException;
import com.javabi.common.service.Status;

public class LoopingThreadControl extends ThreadControl implements ILoopingThreadControl {

	private static final Logger log = LoggerFactory.getLogger(LoopingThreadControl.class);

	private final Runnable runnable;
	private final LoopingRunnable looping;

	public LoopingThreadControl(String name, Runnable runnable) {
		super(name);
		if (runnable == null) {
			throw new NullPointerException("runnable");
		}
		this.runnable = runnable;
		this.looping = new LoopingRunnable();
	}

	public LoopingThreadControl(String name, Runnable runnable, ThreadFactory factory) {
		super(name, factory);
		if (runnable == null) {
			throw new NullPointerException("runnable");
		}
		this.runnable = runnable;
		this.looping = new LoopingRunnable();
	}

	@Override
	public Runnable getRunnable() {
		return looping;
	}

	public void loop() {
		Status status = getStatus();

		// Run can only be called if starting / started up ...
		if (!status.equals(STARTING_UP) && !status.equals(STARTED_UP)) {
			throw new IllegalStateException("status: " + status);
		}

		// Wait for control to start up
		while (hasStatus(STARTING_UP)) {
			Threads.sleep(10);
		}

		// The status STARTED_UP is equivalent of RUNNING ...
		try {
			while (hasStatus(STARTED_UP)) {
				runnable.run();
			}
		} catch (Throwable t) {
			t = Throwables.getRootCause(t);

			if (!(t instanceof ShutdownException)) {
				log.error("Uncaught Throwable", t);
			}
			shutdown();
		}
	}

	private class LoopingRunnable implements Runnable {

		@Override
		public void run() {
			loop();
		}

	}

}
