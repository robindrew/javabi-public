package com.javabi.common.concurrent.lock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockHashSet implements ILockSet {

	private final Lock[] locks;
	private final Condition[] conditions;

	public LockHashSet(int length, boolean fair) {
		if (length < 1) {
			throw new IllegalArgumentException("length=" + length);
		}
		this.locks = new Lock[length];
		this.conditions = new Condition[length];
		for (int i = 0; i < length; i++) {
			ReentrantLock lock = new ReentrantLock(fair);
			locks[i] = lock;
			conditions[i] = lock.newCondition();
		}
	}

	public int size() {
		return locks.length;
	}

	@Override
	public Lock getLock(Object object) {
		int index = getIndex(object, locks.length);
		return locks[index];
	}

	@Override
	public Condition getCondition(Object object) {
		int index = getIndex(object, conditions.length);
		return conditions[index];
	}

	protected int getIndex(Object object, int length) {
		// Simplistic mapping
		int index = System.identityHashCode(object) % length;
		if (index < 0) {
			index = -index;
		}
		return index;
	}

	public static void main(String[] args) {
		LockHashSet set = new LockHashSet(10, false);
		for (int i = 0; i < Integer.MAX_VALUE; i++) {
			Object object = new Object();
			Lock lock = set.getLock(object);
			lock.lock();
			lock.unlock();
		}
	}

}
