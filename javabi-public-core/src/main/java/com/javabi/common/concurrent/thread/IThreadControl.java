package com.javabi.common.concurrent.thread;

import com.javabi.common.service.control.IControl;

public interface IThreadControl extends IControl {

	void interrupt();

	Runnable getRunnable();

	boolean isRunning();

}
