package com.javabi.common.concurrent;

public class RuntimeInterruptedException extends RuntimeException {

	private static final long serialVersionUID = -1995251026547449452L;

	public RuntimeInterruptedException(InterruptedException cause) {
		super(cause);
	}
}
