package com.javabi.common.dependency.instance;

public interface IInstanceFactory<I> {

	I newInstance(Object... args);

}
