package com.javabi.common.dependency.lazy;

public interface ILazyInstance<I> {

	I initialize();

}
