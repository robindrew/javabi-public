package com.javabi.common.dependency.locator;

import java.util.List;

import com.javabi.common.dependency.instance.IInstanceFactory;
import com.javabi.common.dependency.lazy.ILazyInstance;

public interface IDependencyLocator {

	String getName();

	<C, I extends C> I set(Class<C> key, I instance);

	<C, I extends C> void setLazyInstance(Class<C> key, ILazyInstance<I> instance);

	<C, I extends C> void setInstanceFactory(Class<C> key, IInstanceFactory<I> instance);

	<C, I extends C> I get(Class<C> key);

	<C, I extends C> I get(Class<C> key, I defaultInstance);

	<C, I extends C> I getInstance(Class<C> key, Object... args);

	List<Object> getAll();

	boolean remove(Object key);

	int size();

	boolean isEmpty();

	void clear();

}
