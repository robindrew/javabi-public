package com.javabi.common.io.connection;

public interface INetworkUser {

	INetworkAddress getAddress();

	String getUsername();

	String getPassword();

}
