package com.javabi.common.io.pipe;

public interface IBufferPipeObserver {

	void piped(long bytes, long nanos);

}
