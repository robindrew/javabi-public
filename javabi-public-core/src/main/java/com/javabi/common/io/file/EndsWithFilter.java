package com.javabi.common.io.file;

import java.io.File;
import java.io.FileFilter;

public class EndsWithFilter implements FileFilter {

	private final String suffix;
	private final boolean ignoreCase;

	public EndsWithFilter(String suffix, boolean ignoreCase) {
		if (suffix.isEmpty()) {
			throw new IllegalArgumentException("suffix is empty");
		}
		this.suffix = (ignoreCase ? suffix.toLowerCase() : suffix);
		this.ignoreCase = ignoreCase;
	}

	@Override
	public boolean accept(File file) {
		String filename = file.getName();
		if (ignoreCase) {
			filename = filename.toLowerCase();
		}
		return filename.endsWith(suffix);
	}

}
