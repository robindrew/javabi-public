package com.javabi.common.io.connection;

import java.io.Closeable;
import java.io.InputStream;
import java.io.OutputStream;

public interface IConnection extends Closeable {

	INetworkAddress getLocalAddress();

	INetworkAddress getRemoteAddress();

	InputStream getInput();

	OutputStream getOutput();

	boolean isConnected();

	void disconnect();

}
