package com.javabi.common.io.file.filters;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Useful helper class for common file filtering cases.
 */
public class FileFilters implements FileFilter {

	private final List<FileFilter> filterList = new ArrayList<>();
	private final boolean accept;

	public FileFilters(boolean accept) {
		this.accept = accept;
	}

	public FileFilters hasName(String filename) {
		filterList.add(new FileFilter() {

			@Override
			public boolean accept(File file) {
				String name = file.getName();
				return name.equals(filename);
			}
		});
		return this;
	}

	public FileFilters notHasName(String filename) {
		filterList.add(new FileFilter() {

			@Override
			public boolean accept(File file) {
				String name = file.getName();
				return !name.equals(filename);
			}
		});
		return this;
	}

	public FileFilters matches(String regex) {
		Pattern pattern = Pattern.compile(regex);
		filterList.add(new FileFilter() {

			@Override
			public boolean accept(File file) {
				String path = file.getAbsolutePath();
				return pattern.matcher(path).matches();
			}
		});
		return this;
	}

	public FileFilters notMatches(String regex) {
		Pattern pattern = Pattern.compile(regex);
		filterList.add(new FileFilter() {

			@Override
			public boolean accept(File file) {
				String path = file.getAbsolutePath();
				return !pattern.matcher(path).matches();
			}
		});
		return this;
	}

	public FileFilters contains(String text) {
		filterList.add(new FileFilter() {

			@Override
			public boolean accept(File file) {
				String path = file.getAbsolutePath();
				return path.contains(text);
			}
		});
		return this;
	}

	public FileFilters notContains(String text) {
		filterList.add(new FileFilter() {

			@Override
			public boolean accept(File file) {
				String path = file.getAbsolutePath();
				return !path.contains(text);
			}
		});
		return this;
	}

	public FileFilters endsWith(String suffix) {
		filterList.add(new FileFilter() {

			@Override
			public boolean accept(File file) {
				String path = file.getAbsolutePath();
				return path.endsWith(suffix);
			}
		});
		return this;
	}

	public FileFilters notEndsWith(String suffix) {
		filterList.add(new FileFilter() {

			@Override
			public boolean accept(File file) {
				String path = file.getAbsolutePath();
				return !path.endsWith(suffix);
			}
		});
		return this;
	}

	public FileFilters startsWith(String prefix) {
		filterList.add(new FileFilter() {

			@Override
			public boolean accept(File file) {
				String path = file.getAbsolutePath();
				return path.startsWith(prefix);
			}
		});
		return this;
	}

	public FileFilters notStartsWith(String prefix) {
		filterList.add(new FileFilter() {

			@Override
			public boolean accept(File file) {
				String path = file.getAbsolutePath();
				return !path.startsWith(prefix);
			}
		});
		return this;
	}

	@Override
	public boolean accept(File file) {
		if (accept) {
			for (FileFilter filter : filterList) {
				if (!filter.accept(file)) {
					return false;
				}
			}
		} else {
			for (FileFilter filter : filterList) {
				if (filter.accept(file)) {
					return true;
				}
			}
		}
		return accept;
	}

}
