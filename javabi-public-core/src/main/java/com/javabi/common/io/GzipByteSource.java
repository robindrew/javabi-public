package com.javabi.common.io;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import com.google.common.io.ByteSource;

public class GzipByteSource extends ByteSource {

	private final ByteSource source;

	public GzipByteSource(ByteSource source) {
		if (source == null) {
			throw new NullPointerException("source");
		}
		this.source = source;
	}

	@Override
	public InputStream openStream() throws IOException {
		return new GZIPInputStream(source.openStream());
	}

}
