package com.javabi.common.io.serial;

import java.io.IOException;

/**
 * A Serial Output Stream.
 */
public interface SerialOutputStream {

	/**
	 * Write a portion of the given byte array.
	 * @param value the byte array.
	 * @param offset the offset.
	 * @param length the length.
	 */
	void write(byte[] value, int offset, int length) throws IOException;

	/**
	 * Write the given byte array.
	 * @param value the byte array.
	 */
	void write(byte[] value) throws IOException;

	/**
	 * Write the given string.
	 * @param value the string.
	 */
	void writeString(String value) throws IOException;

	/**
	 * Write the given object.
	 * @param value the object.
	 */
	void writeObject(Object value) throws IOException;

	/**
	 * Write the given double.
	 * @param value the double.
	 */
	void writeDouble(double value) throws IOException;

	/**
	 * Write the given float.
	 * @param value the float.
	 */
	void writeFloat(float value) throws IOException;

	/**
	 * Write the given long.
	 * @param value the long.
	 */
	void writeLong(long value) throws IOException;

	/**
	 * Write the given integer.
	 * @param value the integer.
	 */
	void writeInt(int value) throws IOException;

	/**
	 * Write the given character.
	 * @param value the character.
	 */
	void writeChar(char value) throws IOException;

	/**
	 * Write the given short.
	 * @param value the short.
	 */
	void writeShort(short value) throws IOException;

	/**
	 * Write the given byte.
	 * @param value the byte.
	 */
	void writeByte(byte value) throws IOException;

	/**
	 * Write the given boolean.
	 * @param value the boolean.
	 */
	void writeBoolean(boolean value) throws IOException;

}
