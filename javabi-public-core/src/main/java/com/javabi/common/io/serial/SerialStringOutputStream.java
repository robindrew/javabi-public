package com.javabi.common.io.serial;

import java.nio.charset.Charset;

/**
 * A Serial String Output Stream.
 */
public final class SerialStringOutputStream implements SerialOutputStream {

	/** The underlying string builder. */
	private final StringBuilder builder;
	/** The character set. */
	private final Charset charset;

	/**
	 * Creates a new output stream.
	 */
	public SerialStringOutputStream() {
		builder = new StringBuilder();
		charset = Charset.forName("UTF-8");
	}

	@Override
	public final String toString() {
		return builder.toString();
	}

	@Override
	public final void write(byte[] value, int offset, int length) {
		String text = new String(value, offset, length, charset);
		writeString(text);
	}

	@Override
	public final void write(byte[] value) {
		write(value, 0, value.length);
	}

	@Override
	public final void writeBoolean(boolean value) {
		builder.append(value);
	}

	@Override
	public final void writeByte(byte value) {
		builder.append(value);
	}

	@Override
	public final void writeChar(char value) {
		builder.append(value);
	}

	@Override
	public final void writeDouble(double value) {
		builder.append(value);
	}

	@Override
	public final void writeFloat(float value) {
		builder.append(value);
	}

	@Override
	public final void writeInt(int value) {
		builder.append(value);
	}

	@Override
	public final void writeLong(long value) {
		builder.append(value);
	}

	@Override
	public final void writeShort(short value) {
		builder.append(value);
	}

	@Override
	public final void writeString(String value) {
		builder.append(value);
	}

	@Override
	public final void writeObject(Object value) {
		builder.append(value);
	}

}
