package com.javabi.common.io;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;

import com.google.common.primitives.Chars;

/**
 * A Reader Parser.
 * <p>
 * Maintains an internal buffer over the reader during parsing.
 */
public class ReaderParser {

	/** The end of file character. */
	private static final int EOF = -1;

	/** The underlying reader. */
	private final Reader reader;
	/** The character buffer. */
	private char[] buffer;
	/** The buffer index. */
	private int bufferIndex = 0;
	/** The buffer size. */
	private int bufferSize = 0;

	/**
	 * Creates a new parse reader.
	 * @param reader the reader.
	 */
	public ReaderParser(Reader reader) {
		this.reader = reader;
		this.buffer = new char[16];
	}

	/**
	 * Creates a new parse reader.
	 * @param reader the reader.
	 * @param initialCapacity the initial capacity of the buffer.
	 */
	public ReaderParser(Reader reader, int initialCapacity) {
		if (initialCapacity < 1) {
			throw new IllegalArgumentException("initialCapacity=" + initialCapacity);
		}
		this.reader = reader;
		this.buffer = new char[initialCapacity];
	}

	/**
	 * Creates a new parse reader.
	 * @param text the text.
	 */
	public ReaderParser(String text) {
		this(new StringReader(text));
	}

	/**
	 * Flush the character buffer.
	 * <p>
	 * Call flush buffer occasionally when parsing long streams to avoid the buffer growing unnecessarily large.
	 */
	public void flushBuffer() {
		if (bufferIndex > 0) {
			int length = buffer.length - bufferIndex;
			char[] newBuffer = new char[length];
			System.arraycopy(buffer, bufferIndex, newBuffer, 0, length);
			buffer = newBuffer;
			bufferSize -= bufferIndex;
			bufferIndex = 0;
		}
	}

	/**
	 * Update the buffer if necessary.
	 * @return true if one or more characters are available to read.
	 */
	private final boolean updateBuffer() {
		try {
			if (bufferIndex == bufferSize) {
				// Grow?
				if (bufferSize == buffer.length) {
					char[] newBuffer = new char[buffer.length * 2];
					System.arraycopy(buffer, 0, newBuffer, 0, buffer.length);
					buffer = newBuffer;
				}
				// Fill!
				int read = reader.read(buffer, bufferIndex, buffer.length - bufferIndex);
				if (read == 0) {
					throw new ParserException("stream blocked, unable to read");
				}
				if (read != EOF) {
					bufferSize += read;
				}
				return bufferIndex < bufferSize;
			}
		} catch (IOException ioe) {
			throw new ParserException(ioe);
		}
		return true;
	}

	/**
	 * Read an individual character.
	 * @return the character or -1 if end of stream reached.
	 */
	public int read() {
		if (!updateBuffer()) {
			return EOF;
		}
		return buffer[bufferIndex++];
	}

	/**
	 * Read in to an array of characters.
	 * @param array the array to read in to.
	 * @param offset the offset to read in from.
	 * @param length the maximum number of characters to read.
	 * @return the number of characters read or -1 if end of stream reached.
	 */
	public int read(char[] array, int offset, int length) {
		if (!updateBuffer()) {
			return EOF;
		}
		if (length > bufferSize - bufferIndex) {
			length = bufferSize - bufferIndex;
		}
		System.arraycopy(buffer, bufferIndex, array, offset, length);
		return length;
	}

	/**
	 * Read in to an array of characters.
	 * @param array the array to read in to.
	 * @return the number of characters read or -1 if end of stream reached.
	 */
	public int read(char[] array) {
		return read(array, 0, array.length);
	}

	/**
	 * Read until the given array is filled.
	 * @param array the array to fill.
	 * @throws IOException if end of stream reached
	 */
	public void fill(char[] array) {
		int offset = 0;
		while (offset < array.length) {
			int read = read(array, offset, array.length - offset);
			if (read == EOF) {
				throw new ParserException("end of stream");
			}
			if (read == 0) {
				throw new ParserException("stream blocked");
			}
			offset += read;
		}
	}

	/**
	 * Read a string with the given length.
	 * @param length the length.
	 * @return the string.
	 */
	public String readString(int length) {
		char[] array = new char[length];
		fill(array);
		return new String(array);
	}

	/**
	 * Skip whitespace characters until a non-whitespace character is encountered.
	 * @param minimum the minimum number to skip.
	 * @return the number of whitespace characters skipped.
	 */
	public int skipWhitespace(int minimum) {
		if (minimum < 0) {
			throw new IllegalArgumentException("minimum=" + minimum);
		}
		int skipped = 0;
		while (true) {
			int read = read();
			if (read == EOF) {
				break;
			}
			if (!Character.isWhitespace((char) read)) {
				bufferIndex--; // Backup!
				break;
			}
			skipped++;
		}
		if (skipped < minimum) {
			throw new ParserException("insufficient whitespace skipped: " + skipped + "<" + minimum);
		}
		return skipped;
	}

	/**
	 * Skip whitespace characters until a non-whitespace character is encountered.
	 * @return the number of whitespace characters skipped.
	 */
	public int skipWhitespace() {
		return skipWhitespace(1);
	}

	/**
	 * Skip a new line.
	 * @param optional true if optional.
	 * @return true if a new line was found and skipped.
	 */
	public boolean skipNewLine(boolean optional) {
		boolean newLine = false;
		boolean carriageReturn = false;
		while (true) {
			int read = read();
			if (read == EOF) {
				break;
			}
			if (read == '\n' && !newLine) {
				newLine = true;
			} else if (read == '\r' && !carriageReturn) {
				carriageReturn = true;
			} else {
				bufferIndex--; // Backup!
				break;
			}
		}
		if (!newLine && !carriageReturn) {
			if (!optional) {
				throw new ParserException("new line not found");
			}
			return false;
		}
		return true;
	}

	/**
	 * Skip a new line.
	 */
	public void skipNewLine() {
		skipNewLine(false);
	}

	/**
	 * Skip a new line.
	 * @param optional true if optional.
	 * @param includeEndOfFile true to include the end of the file.
	 * @return true if a new line was found and skipped.
	 */
	public String readToNewLine(boolean optional, boolean includeEndOfFile) {
		int offset = bufferIndex;
		int index = 0;
		boolean newLine = false;
		boolean carriageReturn = false;
		int read = EOF;
		while (true) {
			read = read();
			if (read == EOF) {
				index = bufferSize;
				break;
			}
			if (read == '\n') {
				if (newLine) {
					break;
				}
				if (index == 0) {
					index = bufferIndex;
				}
				newLine = true;
			} else if (read == '\r') {
				if (carriageReturn) {
					break;
				}
				if (index == 0) {
					index = bufferIndex;
				}
				carriageReturn = true;
			} else if (newLine || carriageReturn) {
				break;
			}
		}
		if (!newLine && !carriageReturn && !(includeEndOfFile && read == EOF)) {
			if (!optional) {
				throw new ParserException("new line not found");
			}
			bufferIndex = offset;
			return null;
		}
		if (read != EOF) {
			bufferIndex--; // Backup!
		}
		int length = (index - offset);
		return new String(buffer, offset, length);
	}

	/**
	 * Skip a new line.
	 * @param optional true if optional.
	 * @return true if a new line was found and skipped.
	 */
	public String readToNewLine(boolean optional) {
		return readToNewLine(optional, false);
	}

	/**
	 * Skip a new line.
	 * @return true if a new line was found and skipped.
	 */
	public String readToNewLine() {
		return readToNewLine(false, false);
	}

	/**
	 * Skip all text up to and including the given text.
	 * @param text the text.
	 * @param optional true to make this read optional.
	 * @return the string read.
	 */
	public String readTo(String text, boolean optional) {
		int offset = bufferIndex;
		int textIndex = 0;
		while (updateBuffer()) {
			while (bufferIndex < bufferSize) {
				if (buffer[bufferIndex] == text.charAt(textIndex)) {
					textIndex++;
					if (textIndex == text.length()) {
						bufferIndex++;
						int length = (bufferIndex - offset) - text.length();
						return new String(buffer, offset, length);
					}
				} else {
					textIndex = 0;
				}
				bufferIndex++;
			}
		}
		if (optional) {
			bufferIndex = offset;
			return null;
		}
		throw new ParserException("end of file");
	}

	/**
	 * Skip all text up to and including the given text.
	 * @param text the text.
	 * @return the string read.
	 */
	public String readTo(String text) {
		return readTo(text, false);
	}

	/**
	 * Read until whitespace is encountered.
	 * @param optional the true if the read is optional.
	 * @return the string read.
	 */
	public String readToWhitespace(boolean optional) {
		int offset = bufferIndex;
		while (true) {
			int read = read();
			if (read == EOF) {
				break;
			}
			if (Character.isWhitespace((char) read)) {
				bufferIndex--;
				return new String(buffer, offset, bufferIndex - offset);
			}
		}
		if (optional) {
			bufferIndex = offset;
			return null;
		}
		throw new ParserException("end of file");
	}

	/**
	 * Read until whitespace is encountered.
	 * @return the string read.
	 */
	public String readToWhitespace() {
		return readToWhitespace(false);
	}

	/**
	 * Read a character, expecting it to be the given one.
	 * @param expected the character expected.
	 * @param optional true to make this read optional.
	 * @return true if the character was read.
	 */
	public boolean readChar(char expected, boolean optional) {
		char found = (char) read();
		if (found != expected) {
			bufferIndex--;
			if (!optional) {
				throw new ParserException("Expected character: '" + expected + "' (" + ((int) expected) + "), found: '" + found + "' (" + ((int) found) + ")");
			}
			return false;
		}
		return true;
	}

	/**
	 * Read a character, expecting it to be the given one.
	 * @param expected the character expected.
	 */
	public void readChar(char expected) {
		readChar(expected, false);
	}

	/**
	 * Read a character, expecting it to be the given one.
	 * @param expected the characters expected.
	 * @return the read character.
	 */
	public char readChar(char[] expected) {
		char found = (char) read();
		if (!Chars.contains(expected, found)) {
			bufferIndex--;
			throw new ParserException("Expected character(s): " + Arrays.toString(expected) + ", found: '" + found + "' (" + ((int) found) + ")");
		}
		return found;
	}

	/**
	 * Read text, expecting it to be the given text.
	 * @param expected the text expected.
	 * @param optional true to make the expected text optional.
	 * @return the string read.
	 */
	public boolean readString(String expected, boolean optional) {
		int offset = bufferIndex;
		boolean found = true;
		for (int i = 0; i < expected.length(); i++) {
			int read = read();
			if (read == EOF || read != expected.charAt(i)) {
				found = false;
				break;
			}
		}
		if (!found) {
			bufferIndex = offset;
			if (!optional) {
				throw new ParserException("Expected string not found: '" + expected + "'");
			}
		}
		return found;
	}

	/**
	 * Read text, expecting it to be the given text.
	 * @param expected the text expected.
	 */
	public void readString(String expected) {
		readString(expected, false);
	}

	/**
	 * Read a string.
	 * @param optional true if optional.
	 * @return the string.
	 */
	public String readLine(boolean optional) {
		int offset = bufferIndex;
		boolean eof = false;
		while (true) {
			int read = read();
			if (read == EOF) {
				eof = true;
				break;
			}
			if (read == '\r' || read == '\n') {
				bufferIndex--; // Backup!
				break;
			}
		}
		if (offset == bufferIndex) {
			if (optional) {
				if (eof) {
					return null;
				}
				return "";
			}
			throw new ParserException("no string found");
		}
		return new String(buffer, offset, bufferIndex - offset);
	}

	/**
	 * Read a string.
	 * @return the string.
	 */
	public String readLine() {
		return readLine(false);
	}

	/**
	 * Parse an integer string.
	 * @param optional true make the parse optional.
	 * @return the integer string.
	 */
	public String parseIntegerString(boolean optional) {
		int offset = bufferIndex;
		int length = 0;
		while (true) {
			int read = read();
			if (read == EOF) {
				break;
			}
			if (!Character.isDigit(read) && !(length == 0 && read == '-')) {
				bufferIndex--; // Backup!
				break;
			}
			length++;
		}
		if (length == 0) {
			if (optional) {
				return null;
			}
			throw new ParserException("long not found");
		}
		return new String(buffer, offset, bufferIndex - offset);
	}

	/**
	 * Parse a decimal string.
	 * @param optional true make the parse optional.
	 * @return the decimal string.
	 */
	public String parseDecimalString(boolean optional) {
		int offset = bufferIndex;
		while (true) {
			int read = read();
			if (read == EOF) {
				break;
			}
			if (read != '.' && !Character.isDigit(read)) {
				bufferIndex--; // Backup!
				break;
			}
		}
		if (offset == bufferIndex) {
			if (optional) {
				return null;
			}
			throw new ParserException("decimal not found");
		}
		return new String(buffer, offset, bufferIndex - offset);
	}

	/**
	 * Parse a long.
	 * @return the long.
	 */
	public long parseLong() {
		String integer = parseIntegerString(false);
		return Long.parseLong(integer);
	}

	/**
	 * Parse a long.
	 * @param defaultValue the default value.
	 * @return the long, or the default value if nothing parsed.
	 */
	public long parseLong(long defaultValue) {
		String integer = parseIntegerString(true);
		if (integer == null) {
			return defaultValue;
		}
		return Long.parseLong(integer);
	}

	/**
	 * Parse a big integer.
	 * @return the big integer.
	 */
	public BigInteger parseBigInteger() {
		String integer = parseIntegerString(false);
		return new BigInteger(integer);
	}

	/**
	 * Parse a big integer.
	 * @param defaultValue the default value.
	 * @return the big integer, or the default value if nothing parsed.
	 */
	public BigInteger parseBigInteger(BigInteger defaultValue) {
		String integer = parseIntegerString(true);
		if (integer == null) {
			return defaultValue;
		}
		return new BigInteger(integer);
	}

	/**
	 * Parse a double.
	 * @return the double.
	 */
	public double parseDouble() {
		String decimal = parseDecimalString(false);
		return Double.parseDouble(decimal);
	}

	/**
	 * Parse a double.
	 * @param defaultValue the default value.
	 * @return the double, or the default value if nothing parsed.
	 */
	public double parseDouble(double defaultValue) {
		String decimal = parseDecimalString(true);
		if (decimal == null) {
			return defaultValue;
		}
		return Double.parseDouble(decimal);
	}

	/**
	 * Parse a big decimal.
	 * @return the big decimal.
	 */
	public BigDecimal parseBigDecimal() {
		String decimal = parseDecimalString(false);
		return new BigDecimal(decimal);
	}

	/**
	 * Parse a big decimal.
	 * @param defaultValue the default value.
	 * @return the big decimal, or the default value if nothing parsed.
	 */
	public BigDecimal parseBigDecimal(BigDecimal defaultValue) {
		String decimal = parseDecimalString(false);
		if (decimal == null) {
			return defaultValue;
		}
		return new BigDecimal(decimal);
	}
}
