package com.javabi.common.io;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.google.common.base.Charsets;
import com.google.common.base.Throwables;

public class UserInput {

	private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in, Charsets.UTF_8));

	public static String readLine() {
		try {
			return reader.readLine();
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	public static String readLine(String prompt) {
		System.out.print(prompt);
		return readLine();
	}

}
