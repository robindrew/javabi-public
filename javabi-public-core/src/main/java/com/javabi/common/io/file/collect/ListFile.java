package com.javabi.common.io.file.collect;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.collect.ForwardingList;
import com.google.common.collect.ImmutableList;
import com.javabi.common.io.file.Files;
import com.javabi.common.lang.Java;

public class ListFile extends ForwardingList<String>implements ICollectionFile<List<String>> {

	private final File file;
	private final List<String> list;
	private Charset charset = Charsets.UTF_8;
	private String lineSeparator = Java.getLineSeparator();

	public ListFile(File file, List<String> list) {
		if (file == null) {
			throw new NullPointerException("file");
		}
		if (list == null) {
			throw new NullPointerException("list");
		}
		this.file = file;
		this.list = list;
	}

	@Override
	protected List<String> delegate() {
		return list;
	}

	protected void validateLine(String line) {
		if (line.contains(lineSeparator)) {
			throw new IllegalArgumentException("invalid line: '" + line + "'");
		}
	}

	@Override
	public void read() {
		list.clear();
		if (file.exists() && file.length() > 0) {
			Files.readToLines(file, list, charset);
		}
	}

	@Override
	public void write() {
		Files.writeFromLines(file, list, charset, lineSeparator);
	}

	@Override
	public boolean add(String line) {
		if (line == null) {
			throw new NullPointerException("line");
		}
		validateLine(line);
		return super.add(line);
	}

	@Override
	public boolean addAll(Collection<? extends String> lines) {
		if (lines == null) {
			throw new NullPointerException("line");
		}
		boolean added = false;
		for (String line : lines) {
			added = added || add(line);
		}
		return added;
	}

	public List<String> toList() {
		return ImmutableList.copyOf(list);
	}

	@Override
	public List<String> toCollection() {
		return toList();
	}

}
