package com.javabi.common.io.pipe;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.google.common.base.Throwables;

public class ByteBufferPipe implements IByteBufferPipe {

	private static final int DEFAULT_BUFFER_LENGTH = Short.MAX_VALUE;
	private static final long NO_LIMIT = 0;

	private final byte[] buffer;

	public ByteBufferPipe(int bufferLength) {
		if (bufferLength < 1) {
			throw new IllegalArgumentException("bufferLength=" + bufferLength);
		}
		buffer = new byte[bufferLength];
	}

	public ByteBufferPipe() {
		this(DEFAULT_BUFFER_LENGTH);
	}

	@Override
	public int getBufferSize() {
		return buffer.length;
	}

	@Override
	public final long pipe(InputStream input, OutputStream output, long limit) {
		return pipe(input, output, null, limit);
	}

	@Override
	public final long pipe(InputStream input, OutputStream output, IBufferPipeObserver observer, long limit) {
		if (limit < NO_LIMIT) {
			throw new IllegalArgumentException("limit=" + limit);
		}
		long read = 0;
		try {
			while (true) {
				int readLimit = buffer.length;
				if (limit > NO_LIMIT && (limit - read) < buffer.length) {
					readLimit = (int) (limit - read);
				}

				long before = System.nanoTime();
				int length = input.read(buffer, 0, readLimit);
				if (length == -1) {
					if (read > 0) {
						output.flush();
					}
					break;
				}
				if (length == 0) {
					// Handle reads of zero bytes?
					continue;
				}

				read += length;
				output.write(buffer, 0, length);
				if (observer != null) {
					long after = System.nanoTime();
					observer.piped(length, after - before);
				}

				// Limit reached ...
				if (limit > NO_LIMIT && limit == read) {
					break;
				}
			}
		} catch (IOException ioe) {
			throw Throwables.propagate(ioe);
		}

		return read;
	}

	@Override
	public final long pipe(InputStream input, OutputStream output) {
		return pipe(input, output, null, NO_LIMIT);
	}

	@Override
	public final long pipe(InputStream input, OutputStream output, IBufferPipeObserver observer) {
		return pipe(input, output, observer, NO_LIMIT);
	}
}
