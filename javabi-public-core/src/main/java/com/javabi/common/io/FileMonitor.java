package com.javabi.common.io;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import com.google.common.io.Files;

/**
 * A File Monitor.
 */
public class FileMonitor implements Runnable {

	/** The modified time for a file that does not exist. */
	private static final long FILE_NOT_EXISTS = 0L;

	/**
	 * Returns true if the file exists.
	 * @param lastModified the last modified date.
	 * @return true if the file exists.
	 */
	public static final boolean fileExists(long lastModified) {
		return lastModified != FILE_NOT_EXISTS;
	}

	/** The time last modified. */
	private volatile long lastModified;
	/** The file. */
	private final File file;

	/**
	 * Creates a new file monitor.
	 * @param file the file to monitor.
	 */
	public FileMonitor(File file) {
		if (file == null) {
			throw new NullPointerException();
		}
		this.file = file;
		this.lastModified = file.lastModified();
	}

	/**
	 * Creates a new file monitor.
	 * @param filename the name of the file to monitor.
	 */
	public FileMonitor(String filename) {
		this(new File(filename));
	}

	/**
	 * Returns the file.
	 * @return the file.
	 */
	public final File getFile() {
		return file;
	}

	/**
	 * Write this file.
	 * @param text the text.
	 * @param charset the character set.
	 */
	public void modifyFile(String text, Charset charset) throws IOException {
		synchronized (file) {
			Files.write(text, file, charset);
		}
	}

	/**
	 * Write this file.
	 * @param bytes the bytes.
	 */
	public void modifyFile(byte[] bytes) throws IOException {
		synchronized (file) {
			Files.write(bytes, file);
		}
	}

	/**
	 * Returns true if the file exists.
	 * @return true if the file exists.
	 */
	public final boolean fileExists() {
		return lastModified != FILE_NOT_EXISTS;
	}

	/**
	 * Called to indicate the file has just been deleted.
	 */
	protected void fileDeleted() {
	}

	/**
	 * Called to indicate the file has just been created.
	 */
	protected void fileCreated() {
	}

	/**
	 * Called to indicate the file has just been modified.
	 */
	protected void fileModified() {
	}

	/**
	 * Run this monitor.
	 */
	public void run() {
		synchronized (file) {
			long modified = file.lastModified();
			if (lastModified != modified) {
				if (fileExists(modified)) {

					// Newly created? (or just modified)
					if (!fileExists()) {
						lastModified = file.lastModified();
						fileCreated();
						fileModified();
					} else {
						lastModified = file.lastModified();
						fileModified();
					}
				} else {

					// Newly deleted
					lastModified = modified;
					fileDeleted();
				}
			}
		}
	}
}
