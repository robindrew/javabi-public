package com.javabi.common.io.connection;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.concurrent.Threads;

public class SocketConnector implements IConnector {

	private static final Logger log = LoggerFactory.getLogger(SocketConnector.class);

	private final INetworkAddress address;
	private final long retryInterval;
	private final TimeUnit retryUnit;
	private volatile IConnection connection = null;

	public SocketConnector(INetworkAddress address, long retryInterval, TimeUnit retryUnit) {
		if (retryInterval < 1) {
			throw new IllegalArgumentException("retryInterval=" + retryInterval);
		}
		if (address == null) {
			throw new NullPointerException("address");
		}
		if (retryUnit == null) {
			throw new NullPointerException("unit");
		}
		this.address = address;
		this.retryInterval = retryInterval;
		this.retryUnit = retryUnit;
	}

	public SocketConnector(INetworkAddress address) {
		this(address, 15, SECONDS);
	}

	@Override
	public IConnection getConnection() {
		synchronized (this) {
			if (!isConnected()) {
				connection = connectAndRetry();
			}
			return connection;
		}
	}

	@Override
	public boolean isConnected() {
		synchronized (this) {
			return connection != null && connection.isConnected();
		}
	}

	private IConnection connectAndRetry() {
		while (true) {

			// Connect
			log.info("Connecting to " + address);
			try {
				return new SocketConnection(address);
			} catch (Throwable t) {
				t = Throwables.getRootCause(t);
				log.warn("Error connecting to " + address, t);
			}

			// Sleep
			log.info("Retrying connection to " + address + " in " + retryInterval + " " + retryUnit);
			Threads.sleep(retryInterval, retryUnit);
		}
	}

	@Override
	public void disconnect() {
		synchronized (this) {
			if (connection != null) {
				connection.disconnect();
				connection = null;
			}
		}
	}

}
