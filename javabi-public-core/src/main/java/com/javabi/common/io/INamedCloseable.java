package com.javabi.common.io;

import java.io.IOException;

public interface INamedCloseable extends AutoCloseable {

	String getName();

	@Override
	void close() throws IOException;

}
