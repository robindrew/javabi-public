package com.javabi.common.io.file.indexed;

import java.io.File;
import java.io.IOException;

import com.javabi.common.io.data.IDataReader;
import com.javabi.common.io.data.IDataSerializer;
import com.javabi.common.io.data.IDataWriter;

public class IndexedFileSerializer implements IDataSerializer<IndexedFile> {

	private final File directory;

	public IndexedFileSerializer(File directory) {
		if (directory == null) {
			throw new NullPointerException("directory");
		}
		this.directory = directory;
	}

	@Override
	public IndexedFile readObject(IDataReader reader) throws IOException {
		String filename = reader.readString(false);
		long length = reader.readPositiveLong();
		long timeCreated = reader.readDynamicLong();
		long timeModified = reader.readDynamicLong();
		String hash = reader.readString(false);
		File file = new File(directory, filename);
		return new IndexedFile(file, length, timeCreated, timeModified, hash);
	}

	@Override
	public void writeObject(IDataWriter writer, IndexedFile instance) throws IOException {
		writer.writeString(instance.getFile().getName(), false);
		writer.writePositiveLong(instance.getLength());
		writer.writeDynamicLong(instance.getTimeCreated());
		writer.writeDynamicLong(instance.getTimeModified());
		writer.writeString(instance.getHash(), false);
	}

}
