package com.javabi.common.io.file.indexed;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.NavigableSet;
import java.util.Set;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;

public class IndexedFileMap {

	private final TreeMultimap<File, IndexedFile> directoryMap = TreeMultimap.create();
	private final Multimap<String, IndexedFile> hashMap = HashMultimap.create();

	public Set<String> getHashCollisions() {
		Set<String> collisions = new HashSet<>();
		Set<String> hashes = hashMap.keySet();
		for (String hash : hashes) {
			if (hashMap.get(hash).size() > 1) {
				collisions.add(hash);
			}
		}
		return collisions;
	}

	public void put(IndexedFile file) {
		put(file.getDirectory(), file);
	}

	public void put(File directory, IndexedFile file) {
		if (!file.hasHash()) {
			throw new IllegalArgumentException("file without hash: " + file);
		}
		removeHash(directory, file);
		directoryMap.put(directory, file);
		updateHash(file);
	}

	private void removeHash(File directory, IndexedFile file) {
		IndexedFile existing = getIndexedFile(file.getFile());
		if (existing != null) {
			hashMap.remove(existing.getHash(), existing);
		}
	}

	private IndexedFile getIndexedFile(File file) {
		File directory = file.getParentFile();
		NavigableSet<IndexedFile> files = directoryMap.get(directory);
		for (IndexedFile indexed : files) {
			if (indexed.getFile().equals(file)) {
				return indexed;
			}
		}
		return null;
	}

	private void updateHash(IndexedFile file) {
		String hash = file.getHash();
		hashMap.put(hash, file);
	}

	public Set<File> getDirectories() {
		return ImmutableSet.copyOf(directoryMap.keySet());
	}

	public List<IndexedFile> getFiles(String hash) {
		return ImmutableList.copyOf(hashMap.get(hash));
	}

	public List<IndexedFile> getFiles(File directory) {
		return ImmutableList.copyOf(directoryMap.get(directory));
	}

	public boolean contains(IndexedFile file) {
		IndexedFile existing = getIndexedFile(file.getFile());
		return existing != null && existing.isIdenticalTo(file);
	}
}
