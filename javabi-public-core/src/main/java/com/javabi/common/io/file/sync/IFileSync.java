package com.javabi.common.io.file.sync;

public interface IFileSync {

	void syncFiles();

	int getModified();

	int getCreated();

	int getDeleted();

	void ignoreDirectory(String path);

	void setBinaryMatch(boolean match);

	void setLastModifiedMatch(boolean match);

	void setDeleteFiles(boolean delete);
}
