package com.javabi.common.io.scp.jsch;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.io.connection.INetworkUser;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public abstract class JSchOperation {

	private static final Logger log = LoggerFactory.getLogger(JSchOperation.class);

	protected Session getSession(INetworkUser user) throws Exception {
		String username = user.getUsername();
		String host = user.getAddress().getHostname();
		int port = user.getAddress().getPort();
		Session session = new JSch().getSession(username, host, port);
		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		session.setUserInfo(new JSchUserInfo(user));
		return session;
	}

	protected void check(InputStream input) throws Exception {
		int code = input.read();
		if (code != 0) {
			String error = readTo(input, '\n');
			throw new IOException("SCP error: '" + error + "' (code=" + code + ")");
		}
	}

	protected void read(InputStream input, String ascii) throws Exception {
		for (int i = 0; i < ascii.length(); i++) {
			int read = input.read();
			if (read == -1) {
				throw new IOException("Unexpected end of stream");
			}
			if (read != ascii.charAt(i)) {
				throw new IOException("Unexpected character read: " + read + " != " + ascii.charAt(i));
			}
		}
	}

	protected void write(OutputStream output, String ascii) throws IOException {
		output.write(ascii.getBytes());
		output.flush();
	}

	protected void write(OutputStream output, int value) throws IOException {
		output.write(value);
		output.flush();
	}

	protected String readTo(InputStream input, char character) throws Exception {
		StringBuffer ascii = new StringBuffer();
		while (true) {
			int read = input.read();
			if (read == -1) {
				throw new IOException("Unexpected end of stream");
			}
			if (read == character) {
				break;
			}
			ascii.append((char) read);
		}
		return ascii.toString();
	}

	protected void close(Channel channel) {
		try {
			channel.disconnect();
		} catch (Exception e) {
			log.debug("Error closing channel: " + channel, e);
		}
	}

	protected void close(Session session) {
		try {
			session.disconnect();
		} catch (Exception e) {
			log.debug("Error closing session: " + session, e);
		}
	}
}
