package com.javabi.common.io.connection;

public interface IConnector {

	boolean isConnected();

	IConnection getConnection();

	void disconnect();
	
}
