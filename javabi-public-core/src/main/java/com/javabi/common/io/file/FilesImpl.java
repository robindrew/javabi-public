package com.javabi.common.io.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.common.io.LineProcessor;
import com.javabi.common.crypto.Md5Encoder;
import com.javabi.common.io.RuntimeIOException;
import com.javabi.common.io.file.lineprocessor.CollectionLineProcessor;
import com.javabi.common.lang.Java;
import com.javabi.common.text.StringFormats;

public class FilesImpl implements IFiles {

	public static final FileFilter ACCEPT_ALL = file -> true;
	public static final FileFilter ACCEPT_NONE = file -> false;
	public static final Charset DEFAULT_CHARSET = Charsets.UTF_8;
	public static final String DEFAULT_LINE_SEPARATOR = Java.getLineSeparator();

	@Override
	public byte[] readToBytes(File file) {
		try {
			return Files.toByteArray(file);
		} catch (IOException ioe) {
			throw new RuntimeIOException(ioe);
		}
	}

	@Override
	public String readToString(File file) {
		return readToString(file, DEFAULT_CHARSET);
	}

	@Override
	public String readToString(File file, Charset charset) {
		try {
			return com.google.common.io.Files.toString(file, charset);
		} catch (IOException ioe) {
			throw new RuntimeIOException(ioe);
		}
	}

	@Override
	public List<File> listContents(File directory) {
		if (!directory.isDirectory()) {
			throw new IllegalArgumentException("Not a directory: " + directory);
		}

		File[] files = directory.listFiles();
		if (files == null) {
			return Collections.emptyList();
		}
		return Arrays.asList(files);
	}

	@Override
	public List<File> listContents(File directory, FileFilter filter) {
		if (!directory.isDirectory()) {
			throw new IllegalArgumentException("Not a directory: " + directory);
		}
		if (filter == null) {
			throw new NullPointerException("filter");
		}

		File[] files = directory.listFiles();
		if (files == null) {
			return Collections.emptyList();
		}
		List<File> list = new ArrayList<>();
		for (File file : files) {
			if (filter.accept(file)) {
				list.add(file);
			}
		}
		return list;
	}

	@Override
	public List<File> listFiles(File directory, boolean recursive) {
		return listFiles(directory, recursive, ACCEPT_ALL);
	}

	@Override
	public List<File> listFiles(File directory, boolean recursive, FileFilter filter) {
		if (filter == null) {
			throw new NullPointerException("filter");
		}
		if (!directory.isDirectory()) {
			throw new IllegalArgumentException("Not a directory: " + directory);
		}

		List<File> list = new ArrayList<>();

		if (!recursive) {
			File[] files = directory.listFiles();
			if (files != null) {
				for (File file : files) {
					if (filter == ACCEPT_ALL || filter.accept(file)) {
						list.add(file);
					}
				}
			}
		}

		else {
			try {
				Path path = directory.toPath();
				java.nio.file.Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

					@Override
					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
						if (filter == ACCEPT_ALL || filter.accept(file.toFile())) {
							list.add(file.toFile());
						}
						return FileVisitResult.CONTINUE;
					}
				});
			} catch (IOException ioe) {
				throw new RuntimeIOException(ioe);
			}
		}
		return list;
	}

	@Override
	public File existingDirectory(File directory, boolean create) {
		if (!directory.exists()) {
			if (!create) {
				throw new IllegalArgumentException("directory does not exist: " + directory);
			}
			if (!directory.mkdirs()) {
				throw new IllegalArgumentException("unable to create directory: " + directory);
			}
		}
		if (!directory.isDirectory()) {
			throw new IllegalArgumentException("file is not a directory: " + directory);
		}
		return directory;
	}

	@Override
	public File existingDirectory(String filename, boolean create) {
		return existingDirectory(new File(filename), create);
	}

	@Override
	public File existingFile(File file, boolean create) {
		if (!file.exists()) {
			if (!create) {
				throw new IllegalArgumentException("file does not exist: " + file);
			}
			try {
				if (!file.createNewFile()) {
					throw new IllegalArgumentException("unable to set modified for file: " + file);
				}
			} catch (IOException ioe) {
				throw new RuntimeIOException(ioe);
			}
		}
		if (file.isDirectory()) {
			throw new IllegalArgumentException("file is a directory: " + file);
		}
		return file;
	}

	@Override
	public File existingFile(String filename, boolean create) {
		return existingFile(new File(filename), create);
	}

	@Override
	public void copy(File from, File to) {
		try {
			Files.copy(from, to);
		} catch (IOException ioe) {
			throw new RuntimeIOException(ioe);
		}
	}

	@Override
	public void move(File from, File to) {
		try {
			Files.move(from, to);
		} catch (IOException ioe) {
			throw new RuntimeIOException(ioe);
		}
	}

	@Override
	public void writeFromBytes(File file, byte[] contents) {
		try {
			Files.write(contents, file);
		} catch (IOException ioe) {
			throw new RuntimeIOException(ioe);
		}
	}

	@Override
	public void writeFromString(File file, String contents) {
		writeFromString(file, contents, DEFAULT_CHARSET);
	}

	@Override
	public void writeFromString(File file, String contents, Charset charset) {
		try {
			Files.write(contents, file, charset);
		} catch (IOException ioe) {
			throw new RuntimeIOException(ioe);
		}
	}

	@Override
	public File existingDirectory(File file) {
		return existingDirectory(file, false);
	}

	@Override
	public File existingDirectory(String filename) {
		return existingDirectory(filename, false);
	}

	@Override
	public File existingFile(File file) {
		return existingFile(file, false);
	}

	@Override
	public File existingFile(String filename) {
		return existingFile(filename, false);
	}

	@Override
	public byte[] readToMd5(File file) {
		return new Md5Encoder().encodeToBytes(file);
	}

	@Override
	public String readToMd5String(File file) {
		byte[] bytes = readToMd5(file);
		return StringFormats.hex(bytes);
	}

	@Override
	public File getRoot(File file) {
		while (true) {
			if (file.getParentFile() == null) {
				return file;
			}
			file = file.getParentFile();
		}
	}

	@Override
	public <R> R readToLines(File file, LineProcessor<R> processor) {
		return readToLines(file, processor, DEFAULT_CHARSET);
	}

	@Override
	public <R> R readToLines(File file, LineProcessor<R> processor, Charset charset) {
		try {
			return Files.readLines(file, charset, processor);
		} catch (IOException ioe) {
			throw new RuntimeIOException(ioe);
		}
	}

	@Override
	public BasicFileAttributes getAttributes(File file) {
		try {
			return java.nio.file.Files.readAttributes(file.toPath(), BasicFileAttributes.class);
		} catch (IOException ioe) {
			throw new RuntimeIOException(ioe);
		}
	}

	@Override
	public long getCreationTime(File file) {
		return getAttributes(file).creationTime().toMillis();
	}

	@Override
	public FileFilter acceptAllFilter() {
		return ACCEPT_ALL;
	}

	@Override
	public FileFilter acceptNoneFilter() {
		return ACCEPT_NONE;
	}

	@Override
	public FileFilter acceptNameEndsWithFilter(String suffix, boolean ignoreCase) {
		return new EndsWithFilter(suffix, ignoreCase);
	}

	@Override
	public FileFilter acceptNameMatchesFilter(String regex) {
		return file -> file.getName().matches(regex);
	}

	@Override
	public void writeFromLines(File file, Iterable<? extends String> lines) {
		writeFromLines(file, lines, DEFAULT_CHARSET);
	}

	@Override
	public void writeFromLines(File file, Iterable<? extends String> lines, Charset charset) {
		writeFromLines(file, lines, DEFAULT_CHARSET, DEFAULT_LINE_SEPARATOR);

	}

	@Override
	public void writeFromLines(File file, Iterable<? extends String> lines, Charset charset, String lineSeparator) {
		try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), charset))) {
			for (String line : lines) {
				writer.write(line);
				writer.write(lineSeparator);
			}
			writer.flush();
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}

	@Override
	public void readToLines(File file, Collection<String> lines) {
		readToLines(file, lines, DEFAULT_CHARSET);
	}

	@Override
	public void readToLines(File file, Collection<String> lines, Charset charset) {
		try {
			Files.readLines(file, charset, new CollectionLineProcessor(lines));
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}

}
