package com.javabi.common.io;

import java.io.IOException;

public class RuntimeIOException extends RuntimeException {

	private static final long serialVersionUID = 5368087475891979938L;

	public RuntimeIOException(IOException cause) {
		super(cause);
	}
}
