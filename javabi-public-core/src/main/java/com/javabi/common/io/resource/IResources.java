package com.javabi.common.io.resource;

import java.net.URL;
import java.nio.charset.Charset;

public interface IResources {

	boolean exists(String resourceName);

	URL getResource(String resourceName, boolean optional);

	String readToString(String resourceName);

	String readToString(String resourceName, Charset charset);

	String readToString(URL url);

	String readToString(URL url, Charset charset);

}
