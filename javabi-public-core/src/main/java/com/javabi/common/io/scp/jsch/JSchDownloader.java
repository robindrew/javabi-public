package com.javabi.common.io.scp.jsch;

import static com.javabi.common.text.StringFormats.bytes;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.common.io.connection.INetworkUser;
import com.javabi.common.io.pipe.ByteBufferPipe;
import com.javabi.common.lang.Quietly;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.Session;

class JSchDownloader extends JSchOperation {

	private static final Logger log = LoggerFactory.getLogger(JSchDownloader.class);
	private final INetworkUser user;
	private final File localFile;
	private final String remoteFilename;

	public JSchDownloader(INetworkUser user, String remoteFilename, File localFile) {
		this.user = user;
		this.localFile = localFile;
		this.remoteFilename = remoteFilename;
	}

	public void download() {
		if (localFile.isDirectory()) {
			throw new IllegalArgumentException("Local file is a directory: " + localFile);
		}
		log.info("scp " + user.getUsername() + "@" + user.getAddress().getHostname() + ":" + remoteFilename + " " + localFile);
		try {

			// Session
			Session session = getSession(user);
			session.connect();
			try {
				download(session);
			} finally {
				close(session);
			}
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	private void download(Session session) throws Exception {
		ChannelExec channel = (ChannelExec) session.openChannel("exec");
		channel.setCommand("scp -f " + remoteFilename);

		InputStream input = new BufferedInputStream(channel.getInputStream());
		OutputStream output = new BufferedOutputStream(channel.getOutputStream());
		channel.connect();
		try {

			// Write 0 to kick things off
			write(output, 0);

			// Read C0644
			read(input, "C0644 ");

			// Read file length
			String text = readTo(input, ' ');
			long length = Long.parseLong(text);

			// Read file!
			String filename = readTo(input, '\n');

			// Write 0 to acknowledge we are happy
			write(output, 0);

			// Download file
			log.info("Downloading: " + localFile + " (" + bytes(length) + ") from " + filename);
			NanoTimer timer = new NanoTimer();
			timer.start();
			FileOutputStream fileOutput = new FileOutputStream(localFile);
			try {
				new ByteBufferPipe().pipe(input, fileOutput, length);
				fileOutput.flush();
			} finally {
				Quietly.close(fileOutput);
			}
			timer.stop();
			log.info("Downloaded: " + localFile + " in " + timer);

			Quietly.close(output);
		} finally {
			close(channel);
		}
	}

}
