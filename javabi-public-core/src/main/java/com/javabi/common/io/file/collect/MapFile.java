package com.javabi.common.io.file.collect;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Map;

import com.google.common.base.Charsets;
import com.google.common.collect.ForwardingMap;
import com.google.common.collect.ImmutableMap;
import com.javabi.common.io.file.Files;
import com.javabi.common.io.file.lineprocessor.MapLineProcessor;
import com.javabi.common.lang.Java;

public class MapFile extends ForwardingMap<String, String>implements IMapFile<Map<String, String>> {

	private final File file;
	private final Map<String, String> map;
	private Charset charset = Charsets.UTF_8;
	private String lineSeparator = Java.getLineSeparator();
	private String entrySeparator = "|";

	public MapFile(File file, Map<String, String> set) {
		if (file == null) {
			throw new NullPointerException("file");
		}
		if (set == null) {
			throw new NullPointerException("set");
		}
		this.file = file;
		this.map = set;
	}

	@Override
	protected Map<String, String> delegate() {
		return map;
	}

	protected void validateLine(String key, String value) {
		if (key.contains(lineSeparator)) {
			throw new IllegalArgumentException("invalid key: '" + key + "'");
		}
		if (value.contains(lineSeparator)) {
			throw new IllegalArgumentException("invalid value: '" + value + "'");
		}
	}

	@Override
	public void read() {
		map.clear();
		if (file.exists() && file.length() > 0) {
			Files.readToLines(file, new MapLineProcessor(map, entrySeparator), charset);
		}
	}

	@Override
	public void write() {
		Files.writeFromLines(file, new MapLineIterable(), charset, lineSeparator);
	}

	@Override
	public String put(String key, String value) {
		if (key == null) {
			throw new NullPointerException("line");
		}
		if (value == null) {
			throw new NullPointerException("value");
		}
		validateLine(key, value);
		return super.put(key, value);
	}

	@Override
	public void putAll(Map<? extends String, ? extends String> map) {
		if (map == null) {
			throw new NullPointerException("map");
		}
		for (Entry<? extends String, ? extends String> entry : map.entrySet()) {
			put(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public Map<String, String> toMap() {
		return ImmutableMap.copyOf(map);
	}

	private class MapLineIterable implements Iterable<String> {

		@Override
		public Iterator<String> iterator() {
			return new MapLineIterator();
		}
	}

	private class MapLineIterator implements Iterator<String> {

		private final Iterator<Entry<String, String>> iterator = map.entrySet().iterator();

		@Override
		public boolean hasNext() {
			return iterator.hasNext();
		}

		@Override
		public String next() {
			Entry<String, String> entry = iterator.next();
			return entry.getKey() + entrySeparator + entry.getValue();
		}
	}
}
