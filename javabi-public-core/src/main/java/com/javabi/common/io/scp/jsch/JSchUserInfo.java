package com.javabi.common.io.scp.jsch;

import com.javabi.common.io.connection.INetworkUser;
import com.jcraft.jsch.UserInfo;

class JSchUserInfo implements UserInfo {

	private final INetworkUser user;

	public JSchUserInfo(INetworkUser user) {
		this.user = user;
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public boolean promptPassword(String prompt) {
		return user.getPassword() != null;
	}

	@Override
	public String getPassphrase() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean promptPassphrase(String arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean promptYesNo(String arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void showMessage(String arg0) {
		throw new UnsupportedOperationException();
	}

}
