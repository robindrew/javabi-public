package com.javabi.common.io.file;

import static com.javabi.common.lang.Variables.existsDirectory;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Pattern;

import com.google.common.base.Throwables;

public class FileTree implements IFileSource {

	private final File directory;
	private final Set<Pattern> includePatterns = new HashSet<Pattern>();
	private final Set<Pattern> excludePatterns = new HashSet<Pattern>();
	private boolean skipHiddenDirectories = false;

	public FileTree(String directory) {
		this.directory = existsDirectory("directory", directory);
	}

	public FileTree(File directory) {
		this.directory = existsDirectory("directory", directory);
	}

	public File getDirectory() {
		return directory;
	}

	public FileTree include(String regex) {
		Pattern pattern = Pattern.compile(regex);
		includePatterns.add(pattern);
		return this;
	}

	public FileTree exclude(String regex) {
		Pattern pattern = Pattern.compile(regex);
		excludePatterns.add(pattern);
		return this;
	}

	public FileTree setSkipHiddenDirectories(boolean skip) {
		skipHiddenDirectories = skip;
		return this;
	}

	public boolean matches(String name) {

		// If include patterns exist, they must match
		if (!includePatterns.isEmpty()) {
			for (Pattern pattern : includePatterns) {
				if (pattern.matcher(name).matches()) {
					return true;
				}
			}
			return false;
		}

		// Exclude if in exclude list
		if (!excludePatterns.isEmpty()) {
			for (Pattern pattern : excludePatterns) {
				if (pattern.matcher(name).matches()) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public Set<File> getFiles(boolean notEmpty) {
		final Path path = directory.toPath();
		final Set<File> fileSet = new LinkedHashSet<File>();

		// File visitor
		FileVisitor<Path> visitor = new SimpleFileVisitor<Path>() {

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
				String name = file.toString();
				if (matches(name)) {
					fileSet.add(file.toFile());
				}
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult preVisitDirectory(Path directory, BasicFileAttributes attrs) {
				if (skipHiddenDirectories && directory.toString().startsWith(".")) {
					return FileVisitResult.SKIP_SUBTREE;
				}
				return FileVisitResult.CONTINUE;
			}

		};

		// Walk the file tree
		try {
			Files.walkFileTree(path, visitor);
		} catch (IOException e) {
			throw Throwables.propagate(e);
		}

		// Not empty?
		if (notEmpty && fileSet.isEmpty()) {
			throw new IllegalArgumentException("No files found in directory: " + directory + ", includes=" + includePatterns + ", excludes=" + excludePatterns);
		}

		return fileSet;
	}

}
