package com.javabi.common.io.file;

import java.io.File;
import java.io.FileFilter;
import java.nio.charset.Charset;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.List;

import com.google.common.io.LineProcessor;

public class Files {

	private Files() {
		// Inaccessible constructor
	}

	private static volatile IFiles instance = new FilesImpl();

	public static void copy(File from, File to) {
		instance.copy(from, to);
	}

	public static void move(File from, File to) {
		instance.move(from, to);
	}

	public static byte[] readToMd5(File file) {
		return instance.readToMd5(file);
	}

	public static String readToMd5String(File file) {
		return instance.readToMd5String(file);
	}

	public static byte[] readToBytes(File file) {
		return instance.readToBytes(file);
	}

	public static void writeFromBytes(File file, byte[] contents) {
		instance.writeFromBytes(file, contents);
	}

	public static void writeFromString(File file, String contents) {
		instance.writeFromString(file, contents);
	}

	public static void writeFromString(File file, String contents, Charset charset) {
		instance.writeFromString(file, contents, charset);
	}

	public static IFiles getFiles() {
		return instance;
	}

	public static void setFiles(IFiles files) {
		if (files == null) {
			throw new NullPointerException("files");
		}
		instance = files;
	}

	public static String readToString(File file) {
		return instance.readToString(file);
	}

	public static String readToString(File file, Charset charset) {
		return instance.readToString(file, charset);
	}

	public static List<File> listContents(File directory) {
		return instance.listContents(directory);
	}

	public static List<File> listContents(File directory, FileFilter filter) {
		return instance.listContents(directory, filter);
	}

	public static List<File> listFiles(File directory, boolean recursive) {
		return instance.listFiles(directory, recursive);
	}

	public static List<File> listFiles(File directory, boolean recursive, FileFilter filter) {
		return instance.listFiles(directory, recursive, filter);
	}

	public static File existingDirectory(File file) {
		return instance.existingDirectory(file);
	}

	public static File existingDirectory(String filename) {
		return instance.existingDirectory(filename);
	}

	public static File existingDirectory(File file, boolean create) {
		return instance.existingDirectory(file, create);
	}

	public static File existingDirectory(String filename, boolean create) {
		return instance.existingDirectory(filename, create);
	}

	public static File existingFile(File file) {
		return instance.existingFile(file);
	}

	public static File existingFile(String filename) {
		return instance.existingFile(filename);
	}

	public static File existingFile(File file, boolean create) {
		return instance.existingFile(file, create);
	}

	public static File existingFile(String filename, boolean create) {
		return instance.existingFile(filename, create);
	}

	public static File getRoot(File file) {
		return instance.getRoot(file);
	}

	public static <R> R readToLines(File file, LineProcessor<R> processor) {
		return instance.readToLines(file, processor);
	}

	public static <R> R readToLines(File file, LineProcessor<R> processor, Charset charset) {
		return instance.readToLines(file, processor, charset);
	}

	public static void readToLines(File file, Collection<String> lines) {
		instance.readToLines(file, lines);
	}

	public static void readToLines(File file, Collection<String> lines, Charset charset) {
		instance.readToLines(file, lines, charset);
	}

	public static long getCreationTime(File file) {
		return instance.getCreationTime(file);
	}

	public static BasicFileAttributes getAttributes(File file) {
		return instance.getAttributes(file);
	}

	public static FileFilter acceptAllFilter() {
		return instance.acceptAllFilter();
	}

	public static FileFilter acceptNoneFilter() {
		return instance.acceptNoneFilter();
	}

	public static FileFilter acceptEndsWithFilter(String suffix, boolean ignoreCase) {
		return instance.acceptNameEndsWithFilter(suffix, ignoreCase);
	}

	public static void writeFromLines(File file, Iterable<? extends String> lines) {
		instance.writeFromLines(file, lines);
	}

	public static void writeFromLines(File file, Iterable<? extends String> lines, Charset charset) {
		instance.writeFromLines(file, lines, charset);
	}

	public static void writeFromLines(File file, Iterable<? extends String> lines, Charset charset, String lineSeparator) {
		instance.writeFromLines(file, lines, charset, lineSeparator);
	}

}
