package com.javabi.common.io.file;

import java.io.File;

import com.javabi.common.io.stream.IStreamInput;

public interface IFileInput extends IStreamInput {

	File getFile();

}
