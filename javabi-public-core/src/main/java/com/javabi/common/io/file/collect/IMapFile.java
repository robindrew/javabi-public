package com.javabi.common.io.file.collect;

import java.util.Map;

public interface IMapFile<M extends Map<String, String>> {

	void read();

	void write();

	Map<String, String> toMap();
	
}
