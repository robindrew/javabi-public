package com.javabi.common.io.serial;

import java.io.IOException;

/**
 * A Serial Input Stream.
 */
public interface SerialInputStream {

	/**
	 * Read and return a string.
	 * @return the string.
	 */
	String readString() throws IOException;

	/**
	 * Read bytes into the given array.
	 * @param bytes the byte array.
	 */
	void read(byte[] bytes) throws IOException;

	/**
	 * Read and return a double.
	 * @return the double.
	 */
	double readDouble() throws IOException;

	/**
	 * Read and return a float.
	 * @return the float.
	 */
	float readFloat() throws IOException;

	/**
	 * Read and return a long.
	 * @return the long.
	 */
	long readLong() throws IOException;

	/**
	 * Read and return an integer.
	 * @return the integer.
	 */
	int readInt() throws IOException;

	/**
	 * Read and return a character.
	 * @return the character.
	 */
	char readChar() throws IOException;

	/**
	 * Read and return a short.
	 * @return the short.
	 */
	short readShort() throws IOException;

	/**
	 * Read and return a byte.
	 * @return the byte.
	 */
	byte readByte() throws IOException;

	/**
	 * Read and return a boolean.
	 * @return the boolean.
	 */
	boolean readBoolean() throws IOException;

}
