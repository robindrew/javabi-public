package com.javabi.common.io.scp.jsch;

import static com.javabi.common.text.StringFormats.bytes;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.common.io.connection.INetworkUser;
import com.javabi.common.io.pipe.ByteBufferPipe;
import com.javabi.common.lang.Quietly;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.Session;

class JSchUploader extends JSchOperation {

	private static final Logger log = LoggerFactory.getLogger(JSchUploader.class);

	private final INetworkUser user;
	private final File localFile;
	private final String remoteFilename;

	public JSchUploader(INetworkUser user, File localFile, String remoteFilename) {
		this.user = user;
		this.localFile = localFile;
		this.remoteFilename = remoteFilename;
	}

	public void upload() {
		if (!localFile.isFile()) {
			throw new IllegalArgumentException("Not a regular file: " + localFile);
		}
		log.info("scp " + localFile + " " + user.getUsername() + "@" + user.getAddress().getHostname() + ":" + remoteFilename);
		try {

			// Session
			Session session = getSession(user);
			session.connect();
			try {
				upload(session);
			} finally {
				close(session);
			}
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	private void upload(Session session) throws Exception {
		ChannelExec channel = (ChannelExec) session.openChannel("exec");
		channel.setCommand("scp -t " + remoteFilename);

		InputStream input = new BufferedInputStream(channel.getInputStream());
		OutputStream output = new BufferedOutputStream(channel.getOutputStream());
		channel.connect();
		try {
			check(input);

			// Single file copy: Cxxxx
			long bytes = localFile.length();
			String command = "C0644 " + bytes + " " + localFile.getName() + "\n";
			write(output, command);

			check(input);

			// Upload file
			log.info("Uploading: " + localFile + " (" + bytes(bytes) + ")");
			NanoTimer timer = new NanoTimer();
			timer.start();
			FileInputStream fileInput = new FileInputStream(localFile);
			try {
				new ByteBufferPipe().pipe(fileInput, output);
			} finally {
				Quietly.close(fileInput);
			}
			output.flush();
			timer.stop();
			log.info("Uploaded: " + localFile + " in " + timer);

			Quietly.close(output);
		} finally {
			close(channel);
		}
	}

}
