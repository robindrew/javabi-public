package com.javabi.common.io.pipe;

import java.io.InputStream;
import java.io.OutputStream;

public interface IByteBufferPipe {

	int getBufferSize();

	long pipe(InputStream input, OutputStream output);

	long pipe(InputStream input, OutputStream output, long limit);

	long pipe(InputStream input, OutputStream output, IBufferPipeObserver observer);

	long pipe(InputStream input, OutputStream output, IBufferPipeObserver observer, long limit);

}
