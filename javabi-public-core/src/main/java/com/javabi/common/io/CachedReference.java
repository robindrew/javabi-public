package com.javabi.common.io;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

/**
 * A weakly/softly referenced object, cached in a temporary file.
 * <p>
 * This class is thread safe.
 * </p>
 */
public final class CachedReference<S extends Serializable> {

	/** The reference. */
	private Reference<S> reference = null;
	/** Indicates if the reference is weak. */
	private final boolean weak;

	/** The temporary file. */
	private File temporaryFile = null;
	/** The hash code. */
	private final int hashCode;

	/**
	 * Creates a new weakly referenced cached object.
	 * @param value the object value.
	 * @param weak true for a weak reference, false for soft.
	 */
	public CachedReference(S value, boolean weak) {
		this.weak = weak;
		if (value != null) {
			hashCode = value.hashCode();
			reference = newReference(value);
			writeToTemporaryFile(value);
		} else {
			hashCode = super.hashCode();
		}
	}

	/**
	 * Returns a new reference for the given value.
	 * @param value the value.
	 * @return the new reference.
	 */
	private final Reference<S> newReference(S value) {
		return weak ? new WeakReference<S>(value) : new SoftReference<S>(value);
	}

	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object instanceof CachedReference) {
			CachedReference<?> compare = (CachedReference<?>) object;
			if (hashCode() == compare.hashCode() && isNull() == compare.isNull()) {
				S value = get();
				return value == null || value.equals(compare.get());
			}
		}
		return false;
	}

	/**
	 * Returns true if the object value is null.
	 * @return true if the object value is null.
	 */
	public boolean isNull() {
		return reference == null;
	}

	/**
	 * Returns the cached object.
	 * @return the cached object.
	 */
	public S get() {
		if (isNull()) {
			return null;
		}
		synchronized (this) {
			S value = reference.get();
			if (value == null) {
				value = readFromTemporaryFile();
				reference = newReference(value);
			}
			return value;
		}
	}

	@Override
	public String toString() {
		return get().toString();
	}

	/**
	 * Read the value from the temporary file.
	 * @return the value.
	 */
	@SuppressWarnings("unchecked")
	private S readFromTemporaryFile() {
		try (ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(new FileInputStream(temporaryFile)))) {
			return (S) input.readObject();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Write the value to a temporary file.
	 * @param value the value.
	 */
	private void writeToTemporaryFile(S value) {
		try {
			temporaryFile = File.createTempFile("cachedreference", null);
			try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(temporaryFile))) {
				output.writeObject(value);
				output.flush();
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}