package com.javabi.common.io.file;

import java.io.File;
import java.io.FileFilter;
import java.nio.charset.Charset;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.List;

import com.google.common.io.LineProcessor;

public interface IFiles {

	void copy(File from, File to);

	void move(File from, File to);

	byte[] readToBytes(File file);

	byte[] readToMd5(File file);

	String readToMd5String(File file);

	void writeFromBytes(File file, byte[] contents);

	void writeFromString(File file, String contents);

	void writeFromString(File file, String contents, Charset charset);

	void writeFromLines(File file, Iterable<? extends String> lines);

	void writeFromLines(File file, Iterable<? extends String> lines, Charset charset);

	void writeFromLines(File file, Iterable<? extends String> lines, Charset charset, String lineSeparator);

	String readToString(File file);

	String readToString(File file, Charset charset);

	List<File> listContents(File directory);

	List<File> listContents(File directory, FileFilter filter);

	List<File> listFiles(File directory, boolean recursive);

	List<File> listFiles(File directory, boolean recursive, FileFilter filter);

	File existingDirectory(File file);

	File existingDirectory(String filename);

	File existingDirectory(File file, boolean create);

	File existingDirectory(String filename, boolean create);

	File existingFile(File file);

	File existingFile(String filename);

	File existingFile(File file, boolean create);

	File existingFile(String filename, boolean create);

	File getRoot(File file);

	void readToLines(File file, Collection<String> lines);

	void readToLines(File file, Collection<String> lines, Charset charset);

	<R> R readToLines(File file, LineProcessor<R> processor);

	<R> R readToLines(File file, LineProcessor<R> processor, Charset charset);

	long getCreationTime(File file);

	BasicFileAttributes getAttributes(File file);

	FileFilter acceptAllFilter();

	FileFilter acceptNoneFilter();

	FileFilter acceptNameEndsWithFilter(String suffix, boolean ignoreCase);

	FileFilter acceptNameMatchesFilter(String regex);

}
