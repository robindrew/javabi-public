package com.javabi.common.io.serial;

import java.io.IOException;
import java.nio.charset.Charset;

// ESCA-JAVA0076: magic numbers valid
/**
 * A Serial Byte Output Stream.
 */
public class SerialByteOutputStream extends SerialByteStream implements SerialOutputStream {

	/** The size. */
	private int size = 0;
	/** The bytes. */
	private byte[] array;

	/**
	 * Creates a new stream.
	 * @param initialCapacity the initial capacity.
	 * @param charset the character set.
	 */
	public SerialByteOutputStream(int initialCapacity, Charset charset) {
		super(charset);
		if (initialCapacity < 1) {
			throw new IllegalArgumentException("initialCapacity=" + initialCapacity);
		}
		this.array = new byte[initialCapacity];
	}

	/**
	 * Creates a new stream.
	 * @param initialCapacity the initial capacity.
	 */
	public SerialByteOutputStream(int initialCapacity) {
		if (initialCapacity < 1) {
			throw new IllegalArgumentException("initialCapacity=" + initialCapacity);
		}
		this.array = new byte[initialCapacity];
	}

	/**
	 * Returns the byte array.
	 * @return the byte array.
	 */
	public final byte[] toByteArray() {
		byte[] newArray = new byte[size];
		System.arraycopy(array, 0, newArray, 0, size);
		return newArray;
	}

	/**
	 * Dynamically grow the array if necessary.
	 * @param bytes the number of bytes to grow by.
	 */
	private final void grow(int bytes) {
		if (size + bytes > array.length) {
			int newLength = array.length * 2;
			if (newLength < size + bytes) {
				newLength = size + bytes;
			}
			byte[] newArray = new byte[newLength];
			System.arraycopy(array, 0, newArray, 0, size);
			array = newArray;
		}
	}

	/**
	 * Write the given byte array.
	 * @param b the byte array to write.
	 */
	@Override
	public final void write(byte[] b, int offset, int length) {
		grow(length);
		System.arraycopy(b, offset, this.array, this.size, length);
		this.size += length;
	}

	/**
	 * Write the given byte array.
	 * @param b the byte array to write.
	 */
	@Override
	public final void write(byte[] b) {
		write(b, 0, b.length);
	}

	@Override
	public final void writeByte(byte value) {
		grow(1);
		array[size++] = value;
	}

	@Override
	public final void writeBoolean(boolean value) {
		writeByte(value ? TRUE : FALSE);
	}

	@Override
	public final void writeChar(char value) {
		int intValue = value;
		writeInt(intValue);
	}

	/**
	 * Write the given float.
	 * @param value the float to write.
	 */
	@Override
	public final void writeFloat(float value) {
		int intValue = Float.floatToIntBits(value);
		writeInt(intValue);
	}

	/**
	 * Write the given double.
	 * @param value the double to write.
	 */
	@Override
	public final void writeDouble(double value) {
		long longValue = Double.doubleToLongBits(value);
		writeLong(longValue);
	}

	/**
	 * Write the given byte array.
	 * @param bytes the byte array to write.
	 */
	public final void writeByteArray(byte[] bytes) {
		int length = bytes.length;
		grow(4 + length);

		// Write Int
		writeInt(length);

		// Write Byte Array
		System.arraycopy(bytes, 0, this.array, size, length);
		size += length;
	}

	/**
	 * Write the given string.
	 * @param value the string to write.
	 */
	@Override
	public final void writeString(String value) {
		byte[] byteArray = value.getBytes(getCharset());
		writeByteArray(byteArray);
	}

	/**
	 * Write the given short.
	 * @param value the short to write.
	 */
	@Override
	public final void writeShort(short value) {
		if (value > TYPE_LONG && value <= Byte.MAX_VALUE) {
			writeByte((byte) value);
		} else {
			grow(3);
			writeFixedByte(TYPE_SHORT);
			writeFixedShort(value);
		}
	}

	/**
	 * Write the given integer.
	 * @param value the integer to write.
	 */
	@Override
	public final void writeInt(int value) {
		if (value > TYPE_LONG && value <= Byte.MAX_VALUE) {
			writeByte((byte) value);
		} else if (value >= Short.MIN_VALUE && value <= Short.MAX_VALUE) {
			grow(3);
			writeFixedByte(TYPE_SHORT);
			writeFixedShort((short) value);
		} else {
			grow(5);
			writeFixedByte(TYPE_INTEGER);
			writeFixedInt(value);
		}
	}

	/**
	 * Write the given long.
	 * @param value the long to write.
	 */
	@Override
	public final void writeLong(long value) {
		if (value > TYPE_LONG && value <= Byte.MAX_VALUE) {
			grow(1);
			writeFixedByte((byte) value);
		} else if (value >= Short.MIN_VALUE && value <= Short.MAX_VALUE) {
			grow(3);
			writeFixedByte(TYPE_SHORT);
			writeFixedShort((short) value);
		} else {
			if (value >= Integer.MIN_VALUE && value <= Integer.MAX_VALUE) {
				grow(5);
				writeFixedByte(TYPE_INTEGER);
				writeFixedInt((int) value);
			} else {
				grow(9);
				writeFixedByte(TYPE_LONG);
				writeFixedLong(value);
			}
		}
	}

	/**
	 * Write the given byte.
	 * @param value the byte to write.
	 */
	private final void writeFixedByte(byte value) {
		array[size++] = value;
	}

	/**
	 * Write the given short.
	 * @param value the short to write.
	 */
	private final void writeFixedShort(short value) {
		array[size++] = (byte) (value >> 8);
		array[size++] = (byte) (value >> 0);
	}

	/**
	 * Write the given integer.
	 * @param value the integer to write.
	 */
	private final void writeFixedInt(int value) {
		array[size++] = (byte) (value >> 24);
		array[size++] = (byte) (value >> 16);
		array[size++] = (byte) (value >> 8);
		array[size++] = (byte) (value >> 0);
	}

	/**
	 * Write the given long.
	 * @param value the long to write.
	 */
	private final void writeFixedLong(long value) {
		array[size++] = (byte) (value >> 56);
		array[size++] = (byte) (value >> 48);
		array[size++] = (byte) (value >> 40);
		array[size++] = (byte) (value >> 32);
		array[size++] = (byte) (value >> 24);
		array[size++] = (byte) (value >> 16);
		array[size++] = (byte) (value >> 8);
		array[size++] = (byte) (value >> 0);
	}

	@Override
	public void writeObject(Object value) throws IOException {
		if (value instanceof byte[]) {
			writeByteArray((byte[]) value);
		} else {
			writeString(value.toString());
		}
	}

}
