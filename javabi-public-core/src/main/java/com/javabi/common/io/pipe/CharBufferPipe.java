package com.javabi.common.io.pipe;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import com.google.common.base.Throwables;

public class CharBufferPipe implements ICharBufferPipe {

	private static final int DEFAULT_BUFFER_LENGTH = Short.MAX_VALUE;
	private static final long NO_LIMIT = 0;

	private final char[] buffer;

	public CharBufferPipe(int bufferLength) {
		if (bufferLength < 1) {
			throw new IllegalArgumentException("bufferLength=" + bufferLength);
		}
		buffer = new char[bufferLength];
	}

	public CharBufferPipe() {
		this(DEFAULT_BUFFER_LENGTH);
	}

	@Override
	public int getBufferSize() {
		return buffer.length;
	}

	@Override
	public final long pipe(Reader reader, Writer writer, long limit) {
		return pipe(reader, writer, null, limit);
	}

	@Override
	public final long pipe(Reader reader, Writer writer, IBufferPipeObserver observer, long limit) {
		if (limit < NO_LIMIT) {
			throw new IllegalArgumentException("limit=" + limit);
		}
		long read = 0;
		try {
			while (true) {
				int readLimit = buffer.length;
				if (limit > NO_LIMIT && (limit - read) < buffer.length) {
					readLimit = (int) (limit - read);
				}

				int length = reader.read(buffer, 0, readLimit);
				if (length == -1) {
					if (read > 0) {
						writer.flush();
					}
					break;
				}
				if (length == 0) {
					// Handle reads of zero bytes?
					continue;
				}
				read += length;
				writer.write(buffer, 0, length);

				// Limit reached ...
				if (limit > NO_LIMIT && limit == read) {
					break;
				}
			}
		} catch (IOException ioe) {
			throw Throwables.propagate(ioe);
		}
		return read;
	}

	@Override
	public final long pipe(Reader reader, Writer writer) {
		return pipe(reader, writer, null, NO_LIMIT);
	}

	@Override
	public final long pipe(Reader reader, Writer writer, IBufferPipeObserver observer) {
		return pipe(reader, writer, observer, NO_LIMIT);
	}
}
