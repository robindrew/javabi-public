package com.javabi.common.io.scp.jsch;

import java.io.File;

import com.javabi.common.io.connection.INetworkAddress;
import com.javabi.common.io.connection.INetworkUser;
import com.javabi.common.io.connection.NetworkAddress;
import com.javabi.common.io.connection.NetworkUser;
import com.javabi.common.io.scp.ISecureCopy;

public class JSchSecureCopy implements ISecureCopy {

	private final INetworkUser user;

	public JSchSecureCopy(INetworkUser user) {
		if (user == null) {
			throw new NullPointerException("user");
		}
		this.user = user;
	}

	@Override
	public INetworkUser getUser() {
		return user;
	}

	@Override
	public void upload(File localFile, String remoteFilename) {
		new JSchUploader(user, localFile, remoteFilename).upload();
	}

	@Override
	public void download(String remoteFilename, File localFile) {
		new JSchDownloader(user, remoteFilename, localFile).download();
	}

	public static void main(String[] args) {
		INetworkAddress address = new NetworkAddress("dev.turnengine.com", 22);
		INetworkUser user = new NetworkUser(address, "temp", "temppass12345");
		ISecureCopy copy = new JSchSecureCopy(user);
		copy.upload(new File("c:/temp.txt"), "temp.txt");
	}
}
