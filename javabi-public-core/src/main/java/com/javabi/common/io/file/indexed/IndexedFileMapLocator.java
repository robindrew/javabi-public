package com.javabi.common.io.file.indexed;

import static com.javabi.common.text.StringFormats.number;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.date.timer.ITimer;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.common.date.timer.counting.CountingTimer;
import com.javabi.common.date.timer.counting.ICountingTimer;
import com.javabi.common.io.file.Files;

public class IndexedFileMapLocator {

	private static final Logger log = LoggerFactory.getLogger(IndexedFileMapLocator.class);

	private final IFileHasher hasher;
	private FileFilter filter = Files.acceptAllFilter();

	public IndexedFileMapLocator(IFileHasher hasher) {
		if (hasher == null) {
			throw new NullPointerException("hasher");
		}
		this.hasher = hasher;
	}

	public void setFilter(FileFilter filter) {
		if (filter == null) {
			throw new NullPointerException("filter");
		}
		this.filter = filter;
	}

	public int locateFile(File directory, IndexedFileMap map, boolean forceHashing) {

		// Firstly find all files
		log.info("[Locating Files] in " + directory);
		ITimer timer = NanoTimer.startTimer();
		List<IndexedFile> files = getIndexedFiles(directory);
		timer.stop();
		log.info("[Located] " + number(files) + " files in " + timer);

		// Check for files that already exist
		if (!forceHashing) {
			Iterator<IndexedFile> iterator = files.iterator();
			while (iterator.hasNext()) {
				IndexedFile file = iterator.next();
				if (map.contains(file)) {
					iterator.remove();
				}
			}
		}

		// Generate hashes for missing files
		if (!files.isEmpty()) {
			log.info("[Hashing] " + number(files) + " files");
			ICountingTimer counter = CountingTimer.startTimer();

			for (IndexedFile file : files) {
				String hash = hasher.hash(file.getFile());
				IndexedFile hashed = file.setHash(hash);
				map.put(hashed);
				counter.increment();
				if (counter.expired()) {
					log.info("[Hashed] " + number(counter.getCount()) + " files in " + counter);
				}
			}

			timer.stop();
			log.info("[Hashed] " + number(files) + " files in " + counter);
		}

		return files.size();
	}

	private List<IndexedFile> getIndexedFiles(File directory) {
		List<File> files = Files.listFiles(directory, true, filter);
		List<IndexedFile> list = new ArrayList<>(files.size());
		for (File file : files) {
			list.add(new IndexedFile(file, ""));
		}
		return list;
	}

}
