package com.javabi.common.io.file.datetime;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public interface IDateTimeFileStorage {

	void setPrecision(TimeUnit precision);

	Map<LocalDateTime, File> getAllFiles();

	Map<LocalDateTime, File> getFiles(LocalDate date);

	File getFile(LocalDateTime date, boolean create);

}