package com.javabi.common.io.serial;

import java.nio.charset.Charset;

// ESCA-JAVA0076: magic numbers valid
/**
 * A Serial Byte Input Stream.
 */
public class SerialByteInputStream extends SerialByteStream implements SerialInputStream {

	/** The bytes. */
	private final byte[] bytes;
	/** The offset. */
	private int offset = 0;

	/**
	 * Creates a new stream.
	 * @param input the input bytes.
	 * @param charset the character set.
	 */
	public SerialByteInputStream(byte[] input, Charset charset) {
		super(charset);
		if (input == null) {
			throw new NullPointerException();
		}
		// ESCA-JAVA0256: array wrapper
		this.bytes = input;
	}

	/**
	 * Creates a new stream.
	 * @param input the input bytes.
	 */
	public SerialByteInputStream(byte[] input) {
		if (input == null) {
			throw new NullPointerException();
		}
		// ESCA-JAVA0256: array wrapper
		this.bytes = input;
	}

	/**
	 * Read a byte array.
	 * @param output the byte array to read into.
	 */
	@Override
	public final void read(byte[] output) {
		System.arraycopy(bytes, offset, output, 0, output.length);
		offset += output.length;
	}

	/**
	 * Read a boolean.
	 * @return the boolean read.
	 */
	@Override
	public final boolean readBoolean() {
		return bytes[offset++] != FALSE;
	}

	/**
	 * Read a byte.
	 * @return the byte read.
	 */
	@Override
	public final byte readByte() {
		return bytes[offset++];
	}

	/**
	 * Read a char.
	 * @return the char read.
	 */
	@Override
	public final char readChar() {
		return (char) readInt();
	}

	/**
	 * Read a float.
	 * @return the float read.
	 */
	@Override
	public final float readFloat() {
		return Float.intBitsToFloat(readInt());
	}

	/**
	 * Read a double.
	 * @return the double read.
	 */
	@Override
	public final double readDouble() {
		return Double.longBitsToDouble(readLong());
	}

	/**
	 * Read an int.
	 * @return the int read.
	 */
	@Override
	public final int readInt() {
		byte type = readByte();
		switch (type) {
			case TYPE_INTEGER:
				return readFixedInt();
			case TYPE_SHORT:
				return readFixedShort();
			default:
				return type;
		}
	}

	/**
	 * Read an long.
	 * @return the long read.
	 */
	@Override
	public final long readLong() {
		byte type = readByte();
		switch (type) {
			case TYPE_LONG:
				return readFixedLong();
			case TYPE_INTEGER:
				return readFixedInt();
			case TYPE_SHORT:
				return readFixedShort();
			default:
				return type;
		}
	}

	/**
	 * Read an short.
	 * @return the short read.
	 */
	@Override
	public final short readShort() {
		byte type = readByte();
		switch (type) {
			case TYPE_SHORT:
				return readFixedShort();
			default:
				return type;
		}
	}

	/**
	 * Read a string.
	 * @return the string.
	 */
	@Override
	public final String readString() {
		int length = readInt();
		byte[] text = new byte[length];
		return new String(text, getCharset());
	}

	/**
	 * Read a fixed short.
	 * @return the short read.
	 */
	private final short readFixedShort() {
		int b0 = bytes[offset + 0];
		int b1 = bytes[offset + 1];
		offset += 2;
		return (short) (((b0 & 0xff) << 8) | ((b1 & 0xff) << 0));
	}

	/**
	 * Read a fixed integer.
	 * @return the integer read.
	 */
	private final int readFixedInt() {
		int b0 = bytes[offset + 0];
		int b1 = bytes[offset + 1];
		int b2 = bytes[offset + 2];
		int b3 = bytes[offset + 3];
		offset += 4;
		return ((b0 & 0xff) << 24) | ((b1 & 0xff) << 16) | ((b2 & 0xff) << 8) | ((b3 & 0xff) << 0);
	}

	/**
	 * Read a fixed long.
	 * @return the long read.
	 */
	private final long readFixedLong() {
		long b0 = bytes[offset + 0];
		long b1 = bytes[offset + 1];
		long b2 = bytes[offset + 2];
		long b3 = bytes[offset + 3];
		long b4 = bytes[offset + 4];
		long b5 = bytes[offset + 5];
		long b6 = bytes[offset + 6];
		long b7 = bytes[offset + 7];
		offset += 8;
		return ((b0 & 0xff) << 56) | ((b1 & 0xff) << 48) | ((b2 & 0xff) << 40) | ((b3 & 0xff) << 32) | ((b4 & 0xff) << 24) | ((b5 & 0xff) << 16) | ((b6 & 0xff) << 8) | ((b7 & 0xff) << 0);
	}
}
