package com.javabi.common.io;

import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

import com.google.common.io.ByteSink;

public class GzipByteSink extends ByteSink {

	private final ByteSink sink;

	public GzipByteSink(ByteSink sink) {
		if (sink == null) {
			throw new NullPointerException("sink");
		}
		this.sink = sink;
	}

	@Override
	public OutputStream openStream() throws IOException {
		return new GZIPOutputStream(sink.openStream());
	}

}
