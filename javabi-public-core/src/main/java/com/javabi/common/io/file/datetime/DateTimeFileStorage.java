package com.javabi.common.io.file.datetime;

import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import com.google.common.collect.Sets;
import com.javabi.common.io.file.Files;
import com.javabi.common.lang.Empty;

public class DateTimeFileStorage implements IDateTimeFileStorage {

	private static final Set<TimeUnit> PRECISIONS = Sets.newHashSet(HOURS, MINUTES, SECONDS, NANOSECONDS);

	private final File rootDirectory;
	private final String fileExtension;
	private TimeUnit precision = TimeUnit.MINUTES;

	public DateTimeFileStorage(File rootDirectory, String fileExtension) {
		if (fileExtension.isEmpty()) {
			throw new IllegalArgumentException("fileExtension is empty");
		}
		this.rootDirectory = Files.existingDirectory(rootDirectory);
		this.fileExtension = fileExtension;
	}

	@Override
	public void setPrecision(TimeUnit precision) {
		if (!PRECISIONS.contains(precision)) {
			throw new IllegalArgumentException("precision: " + precision + ", must be one of " + PRECISIONS);
		}
		this.precision = precision;
	}

	@Override
	public Map<LocalDateTime, File> getAllFiles() {
		Map<LocalDateTime, File> map = new TreeMap<>();
		for (File dir : Files.listContents(rootDirectory)) {
			LocalDate date = getLocalDate(dir);
			if (date == null) {
				continue;
			}
			getFiles(map, dir, date);
		}
		return map;
	}

	private void getFiles(Map<LocalDateTime, File> map, File dateDir, LocalDate date) {
		for (File timeFile : Files.listContents(dateDir)) {
			LocalTime time = getLocalTime(timeFile);
			if (time != null) {
				map.put(LocalDateTime.of(date, time), timeFile);
			}
		}
	}

	private LocalDate getLocalDate(File directory) {
		if (!directory.exists() || !directory.isDirectory()) {
			return null;
		}
		String name = directory.getName();
		return LocalDate.parse(name);
	}

	@Override
	public Map<LocalDateTime, File> getFiles(LocalDate date) {
		File dir = getDirectory(date);
		if (!dir.exists()) {
			return Empty.map();
		}

		Map<LocalDateTime, File> map = new TreeMap<>();
		getFiles(map, dir, date);
		return map;
	}

	private File getDirectory(LocalDate date) {
		String name = date.toString();
		return new File(rootDirectory, name);
	}

	@Override
	public File getFile(LocalDateTime date, boolean create) {
		File directory = getDirectory(date.toLocalDate());
		if (!directory.exists() && create) {
			directory.mkdir();
		}
		String filename = getFilename(date.toLocalTime());
		return new File(directory, filename);
	}

	private String getFilename(LocalTime time) {
		switch (precision) {
			case HOURS:
				time = time.withMinute(0);
			case MINUTES:
				time = time.withSecond(0);
			case SECONDS:
				time = time.withNano(0);
			default:
				break;
		}

		String name = time.toString();
		name = name.replace(':', '-');
		name = name + fileExtension;
		return name;
	}

	private LocalTime getLocalTime(File file) {
		String name = file.getName();
		if (!name.endsWith(fileExtension)) {
			return null;
		}
		name = name.substring(0, name.length() - fileExtension.length());
		name = name.replace('-', ':');
		return LocalTime.parse(name);
	}

}
