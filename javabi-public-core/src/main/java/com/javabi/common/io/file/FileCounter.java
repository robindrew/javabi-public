package com.javabi.common.io.file;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

import com.javabi.common.lang.Variables;

public class FileCounter {

	private List<FileFilter> filterList = new ArrayList<FileFilter>();

	public void addFilter(FileFilter filter) {
		if (filter == null) {
			throw new NullPointerException("filter");
		}
		this.filterList.add(filter);
	}

	public boolean countFile(File file) {
		for (FileFilter filter : filterList) {
			if (!filter.accept(file)) {
				return false;
			}
		}
		return true;
	}

	public int countFiles(String directory) {
		return countFiles(new File(directory));
	}

	public int countFiles(File directory) {
		Variables.directory("directory", directory);

		List<File> files = new DirectoryLister().listFiles(directory);

		int count = 0;
		for (File file : files) {
			if (file.isDirectory()) {
				count += countFiles(file);
			} else {
				if (countFile(file)) {
					count++;
				}
			}
		}
		return count;
	}

}
