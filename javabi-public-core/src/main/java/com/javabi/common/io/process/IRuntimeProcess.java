package com.javabi.common.io.process;

import java.io.File;

import com.javabi.common.io.stream.IStreamInput;

public interface IRuntimeProcess extends IStreamInput {

	String getCommand();

	String[] getEnvironment();

	File getDirectory();

	boolean isWaitFor();

	void setWaitFor(boolean waitFor);

	void setCommand(String command);

	void setEnvironment(String[] environment);

	void setDirectory(File directory);

}