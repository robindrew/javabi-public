package com.javabi.common.io.file.indexed;

import java.io.File;

import com.javabi.common.crypto.HexDecoder;
import com.javabi.common.crypto.IDecoder;
import com.javabi.common.crypto.IEncoder;
import com.javabi.common.crypto.Md5Encoder;

public class FileHasher implements IFileHasher {

	private final IEncoder encoder;
	private final IDecoder decoder = new HexDecoder();

	public FileHasher(IEncoder encoder) {
		if (encoder == null) {
			throw new NullPointerException("encoder");
		}
		this.encoder = encoder;
	}

	public FileHasher() {
		this(new Md5Encoder());
	}

	@Override
	public String hash(File file) {
		byte[] bytes = encoder.encodeToBytes(file);
		return decoder.decodeToString(bytes);
	}

}
