package com.javabi.common.io.connection;

import java.net.SocketAddress;

public interface INetworkAddress {

	String getHostname();

	String getAddress();

	int getPort();

	int getCode();

	SocketAddress toSocketAddress();

}
