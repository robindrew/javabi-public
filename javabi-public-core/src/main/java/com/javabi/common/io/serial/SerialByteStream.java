package com.javabi.common.io.serial;

import java.nio.charset.Charset;

/**
 * A Serial Byte Stream.
 */
public class SerialByteStream {

	/** True. */
	public static final byte TRUE = 1;
	/** False. */
	public static final byte FALSE = 0;

	/** The short type. */
	public static final byte TYPE_SHORT = (Byte.MIN_VALUE + 0);
	/** The integer type. */
	public static final byte TYPE_INTEGER = (Byte.MIN_VALUE + 1);
	/** The long type. */
	public static final byte TYPE_LONG = (Byte.MIN_VALUE + 2);

	/** The default character set (UTF-8). */
	public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

	/** The character set. */
	private final Charset charset;

	/**
	 * Creates a new stream.
	 * @param charset the charset.
	 */
	protected SerialByteStream(Charset charset) {
		if (charset == null) {
			throw new NullPointerException();
		}
		this.charset = charset;
	}

	/**
	 * Creates a new stream.
	 */
	protected SerialByteStream() {
		this.charset = DEFAULT_CHARSET;
	}

	/**
	 * Returns the character set.
	 * @return the character set.
	 */
	public final Charset getCharset() {
		return charset;
	}

}
