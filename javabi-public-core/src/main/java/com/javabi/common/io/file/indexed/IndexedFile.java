package com.javabi.common.io.file.indexed;

import java.io.File;

import com.javabi.common.io.file.Files;
import com.javabi.common.lang.Java;

public class IndexedFile implements Comparable<IndexedFile> {

	private final String hash;
	private final long timeCreated;
	private final long timeModified;
	private final long length;
	private final File file;

	public IndexedFile(File file) {
		this(file, "");
	}

	public IndexedFile(File file, String hash) {
		if (hash == null) {
			throw new NullPointerException("hash");
		}
		if (file == null) {
			throw new NullPointerException("file");
		}
		this.file = file;
		this.length = file.length();
		this.timeCreated = Files.getCreationTime(file);
		this.timeModified = file.lastModified();
		this.hash = hash;
	}

	public IndexedFile(File file, long length, long timeCreated, long timeModified, String hash) {
		this.file = file;
		this.length = length;
		this.timeCreated = timeCreated;
		this.timeModified = timeModified;
		this.hash = hash;
	}

	public File getDirectory() {
		return getFile().getParentFile();
	}

	public String getHash() {
		return hash;
	}

	public long getTimeCreated() {
		return timeCreated;
	}

	public long getTimeModified() {
		return timeModified;
	}

	public long getLength() {
		return length;
	}

	public File getFile() {
		return file;
	}

	public boolean hasHash() {
		return !hash.isEmpty();
	}

	@Override
	public int hashCode() {
		return file.hashCode();
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object instanceof IndexedFile) {
			IndexedFile indexed = (IndexedFile) object;
			return getFile().equals(indexed.getFile());
		}
		return false;
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

	public boolean isIdenticalTo(IndexedFile indexed) {
		if (getLength() != indexed.getLength()) {
			return false;
		}
		if (getTimeCreated() != indexed.getTimeCreated()) {
			return false;
		}
		if (getTimeModified() != indexed.getTimeModified()) {
			return false;
		}
		if (!getFile().equals(indexed.getFile())) {
			return false;
		}
		if (hasHash() && indexed.hasHash()) {
			if (!getHash().equals(indexed.getHash())) {
				return false;
			}
		}
		return true;
	}

	public boolean hasSameContentsAs(IndexedFile indexed) {
		if (!hasHash() || !indexed.hasHash()) {
			throw new IllegalArgumentException("hash missing for contents matching");
		}
		if (getLength() != indexed.getLength()) {
			return false;
		}
		if (!getHash().equals(indexed.getHash())) {
			return false;
		}
		return true;
	}

	public IndexedFile setHash(String hash) {
		return new IndexedFile(file, length, timeCreated, timeModified, hash);
	}

	@Override
	public int compareTo(IndexedFile file) {
		return getFile().compareTo(file.getFile());
	}

}
