package com.javabi.common.io.pipe;

import java.io.Reader;
import java.io.Writer;

public interface ICharBufferPipe {

	int getBufferSize();

	long pipe(Reader reader, Writer writer);

	long pipe(Reader reader, Writer writer, long limit);

	long pipe(Reader reader, Writer writer, IBufferPipeObserver observer);

	long pipe(Reader reader, Writer writer, IBufferPipeObserver observer, long limit);

}
