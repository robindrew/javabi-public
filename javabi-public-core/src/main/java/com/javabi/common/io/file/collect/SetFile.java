package com.javabi.common.io.file.collect;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Set;

import com.google.common.base.Charsets;
import com.google.common.collect.ForwardingSet;
import com.google.common.collect.ImmutableSet;
import com.javabi.common.io.file.Files;
import com.javabi.common.lang.Java;

public class SetFile extends ForwardingSet<String>implements ICollectionFile<Set<String>> {

	private final File file;
	private final Set<String> set;
	private Charset charset = Charsets.UTF_8;
	private String lineSeparator = Java.getLineSeparator();

	public SetFile(File file, Set<String> set) {
		if (file == null) {
			throw new NullPointerException("file");
		}
		if (set == null) {
			throw new NullPointerException("set");
		}
		this.file = file;
		this.set = set;
	}

	@Override
	protected Set<String> delegate() {
		return set;
	}

	protected void validateLine(String line) {
		if (line.contains(lineSeparator)) {
			throw new IllegalArgumentException("invalid line: '" + line + "'");
		}
	}

	@Override
	public void read() {
		set.clear();
		if (file.exists() && file.length() > 0) {
			Files.readToLines(file, set, charset);
		}
	}

	@Override
	public void write() {
		Files.writeFromLines(file, set, charset, lineSeparator);
	}

	@Override
	public boolean add(String line) {
		if (line == null) {
			throw new NullPointerException("line");
		}
		validateLine(line);
		return super.add(line);
	}

	@Override
	public boolean addAll(Collection<? extends String> lines) {
		if (lines == null) {
			throw new NullPointerException("line");
		}
		boolean added = false;
		for (String line : lines) {
			added = added || add(line);
		}
		return added;
	}

	public Set<String> toSet() {
		return ImmutableSet.copyOf(set);
	}

	@Override
	public Set<String> toCollection() {
		return toSet();
	}

}
