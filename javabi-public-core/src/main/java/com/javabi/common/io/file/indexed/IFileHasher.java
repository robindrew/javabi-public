package com.javabi.common.io.file.indexed;

import java.io.File;

public interface IFileHasher {

	String hash(File file);

}
