package com.javabi.common.io.file.indexed;

import static com.javabi.common.text.StringFormats.number;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.date.timer.ITimer;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.common.io.RuntimeIOException;
import com.javabi.common.io.data.DataReader;
import com.javabi.common.io.data.DataWriter;
import com.javabi.common.io.data.IDataReader;
import com.javabi.common.io.data.IDataSerializer;
import com.javabi.common.io.data.IDataWriter;

public class IndexedFileMapSerializer implements IDataSerializer<IndexedFileMap> {

	private static final Logger log = LoggerFactory.getLogger(IndexedFileMapSerializer.class);

	private final String root;

	public IndexedFileMapSerializer(String root) {
		if (root.isEmpty()) {
			throw new IllegalArgumentException("root is empty");
		}
		this.root = root;
	}

	@Override
	public IndexedFileMap readObject(IDataReader reader) throws IOException {
		log.info("[Read Started] ...");
		ITimer timer = NanoTimer.startTimer();

		IndexedFileMap map = new IndexedFileMap();

		int count = 0;
		int directories = reader.readPositiveInt();
		for (int i = 0; i < directories; i++) {
			String path = reader.readString(false);

			File directory = new File(root, path);

			IndexedFileSerializer serializer = new IndexedFileSerializer(directory);

			int files = reader.readPositiveInt();
			for (int j = 0; j < files; j++) {
				IndexedFile file = serializer.readObject(reader);
				map.put(directory, file);
				count++;
			}
		}

		timer.stop();
		log.info("[Read Finished] " + number(count) + " files in " + timer);
		return map;
	}

	@Override
	public void writeObject(IDataWriter writer, IndexedFileMap map) throws IOException {
		log.info("[Write Started] ...");
		ITimer timer = NanoTimer.startTimer();

		int count = 0;
		Set<File> directories = map.getDirectories();
		writer.writePositiveInt(directories.size());
		for (File directory : directories) {
			IndexedFileSerializer serializer = new IndexedFileSerializer(directory);

			String path = directory.getAbsolutePath();
			if (!path.startsWith(root)) {
				throw new IllegalStateException();
			}
			path = path.substring(root.length());

			writer.writeString(path, false);
			List<IndexedFile> files = map.getFiles(directory);
			writer.writePositiveInt(files.size());
			for (IndexedFile file : files) {
				serializer.writeObject(writer, file);
				count++;
			}
		}

		timer.stop();
		log.info("[Write Finished] " + number(count) + " files in " + timer);
	}

	public void writeToFile(File file, IndexedFileMap map) {
		log.info("[Writing File] " + file);
		try (IDataWriter writer = new DataWriter(new BufferedOutputStream(new FileOutputStream(file)))) {
			writeObject(writer, map);
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}

	public IndexedFileMap readFromFile(File file) {
		log.info("[Reading File] " + file);
		if (file.length() == 0) {
			return new IndexedFileMap();
		}
		try (IDataReader reader = new DataReader(new BufferedInputStream(new FileInputStream(file)))) {
			return readObject(reader);
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}

}
