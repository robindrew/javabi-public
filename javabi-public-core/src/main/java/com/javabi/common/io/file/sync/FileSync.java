package com.javabi.common.io.file.sync;

import static com.javabi.common.lang.Variables.existsDirectory;
import static com.javabi.common.lang.Variables.notEmpty;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.io.file.DirectoryLister;

public class FileSync implements IFileSync {

	/** The logger. */
	private static final Logger log = LoggerFactory.getLogger(FileSync.class);

	private final File fromDirectory;
	private final File toDirectory;
	private Set<String> ignoreSet = new HashSet<String>();
	private int modified = 0;
	private int created = 0;
	private int deleted = 0;
	private boolean deleteFiles = false;
	private boolean binaryMatch = false;
	private boolean lastModifiedMatch = true;

	public FileSync(String fromDirectory, String toDirectory) {
		this.fromDirectory = existsDirectory("fromDirectory", fromDirectory);
		this.toDirectory = existsDirectory("toDirectory", toDirectory);
	}

	public FileSync(File fromDirectory, File toDirectory) {
		this.fromDirectory = existsDirectory("fromDirectory", fromDirectory);
		this.toDirectory = existsDirectory("toDirectory", toDirectory);
	}

	@Override
	public void ignoreDirectory(String directory) {
		notEmpty("directory", directory);
		ignoreSet.add(directory);
	}

	@Override
	public void syncFiles() {
		sync(fromDirectory, toDirectory);
	}

	@Override
	public int getModified() {
		return modified;
	}

	@Override
	public int getCreated() {
		return created;
	}

	@Override
	public int getDeleted() {
		return deleted;
	}

	private void sync(File fromDirectory, File toDirectory) {
		log.info("Sync: " + fromDirectory + " -> " + toDirectory);
		Set<File> fromFileSet = getFileSet(fromDirectory);
		Map<String, File> toFileMap = getFileMap(toDirectory);

		Iterator<File> fromIterator = fromFileSet.iterator();
		while (fromIterator.hasNext()) {
			File fromFile = fromIterator.next();
			File toFile = toFileMap.remove(fromFile.getName());

			// File does not exist?
			if (toFile == null) {
				toFile = new File(toDirectory, fromFile.getName());

				// New directory?
				if (fromFile.isDirectory()) {
					toFile.mkdirs();
					sync(fromFile, toFile);
					continue;
				}

				// Move new file
				log.info("Created: " + toFile);
				fromFile.renameTo(toFile);
				created++;
				continue;
			}

			// Both types must match ...
			if (fromFile.isDirectory() != toFile.isDirectory()) {
				throw new IllegalStateException(fromFile + " cannot be mapped to " + toFile);
			}

			// Existing directory?
			if (fromFile.isDirectory()) {
				sync(fromFile, toFile);
				continue;
			}

			// File already exists, replace?
			if (!match(fromFile, toFile)) {
				log.info("Modified: " + toFile);
				delete(toFile);
				fromFile.renameTo(toFile);
				modified++;
				continue;
			}
		}

		for (File toFile : toFileMap.values()) {
			if (isDeleteFiles()) {
				log.warn("Deleted File: " + toFile);
				delete(toFile);
				deleted++;
			} else {
				log.warn("Skipping File: " + toFile);
			}
		}
	}

	private void delete(File file) {
		if (file.isDirectory()) {
			List<File> files = new DirectoryLister().listFiles(file);
			for (File child : files) {
				delete(child);
			}
		}
		if (!file.delete()) {
			throw new IllegalStateException("Unable to delete file: " + file);
		}
	}

	protected boolean match(File fromFile, File toFile) {

		// Do a length comparison (always)
		if (fromFile.length() != toFile.length()) {
			return false;
		}

		// Do a last modified comparison?
		if (isLastModifiedMatch()) {
			if (fromFile.lastModified() != toFile.lastModified()) {
				return false;
			}
		}

		// Do a binary comparison?
		if (isBinaryMatch()) {

			// Perform binary comparison ...
			return binaryMatch(fromFile, toFile);
		}

		// Match
		return true;
	}

	private boolean binaryMatch(File fromFile, File toFile) {
		try (InputStream fromInput = new BufferedInputStream(new FileInputStream(fromFile))) {
			try (InputStream toInput = new BufferedInputStream(new FileInputStream(toFile))) {
				while (true) {
					int from = fromInput.read();
					int to = toInput.read();
					if (from != to) {
						return false;
					}
					if (from == -1) {
						return to == -1;
					}
					if (to == -1) {
						return from == -1;
					}
				}
			}
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	private Set<File> getFileSet(File directory) {
		Set<File> set = new LinkedHashSet<File>();
		List<File> files = new DirectoryLister().listFiles(directory);
		for (File file : files) {
			if (ignoreSet.contains(file.getName())) {
				continue;
			}
			set.add(file);
		}
		return set;
	}

	private Map<String, File> getFileMap(File directory) {
		Map<String, File> map = new LinkedHashMap<String, File>();
		List<File> files = new DirectoryLister().listFiles(directory);
		for (File file : files) {
			if (ignoreSet.contains(file.getName())) {
				continue;
			}
			map.put(file.getName(), file);
		}
		return map;
	}

	@Override
	public void setBinaryMatch(boolean match) {
		this.binaryMatch = match;
	}

	public boolean isBinaryMatch() {
		return binaryMatch;
	}

	@Override
	public void setLastModifiedMatch(boolean match) {
		this.lastModifiedMatch = match;
	}

	public boolean isLastModifiedMatch() {
		return lastModifiedMatch;
	}

	@Override
	public void setDeleteFiles(boolean delete) {
		this.deleteFiles = delete;
	}

	public boolean isDeleteFiles() {
		return deleteFiles;
	}

}
