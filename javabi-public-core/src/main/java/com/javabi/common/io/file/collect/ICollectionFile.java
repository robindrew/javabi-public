package com.javabi.common.io.file.collect;

import java.util.Collection;

public interface ICollectionFile<C extends Collection<String>> extends Collection<String> {

	void read();

	void write();

	C toCollection();

}
