package com.javabi.common.io.resource;

import java.net.URL;
import java.nio.charset.Charset;

public class Resources {

	private static volatile IResources instance = new ResourcesImpl();

	public static IResources getResources() {
		return instance;
	}

	public static void setResources(IResources resources) {
		if (resources == null) {
			throw new NullPointerException("resources");
		}
		instance = resources;
	}

	private Resources() {
		// Inaccessible constructor
	}

	public static String readToString(URL url) {
		return instance.readToString(url);
	}

	public static String readToString(URL url, Charset charset) {
		return instance.readToString(url, charset);
	}

	public static boolean exists(String resourceName) {
		return instance.exists(resourceName);
	}

	public static URL getResource(String resourceName, boolean optional) {
		return instance.getResource(resourceName, optional);
	}

	public static String readToString(String resourceName) {
		return instance.readToString(resourceName);
	}

	public static String readToString(String resourceName, Charset charset) {
		return instance.readToString(resourceName, charset);
	}

}
