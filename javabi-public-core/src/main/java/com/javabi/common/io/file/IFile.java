package com.javabi.common.io.file;

import java.io.File;
import java.util.List;

public interface IFile {

	File getFile();

	boolean exists();

	boolean isFile();

	boolean isDirectory();

	void checkExists();

	void checkFile(boolean exists);

	void checkDirectory(boolean exists);

	List<IFile> getFiles();

}
