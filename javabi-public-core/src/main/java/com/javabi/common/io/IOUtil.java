package com.javabi.common.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringWriter;

import com.google.common.base.Throwables;
import com.javabi.common.io.pipe.ByteBufferPipe;
import com.javabi.common.io.pipe.CharBufferPipe;

/**
 * A collection of IO utility methods.
 */
public final class IOUtil {

	private static final int APPEND_BUFFER_LENGTH = 1024;

	/**
	 * Inaccessible constructor.
	 */
	private IOUtil() {
	}

	/**
	 * Read from the input stream to fill the given buffer.
	 * @param input the input stream.
	 * @param buffer the buffer to fill.
	 * @param allowPartial true to allow partial fill of buffer (N.B. new buffer might be allocated).
	 * @return the filled buffer.
	 */
	public static final byte[] readFully(InputStream input, byte[] buffer, boolean allowPartial) {
		int length = buffer.length;
		int offset = 0;
		try {
			while (offset < length) {
				int read = input.read(buffer, offset, length - offset);
				if (read == -1) {

					// Special case, partial buffering
					if (allowPartial) {
						byte[] partialBuffer = new byte[offset];
						System.arraycopy(buffer, 0, partialBuffer, 0, offset);
						return partialBuffer;
					}

					throw new IOException("unexpected end of stream");
				}
				if (read == 0) {
					throw new IOException("unexpected empty stream");
				}
				offset += read;
			}
		} catch (IOException ioe) {
			throw Throwables.propagate(ioe);
		}
		return buffer;
	}

	/**
	 * Read from the input stream until it closes.
	 * @param input the input stream.
	 * @return the bytes read.
	 */
	public static final byte[] readFully(InputStream input) {
		return readFully(input, 0);
	}

	/**
	 * Read from the input stream until it closes.
	 * @param input the input stream.
	 * @param limit the limit on the number of bytes read.
	 * @return the bytes read.
	 */
	public static final byte[] readFully(InputStream input, int limit) {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		new ByteBufferPipe().pipe(input, output, limit);
		return output.toByteArray();
	}

	/**
	 * Read from the reader until it closes.
	 * @param reader the reader.
	 * @return the bytes read.
	 */
	public static final String readFully(Reader reader) {
		StringWriter writer = new StringWriter();
		new CharBufferPipe().pipe(reader, writer);
		return writer.toString();
	}

	/**
	 * Append the given reader to the builder.
	 * @param reader the reader.
	 * @param builder the builder.
	 * @param bufferLength the buffer length.
	 */
	public static final void append(Reader reader, StringBuilder builder, int bufferLength) {
		char[] buffer = new char[bufferLength];
		try {
			while (true) {
				int length = reader.read(buffer);
				if (length < 1) {
					break;
				}
				builder.append(buffer, 0, length);
			}
		} catch (IOException ioe) {
			throw Throwables.propagate(ioe);
		}
	}

	/**
	 * Append the given reader to the builder.
	 * @param builder the builder.
	 * @param reader the reader.
	 */
	public static final void append(Reader reader, StringBuilder builder) {
		append(reader, builder, APPEND_BUFFER_LENGTH);
	}

}
