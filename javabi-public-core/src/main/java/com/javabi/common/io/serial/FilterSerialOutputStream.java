package com.javabi.common.io.serial;

import java.io.IOException;


/**
 * A Filter Serial Output Stream.
 */
public abstract class FilterSerialOutputStream implements SerialOutputStream {

	/** The underlying stream. */
	protected final SerialOutputStream stream;

	/**
	 * Creates a new filter stream.
	 * @param stream the underlying stream.
	 */
	protected FilterSerialOutputStream(SerialOutputStream stream) {
		if (stream == null) {
			throw new NullPointerException();
		}
		this.stream = stream;
	}

	@Override
	public void write(byte[] value, int offset, int length) throws IOException {
		stream.write(value, offset, length);
	}

	@Override
	public void write(byte[] value) throws IOException {
		stream.write(value);
	}

	@Override
	public void writeBoolean(boolean value) throws IOException {
		stream.writeBoolean(value);
	}

	@Override
	public void writeByte(byte value) throws IOException {
		stream.writeByte(value);
	}

	@Override
	public void writeChar(char value) throws IOException {
		stream.writeChar(value);
	}

	@Override
	public void writeDouble(double value) throws IOException {
		stream.writeDouble(value);
	}

	@Override
	public void writeFloat(float value) throws IOException {
		stream.writeFloat(value);
	}

	@Override
	public void writeInt(int value) throws IOException {
		stream.writeInt(value);
	}

	@Override
	public void writeLong(long value) throws IOException {
		stream.writeLong(value);
	}

	@Override
	public void writeObject(Object value) throws IOException {
		stream.writeObject(value);
	}

	@Override
	public void writeShort(short value) throws IOException {
		stream.writeShort(value);
	}

	@Override
	public void writeString(String value) throws IOException {
		stream.writeString(value);
	}

}
