package com.javabi.common.io.file;

import static com.javabi.common.lang.Variables.notNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.google.common.base.Throwables;
import com.google.common.io.ByteSink;
import com.google.common.io.CharSink;
import com.google.common.io.Files;
import com.javabi.common.io.stream.StreamOutput;

public class FileOutput extends StreamOutput implements IFileOutput {

	private final File file;
	private boolean append = false;

	public FileOutput(File file) {
		this.file = notNull("file", file);
	}

	@Override
	public boolean isAppend() {
		return append;
	}

	@Override
	public void setAppend(boolean append) {
		this.append = append;
	}

	@Override
	public OutputStream newOutputStream() {
		try {
			// Take control of output stream, so that file appending can be handled transparently
			return new FileOutputStream(file, isAppend());
		} catch (Throwable t) {
			throw Throwables.propagate(t);
		}
	}

	@Override
	public CharSink asCharSink() {
		return Files.asCharSink(file, getCharset());
	}

	@Override
	public ByteSink asByteSink() {
		return Files.asByteSink(file);
	}

}
