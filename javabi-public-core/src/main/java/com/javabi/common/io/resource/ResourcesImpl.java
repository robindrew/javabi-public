package com.javabi.common.io.resource;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.javabi.common.io.RuntimeIOException;

public class ResourcesImpl implements IResources {

	private static final Charset DEFAULT_CHARSET = Charsets.UTF_8;

	@Override
	public String readToString(URL url) {
		return readToString(url, DEFAULT_CHARSET);
	}

	@Override
	public String readToString(URL url, Charset charset) {
		if (url == null) {
			throw new NullPointerException("url");
		}
		if (charset == null) {
			throw new NullPointerException("charset");
		}
		try {
			return com.google.common.io.Resources.toString(url, charset);
		} catch (IOException ioe) {
			throw new RuntimeIOException(ioe);
		}
	}

	@Override
	public String readToString(String resourceName) {
		URL url = getResource(resourceName, false);
		return readToString(url);
	}

	@Override
	public String readToString(String resourceName, Charset charset) {
		URL url = getResource(resourceName, false);
		return readToString(url, charset);
	}

	@Override
	public boolean exists(String resourceName) {
		return Resources.getResource(resourceName) != null;
	}

	@Override
	public URL getResource(String resourceName, boolean optional) {
		URL url = Resources.getResource(resourceName);
		if (url == null && !optional) {
			throw new IllegalArgumentException("Resource not found: '" + resourceName + "'");
		}
		return url;
	}

}
