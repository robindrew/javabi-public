package com.javabi.common.io.file;

import static java.util.Collections.emptyList;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RegularFile implements IFile {

	private final File file;

	public RegularFile(String filename) {
		this.file = new File(filename);
	}

	public RegularFile(File file) {
		this.file = file;
	}

	@Override
	public void checkExists() {
		if (!exists()) {
			throw new IllegalStateException("Directory does not exist: " + file);
		}
	}

	@Override
	public File getFile() {
		return file;
	}

	@Override
	public boolean exists() {
		return file.exists();
	}

	@Override
	public void checkFile(boolean exists) {
		if (exists) {
			checkExists();
		}
		if (!isFile()) {
			throw new IllegalStateException("Not a regular file: " + file);
		}
	}

	@Override
	public void checkDirectory(boolean exists) {
		if (exists) {
			checkExists();
		}
		if (!isDirectory()) {
			throw new IllegalStateException("Not a directory: " + file);
		}
	}

	@Override
	public List<IFile> getFiles() {
		File[] files = file.listFiles();
		if (files == null || files.length == 0) {
			return emptyList();
		}
		List<IFile> list = new ArrayList<>(files.length);
		for (File file : files) {
			list.add(new RegularFile(file));
		}
		return list;
	}

	@Override
	public boolean isFile() {
		return file.isFile();
	}

	@Override
	public boolean isDirectory() {
		return file.isDirectory();
	}

}
