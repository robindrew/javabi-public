package com.javabi.common.io.scp;

import java.io.File;

import com.javabi.common.io.connection.INetworkUser;

public interface ISecureCopy {

	INetworkUser getUser();

	void upload(File localFile, String remoteFilename);

	void download(String remoteFilename, File localFile);

}
