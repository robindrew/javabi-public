package com.javabi.common.io.file;

import static com.javabi.common.lang.Variables.notNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import com.google.common.base.Throwables;
import com.google.common.io.ByteSource;
import com.google.common.io.CharSource;
import com.google.common.io.Files;
import com.javabi.common.crypto.IEncoder;
import com.javabi.common.crypto.Md5Encoder;
import com.javabi.common.io.stream.StreamInput;

public class FileInput extends StreamInput implements IFileInput {

	private final File file;

	public FileInput(File file) {
		this.file = notNull("file", file);
	}

	public FileInput(String filename) {
		this(new File(filename));
	}

	@Override
	public File getFile() {
		return file;
	}

	@Override
	public byte[] toMd5() {
		IEncoder encoder = new Md5Encoder();
		return encoder.encodeToBytes(file);
	}

	@Override
	public byte[] readToByteArray() {

		// NIO approach (10% slower on very small files, 100% faster on large files)
		try (FileInputStream input = new FileInputStream(file)) {
			FileChannel channel = input.getChannel();
			int length = (int) channel.size();
			MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
			byte[] bytes = new byte[length];
			buffer.get(bytes, 0, length);
			return bytes;

		} catch (IOException e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public CharSource asCharSource() {
		return Files.asCharSource(getFile(), getCharset());
	}

	@Override
	public ByteSource asByteSource() {
		return Files.asByteSource(getFile());
	}

}
