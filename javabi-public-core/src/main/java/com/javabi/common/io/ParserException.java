package com.javabi.common.io;

public class ParserException extends RuntimeException {

	public ParserException(String message) {
		super(message);
	}

	public ParserException(Throwable t) {
		super(t);
	}

}
