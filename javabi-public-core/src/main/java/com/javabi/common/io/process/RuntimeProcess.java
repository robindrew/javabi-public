package com.javabi.common.io.process;

import java.io.File;
import java.io.InputStream;

import com.google.common.base.Throwables;
import com.google.common.io.ByteSource;
import com.google.common.io.CharSource;
import com.javabi.common.io.stream.StreamInput;

public class RuntimeProcess extends StreamInput implements IRuntimeProcess {

	private String command;
	private String[] environment = null;
	private File directory = null;
	private boolean waitFor = true;

	public RuntimeProcess(String command) {
		if (command.isEmpty()) {
			throw new IllegalArgumentException("command is emtpy");
		}
		this.command = command;
	}

	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public String[] getEnvironment() {
		return environment;
	}

	@Override
	public File getDirectory() {
		return directory;
	}

	@Override
	public boolean isWaitFor() {
		return waitFor;
	}

	@Override
	public void setWaitFor(boolean waitFor) {
		this.waitFor = waitFor;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}

	@Override
	public void setEnvironment(String[] environment) {
		this.environment = environment;
	}

	@Override
	public void setDirectory(File directory) {
		this.directory = directory;
	}

	protected InputStream createInputStream() {
		Runtime runtime = Runtime.getRuntime();
		try {
			Process process = runtime.exec(command, environment, getDirectory());
			if (waitFor) {
				process.waitFor();
			}
			return process.getInputStream();
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public CharSource asCharSource() {
		// Make this a proper char source?
		return CharSource.wrap(readToString());
	}

	@Override
	public ByteSource asByteSource() {
		// Make this a proper char source?
		return ByteSource.wrap(readToByteArray());
	}

}
