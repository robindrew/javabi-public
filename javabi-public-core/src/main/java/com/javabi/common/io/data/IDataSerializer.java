package com.javabi.common.io.data;

public interface IDataSerializer<O> extends IDataReadable<O>, IDataWritable<O> {

}
