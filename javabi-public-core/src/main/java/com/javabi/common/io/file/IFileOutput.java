package com.javabi.common.io.file;

import com.javabi.common.io.stream.IStreamOutput;

public interface IFileOutput extends IStreamOutput {

	boolean isAppend();

	void setAppend(boolean append);

}
