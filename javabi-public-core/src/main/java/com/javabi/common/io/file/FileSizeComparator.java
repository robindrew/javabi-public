package com.javabi.common.io.file;

import java.io.File;
import java.util.Comparator;

public class FileSizeComparator implements Comparator<File> {

	private boolean ascending = true;

	public boolean isAscending() {
		return ascending;
	}

	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}

	@Override
	public int compare(File file1, File file2) {
		long length1 = file1.length();
		long length2 = file2.length();
		if (length1 > length2) {
			return ascending ? 1 : -1;
		}
		if (length1 < length2) {
			return ascending ? -1 : 1;
		}

		// Same size
		return file1.compareTo(file2);
	}

}
