package com.javabi.common.io.file;

import java.io.File;
import java.util.Set;

public interface IFileSource {

	Set<File> getFiles(boolean notEmpty);

}
