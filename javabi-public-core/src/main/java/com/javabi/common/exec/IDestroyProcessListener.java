package com.javabi.common.exec;

public interface IDestroyProcessListener {

	void destroy();

}
