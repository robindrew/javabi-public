package com.javabi.common.exec;

import com.javabi.common.exec.stream.IStreamHandler;

public interface IExecProcess {

	long getNumber();

	Process getProcess();

	IStreamHandler getInput();

	IStreamHandler getError();

	void destroy();

	int waitFor();

}
