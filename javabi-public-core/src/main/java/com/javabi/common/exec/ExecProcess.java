package com.javabi.common.exec;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.date.timer.ITimer;
import com.javabi.common.exec.stream.IStreamHandler;

public class ExecProcess implements IExecProcess, IDestroyProcessListener {

	private static final Logger log = LoggerFactory.getLogger(ExecProcess.class);

	private static final AtomicLong counter = new AtomicLong(0);

	private final long number;
	private final ITimer timer;
	private final Process process;
	private final IStreamHandler input;
	private final IStreamHandler error;
	private final AtomicBoolean destroyed = new AtomicBoolean(false);

	public ExecProcess(Process process, ITimer timer, IStreamHandler input, IStreamHandler error) {
		if (process == null) {
			throw new NullPointerException("process");
		}
		this.process = process;
		this.timer = timer;
		this.number = counter.addAndGet(1);

		this.input = input;
		this.error = error;
		input.setDestroyProcessListener(this);
		error.setDestroyProcessListener(this);

		log.info("Process#" + number + " started ...");

		// Input handler
		new Thread(input, toString()).start();

		// Error handler
		new Thread(error, toString()).start();
	}

	@Override
	public void destroy() {
		if (destroyed.compareAndSet(false, true)) {
			process.destroy();
		}
	}

	@Override
	public int waitFor() {
		try {

			// Wait for streams ...
			input.waitFor();
			error.waitFor();

			int exitValue = process.waitFor();
			timer.stop();
			log.info(toString() + " finished (status: " + exitValue + ") in " + timer);
			return exitValue;

		} catch (Exception e) {
			if (destroyed.get()) {
				log.debug("Process was destroyed", e);
				return 0;
			}
			throw Throwables.propagate(e);
		}
	}

	@Override
	public long getNumber() {
		return number;
	}

	@Override
	public Process getProcess() {
		return process;
	}

	@Override
	public String toString() {
		return "Process#" + number;
	}

	@Override
	public IStreamHandler getInput() {
		return input;
	}

	@Override
	public IStreamHandler getError() {
		return error;
	}

}
