package com.javabi.common.exec.script;

import java.io.File;

import com.javabi.common.exec.IExecHandler;

public interface IScriptExecutor extends IExecHandler {

	IScriptExecutor name(String filename);

	IScriptExecutor name(File file);

	IScriptExecutor name(File file, File directory);

	IScriptExecutor argument(Object argument);

	IScriptExecutor arguments(Object... arguments);

	void setSudo(boolean sudo);

}
