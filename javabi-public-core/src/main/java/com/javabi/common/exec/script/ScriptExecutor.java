package com.javabi.common.exec.script;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.javabi.common.exec.ExecHandler;
import com.javabi.common.exec.IExecProcess;

public class ScriptExecutor extends ExecHandler implements IScriptExecutor {

	private static final ScriptType getType(File file) {
		String name = file.getName().toLowerCase();
		if (name.endsWith(".bat")) {
			return ScriptType.BATCH;
		}
		if (name.endsWith(".sh")) {
			return ScriptType.SHELL;
		}
		throw new IllegalArgumentException("Unable to determine type for file: " + file);
	}

	private File scriptFile = null;
	private File workingDirectory = null;
	private List<String> argumentList = new ArrayList<String>();
	private boolean sudo = false;

	@Override
	public void setSudo(boolean sudo) {
		this.sudo = sudo;
	}

	public boolean isSudo() {
		return sudo;
	}

	@Override
	public IExecProcess execute() {
		if (scriptFile == null) {
			throw new IllegalStateException("script file not set");
		}
		return super.execute();
	}

	@Override
	public IScriptExecutor name(String filename) {
		return name(new File(filename));
	}

	@Override
	public IScriptExecutor name(File file) {
		return name(file, file.getParentFile());
	}

	@Override
	public IScriptExecutor name(File file, File directory) {
		if (file.isDirectory()) {
			throw new IllegalArgumentException("script file is a directory: " + file);
		}
		if (!directory.isDirectory()) {
			throw new IllegalArgumentException("working directory is not a directory: " + directory);
		}
		if (!file.getAbsolutePath().startsWith(directory.getAbsolutePath())) {
			throw new IllegalArgumentException("script file is not in working directory: " + file + " not in " + directory);
		}
		getType(file);
		this.scriptFile = file;
		this.workingDirectory = directory;
		return this;
	}

	@Override
	public ScriptExecutor argument(Object argument) {
		if (argument == null) {
			throw new NullPointerException("argument");
		}
		String text = argument.toString();
		if (text.isEmpty()) {
			throw new IllegalArgumentException("argument is empty");
		}
		argumentList.add(text);
		return this;
	}

	@Override
	public ScriptExecutor arguments(Object... arguments) {
		for (Object argument : arguments) {
			argument(argument);
		}
		return this;
	}

	@Override
	public String[] getCommand() {
		ScriptType type = getType(scriptFile);
		switch (type) {
			case BATCH:
				return createBatchCommand();
			case SHELL:
				return createShellCommand();
			default:
				throw new IllegalStateException("type not supported: " + type);
		}
	}

	private String[] createShellCommand() {

		// Linux Shell Script
		String[] command = new String[3];
		command[0] = "/bin/bash";
		command[1] = "-c";

		StringBuilder exec = new StringBuilder();
		if (isSudo()) {
			exec.append("sudo ");
		}
		exec.append(scriptFile.getAbsolutePath());
		for (String argument : argumentList) {
			exec.append(' ').append(argument);
		}
		command[2] = exec.toString();

		return command;
	}

	private String[] createBatchCommand() {

		// Windows Batch File
		String[] command = new String[3 + argumentList.size()];
		command[0] = "cmd";
		command[1] = "/c";
		command[2] = getBatchFile();
		for (int i = 0; i < argumentList.size(); i++) {
			command[3 + i] = argumentList.get(i);
		}

		return command;
	}

	private String getBatchFile() {
		String working = workingDirectory.getAbsolutePath().replace('/', '\\');
		String path = scriptFile.getAbsolutePath().replace('/', '\\');
		path = path.substring(working.length());
		if (path.charAt(0) == '\\') {
			path = path.substring(1);
		}
		return path;
	}

	@Override
	public File getWorkingDirectory() {
		return workingDirectory;
	}

}
