package com.javabi.common.exec.script;

public enum ScriptType {

	BATCH, SHELL;

}
