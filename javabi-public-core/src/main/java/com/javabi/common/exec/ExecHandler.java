package com.javabi.common.exec;

import java.io.File;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.common.exec.stream.IStreamHandler;
import com.javabi.common.exec.stream.ProcessError;
import com.javabi.common.exec.stream.ProcessInput;

public abstract class ExecHandler implements IExecHandler {

	private static final Logger log = LoggerFactory.getLogger(ExecHandler.class);

	private IStreamHandler inputHandler = null;
	private IStreamHandler errorHandler = null;

	@Override
	public IExecHandler setInputHandler(IStreamHandler handler) {
		if (handler == null) {
			throw new NullPointerException("handler");
		}
		this.inputHandler = handler;
		return this;
	}

	@Override
	public IExecHandler setErrorHandler(IStreamHandler handler) {
		if (handler == null) {
			throw new NullPointerException("handler");
		}
		this.errorHandler = handler;
		return this;
	}

	@Override
	public IExecProcess execute() {
		try {

			// Handlers
			IStreamHandler input = (inputHandler == null ? new ProcessInput() : inputHandler);
			IStreamHandler error = (errorHandler == null ? new ProcessError() : errorHandler);

			// Command
			String[] command = getCommand();

			// Environment
			String[] environment = getEnvironment();

			// Directory
			File directory = getWorkingDirectory();

			// Start timing ...
			NanoTimer timer = new NanoTimer();
			timer.start();

			// Execute!
			log.info("Executing: command=" + toString(command) + ", environment=" + toString(environment) + ", directory=" + directory);
			Process process = Runtime.getRuntime().exec(command, environment, directory);
			input.setStream(process.getInputStream());
			error.setStream(process.getErrorStream());

			return new ExecProcess(process, timer, input, error);

		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	private String toString(String[] array) {
		return Arrays.toString(array);
	}

	@Override
	public String[] getEnvironment() {
		return null;
	}

	@Override
	public File getWorkingDirectory() {
		return null;
	}

}
