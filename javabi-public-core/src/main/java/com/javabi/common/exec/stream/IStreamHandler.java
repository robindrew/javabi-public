package com.javabi.common.exec.stream;

import java.io.InputStream;

import com.javabi.common.exec.IDestroyProcessListener;

public interface IStreamHandler extends Runnable {

	void setStream(InputStream input);

	void setDestroyProcessListener(IDestroyProcessListener destroy);

	void waitFor();

	void destroy();
}
