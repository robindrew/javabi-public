package com.javabi.common.exec.stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProcessInput extends LineReaderHandler {

	private static final Logger log = LoggerFactory.getLogger(ProcessInput.class);

	@Override
	protected boolean handleLine(String line) {
		log.info(line);
		return true;
	}

	@Override
	protected void handleException(Exception e) {
		log.error("Error reading stream", e);
	}

}
