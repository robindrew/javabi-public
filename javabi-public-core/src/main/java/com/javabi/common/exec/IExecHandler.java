package com.javabi.common.exec;

import java.io.File;

import com.javabi.common.exec.stream.IStreamHandler;

public interface IExecHandler {

	String[] getCommand();

	String[] getEnvironment();

	File getWorkingDirectory();

	IExecHandler setInputHandler(IStreamHandler handler);

	IExecHandler setErrorHandler(IStreamHandler handler);

	IExecProcess execute();
}
