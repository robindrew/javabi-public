package com.javabi.common.exec.stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProcessError extends LineReaderHandler {

	private static final Logger log = LoggerFactory.getLogger(ProcessError.class);

	@Override
	protected boolean handleLine(String line) {
		log.error(line);
		return true;
	}

	@Override
	protected void handleException(Exception e) {
		log.error("Error reading stream", e);
	}

}
