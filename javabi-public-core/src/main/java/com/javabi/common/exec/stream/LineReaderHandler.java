package com.javabi.common.exec.stream;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.javabi.common.concurrent.CountDown;
import com.javabi.common.exec.IDestroyProcessListener;

public abstract class LineReaderHandler implements IStreamHandler {

	private volatile BufferedReader reader = null;
	private volatile IDestroyProcessListener destroyListener = null;
	private final CountDown count = new CountDown();

	@Override
	public void setStream(InputStream stream) {
		if (stream == null) {
			throw new NullPointerException("stream");
		}
		this.reader = new BufferedReader(new InputStreamReader(stream));
	}

	@Override
	public void setDestroyProcessListener(IDestroyProcessListener destroy) {
		if (destroy == null) {
			throw new NullPointerException("destroy");
		}
		this.destroyListener = destroy;
	}

	@Override
	public void destroy() {
		destroyListener.destroy();
	}

	@Override
	public void run() {
		if (reader == null) {
			throw new IllegalStateException("stream not set");
		}
		if (destroyListener == null) {
			throw new IllegalStateException("destroy listener not set");
		}
		count.increment();
		try {
			while (true) {
				String line = reader.readLine();
				if (line == null) {
					break;
				}
				if (!handleLine(line)) {
					break;
				}
			}
		} catch (Exception e) {
			handleException(e);
		} finally {
			count.decrement();
		}
	}

	@Override
	public void waitFor() {
		count.waitFor();
	}

	protected abstract void handleException(Exception e);

	protected abstract boolean handleLine(String line);
}
