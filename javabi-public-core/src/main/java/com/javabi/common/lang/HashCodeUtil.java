package com.javabi.common.lang;

/**
 * A Hash Code Utility.
 */
public class HashCodeUtil {

	/**
	 * Returns the hash code for the given value.
	 * @param value the value.
	 * @return the hash code.
	 */
	public static final int hash(byte value) {
		return value;
	}

	/**
	 * Returns the hash code for the given value.
	 * @param value the value.
	 * @return the hash code.
	 */
	public static final int hash(short value) {
		return value;
	}

	/**
	 * Returns the hash code for the given value.
	 * @param value the value.
	 * @return the hash code.
	 */
	public static final int hash(int value) {
		return value;
	}

	/**
	 * Returns the hash code for the given value.
	 * @param value the value.
	 * @return the hash code.
	 */
	public static final int hash(long value) {
		return (int) value;
	}

	/**
	 * Returns the hash code for the given value.
	 * @param value the value.
	 * @return the hash code.
	 */
	public static final int hash(float value) {
		return (int) value;
	}

	/**
	 * Returns the hash code for the given value.
	 * @param value the value.
	 * @return the hash code.
	 */
	public static final int hash(double value) {
		return (int) value;
	}

	/**
	 * Returns the hash code for the given value.
	 * @param value the value.
	 * @return the hash code.
	 */
	public static final int hash(char value) {
		return value;
	}

	/**
	 * Returns the hash code for the given value.
	 * @param value the value.
	 * @return the hash code.
	 */
	public static final int hash(boolean value) {
		return value ? 1231 : 1237;
	}

	/**
	 * Returns the hash code for the given value.
	 * @param value the value.
	 * @return the hash code.
	 */
	public static final int hash(Object value) {
		return value == null ? 1249 : value.hashCode();
	}

	/**
	 * Inaccessible constructor.
	 */
	private HashCodeUtil() {
	}

}
