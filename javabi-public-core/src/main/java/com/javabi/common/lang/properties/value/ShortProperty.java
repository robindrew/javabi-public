package com.javabi.common.lang.properties.value;

import java.util.Arrays;
import java.util.Collection;

import com.javabi.common.lang.properties.AbstractProperty;
import com.javabi.common.lang.properties.PropertyException;

/**
 * A Short Property.
 */
public class ShortProperty extends AbstractProperty<Short> {

	/** The valid values. */
	private volatile short[] validValues = null;
	/** The invalid values. */
	private volatile short[] invalidValues = null;
	/** The minimum value. */
	private volatile short minimumValue = Short.MIN_VALUE;
	/** The maximum value. */
	private volatile short maximumValue = Short.MAX_VALUE;

	public ShortProperty(String... keys) {
		super(keys);
	}

	public ShortProperty(Collection<String> keys) {
		super(keys);
	}

	public ShortProperty min(short minimumValue) {
		this.minimumValue = minimumValue;
		return this;
	}

	public ShortProperty max(short maximumValue) {
		this.maximumValue = maximumValue;
		return this;
	}

	public ShortProperty range(short minimumValue, short maximumValue) {
		min(minimumValue);
		max(maximumValue);
		return this;
	}

	public ShortProperty valid(short... values) {
		this.validValues = values;
		return this;
	}

	public ShortProperty intvalid(short... values) {
		this.invalidValues = values;
		return this;
	}

	public short getMinimumValue() {
		return minimumValue;
	}

	public short getMaximumValue() {
		return maximumValue;
	}

	@Override
	protected synchronized Short parseValue(String key, String value) {
		Short shortValue = Short.parseShort(value);

		// Check Valid Values
		if (validValues != null) {
			for (short validValue : validValues) {
				if (shortValue == validValue) {
					return shortValue;
				}
			}
			throw new PropertyException(toString(key, value) + " is not a valid value: " + Arrays.toString(validValues));
		}

		// Check Invalid Values
		if (invalidValues != null) {
			for (short invalidValue : invalidValues) {
				if (shortValue == invalidValue) {
					throw new PropertyException(toString(key, value) + " is an invalid value: " + Arrays.toString(invalidValues));
				}
			}
		}

		// Check Minimum Value
		if (shortValue < minimumValue) {
			throw new PropertyException(toString(key, value) + " less than minimum value: " + minimumValue);
		}

		// Check Maximum Value
		if (shortValue > maximumValue) {
			throw new PropertyException(toString(key, value) + " greater than maximum value: " + maximumValue);
		}
		return shortValue;
	}

}
