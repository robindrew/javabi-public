package com.javabi.common.lang.properties.value;

import java.util.concurrent.TimeUnit;

/**
 * Very common to use times, and hence the {@link TimeUnit} enum for configuration.
 */
public class TimeUnitProperty extends EnumProperty<TimeUnit> {

	public TimeUnitProperty(String... keys) {
		super(TimeUnit.class, keys);
	}

}
