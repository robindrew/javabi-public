package com.javabi.common.lang.properties.value;

import java.util.Arrays;
import java.util.Collection;

import com.javabi.common.lang.properties.AbstractProperty;
import com.javabi.common.lang.properties.PropertyException;

/**
 * A Byte Property.
 */
public class ByteProperty extends AbstractProperty<Byte> {

	/** The valid values. */
	private volatile byte[] validValues = null;
	/** The invalid values. */
	private volatile byte[] invalidValues = null;
	/** The minimum value. */
	private volatile byte minimumValue = Byte.MIN_VALUE;
	/** The maximum value. */
	private volatile byte maximumValue = Byte.MAX_VALUE;

	public ByteProperty(String... keys) {
		super(keys);
	}

	public ByteProperty(Collection<String> keys) {
		super(keys);
	}

	public ByteProperty min(byte minimumValue) {
		this.minimumValue = minimumValue;
		return this;
	}

	public ByteProperty max(byte maximumValue) {
		this.maximumValue = maximumValue;
		return this;
	}

	public ByteProperty range(byte minimumValue, byte maximumValue) {
		min(minimumValue);
		max(maximumValue);
		return this;
	}

	public ByteProperty valid(byte... values) {
		this.validValues = values;
		return this;
	}

	public ByteProperty intvalid(byte... values) {
		this.invalidValues = values;
		return this;
	}

	public byte getMinimumValue() {
		return minimumValue;
	}

	public byte getMaximumValue() {
		return maximumValue;
	}

	@Override
	protected synchronized Byte parseValue(String key, String value) {
		Byte byteValue = Byte.parseByte(value);

		// Check Valid Values
		if (validValues != null) {
			for (byte validValue : validValues) {
				if (byteValue == validValue) {
					return byteValue;
				}
			}
			throw new PropertyException(toString(key, value) + " is not a valid value: " + Arrays.toString(validValues));
		}

		// Check Invalid Values
		if (invalidValues != null) {
			for (byte invalidValue : invalidValues) {
				if (byteValue == invalidValue) {
					throw new PropertyException(toString(key, value) + " is an invalid value: " + Arrays.toString(invalidValues));
				}
			}
		}

		// Check Minimum Value
		if (byteValue < minimumValue) {
			throw new PropertyException(toString(key, value) + " less than minimum value: " + minimumValue);
		}

		// Check Maximum Value
		if (byteValue > maximumValue) {
			throw new PropertyException(toString(key, value) + " greater than maximum value: " + maximumValue);
		}
		return byteValue;
	}

}
