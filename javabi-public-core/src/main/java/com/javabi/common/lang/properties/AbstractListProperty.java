package com.javabi.common.lang.properties;

import static com.javabi.common.dependency.DependencyFactory.getDependency;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.google.common.base.Objects;
import com.javabi.common.lang.Variables;

/**
 * An Abstract Property.
 */
public abstract class AbstractListProperty<V> implements IListProperty<V> {

	public static String toString(String key, String value) {
		return "key: '" + key + "', value: '" + value + "'";
	}

	/** The property key. */
	private final String key;
	/** The current value. */
	private volatile List<V> currentValue;
	/** The default value. */
	private volatile List<V> defaultValue;
	/** Indicates if null is a valid value. */
	private boolean notNull = true;
	/** Indicates if empty is a valid value. */
	private boolean notEmpty = true;
	/** Indicates if this is optional. */
	private boolean optional = false;

	protected AbstractListProperty(String key) {
		this.key = key;
	}

	@Override
	public IProperty<List<V>> optional() {
		this.optional = true;
		return this;
	}

	public final String getKey() {
		return key;
	}

	public AbstractListProperty<V> notNull(boolean notNull) {
		this.notNull = notNull;
		return this;
	}

	@SuppressWarnings("unchecked")
	public AbstractListProperty<V> defaultValue(V... values) {
		if (notNull) {
			Variables.notNull("values", values);
		}
		if (notEmpty) {
			Variables.notEmpty("values", values);
		}
		this.defaultValue = new ArrayList<V>(Arrays.asList(values));
		return this;
	}

	@Override
	public boolean hasValue(List<V> value) {
		return Objects.equal(get(), value);
	}

	@Override
	public V[] toArray() {
		List<V> list = get();
		if (list == null) {
			return null;
		}
		return list.toArray(newArray(list.size()));
	}

	protected abstract V[] newArray(int length);

	@Override
	public List<V> get() {
		if (currentValue != null) {
			return currentValue;
		}

		List<V> list = new ArrayList<V>();
		IPropertyMap factory = getDependency(IPropertyMap.class);
		for (int i = 0; i < Integer.MAX_VALUE; i++) {
			String elementKey = key + "[" + i + "]";
			String value = factory.getString(elementKey);
			if (value == null) {
				break;
			}
			V elementValue = parseValue(elementKey, value);
			list.add(elementValue);
		}
		if (!list.isEmpty()) {
			currentValue = list;
			return list;
		}

		// Not found, use default value if set
		if (defaultValue != null) {
			return defaultValue;
		}

		// Not found, is null valid?
		if (!notNull) {
			return null;
		}

		// Optional?
		if (optional) {
			return null;
		}

		// Default handling of not found cases
		factory.notFound(key + "[0]");
		return null;
	}

	@Override
	public String getString() {
		return String.valueOf(get());
	}

	@Override
	public boolean exists() {
		if (currentValue != null) {
			return true;
		}

		// Only need to check existance of first element
		IPropertyMap factory = getDependency(IPropertyMap.class);
		String elementKey = key + "[0]";
		String value = factory.getString(elementKey);
		if (value != null) {
			parseValue(elementKey, value);
			return true;
		}

		// Not exists
		return false;
	}

	/**
	 * Called to indicate a key was not found.
	 * @param factory the property factory.
	 * @param keys the keys.
	 */
	protected V notFound(IPropertyMap factory, Set<String> keys) {
		factory.notFound(keys);
		return null;
	}

	/**
	 * Returns the value.
	 * @param key the key.
	 * @param value the value.
	 * @return the value.
	 */
	protected abstract V parseValue(String key, String value);

}
