package com.javabi.common.lang.properties.value;

import java.io.File;
import java.util.Collection;
import java.util.regex.Pattern;

import com.javabi.common.lang.properties.AbstractProperty;
import com.javabi.common.lang.properties.PropertyException;

/**
 * A File Property.
 */
public class FileProperty extends AbstractProperty<File> {

	/** The pattern. */
	private volatile Pattern pattern = null;
	/** Check the file exists? */
	private volatile boolean checkExists = false;
	/** Check the file is a checkDirectory? */
	private volatile boolean checkDirectory = false;
	/** Check the file is a file? */
	private volatile boolean checkFile = false;

	/**
	 * Creates the property.
	 * @param keys the keys.
	 */
	public FileProperty(String... keys) {
		super(keys);
	}

	/**
	 * Creates the property.
	 * @param keys the keys.
	 */
	public FileProperty(Collection<String> keys) {
		super(keys);
	}

	public FileProperty pattern(Pattern pattern) {
		this.pattern = pattern;
		return this;
	}

	public FileProperty pattern(String regex) {
		return pattern(Pattern.compile(regex));
	}

	public FileProperty checkExists() {
		this.checkExists = true;
		return this;
	}

	public FileProperty checkDirectory() {
		this.checkDirectory = true;
		return this;
	}

	public FileProperty checkFile() {
		this.checkFile = true;
		return this;
	}

	public Pattern getPattern() {
		return pattern;
	}

	public boolean isCheckExists() {
		return checkExists;
	}

	public boolean isCheckDirectory() {
		return checkDirectory;
	}

	public boolean isCheckFile() {
		return checkFile;
	}

	@Override
	protected synchronized File parseValue(String key, String value) {
		if (pattern != null && !pattern.matcher(value).matches()) {
			throw new PropertyException(toString(key, value) + " not matching pattern: '" + pattern.pattern() + "'");
		}
		File file = new File(value);
		if (checkExists && !file.exists()) {
			throw new PropertyException(toString(key, value) + " file does not exist");
		}
		if (checkDirectory && !file.isDirectory()) {
			throw new PropertyException(toString(key, value) + " is not a directory");
		}
		if (checkFile && !file.isFile()) {
			throw new PropertyException(toString(key, value) + " is not a regular file");
		}
		return file;
	}
}
