package com.javabi.common.lang.properties.value;

import java.util.Arrays;
import java.util.Collection;

import com.javabi.common.lang.properties.AbstractProperty;
import com.javabi.common.lang.properties.PropertyException;

/**
 * A Long Property.
 */
public class LongProperty extends AbstractProperty<Long> {

	/** The valid values. */
	private volatile long[] validValues = null;
	/** The invalid values. */
	private volatile long[] invalidValues = null;
	/** The minimum value. */
	private volatile long minimumValue = Long.MIN_VALUE;
	/** The maximum value. */
	private volatile long maximumValue = Long.MAX_VALUE;

	public LongProperty(String... keys) {
		super(keys);
	}

	public LongProperty(Collection<String> keys) {
		super(keys);
	}

	public LongProperty min(long minimumValue) {
		this.minimumValue = minimumValue;
		return this;
	}

	public LongProperty max(long maximumValue) {
		this.maximumValue = maximumValue;
		return this;
	}

	public LongProperty range(long minimumValue, long maximumValue) {
		min(minimumValue);
		max(maximumValue);
		return this;
	}

	public LongProperty valid(long... values) {
		this.validValues = values;
		return this;
	}

	public LongProperty intvalid(long... values) {
		this.invalidValues = values;
		return this;
	}

	public long getMinimumValue() {
		return minimumValue;
	}

	public long getMaximumValue() {
		return maximumValue;
	}

	@Override
	protected synchronized Long parseValue(String key, String value) {
		Long longValue = Long.parseLong(value);

		// Check Valid Values
		if (validValues != null) {
			for (long validValue : validValues) {
				if (longValue == validValue) {
					return longValue;
				}
			}
			throw new PropertyException(toString(key, value) + " is not a valid value: " + Arrays.toString(validValues));
		}

		// Check Invalid Values
		if (invalidValues != null) {
			for (long invalidValue : invalidValues) {
				if (longValue == invalidValue) {
					throw new PropertyException(toString(key, value) + " is an invalid value: " + Arrays.toString(invalidValues));
				}
			}
		}

		// Check Minimum Value
		if (longValue < minimumValue) {
			throw new PropertyException(toString(key, value) + " less than minimum value: " + minimumValue);
		}

		// Check Maximum Value
		if (longValue > maximumValue) {
			throw new PropertyException(toString(key, value) + " greater than maximum value: " + maximumValue);
		}
		return longValue;
	}

}
