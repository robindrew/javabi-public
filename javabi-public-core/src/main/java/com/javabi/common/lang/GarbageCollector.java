package com.javabi.common.lang;

import static com.javabi.common.text.StringFormats.bytes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.date.timer.NanoTimer;

public class GarbageCollector {

	private static final Logger log = LoggerFactory.getLogger(GarbageCollector.class);

	public static synchronized void gc() {
		Runtime runtime = Runtime.getRuntime();
		long usedBefore = runtime.totalMemory() - runtime.freeMemory();
		NanoTimer timer = new NanoTimer();
		timer.start();
		System.gc();
		timer.stop();
		long usedAfter = runtime.totalMemory() - runtime.freeMemory();
		String used = bytes(usedAfter);
		String change = bytes(usedBefore - usedAfter);
		log.info("[Garbage Collection] " + used + " (" + change + ") in " + timer);
	}

	private GarbageCollector() {
	}

}
