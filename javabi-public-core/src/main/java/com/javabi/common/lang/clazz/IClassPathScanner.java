package com.javabi.common.lang.clazz;

import java.util.Map;

public interface IClassPathScanner {

	Map<String, String> scanClassPath();

}
