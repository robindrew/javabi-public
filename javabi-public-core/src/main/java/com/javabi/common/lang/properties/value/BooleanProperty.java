package com.javabi.common.lang.properties.value;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.javabi.common.lang.properties.AbstractProperty;
import com.javabi.common.lang.properties.PropertyException;

/**
 * A Boolean Property.
 */
public class BooleanProperty extends AbstractProperty<Boolean> {

	/** The true values. */
	private volatile Set<String> trueValues = null;
	/** The false values. */
	private volatile Set<String> falseValues = null;

	public BooleanProperty(String... keys) {
		super(keys);
	}

	public BooleanProperty(Collection<String> keys) {
		super(keys);
	}

	public BooleanProperty trueValues(Collection<String> values) {
		trueValues = new HashSet<String>(values);
		return this;
	}

	public BooleanProperty falseValues(Collection<String> values) {
		falseValues = new HashSet<String>(values);
		return this;
	}

	public BooleanProperty trueValues(String... values) {
		return trueValues(Arrays.asList(values));
	}

	public BooleanProperty falseValues(String... values) {
		return falseValues(Arrays.asList(values));
	}

	@Override
	protected synchronized Boolean parseValue(String key, String value) {
		if (value.equalsIgnoreCase("true")) {
			return Boolean.TRUE;
		}
		if (value.equalsIgnoreCase("false")) {
			return Boolean.FALSE;
		}

		// Check Valid Values
		if (trueValues != null) {
			for (String trueValue : trueValues) {
				if (value.equals(trueValue)) {
					return true;
				}
			}
		}

		// Check Invalid Values
		if (falseValues != null) {
			for (String falseValue : falseValues) {
				if (value.equals(falseValue)) {
					return false;
				}
			}
		}

		throw new PropertyException(toString(key, value) + " not a valid boolean value");
	}
}
