package com.javabi.common.lang;

/**
 * A Copyable.
 * @param <C> the copy type.
 */
public interface Copyable<C> {

	/**
	 * Returns a copy of this object.
	 * @return a copy of this object.
	 */
	C copy();

}
