package com.javabi.common.lang;

import java.net.URL;

import com.javabi.common.io.stream.IStreamInput;

public interface IResource extends IStreamInput {

	String getName();

	boolean exists();

	URL getURL();

}
