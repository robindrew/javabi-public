package com.javabi.common.lang.properties.bean;

import com.google.common.base.Supplier;
import com.javabi.common.lang.clazz.Classes;

public class BeanSupplier {

	private final String keyPrefix;

	public BeanSupplier(String keyPrefix) {
		if (keyPrefix.isEmpty()) {
			throw new IllegalArgumentException("keyPrefix is empty");
		}
		this.keyPrefix = keyPrefix;
	}

	public <B> B getInstance(Class<? extends Supplier<B>> type) {
		Supplier<B> supplier = Classes.newInstance(type);
		return get(supplier);
	}

	public <B> B get(Supplier<B> supplier) {
		BeanParser<Supplier<B>> parser = new BeanParser<Supplier<B>>(supplier, keyPrefix);
		parser.parseBean();
		B bean = supplier.get();
		if (bean == null) {
			throw new NullPointerException("bean is null!");
		}
		return bean;
	}

	public <B> B get(B bean) {
		BeanParser<B> parser = new BeanParser<B>(bean, keyPrefix);
		parser.parseBean();
		return bean;
	}

}
