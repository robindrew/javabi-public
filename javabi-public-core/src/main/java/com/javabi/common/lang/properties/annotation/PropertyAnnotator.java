package com.javabi.common.lang.properties.annotation;

import static com.javabi.common.dependency.DependencyFactory.getDependency;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.lang.properties.IPropertyMap;
import com.javabi.common.lang.reflect.field.FieldLister;
import com.javabi.common.lang.reflect.field.IField;
import com.javabi.common.text.parser.StringParserMap;

public class PropertyAnnotator {

	private static final Logger log = LoggerFactory.getLogger(PropertyAnnotator.class);

	private final String keyPrefix;
	private final IPropertyMap map;

	public PropertyAnnotator(String keyPrefix) {
		this(keyPrefix, getDependency(IPropertyMap.class));
	}

	public PropertyAnnotator(String keyPrefix, IPropertyMap map) {
		if (keyPrefix == null) {
			throw new NullPointerException("keyPrefix");
		}
		if (map == null) {
			throw new NullPointerException("map");
		}
		this.keyPrefix = keyPrefix;
		this.map = map;
	}

	public void set(Object object) {
		if (object == null) {
			throw new NullPointerException("object");
		}

		Class<?> type = object.getClass();
		List<IField> fields = new FieldLister().setRecursive(false).getFieldList(type);
		for (IField field : fields) {

			// Field
			PropertyField annotation1 = field.getAnnotation(PropertyField.class);
			if (annotation1 != null) {
				set(type, field, object, annotation1);
			}

			// List field
			PropertyListField annotation2 = field.getAnnotation(PropertyListField.class);
			if (annotation2 != null) {
				setList(type, field, object, annotation2);
			}
		}
	}

	private void setList(Class<?> type, IField field, Object object, PropertyListField annotation) {
		String name = field.getName();
		log.info("Set list property: " + type.getName() + "." + name);

		Class<?> elementType = annotation.type();
		List<Object> list = new ArrayList<Object>();
		int index = 0;
		while (true) {
			String key = keyPrefix + "." + field.getName() + "[" + index + "]";
			String value = map.getString(key);
			if (value == null) {
				if (index > 0) {
					break;
				}
				if (!annotation.required()) {
					return;
				}
				throw new IllegalStateException("Unable to parse property bean: " + type + "." + name + ", missing property: " + key);
			}

			// Parse
			StringParserMap map = new StringParserMap();
			Object parsed = map.parse(elementType, value);
			list.add(parsed);

			index++;
		}
		if (list.size() < annotation.minSize()) {
			throw new IllegalStateException("List property too small: " + type + "." + name + ", minimum size=" + annotation.minSize() + ", parsed size=" + list.size());
		}

		try {
			field.setAccessible(true);
			field.set(object, list);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	private void set(Class<?> type, IField field, Object object, PropertyField annotation) {
		String name = field.getName();
		log.info("Set property: " + type.getName() + "." + name);

		String key = keyPrefix + "." + field.getName();
		String value = map.getString(key);
		if (value == null) {
			if (!annotation.required()) {
				return;
			}
			throw new IllegalStateException("Unable to parse property bean: " + type + "." + name + ", missing property: " + key);
		}

		// Parse
		StringParserMap map = new StringParserMap();
		Object parsed = map.parse(field.getType(), value);
		try {
			field.setAccessible(true);
			field.set(object, parsed);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

}
