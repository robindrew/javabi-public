package com.javabi.common.lang.properties.parser;

import java.io.InputStream;
import java.io.Reader;
import java.util.Properties;

public interface IPropertyParser {

	Properties parseProperties(InputStream input);

	Properties parseProperties(Reader reader);

}
