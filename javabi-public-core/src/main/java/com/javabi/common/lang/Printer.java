package com.javabi.common.lang;

import java.util.Collection;

public class Printer {

	public static <E> void println(Collection<E> collection) {
		for (E element : collection) {
			System.out.println(element);
		}
	}

}
