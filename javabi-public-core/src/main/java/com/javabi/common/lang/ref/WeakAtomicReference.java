package com.javabi.common.lang.ref;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicReference;

public class WeakAtomicReference<T> {

	private final AtomicReference<WeakReference<T>> reference = new AtomicReference<WeakReference<T>>();

	public void set(T value) {
		if (value == null) {
			reference.set(null);
		} else {
			reference.set(new WeakReference<T>(value));
		}
	}

	public T get() {
		WeakReference<T> weak = reference.get();
		if (weak == null) {
			return null;
		}
		return weak.get();
	}

}
