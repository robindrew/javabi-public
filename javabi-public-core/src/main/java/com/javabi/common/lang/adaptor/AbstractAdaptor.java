package com.javabi.common.lang.adaptor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractAdaptor<F, T> implements IAdaptor<F, T> {

	public abstract Class<F> getFromClass();

	public abstract Class<T> getToClass();

	public T adapt(F from) {
		if (from == null) {
			return null;
		}
		T to = newToInstance();
		adapt(from, to);
		return to;
	}

	public T newToInstance() {
		// By default we attempt to instantiate the class directly.
		// Override this when interfaces or complex constructors are involved
		try {
			return getToClass().newInstance();
		} catch (Exception e) {
			throw new AdaptorException(e);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <C extends Collection<?>> C adapt(Collection<? extends F> fromCollection, C toCollection) {
		for (F from : fromCollection) {
			T to = adapt(from);
			((Collection) toCollection).add(to);
		}
		return toCollection;
	}

	public List<T> adapt(Collection<? extends F> fromCollection) {
		return adapt(fromCollection, new ArrayList<T>(fromCollection.size()));
	}

}
