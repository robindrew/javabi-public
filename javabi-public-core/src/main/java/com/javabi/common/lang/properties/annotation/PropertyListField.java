package com.javabi.common.lang.properties.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface PropertyListField {

	Class<?> type();

	String name() default "";

	boolean required() default true;

	int minSize() default 0;

}
