package com.javabi.common.lang;

import java.lang.management.MemoryUsage;
import java.net.InetAddress;
import java.util.List;
import java.util.Map;

/**
 * A utility class encapsulating many of the most important core methods of java.
 */
public final class Java {

	private static volatile IJava instance = new JavaImpl();

	public static IJava getJava() {
		return instance;
	}

	public static void setJava(IJava java) {
		if (java == null) {
			throw new NullPointerException("java");
		}
		instance = java;
	}

	private Java() {
		// Inaccessible constructor
	}

	public static long nanoTime() {
		return instance.nanoTime();
	}

	public static long currentTimeMillis() {
		return instance.currentTimeMillis();
	}

	public static int currentTimeSeconds() {
		return instance.currentTimeSeconds();
	}

	public static long maxMemory() {
		return instance.maxMemory();
	}

	public static long totalMemory() {
		return instance.totalMemory();
	}

	public static long freeMemory() {
		return instance.freeMemory();
	}

	public static long usedMemory() {
		return instance.usedMemory();
	}

	public static int availableProcessors() {
		return instance.availableProcessors();
	}

	public static String getOperatingSystem() {
		return instance.getOperatingSystem();
	}

	public static String getUserName() {
		return instance.getUserName();
	}

	public static String getTimeZone() {
		return instance.getTimeZone();
	}

	public static String getFileEncoding() {
		return instance.getFileEncoding();
	}

	public static String getFileSeparator() {
		return instance.getFileSeparator();
	}

	public static String getPathSeparator() {
		return instance.getPathSeparator();
	}

	public static String getLineSeparator() {
		return instance.getLineSeparator();
	}

	public static String getClassPath() {
		return instance.getClassPath();
	}

	public static String getLibraryPath() {
		return instance.getLibraryPath();
	}

	public static String getJavaVersion() {
		return instance.getJavaVersion();
	}

	public static String getWorkingDirectory() {
		return instance.getWorkingDirectory();
	}

	public static long getProcessId() {
		return instance.getProcessId();
	}

	public static InetAddress getLocalHost() {
		return instance.getLocalHost();
	}

	public static String getHostName() {
		return instance.getHostName();
	}

	public static String getHostAddress() {
		return instance.getHostAddress();
	}

	public static List<String> getClassPathList() {
		return instance.getClassPathList();
	}

	public static List<String> getLibraryPathList() {
		return instance.getLibraryPathList();
	}

	public static List<String> getInputArguments() {
		return instance.getInputArguments();
	}

	public static long getStartTime() {
		return instance.getStartTime();
	}

	public static long getUptime() {
		return instance.getUptime();
	}

	public static int getLoadedClassCount() {
		return instance.getLoadedClassCount();
	}

	public static MemoryUsage getHeapMemoryUsage() {
		return instance.getHeapMemoryUsage();
	}

	public static MemoryUsage getNonHeapMemoryUsage() {
		return instance.getNonHeapMemoryUsage();
	}

	public static Map<String, String> getEnvironmentPropertyMap() {
		return instance.getEnvironmentPropertyMap();
	}

	public static Map<String, String> getSystemPropertyMap() {
		return instance.getSystemPropertyMap();
	}

	public static String getSystemProperty(String key) {
		return instance.getSystemProperty(key);
	}

	public static String getSystemProperty(String key, String defaultValue) {
		return instance.getSystemProperty(key, defaultValue);
	}

	public static String getEnvironmentProperty(String key) {
		return instance.getEnvironmentProperty(key);
	}

	public static String getEnvironmentProperty(String key, String defaultValue) {
		return instance.getEnvironmentProperty(key, defaultValue);
	}

	public static void gc() {
		instance.gc();
	}

	public static void exit(int status) {
		instance.exit(status);
	}

	public static void addShutdownHook(Thread hook) {
		instance.addShutdownHook(hook);
	}

	public static void print(Object text) {
		instance.print(text);
	}

	public static void print(char[] text) {
		instance.print(text);
	}

	public static void println() {
		instance.println();
	}

	public static void println(Object text) {
		instance.println(text);
	}

	public static void println(char[] text) {
		instance.println(text);
	}

	public static void printf(String text, Object... args) {
		instance.printf(text, args);
	}

	public static double random() {
		return instance.random();
	}

	public static String toString(Object object) {
		return instance.toString(object);
	}

	public static String getJavaRuntimeVersion() {
		return instance.getJavaRuntimeVersion();
	}

	public static int hashCode(Object object) {
		return instance.hashCode(object);
	}

	public static boolean equals(Object object1, Object object2) {
		return instance.equals(object1, object2);
	}
}
