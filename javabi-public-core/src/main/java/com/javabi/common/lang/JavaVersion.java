package com.javabi.common.lang;

import static java.lang.Integer.parseInt;

/**
 * A Java Version.
 * <p>
 * Consists of three digits, followed by an optional update number, and optional identifier. For example:
 * </p>
 * <ul>
 * <li>1.4.0_03-ea</li>
 * <li>1.5.0_22</li>
 * <li>1.8.0_73-b02</li>
 * </ul>
 * <p>
 * For more information see <a href="http://www.oracle.com/technetwork/java/javase/versioning-naming-139433.html">J2SE
 * SDK/JRE Version String Naming Convention</a>
 * </p>
 */
public class JavaVersion {

	/**
	 * Parse a {@link JavaVersion} from the given text!
	 * @param text the text.
	 * @return the java version.
	 */
	public static JavaVersion parseJavaVersion(String text) {
		int major1 = 0;
		int major2 = 0;
		int major3 = 0;
		int update = 0;
		String identifier = "";

		// Parse the identifier (optional)
		int dash = text.indexOf('-');
		if (dash != -1) {
			identifier = text.substring(dash + 1);
			text = text.substring(0, dash);
		}

		// Parse the update (optional)
		int underscore = text.indexOf('_');
		if (underscore != -1) {
			update = parseInt(text.substring(underscore + 1));
			text = text.substring(0, underscore);
		}

		String[] parts = text.split("\\.");
		major1 = parseInt(parts[0]);
		major2 = parseInt(parts[1]);
		major3 = parseInt(parts[2]);

		return new JavaVersion(major1, major2, major3, update, identifier);
	}

	/** The first version (currently always 1). */
	private final int major1;
	/** The important version (e.g. 3,4,5,6,7,8,9, etc...) */
	private final int major2;
	/** The minor version. */
	private final int major3;
	/** The update number (optional) */
	private final int update;
	/** The build identifier (optional) */
	private final String identifier;

	public JavaVersion(int major1, int major2, int major3, int update, String identifier) {
		if (major1 < 1) {
			throw new IllegalArgumentException("major1=" + major1);
		}
		if (major2 < 0) {
			throw new IllegalArgumentException("major2=" + major2);
		}
		if (major3 < 0) {
			throw new IllegalArgumentException("major3=" + major3);
		}
		if (update < 0) {
			throw new IllegalArgumentException("update=" + update);
		}
		if (identifier == null) {
			throw new NullPointerException("identifier");
		}

		this.major1 = major1;
		this.major2 = major2;
		this.major3 = major3;
		this.update = update;
		this.identifier = identifier;
	}

	/**
	 * Returns the first version number (currently always 1).
	 */
	public int getMajor1() {
		return major1;
	}

	/**
	 * Returns the second version number (this is the actual Java version e.g. 8).
	 */
	public int getMajor2() {
		return major2;
	}

	/**
	 * Returns the third version number (bug-fix version number).
	 */
	public int getMajor3() {
		return major3;
	}

	/**
	 * Returns the update number (for specific bug fixes).
	 */
	public int getUpdate() {
		return update;
	}

	/**
	 * Returns the build identifier.
	 */
	public String getIdentifier() {
		return identifier;
	}

	@Override
	public String toString() {
		StringBuilder version = new StringBuilder();

		version.append(major1).append('.');
		version.append(major2).append('.');
		version.append(major3);

		if (update > 0) {
			version.append('_');
			if (update < 10) {
				version.append('0');
			}
			version.append(update);
		}

		if (!identifier.isEmpty()) {
			version.append('-');
			version.append(identifier);
		}

		return version.toString();
	}
}
