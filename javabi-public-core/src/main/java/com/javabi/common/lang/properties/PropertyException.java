package com.javabi.common.lang.properties;

/**
 * A Property Exception.
 */
public class PropertyException extends RuntimeException {

	/** The serialization id. */
	private static final long serialVersionUID = 7736080708565936746L;

	public PropertyException(Throwable cause) {
		super(cause);
	}

	public PropertyException(String message, Throwable cause) {
		super(message, cause);
	}

	public PropertyException(String message) {
		super(message);
	}

	public PropertyException(String key, String value, String message) {
		super(AbstractProperty.toString(key, value) + ", " + message);
	}

	public PropertyException(String key, String value) {
		super(AbstractProperty.toString(key, value));
	}

}
