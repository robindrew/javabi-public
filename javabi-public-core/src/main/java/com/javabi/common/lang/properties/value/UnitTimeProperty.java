package com.javabi.common.lang.properties.value;

import java.util.Collection;

import com.javabi.common.date.UnitTime;
import com.javabi.common.lang.properties.AbstractProperty;
import com.javabi.common.text.parser.UnitTimeParser;

/**
 * Very common to use duration with units, and hence the {@link UnitTime} property.
 */
public class UnitTimeProperty extends AbstractProperty<UnitTime> {

	public UnitTimeProperty(String... keys) {
		super(keys);
	}

	public UnitTimeProperty(Collection<String> keys) {
		super(keys);
	}

	@Override
	protected UnitTime parseValue(String key, String value) {
		return new UnitTimeParser().parse(value);
	}

}
