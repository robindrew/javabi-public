package com.javabi.common.lang;

public class Arrays {

	private static volatile IArrays instance = new ArraysImpl();

	public static IArrays getArrays() {
		return instance;
	}

	public static void setArrays(IArrays arrays) {
		if (arrays == null) {
			throw new NullPointerException("arrays");
		}
		instance = arrays;
	}

	private Arrays() {
		// Inaccessible constructor
	}

	public static int getLength(Object array) {
		return instance.getLength(array);
	}

	public static Object newInstance(Class<?> componentType, int length) {
		return instance.newInstance(componentType, length);
	}

	public static Object newInstance(Class<?> componentType, int... dimensions) {
		return instance.newInstance(componentType, dimensions);
	}

	public static void copy(Object from, int fromIndex, Object to, int toIndex, int length) {
		instance.copy(from, fromIndex, to, toIndex, length);
	}

	public static String toString(byte[] array) {
		return instance.toString(array);
	}

	public static String toString(short[] array) {
		return instance.toString(array);
	}

	public static String toString(int[] array) {
		return instance.toString(array);
	}

	public static String toString(long[] array) {
		return instance.toString(array);
	}

	public static String toString(float[] array) {
		return instance.toString(array);
	}

	public static String toString(double[] array) {
		return instance.toString(array);
	}

	public static String toString(char[] array) {
		return instance.toString(array);
	}

	public static String toString(boolean[] array) {
		return instance.toString(array);
	}

	public static String toString(Object[] array) {
		return instance.toString(array);
	}

}
