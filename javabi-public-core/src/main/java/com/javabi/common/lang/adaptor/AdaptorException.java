package com.javabi.common.lang.adaptor;

public class AdaptorException extends RuntimeException {

	private static final long serialVersionUID = -1327270253016145255L;

	public AdaptorException(Throwable cause) {
		super(cause);
	}

}
