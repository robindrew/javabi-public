package com.javabi.common.lang.properties.value;

import java.util.Arrays;
import java.util.Collection;

import com.javabi.common.lang.properties.AbstractProperty;
import com.javabi.common.lang.properties.PropertyException;

/**
 * A Float Property.
 */
public class FloatProperty extends AbstractProperty<Float> {

	/** The valid values. */
	private volatile float[] validValues = null;
	/** The invalid values. */
	private volatile float[] invalidValues = null;
	/** The minimum value. */
	private volatile float minimumValue = Float.MIN_VALUE;
	/** The maximum value. */
	private volatile float maximumValue = Float.MAX_VALUE;

	public FloatProperty(String... keys) {
		super(keys);
	}

	public FloatProperty(Collection<String> keys) {
		super(keys);
	}

	public FloatProperty min(float minimumValue) {
		this.minimumValue = minimumValue;
		return this;
	}

	public FloatProperty max(float maximumValue) {
		this.maximumValue = maximumValue;
		return this;
	}

	public FloatProperty range(float minimumValue, float maximumValue) {
		min(minimumValue);
		max(maximumValue);
		return this;
	}

	public FloatProperty valid(float... values) {
		this.validValues = values;
		return this;
	}

	public FloatProperty intvalid(float... values) {
		this.invalidValues = values;
		return this;
	}

	public float getMinimumValue() {
		return minimumValue;
	}

	public float getMaximumValue() {
		return maximumValue;
	}

	@Override
	protected synchronized Float parseValue(String key, String value) {
		Float floatValue = Float.parseFloat(value);

		// Check Valid Values
		if (validValues != null) {
			for (float validValue : validValues) {
				if (floatValue == validValue) {
					return floatValue;
				}
			}
			throw new PropertyException(toString(key, value) + " is not a valid value: " + Arrays.toString(validValues));
		}

		// Check Invalid Values
		if (invalidValues != null) {
			for (float invalidValue : invalidValues) {
				if (floatValue == invalidValue) {
					throw new PropertyException(toString(key, value) + " is an invalid value: " + Arrays.toString(invalidValues));
				}
			}
		}

		// Check Minimum Value
		if (floatValue < minimumValue) {
			throw new PropertyException(toString(key, value) + " less than minimum value: " + minimumValue);
		}

		// Check Maximum Value
		if (floatValue > maximumValue) {
			throw new PropertyException(toString(key, value) + " greater than maximum value: " + maximumValue);
		}
		return floatValue;
	}

}
