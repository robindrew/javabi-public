package com.javabi.common.lang.properties.bean;

import static com.javabi.common.lang.Variables.notEmpty;

import com.google.common.base.Objects;
import com.javabi.common.lang.Variables;
import com.javabi.common.lang.clazz.Classes;
import com.javabi.common.lang.properties.IProperty;

public class BeanProperty<B> implements IProperty<B> {

	/** The bean class. */
	private final Class<B> beanClass;
	/** The key prefix. */
	private final String keyPrefix;
	/** The current value. */
	private volatile B currentValue;

	public BeanProperty(Class<B> beanClass, String keyPrefix) {
		this.beanClass = Variables.notNull("beanClass", beanClass);
		this.keyPrefix = notEmpty("keyPrefix", keyPrefix);
	}

	@Override
	public boolean hasValue(B value) {
		return Objects.equal(get(), value);
	}

	@Override
	public B get() {
		if (currentValue != null) {
			return currentValue;
		}
		populate();
		return currentValue;
	}

	@Override
	public String getString() {
		return String.valueOf(get());
	}

	public Class<B> getBeanClass() {
		return beanClass;
	}

	@Override
	public boolean exists() {
		if (currentValue != null) {
			return true;
		}
		try {
			populate();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void populate() {
		B bean = Classes.newInstance(beanClass);
		BeanParser<B> parser = new BeanParser<B>(bean, keyPrefix);
		parser.parseBean();
		currentValue = bean;
	}

	@Override
	public IProperty<B> optional() {
		throw new IllegalStateException("Bean properties can not be optional");
	}

}
