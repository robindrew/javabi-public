package com.javabi.common.lang;

import java.math.BigDecimal;
import java.util.regex.Pattern;

public interface IStringValue extends CharSequence, Comparable<IStringValue> {

	String toString();

	boolean isNull();

	String get();

	String get(String defaultValue);

	Pattern pattern();

	Pattern pattern(Pattern defaultValue);

	String stringValue();

	String stringValue(String defaultValue);

	boolean booleanValue();

	boolean booleanValue(boolean defaultValue);

	byte byteValue();

	byte byteValue(byte defaultValue);

	short shortValue();

	short shortValue(short defaultValue);

	int intValue();

	int intValue(int defaultValue);

	long longValue();

	long longValue(long defaultValue);

	float floatValue();

	float floatValue(float defaultValue);

	double doubleValue();

	double doubleValue(double defaultValue);

	<E extends Enum<E>> E enumValue(Class<E> clazz);

	<E extends Enum<E>> E enumValue(E defaultValue);

	BigDecimal decimalValue();

	int hashCode();

	boolean equals(Object object);

	int compareTo(IStringValue value);

}