package com.javabi.common.lang.ref;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public class TimeoutReference<T> {

	private final long timeout;
	private final AtomicLong timeLastSet = new AtomicLong(0);
	private final AtomicReference<T> value = new AtomicReference<T>();

	public TimeoutReference(long timeout, TimeUnit unit) {
		if (timeout < 1) {
			throw new IllegalArgumentException("timeout=" + timeout);
		}
		this.timeout = unit.toMillis(timeout);
	}

	public TimeoutReference(long timeout, TimeUnit unit, T value) {
		if (timeout < 1) {
			throw new IllegalArgumentException("timeout=" + timeout);
		}
		this.timeout = unit.toMillis(timeout);
		set(value);
	}

	public T set(T value) {
		if (value != null) {
			timeLastSet.set(System.currentTimeMillis());
		}
		this.value.set(value);
		return value;
	}

	public T get() {
		T value = this.value.get();
		if (value == null) {
			return null;
		}
		long timeNow = System.currentTimeMillis();
		if (timeLastSet.get() + timeout < timeNow) {
			this.value.set(null);
			return null;
		}
		return value;
	}

}
