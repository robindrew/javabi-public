package com.javabi.common.lang.properties;

import java.io.File;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * A Property Factory.
 */
public interface IPropertyMap {

	/**
	 * Add custom properties to the factory.
	 * @param properties the properties to add.
	 * @return
	 */
	Map<String, String> addCustomProperties(Properties properties);

	/**
	 * Returns the property for the given key.
	 * @param key the key.
	 * @return the property value.
	 */
	String getString(String key);

	/**
	 * Returns the property for the given key.
	 * @param key the key.
	 * @return the value.
	 */
	int getInteger(String key);

	/**
	 * Returns the property for the given key.
	 * @param key the key.
	 * @return the value.
	 */
	long getLong(String key);

	/**
	 * Returns the property for the given key.
	 * @param key the key.
	 * @return the value.
	 */
	boolean getBoolean(String key);

	/**
	 * Returns the enum for the given key.
	 * @param key the key.
	 * @param enumClass the enum class.
	 * @return the value.
	 */
	<E extends Enum<E>> E getEnum(String key, Class<E> enumClass);

	/**
	 * Returns the existing file for the given key.
	 * @param key the key.
	 * @param directory true if it is a directory.
	 * @return the name.
	 */
	File getFile(String key, boolean directory);

	/**
	 * Called to indicate keys where not found.
	 * @param keys the keys.
	 */
	void notFound(Set<String> keys);

	/**
	 * Called to indicate keys where not found.
	 * @param keys the keys.
	 */
	void notFound(String... keys);

	/**
	 * Returns the environment properties.
	 * @return the environment properties.
	 */
	Map<String, String> getEnvProperties();

	/**
	 * Returns the system properties.
	 * @return the system properties.
	 */
	Map<String, String> getSystemProperties();

	/**
	 * Returns the custom properties.
	 * @return the custom properties.
	 */
	Set<Map<String, String>> getCustomProperties();

}
