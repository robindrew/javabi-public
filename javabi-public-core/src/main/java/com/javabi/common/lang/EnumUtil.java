package com.javabi.common.lang;

/**
 * A collection of enumeration utility methods.
 */
public final class EnumUtil {

	/**
	 * Utility constructor.
	 */
	private EnumUtil() {
	}

	/**
	 * Returns the names of the given enumeration values.
	 * @param values the values.
	 * @return the names.
	 */
	public static final String[] names(Enum<?>[] values) {
		String[] names = new String[values.length];
		for (int i = 0; i < names.length; i++) {
			names[i] = values[i].name();
		}
		return names;
	}
}
