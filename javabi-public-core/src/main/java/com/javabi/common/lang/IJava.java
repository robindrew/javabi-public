package com.javabi.common.lang;

import java.lang.management.MemoryUsage;
import java.net.InetAddress;
import java.util.List;
import java.util.Map;

public interface IJava {

	long nanoTime();

	long currentTimeMillis();

	int currentTimeSeconds();

	long maxMemory();

	long totalMemory();

	long freeMemory();

	long usedMemory();

	int availableProcessors();

	String getOperatingSystem();

	String getUserName();

	String getTimeZone();

	String getFileEncoding();

	String getFileSeparator();

	String getPathSeparator();

	String getLineSeparator();

	String getClassPath();

	String getLibraryPath();

	String getJavaVersion();

	String getJavaRuntimeVersion();

	String getWorkingDirectory();

	long getProcessId();

	InetAddress getLocalHost();

	String getHostName();

	String getHostAddress();

	List<String> getClassPathList();

	List<String> getLibraryPathList();

	List<String> getInputArguments();

	long getStartTime();

	long getUptime();

	int getLoadedClassCount();

	MemoryUsage getHeapMemoryUsage();

	MemoryUsage getNonHeapMemoryUsage();

	Map<String, String> getEnvironmentPropertyMap();

	Map<String, String> getSystemPropertyMap();

	String getSystemProperty(String key);

	String getSystemProperty(String key, String defaultValue);

	String getEnvironmentProperty(String key);

	String getEnvironmentProperty(String key, String defaultValue);

	void gc();

	void exit(int status);

	void addShutdownHook(Thread hook);

	void print(Object text);

	void print(char[] text);

	void println();

	void println(Object text);

	void println(char[] text);

	void printf(String text, Object... args);

	double random();

	int hashCode(Object object);

	boolean equals(Object object1, Object object2);

	String toString(Object object);
}