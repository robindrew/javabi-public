package com.javabi.common.lang.properties.value;

import java.util.regex.Pattern;

import com.javabi.common.io.connection.INetworkAddress;
import com.javabi.common.io.connection.NetworkAddress;
import com.javabi.common.lang.properties.AbstractListProperty;
import com.javabi.common.lang.properties.PropertyException;

/**
 * A Network Address Property.
 */
public class NetworkAddressListProperty extends AbstractListProperty<INetworkAddress> {

	/** The maximum length. */
	private static final Pattern pattern = Pattern.compile(".+:[0-9]+");

	/**
	 * Creates the property.
	 * @param keys the keys.
	 */
	public NetworkAddressListProperty(String key) {
		super(key);
	}

	@Override
	protected synchronized INetworkAddress parseValue(String key, String value) {
		if (!pattern.matcher(value).matches()) {
			throw new PropertyException(toString(key, value) + " is not correctly formatted");
		}
		int colon = value.indexOf(':');
		String host = value.substring(0, colon);
		int port = Integer.parseInt(value.substring(colon + 1));
		return new NetworkAddress(host, port);
	}

	@Override
	protected INetworkAddress[] newArray(int length) {
		return new INetworkAddress[length];
	}

}
