package com.javabi.common.lang;

/**
 * A Prime Number.
 */
public class PrimeNumber extends Number {

	/** The serialVersionUID. */
	private static final long serialVersionUID = 7468555381743846032L;

	/** The first twenty five prime numbers. */
	private static final int[] PRIME_NUMBERS = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 };

	/**
	 * Returns true if the given number is prime.
	 * @param number the number.
	 * @return true if the given number is prime.
	 */
	public static final boolean isPrime(int number) {
		if (number < 1) {
			throw new IllegalArgumentException("number=" + number);
		}
		if (number == 1) {
			return false;
		}
		for (int divisor = 2; divisor < number; divisor++) {
			if (number % divisor == 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns the nth prime number.
	 * @param sequence the nth sequence.
	 * @return the prime number.
	 */
	public static final int getValue(int sequence) {
		if (sequence < 0) {
			throw new IllegalArgumentException();
		}
		if (sequence < PRIME_NUMBERS.length) {
			return PRIME_NUMBERS[sequence];
		}

		// Calculate on the fly ...
		sequence -= PRIME_NUMBERS.length;
		int value = 101;
		while (true) {
			if (isPrime(value)) {
				if (sequence == 0) {
					return value;
				}
				sequence--;
			}
			value++;
		}
	}

	/** The value. */
	private final int value;

	/**
	 * Inaccessible constructor.
	 * @param number the start prime number.
	 */
	private PrimeNumber(int number) {
		this.value = number;
	}

	/**
	 * Returns the current number.
	 * @return the current number.
	 */
	public int get() {
		return value;
	}

	/**
	 * Returns the next number.
	 * @return the next number.
	 */
	public int next() {
		int next = value + 1;
		while (!isPrime(next)) {
			next++;
		}
		return next;
	}

	@Override
	public String toString() {
		return String.valueOf(get());
	}

	@Override
	public double doubleValue() {
		return get();
	}

	@Override
	public float floatValue() {
		return get();
	}

	@Override
	public int intValue() {
		return get();
	}

	@Override
	public long longValue() {
		return get();
	}
}
