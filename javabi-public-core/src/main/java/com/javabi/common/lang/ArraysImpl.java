package com.javabi.common.lang;

import java.lang.reflect.Array;

public class ArraysImpl implements IArrays {

	@Override
	public void copy(Object from, int fromIndex, Object to, int toIndex, int length) {
		System.arraycopy(from, fromIndex, to, toIndex, length);
	}

	@Override
	public int getLength(Object array) {
		return Array.getLength(array);
	}

	@Override
	public Object newInstance(Class<?> componentType, int length) {
		return Array.newInstance(componentType, length);
	}

	@Override
	public Object newInstance(Class<?> componentType, int... dimensions) {
		return Array.newInstance(componentType, dimensions);
	}

	@Override
	public String toString(byte[] array) {
		return Arrays.toString(array);
	}

	@Override
	public String toString(short[] array) {
		return Arrays.toString(array);
	}

	@Override
	public String toString(int[] array) {
		return Arrays.toString(array);
	}

	@Override
	public String toString(long[] array) {
		return Arrays.toString(array);
	}

	@Override
	public String toString(float[] array) {
		return Arrays.toString(array);
	}

	@Override
	public String toString(double[] array) {
		return Arrays.toString(array);
	}

	@Override
	public String toString(char[] array) {
		return Arrays.toString(array);
	}

	@Override
	public String toString(boolean[] array) {
		return Arrays.toString(array);
	}

	@Override
	public String toString(Object[] array) {
		return Arrays.toString(array);
	}

}
