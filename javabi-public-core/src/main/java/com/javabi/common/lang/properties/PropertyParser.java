package com.javabi.common.lang.properties;

public class PropertyParser {

	private static final char ESCAPE = '\\';
	private static final String DEFAULT_START = "${";
	private static final String DEFAULT_FINISH = "}";

	private final IPropertyMap map;
	private final String startDelimiter;
	private final String finishDelimiter;

	public PropertyParser(IPropertyMap map) {
		this(map, DEFAULT_START, DEFAULT_FINISH);
	}

	public PropertyParser(IPropertyMap map, String startDelimiter, String finishDelimiter) {
		if (map == null) {
			throw new NullPointerException("map");
		}
		if (startDelimiter.isEmpty()) {
			throw new IllegalArgumentException("startDelimiter is empty");
		}
		if (finishDelimiter.isEmpty()) {
			throw new IllegalArgumentException("finishDelimiter is empty");
		}
		this.map = map;
		this.startDelimiter = startDelimiter;
		this.finishDelimiter = finishDelimiter;
	}

	protected String getProperty(String key) {
		return map.getString(key);
	}

	public String parse(String text) {
		if (indexOf(text, startDelimiter, 0) == -1) {
			return text;
		}

		// Substitute properties in text
		StringBuilder parsed = new StringBuilder();
		int count = 0;
		int offset = 0;
		while (true) {

			// Start index
			int startIndex = indexOf(text, startDelimiter, offset);
			if (startIndex == -1) {
				parsed.append(text.substring(offset));
				break;
			} else {
				parsed.append(text.substring(offset, startIndex));
			}
			int propertyIndex = startIndex + startDelimiter.length();

			// Finish index
			int finishIndex = indexOf(text, finishDelimiter, propertyIndex);
			if (finishIndex == -1) {
				throw new IllegalStateException("Badly formatted text: '" + text + "'");
			}

			// Evaluate
			String property = text.substring(propertyIndex, finishIndex);
			String value = evaluate(property);
			parsed.append(value);

			// Update offset
			offset = finishIndex + finishDelimiter.length();
			count++;
		}

		// No substitutions made?
		if (count == 0) {
			return text;
		}

		// Recursive call
		String value = parse(parsed.toString());
		return value.replace(ESCAPE + DEFAULT_START, DEFAULT_START);
	}

	private String evaluate(String key) {
		String value = getProperty(key);
		if (value == null) {
			throw new IllegalArgumentException("Property not found: " + key);
		}
		return value;
	}

	private int indexOf(String text, String delimiter, int offset) {
		int index = text.indexOf(delimiter, offset);
		if (index == -1) {
			return -1;
		}

		// Found delimiter, but it is escaped ...
		if (index > 0 && text.charAt(index - 1) == ESCAPE) {
			offset = index + startDelimiter.length();
			return indexOf(text, delimiter, offset);
		}
		return index;
	}
}
