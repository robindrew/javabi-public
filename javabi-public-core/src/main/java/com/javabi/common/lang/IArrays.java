package com.javabi.common.lang;

public interface IArrays {

	int getLength(Object array);

	Object newInstance(Class<?> componentType, int length);

	Object newInstance(Class<?> componentType, int... dimensions);

	void copy(Object from, int fromIndex, Object to, int toIndex, int length);

	String toString(byte[] array);

	String toString(short[] array);

	String toString(int[] array);

	String toString(long[] array);

	String toString(float[] array);

	String toString(double[] array);

	String toString(char[] array);

	String toString(boolean[] array);

	String toString(Object[] array);

}
