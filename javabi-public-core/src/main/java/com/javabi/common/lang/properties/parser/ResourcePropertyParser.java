package com.javabi.common.lang.properties.parser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Charsets;
import com.google.common.base.Throwables;
import com.google.common.io.CharStreams;
import com.javabi.common.lang.Resource;

public class ResourcePropertyParser implements IResourcePropertyParser {

	private static final Logger log = LoggerFactory.getLogger(ResourcePropertyParser.class);

	private static final String INCLUDE_DIRECTIVE = "#include";
	private Charset charset = Charsets.UTF_8;
	private String path = null;

	public void setPath(String path) {
		if (path.isEmpty()) {
			throw new IllegalArgumentException("path is empty");
		}
		this.path = path;
	}

	public void setCharset(Charset charset) {
		if (charset == null) {
			throw new NullPointerException("charset");
		}
		this.charset = charset;
	}

	public Charset getCharset() {
		return charset;
	}

	@Override
	public Properties parseProperties(String resourceName) {
		Resource resource = getResource(resourceName);
		log.info("Parsing properties from: " + resource.getName());
		Reader reader = resource.newReader();
		return parseProperties(reader);
	}

	protected Resource getResource(String resourceName) {
		Resource resource = null;

		// With path ...
		if (path != null) {
			resource = new Resource(path + "/" + resourceName);
			if (!resource.exists()) {
				resource = null;
			}
		}

		// No path ...
		if (resource == null) {
			resource = new Resource(resourceName);
			if (!resource.exists()) {
				throw new IllegalArgumentException("Resource not found: " + resourceName);
			}
		}

		resource.setCharset(charset);
		return resource;
	}

	@Override
	public Properties parseProperties(InputStream input) {
		Reader reader = new InputStreamReader(input, getCharset());
		return parseProperties(reader);
	}

	@Override
	public Properties parseProperties(Reader reader) {
		try {
			// Read contents in entirety
			String contents = CharStreams.toString(reader);
			reader = new StringReader(contents);

			// Parse directives
			BufferedReader buffered = new BufferedReader(reader);
			Properties properties = new Properties();
			while (true) {
				String line = buffered.readLine();
				if (line == null) {
					break;
				}
				line = line.trim();
				parseIncludeDirective(properties, line);
			}

			reader = new StringReader(contents);
			properties.load(reader);
			return properties;
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	private void parseIncludeDirective(Properties properties, String line) {

		// #include
		if (line.toLowerCase().startsWith(INCLUDE_DIRECTIVE)) {
			String resourceName = line.substring(INCLUDE_DIRECTIVE.length());
			resourceName = resourceName.trim();
			Properties included = parseProperties(resourceName);
			properties.putAll(included);
		}
	}
}
