package com.javabi.common.lang.ref;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

public class WeakIdentityReference<T> {

	private final int identityHashCode;
	private final Reference<T> reference;

	public WeakIdentityReference(T referent) {
		this.identityHashCode = System.identityHashCode(referent);
		this.reference = new WeakReference<T>(referent);
	}

	public T get() {
		return reference.get();
	}

	@Override
	public int hashCode() {
		return identityHashCode;
	}

	@Override
	public String toString() {
		return String.valueOf(identityHashCode);
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object instanceof WeakIdentityReference) {
			WeakIdentityReference<?> compare = (WeakIdentityReference<?>) object;
			if (hashCode() == compare.hashCode()) {
				return get() == compare.get();
			}
		}
		return false;
	}

	public boolean isEmpty() {
		return get() == null;
	}

}
