package com.javabi.common.lang;

import static com.javabi.common.lang.SystemProperties.getSystemProperties;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryUsage;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.google.common.base.Throwables;

/**
 * The default implementation of {@link IJava}.
 */
public class JavaImpl implements IJava {

	@Override
	public long nanoTime() {
		return System.nanoTime();
	}

	@Override
	public long currentTimeMillis() {
		return System.currentTimeMillis();
	}

	@Override
	public int currentTimeSeconds() {
		return (int) (System.currentTimeMillis() / 1000);
	}

	@Override
	public long maxMemory() {
		return Runtime.getRuntime().maxMemory();
	}

	@Override
	public long totalMemory() {
		return Runtime.getRuntime().totalMemory();
	}

	@Override
	public long freeMemory() {
		return Runtime.getRuntime().freeMemory();
	}

	@Override
	public long usedMemory() {
		Runtime runtime = Runtime.getRuntime();
		return runtime.totalMemory() - runtime.freeMemory();
	}

	@Override
	public int availableProcessors() {
		return Runtime.getRuntime().availableProcessors();
	}

	@Override
	public String getOperatingSystem() {
		return getSystemProperties().getOperatingSystem();
	}

	@Override
	public String getUserName() {
		return getSystemProperties().getUserName();
	}

	@Override
	public String getTimeZone() {
		return getSystemProperties().getTimeZone();
	}

	@Override
	public String getFileEncoding() {
		return getSystemProperties().getFileEncoding();
	}

	@Override
	public String getFileSeparator() {
		return getSystemProperties().getFileSeparator();
	}

	@Override
	public String getPathSeparator() {
		return getSystemProperties().getPathSeparator();
	}

	@Override
	public String getLineSeparator() {
		return getSystemProperties().getLineSeparator();
	}

	@Override
	public String getClassPath() {
		return getSystemProperties().getClassPath();
	}

	@Override
	public String getLibraryPath() {
		return getSystemProperties().getLibraryPath();
	}

	@Override
	public String getJavaVersion() {
		return getSystemProperties().getJavaVersion();
	}

	@Override
	public String getJavaRuntimeVersion() {
		return getSystemProperties().getJavaRuntimeVersion();
	}

	@Override
	public String getWorkingDirectory() {
		return getSystemProperties().getWorkingDirectory();
	}

	@Override
	public long getProcessId() {
		String name = ManagementFactory.getRuntimeMXBean().getName();
		int index = name.indexOf('@');
		return Long.parseLong(name.substring(0, index));
	}

	@Override
	public InetAddress getLocalHost() {
		try {
			return InetAddress.getLocalHost();
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public String getHostName() {
		return getLocalHost().getHostName();
	}

	@Override
	public String getHostAddress() {
		return getLocalHost().getHostAddress();
	}

	@Override
	public List<String> getClassPathList() {
		String separator = getPathSeparator();
		return Arrays.asList(getClassPath().split(separator));
	}

	@Override
	public List<String> getLibraryPathList() {
		String separator = getPathSeparator();
		return Arrays.asList(getLibraryPath().split(separator));
	}

	@Override
	public List<String> getInputArguments() {
		return ManagementFactory.getRuntimeMXBean().getInputArguments();
	}

	@Override
	public long getStartTime() {
		return ManagementFactory.getRuntimeMXBean().getStartTime();
	}

	@Override
	public long getUptime() {
		return ManagementFactory.getRuntimeMXBean().getUptime();
	}

	@Override
	public int getLoadedClassCount() {
		return ManagementFactory.getClassLoadingMXBean().getLoadedClassCount();
	}

	@Override
	public MemoryUsage getHeapMemoryUsage() {
		return ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
	}

	@Override
	public MemoryUsage getNonHeapMemoryUsage() {
		return ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage();
	}

	@Override
	public Map<String, String> getEnvironmentPropertyMap() {
		return System.getenv();
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, String> getSystemPropertyMap() {
		return (Map) System.getProperties();
	}

	@Override
	public void gc() {
		Runtime.getRuntime().gc();
	}

	@Override
	public void exit(int status) {
		Runtime.getRuntime().exit(status);
	}

	@Override
	public String getSystemProperty(String key) {
		return System.getProperty(key);
	}

	@Override
	public String getEnvironmentProperty(String key) {
		return System.getenv(key);
	}

	@Override
	public String getSystemProperty(String key, String defaultValue) {
		return System.getProperty(key, defaultValue);
	}

	@Override
	public String getEnvironmentProperty(String key, String defaultValue) {
		String value = getEnvironmentProperty(key);
		return value == null ? defaultValue : value;
	}

	@Override
	public void addShutdownHook(Thread hook) {
		Runtime.getRuntime().addShutdownHook(hook);
	}

	@Override
	public void print(Object text) {
		System.out.print(text);
	}

	@Override
	public void print(char[] text) {
		System.out.print(text);
	}

	@Override
	public void println() {
		System.out.println();
	}

	@Override
	public void println(Object text) {
		System.out.println(text);
	}

	@Override
	public void println(char[] text) {
		System.out.println(text);
	}

	@Override
	public void printf(String text, Object... args) {
		System.out.printf(text, args);
	}

	@Override
	public double random() {
		return Math.random();
	}

	@Override
	public String toString(Object object) {
		if (object == null) {
			return "null";
		}
		return ToStringBuilder.reflectionToString(object, SHORT_PREFIX_STYLE);
	}

	@Override
	public int hashCode(Object object) {
		return HashCodeBuilder.reflectionHashCode(object, true);
	}

	@Override
	public boolean equals(Object object1, Object object2) {
		if (object1 == object2) {
			return true;
		}
		if (object1 == null || object2 == null) {
			return false;
		}
		return EqualsBuilder.reflectionEquals(object1, object2, true);
	}
}
