package com.javabi.common.lang.properties.value;

import java.util.Arrays;
import java.util.Collection;

import com.javabi.common.lang.properties.AbstractProperty;
import com.javabi.common.lang.properties.PropertyException;

/**
 * An Integer Property.
 */
public class IntegerProperty extends AbstractProperty<Integer> {

	/** The valid values. */
	private volatile int[] validValues = null;
	/** The invalid values. */
	private volatile int[] invalidValues = null;
	/** The minimum value. */
	private volatile int minimumValue = Integer.MIN_VALUE;
	/** The maximum value. */
	private volatile int maximumValue = Integer.MAX_VALUE;

	public IntegerProperty(String... keys) {
		super(keys);
	}

	public IntegerProperty(Collection<String> keys) {
		super(keys);
	}

	public IntegerProperty min(int minimumValue) {
		this.minimumValue = minimumValue;
		return this;
	}

	public IntegerProperty max(int maximumValue) {
		this.maximumValue = maximumValue;
		return this;
	}

	public IntegerProperty range(int minimumValue, int maximumValue) {
		min(minimumValue);
		max(maximumValue);
		return this;
	}

	public IntegerProperty valid(int... values) {
		this.validValues = values;
		return this;
	}

	public IntegerProperty intvalid(int... values) {
		this.invalidValues = values;
		return this;
	}

	public int getMinimumValue() {
		return minimumValue;
	}

	public int getMaximumValue() {
		return maximumValue;
	}

	@Override
	protected synchronized Integer parseValue(String key, String value) {
		Integer intValue = Integer.parseInt(value);

		// Check Valid Values
		if (validValues != null) {
			for (int validValue : validValues) {
				if (intValue == validValue) {
					return intValue;
				}
			}
			throw new PropertyException(toString(key, value) + " is not a valid value: " + Arrays.toString(validValues));
		}

		// Check Invalid Values
		if (invalidValues != null) {
			for (int invalidValue : invalidValues) {
				if (intValue == invalidValue) {
					throw new PropertyException(toString(key, value) + " is an invalid value: " + Arrays.toString(invalidValues));
				}
			}
		}

		// Check Minimum Value
		if (intValue < minimumValue) {
			throw new PropertyException(toString(key, value) + " less than minimum value: " + minimumValue);
		}

		// Check Maximum Value
		if (intValue > maximumValue) {
			throw new PropertyException(toString(key, value) + " greater than maximum value: " + maximumValue);
		}
		return intValue;
	}

}
