package com.javabi.common.lang;

/**
 * A collection of useful stack trace methods.
 */
public final class StackTraceUtil {

	/**
	 * Returns the stack trace of the given throwable.
	 * @param t the throwable.
	 * @return the stack trace.
	 */
	public static final String toString(Throwable t) {
		StringBuilder builder = new StringBuilder();
		appendTo(t, builder);
		return builder.toString();
	}

	/**
	 * Returns the stack trace of the given throwable.
	 * @param trace the stack trace array.
	 * @return the stack trace.
	 */
	public static final String toString(StackTraceElement[] trace) {
		StringBuilder builder = new StringBuilder();
		appendTo(trace, builder);
		return builder.toString();
	}

	/**
	 * Appends the stack trace of the given throwable to the builder.
	 * @param t the throwable.
	 * @param builder the builder.
	 */
	public static final void appendTo(Throwable t, StringBuilder builder) {
		builder.append(t.getClass().getName());
		String message = t.getLocalizedMessage();
		if (message != null) {
			builder.append(": ").append(message);
		}
		builder.append('\n');
		appendTo(t.getStackTrace(), builder);
		t = t.getCause();
		if (t != null) {
			builder.append("Caused by: ");
			appendTo(t, builder);
		}
	}

	/**
	 * Appends the stack trace of the given throwable to the builder.
	 * @param trace the stack trace array.
	 * @param builder the builder.
	 */
	public static final void appendTo(StackTraceElement[] trace, StringBuilder builder) {
		for (StackTraceElement element : trace) {
			builder.append("\tat ");
			builder.append(element.getClassName()).append('.').append(element.getMethodName());
			if (element.isNativeMethod()) {
				builder.append("(Native Method)");
			} else {
				String fileName = element.getFileName();
				if (fileName == null) {
					builder.append("(Unknown Source)");
				} else {
					int lineNumber = element.getLineNumber();
					if (lineNumber >= 0) {
						builder.append('(').append(fileName).append(':');
						builder.append(lineNumber).append(')');
					} else {
						builder.append('(').append(fileName).append(')');
					}
				}
			}
			builder.append('\n');
		}
	}

	/**
	 * Inaccessible constructor.
	 */
	private StackTraceUtil() {
	}

}
