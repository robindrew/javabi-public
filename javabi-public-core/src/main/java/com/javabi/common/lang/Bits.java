package com.javabi.common.lang;

public class Bits {

	private static final char ZERO = '0';
	private static final char ONE = '1';

	/** The number with all the bits set to zero. */
	public static final long NONE_SET = 0;
	/** The number with all the bits set to one. */
	public static final long ALL_SET = -1;

	/** The number of bits in a byte. */
	public static final int BYTE_BITS = 8;
	/** The number of bits in a short. */
	public static final int SHORT_BITS = 16;
	/** The number of bits in an int. */
	public static final int INT_BITS = 32;
	/** The number of bits in a long. */
	public static final int LONG_BITS = 64;

	private static final long[] MASK = createMask();

	private static long[] createMask() {
		long[] mask = new long[LONG_BITS];

		long value = 1;
		for (int i = 0; i < mask.length; i++) {
			mask[i] = value;
			value = value * 2;
		}

		return mask;
	}

	/**
	 * Sets the bit at the given index to one or zero.
	 * @param value the byte value.
	 * @param index the index at which to set the bit.
	 * @param one true to set the bit to one, false to set the bit to zero.
	 * @return the changed value.
	 */
	public static byte set(byte value, int index, boolean one) {
		return (byte) setBit(value, index, one);
	}

	/**
	 * Sets the bit at the given index to one or zero.
	 * @param value the short value.
	 * @param index the index at which to set the bit.
	 * @param one true to set the bit to one, false to set the bit to zero.
	 * @return the changed value.
	 */
	public static short set(short value, int index, boolean one) {
		return (short) setBit(value, index, one);
	}

	/**
	 * Sets the bit at the given index to one or zero.
	 * @param value the int value.
	 * @param index the index at which to set the bit.
	 * @param one true to set the bit to one, false to set the bit to zero.
	 * @return the changed value.
	 */
	public static int set(int value, int index, boolean one) {
		return (int) setBit(value, index, one);
	}

	/**
	 * Sets the bit at the given index to one or zero.
	 * @param value the long value.
	 * @param index the index at which to set the bit.
	 * @param one true to set the bit to one, false to set the bit to zero.
	 * @return the changed value.
	 */
	public static long set(long value, int index, boolean one) {
		return setBit(value, index, one);
	}

	private static long setBit(long value, int index, boolean one) {
		if (index < 0 || index >= MASK.length) {
			throw new IllegalArgumentException("index=" + index);
		}
		return one ? (value | MASK[index]) : (value & ~MASK[index]);
	}

	/**
	 * Returns true if the bit at the given index is set.
	 * @param value the value to examine.
	 * @param index the index (0-63).
	 * @return true if the bit is set.
	 */
	public static boolean isSet(long value, int index) {
		if (index < 0 || index >= MASK.length) {
			throw new IllegalArgumentException("index=" + index);
		}
		return (value & MASK[index]) != 0;
	}

	/**
	 * Parses a byte from the given text (in bit representation).
	 * @param text the text.
	 * @return the parsed byte value.
	 */
	public static byte parseByte(String text) {
		return (byte) parseBits(text, BYTE_BITS);
	}

	/**
	 * Parses a short from the given text (in bit representation).
	 * @param text the text.
	 * @return the parsed short value.
	 */
	public static short parseShort(String text) {
		return (short) parseBits(text, SHORT_BITS);
	}

	/**
	 * Parses an int from the given text (in bit representation).
	 * @param text the text.
	 * @return the parsed int value.
	 */
	public static int parseInt(String text) {
		return (int) parseBits(text, INT_BITS);
	}

	/**
	 * Parses a long from the given text (in bit representation).
	 * @param text the text.
	 * @return the parsed long value.
	 */
	public static long parseLong(String text) {
		return parseBits(text, LONG_BITS);
	}

	private static long parseBits(String text, int length) {
		if (text == null) {
			throw new NullPointerException("text");
		}
		if (text.isEmpty()) {
			throw new IllegalArgumentException("text is empty");
		}
		if (text.length() > length) {
			throw new IllegalArgumentException("text too long: '" + text + "'");
		}

		long value = 0;
		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);

			if (c == ONE) {
				int index = text.length() - (i + 1);
				value = set(value, index, true);
			}

			else if (c != ZERO) {
				throw new IllegalArgumentException("text contains invalid characters: '" + text + "'");
			}
		}
		return value;
	}

	/**
	 * Returns an 8-bit string representation of the given byte.
	 * @param value the byte value.
	 * @return the string.
	 */
	public static String toString(byte value) {
		return toString(value, BYTE_BITS);
	}

	/**
	 * Returns an 16-bit string representation of the given short.
	 * @param value the short value.
	 * @return the string.
	 */
	public static String toString(short value) {
		return toString(value, SHORT_BITS);
	}

	/**
	 * Returns an 32-bit string representation of the given int.
	 * @param value the int value.
	 * @return the string.
	 */
	public static String toString(int value) {
		return toString(value, INT_BITS);
	}

	/**
	 * Returns an 64-bit string representation of the given long.
	 * @param value the long value.
	 * @return the string.
	 */
	public static String toString(long value) {
		return toString(value, LONG_BITS);
	}

	/**
	 * Returns an arbitrary-bit string representation of the given number.
	 * @param value the number value.
	 * @param bitLength the number of bits.
	 * @return the string.
	 */
	public static String toString(long value, int bitLength) {
		if (bitLength < 1 || bitLength > LONG_BITS) {
			throw new IllegalArgumentException("length=" + bitLength);
		}

		char[] text = new char[bitLength];
		for (int i = 0; i < bitLength; i++) {
			int index = bitLength - (i + 1);
			text[index] = (MASK[i] & value) == 0 ? ZERO : ONE;
		}
		return new String(text);
	}

	/**
	 * Inaccessible constructor.
	 */
	private Bits() {
	}
}
