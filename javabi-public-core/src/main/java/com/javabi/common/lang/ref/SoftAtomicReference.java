package com.javabi.common.lang.ref;

import java.lang.ref.SoftReference;
import java.util.concurrent.atomic.AtomicReference;

public class SoftAtomicReference<T> {

	private final AtomicReference<SoftReference<T>> reference = new AtomicReference<SoftReference<T>>();

	public void set(T value) {
		if (value == null) {
			reference.set(null);
		} else {
			reference.set(new SoftReference<T>(value));
		}
	}

	public T get() {
		SoftReference<T> soft = reference.get();
		if (soft == null) {
			return null;
		}
		return soft.get();
	}

}
