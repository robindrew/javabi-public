package com.javabi.common.lang.dynamic;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("unchecked")
public abstract class DynamicClassLoader {

	private static final Logger log = LoggerFactory.getLogger(DynamicClassLoader.class);

	private final File directory;
	private final Map<String, DynamicClass<?>> nameToClassMap = new HashMap<String, DynamicClass<?>>();

	public DynamicClassLoader(File directory) throws IOException {
		if (!directory.exists()) {
			throw new IOException("File does not exist: " + directory);
		}
		if (!directory.isDirectory()) {
			throw new IOException("File is not a directory: " + directory);
		}
		this.directory = directory;
	}

	//
	// private <T> void refresh(DynamicClass<T> type) throws IOException {
	// type.clear();
	// String name = type.getName();
	// byte[] content = type.readToByteArray();
	// Class<T> clazz = (Class<T>) defineClass(name, content, 0, content.length);
	// type.setType(clazz);
	// }

	public <T> Class<T> forName(String className) throws IOException {
		synchronized (nameToClassMap) {
			DynamicClass<T> type = (DynamicClass<T>) nameToClassMap.get(className);
			if (type == null) {
				String filename = toFileName(className);
				log.info("Loading Class: " + filename);
				File classFile = new File(directory, filename);
				type = new DynamicClass<T>(className, classFile);
				nameToClassMap.put(className, type);
				type.refresh();
			} else if (type.isModified()) {
				log.info("Refreshing Class: " + type);
				type.refresh();
			}
			return type.getType();
		}
	}

	public <T> T newInstance(String className) throws Exception {
		Class<T> type = forName(className);
		return type.newInstance();
	}

	protected abstract String toFileName(String className);

}
