package com.javabi.common.lang.properties.bean;

import static com.javabi.common.lang.Variables.notEmpty;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Objects;
import com.google.common.base.Throwables;
import com.javabi.common.lang.Variables;
import com.javabi.common.lang.clazz.Classes;
import com.javabi.common.lang.properties.IListProperty;
import com.javabi.common.lang.properties.IProperty;

public class BeanListProperty<B> implements IListProperty<B> {

	private final Class<B> beanClass;
	private final String keyPrefix;
	private volatile List<B> currentValue;

	public BeanListProperty(Class<B> beanClass, String keyPrefix) {
		this.beanClass = Variables.notNull("beanClass", beanClass);
		this.keyPrefix = notEmpty("keyPrefix", keyPrefix);
	}

	@Override
	public boolean hasValue(List<B> value) {
		return Objects.equal(get(), value);
	}

	@Override
	public List<B> get() {
		if (currentValue != null) {
			return currentValue;
		}
		populate();
		return currentValue;
	}

	@Override
	public String getString() {
		return String.valueOf(get());
	}

	public Class<B> getBeanClass() {
		return beanClass;
	}

	@Override
	public boolean exists() {
		if (currentValue != null) {
			return true;
		}
		try {
			populate();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void populate() {
		List<B> list = new ArrayList<B>();
		try {
			for (int i = 0; i < Integer.MAX_VALUE; i++) {
				String prefix = keyPrefix + "[" + i + "]";
				B value = parseBean(prefix);
				if (value == null) {
					break;
				}
				list.add(value);
			}
		} catch (Exception e) {
			if (list.isEmpty()) {
				throw Throwables.propagate(e);
			}
		}
		currentValue = list;
	}

	private B parseBean(String prefix) {
		B value = Classes.newInstance(beanClass);
		BeanParser<B> parser = new BeanParser<B>(value, prefix);
		parser.parseBean();
		return value;
	}

	@Override
	public IProperty<List<B>> optional() {
		throw new IllegalStateException("Bean properties can not be optional");
	}

	@SuppressWarnings("unchecked")
	@Override
	public B[] toArray() {
		List<B> list = get();
		B[] array = (B[]) Array.newInstance(beanClass, list.size());
		return list.toArray(array);
	}

}
