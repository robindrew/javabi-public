package com.javabi.common.lang;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface IRandomElement {

	boolean nextBoolean();

	void nextBytes(byte[] bytes);

	byte nextByte();

	short nextShort();

	int nextInt();

	int nextInt(int n);

	int nextInt(int min, int max);

	long nextLong();

	long nextLong(long min, long max);

	char nextChar();

	float nextFloat();

	double nextDouble();

	double nextGaussian();

	void setSeed(long seed);

	<E> E nextElement(List<E> list, boolean remove);

	<E> E nextElement(List<E> list);

	<E> E nextElement(E[] array);

	<E> E nextElement(Collection<E> collection, boolean remove);

	<E> E nextElement(Collection<E> collection);

	<E extends Enum<E>> E nextEnum(Class<E> enumClass);

	Byte nextByteObject(boolean nullable);

	Short nextShortObject(boolean nullable);

	Integer nextIntegerObject(boolean nullable);

	Long nextLongObject(boolean nullable);

	Float nextFloatObject(boolean nullable);

	Double nextDoubleObject(boolean nullable);

	Boolean nextBooleanObject(boolean nullable);

	Character nextCharacterObject(boolean nullable);

	String nextString(int length, boolean nullable);

	Date nextDate(boolean nullable);

	byte[] nextByteArray(int length, boolean nullableArray);

	short[] nextShortArray(int length, boolean nullableArray);

	int[] nextIntArray(int length, boolean nullableArray);

	long[] nextLongArray(int length, boolean nullableArray);

	char[] nextCharArray(int length, boolean nullableArray);

	boolean[] nextBooleanArray(int length, boolean nullableArray);

	float[] nextFloatArray(int length, boolean nullableArray);

	double[] nextDoubleArray(int length, boolean nullableArray);

	Byte[] nextByteObjectArray(int length, boolean nullableArray, boolean nullableElements);

	Short[] nextShortObjectArray(int length, boolean nullableArray, boolean nullableElements);

	Integer[] nextIntegerObjectArray(int length, boolean nullableArray, boolean nullableElements);

	Long[] nextLongObjectArray(int length, boolean nullableArray, boolean nullableElements);

	Float[] nextFloatObjectArray(int length, boolean nullableArray, boolean nullableElements);

	Double[] nextDoubleObjectArray(int length, boolean nullableArray, boolean nullableElements);

	Character[] nextCharacterObjectArray(int length, boolean nullableArray, boolean nullableElements);

	Boolean[] nextBooleanObjectArray(int length, boolean nullableArray, boolean nullableElements);

	String[] nextStringObjectArray(int length, int elementLength, boolean nullableArray, boolean nullableElements);

	Date[] nextDateObjectArray(int length, boolean nullableArray, boolean nullableElements);
}
