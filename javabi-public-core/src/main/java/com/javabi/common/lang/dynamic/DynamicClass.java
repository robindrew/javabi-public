package com.javabi.common.lang.dynamic;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import com.google.common.io.Files;

public class DynamicClass<T> {

	private final String name;
	private final File file;
	private final AtomicLong lastModified = new AtomicLong();
	private final AtomicReference<Class<T>> type = new AtomicReference<Class<T>>();

	public DynamicClass(String name, File file) throws IOException {
		if (name.isEmpty()) {
			throw new IllegalArgumentException("name is empty");
		}
		if (!file.exists()) {
			throw new IOException("File does not exist: " + file.getAbsolutePath());
		}
		this.name = name;
		this.file = file;
		this.lastModified.set(file.lastModified());
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}

	public boolean isModified() {
		long previous = lastModified.get();
		long latest = file.lastModified();
		if (previous == latest) {
			return false;
		}
		lastModified.set(latest);
		return true;
	}

	public void clear() {
		this.type.set(null);
	}

	public void setType(Class<T> type) {
		if (type == null) {
			throw new NullPointerException("type");
		}
		this.type.set(type);
	}

	public Class<T> getType() {
		return type.get();
	}

	public byte[] readToByteArray() throws IOException {
		return Files.toByteArray(file);
	}

	public void refresh() throws IOException {
		clear();
		String name = getName();
		byte[] content = readToByteArray();
		Class<T> clazz = new Loader().load(name, content);
		setType(clazz);
	}

	private static class Loader extends ClassLoader {

		@SuppressWarnings("unchecked")
		public <T> Class<T> load(String name, byte[] content) {
			return (Class<T>) defineClass(name, content, 0, content.length);
		}
	}

}
