package com.javabi.common.lang.properties;

public final class PropertyUtil {

	public static final boolean setSystem(String key, Object value, boolean onlySetIfNull) {
		if (key.isEmpty()) {
			throw new IllegalArgumentException("key is empty");
		}
		if (value == null) {
			throw new NullPointerException("value");
		}
		if (onlySetIfNull) {
			if (System.getProperty(key) == null) {
				System.setProperty(key, value.toString());
				return true;
			}
		} else {
			System.setProperty(key, value.toString());
			return true;
		}
		return false;
	}

	public static final boolean exists(String key) {
		return get(key) != null;
	}

	public static final String get(String key) {
		String value = System.getProperty(key);
		if (value != null) {
			return value;
		}
		return System.getenv(key);
	}

	private PropertyUtil() {
	}
}
