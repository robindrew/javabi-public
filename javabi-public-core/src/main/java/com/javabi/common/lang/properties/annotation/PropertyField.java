package com.javabi.common.lang.properties.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface PropertyField {

	String name() default "";

	boolean required() default true;
}
