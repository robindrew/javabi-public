package com.javabi.common.lang.properties.value;

import java.util.Collection;
import java.util.regex.Pattern;

import com.javabi.common.lang.properties.AbstractProperty;
import com.javabi.common.lang.properties.PropertyException;

/**
 * A String Property.
 */
public class StringProperty extends AbstractProperty<String> {

	/** The minimum length. */
	private volatile int minimumLength = 0;
	/** The maximum length. */
	private volatile int maximumLength = Integer.MAX_VALUE;
	/** The pattern. */
	private volatile Pattern pattern = null;

	/**
	 * Creates the property.
	 * @param keys the keys.
	 */
	public StringProperty(String... keys) {
		super(keys);
	}

	/**
	 * Creates the property.
	 * @param keys the keys.
	 */
	public StringProperty(Collection<String> keys) {
		super(keys);
	}

	public StringProperty min(int minimumLength) {
		this.minimumLength = minimumLength;
		return this;
	}

	public StringProperty max(int maximumLength) {
		this.maximumLength = maximumLength;
		return this;
	}

	public StringProperty pattern(Pattern pattern) {
		this.pattern = pattern;
		return this;
	}

	public StringProperty pattern(String regex) {
		return pattern(Pattern.compile(regex));
	}

	public int getMinimumLength() {
		return minimumLength;
	}

	public int getMaximumLength() {
		return maximumLength;
	}

	public Pattern getPattern() {
		return pattern;
	}

	@Override
	protected synchronized String parseValue(String key, String value) {
		int length = value.length();
		if (length < minimumLength) {
			throw new PropertyException(toString(key, value) + " less than minimum length: " + minimumLength);
		}
		if (length > maximumLength) {
			throw new PropertyException(toString(key, value) + " greater than maximum length: " + maximumLength);
		}
		if (pattern != null) {
			if (!pattern.matcher(value).matches()) {
				throw new PropertyException(toString(key, value) + " not matching pattern: '" + pattern.pattern() + "'");
			}
		}
		return value;
	}

}
