package com.javabi.common.lang.properties;

import java.io.File;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CopyOnWriteArraySet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.lang.Variables;

/**
 * A Property Factory.
 */
public class PropertyMap implements IPropertyMap {

	/** The logger. */
	private static final Logger log = LoggerFactory.getLogger(PropertyMap.class);

	/** The properties. */
	private final Set<Map<String, String>> customPropertiesSet = new CopyOnWriteArraySet<Map<String, String>>();

	@Override
	public Map<String, String> getEnvProperties() {
		return new TreeMap<String, String>(System.getenv());
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, String> getSystemProperties() {
		return new TreeMap<String, String>((Map) System.getProperties());
	}

	@Override
	public Set<Map<String, String>> getCustomProperties() {
		return customPropertiesSet;
	}

	/**
	 * Add properties.
	 * @param properties the properties.
	 */
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, String> addCustomProperties(Properties properties) {
		Variables.notNull("properties", properties);
		Map<String, String> map = new TreeMap<String, String>((Map) properties);
		customPropertiesSet.add(map);
		return map;
	}

	/**
	 * Returns the property for the given key.
	 * @param key the key.
	 * @return the property value.
	 */
	@Override
	public String getString(String key) {
		String value = null;

		// System Property
		value = getSystemProperty(key);
		if (value != null) {
			system(key, value);
			return parseValue(value);
		}

		// Property
		for (Map<String, String> customProperties : customPropertiesSet) {
			value = customProperties.get(key);
			if (value != null) {
				custom(key, value);
				return parseValue(value);
			}
		}

		// Environment Property
		value = getEnvProperty(key);
		if (value != null) {
			environment(key, value);
			return parseValue(value);
		}

		// Missing Properties
		return null;
	}

	private String parseValue(String value) {
		return new PropertyParser(this).parse(value);
	}

	public String getSystemProperty(String key) {
		String value = System.getProperty(key);
		if (value == null) {
			key = key.replace('_', '.').toLowerCase();
			value = System.getProperty(key);
		}
		return value;
	}

	public String getEnvProperty(String key) {
		String value = System.getenv(key);
		if (value == null) {
			key = key.replace('.', '_').toUpperCase();
			value = System.getenv(key);
		}
		return value;
	}

	@Override
	public final boolean getBoolean(String key) {
		String value = getString(key);
		return Boolean.parseBoolean(value);
	}

	@Override
	public final int getInteger(String key) {
		String value = getString(key);
		try {
			return Integer.parseInt(value);
		} catch (NumberFormatException nfe) {
			throw new PropertyException(key, value, " - badly formatted integer");
		}
	}

	@Override
	public final long getLong(String key) {
		String value = getString(key);
		try {
			return Long.parseLong(value);
		} catch (NumberFormatException nfe) {
			throw new PropertyException(key, value, " - badly formatted long");
		}
	}

	@Override
	public <E extends Enum<E>> E getEnum(String key, Class<E> enumClass) {
		String value = getString(key);
		try {
			return Enum.valueOf(enumClass, value);
		} catch (RuntimeException re) {
			throw new PropertyException(key, value, " - badly formatted enum: " + enumClass);
		}
	}

	@Override
	public File getFile(String key, boolean directory) {
		String filename = getString(key);
		if (filename == null) {
			throw new PropertyException("Property not set: '" + key + "'");
		}
		File file = new File(filename);
		if (!file.exists()) {
			throw new PropertyException("File not found: '" + filename + "'");
		}
		if (directory != file.isDirectory()) {
			throw new PropertyException("File not valid: '" + filename + "'");
		}
		return file;
	}

	@Override
	public void notFound(Set<String> keys) {
		throw new PropertyException("Property Not Found: " + keys);
	}

	@Override
	public void notFound(String... keys) {
		throw new PropertyException("Property Not Found: " + Arrays.toString(keys));
	}

	/**
	 * Called to indicate a custom key was found.
	 * @param key the key.
	 * @param value the value for the key.
	 */
	protected void custom(String key, String value) {
		log.info("[Custom] '" + key + "' -> '" + value + "'");
	}

	/**
	 * Called to indicate a environment key was found.
	 * @param key the key.
	 * @param value the value for the key.
	 */
	protected void environment(String key, String value) {
		log.info("[Environment] '" + key + "' -> '" + value + "'");
	}

	/**
	 * Called to indicate a system key was found.
	 * @param key the key.
	 * @param value the value for the key.
	 */
	protected void system(String key, String value) {
		log.info("[System] '" + key + "' -> '" + value + "'");
	}

}
