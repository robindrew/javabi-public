package com.javabi.common.lang.properties.parser;

import java.util.Properties;

public interface IResourcePropertyParser extends IPropertyParser {

	Properties parseProperties(String resourceName);

}
