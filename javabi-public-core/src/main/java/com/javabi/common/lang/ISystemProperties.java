package com.javabi.common.lang;

public interface ISystemProperties {

	String get(String key);

	String get(String key, boolean optional);

	String get(String key, String defaultValue);

	String getOperatingSystem();

	String getUserName();

	String getTimeZone();

	String getFileEncoding();

	String getFileSeparator();

	String getPathSeparator();

	String getLineSeparator();

	String getClassPath();

	String getLibraryPath();

	String getJavaVersion();

	String getJavaRuntimeVersion();

	String getWorkingDirectory();

	void set(String key, String value);
}