package com.javabi.common.lang;

import java.net.URL;

import com.google.common.io.ByteSource;
import com.google.common.io.CharSource;
import com.google.common.io.Resources;
import com.javabi.common.io.stream.StreamInput;

public class Resource extends StreamInput implements IResource {

	private final String name;

	public Resource(String name) {
		if (name.isEmpty()) {
			throw new IllegalArgumentException("name is empty");
		}
		this.name = name;
	}

	public Resource(URL url) {
		this(url.getPath());
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public URL getURL() {
		URL url = Resources.getResource(name);
		if (url == null) {
			throw new IllegalStateException("Unable to resolve resource: " + getURL());
		}
		return url;
	}

	@Override
	public boolean exists() {
		try {
			getURL();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public String toString() {
		return name.toString();
	}

	@Override
	public CharSource asCharSource() {
		return Resources.asCharSource(getURL(), getCharset());
	}

	@Override
	public ByteSource asByteSource() {
		return Resources.asByteSource(getURL());
	}

}