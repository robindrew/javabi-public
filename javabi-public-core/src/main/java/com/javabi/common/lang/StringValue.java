package com.javabi.common.lang;

import java.math.BigDecimal;
import java.util.regex.Pattern;

/**
 * A versatile string wrapper.
 */
public final class StringValue extends Number implements IStringValue {

	/** The serialVersionUID. */
	private static final long serialVersionUID = 7377601235077203658L;

	/** The null string value. */
	public static final IStringValue NULL = new StringValue(null);

	/** The value. */
	private final String value;

	/**
	 * Creates a new string value.
	 * @param value the value.
	 */
	public StringValue(String value) {
		this.value = value;
	}

	@Override
	public char charAt(int index) {
		return value.charAt(index);
	}

	@Override
	public int length() {
		return value.length();
	}

	@Override
	public CharSequence subSequence(int start, int end) {
		return value.subSequence(start, end);
	}

	@Override
	public String toString() {
		return value;
	}

	/**
	 * Returns true if this value is null.
	 * @return true if this value is null.
	 */
	@Override
	public boolean isNull() {
		return value == null;
	}

	/**
	 * Returns this as a string.
	 * @return this as a string.
	 */
	@Override
	public String get() {
		return value;
	}

	/**
	 * Returns this as a string.
	 * @param defaultValue the value to return if null.
	 * @return this as a string.
	 */
	@Override
	public String get(String defaultValue) {
		if (value == null) {
			return defaultValue;
		}
		return value;
	}

	/**
	 * Returns this as a {@link Pattern}.
	 * @return this as a {@link Pattern}.
	 */
	@Override
	public Pattern pattern() {
		return Pattern.compile(value);
	}

	/**
	 * Returns this as a {@link Pattern}.
	 * @param defaultValue the value to return if null.
	 * @return this as a {@link Pattern}.
	 */
	@Override
	public Pattern pattern(Pattern defaultValue) {
		if (value == null) {
			return defaultValue;
		}
		return Pattern.compile(value);
	}

	/**
	 * Returns the {@link String} value of this string.
	 * @return the {@link String} value of this string.
	 */
	@Override
	public String stringValue() {
		if (value == null) {
			throw new NullPointerException();
		}
		return value;
	}

	/**
	 * Returns the {@link String} value of this string.
	 * @param defaultValue the default value if this is null.
	 * @return the {@link String} value of this string.
	 */
	@Override
	public String stringValue(String defaultValue) {
		if (value == null) {
			return defaultValue;
		}
		return value;
	}

	/**
	 * Returns the boolean value of this string.
	 * @return the boolean value of this string.
	 */
	@Override
	public boolean booleanValue() {
		return Boolean.parseBoolean(value);
	}

	/**
	 * Returns the boolean value of this string.
	 * @param defaultValue the default value.
	 * @return the boolean value of this string or the default value if not a boolean.
	 */
	@Override
	public boolean booleanValue(boolean defaultValue) {
		if (value == null) {
			return defaultValue;
		}
		if (value.equalsIgnoreCase("true")) {
			return true;
		}
		if (value.equalsIgnoreCase("false")) {
			return false;
		}
		return defaultValue;
	}

	@Override
	public byte byteValue() {
		return Byte.parseByte(value);
	}

	/**
	 * Returns the byte value of this string.
	 * @param defaultValue the default value.
	 * @return the byte value of this string or the default value if not a byte.
	 */
	@Override
	public byte byteValue(byte defaultValue) {
		if (value == null) {
			return defaultValue;
		}
		return byteValue();
	}

	@Override
	public short shortValue() {
		return Short.parseShort(value);
	}

	/**
	 * Returns the short value of this string.
	 * @param defaultValue the default value.
	 * @return the short value of this string or the default value if not a short.
	 */
	@Override
	public short shortValue(short defaultValue) {
		if (value == null) {
			return defaultValue;
		}
		return shortValue();
	}

	@Override
	public int intValue() {
		return Integer.parseInt(value);
	}

	/**
	 * Returns the integer value of this string.
	 * @param defaultValue the default value.
	 * @return the integer value of this string or the default value if not an integer.
	 */
	@Override
	public int intValue(int defaultValue) {
		if (value == null) {
			return defaultValue;
		}
		return intValue();
	}

	@Override
	public long longValue() {
		return Long.parseLong(value);
	}

	/**
	 * Returns the long value of this string.
	 * @param defaultValue the default value.
	 * @return the long value of this string or the default value if not a long.
	 */
	@Override
	public long longValue(long defaultValue) {
		if (value == null) {
			return defaultValue;
		}
		return longValue();
	}

	@Override
	public float floatValue() {
		return Float.parseFloat(value);
	}

	/**
	 * Returns the float value of this string.
	 * @param defaultValue the default value.
	 * @return the float value of this string or the default value if not a float.
	 */
	@Override
	public float floatValue(float defaultValue) {
		if (value == null) {
			return defaultValue;
		}
		return floatValue();
	}

	@Override
	public double doubleValue() {
		return Double.parseDouble(value);
	}

	/**
	 * Returns the double value of this string.
	 * @param defaultValue the default value.
	 * @return the double value of this string or the default value if not a double.
	 */
	@Override
	public double doubleValue(double defaultValue) {
		if (value == null) {
			return defaultValue;
		}
		return doubleValue();
	}

	/**
	 * Returns the enum value of this string.
	 * @param clazz the enumeration class.
	 * @return the enum value of this string or the default value if not an enum.
	 */
	@Override
	public <E extends Enum<E>> E enumValue(Class<E> clazz) {
		return Enum.valueOf(clazz, value);
	}

	/**
	 * Returns the enum value of this string.
	 * @param defaultValue the default value.
	 * @return the enum value of this string or the default value if not an enum.
	 */
	@Override
	public <E extends Enum<E>> E enumValue(E defaultValue) {
		if (value == null) {
			return defaultValue;
		}
		return enumValue(defaultValue.getDeclaringClass());
	}

	@Override
	public BigDecimal decimalValue() {
		return new BigDecimal(value);
	}

	@Override
	public int hashCode() {
		return get() == null ? 0 : get().hashCode();
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object instanceof StringValue) {
			IStringValue value = (IStringValue) object;
			return equals(get(), value.get());
		}
		return false;
	}

	/**
	 * Returns true if the given values are equal.
	 * @param value1 the first value.
	 * @param value2 the second value.
	 * @return true if the given values are equal.
	 */
	private boolean equals(String value1, String value2) {
		if (value1 == value2) {
			return true;
		}
		if (value1 == null || value2 == null) {
			return false;
		}
		return value1.equals(value2);
	}

	@Override
	public int compareTo(IStringValue value) {
		String value1 = this.get();
		String value2 = value.get();
		if (value1 == null) {
			return -1;
		}
		if (value2 == null) {
			return 1;
		}
		if (value1.equals(value2)) {
			return 0;
		}
		return value1.compareTo(value2);
	}

}
