package com.javabi.common.lang.properties.value;

import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;

import com.javabi.common.lang.Variables;
import com.javabi.common.lang.properties.AbstractProperty;
import com.javabi.common.lang.properties.PropertyException;

/**
 * A Double Property.
 */
public class EnumProperty<E extends Enum<E>> extends AbstractProperty<E> {

	/** The enum class. */
	private final Class<E> enumClass;
	/** The valid values. */
	private volatile EnumSet<E> validValues = null;
	/** The invalid values. */
	private volatile EnumSet<E> invalidValues = null;

	public EnumProperty(Class<E> enumClass, String... keys) {
		super(keys);
		Variables.notNull("enumClass", enumClass);
		this.enumClass = enumClass;
	}

	public EnumProperty(Class<E> enumClass, Collection<String> keys) {
		super(keys);
		Variables.notNull("enumClass", enumClass);
		this.enumClass = enumClass;
	}

	public EnumProperty<E> valid(Collection<E> values) {
		this.validValues = EnumSet.copyOf(values);
		return this;
	}

	public EnumProperty<E> invalid(Collection<E> values) {
		this.invalidValues = EnumSet.copyOf(values);
		return this;
	}

	@SuppressWarnings("unchecked")
	public EnumProperty<E> valid(E... values) {
		return valid(Arrays.asList(values));
	}

	@SuppressWarnings("unchecked")
	public EnumProperty<E> invalid(E... values) {
		return invalid(Arrays.asList(values));
	}

	@Override
	protected synchronized E parseValue(String key, String value) {
		value = value.trim();
		E enumValue = Enum.valueOf(enumClass, value);

		// Check Valid Values
		if (validValues != null) {
			for (E validValue : validValues) {
				if (enumValue.equals(validValue)) {
					return enumValue;
				}
			}
			throw new PropertyException(toString(key, value) + " is not a valid value: " + validValues);
		}

		// Check Invalid Values
		if (invalidValues != null) {
			for (E invalidValue : invalidValues) {
				if (enumValue.equals(invalidValue)) {
					throw new PropertyException(toString(key, value) + " is an invalid value: " + invalidValues);
				}
			}
		}

		return enumValue;
	}

}
