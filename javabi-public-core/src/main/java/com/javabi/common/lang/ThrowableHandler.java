package com.javabi.common.lang;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThrowableHandler implements IThrowableHandler {

	private static final Logger log = LoggerFactory.getLogger(ThrowableHandler.class);

	@Override
	public void uncaughtException(Thread thread, Throwable throwable) {
		log.error("Exception not caught by thread: " + thread, throwable);
		handle(throwable);
	}

	@Override
	public void handle(Throwable throwable) {

		// Remember that the same throwable may be handled many times
		while (throwable != null) {
			check(throwable);
			throwable = throwable.getCause();
		}
	}

	protected boolean check(Throwable throwable) {

		// If the application has run out of memory, exit cleanly (if possible)
		if (throwable instanceof OutOfMemoryError) {
			exit(1, "The application ran out of memory", throwable);
			return true;
		}

		return false;
	}

	protected void exit(int exitStatus, String description, Throwable throwable) {
		if (SystemExit.exit(exitStatus, throwable)) {
			if (log != null) {
				log.error(description, throwable);
			}
			throwable.printStackTrace();
		}
	}

	public void setDefaultUncaughtExceptionHandler() {
		Thread.setDefaultUncaughtExceptionHandler(this);
	}

}
