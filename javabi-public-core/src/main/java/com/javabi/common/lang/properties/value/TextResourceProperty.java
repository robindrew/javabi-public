package com.javabi.common.lang.properties.value;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Collection;

import com.google.common.base.Charsets;
import com.google.common.base.Throwables;
import com.google.common.io.CharStreams;
import com.javabi.common.lang.Quietly;
import com.javabi.common.lang.properties.AbstractProperty;

/**
 * A String Property.
 */
public class TextResourceProperty extends AbstractProperty<String> {

	private Charset charset = Charsets.UTF_8;

	/**
	 * Creates the property.
	 * @param keys the keys.
	 */
	public TextResourceProperty(String... keys) {
		super(keys);
	}

	/**
	 * Creates the property.
	 * @param keys the keys.
	 */
	public TextResourceProperty(Collection<String> keys) {
		super(keys);
	}

	public TextResourceProperty charset(String charsetName) {
		this.charset = Charset.forName(charsetName);
		return this;
	}

	@Override
	protected synchronized String parseValue(String key, String value) {
		InputStream input = getClass().getResourceAsStream(value);
		if (input == null) {
			throw new IllegalStateException("Resource not found: '" + value + "'");
		}
		try {
			InputStreamReader reader = new InputStreamReader(input, charset);
			return CharStreams.toString(reader);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		} finally {
			Quietly.close(input);
		}
	}

}
