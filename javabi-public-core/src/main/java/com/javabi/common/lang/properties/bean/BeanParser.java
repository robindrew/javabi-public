package com.javabi.common.lang.properties.bean;

import static com.javabi.common.dependency.DependencyFactory.getDependency;
import static com.javabi.common.lang.Variables.notEmpty;

import com.javabi.common.lang.properties.IPropertyMap;
import com.javabi.common.lang.properties.PropertyException;
import com.javabi.common.lang.reflect.field.IInstanceField;
import com.javabi.common.lang.reflect.field.InstanceField;
import com.javabi.common.text.parser.StringParserMap;

public class BeanParser<B> {

	/** The bean. */
	private final B bean;
	/** The bean class. */
	private final Class<B> beanClass;
	/** The key prefix. */
	private final String keyPrefix;

	@SuppressWarnings("unchecked")
	public BeanParser(B bean, String keyPrefix) {
		this.bean = bean;
		this.beanClass = (Class<B>) bean.getClass();
		this.keyPrefix = notEmpty("keyPrefix", keyPrefix);
	}

	public void parseBean() {
		for (IInstanceField field : InstanceField.getInstanceFields(bean)) {
			if (!field.isStatic() && !field.isStatic()) {
				parseField(field);
			}
		}
	}

	private void parseField(IInstanceField field) {
		String name = field.getName();

		// Property
		IPropertyMap factory = getDependency(IPropertyMap.class);
		String key = keyPrefix + "." + name;
		String value = factory.getString(key);
		if (value == null) {
			throw new PropertyException("Missing mandatory property: '" + key + "' for field '" + name + "' in class " + beanClass.getName());
		}

		// Parse
		StringParserMap map = new StringParserMap();
		Object parsed = map.parse(field.getType(), value);
		field.setValue(parsed);
	}

}
