package com.javabi.common.lang.properties;

import java.util.List;

public interface IListProperty<V> extends IProperty<List<V>> {

	V[] toArray();

}
