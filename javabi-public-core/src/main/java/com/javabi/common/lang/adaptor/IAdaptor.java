package com.javabi.common.lang.adaptor;

/**
 * An adaptor from one object to another.
 * @param <F> the from type.
 * @param <T> the to type.
 */
public interface IAdaptor<F, T> {

	/**
	 * Adapt from one object to another.
	 * @param from the object to adapt from.
	 * @param to the object to adapt to.
	 */
	void adapt(F from, T to);

}
