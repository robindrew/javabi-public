package com.javabi.common.commandline;

import static com.javabi.common.text.StringsOld.escape;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.javabi.common.lang.ByteUnit;

public class CommandLineBuilder implements ICommandLineBuilder {

	private static final String getSymbol(ByteUnit unit) {
		switch (unit) {
			case BYTES:
				return "";
			case KILOBYTES:
				return "k";
			case MEGABYTES:
				return "m";
			case GIGABYTES:
				return "g";
			default:
				throw new IllegalArgumentException("Unit not supported: " + unit);
		}
	}

	private final Set<String> options = new LinkedHashSet<String>();
	private final List<String> arguments = new ArrayList<String>();
	private String className = null;
	private String classpath = null;

	public Set<String> getOptions() {
		return options;
	}

	public List<String> getArguments() {
		return arguments;
	}

	public String getClassName() {
		return className;
	}

	public String getClasspath() {
		return classpath;
	}

	@Override
	public ICommandLineBuilder addOption(String option) {
		if (option.isEmpty()) {
			throw new IllegalArgumentException("option is empty");
		}
		options.add(option);
		return this;
	}

	@Override
	public ICommandLineBuilder addArgument(String argument) {
		if (argument.isEmpty()) {
			throw new IllegalArgumentException("argument is empty");
		}
		arguments.add(argument);
		return this;
	}

	@Override
	public ICommandLineBuilder setClassName(String className) {
		if (className.isEmpty()) {
			throw new IllegalArgumentException("className is empty");
		}
		this.className = className;
		return this;
	}

	@Override
	public ICommandLineBuilder setClasspath(String classpath) {
		if (classpath.isEmpty()) {
			throw new IllegalArgumentException("classpath is empty");
		}
		this.classpath = classpath;
		return this;
	}

	@Override
	public ICommandLineBuilder setSystemProperty(String key, String value) {
		if (key == null) {
			throw new NullPointerException("key");
		}
		return addOption("-D" + key + "=" + escape(value, '\"'));
	}

	@Override
	public ICommandLineBuilder setXmx(long value, ByteUnit unit) {
		if (value <= 0) {
			throw new IllegalArgumentException("value=" + value);
		}
		if (unit == null) {
			throw new NullPointerException("unit");
		}
		return addOption("-Xmx" + value + getSymbol(unit));
	}

	@Override
	public ICommandLineBuilder setXms(long value, ByteUnit unit) {
		if (value <= 0) {
			throw new IllegalArgumentException("value=" + value);
		}
		if (unit == null) {
			throw new NullPointerException("unit");
		}
		return addOption("-Xms" + value + getSymbol(unit));
	}

	@Override
	public ICommandLineBuilder setXss(long value, ByteUnit unit) {
		if (value <= 0) {
			throw new IllegalArgumentException("value=" + value);
		}
		if (unit == null) {
			throw new NullPointerException("unit");
		}
		return addOption("-Xss" + value + getSymbol(unit));
	}

	@Override
	public ICommandLineBuilder setJdwp(Jdwp jdwp) {
		return addOption(jdwp.toString());
	}

	@Override
	public ICommandLineBuilder setDebug() {
		return addOption("-Xdebug");
	}

	@Override
	public ICommandLineBuilder setPrintGc() {
		return addOption("-XX:+PrintGC");
	}

	@Override
	public ICommandLineBuilder setPrintGcDetails() {
		return addOption("-XX:+PrintGCDetails");
	}

	@Override
	public ICommandLineBuilder setPrintGcTimeStamps() {
		return addOption("-XX:+PrintGCTimeStamps");
	}

	@Override
	public ICommandLineBuilder setPrintGcDateStamps() {
		return addOption("-XX:+PrintGCDateStamps");
	}

	@Override
	public ICommandLineBuilder setPrintGcApplicationStoppedTime() {
		return addOption("-XX:+PrintGCApplicationStoppedTime");
	}

	@Override
	public ICommandLineBuilder setPrintGcApplicationConcurrentTime() {
		return addOption("-XX:+PrintGCApplicationConcurrentTime");
	}

	@Override
	public ICommandLineBuilder setPrintFlagsFinal() {
		return addOption("-XX:+PrintFlagsFinal");
	}

	@Override
	public ICommandLineBuilder setLogGc(String filename) {
		if (filename.isEmpty()) {
			throw new IllegalArgumentException("filename is empty");
		}
		return addOption("-Xloggc:" + filename);
	}
}
