package com.javabi.common.commandline;

import com.javabi.common.lang.ByteUnit;

public interface ICommandLineBuilder {

	ICommandLineBuilder setClassName(String className);

	ICommandLineBuilder setClasspath(String classpath);

	ICommandLineBuilder setSystemProperty(String key, String value);

	ICommandLineBuilder setXmx(long value, ByteUnit unit);

	ICommandLineBuilder setXms(long value, ByteUnit unit);

	ICommandLineBuilder setXss(long value, ByteUnit unit);

	ICommandLineBuilder setJdwp(Jdwp jdwp);

	ICommandLineBuilder setDebug();

	ICommandLineBuilder setPrintGc();

	ICommandLineBuilder setPrintGcDetails();

	ICommandLineBuilder setPrintGcTimeStamps();

	ICommandLineBuilder setPrintGcDateStamps();

	ICommandLineBuilder setPrintGcApplicationStoppedTime();

	ICommandLineBuilder setPrintGcApplicationConcurrentTime();

	ICommandLineBuilder setPrintFlagsFinal();

	ICommandLineBuilder setLogGc(String filename);

	ICommandLineBuilder addOption(String option);

	ICommandLineBuilder addArgument(String argument);

}
