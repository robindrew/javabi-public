package com.javabi.common.commandline;

public class Jdwp {

	private boolean agent = true;
	private String transport = "dt_socket";
	private int port = 0;
	private boolean suspend = true;
	private boolean server = true;
	private String host = null;

	public boolean isAgent() {
		return agent;
	}

	public String getTransport() {
		return transport;
	}

	public int getPort() {
		return port;
	}

	public boolean isSuspend() {
		return suspend;
	}

	public boolean isServer() {
		return server;
	}

	public String getHost() {
		return host;
	}

	public void setAgent(boolean agent) {
		this.agent = agent;
	}

	public void setTransport(String transport) {
		this.transport = transport;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public void setSuspend(boolean suspend) {
		this.suspend = suspend;
	}

	public void setServer(boolean server) {
		this.server = server;
	}

	public void setHost(String host) {
		this.host = host;
	}

	@Override
	public String toString() {

		StringBuilder jdwp = new StringBuilder();
		if (agent) {
			jdwp.append("-agentlib:jdwp=");
		} else {
			jdwp.append("-xrunjdwp:");
		}

		// Options
		jdwp.append("transport=").append(transport);
		jdwp.append(",server=").append(server ? "y" : "n");
		jdwp.append(",suspend=").append(suspend ? "y" : "n");
		jdwp.append(",address=");
		if (agent && host != null) {
			jdwp.append(host).append(":");
		}
		jdwp.append(port);

		return jdwp.toString();
	}

}
