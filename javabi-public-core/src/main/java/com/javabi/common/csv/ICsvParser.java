package com.javabi.common.csv;

import java.io.File;
import java.io.Reader;
import java.util.List;

public interface ICsvParser<E> {

	void setDelimiter(char delimiter);

	char getDelimiter();

	void setEscape(char escape);

	char getEscape();

	void setQuote(char quote);

	char getQuote();

	void setSkipEquals(boolean skipEquals);

	boolean isSkipEquals();

	List<E> parse(File file);

	List<E> parse(Reader reader);

	List<E> parse(String text);

	E parseLine(List<String> line, int lineNumber);

}
