package com.javabi.common.csv;

import java.util.List;

import com.google.common.io.LineProcessor;

public interface ICsvLineProcessor extends LineProcessor<List<List<String>>> {

	void setSkipBlankLines(boolean skipBlankLines);

	boolean isSkipBlankLines();

	void setSkipEquals(boolean skipEquals);

	boolean isSkipEquals();

	void setDelimiter(char delimiter);

	char getDelimiter();

	void setEscape(char escape);

	char getEscape();

	void setQuote(char quote);

	char getQuote();
}
