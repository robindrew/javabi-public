package com.javabi.common.csv;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CsvLineProcessor implements ICsvLineProcessor {

	private char delimiter = ',';
	private char escape = '\\';
	private char quote = '\"';
	private boolean skipEquals = true;
	private boolean skipBlankLines = true;

	private final List<List<String>> resultList = new ArrayList<List<String>>();

	@Override
	public void setSkipBlankLines(boolean skipBlankLines) {
		this.skipBlankLines = skipBlankLines;
	}

	@Override
	public boolean isSkipBlankLines() {
		return skipBlankLines;
	}

	@Override
	public void setSkipEquals(boolean skipEquals) {
		this.skipEquals = skipEquals;
	}

	@Override
	public boolean isSkipEquals() {
		return skipEquals;
	}

	@Override
	public void setDelimiter(char delimiter) {
		this.delimiter = delimiter;
	}

	@Override
	public char getDelimiter() {
		return delimiter;
	}

	@Override
	public void setEscape(char escape) {
		this.escape = escape;
	}

	@Override
	public char getEscape() {
		return escape;
	}

	@Override
	public void setQuote(char quote) {
		this.quote = quote;
	}

	@Override
	public char getQuote() {
		return quote;
	}

	@Override
	public List<List<String>> getResult() {
		return resultList;
	}

	@Override
	public boolean processLine(String line2) throws IOException {
		List<String> elementList = new ArrayList<String>();
		StringBuilder element = new StringBuilder();

		boolean quoted = false;
		boolean escaped = false;
		boolean newElement = true;
		for (int i = 0; i < line2.length(); i++) {
			int read = line2.charAt(i);
			if (read == -1) {
				break;
			}

			// Skip equals?
			if (isSkipEquals() && newElement && read == '=') {
				continue;
			}
			newElement = false;

			// Escaped?
			if (escaped) {
				escaped = false;
				element.append((char) read);
				continue;
			}
			if (read == escape) {
				escaped = true;
				continue;
			}

			// Quoted?
			if (read == quote) {
				quoted = !quoted;
				continue;
			}
			if (quoted) {
				element.append((char) read);
				continue;
			}

			// Delimiter?
			if (read == delimiter) {
				elementList.add(element.toString());
				element.setLength(0);
				newElement = true;
				continue;
			}

			// End of line?
			if (read == '\r') {
				continue;
			}
			if (read == '\n') {
				break;
			}

			element.append((char) read);
		}

		// Final element
		elementList.add(element.toString());
		element.setLength(0);

		// Done
		resultList.add(elementList);
		return true;
	}

}
