package com.javabi.common.csv;

import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.mutable.MutableBoolean;

import com.google.common.base.Throwables;
import com.javabi.common.io.file.FileInput;
import com.javabi.common.io.stream.IStreamInput;
import com.javabi.common.lang.Quietly;

public abstract class CsvListParser<E> implements ICsvParser<E> {

	private char delimiter = ',';
	private char escape = '\\';
	private char quote = '\"';
	private boolean skipEquals = true;
	private boolean skipBlankLines = true;

	public void setSkipBlankLines(boolean skipBlankLines) {
		this.skipBlankLines = skipBlankLines;
	}

	public boolean isSkipBlankLines() {
		return skipBlankLines;
	}

	@Override
	public void setSkipEquals(boolean skipEquals) {
		this.skipEquals = skipEquals;
	}

	@Override
	public boolean isSkipEquals() {
		return skipEquals;
	}

	@Override
	public void setDelimiter(char delimiter) {
		this.delimiter = delimiter;
	}

	@Override
	public char getDelimiter() {
		return delimiter;
	}

	@Override
	public void setEscape(char escape) {
		this.escape = escape;
	}

	@Override
	public char getEscape() {
		return escape;
	}

	@Override
	public void setQuote(char quote) {
		this.quote = quote;
	}

	@Override
	public char getQuote() {
		return quote;
	}

	@Override
	public List<E> parse(File file) {
		IStreamInput input = new FileInput(file);
		Reader reader = input.newBufferedReader();
		try {
			return parse(reader);
		} finally {
			Quietly.close(reader);
		}
	}

	@Override
	public List<E> parse(String text) {
		return parse(new StringReader(text));
	}

	@Override
	public List<E> parse(Reader reader) {
		try {
			List<E> list = new ArrayList<E>();

			// First row is the header
			int number = 1;
			MutableBoolean finished = new MutableBoolean(false);
			while (!finished.booleanValue()) {
				List<String> line = readLine(reader, finished);

				// Blank line?
				if (skipBlankLines && isBlankLine(line)) {
					continue;
				}

				E element = parseLine(line, number++);
				if (element != null) {
					list.add(element);
				}
			}

			return list;
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	private boolean isBlankLine(List<String> line) {
		return line.size() == 1 && line.get(0).trim().isEmpty();
	}

	private List<String> readLine(Reader reader, MutableBoolean finished) throws Exception {
		List<String> list = new ArrayList<String>();
		StringBuilder line = new StringBuilder();

		boolean quoted = false;
		boolean escaped = false;
		boolean newElement = true;
		while (true) {
			int read = reader.read();
			if (read == -1) {
				finished.setValue(true);
				break;
			}

			// Skip equals?
			if (isSkipEquals() && newElement && read == '=') {
				continue;
			}
			newElement = false;

			// Escaped?
			if (escaped) {
				escaped = false;
				line.append((char) read);
				continue;
			}
			if (read == escape) {
				escaped = true;
				continue;
			}

			// Quoted?
			if (read == quote) {
				quoted = !quoted;
				continue;
			}
			if (quoted) {
				line.append((char) read);
				continue;
			}

			// Delimiter?
			if (read == delimiter) {
				list.add(line.toString());
				line.setLength(0);
				newElement = true;
				continue;
			}

			// End of line?
			if (read == '\r') {
				continue;
			}
			if (read == '\n') {
				break;
			}

			line.append((char) read);
		}

		// Final element
		list.add(line.toString());
		line.setLength(0);

		return list;
	}

}
