package com.javabi.common.csv;

import java.util.List;

import com.javabi.common.collect.StringListMap;

/**
 * A CSV parser that assumes the first available line is a header.
 */
public abstract class CsvHeaderListParser<E> extends CsvListParser<E> {

	private StringListMap headers = null;

	@Override
	public E parseLine(List<String> line, int lineNumber) {
		if (lineNumber == 1) {
			handleHeader(line);
			return null;
		} else {
			return parseElement(line, lineNumber);
		}
	}

	protected void handleHeader(List<String> line) {
		if (line.isEmpty()) {
			throw new IllegalStateException("header line is empty");
		}
		this.headers = new StringListMap(line);
	}

	public int headers() {
		return headers.size();
	}

	public StringListMap getHeaders() {
		return headers;
	}

	public int getInt(String key, List<String> line, int defaultValue) {
		String value = get(key, line);
		if (value == null || value.isEmpty()) {
			return defaultValue;
		}
		return Integer.parseInt(value.trim());
	}

	public String get(String header, List<String> line) {
		return getHeaders().elementOf(header, line);
	}

	public String get(String header, List<String> line, String defaultValue) {
		String value = get(header, line);
		if (value == null || value.isEmpty()) {
			return defaultValue;
		}
		return value;
	}

	protected abstract E parseElement(List<String> line, int lineNumber);

}
