package com.javabi.common.management.bean;

public interface IBeanAttribute extends Comparable<IBeanAttribute> {

	IBean getBean();

	String getName();

	Object getValue();

	Class<?> getType();

}
