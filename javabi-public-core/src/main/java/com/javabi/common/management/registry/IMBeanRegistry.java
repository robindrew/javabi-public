package com.javabi.common.management.registry;

public interface IMBeanRegistry {

	void register(Object object, String name);

	void register(Object object);

}
