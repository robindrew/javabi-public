package com.javabi.common.management.bean;

public interface IBeanParameter {

	int getIndex();

	Class<?> getType();

	String getTypeName();

	String getName();

	String getDescription();

}
