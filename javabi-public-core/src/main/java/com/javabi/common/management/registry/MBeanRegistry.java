package com.javabi.common.management.registry;

import java.lang.management.ManagementFactory;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;

public class MBeanRegistry implements IMBeanRegistry {

	private static final Logger log = LoggerFactory.getLogger(MBeanRegistry.class);

	private final Set<Object> objectSet = new CopyOnWriteArraySet<Object>();

	@Override
	public void register(Object object) {
		register(object, null);
	}

	@Override
	public void register(Object object, String name) {
		if (object == null) {
			throw new NullPointerException();
		}
		if (objectSet.add(object)) {
			try {
				mbean(object, name);
			} catch (Exception e) {
				Throwables.propagate(e);
			}
		}
	}

	private void mbean(Object object, String name) throws Exception {

		// Object Name
		String packageName = object.getClass().getPackage().getName();
		ObjectName objectName = new ObjectName(packageName + ":type=" + object.getClass().getSimpleName() + (name == null ? "" : (",name=" + name)));

		// Annotated MBean
		Object mbean = new AnnotatedMBean(object);

		// Register
		MBeanServer server = ManagementFactory.getPlatformMBeanServer();
		server.registerMBean(mbean, objectName);

		if (name == null) {
			log.info("Registered MBean: " + object);
		} else {
			log.info("Registered MBean: " + object + " (" + name + ")");
		}
	}

}
