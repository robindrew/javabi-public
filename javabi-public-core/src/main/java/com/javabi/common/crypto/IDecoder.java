package com.javabi.common.crypto;

public interface IDecoder {

	String decodeToString(byte[] data);

	byte[] decodeToBytes(byte[] data);

}
