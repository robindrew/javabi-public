package com.javabi.common.crypto;

public class Sha256Encoder extends MessageDigestEncoder {

	public Sha256Encoder() {
		super(SHA_256);
	}

}
