package com.javabi.common.crypto;

public class Sha512Encoder extends MessageDigestEncoder {

	public Sha512Encoder() {
		super(SHA_512);
	}

}
