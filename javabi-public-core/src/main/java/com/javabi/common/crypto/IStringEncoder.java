package com.javabi.common.crypto;

public interface IStringEncoder extends IObjectEncoder {

	String encodeToString(CharSequence text);

}
