package com.javabi.common.crypto;

public interface IObjectDecoder {

	Object decodeToObject(String text);

}
