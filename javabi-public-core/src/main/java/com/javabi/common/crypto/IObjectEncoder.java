package com.javabi.common.crypto;

public interface IObjectEncoder {

	String encodeToString(Object object);

}
