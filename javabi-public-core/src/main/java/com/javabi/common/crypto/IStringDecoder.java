package com.javabi.common.crypto;

public interface IStringDecoder extends IObjectDecoder {

	String decodeToString(CharSequence text);

}
