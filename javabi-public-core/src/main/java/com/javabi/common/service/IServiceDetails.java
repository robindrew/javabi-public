package com.javabi.common.service;

public interface IServiceDetails {

	String getServiceName();

	String getReleaseName();

	int getServicePort();
}
