package com.javabi.common.service.control;

import com.javabi.common.service.Status;

public class DelegatingControl implements IControl {

	private final IControl control;

	protected DelegatingControl(IControl control) {
		if (control == null) {
			throw new NullPointerException("control");
		}
		this.control = control;
	}

	public void reset() {
		control.reset();
	}

	public void startup() {
		control.startup();
	}

	public void shutdown() {
		control.shutdown();
	}

	public String getName() {
		return control.getName();
	}

	public Status getStatus() {
		return control.getStatus();
	}

	public boolean hasStatus(Status status) {
		return control.hasStatus(status);
	}

	public boolean hasStatus(Status... status) {
		return control.hasStatus(status);
	}

	public boolean isDaemon() {
		return control.isDaemon();
	}

	public Throwable getCrashCause() {
		return control.getCrashCause();
	}
}
