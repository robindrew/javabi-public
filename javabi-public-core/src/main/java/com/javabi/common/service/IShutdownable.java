package com.javabi.common.service;

public interface IShutdownable {

	void shutdown();

}
