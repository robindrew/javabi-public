package com.javabi.common.service;

public enum Status {

	/** Ready. */
	READY,
	/** Starting up. */
	STARTING_UP,
	/** Started up. */
	STARTED_UP,
	/** Shutting down. */
	SHUTTING_DOWN,
	/** Shutdown. */
	SHUTDOWN,
	/** Resetting. */
	RESETTING,
	/** Crashed. */
	CRASHED;

}
