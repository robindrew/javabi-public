package com.javabi.common.service;

public interface IStartupable {

	void startup();

}
