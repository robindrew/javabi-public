package com.javabi.common.service;

/**
 * Convenience exception thrown to indicate shutdown should be called and is intended.
 */
public class ShutdownException extends RuntimeException {

}
