package com.javabi.common.service.control;

import static com.javabi.common.lang.Variables.notEmpty;
import static com.javabi.common.lang.Variables.notNull;
import static com.javabi.common.service.Status.CRASHED;
import static com.javabi.common.service.Status.READY;
import static com.javabi.common.service.Status.RESETTING;
import static com.javabi.common.service.Status.SHUTDOWN;
import static com.javabi.common.service.Status.SHUTTING_DOWN;
import static com.javabi.common.service.Status.STARTED_UP;
import static com.javabi.common.service.Status.STARTING_UP;

import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.concurrent.copyonwrite.CopyOnWriteSet;
import com.javabi.common.date.timer.ITimer;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.common.service.Status;

public class Control implements IControl, ControlMBean {

	private static final Logger log = LoggerFactory.getLogger(Control.class);

	private static final Set<String> controlNameSet = new CopyOnWriteSet<String>();

	public static final void clearControlNames() {
		controlNameSet.clear();
	}

	private final String name;
	private final AtomicBoolean daemon = new AtomicBoolean(false);
	private final AtomicReference<Status> status = new AtomicReference<Status>(READY);
	private final AtomicReference<Throwable> crashCause = new AtomicReference<Throwable>();

	protected Control(String name) {
		this.name = notEmpty("name", name);

		if (!controlNameSet.add(name.toLowerCase())) {
			throw new IllegalStateException("Unable to create control: '" + name + "', another control already exists with this name");
		}
	}

	protected Control() {
		this.name = getClass().getSimpleName();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean isDaemon() {
		return daemon.get();
	}

	public void setDaemon(boolean daemon) {
		this.daemon.set(daemon);
	}

	protected void setStatus(Status status) {
		notNull("status", status);
		this.status.set(status);
	}

	protected void setStatus(Status expected, Status update) {
		notNull("expected", expected);
		notNull("update", update);
		if (!status.compareAndSet(expected, update)) {
			throw new IllegalStateException("control: '" + getName() + "', status=" + status + ", expected=" + expected + ", update=" + update);
		}
	}

	protected void setStatus(Status expected1, Status expected2, Status update) {
		notNull("expected1", expected1);
		notNull("expected2", expected2);
		notNull("update", update);
		if (!status.compareAndSet(expected1, update) && !status.compareAndSet(expected2, update)) {
			throw new IllegalStateException("control: '" + getName() + "', status=" + status + ", expected1=" + expected1 + ", expected2=" + expected2 + ", update=" + update);
		}
	}

	@Override
	public Status getStatus() {
		return status.get();
	}

	@Override
	public boolean hasStatus(Status status) {
		return this.status.get().equals(status);
	}

	@Override
	public boolean hasStatus(Status... statuses) {
		for (Status status : statuses) {
			if (hasStatus(status)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Throwable getCrashCause() {
		return crashCause.get();
	}

	protected void handleCrash(Throwable t) {
		t = Throwables.getRootCause(t);
		log.error("Control crashed: '" + getName() + "'", t);
		this.crashCause.set(t);
		Throwables.propagate(t);
	}

	@Override
	public void startup() {
		setStatus(READY, STARTING_UP);

		String type = getClass().getSimpleName();
		if (type.equals(getName())) {
			log.info("Starting up: '" + getName() + "'");
		} else {
			log.info("Starting up: '" + getName() + "' (" + getClass().getSimpleName() + ")");
		}

		ITimer timer = new NanoTimer();
		timer.start();
		try {
			startupControl();
			setStatus(STARTING_UP, STARTED_UP);
		} catch (Throwable t) {
			setStatus(CRASHED);
			handleCrash(t);
			return;
		}
		timer.stop();
		log.info("Started up: '" + getName() + "' in " + timer);
	}

	@Override
	public void shutdown() {

		// Ignore if already shutting down or shutdown
		if (hasStatus(SHUTTING_DOWN, SHUTDOWN)) {
			return;
		}

		// Check status
		setStatus(STARTED_UP, SHUTTING_DOWN);
		log.info("Shutting down: '" + getName() + "'");
		NanoTimer timer = new NanoTimer();
		timer.start();
		try {
			shutdownControl();
			setStatus(SHUTTING_DOWN, SHUTDOWN);
		} catch (Throwable t) {
			setStatus(CRASHED);
			handleCrash(t);
			return;
		}
		timer.stop();
		log.info("Shut down: '" + getName() + "' in " + timer);
	}

	@Override
	public void reset() {
		// Can reset from ready or shutdown
		setStatus(READY, SHUTDOWN, RESETTING);
		log.info("Resetting: '" + getName() + "'");
		NanoTimer timer = new NanoTimer();
		timer.start();
		try {
			Status resetStatus = resetControl();
			setStatus(RESETTING, resetStatus);
		} catch (Throwable t) {
			setStatus(CRASHED);
			handleCrash(t);
			return;
		}
		timer.stop();
		log.info("Reset: '" + getName() + "' in " + timer);
	}

	protected void startupControl() throws Throwable {
	}

	protected void shutdownControl() throws Throwable {
	}

	protected Status resetControl() throws Throwable {
		return READY;
	}

}
