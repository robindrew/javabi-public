package com.javabi.common.service.control;

public interface ControlMBean {

	void startup();

	void shutdown();

	String getName();

	boolean isDaemon();

}
