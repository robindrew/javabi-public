package com.javabi.common.service.control;

import com.javabi.common.service.IResettable;
import com.javabi.common.service.IShutdownable;
import com.javabi.common.service.IStartupable;
import com.javabi.common.service.Status;

public interface IControl extends IStartupable, IShutdownable, IResettable {

	String getName();

	Status getStatus();

	boolean hasStatus(Status status);

	boolean hasStatus(Status... status);

	boolean isDaemon();

	Throwable getCrashCause();

}
