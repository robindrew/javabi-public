package com.javabi.common.util.comparator;

import com.javabi.common.util.ReversableComparator;

public class ByteComparator extends ReversableComparator<Byte> {

	public ByteComparator(boolean reversed) {
		super(reversed);
	}

	public ByteComparator() {
	}

	@Override
	public int compare(Byte value1, Byte value2) {
		if (isReversed()) {
			return Byte.compare(value2, value1);
		} else {
			return Byte.compare(value1, value2);
		}
	}

}
