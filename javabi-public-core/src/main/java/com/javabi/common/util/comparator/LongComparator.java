package com.javabi.common.util.comparator;

import com.javabi.common.util.ReversableComparator;

public class LongComparator extends ReversableComparator<Long> {

	public LongComparator(boolean reversed) {
		super(reversed);
	}

	public LongComparator() {
	}

	@Override
	public int compare(Long value1, Long value2) {
		if (isReversed()) {
			return Long.compare(value2, value1);
		} else {
			return Long.compare(value1, value2);
		}
	}

}
