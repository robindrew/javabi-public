package com.javabi.common.util;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A Capacity-Limited Map.
 */
public class CapacityMap<K, V> extends LinkedHashMap<K, V> {

	private static final long serialVersionUID = -6168576132978864455L;

	/** The capacity. */
	private final int capacity;

	/**
	 * Creates a new PropertyCache.
	 * @param capacity
	 */
	public CapacityMap(int capacity) {
		this(capacity, 0.75f);
	}

	/**
	 * Creates a new PropertyCache.
	 * @param capacity the capacity.
	 * @param loadFactor the load factor.
	 */
	public CapacityMap(int capacity, float loadFactor) {
		super(capacity, loadFactor, true);
		if (capacity < 1) {
			throw new IllegalArgumentException("capacity=" + capacity);
		}
		this.capacity = capacity;
	}

	/**
	 * Returns true if this cache should remove it eldest entry.
	 * @param eldest the entry.
	 * @return true if this cache should remove it eldest entry.
	 */
	@Override
	protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
		return size() > capacity;
	}
}
