package com.javabi.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * An {@link ArrayList} backed set.
 * @param <E> the element type.
 */
public class ArraySet<E> extends ArrayList<E> implements Set<E> {

	/** The serialization id. */
	private static final long serialVersionUID = -1083618372003308990L;

	/**
	 * Creates a new array set.
	 */
	public ArraySet() {
	}

	/**
	 * Creates a new array set.
	 * @param initialCapacity the initial capacity of the underlying array.
	 */
	public ArraySet(int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * Creates a new array set.
	 * @param collection the collection add.
	 */
	public ArraySet(Collection<E> collection) {
		this(collection.size());
		addAll(collection);
	}

	/**
	 * Add the given element.
	 * @param element the element to add.
	 */
	public boolean add(E element) {
		if (contains(element)) {
			return false;
		}
		return super.add(element);
	}

	/**
	 * Add all elements from the given collection.
	 * @param collection the collection.
	 */
	public boolean addAll(Collection<? extends E> collection) {
		boolean modified = false;
		for (E element : collection) {
			if (add(element)) {
				modified = true;
			}
		}
		return modified;
	}
}
