package com.javabi.common.util.comparator;

import com.javabi.common.util.ReversableComparator;

public class FloatComparator extends ReversableComparator<Float> {

	public FloatComparator(boolean reversed) {
		super(reversed);
	}

	public FloatComparator() {
	}

	@Override
	public int compare(Float value1, Float value2) {
		if (isReversed()) {
			return Float.compare(value2, value1);
		} else {
			return Float.compare(value1, value2);
		}
	}

}
