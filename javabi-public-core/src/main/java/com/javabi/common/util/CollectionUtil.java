package com.javabi.common.util;

import java.util.Collection;
import java.util.Iterator;

/**
 * A collection of useful utility methods.
 */
// ESCA-JAVA0258: unexpected
@SuppressWarnings("rawtypes")
public final class CollectionUtil {

	/**
	 * Inaccessible constructor.
	 */
	private CollectionUtil() {
	}

	/** An empty iterator. */
	public static final Iterator EMPTY_ITERATOR = new Iterator() {

		@Override
		public final boolean hasNext() {
			return false;
		}

		@Override
		public final Object next() {
			throw new UnsupportedOperationException("iterator is empty");
		}

		@Override
		public final void remove() {
			throw new UnsupportedOperationException("iterator is empty");
		}

	};

	/** An empty iterable. */
	public static final Iterable EMPTY_ITERABLE = new Iterable() {

		@Override
		public final Iterator iterator() {
			return EMPTY_ITERATOR;
		}

	};

	/**
	 * Type safe way of getting an empty iterable.
	 * @param <T> the type.
	 * @return the empty iterable.
	 */
	@SuppressWarnings("unchecked")
	public static final <T> Iterable<T> emptyIterable() {
		return EMPTY_ITERABLE;
	}

	/**
	 * Type safe way of getting an empty iterator.
	 * @param <T> the type.
	 * @return the empty iterator.
	 */
	@SuppressWarnings("unchecked")
	public static final <T> Iterator<T> emptyIterator() {
		return EMPTY_ITERATOR;
	}

	/**
	 * Filter the given collection.
	 * @param <T> the element type.
	 * @param collection the collection.
	 * @param filter the filter.
	 * @return the number of elements filtered out.
	 */
	public static final <T> int filter(Collection<T> collection, Filter<T> filter) {
		int filtered = 0;
		Iterator<T> iterator = collection.iterator();
		while (iterator.hasNext()) {
			T element = iterator.next();
			if (filter.filter(element)) {
				iterator.remove();
				filtered++;
			}
		}
		return filtered;
	}

	/**
	 * Filter the given collection.
	 * @param <T> the element type.
	 * @param collection the collection.
	 * @param filters the filters.
	 * @return the number of elements filtered out.
	 */
	@SafeVarargs
	public static final <T> int filter(Collection<T> collection, Filter<T>... filters) {
		int filtered = 0;
		Iterator<T> iterator = collection.iterator();
		while (iterator.hasNext()) {
			T element = iterator.next();
			for (Filter<T> filter : filters) {
				if (filter.filter(element)) {
					iterator.remove();
					filtered++;
					break;
				}
			}
		}
		return filtered;
	}

}
