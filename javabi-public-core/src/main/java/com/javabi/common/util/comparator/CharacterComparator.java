package com.javabi.common.util.comparator;

import com.javabi.common.util.ReversableComparator;

public class CharacterComparator extends ReversableComparator<Character> {

	public CharacterComparator(boolean reversed) {
		super(reversed);
	}

	public CharacterComparator() {
	}

	@Override
	public int compare(Character value1, Character value2) {
		if (isReversed()) {
			return Character.compare(value2, value1);
		} else {
			return Character.compare(value1, value2);
		}
	}

}
