package com.javabi.common.util;

/**
 * A Transformable.
 * @param <T> the transformation type.
 */
public interface Transformable<T> {

	/**
	 * The type to transform to.
	 * @return the transformed object.
	 */
	T transform();

}
