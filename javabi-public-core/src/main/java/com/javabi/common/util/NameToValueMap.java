package com.javabi.common.util;

/**
 * A Polymorphic Map.
 */
public abstract class NameToValueMap {

	/**
	 * Returns the value for the named variable.
	 * @param name the name.
	 * @return the value.
	 */
	protected abstract Object getValue(String name);

	/**
	 * Returns the value for the named variable.
	 * @param name the name.
	 * @param value the value to put.
	 */
	protected abstract void putValue(String name, Object value);

	/**
	 * Remove the value for the named variable.
	 * @param name the name.
	 */
	protected abstract void removeValue(String name);

	/**
	 * Returns true if this contains the named value.
	 * @param name the name.
	 * @return true if this contains the named value.
	 */
	public boolean contains(String name) {
		return getValue(name) != null;
	}

	/**
	 * Remove the named value.
	 * @param name the name.
	 */
	public void remove(String name) {
		removeValue(name);
	}

	/**
	 * Put the named value in the map.
	 * @param name the name.
	 * @param value the value.
	 */
	public void put(String name, Object value) {
		putValue(name, value);
	}

	/**
	 * Returns the value for the named variable.
	 * @param name the name.
	 * @param defaultValue the default value.
	 * @return the value.
	 */
	@SuppressWarnings("unchecked")
	public <O> O get(String name, O defaultValue) {
		Object value = getValue(name);
		if (value == null) {
			return defaultValue;
		}
		return (O) value;
	}

	/**
	 * Returns the value for the named variable.
	 * @param name the name.
	 * @return the value.
	 */
	@SuppressWarnings("unchecked")
	public <O> O get(String name) {
		Object value = getValue(name);
		if (value == null) {
			throw new IllegalArgumentException("name: '" + name + "'");
		}
		return (O) value;
	}

	/**
	 * Returns the value for the named variable.
	 * @param name the name.
	 * @return the value.
	 */
	public int getInt(String name) {
		Object value = getValue(name);
		if (value == null) {
			throw new IllegalArgumentException("name: '" + name + "'");
		}
		if (value instanceof Number) {
			return ((Number) value).intValue();
		}
		return Integer.parseInt(value.toString().trim());
	}

	/**
	 * Returns the value for the named variable.
	 * @param name the name.
	 * @param defaultValue the default value.
	 * @return the value.
	 */
	public int getInt(String name, int defaultValue) {
		Object value = getValue(name);
		if (value == null) {
			return defaultValue;
		}
		if (value instanceof Number) {
			return ((Number) value).intValue();
		}
		return Integer.parseInt(value.toString().trim());
	}

	/**
	 * Returns the value for the named variable.
	 * @param name the name.
	 * @return the value.
	 */
	public long getLong(String name) {
		Object value = getValue(name);
		if (value == null) {
			throw new IllegalArgumentException("name: '" + name + "'");
		}
		if (value instanceof Number) {
			return ((Number) value).longValue();
		}
		return Long.parseLong(value.toString().trim());
	}

	/**
	 * Returns the value for the named variable.
	 * @param name the name.
	 * @param defaultValue the default value.
	 * @return the value.
	 */
	public long getLong(String name, long defaultValue) {
		Object value = getValue(name);
		if (value == null) {
			return defaultValue;
		}
		if (value instanceof Number) {
			return ((Number) value).longValue();
		}
		return Long.parseLong(value.toString().trim());
	}

	/**
	 * Returns the value for the named variable.
	 * @param name the name.
	 * @return the value.
	 */
	public boolean getBoolean(String name) {
		Object value = getValue(name);
		if (value == null) {
			throw new IllegalArgumentException("name: '" + name + "'");
		}
		if (value instanceof Boolean) {
			return (Boolean) value;
		}
		return Boolean.parseBoolean(value.toString().trim());
	}

	/**
	 * Returns the value for the named variable.
	 * @param name the name.
	 * @param defaultValue the default value.
	 * @return the value.
	 */
	public boolean getBoolean(String name, boolean defaultValue) {
		Object value = getValue(name);
		if (value == null) {
			return defaultValue;
		}
		if (value instanceof Boolean) {
			return (Boolean) value;
		}
		return Boolean.parseBoolean(value.toString().trim());
	}

	/**
	 * Returns the value for the named variable.
	 * @param <E> the enumeration type.
	 * @param name the name.
	 * @param enumClass the enumeration class.
	 * @return the enumeration.
	 */
	@SuppressWarnings("unchecked")
	public <E extends Enum<E>> E get(String name, Class<E> enumClass) {
		Object value = getValue(name);
		if (value instanceof Enum) {
			return (E) value;
		}
		return Enum.valueOf(enumClass, value.toString().trim());
	}

	/**
	 * Returns the value for the named variable.
	 * @param <E> the enumeration type.
	 * @param name the name.
	 * @param defaultValue the default value.
	 * @return the enumeration.
	 */
	@SuppressWarnings("unchecked")
	public <E extends Enum<E>> E get(String name, E defaultValue) {
		Object value = getValue(name);
		if (value == null) {
			return defaultValue;
		}
		if (value instanceof Enum) {
			return (E) value;
		}
		Class<E> enumClass = (Class<E>) defaultValue.getClass();
		return Enum.valueOf(enumClass, value.toString().trim());
	}
}
