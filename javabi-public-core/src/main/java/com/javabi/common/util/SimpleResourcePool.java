package com.javabi.common.util;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A Simple Resource Pool.
 */
public abstract class SimpleResourcePool<R, E extends Exception> implements ResourcePool<R, E> {

	/** The resource set. */
	private final Deque<R> resourceDeque;
	/** The semaphore. */
	private final Semaphore semaphore;

	/** Indicates if pool is closed. */
	private final AtomicBoolean closed = new AtomicBoolean(false);

	/**
	 * Create a new resource pool.
	 * @param size the pool size.
	 */
	protected SimpleResourcePool(int size) {
		if (size < 1) {
			throw new NullPointerException();
		}
		semaphore = new Semaphore(size);
		resourceDeque = new ArrayDeque<R>(size);
	}

	/**
	 * Creates and returns a new resource.
	 * @return a new resource.
	 */
	protected abstract R newResource() throws E;

	/**
	 * Returns true if the given resource is closed.
	 * @param resource the resource.
	 * @return true if closed.
	 */
	protected abstract boolean isClosed(R resource);

	/**
	 * Close the given resource.
	 * @param resource the resource.
	 */
	protected abstract void close(R resource);

	/**
	 * Handle an interruption.
	 * @param ie the exception.
	 */
	protected void handleInterruption(InterruptedException ie) {
		throw new RuntimeException(ie);
	}

	@Override
	public R lock() throws E {
		try {
			semaphore.acquire();
		} catch (InterruptedException ie) {
			handleInterruption(ie);
		}

		synchronized (resourceDeque) {

			// Closed?
			if (isClosed()) {
				semaphore.release();
				throw new IllegalStateException("pool is closed");
			}

			// Existing resource?
			if (!resourceDeque.isEmpty()) {
				return resourceDeque.pop();
			}
		}

		// New resource
		return newResource();
	}

	@Override
	public void unlock(R resource, boolean close) throws E {
		if (resource == null) {
			throw new NullPointerException();
		}

		// Push back on the stack
		synchronized (resourceDeque) {
			if (resourceDeque.contains(resource)) {
				throw new IllegalStateException("Attempt to unlock free resource");
			}

			// Closed?
			if (isClosed()) {
				close(resource);
			} else {

				// Close?
				if (close) {
					close(resource);
					resource = newResource();
				}

				// Push back on the stack
				resourceDeque.push(resource);
			}
		}

		// Release
		semaphore.release();
	}

	/**
	 * Returns true if closed.
	 * @return true if closed.
	 */
	public boolean isClosed() {
		return closed.get();
	}

	/**
	 * Close the resources in this pool.
	 */
	public void close() {
		synchronized (resourceDeque) {
			closed.set(true);

			// Close resources
			for (R resource : resourceDeque) {
				close(resource);
			}
			resourceDeque.clear();
		}
	}
}
