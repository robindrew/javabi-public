package com.javabi.common.util;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * A One-to-Many map.
 */
public class OneToManyMap<K, V> {

	private Map<K, Many<V>> map;

	/**
	 * Creates a new One-to-Many Map.
	 */
	public OneToManyMap() {
		this.map = new LinkedHashMap<K, Many<V>>();
	}

	/**
	 * Creates a new One-to-Many Map.
	 * @param map the map around which to wrap.
	 */
	@SuppressWarnings("unchecked")
	public OneToManyMap(Map<?, ?> map) {
		if (map == null) {
			throw new NullPointerException();
		}
		this.map = (Map<K, Many<V>>) map;
	}

	public int size() {
		return map.size();
	}

	public boolean isEmpty() {
		return map.isEmpty();
	}

	public boolean containsKey(K key) {
		return map.containsKey(key);
	}

	public Set<K> keySet() {
		return map.keySet();
	}

	public Set<Map.Entry<K, Many<V>>> entrySet() {
		return map.entrySet();
	}

	public Collection<Many<V>> values() {
		return map.values();
	}

	public Collection<V> remove(K key) {
		return map.remove(key);
	}

	public V get(K key) {
		Many<V> many = map.get(key);
		if (many == null) {
			return null;
		}
		V value = many.get(0);
		return value;
	}

	public Collection<V> getAll(K key) {
		Many<V> many = map.get(key);
		if (many == null) {
			return null;
		}
		return Collections.unmodifiableCollection(many);
	}

	public void put(K key, V value) {
		Many<V> many = map.get(key);
		if (many == null) {
			many = new Many<V>(value);
			map.put(key, many);
		} else {
			many.add(value);
		}
	}

	public void putAll(K key, Collection<V> values) {
		Many<V> many = map.get(key);
		for (V value : values) {
			if (many == null) {
				many = new Many<V>(value);
			} else {
				many.add(value);
			}
		}
	}

	public String toString() {
		return map.toString();
	}

	public static final class Many<V> extends AbstractCollection<V> {

		private final V value;
		private int size;
		private Many<V> next = null;

		public Many(V value) {
			this.value = value;
		}

		public V get(int index) {
			Many<V> many = this;
			while (index > 0) {
				many = many.next;
				index--;
			}
			return many.value;
		}

		@Override
		public int size() {
			return size;
		}

		@Override
		public boolean add(V value) {
			Many<V> many = this;
			while (many.next != null) {
				many = many.next;
			}
			many.next = new Many<V>(value);
			size++;
			return true;
		}

		@Override
		public Iterator<V> iterator() {
			return new ManyIterator<V>(this);
		}
	}

	private static final class ManyIterator<V> implements Iterator<V> {

		private Many<V> many;

		ManyIterator(Many<V> many) {
			// ESCA-JAVA0256: assignment not thread safe
			this.many = many;
		}

		@Override
		public boolean hasNext() {
			return many != null;
		}

		@Override
		public V next() {
			if (many == null) {
				throw new NoSuchElementException();
			}
			V value = many.value;
			many = many.next;
			return value;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

}
