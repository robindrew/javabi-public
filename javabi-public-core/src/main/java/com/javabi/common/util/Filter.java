package com.javabi.common.util;

/**
 * A Filter
 */
public interface Filter<F> {

	/**
	 * Returns true to filter out the given object.
	 * @param object the object.
	 * @return true to filter out the given object.
	 */
	boolean filter(F object);

}
