package com.javabi.common.util.comparator;

import com.javabi.common.util.ReversableComparator;

public class ShortComparator extends ReversableComparator<Short> {

	public ShortComparator(boolean reversed) {
		super(reversed);
	}

	public ShortComparator() {
	}

	@Override
	public int compare(Short value1, Short value2) {
		if (isReversed()) {
			return Short.compare(value2, value1);
		} else {
			return Short.compare(value1, value2);
		}
	}

}
