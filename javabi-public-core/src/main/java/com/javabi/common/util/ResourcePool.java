package com.javabi.common.util;

/**
 * A Resource Pool.
 * @param <R> the resource.
 * @param <E> the exception.
 */
public interface ResourcePool<R, E extends Exception> {

	/**
	 * Lock a resource in the pool and return it.
	 * @return the resource.
	 */
	R lock() throws E;

	/**
	 * Unlock the given resource in the pool.
	 * @param resource the resource to unlock.
	 * @param stale true to indicate the resource is stale.
	 */
	void unlock(R resource, boolean stale) throws E;

}
