package com.javabi.common.util;

public class Counter extends Number {

	private final long threshold;
	private long count = 0;

	public Counter(long threshold) {
		if (threshold < 0) {
			throw new IllegalArgumentException();
		}
		this.threshold = threshold;
	}

	public Counter() {
		this(0);
	}

	public boolean increment() {
		count++;
		if (threshold > 0) {
			return count % threshold == 0;
		}
		return false;
	}

	public long get() {
		return count;
	}

	@Override
	public String toString() {
		return String.valueOf(count);
	}

	@Override
	public int intValue() {
		return (int) count;
	}

	@Override
	public long longValue() {
		return count;
	}

	@Override
	public float floatValue() {
		return count;
	}

	@Override
	public double doubleValue() {
		return count;
	}

}
