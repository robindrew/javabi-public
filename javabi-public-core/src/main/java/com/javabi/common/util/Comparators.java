package com.javabi.common.util;

import com.javabi.common.util.comparator.ByteComparator;
import com.javabi.common.util.comparator.CharacterComparator;
import com.javabi.common.util.comparator.DoubleComparator;
import com.javabi.common.util.comparator.FloatComparator;
import com.javabi.common.util.comparator.IntegerComparator;
import com.javabi.common.util.comparator.LongComparator;
import com.javabi.common.util.comparator.ShortComparator;

public class Comparators {

	public static final ByteComparator BYTE = new ByteComparator();
	public static final ShortComparator SHORT = new ShortComparator();
	public static final IntegerComparator INTEGER = new IntegerComparator();
	public static final LongComparator LONG = new LongComparator();
	public static final FloatComparator FLOAT = new FloatComparator();
	public static final DoubleComparator DOUBLE = new DoubleComparator();
	public static final CharacterComparator CHARACTER = new CharacterComparator();

}
