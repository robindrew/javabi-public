package com.javabi.common.util.comparator;

import com.javabi.common.util.ReversableComparator;

public class IntegerComparator extends ReversableComparator<Integer> {

	public IntegerComparator(boolean reversed) {
		super(reversed);
	}

	public IntegerComparator() {
	}

	@Override
	public int compare(Integer value1, Integer value2) {
		if (isReversed()) {
			return Integer.compare(value2, value1);
		} else {
			return Integer.compare(value1, value2);
		}
	}

}
