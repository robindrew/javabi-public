package com.javabi.common.util;

import java.lang.reflect.Array;

/**
 * A collection of array utility methods.
 */
// ESCA-JAVA0259: desired constant
public final class ArrayUtil {

	/**
	 * Inaccessible constructor.
	 */
	private ArrayUtil() {
	}

	/**
	 * Convenience method to perform simple validation on an entire array
	 * @param <T> the type.
	 * @param array the array.
	 * @param minLength the minimum length of the array.
	 */
	public static <T> void validate(T[] array, int minLength) {
		if (array == null) {
			throw new NullPointerException();
		}
		if (array.length < minLength) {
			throw new IllegalArgumentException("length=" + array.length + " (minimum length=" + minLength + ")");
		}
		for (int i = 0; i < array.length; i++) {
			if (array[i] == null) {
				throw new NullPointerException("index=" + i);
			}
		}
	}

	/**
	 * Add elements to the given array.
	 * @param <T> the array type.
	 * @param array the array.
	 * @param elements the the elements to add.
	 * @return the new array.
	 */
	@SuppressWarnings("unchecked")
	public static final <T> T[] add(T[] array, T... elements) {
		if (elements == null) {
			throw new NullPointerException();
		}
		if (array == null) {
			return elements;
		}
		if (elements.length == 0) {
			return array;
		}
		T[] newArray = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length + elements.length);
		System.arraycopy(array, 0, newArray, 0, array.length);
		System.arraycopy(elements, 0, newArray, array.length, elements.length);
		return newArray;
	}

	/**
	 * Ensure the an array has a capacity.
	 * @param <T> the array type.
	 * @param array the array.
	 * @param capacity the capacity to ensure.
	 * @return the new array.
	 */
	@SuppressWarnings("unchecked")
	public static final <T> T[] ensureCapacity(T[] array, int capacity) {
		if (array == null) {
			throw new NullPointerException();
		}
		if (capacity < 0) {
			throw new IllegalArgumentException("capacity=" + capacity);
		}
		if (capacity <= array.length) {
			return array;
		}
		T[] newArray = (T[]) Array.newInstance(array.getClass().getComponentType(), capacity);
		System.arraycopy(array, 0, newArray, 0, array.length);
		return newArray;
	}

	/**
	 * Grow the given array by the given length.
	 * @param <T> the array type.
	 * @param array the array.
	 * @param extraElements the extra length.
	 * @param copyElements true to copy elements from the old to the new array.
	 * @return the new array.
	 */
	@SuppressWarnings("unchecked")
	public static final <T> T[] grow(T[] array, int extraElements, boolean copyElements) {
		if (array == null) {
			throw new NullPointerException();
		}
		if (extraElements < 1) {
			throw new IllegalArgumentException("extraElements=" + extraElements);
		}
		T[] newArray = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length + extraElements);
		if (copyElements) {
			System.arraycopy(array, 0, newArray, 0, array.length);
		}
		return newArray;
	}

	/**
	 * Returns true if the given array contains the given value.
	 * @param <T> the type.
	 * @param array the array.
	 * @param value the value.
	 * @return true if the given array contains the given value.
	 */
	public static final <T> boolean contains(T[] array, T value) {
		for (T element : array) {
			if (value == null) {
				if (element == null) {
					return true;
				}
			} else {
				if (value.equals(element)) {
					return true;
				}
			}
		}
		return false;
	}

}
