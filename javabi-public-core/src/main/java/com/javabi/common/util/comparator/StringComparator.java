package com.javabi.common.util.comparator;

import com.javabi.common.util.ReversableComparator;

public class StringComparator extends ReversableComparator<String> {

	public StringComparator(boolean reversed) {
		super(reversed);
	}

	public StringComparator() {
	}

	@Override
	public int compare(String value1, String value2) {
		if (isReversed()) {
			return compareStrings(value2, value1);
		} else {
			return compareStrings(value1, value2);
		}
	}

	public int compareStrings(String value1, String value2) {
		return value1.compareTo(value2);
	}

}
