package com.javabi.common.util.comparator;

import com.javabi.common.util.ReversableComparator;

public class DoubleComparator extends ReversableComparator<Double> {

	public DoubleComparator(boolean reversed) {
		super(reversed);
	}

	public DoubleComparator() {
	}

	@Override
	public int compare(Double value1, Double value2) {
		if (isReversed()) {
			return Double.compare(value2, value1);
		} else {
			return Double.compare(value1, value2);
		}
	}

}
