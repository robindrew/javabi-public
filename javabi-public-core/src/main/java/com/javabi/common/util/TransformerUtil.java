package com.javabi.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * An Object Transformer.
 */
public class TransformerUtil {

	/**
	 * Transform the given collection of objects to a list.
	 * @param collection the collection.
	 * @param list the translated list.
	 * @return the list of translated items.
	 */
	public static final <T> List<T> asList(Collection<? extends Transformable<T>> collection, List<T> list) {
		if (collection.isEmpty()) {
			return list;
		}
		for (Transformable<T> translatable : collection) {
			list.add(translatable.transform());
		}
		return list;
	}

	/**
	 * Transform the given collection of objects to a list.
	 * @param collection the collection.
	 * @return the list of translated items.
	 */
	public static final <T> List<T> asList(Collection<? extends Transformable<T>> collection) {
		if (collection.isEmpty()) {
			return Collections.emptyList();
		}
		List<T> translatedList = new ArrayList<T>(collection.size());
		for (Transformable<T> translatable : collection) {
			translatedList.add(translatable.transform());
		}
		return translatedList;
	}

	/**
	 * Inaccessible constructor.
	 */
	private TransformerUtil() {
	}
}
