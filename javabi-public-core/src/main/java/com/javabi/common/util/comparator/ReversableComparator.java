package com.javabi.common.util.comparator;

import java.util.Comparator;

public abstract class ReversableComparator<T> implements Comparator<T> {

	private final boolean reversed;

	protected ReversableComparator(boolean reversed) {
		this.reversed = reversed;
	}

	protected ReversableComparator() {
		this(false);
	}

	public boolean isReversed() {
		return reversed;
	}

}
