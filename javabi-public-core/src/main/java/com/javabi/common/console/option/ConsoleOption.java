package com.javabi.common.console.option;

public abstract class ConsoleOption implements IConsoleOption {

	private final String description;

	public ConsoleOption(String description) {
		if (description.isEmpty()) {
			throw new IllegalArgumentException("description is empty");
		}
		this.description = description;
	}

	@Override
	public String getDescription() {
		return description;
	}
}
