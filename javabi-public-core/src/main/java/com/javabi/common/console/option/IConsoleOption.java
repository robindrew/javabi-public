package com.javabi.common.console.option;

import com.javabi.common.console.IConsoleExecutable;

public interface IConsoleOption extends IConsoleExecutable {

	String getDescription();

}
