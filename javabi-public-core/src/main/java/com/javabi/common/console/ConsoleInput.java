package com.javabi.common.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.google.common.base.Charsets;
import com.google.common.base.Throwables;
import com.javabi.common.lang.Java;

public class ConsoleInput {

	private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in, Charsets.UTF_8));

	public static String readLine(String prompt) {
		if (prompt == null) {
			throw new NullPointerException("prompt");
		}

		if (!prompt.isEmpty()) {
			Java.print(prompt);
		}

		try {
			return reader.readLine();
		} catch (IOException ioe) {
			throw Throwables.propagate(ioe);
		}
	}

}
