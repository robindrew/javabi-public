package com.javabi.common.console;

@FunctionalInterface
public interface IConsoleExecutable {

	void execute();

}
