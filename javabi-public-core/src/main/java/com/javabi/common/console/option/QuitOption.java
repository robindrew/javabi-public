package com.javabi.common.console.option;

public class QuitOption extends ConsoleOption implements IQuitOption {

	public QuitOption(String description) {
		super(description);
	}

	public QuitOption() {
		super("Quit");
	}

	@Override
	public void execute() {
		// Nothing to do, special case to break out of menus
	}

}
