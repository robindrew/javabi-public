package com.javabi.common.console;

import com.javabi.common.console.option.ConsoleMenu;
import com.javabi.common.console.option.IConsoleOption;
import com.javabi.common.lang.Java;

public class ConsoleManager implements IConsoleExecutable {

	private final ConsoleMenu menu;

	public ConsoleManager(ConsoleMenu menu) {
		if (menu == null) {
			throw new NullPointerException("menu");
		}
		this.menu = menu;
	}

	public ConsoleManager() {
		this.menu = new ConsoleMenu("Main Menu");
	}

	@Override
	public void execute() {
		try {

			// Execute the top level menu
			getMenu().execute();

			// Cleanly quit
			quit();
		} catch (Throwable t) {

			// Crashed!
			crashed(t);
		}
	}

	protected void crashed(Throwable t) {
		t.printStackTrace();
		Java.exit(1);
	}

	protected void quit() {
		Java.exit(0);
	}

	public ConsoleMenu getMenu() {
		return menu;
	}

	protected void add(String description, IConsoleExecutable executable) {
		menu.add(description, executable);
	}

	protected void add(IConsoleOption option) {
		menu.add(option);
	}
}
