package com.javabi.common.console.option;

import static com.javabi.common.lang.Java.println;

import java.util.LinkedHashSet;
import java.util.Set;

import com.javabi.common.console.ConsoleInput;
import com.javabi.common.console.IConsoleExecutable;

public class ConsoleMenu implements IConsoleExecutable {

	private final String title;
	private final Set<IConsoleOption> options = new LinkedHashSet<>();

	public ConsoleMenu(String title) {
		if (title.isEmpty()) {
			throw new IllegalStateException("menu is empty");
		}
		this.title = title;
	}

	public void add(IConsoleOption option) {
		if (option == null) {
			throw new NullPointerException("option");
		}
		options.add(option);
	}

	public void add(String description, IConsoleExecutable executable) {
		add(new ExecutableConsoleOption(description, executable));
	}

	@Override
	public void execute() {
		if (options.isEmpty()) {
			throw new IllegalStateException("Menu '" + title + "' has no options");
		}

		while (true) {

			// 1. Select the option
			IConsoleOption option = selectOption();

			// 2. Execute the selected option
			option.execute();
			println();

			// Special case: if option is a quit option, execute then quit!
			if (option instanceof IQuitOption) {
				break;
			}
		}
	}

	private IConsoleOption selectOption() {
		while (true) {
			showOptions();

			println();
			String line = ConsoleInput.readLine("Enter Selection > ").trim();
			println();

			IConsoleOption option = selectOption(line);
			if (option == null) {
				continue;
			}

			return option;
		}
	}

	private IConsoleOption selectOption(String line) {
		if (line.isEmpty()) {
			return null;
		}

		int selected = parseInt(line);
		line = line.toLowerCase();

		// Look for an exact match
		int number = 1;
		IConsoleOption partial = null;
		for (IConsoleOption option : options) {

			// Match!
			if (selected == number) {
				return option;
			}

			String description = option.getDescription().toLowerCase();

			// Match!
			if (description.equals(line)) {
				return option;
			}
			if (partial == null && description.startsWith(line)) {
				partial = option;
			}
			number++;
		}

		// If we did not get an exact match, did we get a partial match?
		if (partial != null) {
			return partial;
		}

		return null;
	}

	private int parseInt(String line) {
		try {
			return Integer.parseInt(line);
		} catch (Exception e) {
			return 0;
		}
	}

	private void showOptions() {
		int number = 1;
		println("[" + title + "]");
		for (IConsoleOption option : options) {
			println(number + ". " + option.getDescription());
			number++;
		}
	}

}
