package com.javabi.common.console.option;

import com.javabi.common.console.IConsoleExecutable;

public class ExecutableConsoleOption extends ConsoleOption {

	private IConsoleExecutable executable;

	public ExecutableConsoleOption(String description, IConsoleExecutable executable) {
		super(description);
		if (executable == null) {
			throw new NullPointerException("executable");
		}
		this.executable = executable;
	}

	@Override
	public void execute() {
		executable.execute();
	}
}
