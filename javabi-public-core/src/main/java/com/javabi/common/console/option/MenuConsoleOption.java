package com.javabi.common.console.option;

public class MenuConsoleOption extends ConsoleOption {

	private final ConsoleMenu menu;

	public MenuConsoleOption(String title) {
		super(title);
		this.menu = new ConsoleMenu(title);
	}

	@Override
	public void execute() {
		menu.execute();
	}

	public ConsoleMenu getMenu() {
		return menu;
	}

}
