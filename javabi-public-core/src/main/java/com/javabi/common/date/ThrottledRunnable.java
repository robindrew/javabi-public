package com.javabi.common.date;

import java.util.concurrent.TimeUnit;

/**
 * A Throttled Runnable.
 * <ul>
 * <li>Initially not throttled.</li>
 * <li>A runnable that can not run more frequently than a given minimum.</li>
 * </ul>
 */
public class ThrottledRunnable implements Runnable {

	/** The underlying runnable. */
	private final Runnable runnable;

	/** The last run time. */
	private volatile long lastRunTime = 0;
	/** The minimum run interval. */
	private volatile long minimumRunInterval = 0;

	/**
	 * Creates a new throttled runnable.
	 * @param runnable the underlying runnable.
	 */
	public ThrottledRunnable(Runnable runnable) {
		if (runnable == null) {
			throw new NullPointerException();
		}
		this.runnable = runnable;
	}

	/**
	 * Sets the minimum refresh interval in milliseconds.
	 * @param interval the interval.
	 * @param timeUnit the time unit of the interval.
	 */
	public final void setMinimumRunInterval(long interval, TimeUnit timeUnit) {
		if (interval < 1) {
			throw new IllegalArgumentException("interval=" + interval);
		}
		this.minimumRunInterval = timeUnit.toMillis(interval);
	}

	/**
	 * Check whether this can run.
	 * @return true if it can run.
	 */
	private synchronized boolean checkRun() {
		if (minimumRunInterval != 0) {
			long timeNow = System.currentTimeMillis();
			if (timeNow < lastRunTime + minimumRunInterval) {
				return false;
			}
		}
		lastRunTime = System.currentTimeMillis();
		return true;
	}

	/**
	 * Run!.
	 */
	public void run() {
		if (checkRun()) {
			runnable.run();
		}
	}

}
