package com.javabi.common.date;

public class ThreadInterruptedException extends RuntimeException {

	private static final long serialVersionUID = 3314373726392153295L;

	public ThreadInterruptedException(InterruptedException ie) {
		super(ie);
	}

}
