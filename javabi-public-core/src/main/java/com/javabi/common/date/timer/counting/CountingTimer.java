package com.javabi.common.date.timer.counting;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.concurrent.TimeUnit;

import com.javabi.common.date.Delay;
import com.javabi.common.date.timer.ITimer;
import com.javabi.common.date.timer.MilliTimer;

public class CountingTimer implements ICountingTimer {

	public static ICountingTimer startTimer(long amount, TimeUnit unit) {
		CountingTimer timer = new CountingTimer(amount, unit);
		timer.start();
		return timer;
	}

	public static ICountingTimer startTimer() {
		return startTimer(10, SECONDS);
	}

	private long count = 0;
	private final ITimer timer = new MilliTimer();
	private final Delay delay;

	public CountingTimer(Delay delay) {
		this.delay = delay;
	}

	public CountingTimer(long amount, TimeUnit unit) {
		this.delay = new Delay(amount, unit);
	}

	public boolean expired() {
		return delay.expired(true);
	}

	@Override
	public long getCount() {
		return count;
	}

	@Override
	public void increment() {
		count++;
	}

	@Override
	public void increment(long amount) {
		count += amount;
	}

	@Override
	public void start() {
		timer.start();
	}

	@Override
	public void stop() {
		timer.stop();
	}

	@Override
	public void reset() {
		timer.reset();
		delay.reset();
	}

	@Override
	public String toString() {
		return timer.toString();
	}

}
