package com.javabi.common.date;

import static com.javabi.common.lang.Variables.min;
import static com.javabi.common.lang.Variables.notEmpty;
import static com.javabi.common.lang.Variables.notNull;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import com.google.common.base.Supplier;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

public class SchedulerBuilder implements Supplier<ScheduledExecutorService> {

	private final String name;
	private String formatPostfix = "-%d";
	private int corePoolSize = 1;
	private boolean daemon = false;
	private UncaughtExceptionHandler handler = null;

	public SchedulerBuilder(String name) {
		this.name = notEmpty("name", name);
	}

	public SchedulerBuilder setCorePoolSize(int size) {
		this.corePoolSize = min("size", size, 1);
		return this;
	}

	public SchedulerBuilder setFormatPostfix(String postfix) {
		this.formatPostfix = notEmpty("postfix", postfix);
		return this;
	}

	public SchedulerBuilder setDaemon(boolean daemon) {
		this.daemon = daemon;
		return this;
	}

	public SchedulerBuilder setUncaughtExceptionHandler(UncaughtExceptionHandler handler) {
		this.handler = notNull("handler", handler);
		return this;
	}

	@Override
	public ScheduledExecutorService get() {
		ThreadFactoryBuilder factory = new ThreadFactoryBuilder();
		factory.setNameFormat(name + formatPostfix);
		factory.setDaemon(daemon);
		if (handler != null) {
			factory.setUncaughtExceptionHandler(handler);
		}
		return Executors.newScheduledThreadPool(corePoolSize, factory.build());
	}
}
