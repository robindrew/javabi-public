package com.javabi.common.date;

import static com.javabi.common.dependency.DependencyFactory.getDependency;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.lang.IThrowableHandler;

/**
 * A Scheduled Task.
 * <ul>
 * <li>Can only ever have one Runnable to schedule.</li>
 * <li>Frequency and offset of scheduled run can be changed at any time.</li>
 * <li>Can be started and stopped as often as necessary.</li>
 * </ul>
 */
public class FixedRateScheduler {

	private static final Logger log = LoggerFactory.getLogger(FixedRateScheduler.class);

	/** The mutex. */
	private final Object mutex = new Object();

	/** The thread name. */
	private final String threadName;
	/** The task. */
	private final Runnable task;

	/** The frequency between updates. */
	private volatile long frequency = 0;
	/** The frequency offset. */
	private volatile long offset = 0;

	/** The scheduled thread. */
	private volatile ScheduledThread thread = null;

	/**
	 * Creates a new scheduled task.
	 * @param task the task.
	 * @param threadName the thread name.
	 */
	public FixedRateScheduler(Runnable task, String threadName) {
		if (task == null || threadName == null) {
			throw new NullPointerException();
		}
		this.threadName = threadName;
		this.task = task;
	}

	/**
	 * Returns the thread name.
	 * @return the thread name.
	 */
	public final String getThreadName() {
		return threadName;
	}

	/**
	 * Returns the frequency.
	 * @return the frequency.
	 */
	public final long getFrequency() {
		return frequency;
	}

	/**
	 * Sets the frequency.
	 * @param frequency the frequency.
	 * @param timeUnit the time unit.
	 */
	public final void setFrequency(long frequency, TimeUnit timeUnit) {
		if (frequency < 1) {
			throw new IllegalArgumentException("frequency=" + frequency);
		}
		synchronized (mutex) {
			this.frequency = timeUnit.toMillis(frequency);
			update();
		}
	}

	/**
	 * Returns the offset.
	 * @return the offset.
	 */
	public final long getOffset() {
		return offset;
	}

	/**
	 * Sets the offset.
	 * @param offset the offset.
	 * @param timeUnit the time unit.
	 */
	public final void setOffset(long offset, TimeUnit timeUnit) {
		if (offset < 0) {
			throw new IllegalArgumentException("offset=" + offset);
		}
		synchronized (mutex) {
			this.offset = timeUnit.toMillis(offset);
			update();
		}
	}

	/**
	 * Returns true if this task is currently running.
	 * @return true if this task is currently running.
	 */
	public final boolean isRunning() {
		return thread != null;
	}

	/**
	 * Start this task.
	 */
	public final void start() {
		synchronized (mutex) {
			if (frequency == 0) {
				throw new IllegalStateException("frequency not set");
			}

			// Create new thread
			if (thread == null) {
				thread = new ScheduledThread(threadName);
				thread.start();
			} else {
				throw new IllegalStateException("Already started");
			}
		}
	}

	/**
	 * Stop the task (can be started again at any time).
	 */
	public final void stop() {
		synchronized (mutex) {

			// Stop!
			if (thread != null) {
				thread.kill();
				thread = null;
			}
		}
	}

	/**
	 * Update the thread (if it is running).
	 */
	protected final void update() {
		synchronized (mutex) {
			if (isRunning()) {
				thread.interrupt();
			}
		}
	}

	/**
	 * Returns the task.
	 * @return the task.
	 */
	public final Runnable getTask() {
		return task;
	}

	/**
	 * Handle the given throwable.
	 * @param throwable the throwable.
	 */
	protected void handleThrowable(Throwable throwable) {
		IThrowableHandler handler = getDependency(IThrowableHandler.class);
		handler.handle(throwable);

		log.error("Error running scheduler", throwable);
	}

	/**
	 * Returns the time of the next run.
	 * @return the time of the next run.
	 */
	public final long getTimeOfNextRun() {
		synchronized (mutex) {
			long timeOfNextRun = System.currentTimeMillis();
			timeOfNextRun /= frequency;
			timeOfNextRun *= frequency;
			timeOfNextRun += frequency;
			timeOfNextRun += offset;
			return timeOfNextRun;
		}
	}

	/**
	 * A Scheduled Thread.
	 */
	private final class ScheduledThread extends Thread {

		/** Indicates if this thread has been killed. */
		private volatile boolean killed = false;

		/**
		 * Creates a new scheduled thread.
		 * @param name the name.
		 */
		private ScheduledThread(String name) {
			super(name);
			setDaemon(true);
		}

		/**
		 * Kill this thread (can not be undone).
		 */
		public void kill() {
			killed = true;
			interrupt();
		}

		/**
		 * Run this thread.
		 */
		@Override
		public void run() {
			while (!killed) {
				try {

					// Sleep!
					long timeOfNextRun = getTimeOfNextRun();
					long sleep = timeOfNextRun - System.currentTimeMillis();
					if (sleep > 0) {
						Thread.sleep(sleep);
					}

					// Run
					try {
						if (killed) {
							break;
						}
						if (!interrupted()) {
							task.run();
						}
					} catch (Throwable t) {
						handleThrowable(t);
					}

				} catch (InterruptedException ie) {
					interrupted();
				}
			}
		}
	}

}
