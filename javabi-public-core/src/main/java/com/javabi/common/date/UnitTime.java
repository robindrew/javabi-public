package com.javabi.common.date;

import java.util.concurrent.TimeUnit;

public class UnitTime {

	/**
	 * Adds two unit times together, normalizing the units between them.
	 * @param unit1 the first unit.
	 * @param unit2 the second unit.
	 * @return the two units added, normalized to the smaller TimeUnit.
	 */
	public static UnitTime add(UnitTime unit1, UnitTime unit2) {

		// Reverse the add if the
		if (unit1.getUnit().ordinal() > unit2.getUnit().ordinal()) {
			return add(unit2, unit1);
		}

		long time = unit1.getTime();
		TimeUnit unit = unit1.getUnit();

		time += unit2.getTime(unit);
		return new UnitTime(time, unit);
	}

	private final long time;
	private final TimeUnit unit;

	public UnitTime(long time, TimeUnit unit) {
		if (time < 0) {
			throw new IllegalArgumentException("time=" + time);
		}
		if (unit == null) {
			throw new NullPointerException("unit");
		}
		this.time = time;
		this.unit = unit;
	}

	public long getTime() {
		return time;
	}

	public long getTime(TimeUnit unit) {
		return unit.convert(getTime(), getUnit());
	}

	public TimeUnit getUnit() {
		return unit;
	}

	@Override
	public int hashCode() {
		return ((int) time) * unit.hashCode() * 1999;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (object == null) {
			return false;
		}
		if (getClass() != object.getClass()) {
			return false;
		}
		UnitTime other = (UnitTime) object;
		if (time != other.time) {
			return false;
		}
		if (unit != other.unit) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return time + " " + unit;
	}
}
