package com.javabi.common.date;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Thread-safe, efficient date parsing & formatting.
 * <p>
 * Performance tests reveal a significant drop in object allocation, speed and memory footprint (~5x).
 */
public final class Dates {

	/** The number of milliseconds in a second. */
	private static final long MILLIS_PER_SECOND = 1000;
	/** The GMT Time Zone. */
	private static final TimeZone UTC = TimeZone.getTimeZone("UTC");
	/** The UTC Zone Id. */
	public static final ZoneId UTC_ZONE = ZoneOffset.UTC.normalized();

	/** The number of nanoseconds per millisecond. */
	private static final long NANOS_PER_MILLI = 1000000;

	/** The thread-local calendar. */
	private static final ThreadLocal<Calendar> localCalendar = new ThreadLocal<Calendar>() {

		@Override
		protected Calendar initialValue() {
			return new GregorianCalendar(UTC);
		}
	};

	/**
	 * Inaccessible Constructor.
	 */
	private Dates() {
	}

	/**
	 * Returns the thread-local UTC calendar.
	 * @return the thread-local UTC calendar.
	 */
	public static final Calendar getLocalCalendar() {
		Calendar calendar = localCalendar.get();
		if (calendar.getTimeZone() != UTC) {
			calendar.setTimeZone(UTC);
		}
		return calendar;
	}

	/**
	 * Returns the thread-local calendar initialized to the given time.
	 * @param milliseconds the time in milliseconds.
	 * @return the thread-local calendar.
	 */
	public static final Calendar getLocalCalendar(long milliseconds) {
		Calendar calendar = getLocalCalendar();
		calendar.setTimeInMillis(milliseconds);
		return calendar;
	}

	/**
	 * Returns the thread-local UTC calendar.
	 * @param year the year.
	 * @param month the month (1-12).
	 * @param day the day.
	 * @return the thread-local UTC calendar.
	 */
	public static final Calendar getLocalCalendar(int year, int month, int day) {
		Calendar calendar = localCalendar.get();
		if (calendar.getTimeZone() != UTC) {
			calendar.setTimeZone(UTC);
		}
		calendar.set(year, month - 1, day, 0, 0, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

	/**
	 * Returns the thread-local UTC calendar.
	 * @param year the year.
	 * @param month the month (1-12).
	 * @param dayOfMonth the day of the month.
	 * @param hourOfDay the hour of the day.
	 * @param minute the minute.
	 * @param second the second.
	 * @return the thread-local UTC calendar.
	 */
	public static final Calendar getLocalCalendar(int year, int month, int dayOfMonth, int hourOfDay, int minute, int second) {
		Calendar calendar = localCalendar.get();
		if (calendar.getTimeZone() != UTC) {
			calendar.setTimeZone(UTC);
		}
		calendar.set(year, month - 1, dayOfMonth, hourOfDay, minute, second);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

	/**
	 * Returns the given date in milliseconds.
	 * @param year the year.
	 * @param month the month (1-12).
	 * @param dayOfMonth the day of the month.
	 * @param hourOfDay the hour of the day.
	 * @param minute the minute.
	 * @param second the second.
	 * @return the date in milliseconds
	 */
	public static final long toMillis(int year, int month, int dayOfMonth, int hourOfDay, int minute, int second) {
		Calendar calendar = getLocalCalendar(year, month, dayOfMonth, hourOfDay, minute, second);
		return calendar.getTimeInMillis();
	}

	/**
	 * Returns the given date in milliseconds.
	 * @param year the year.
	 * @param month the month (1-12).
	 * @param dayOfMonth the day of the month.
	 * @return the date in milliseconds
	 */
	public static final long toMillis(int year, int month, int dayOfMonth) {
		Calendar calendar = getLocalCalendar(year, month, dayOfMonth);
		return calendar.getTimeInMillis();
	}

	/**
	 * Parses the given date.
	 * @param pattern the date format pattern.
	 * @param date the date as a string.
	 * @return the date.
	 */
	public static final Date parseDate(String pattern, String date) {
		IDateFormat format = ImmutableDateFormat.getInstance(pattern);
		synchronized (format) {
			return format.parse(date);
		}
	}

	/**
	 * Parses the given date.
	 * @param pattern the date format pattern.
	 * @param date the date as a string.
	 * @return the date in milliseconds.
	 */
	public static final long parseMillis(String pattern, String date) {
		return parseDate(pattern, date).getTime();
	}

	/**
	 * Formats the given date.
	 * @param pattern the date format pattern.
	 * @param date the date.
	 * @return the date as a string.
	 */
	public static final String formatDate(String pattern, Date date) {
		IDateFormat format = ImmutableDateFormat.getInstance(pattern);
		return format.format(date);
	}

	/**
	 * Formats the given date.
	 * @param pattern the date format pattern.
	 * @param date the date.
	 * @return the date as a string.
	 */
	public static final String formatDate(String pattern, long date) {
		IDateFormat format = ImmutableDateFormat.getInstance(pattern);
		return format.format(date);
	}

	/**
	 * Formats the given date in milliseconds.
	 * @param pattern the date format pattern.
	 * @param date the date in milliseconds.
	 * @return the date as a string.
	 */
	public static final String formatMillis(String pattern, long date) {
		return formatDate(pattern, new Date(date));
	}

	/**
	 * Returns the given seconds as milliseconds.
	 * @param seconds the seconds.
	 * @return the seconds.
	 */
	public static final long toMillis(int seconds) {
		return seconds * MILLIS_PER_SECOND;
	}

	/**
	 * Returns the given milliseconds as seconds.
	 * @param millis the milliseconds.
	 * @return the seconds.
	 */
	public static final int toSeconds(long millis) {
		return (int) (millis / MILLIS_PER_SECOND);
	}

	/**
	 * Returns the current time in seconds.
	 * @return the current time in seconds.
	 */
	public static final int currentTimeSeconds() {
		return (int) (System.currentTimeMillis() / MILLIS_PER_SECOND);
	}

	/**
	 * Returns the current time in milliseconds.
	 * @return the current time in milliseconds.
	 */
	public static final long currentTimeMillis() {
		return System.currentTimeMillis();
	}

	/**
	 * Returns the given date as an integer date.
	 * @param year the year.
	 * @param month the month.
	 * @param day the day.
	 * @return the integer date.
	 */
	public static int toYYYYMMDD(int year, int month, int day) {
		return (year * 10000) + (month * 100) + day;
	}

	/**
	 * Returns the given date as an integer date.
	 * @param calendar the calendar.
	 * @return the integer date.
	 */
	public static int toYYYYMMDD(Calendar calendar) {
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		return toYYYYMMDD(year, month, day);
	}

	/**
	 * Returns the given date as an integer date.
	 * @param date the date.
	 * @return the integer date.
	 */
	public static int toYYYYMMDD(Date date) {
		return toYYYYMMDD(getLocalCalendar(date.getTime()));
	}

	/**
	 * Returns the given nanoseconds as milliseconds.
	 * @param nanoseconds the nanoseconds.
	 * @return the milliseconds.
	 */
	public static long nanosToMillis(long nanoseconds) {
		return nanoseconds / NANOS_PER_MILLI;
	}

	public static String toString(long date) {
		return formatDate(ImmutableDateFormat.DEFAULT_FORMAT, date);
	}

	public static String toString(Date date) {
		return toString(date.getTime());
	}

	public static long normalize(long timeInMillis, Interval interval) {
		Calendar calendar = Dates.getLocalCalendar();
		calendar.setTimeInMillis(timeInMillis);
		normalize(calendar, interval);
		return calendar.getTimeInMillis();
	}

	public static void normalize(Calendar calendar, Interval interval) {
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.SECOND, 0);

		switch (interval) {
			case YEAR:
				calendar.set(Calendar.MONTH, Calendar.JANUARY);
			case MONTH:
				calendar.set(Calendar.DAY_OF_MONTH, 1);
			case DAY:
				calendar.set(Calendar.HOUR_OF_DAY, 0);
			case HOUR:
				calendar.set(Calendar.MINUTE, 0);
			case MINUTE:
				break;
			case WEEK:
				int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
				calendar.add(Calendar.DAY_OF_MONTH, -dayOfWeek);
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
				break;
			default:
				throw new IllegalStateException("Interval not supported: " + interval);
		}
	}

	public static long getAgo(int amount, Interval interval, boolean normalize) {
		if (amount < 1) {
			throw new IllegalArgumentException("amount=" + amount);
		}

		Calendar calendar = getLocalCalendar(System.currentTimeMillis());
		if (normalize) {
			normalize(calendar, interval);
			amount--;
		}
		if (amount > 0) {
			switch (interval) {
				case MINUTE:
					calendar.add(Calendar.MINUTE, -amount);
					break;
				case HOUR:
					calendar.add(Calendar.HOUR_OF_DAY, -amount);
					break;
				case DAY:
					calendar.add(Calendar.DAY_OF_MONTH, -amount);
					break;
				case WEEK:
					calendar.add(Calendar.DAY_OF_MONTH, -(amount * 7));
					break;
				case MONTH:
					calendar.add(Calendar.MONTH, -amount);
					break;
				case YEAR:
					calendar.add(Calendar.YEAR, -amount);
					break;
				default:
					throw new IllegalStateException("Interval not supported: " + interval);
			}
		}
		return calendar.getTimeInMillis();
	}

	public static boolean isWeekend(DayOfWeek day) {
		return day.equals(DayOfWeek.SATURDAY) || day.equals(DayOfWeek.SUNDAY);
	}

	public static boolean isWeekday(DayOfWeek day) {
		return !isWeekend(day);
	}

	public static boolean isWeekend(LocalDateTime date) {
		return isWeekend(date.getDayOfWeek());
	}

	public static boolean isWeekday(LocalDateTime date) {
		return isWeekday(date.getDayOfWeek());
	}

	public static long toMillis(LocalDateTime date) {
		return toMillis(date, ZoneOffset.UTC);
	}

	public static long toMillis(LocalDateTime date, ZoneOffset offset) {
		return date.toInstant(offset).toEpochMilli();
	}

	public static LocalDateTime toLocalDateTime(long millis) {
		Instant instant = Instant.ofEpochMilli(millis);
		return LocalDateTime.ofInstant(instant, UTC_ZONE);
	}

}
