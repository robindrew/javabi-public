package com.javabi.common.date.timer.counting;

public interface ICountingTimer {

	void increment();

	void increment(long amount);

	void start();

	void stop();

	void reset();

	long getCount();

	boolean expired();

}
