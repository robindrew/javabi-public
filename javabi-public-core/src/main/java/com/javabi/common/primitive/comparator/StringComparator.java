package com.javabi.common.primitive.comparator;

import com.javabi.common.util.ReversableComparator;

public class StringComparator extends ReversableComparator<String> {

	public static final int compareStrings(String value1, String value2) {
		return value1.compareTo(value2);
	}

	public StringComparator(boolean reversed) {
		super(reversed);
	}

	public StringComparator() {
	}

	@Override
	public int compare(String value1, String value2) {
		if (isReversed()) {
			return compare(value2, value1);
		} else {
			return compare(value1, value2);
		}
	}

}
