package com.javabi.common.primitive.comparator;

import com.google.common.primitives.Doubles;
import com.javabi.common.util.ReversableComparator;

public class DoubleComparator extends ReversableComparator<Double> {

	public DoubleComparator(boolean reversed) {
		super(reversed);
	}

	public DoubleComparator() {
	}

	@Override
	public int compare(Double value1, Double value2) {
		if (isReversed()) {
			return Doubles.compare(value2, value1);
		} else {
			return Doubles.compare(value1, value2);
		}
	}

}
