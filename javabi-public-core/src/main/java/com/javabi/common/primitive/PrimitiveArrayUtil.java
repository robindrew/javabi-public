package com.javabi.common.primitive;

import java.util.Collection;

/**
 * A collection of useful serialization utility functions.
 */
public final class PrimitiveArrayUtil {

	/**
	 * Inaccessible Constructor.
	 */
	private PrimitiveArrayUtil() {
	}

	public static final boolean[] toArray(Collection<Boolean> collection) {
		boolean[] array = new boolean[collection.size()];
		int index = 0;
		for (Boolean element : collection) {
			if (element == null) {
				throw new IllegalArgumentException("collection contains null");
			}
			array[index++] = element;
		}
		return array;
	}
}