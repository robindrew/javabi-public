package com.javabi.common.primitive;

/**
 * A collection of useful serialization utility functions.
 */
public final class ByteArrayUtil {

	/**
	 * Inaccessible Constructor.
	 */
	private ByteArrayUtil() {
	}

	/**
	 * Optimized method for inserting a boolean into a byte array.
	 * @param value the value.
	 * @param array the array.
	 * @param offset the array offset.
	 */
	public static final void putBoolean(boolean value, byte[] array, int offset) {
		array[offset] = (value ? (byte) 0 : (byte) 1);
	}

	/**
	 * Optimized method for inserting a double into a byte array.
	 * @param value the value.
	 * @param array the array.
	 * @param offset the array offset.
	 */
	public static final void putDouble(double value, byte[] array, int offset) {
		long l = Double.doubleToLongBits(value);
		putLong(l, array, offset);
	}

	/**
	 * Optimized method for inserting a float into a byte array.
	 * @param value the value.
	 * @param array the array.
	 * @param offset the array offset.
	 */
	public static final void putFloat(float value, byte[] array, int offset) {
		int i = Float.floatToIntBits(value);
		putInt(i, array, offset);
	}

	/**
	 * Optimized method for inserting a long into a byte array.
	 * @param value the value.
	 * @param array the array.
	 * @param offset the array offset.
	 */
	public static final void putLong(long value, byte[] array, int offset) {
		array[offset + 0] = (byte) (value >> 56);
		array[offset + 1] = (byte) (value >> 48);
		array[offset + 2] = (byte) (value >> 40);
		array[offset + 3] = (byte) (value >> 32);
		array[offset + 4] = (byte) (value >> 24);
		array[offset + 5] = (byte) (value >> 16);
		array[offset + 6] = (byte) (value >> 8);
		array[offset + 7] = (byte) (value >> 0);
	}

	/**
	 * Optimized method for inserting a long into a byte array.
	 * @param value the value.
	 * @param array the array.
	 */
	public static final void putLong(long value, byte[] array) {
		putLong(value, array, 0);
	}

	/**
	 * Optimized method for inserting a long into a byte array.
	 * @param value the value.
	 * @return the byte array.
	 */
	public static final byte[] putLong(long value) {
		byte[] bytes = new byte[8];
		putLong(value, bytes, 0);
		return bytes;
	}

	/**
	 * Optimized method for inserting an integer into a byte array.
	 * @param value the value.
	 * @param offset the array offset.
	 * @param array the array.
	 */
	public static final void putInt(int value, byte[] array, int offset) {
		array[offset + 0] = (byte) (value >> 24);
		array[offset + 1] = (byte) (value >> 16);
		array[offset + 2] = (byte) (value >> 8);
		array[offset + 3] = (byte) (value >> 0);
	}

	/**
	 * Optimized method for inserting a integer into a byte array.
	 * @param value the value.
	 * @param array the array.
	 */
	public static final void putInt(int value, byte[] array) {
		putInt(value, array, 0);
	}

	/**
	 * Optimized method for inserting a integer into a byte array.
	 * @param value the value.
	 * @return the byte array.
	 */
	public static final byte[] putInt(int value) {
		byte[] array = new byte[4];
		putInt(value, array, 0);
		return array;
	}

	/**
	 * Optimized method for inserting an short into a byte array.
	 * @param value the value.
	 * @param array the array.
	 * @param offset the array offset.
	 */
	public static final void putShort(short value, byte[] array, int offset) {
		array[offset + 0] = (byte) (value >> 8);
		array[offset + 1] = (byte) (value >> 0);
	}

	/**
	 * Optimized method for inserting a short into a byte array.
	 * @param value the value.
	 * @param array the array.
	 */
	public static final void putShort(short value, byte[] array) {
		putShort(value, array, 0);
	}

	/**
	 * Optimized method for inserting a short into a byte array.
	 * @param value the value.
	 * @return the byte array.
	 */
	public static final byte[] putShort(short value) {
		byte[] array = new byte[2];
		putShort(value, array, 0);
		return array;
	}

	/**
	 * Optimized method for inserting a character into a byte array.
	 * @param value the value.
	 * @param array the array.
	 * @param offset the array offset.
	 */
	public static final void putChar(char value, byte[] array, int offset) {
		int intValue = value;
		putInt(intValue, array, offset);
	}

	/**
	 * Optimized method for inserting a character into a byte array.
	 * @param value the value.
	 * @param array the array.
	 */
	public static final void putChar(char value, byte[] array) {
		int intValue = value;
		putInt(intValue, array);
	}

	/**
	 * Optimized method for converting a byte to a boolean.
	 * @param value the value.
	 * @return the boolean value.
	 */
	public static final boolean getBoolean(byte value) {
		return value == 0;
	}

	/**
	 * Optimized method for converting 8 bytes to a double.
	 * @param b0 the first byte.
	 * @param b1 the second byte.
	 * @param b2 the third byte.
	 * @param b3 the fourth byte.
	 * @param b4 the fifth byte.
	 * @param b5 the sixth byte.
	 * @param b6 the seventh byte.
	 * @param b7 the eighth byte.
	 * @return the double value.
	 */
	public static final double getDouble(byte b0, byte b1, byte b2, byte b3, byte b4, byte b5, byte b6, byte b7) {
		return Double.longBitsToDouble(getLong(b0, b1, b2, b3, b4, b5, b6, b7));
	}

	/**
	 * Optimized method for converting 8 bytes to a double.
	 * @param array the array.
	 * @param offset the array offset.
	 * @return the double value.
	 */
	public static final double getDouble(byte[] array, int offset) {
		return getDouble(array[offset + 0], array[offset + 1], array[offset + 2], array[offset + 3], array[offset + 4], array[offset + 5], array[offset + 6], array[offset + 7]);
	}

	/**
	 * Optimized method for converting 8 bytes to a double.
	 * @param array the array.
	 * @return the double value.
	 */
	public static final double getDouble(byte[] array) {
		return getDouble(array, 0);
	}

	/**
	 * Optimized method for converting 4 bytes to a float.
	 * @param b0 the first byte.
	 * @param b1 the second byte.
	 * @param b2 the third byte.
	 * @param b3 the fourth byte.
	 * @return the float value.
	 */
	public static final float getFloat(byte b0, byte b1, byte b2, byte b3) {
		return Float.intBitsToFloat(getInt(b0, b1, b2, b3));
	}

	/**
	 * Optimized method for converting 4 bytes to a float.
	 * @param array the array.
	 * @param offset the array offset.
	 * @return the float value.
	 */
	public static final float getFloat(byte[] array, int offset) {
		return getFloat(array[offset + 0], array[offset + 1], array[offset + 2], array[offset + 3]);
	}

	/**
	 * Optimized method for converting 4 bytes to a float.
	 * @param array the array.
	 * @return the float value.
	 */
	public static final float getFloat(byte[] array) {
		return getFloat(array, 0);
	}

	/**
	 * Optimized method for converting 8 bytes to a long.
	 * @param b0 the first byte.
	 * @param b1 the second byte.
	 * @param b2 the third byte.
	 * @param b3 the fourth byte.
	 * @param b4 the fifth byte.
	 * @param b5 the sixth byte.
	 * @param b6 the seventh byte.
	 * @param b7 the eighth byte.
	 * @return the long value.
	 */
	public static final long getLong(byte b0, byte b1, byte b2, byte b3, byte b4, byte b5, byte b6, byte b7) {
		return ((((long) b0 & 0xff) << 56) | (((long) b1 & 0xff) << 48) | (((long) b2 & 0xff) << 40) | (((long) b3 & 0xff) << 32) | (((long) b4 & 0xff) << 24) | (((long) b5 & 0xff) << 16) | (((long) b6 & 0xff) << 8) | (((long) b7 & 0xff) << 0));
	}

	/**
	 * Optimized method for converting 8 bytes to a long.
	 * @param array the array.
	 * @param offset the array offset.
	 * @return the long value.
	 */
	public static final long getLong(byte[] array, int offset) {
		return getLong(array[offset + 0], array[offset + 1], array[offset + 2], array[offset + 3], array[offset + 4], array[offset + 5], array[offset + 6], array[offset + 7]);
	}

	/**
	 * Optimized method for converting 8 bytes to a long.
	 * @param array the array.
	 * @return the long value.
	 */
	public static final long getLong(byte[] array) {
		return getLong(array, 0);
	}

	/**
	 * Optimized method for converting 4 bytes to an integer.
	 * @param b0 the first byte.
	 * @param b1 the second byte.
	 * @param b2 the third byte.
	 * @param b3 the fourth byte.
	 * @return the integer value.
	 */
	public static final int getInt(byte b0, byte b1, byte b2, byte b3) {
		return ((b0 & 0xff) << 24) | ((b1 & 0xff) << 16) | ((b2 & 0xff) << 8) | ((b3 & 0xff) << 0);
	}

	/**
	 * Optimized method for converting 4 bytes to an integer.
	 * @param array the array.
	 * @param offset the array offset.
	 * @return the integer value.
	 */
	public static final int getInt(byte[] array, int offset) {
		return getInt(array[offset + 0], array[offset + 1], array[offset + 2], array[offset + 3]);
	}

	/**
	 * Optimized method for converting 4 bytes to an integer.
	 * @param array the array.
	 * @return the integer value.
	 */
	public static final int getInt(byte[] array) {
		return getInt(array, 0);
	}

	/**
	 * Optimized method for converting 2 bytes to a short.
	 * @param b0 the first byte.
	 * @param b1 the second byte.
	 * @return the short value.
	 */
	public static final short getShort(byte b0, byte b1) {
		return (short) (((b0 & 0xff) << 8) | ((b1 & 0xff) << 0));
	}

	/**
	 * Optimized method for converting 2 bytes to a short.
	 * @param array the array.
	 * @param offset the array offset.
	 * @return the short value.
	 */
	public static final short getShort(byte[] array, int offset) {
		return getShort(array[offset + 0], array[offset + 1]);
	}

	/**
	 * Optimized method for converting 2 bytes to a short.
	 * @param array the array.
	 * @return the short value.
	 */
	public static final short getShort(byte[] array) {
		return getShort(array, 0);
	}

	/**
	 * Optimized method for converting 4 bytes to a character.
	 * @param b0 the first byte.
	 * @param b1 the second byte.
	 * @param b2 the third byte.
	 * @param b3 the fourth byte.
	 * @return the character value.
	 */
	public static final char getChar(byte b0, byte b1, byte b2, byte b3) {
		return (char) getInt(b0, b1, b2, b3);
	}

	/**
	 * Optimized method for converting 4 bytes to an integer.
	 * @param array the array.
	 * @param offset the array offset.
	 * @return the character value.
	 */
	public static final char getChar(byte[] array, int offset) {
		return (char) getInt(array, offset);
	}

	/**
	 * Optimized method for converting 4 bytes to an integer.
	 * @param array the array.
	 * @return the character value.
	 */
	public static final char getChar(byte[] array) {
		return (char) getInt(array);
	}
}