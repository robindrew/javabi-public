package com.javabi.common.primitive.comparator;

import com.google.common.primitives.Ints;
import com.javabi.common.util.ReversableComparator;

public class IntegerComparator extends ReversableComparator<Integer> {

	public IntegerComparator(boolean reversed) {
		super(reversed);
	}

	public IntegerComparator() {
	}

	@Override
	public int compare(Integer value1, Integer value2) {
		if (isReversed()) {
			return Ints.compare(value2, value1);
		} else {
			return Ints.compare(value1, value2);
		}
	}

}
