package com.javabi.common.primitive.comparator;

import com.google.common.primitives.Floats;
import com.javabi.common.util.ReversableComparator;

public class FloatComparator extends ReversableComparator<Float> {

	public FloatComparator(boolean reversed) {
		super(reversed);
	}

	public FloatComparator() {
	}

	@Override
	public int compare(Float value1, Float value2) {
		if (isReversed()) {
			return Floats.compare(value2, value1);
		} else {
			return Floats.compare(value1, value2);
		}
	}

}
