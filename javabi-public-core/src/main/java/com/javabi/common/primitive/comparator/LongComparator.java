package com.javabi.common.primitive.comparator;

import com.google.common.primitives.Longs;
import com.javabi.common.util.ReversableComparator;

public class LongComparator extends ReversableComparator<Long> {

	public LongComparator(boolean reversed) {
		super(reversed);
	}

	public LongComparator() {
	}

	@Override
	public int compare(Long value1, Long value2) {
		if (isReversed()) {
			return Longs.compare(value2, value1);
		} else {
			return Longs.compare(value1, value2);
		}
	}

}
