package com.javabi.common.unix;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import com.javabi.common.text.parser.IStringParser;
import com.javabi.common.text.parser.ParserException;

public class UnixParser {

	private static final int END_OF_LINE = '\n';
	private static final int END_OF_FILE = -1;

	private final Reader reader;
	private boolean endOfFile = false;

	public UnixParser(Reader reader) {
		if (reader == null) {
			throw new NullPointerException("reader");
		}
		this.reader = reader;
	}

	public UnixParser(String text) {
		if (text == null) {
			throw new NullPointerException("text");
		}
		this.reader = new StringReader(text);
	}

	private int read() {
		try {
			return reader.read();
		} catch (IOException e) {
			throw new ParserException(e);
		}
	}

	public boolean isEndOfFile() {
		return endOfFile;
	}

	public String line() {
		if (endOfFile) {
			return null;
		}
		StringBuilder line = new StringBuilder();
		while (true) {
			int character = read();

			// Break on end of file
			if (character == END_OF_FILE) {
				endOfFile = true;
				break;
			}

			// Break on end of line
			if (character == END_OF_LINE) {
				break;
			}
			if (character == 13) {
				continue;
			}
			line.append((char) character);
		}
		if (line.length() == 0) {
			return null;
		}
		return line.toString();
	}

	public UnixParser lineParser() {
		String line = line();
		if (line == null) {
			return null;
		}
		return new UnixParser(line);
	}

	public String next(Character end) {
		return nextToken(end);
	}

	public String next() {
		return nextToken(null);
	}

	public <P> P next(IStringParser<P> parser) {
		String text = nextToken(null);
		return parser.parse(text);
	}

	public <P> P next(IStringParser<P> parser, Character end) {
		String text = nextToken(end);
		return parser.parse(text);
	}

	private String nextToken(Character end) {
		if (endOfFile) {
			return null;
		}
		StringBuilder next = new StringBuilder();
		while (true) {
			int character = read();

			// Break on end of file
			if (character == END_OF_FILE) {
				endOfFile = true;
				break;
			}

			// Break on end of line
			if (character == END_OF_LINE) {
				break;
			}

			// Skip any initial whitespace
			if (Character.isWhitespace(character)) {
				if (next.length() > 0) {
					break;
				}
				continue;
			}

			// Stop at end if set
			if (end != null && end == character) {
				break;
			}

			next.append((char) character);
		}
		if (next.length() == 0) {
			return null;
		}
		return next.toString();
	}

}
