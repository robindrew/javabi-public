package com.javabi.common.unix.parser;

import java.io.Reader;
import java.math.BigDecimal;

import com.javabi.common.text.parser.BigDecimalParser;
import com.javabi.common.text.parser.IStringParser;
import com.javabi.common.unix.UnixParser;

public class Uptime {

	private static final IStringParser<BigDecimal> BIG_DECIMAL = new BigDecimalParser().setNullValid(false);

	public static final Uptime parseUptime(UnixParser parser) {
		parser.next(','); // uptime
		parser.next(','); // time?
		parser.next(','); // users
		parser.next(':'); // load average:
		BigDecimal one = parser.next(BIG_DECIMAL, ',');
		BigDecimal five = parser.next(BIG_DECIMAL, ',');
		BigDecimal fifteen = parser.next(BIG_DECIMAL);
		return new Uptime(one, five, fifteen);
	}

	public static final Uptime parseUptime(String text) {
		return parseUptime(new UnixParser(text));
	}

	public static final Uptime parseUptime(Reader reader) {
		return parseUptime(new UnixParser(reader));
	}

	private final BigDecimal one;
	private final BigDecimal five;
	private final BigDecimal fifteen;

	public Uptime(BigDecimal one, BigDecimal five, BigDecimal fifteen) {
		if (one == null || five == null || fifteen == null) {
			throw new NullPointerException();
		}
		this.one = one;
		this.five = five;
		this.fifteen = fifteen;
	}

	public BigDecimal getOneMinute() {
		return one;
	}

	public BigDecimal getFiveMinute() {
		return five;
	}

	public BigDecimal getFifteenMinute() {
		return fifteen;
	}

	@Override
	public String toString() {
		return one + ", " + five + ", " + fifteen;
	}

	public static void main(String[] args) {
		System.out.println(Uptime.parseUptime(" 16:20:25 up 345 days, 20:18,  1 user,  load average: 0.00, 0.01, 0.00"));
	}
}
