package com.javabi.common.unix.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.io.Files;
import com.javabi.common.unix.UnixParser;

public class ProcessList {

	public static final ProcessList parseProcessList(String text) {
		return parseProcessList(new UnixParser(text));
	}

	public static final ProcessList parseProcessList(Reader reader) {
		return parseProcessList(new UnixParser(reader));
	}

	public static final ProcessList parseProcessList(UnixParser parser) {

		// Header Line
		Map<String, List<String>> processMap = new LinkedHashMap<String, List<String>>();
		UnixParser line = parser.lineParser();
		String lastColumn = null;
		while (!line.isEndOfFile()) {
			String column = line.next();
			if (column == null) {
				break;
			}
			processMap.put(column, new ArrayList<String>());
			lastColumn = column;
		}

		// Process Lines
		while (!parser.isEndOfFile()) {
			line = parser.lineParser();
			if (line == null) {
				break;
			}
			for (Entry<String, List<String>> entry : processMap.entrySet()) {
				List<String> valueList = entry.getValue();
				if (entry.getKey().equals(lastColumn)) {
					String value = line.line().trim();
					valueList.add(value);
					break;
				}
				String value = line.next();
				valueList.add(value);
			}
		}
		for (Entry<String, List<String>> entry : processMap.entrySet()) {
			System.err.println(entry);
		}
		return null;
	}

	public static void main(String[] args) {
		try {
			Reader reader = Files.newReader(new File("src/test/resources/psauxwww.txt"), Charset.forName("UTF-8"));
			parseProcessList(reader);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
