package com.javabi.common.cache;

import net.sf.ehcache.Cache;

public interface IEhCacheManager {

	void addCache(Cache cache);

}
