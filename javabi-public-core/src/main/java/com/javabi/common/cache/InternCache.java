package com.javabi.common.cache;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.javabi.common.date.ImmutableDate;

/**
 * An Intern Cache.
 */
public final class InternCache {

	/** The range. */
	private static final int BYTE_RANGE = (Byte.MAX_VALUE - Byte.MIN_VALUE) + 1;
	/** The byte cache. */
	private static final Byte[] BYTE_CACHE = new Byte[BYTE_RANGE];

	/** The short cache. */
	private static final ConcurrentMap<Short, Short> shortCache = new ConcurrentHashMap<Short, Short>();
	/** The integer cache. */
	private static final ConcurrentMap<Integer, Integer> integerCache = new ConcurrentHashMap<Integer, Integer>();
	/** The long cache. */
	private static final ConcurrentMap<Long, Long> longCache = new ConcurrentHashMap<Long, Long>();
	/** The float cache. */
	private static final ConcurrentMap<Float, Float> floatCache = new ConcurrentHashMap<Float, Float>();
	/** The double cache. */
	private static final ConcurrentMap<Double, Double> doubleCache = new ConcurrentHashMap<Double, Double>();

	/** The date cache. */
	private static final ConcurrentMap<ImmutableDate, ImmutableDate> dateCache = new ConcurrentHashMap<ImmutableDate, ImmutableDate>();

	/** The singleton instance. */
	private static final InternCache instance = new InternCache();

	/**
	 * Static initialisation block.
	 */
	static {

		// Populate byte cache
		for (int i = 0; i < BYTE_RANGE; i++) {
			BYTE_CACHE[i] = (byte) (i + Byte.MIN_VALUE);
		}
	}

	/**
	 * Returns the singleton instance.
	 * @return the singleton instance.
	 */
	public static final InternCache getInstance() {
		return instance;
	}

	/**
	 * Intern the value to the given cache.
	 * @param <V> the value type.
	 * @param cache the cache.
	 * @param value the value.
	 * @return the interned value.
	 */
	private static <V> V intern(ConcurrentMap<V, V> cache, V value) {
		V cached = cache.get(value);
		if (cached == null) {
			cached = value;
			cache.put(cached, cached);
		}
		return cached;
	}

	/**
	 * Returns the cached instance of the given value.
	 * @param value the value.
	 * @return the cached value.
	 */
	public static final Boolean intern(boolean value) {
		return value ? Boolean.TRUE : Boolean.FALSE;
	}

	/**
	 * Returns the cached instance of the given value.
	 * @param value the value.
	 * @return the cached value.
	 */
	public static final Boolean intern(Boolean value) {
		if (value == null) {
			return null;
		}
		return value.booleanValue() ? Boolean.TRUE : Boolean.FALSE;
	}

	/**
	 * Returns the cached instance of the given value.
	 * @param value the value.
	 * @return the cached value.
	 */
	public static final Byte intern(byte value) {
		int index = value - Byte.MIN_VALUE;
		return BYTE_CACHE[index];
	}

	/**
	 * Returns the cached instance of the given value.
	 * @param value the value.
	 * @return the cached value.
	 */
	public static final Byte intern(Byte value) {
		if (value == null) {
			return null;
		}
		int index = value.byteValue() - Byte.MIN_VALUE;
		return BYTE_CACHE[index];
	}

	/**
	 * Returns the cached instance of the given value.
	 * @param value the value.
	 * @return the cached value.
	 */
	public static final Short intern(Short value) {
		if (value == null) {
			return null;
		}
		return intern(shortCache, value);
	}

	/**
	 * Returns the cached instance of the given value.
	 * @param value the value.
	 * @return the cached value.
	 */
	public static final Integer intern(Integer value) {
		if (value == null) {
			return null;
		}
		return intern(integerCache, value);
	}

	/**
	 * Returns the cached instance of the given value.
	 * @param value the value.
	 * @return the cached value.
	 */
	public static final Long intern(Long value) {
		if (value == null) {
			return null;
		}
		return intern(longCache, value);
	}

	/**
	 * Returns the cached instance of the given value.
	 * @param value the value.
	 * @return the cached value.
	 */
	public static final Float intern(Float value) {
		if (value == null) {
			return null;
		}
		return intern(floatCache, value);
	}

	/**
	 * Returns the cached instance of the given value.
	 * @param value the value.
	 * @return the cached value.
	 */
	public static final Double intern(Double value) {
		if (value == null) {
			return null;
		}
		return intern(doubleCache, value);
	}

	/**
	 * Returns the cached instance of the given value.
	 * @param value the value.
	 * @return the cached value.
	 */
	public static final Date intern(Date value) {
		if (value == null) {
			return null;
		}
		ImmutableDate cached = dateCache.get(value);
		if (cached == null) {
			cached = new ImmutableDate(value);
			dateCache.put(cached, cached);
		}
		return cached;
	}

	/**
	 * Returns the cached instance of the given value.
	 * @param value the value.
	 * @return the cached value.
	 */
	public static final String intern(String value) {
		if (value == null) {
			return null;
		}
		return value.intern();
	}

	/**
	 * An Inaccessible Constructor.
	 */
	private InternCache() {
	}

}
