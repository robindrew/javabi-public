package com.javabi.common.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

public class EhCacheManager implements IEhCacheManager {

	public static final String PROPERTY_SKIP_UPDATE_CHECK = "net.sf.ehcache.skipUpdateCheck";

	private final CacheManager manager;

	public EhCacheManager(boolean skipUpdateCheck) {
		if (skipUpdateCheck) {
			System.setProperty(PROPERTY_SKIP_UPDATE_CHECK, "true");
		}
		manager = CacheManager.create();
	}

	@Override
	public void addCache(Cache cache) {
		if (cache == null) {
			throw new NullPointerException("cache");
		}
		manager.addCache(cache);
	}

}
