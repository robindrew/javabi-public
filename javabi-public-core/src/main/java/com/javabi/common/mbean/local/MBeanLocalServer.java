package com.javabi.common.mbean.local;

import java.lang.management.ManagementFactory;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

import com.javabi.common.mbean.MBeanAbstractServer;
import com.javabi.common.mbean.MBeanRuntimeException;

/**
 * A Local MBean Server.
 */
public class MBeanLocalServer extends MBeanAbstractServer {

	/**
	 * Returns the default object name for the given object.
	 * @param object the object.
	 * @return the object name.
	 */
	private static final ObjectName getDefaultObjectName(Object object) {
		try {
			Class<?> clazz = object.getClass();
			String packageName = clazz.getPackage().getName();
			return new ObjectName(packageName, "type", clazz.getSimpleName());
		} catch (Exception e) {
			throw new MBeanRuntimeException(e);
		}
	}

	/** The server. */
	private final MBeanServer server;
	/** The registered object set. */
	private final Set<Object> registeredSet = Collections.synchronizedSet(new HashSet<Object>());

	/**
	 * Creates a new server.
	 */
	public MBeanLocalServer() {
		this.server = ManagementFactory.getPlatformMBeanServer();
		populate();
	}

	@Override
	protected MBeanServerConnection getConnection() {
		return server;
	}

	/**
	 * Register the given object.
	 * @param object the object.
	 * @param name the object name.
	 */
	public <O> void register(O object, ObjectName name) {
		if (object == null) {
			throw new NullPointerException();
		}
		try {
			if (registeredSet.add(object)) {
				Class<O> mbeanInterface = getMBeanInterface(object);
				Object mbean = new MBeanLocalAnnotation(object, mbeanInterface);
				server.registerMBean(mbean, name);
				populate();
			}
		} catch (Exception e) {
			throw new MBeanRuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private <O> Class<O> getMBeanInterface(O object) {
		String name = object.getClass().getSimpleName() + "MBean";
		Class<?>[] mbeanInterfaces = object.getClass().getInterfaces();
		for (Class<?> mbeanInterface : mbeanInterfaces) {
			if (mbeanInterface.getSimpleName().equals(name)) {
				return (Class<O>) mbeanInterface;
			}
		}
		throw new IllegalArgumentException("Unable to find MBean interface for class: " + object.getClass());
	}

	/**
	 * Register the given object.
	 * @param object the object.
	 */
	public void register(Object object) {
		ObjectName name = getDefaultObjectName(object);
		register(object, name);
	}
}
