package com.javabi.common.mbean.local;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.management.MBeanOperationInfo;

/**
 * An MBean Method.
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface MBeanMethod {

	/**
	 * The description of the method (mandatory).
	 * @return the description.
	 */
	String description();

	/**
	 * The impact of invoking this method (defaults to {@link MBeanOperationInfo#UNKNOWN}).
	 * @return the impact.
	 */
	int impact() default MBeanOperationInfo.UNKNOWN;

	/**
	 * The parameters of the method.
	 * @return the parameters.
	 */
	String[] parameters() default {};

}
