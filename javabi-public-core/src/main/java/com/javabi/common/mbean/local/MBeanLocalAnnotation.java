package com.javabi.common.mbean.local;

import java.lang.reflect.Method;

import javax.management.MBeanAttributeInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.NotCompliantMBeanException;
import javax.management.StandardMBean;

import com.javabi.common.lang.clazz.ClassResolver;

/**
 * An Annotated Standard MBean.
 */
class MBeanLocalAnnotation extends StandardMBean {

	/**
	 * Creates a new MBean.
	 * @param <M> the type.
	 * @param mbean the bean.
	 * @param mbeanInterface the bean interface.
	 */
	public <M> MBeanLocalAnnotation(M mbean, Class<M> mbeanInterface) throws NotCompliantMBeanException {
		super(mbean, mbeanInterface);
	}

	/**
	 * Returns the method for the given operation.
	 * @param operation the operation.
	 * @return the method.
	 */
	protected Method getMethod(MBeanOperationInfo operation) throws Exception {
		Class<?> mbeanInterface = getMBeanInterface();
		String name = operation.getName();

		// Parameters
		MBeanParameterInfo[] signature = operation.getSignature();
		Class<?>[] types = new Class[signature.length];
		ClassResolver resolver = new ClassResolver();
		for (int i = 0; i < signature.length; i++) {
			types[i] = resolver.resolveClass(signature[i].getType());
		}
		return mbeanInterface.getMethod(name, types);
	}

	/**
	 * Returns the method for the given attribute.
	 * @param attribute the attribute.
	 * @return the method.
	 */
	protected Method getMethod(MBeanAttributeInfo attribute) throws Exception {
		Class<?> mbeanInterface = getMBeanInterface();
		String name = attribute.getName();

		// Getter
		if (attribute.isReadable()) {
			return mbeanInterface.getMethod("get" + name, new Class<?>[0]);
		}

		// Setter
		Class<?> type = new ClassResolver().resolveClass(attribute.getType());
		return mbeanInterface.getMethod("set" + name, new Class<?>[] { type });
	}

	@Override
	protected String getDescription(MBeanOperationInfo operation) {
		try {
			Method method = getMethod(operation);
			MBeanMethod annotation = method.getAnnotation(MBeanMethod.class);
			if (annotation != null) {
				return annotation.description();
			}
		} catch (Exception e) {
			handleException(e);
		}
		return super.getDescription(operation);
	}

	@Override
	protected String getDescription(MBeanAttributeInfo attribute) {
		try {
			Method method = getMethod(attribute);
			MBeanMethod annotation = method.getAnnotation(MBeanMethod.class);
			if (annotation != null) {
				return annotation.description();
			}
		} catch (Exception e) {
			handleException(e);
		}
		return super.getDescription(attribute);
	}

	@Override
	protected int getImpact(MBeanOperationInfo operation) {
		try {
			Method method = getMethod(operation);
			MBeanMethod annotation = method.getAnnotation(MBeanMethod.class);
			if (annotation != null) {
				return annotation.impact();
			}
		} catch (Exception e) {
			handleException(e);
		}
		return super.getImpact(operation);
	}

	@Override
	protected String getParameterName(MBeanOperationInfo operation, MBeanParameterInfo parameter, int index) {
		try {
			Method method = getMethod(operation);
			MBeanMethod annotation = method.getAnnotation(MBeanMethod.class);
			String[] parameters = annotation.parameters();
			if (parameters.length != operation.getSignature().length) {
				throw new IllegalStateException("Method incorrectly annotated: " + method);
			}
			return parameters[index];
		} catch (Exception e) {
			handleException(e);
		}
		return super.getParameterName(operation, parameter, index);
	}

	/**
	 * Handle the given exception.
	 * @param e the exception.
	 */
	protected void handleException(Exception e) {
		e.printStackTrace();
	}

}
