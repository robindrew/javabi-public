package com.javabi.common.mbean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.management.MBeanInfo;
import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.ObjectName;

/**
 * An MBean Server.
 */
public abstract class MBeanAbstractServer implements Iterable<MBeanDomain> {

	/** The standard domain set. */
	private static final Set<String> standardDomainSet = new HashSet<String>();

	static {
		standardDomainSet.add("JMImplementation");
		standardDomainSet.add("com.sun.management");
		standardDomainSet.add("java.lang");
		standardDomainSet.add("java.util.logging");
	}

	/** The domain list. */
	private final Map<String, MBeanDomain> nameToDomainMap = new TreeMap<String, MBeanDomain>();

	/**
	 * Returns the domains.
	 * @return the domains.
	 */
	public Collection<MBeanDomain> getDomains() {
		return Collections.unmodifiableCollection(nameToDomainMap.values());
	}

	public Collection<MBeanDomain> getDomains(boolean includeStandard) {
		if (includeStandard) {
			return getDomains();
		}
		List<MBeanDomain> domainList = new ArrayList<MBeanDomain>();
		for (Entry<String, MBeanDomain> entry : nameToDomainMap.entrySet()) {
			String name = entry.getKey();
			if (!standardDomainSet.contains(name)) {
				domainList.add(entry.getValue());
			}
		}
		return Collections.unmodifiableList(domainList);
	}

	/**
	 * Returns an iterator over the domains.
	 * @return an iterator over the domains.
	 */
	public Iterator<MBeanDomain> iterator() {
		return getDomains().iterator();
	}

	/**
	 * Populate this server from the connection.
	 */
	public void populate() {
		MBeanServerConnection connection = getConnection();
		if (connection == null) {
			throw new IllegalStateException("connection not available");
		}
		try {
			nameToDomainMap.clear();
			Set<ObjectInstance> beans = connection.queryMBeans(null, null);
			for (ObjectInstance bean : beans) {
				ObjectName name = bean.getObjectName();
				String domainName = name.getDomain();
				MBeanInfo info = connection.getMBeanInfo(name);
				MBeanDomain domain = getDomain(domainName, true);
				MBeanInstance instance = new MBeanInstance(name, info, domain);
				domain.addInstance(instance);
			}
		} catch (Exception e) {
			throw new MBeanRuntimeException(e);
		}
	}

	/**
	 * Returns the domain with the given name.
	 * @param name the name.
	 * @param create true to create the domain.
	 * @return the domain.
	 */
	private MBeanDomain getDomain(String name, boolean create) {
		MBeanDomain domain = nameToDomainMap.get(name);
		if (domain == null && create) {
			domain = new MBeanDomain(name, getConnection());
			nameToDomainMap.put(name, domain);
		}
		return domain;
	}

	/**
	 * Returns the connection.
	 * @return the connection.
	 */
	protected abstract MBeanServerConnection getConnection();

}
