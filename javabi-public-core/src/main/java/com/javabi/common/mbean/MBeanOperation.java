package com.javabi.common.mbean;

import java.lang.reflect.Method;
import java.util.Arrays;

import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

import com.javabi.common.lang.clazz.ClassResolver;

/**
 * An MBean Operation.
 */
public class MBeanOperation {

	/** The underlying info. */
	private final MBeanOperationInfo info;
	/** The instance. */
	private final MBeanInstance instance;

	/**
	 * Creates a new operation.
	 * @param info the underlying info.
	 * @param instance the instance.
	 */
	public MBeanOperation(MBeanOperationInfo info, MBeanInstance instance) {
		if (info == null || instance == null) {
			throw new NullPointerException();
		}
		this.info = info;
		this.instance = instance;
	}

	/**
	 * Returns the instance.
	 * @return the instance.
	 */
	public MBeanInstance getInstance() {
		return instance;
	}

	/**
	 * Returns the return type name.
	 * @return the return type name.
	 */
	public String getReturnTypeName() {
		return info.getReturnType();
	}

	/**
	 * Returns the return type class.
	 * @return the return type class.
	 */
	public Class<?> getReturnTypeClass() {
		return new ClassResolver().resolveClass(getReturnTypeName());
	}

	/**
	 * Returns the method name.
	 * @return the method name.
	 */
	public String getMethodName() {
		return info.getName();
	}

	/**
	 * Returns the description.
	 * @return the description.
	 */
	public String getDescription() {
		return info.getDescription();
	}

	/**
	 * Returns the number of parameters.
	 * @return the number of parameters.
	 */
	public int parameters() {
		return info.getSignature().length;
	}

	/**
	 * Returns the parameter name.
	 * @param index the parameter index.
	 * @return the parameter name.
	 */
	public String getParameterName(int index) {
		MBeanParameterInfo[] signature = info.getSignature();
		return signature[index].getName();
	}

	/**
	 * Returns the parameter names.
	 * @return the parameter names.
	 */
	public String[] getParameterNames() {
		String[] names = new String[parameters()];
		for (int i = 0; i < names.length; i++) {
			names[i] = getParameterName(i);
		}
		return names;
	}

	/**
	 * Returns the parameter type names.
	 * @return the parameter type names.
	 */
	public String[] getParameterTypeNames() {
		MBeanParameterInfo[] parameters = info.getSignature();
		String[] names = new String[parameters.length];
		for (int i = 0; i < parameters.length; i++) {
			names[i] = parameters[i].getType();
		}
		return names;
	}

	/**
	 * Returns the parameter type classes.
	 * @return the parameter type classes.
	 */
	public Class<?>[] getParameterTypeClasses() {
		MBeanParameterInfo[] parameters = info.getSignature();
		Class<?>[] types = new Class[parameters.length];
		ClassResolver resolver = new ClassResolver();
		for (int i = 0; i < parameters.length; i++) {
			types[i] = resolver.resolveClass(parameters[i].getType());
		}
		return types;
	}

	/**
	 * Attempts to resolve the method and return it.
	 * @return the resolved method.
	 */
	public Method getMethod() {
		Class<?> type = getInstance().getType();
		try {
			return type.getMethod(getMethodName(), getParameterTypeClasses());
		} catch (Exception e) {
			throw new MBeanRuntimeException(e);
		}
	}

	/**
	 * Invoke this operation.
	 * @param parameters the parameters.
	 * @return the return value (if any).
	 */
	public Object invoke(Object... parameters) {
		MBeanServerConnection connection = getInstance().getDomain().getConnection();
		ObjectName name = getInstance().getObjectName();
		try {
			return connection.invoke(name, getMethodName(), parameters, getParameterTypeNames());
		} catch (Exception e) {
			throw new MBeanRuntimeException(e);
		}
	}

	@Override
	public String toString() {
		return getReturnTypeName() + " " + getMethodName() + Arrays.toString(getParameterTypeNames());
	}

	@Override
	public int hashCode() {
		return getMethodName().hashCode() * (parameters() + 37);
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object instanceof MBeanOperation) {
			MBeanOperation operation = (MBeanOperation) object;
			return getMethodName().equals(operation.getMethodName()) && Arrays.equals(getParameterTypeNames(), operation.getParameterTypeNames());
		}
		return false;
	}
}
