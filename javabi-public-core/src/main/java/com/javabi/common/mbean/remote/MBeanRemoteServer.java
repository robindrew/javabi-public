package com.javabi.common.mbean.remote;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import com.javabi.common.mbean.MBeanAbstractServer;
import com.javabi.common.mbean.MBeanRuntimeException;

/**
 * A Remote MBean Server.
 */
public class MBeanRemoteServer extends MBeanAbstractServer {

	/** The URL. */
	private final String url;
	/** The connector. */
	private JMXConnector connector = null;

	/**
	 * Creates a new remote server.
	 * @param hostname the hostname.
	 * @param port the port.
	 */
	public MBeanRemoteServer(String hostname, int port) {
		if (hostname == null) {
			throw new NullPointerException();
		}
		if (port < 0) {
			throw new IllegalArgumentException("port=" + port);
		}
		this.url = "service:jmx:rmi:///jndi/rmi://" + hostname + ":" + port + "/jmxrmi";
	}

	/**
	 * Connect.
	 */
	public void connect() {
		if (connector != null) {
			throw new IllegalStateException("connector already connected to: " + url);
		}
		try {
			connector = JMXConnectorFactory.connect(new JMXServiceURL(url));
		} catch (Exception e) {
			throw new MBeanRuntimeException(e);
		}
	}

	/**
	 * Disconnect.
	 */
	public void disconnect() {
		if (connector != null) {
			try {
				connector.close();
			} catch (Exception e) {
				throw new MBeanRuntimeException(e);
			} finally {
				connector = null;
			}
		}
	}

	@Override
	protected MBeanServerConnection getConnection() {
		if (connector == null) {
			throw new IllegalStateException("not connected to: " + url);
		}
		try {
			return connector.getMBeanServerConnection();
		} catch (Exception e) {
			throw new MBeanRuntimeException(e);
		}
	}

}
