package com.javabi.common.mbean;

import javax.management.Attribute;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

import com.javabi.common.lang.clazz.ClassResolver;

/**
 * An MBean Attribute.
 */
public class MBeanAttribute {

	/** The underlying info. */
	private final MBeanAttributeInfo info;
	/** The instance. */
	private final MBeanInstance instance;

	/**
	 * Creates a new attribute.
	 * @param info the underlying info.
	 * @param instance the instance.
	 */
	public MBeanAttribute(MBeanAttributeInfo info, MBeanInstance instance) {
		if (info == null || instance == null) {
			throw new NullPointerException();
		}
		this.info = info;
		this.instance = instance;
	}

	/**
	 * Returns the instance.
	 * @return the instance.
	 */
	public MBeanInstance getInstance() {
		return instance;
	}

	/**
	 * Returns the attribute name.
	 * @return the attribute name.
	 */
	public String getName() {
		return info.getName();
	}

	/**
	 * Returns the description.
	 * @return the description.
	 */
	public String getDescription() {
		return info.getDescription();
	}

	/**
	 * Returns the attribute type name.
	 * @return the attribute type name.
	 */
	public String getTypeName() {
		return info.getType();
	}

	/**
	 * Returns the attribute type class.
	 * @return the attribute type class.
	 */
	public Class<?> getTypeClass() {
		return new ClassResolver().resolveClass(getTypeName());
	}

	/**
	 * Returns true if this attribute has a setter.
	 * @return true if this attribute has a setter.
	 */
	public boolean hasSetter() {
		return info.isWritable();
	}

	/**
	 * Returns true if this attribute has a getter.
	 * @return true if this attribute has a getter.
	 */
	public boolean hasGetter() {
		return info.isReadable();
	}

	@Override
	public String toString() {
		return getName();
	}

	/**
	 * Returns the value.
	 * @return the value.
	 */
	public Object getValue() {
		if (!hasGetter()) {
			throw new IllegalStateException(getName() + " does not have a getter");
		}
		MBeanServerConnection connection = getInstance().getDomain().getConnection();
		ObjectName name = getInstance().getObjectName();
		try {
			return connection.getAttribute(name, getName());
		} catch (Exception e) {
			throw new MBeanRuntimeException(e);
		}
	}

	/**
	 * Set the attribute value.
	 * @param value the value.
	 */
	public void setValue(Object value) {
		if (!hasSetter()) {
			throw new IllegalStateException(getName() + " does not have a setter");
		}
		MBeanServerConnection connection = getInstance().getDomain().getConnection();
		ObjectName name = getInstance().getObjectName();
		Attribute attribute = new Attribute(getName(), value);
		try {
			connection.setAttribute(name, attribute);
		} catch (Exception e) {
			throw new MBeanRuntimeException(e);
		}
	}

	@Override
	public int hashCode() {
		return getName().hashCode();
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object instanceof MBeanAttribute) {
			MBeanAttribute attribute = (MBeanAttribute) object;
			return getName().equals(attribute.getName());
		}
		return false;
	}
}
