package com.javabi.common.mbean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.ObjectName;

/**
 * An MBean Instance.
 */
public class MBeanInstance {

	/** The object name. */
	private final ObjectName name;
	/** The underlying info. */
	private final MBeanInfo info;
	/** The domain. */
	private final MBeanDomain domain;
	/** The attribute list. */
	private final List<MBeanAttribute> attributeList = new ArrayList<MBeanAttribute>();
	/** The operation list. */
	private final List<MBeanOperation> operationList = new ArrayList<MBeanOperation>();

	/**
	 * Creates a new instance.
	 * @param name the name.
	 * @param info the info.
	 * @param domain the domain.
	 */
	public MBeanInstance(ObjectName name, MBeanInfo info, MBeanDomain domain) {
		if (name == null || info == null || domain == null) {
			throw new NullPointerException();
		}
		this.name = name;
		this.info = info;
		this.domain = domain;

		// Attributes
		for (MBeanAttributeInfo attribute : info.getAttributes()) {
			attributeList.add(new MBeanAttribute(attribute, this));
		}

		// Operations
		for (MBeanOperationInfo operation : info.getOperations()) {
			operationList.add(new MBeanOperation(operation, this));
		}
	}

	/**
	 * Returns the domain.
	 * @return the domain.
	 */
	public MBeanDomain getDomain() {
		return domain;
	}

	/**
	 * Returns the info.
	 * @return the info.
	 */
	public MBeanInfo getInfo() {
		return info;
	}

	/**
	 * Returns the object name.
	 * @return the object name.
	 */
	public ObjectName getObjectName() {
		return name;
	}

	/**
	 * Returns the attribute list.
	 * @return the attribute list.
	 */
	public List<MBeanAttribute> getAttributeList() {
		return Collections.unmodifiableList(attributeList);
	}

	/**
	 * Returns the operation list.
	 * @return the operation list.
	 */
	public List<MBeanOperation> getOperationList() {
		return Collections.unmodifiableList(operationList);
	}

	@Override
	public String toString() {
		return name.toString();
	}

	/**
	 * Attempts to resolve the type of this instance.
	 * @return the resolved type.
	 */
	public Class<?> getType() {
		try {
			String className = getInfo().getClassName() + "MBean";
			return Class.forName(className);
		} catch (Exception e) {
			throw new MBeanRuntimeException(e);
		}
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object instanceof MBeanInstance) {
			MBeanInstance instance = (MBeanInstance) object;
			if (!name.equals(instance.name)) {
				return false;
			}
			if (!getAttributeList().equals(instance.getAttributeList())) {
				return false;
			}
			if (!getOperationList().equals(instance.getOperationList())) {
				return false;
			}
			return true;
		}
		return false;
	}
}
