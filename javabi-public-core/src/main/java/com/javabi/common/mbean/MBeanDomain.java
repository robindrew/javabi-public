package com.javabi.common.mbean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.management.MBeanServerConnection;

/**
 * An MBean Domain.
 */
public class MBeanDomain implements Iterable<MBeanInstance> {

	/** The name. */
	private final String name;
	/** The connection. */
	private final MBeanServerConnection connection;
	/** The instance list. */
	private final List<MBeanInstance> instanceList = new ArrayList<MBeanInstance>();

	/**
	 * Creates a new domain.
	 * @param name the domain name.
	 * @param connection the connection.
	 */
	public MBeanDomain(String name, MBeanServerConnection connection) {
		if (connection == null) {
			throw new NullPointerException();
		}
		if (name.length() == 0) {
			throw new IllegalArgumentException("name is empty");
		}
		this.name = name;
		this.connection = connection;
	}

	/**
	 * Returns the name of the domain.
	 * @return the name of the domain.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the connection.
	 * @return the connection.
	 */
	public MBeanServerConnection getConnection() {
		return connection;
	}

	/**
	 * Returns the instance list.
	 * @return the instance list.
	 */
	public List<MBeanInstance> getInstanceList() {
		return Collections.unmodifiableList(instanceList);
	}

	/**
	 * Returns an iterator over the instances.
	 * @return an iterator over the instances.
	 */
	public Iterator<MBeanInstance> iterator() {
		return getInstanceList().iterator();
	}

	/**
	 * Add the given instance to the domain.
	 * @param instance the instance.
	 */
	public void addInstance(MBeanInstance instance) {
		if (instance == null) {
			throw new NullPointerException();
		}
		instanceList.add(instance);
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object instanceof MBeanDomain) {
			MBeanDomain domain = (MBeanDomain) object;
			return name.equals(domain.name) && getInstanceList().equals(domain.getInstanceList());
		}
		return false;
	}
}
