package com.javabi.common.xml.reflector.type;

public class StringType extends XmlType {

	public StringType() {
		super(String.class);
	}

}
