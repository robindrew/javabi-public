package com.javabi.common.xml.reflector;

import com.javabi.common.xml.IXmlBuilder;
import com.javabi.common.xml.IXmlWriter;
import com.javabi.common.xml.XmlBuilder;
import com.javabi.common.xml.reflector.type.IXmlType;
import com.javabi.common.xml.reflector.type.XmlType;

public class XmlReflector implements IXmlWriter<Object> {

	@Override
	public String toXml(Object value) {
		IXmlBuilder xml = new XmlBuilder();

		IXmlType type = XmlType.get(value);
		type.toXml(xml, value, null);

		return xml.toString();
	}

}
