package com.javabi.common.xml.dom;

public interface IDomAttribute {

	String getName();

	String getValue();

}
