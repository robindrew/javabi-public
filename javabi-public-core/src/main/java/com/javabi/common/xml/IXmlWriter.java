package com.javabi.common.xml;

public interface IXmlWriter<V> {

	String toXml(V value);

}