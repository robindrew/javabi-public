package com.javabi.common.xml.reflector.type;

import com.javabi.common.xml.IXmlBuilder;

public interface IXmlType {

	String getName();
	
	Class<?> getType();
	
	boolean isAttribute();

	void toXml(IXmlBuilder xml, Object instance, String name);

}
