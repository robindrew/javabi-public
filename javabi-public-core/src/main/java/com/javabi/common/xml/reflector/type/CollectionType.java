package com.javabi.common.xml.reflector.type;

import java.util.Collection;

import com.javabi.common.xml.IXmlBuilder;

public class CollectionType extends XmlType {

	public CollectionType(Class<?> type) {
		super(type);
	}

	@Override
	public void toXml(IXmlBuilder xml, Object instance, String name) {
		name = (name == null ? getName() : name);

		xml.open(name);
		Collection<?> collection = (Collection<?>) instance;
		if (collection.isEmpty()) {
			xml.close();
			return;
		}

		// Array Elements
		for (Object element : collection) {
			IXmlType component = XmlType.get(element);
			component.toXml(xml, element, component.getName());
		}

		xml.close();
	}
}
