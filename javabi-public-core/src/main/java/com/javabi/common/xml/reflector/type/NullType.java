package com.javabi.common.xml.reflector.type;

import com.javabi.common.xml.IXmlBuilder;

public class NullType extends XmlType {

	public NullType() {
		super(Void.class);
	}

	@Override
	public boolean isAttribute() {
		return true;
	}

	@Override
	public void toXml(IXmlBuilder xml, Object instance, String name) {
		xml.empty("null");
	}
}
