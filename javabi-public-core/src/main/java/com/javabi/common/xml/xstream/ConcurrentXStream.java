package com.javabi.common.xml.xstream;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.Collection;

public class ConcurrentXStream extends AliasXStream {

	public void alias(Class<?> type) {
		synchronized (this) {
			super.alias(type);
		}
	}

	public void aliasAll(Collection<? extends Class<?>> types) {
		synchronized (this) {
			super.aliasAll(types);
		}
	}

	public void attribute(Class<?> type) {
		synchronized (this) {
			super.attribute(type);
		}
	}

	public <T> String toXml(T type) {
		synchronized (this) {
			return super.toXml(type);
		}
	}

	public <T> void toXml(T type, OutputStream output) {
		synchronized (this) {
			super.toXml(type, output);
		}
	}

	public <T> void toXml(T type, Writer writer) {
		synchronized (this) {
			super.toXml(type, writer);
		}
	}

	public <T> T fromXml(String xml) {
		synchronized (this) {
			return super.fromXml(xml);
		}
	}

	public <T> T fromXml(InputStream input) {
		synchronized (this) {
			return super.fromXml(input);
		}
	}

	public <T> T fromXml(Reader reader) {
		synchronized (this) {
			return super.fromXml(reader);
		}
	}

}
