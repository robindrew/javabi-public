package com.javabi.common.xml.reflector.type;

import java.util.ArrayList;
import java.util.List;

import com.javabi.common.lang.reflect.field.FieldLister;
import com.javabi.common.lang.reflect.field.IField;
import com.javabi.common.xml.IXmlBuilder;

public class FieldType extends XmlType {

	private final List<IField> attributeList = new ArrayList<>();
	private final List<IField> elementList = new ArrayList<>();

	public FieldType(Class<?> type) {
		super(type);

		for (IField field : new FieldLister().getFieldList(type)) {
			IXmlType fieldType = XmlType.get(field.getType());
			if (fieldType.isAttribute()) {
				attributeList.add(field);
			} else {
				elementList.add(field);
			}
		}
	}

	@Override
	public void toXml(IXmlBuilder xml, Object instance, String name) {
		name = (name == null ? getName() : name);

		xml.open(name);

		// Attributes
		for (IField attribute : attributeList) {
			toAttribute(xml, instance, attribute);
		}

		if (elementList.isEmpty()) {
			xml.close();
			return;
		}

		// Elements
		for (IField element : elementList) {
			toElement(xml, instance, element);
		}

		xml.close();
	}

	private void toElement(IXmlBuilder xml, Object instance, IField field) {
		Object value = field.get(instance);
		IXmlType type = XmlType.get(value);
		type.toXml(xml, value, field.getName());
	}

	private void toAttribute(IXmlBuilder xml, Object instance, IField field) {
		Object value = field.get(instance);
		xml.attribute(field.getName(), value);
	}
}
