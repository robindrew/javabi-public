package com.javabi.common.xml.reflector.type;

public class EnumType extends XmlType {

	public EnumType(Class<?> type) {
		super(type);
	}

	@Override
	public boolean isAttribute() {
		return true;
	}
}
