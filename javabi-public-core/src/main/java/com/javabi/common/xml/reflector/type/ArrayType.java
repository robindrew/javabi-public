package com.javabi.common.xml.reflector.type;

import com.javabi.common.xml.IXmlBuilder;

public class ArrayType extends XmlType {

	public ArrayType(Class<?> type) {
		super(type);
	}

	@Override
	public void toXml(IXmlBuilder xml, Object instance, String name) {
		name = (name == null ? getName() : name);

		xml.open(name);
		Object[] array = (Object[]) instance;
		if (array.length == 0) {
			xml.close();
			return;
		}

		// Array Elements
		for (Object element : array) {
			IXmlType component = XmlType.get(element);
			component.toXml(xml, element, component.getName());
		}

		xml.close();
	}
}
