package com.javabi.common.xml.reflector.type;

public class PrimitiveType extends XmlType {

	public PrimitiveType(Class<?> type) {
		super(type);
	}

	@Override
	public boolean isAttribute() {
		return true;
	}
}
