package com.javabi.common.xml.reflector.type;

public class NumberType extends XmlType {

	public NumberType(Class<?> type) {
		super(type);
	}

	@Override
	public boolean isAttribute() {
		return true;
	}
}
