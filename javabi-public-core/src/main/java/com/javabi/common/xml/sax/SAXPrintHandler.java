package com.javabi.common.xml.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXPrintHandler extends DefaultHandler {

	private int indent = 0;

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		System.out.println(startElementString(qName, attributes));
		indent++;
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		indent--;
		System.out.println(endElementString(qName));
	}

	public void characters(char[] text, int offset, int length) throws SAXException {
		String string = new String(text, offset, length);
		if (string.trim().isEmpty()) {
			return;
		}
		System.out.println(string);
	}

	private String startElementString(String element, Attributes attributes) {
		StringBuilder xml = new StringBuilder();
		appendIndex(xml);
		xml.append('<').append(element);
		appendAttributes(xml, attributes);
		xml.append('>');
		return xml.toString();
	}

	private void appendAttributes(StringBuilder xml, Attributes attributes) {
		if (attributes == null) {
			return;
		}
		for (int i = 0; i < attributes.getLength(); i++) {
			String attribute = attributes.getQName(i);
			String value = attributes.getValue(i);
			xml.append(' ').append(attribute).append('=');
			xml.append('\"').append(value).append('\"');
		}
	}

	private String endElementString(String element) {
		StringBuilder xml = new StringBuilder();
		appendIndex(xml);
		xml.append('<').append('/').append(element).append('>');
		return xml.toString();
	}

	private void appendIndex(StringBuilder xml) {
		for (int i = 0; i < indent; i++) {
			xml.append("   ");
		}
	}

}
