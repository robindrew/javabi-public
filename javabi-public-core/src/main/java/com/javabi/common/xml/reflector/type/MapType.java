package com.javabi.common.xml.reflector.type;

import java.util.Map;
import java.util.Map.Entry;

import com.javabi.common.xml.IXmlBuilder;

public class MapType extends XmlType {

	public MapType(Class<?> type) {
		super(type);
	}

	@Override
	public void toXml(IXmlBuilder xml, Object instance, String name) {
		name = (name == null ? getName() : name);

		xml.open(name);
		Map<?, ?> map = (Map<?, ?>) instance;
		if (map.isEmpty()) {
			xml.close();
			return;
		}

		// Array Elements
		for (Entry<?, ?> entry : map.entrySet()) {
			entryToXml(xml, entry);
		}

		xml.close();
	}

	private void entryToXml(IXmlBuilder xml, Entry<?, ?> entry) {

		Object key = entry.getKey();
		Object value = entry.getValue();

		IXmlType keyType = XmlType.get(key);
		IXmlType valueType = XmlType.get(value);

		xml.open("entry");

		keyType.toXml(xml, key, keyType.getName());
		valueType.toXml(xml, value, valueType.getName());

		xml.close();

	}
}
