package com.javabi.common.xml.dom;

import com.javabi.common.xml.IXmlBuilder;

public interface IDomFormatter {

	String format(String xml);

	String format(String xml, IXmlBuilder builder);

}
