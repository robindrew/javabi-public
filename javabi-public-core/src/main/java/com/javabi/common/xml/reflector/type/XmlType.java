package com.javabi.common.xml.reflector.type;

import java.util.Collection;
import java.util.Map;

import com.javabi.common.concurrent.copyonwrite.CopyOnWriteMap;
import com.javabi.common.xml.IXmlBuilder;

public class XmlType implements IXmlType {

	public static final Map<Class<?>, IXmlType> typeMap = new CopyOnWriteMap<>();

	public static final IXmlType get(Object instance) {
		if (instance == null) {
			return new NullType();
		}

		Class<?> type = (instance instanceof Class) ? (Class<?>) instance : instance.getClass();

		IXmlType xmlType = typeMap.get(type);
		if (xmlType == null) {
			xmlType = newXmlType(type);
			typeMap.put(type, xmlType);
		}
		return xmlType;
	}

	private static IXmlType newXmlType(Class<?> type) {
		if (type.isPrimitive()) {
			return new PrimitiveType(type);
		}
		if (type.isArray()) {
			return new ArrayType(type);
		}
		if (String.class.equals(type)) {
			return new StringType();
		}
		if (Number.class.isAssignableFrom(type)) {
			return new NumberType(type);
		}
		if (Enum.class.isAssignableFrom(type)) {
			return new EnumType(type);
		}
		if (Collection.class.isAssignableFrom(type)) {
			return new CollectionType(type);
		}
		if (Map.class.isAssignableFrom(type)) {
			return new MapType(type);
		}

		return new FieldType(type);
	}

	private final Class<?> type;
	private final String name;

	public XmlType(Class<?> type) {
		this.type = type;
		this.name = type.getSimpleName();
	}

	@Override
	public Class<?> getType() {
		return type;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean isAttribute() {
		return false;
	}

	@Override
	public void toXml(IXmlBuilder xml, Object instance, String name) {
		name = (name == null ? getName() : name);

		xml.element(name, instance);
	}

}
