package com.javabi.common.log.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.common.io.LineProcessor;
import com.javabi.common.log.level.LogLevel;
import com.javabi.common.log.level.LogLevelParser;

public class LogLineParser implements LineProcessor<List<LogLine>> {

	private final List<LogLine> lines = new ArrayList<LogLine>();
	private final LogLevel level;
	private final LogLevelParser parser;
	private LogLine lastLine = null;

	public LogLineParser(LogLevel level, LogLevelParser parser) {
		if (level == null) {
			throw new NullPointerException("level");
		}
		if (parser == null) {
			throw new NullPointerException("parser");
		}
		this.level = level;
		this.parser = parser;
	}

	public LogLineParser(String level, LogLevelParser parser) {
		this(LogLevel.valueOf(level), parser);
	}

	@Override
	public List<LogLine> getResult() {
		return lines;
	}

	@Override
	public boolean processLine(String line) throws IOException {
		LogLevel lineLevel = parser.parserLevel(line);

		// None?
		if (lineLevel.equals(LogLevel.NONE)) {
			if (lastLine != null) {
				lastLine.addLine(line);
			}
			return true;
		}

		// Matches?
		if (lineLevel.isGreaterThan(level)) {
			lastLine = new LogLine(line);
			lines.add(lastLine);
		}
		return true;
	}

}
