package com.javabi.common.log.parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.ImmutableList;

public class LogLine {

	private static final String NEW_LINE = System.getProperty("line.separator");

	private final String line;
	private List<String> detailList = Collections.emptyList();

	public LogLine(String line) {
		if (line == null) {
			throw new NullPointerException("line");
		}
		this.line = line;
	}

	public void addLine(String line) {
		if (detailList.isEmpty()) {
			detailList = new ArrayList<String>();
		}
		detailList.add(line);
	}

	public String getLine() {
		return line;
	}

	public List<String> getDetailList() {
		return ImmutableList.copyOf(detailList);
	}

	public String toString() {
		if (detailList.isEmpty()) {
			return line;
		}
		StringBuilder text = new StringBuilder();
		text.append(line);
		for (String detail : detailList) {
			text.append(NEW_LINE);
			text.append(detail);
		}
		return text.toString();
	}

}
