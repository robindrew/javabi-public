package com.javabi.common.log.level;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parse the {@link LogLevel} from a log line. Returns {@link LogLevel#NONE} if the line does not match.
 */
public class LogLevelParser {

	private static final Logger log = LoggerFactory.getLogger(LogLevelParser.class);

	public static final String DEFAULT_REGEX = "\\d+-\\d+-\\d+\\s+\\d+:\\d+:\\d+.\\d+\\s+\\[.+\\]\\s+([A-Z]+)\\s+.+";
	public static final int DEFAULT_GROUP = 1;

	private final Pattern pattern;
	private final int group;

	public LogLevelParser(String regex, int group) {
		this.pattern = Pattern.compile(regex);
		this.group = group;
	}

	public LogLevelParser() {
		this(DEFAULT_REGEX, DEFAULT_GROUP);
	}

	public LogLevel parserLevel(String line) {
		Matcher matcher = pattern.matcher(line);
		if (matcher.find()) {
			String level = matcher.group(group);
			try {
				return LogLevel.valueOf(level);
			} catch (Exception e) {
				log.warn("Unable to parse line: " + line, e);
				return LogLevel.NONE;
			}
		}
		// This is NOT a log line (in the format we expect)
		return LogLevel.NONE;
	}

	public static void main(String[] args) {
		String line = "15-04-30 19:09:27.916 [CommandHandler-3] WARN c.j.service.process.ProcessExecutor - Executing command 'ps auxwww'";
		LogLevelParser parser = new LogLevelParser();
		System.out.println(parser.parserLevel(line));
	}
}
