package com.javabi.common.log.timing;

import com.javabi.common.date.timer.ITimer;

public interface ITimingLog {

	String getName();

	void debug(Object text, ITimer timer);

	void info(Object text, ITimer timer);

	void warn(Object text, ITimer timer);

	void error(Object text, ITimer timer);

}
