package com.javabi.common.log.timing;

import com.javabi.common.date.timer.ITimer;

public class TimingLog implements ITimingLog {

	private final String name;

	public TimingLog(String name) {
		if (name.isEmpty()) {
			throw new IllegalArgumentException("name is empty");
		}
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void debug(Object text, ITimer timer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void info(Object text, ITimer timer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void warn(Object text, ITimer timer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void error(Object text, ITimer timer) {
		// TODO Auto-generated method stub

	}

}
