package com.javabi.common.log.timing;

public interface ITimingLogFactory {

	ITimingLog getLog(String key);

	ITimingLog getLog(Class<?> key);

}
