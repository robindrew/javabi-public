package com.javabi.common.log.level;

public enum LogLevel {

	NONE(0), TRACE(1), DEBUG(2), INFO(3), WARN(4), ERROR(5), FATAL(6), ALL(7);

	private final int level;

	private LogLevel(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}

	public boolean isGreaterThan(LogLevel level) {
		return getLevel() >= level.getLevel();
	}

}
