package com.javabi.common.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;

/**
 * A Swing Utility.
 */
public class SwingUtil {

	/**
	 * Center the given frame.
	 * @param component the component.
	 */
	public static final void center(Component component) {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		int x = ((int) screenSize.getWidth() - component.getWidth()) / 2;
		int y = ((int) screenSize.getHeight() - component.getHeight()) / 2;
		component.setLocation(x, y);
	}

	/**
	 * Center the given frame.
	 * @param component the component.
	 * @param xOffset the x offset.
	 * @param yOffset the y offset.
	 */
	public static final void center(Component component, int xOffset, int yOffset) {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		int x = ((int) screenSize.getWidth() - component.getWidth()) / 2;
		int y = ((int) screenSize.getHeight() - component.getHeight()) / 2;
		component.setLocation(x + xOffset, y + yOffset);
	}

	/**
	 * Center the given frame.
	 * @param component the component.
	 * @param xOffset the x offset.
	 */
	public static final void centerHorizontally(Component component, int xOffset) {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		int x = ((int) screenSize.getWidth() - component.getWidth()) / 2;
		component.setLocation(x + xOffset, component.getY());
	}

	/**
	 * Center the given frame.
	 * @param component the component.
	 * @param yOffset the y offset.
	 */
	public static final void centerVertically(Component component, int yOffset) {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		int y = ((int) screenSize.getHeight() - component.getHeight()) / 2;
		component.setLocation(component.getX(), y + yOffset);
	}

}
