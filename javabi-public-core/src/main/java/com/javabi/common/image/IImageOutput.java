package com.javabi.common.image;

import java.awt.image.BufferedImage;
import java.io.File;

public interface IImageOutput {

	float getQuality();

	void setQuality(float quality);

	void setFormat(ImageFormat format);

	BufferedImage getImage();

	ImageFormat getFormat();

	void writeToFile(File file);

	byte[] writeToByteArray();

}
