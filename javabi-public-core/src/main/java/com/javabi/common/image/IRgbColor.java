package com.javabi.common.image;

import java.awt.Color;

public interface IRgbColor {

	int getRed();

	int getGreen();

	int getBlue();

	Color toColor();

}
