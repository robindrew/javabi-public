package com.javabi.common.image;

import java.awt.Color;
import java.awt.image.BufferedImage;

public interface IImageResizer {

	BufferedImage resize(BufferedImage image, int width, int height);

	void setImageType(int imageType);

	void setBackground(Color background);

}
