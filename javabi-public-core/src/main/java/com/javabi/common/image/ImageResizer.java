package com.javabi.common.image;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageResizer implements IImageResizer {

	private static final Logger log = LoggerFactory.getLogger(ImageResizer.class);

	private int imageType = BufferedImage.TYPE_INT_ARGB;
	private Color background = null;

	public BufferedImage resize(BufferedImage image, int width, int height) {

		int oldWidth = image.getWidth();
		int oldHeight = image.getHeight();
		int newWidth = oldWidth;
		int newHeight = oldHeight;

		Image scaled = image;
		if (newWidth > width || newHeight > height) {

			// Factors
			float widthFactor = (float) oldWidth / (float) width;
			float heightFactor = (float) oldHeight / (float) height;
			float factor = widthFactor > heightFactor ? widthFactor : heightFactor;
			newWidth = (int) (oldWidth / factor);
			newHeight = (int) (oldHeight / factor);

			// Scale
			scaled = image.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
		}

		int x = 0;
		if (width > newWidth) {
			x = (width - newWidth) / 2;
		}

		int y = 0;
		if (height > newHeight) {
			y = (height - newHeight) / 2;
		}

		BufferedImage newImage = new BufferedImage(width, height, imageType);
		Graphics2D graphics = newImage.createGraphics();
		if (background != null) {
			graphics.setBackground(background);
		}
		graphics.drawImage(scaled, x, y, null);
		graphics.dispose();

		return newImage;
	}

	public void setImageType(int imageType) {
		this.imageType = imageType;
	}

	public void setBackground(Color background) {
		this.background = background;
	}

	public static void main(String[] args) throws Exception {
		File file = new File("c:/temp/temp/0020.jpg");
		BufferedImage image1 = ImageIO.read(file);
		BufferedImage image2 = new ImageResizer().resize(image1, 150, 150);

		log.info("Writing image ...");
		IImageOutput output = new ImageOutput(image2, ImageFormat.PNG);
		output.writeToFile(new File("c:/temp/temp/0021.png"));
	}

}
