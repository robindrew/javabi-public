package com.javabi.common.image;

import java.awt.Image;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.ImageObserver;

public class ColorToGrayImageConverter implements ImageObserver {

	public BufferedImage convert(BufferedImage colorImage) {
		return convert(colorImage, this);
	}

	public BufferedImage convert(BufferedImage colorImage, ImageObserver observer) {
		int width = colorImage.getWidth();
		int height = colorImage.getHeight();
		BufferedImage grayImage = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR_PRE);
		grayImage.createGraphics().drawImage(colorImage, 0, 0, width, height, observer);

		ColorSpace space = ColorSpace.getInstance(ColorSpace.CS_GRAY);
		ColorConvertOp convert = new ColorConvertOp(space, grayImage.getColorModel().getColorSpace(), null);
		convert.filter(grayImage, grayImage);

		return grayImage;
	}

	@Override
	public boolean imageUpdate(Image image, int flags, int x, int y, int width, int height) {
		return false;
	}
}
