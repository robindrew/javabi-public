package com.javabi.common.json;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import com.javabi.common.lang.reflect.field.FieldLister;
import com.javabi.common.lang.reflect.field.IField;

public class JsonReflector implements IJsonWriter<Object> {

	@Override
	public String toJson(Object object) {
		IJsonBuilder builder = new JsonBuilder();
		toJson(object, builder);
		return builder.toJson();
	}

	private IJsonBuilder toJson(Object object, IJsonBuilder builder) {

		// Simple types
		if (object == null) {
			return builder.value((Object) null);
		}
		Class<?> type = object.getClass();
		if (Enum.class.isAssignableFrom(type)) {
			return builder.value(object);
		}
		if (Number.class.isAssignableFrom(type)) {
			return builder.value(object);
		}
		if (type.equals(Character.class)) {
			return builder.value(object);
		}
		if (type.equals(Boolean.class)) {
			return builder.value(object);
		}
		if (object instanceof String) {
			return builder.value((String) object);
		}

		// Complex types
		if (type.isArray()) {
			return toArray(object, builder);
		}
		if (Collection.class.isAssignableFrom(type)) {
			return toArray((Collection<?>) object, builder);
		}
		if (Map.class.isAssignableFrom(type)) {
			return toMap((Map<?, ?>) object, builder);
		}

		// Object
		return toObject(object, builder);
	}

	private IJsonBuilder toObject(Object object, IJsonBuilder builder) {
		builder.openObject();
		Class<?> type = object.getClass();
		boolean comma = false;
		for (IField field : new FieldLister().getFieldList(type)) {
			if (comma) {
				builder.comma();
			}
			comma = true;
			builder.value(field.getName());
			builder.colon();

			// Value
			Object value = field.get(object);
			toJson(value, builder);
		}
		builder.closeObject();
		return builder;
	}

	private IJsonBuilder toMap(Map<?, ?> map, IJsonBuilder builder) {
		boolean comma = false;
		builder.openObject();
		for (Entry<?, ?> entry : map.entrySet()) {
			if (comma) {
				builder.comma();
			}
			comma = true;
			toJson(entry.getKey(), builder);
			builder.colon();
			toJson(entry.getValue(), builder);
		}
		builder.closeObject();
		return builder;
	}

	private IJsonBuilder toArray(Collection<?> collection, IJsonBuilder builder) {
		builder.openArray();
		boolean comma = false;
		for (Object element : collection) {
			if (comma) {
				builder.comma();
			}
			comma = true;
			toJson(element, builder);
		}
		builder.closeArray();
		return builder;
	}

	private IJsonBuilder toArray(Object array, IJsonBuilder builder) {
		builder.openArray();
		int length = Array.getLength(array);
		for (int i = 0; i < length; i++) {
			if (i > 0) {
				builder.comma();
			}
			Object element = Array.get(array, i);
			toJson(element, builder);
		}
		builder.closeArray();
		return builder;
	}

}
