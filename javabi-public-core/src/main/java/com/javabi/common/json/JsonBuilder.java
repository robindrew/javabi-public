package com.javabi.common.json;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

public class JsonBuilder implements IJsonBuilder {

	private static final char[] hex = "0123456789ABCDEF".toCharArray();

	private final StringBuilder json;

	public JsonBuilder(StringBuilder json) {
		this.json = json;
	}

	public JsonBuilder() {
		this(new StringBuilder());
	}

	@Override
	public String toJson() {
		return json.toString();
	}

	@Override
	public IJsonBuilder openObject() {
		json.append('{');
		return this;
	}

	@Override
	public IJsonBuilder closeObject() {
		json.append('}');
		return this;
	}

	@Override
	public IJsonBuilder openArray() {
		json.append('[');
		return this;
	}

	@Override
	public IJsonBuilder closeArray() {
		json.append(']');
		return this;
	}

	@Override
	public IJsonBuilder comma() {
		json.append(',');
		return this;
	}

	@Override
	public IJsonBuilder colon() {
		json.append(':');
		return this;
	}

	@Override
	public IJsonBuilder object(String name, Object value) {
		openObject();
		value(name);
		colon();
		value(value);
		closeObject();
		return this;
	}

	@Override
	public IJsonBuilder object(Map<String, ? extends Object> map) {
		openObject();
		boolean first = true;
		for (Entry<String, ?> entry : map.entrySet()) {
			first = comma(first);
			value(entry.getKey());
			colon();
			value(entry.getValue());
		}
		closeObject();
		return this;
	}

	private boolean comma(boolean first) {
		if (!first) {
			json.append(',');
		}
		return false;
	}

	@Override
	public IJsonBuilder value(byte value) {
		json.append(value);
		return this;
	}

	@Override
	public IJsonBuilder value(short value) {
		json.append(value);
		return this;
	}

	@Override
	public IJsonBuilder value(int value) {
		json.append(value);
		return this;
	}

	@Override
	public IJsonBuilder value(long value) {
		json.append(value);
		return this;
	}

	@Override
	public IJsonBuilder value(float value) {
		json.append(value);
		return this;
	}

	@Override
	public IJsonBuilder value(double value) {
		json.append(value);
		return this;
	}

	@Override
	public IJsonBuilder value(boolean value) {
		json.append(value);
		return this;
	}

	@Override
	public IJsonBuilder value(char value) {
		value(String.valueOf(value));
		return this;
	}

	@Override
	public IJsonBuilder value(String value) {
		json.append('"');
		for (int i = 0; i < value.length(); i++) {
			char c = value.charAt(i);
			encode(c);
		}
		json.append('"');
		return this;
	}

	private StringBuilder encode(char c) {
		switch (c) {
			case '"':
				return json.append("\\\"");
			case '\\':
				return json.append("\\\\");
			case '/':
				return json.append("\\/");
			case '\b':
				return json.append("\\b");
			case '\f':
				return json.append("\\f");
			case '\n':
				return json.append("\\n");
			case '\r':
				return json.append("\\r");
			case '\t':
				return json.append("\\t");
			default:
				// Intended fall through
		}
		if (Character.isISOControl(c)) {
			unicode(c);
		} else {
			json.append(c);
		}
		return json;
	}

	private void unicode(char c) {
		json.append("\\u");
		int number = c;
		for (int i = 0; i < 4; ++i) {
			int digit = (number & 0xf000) >> 12;
			json.append(hex[digit]);
			number <<= 4;
		}
	}

	@Override
	public <E extends Enum<E>> IJsonBuilder value(E value) {
		value(value == null ? null : value.name());
		return this;
	}

	@Override
	public IJsonBuilder array(byte... array) {
		openArray();
		boolean first = true;
		for (byte element : array) {
			first = comma(first);
			value(element);
		}
		closeArray();
		return this;
	}

	@Override
	public IJsonBuilder array(short... array) {
		openArray();
		boolean first = true;
		for (short element : array) {
			first = comma(first);
			value(element);
		}
		closeArray();
		return this;
	}

	@Override
	public IJsonBuilder array(int... array) {
		openArray();
		boolean first = true;
		for (int element : array) {
			first = comma(first);
			value(element);
		}
		closeArray();
		return this;
	}

	@Override
	public IJsonBuilder array(long... array) {
		openArray();
		boolean first = true;
		for (long element : array) {
			first = comma(first);
			value(element);
		}
		closeArray();
		return this;
	}

	@Override
	public IJsonBuilder array(float... array) {
		openArray();
		boolean first = true;
		for (float element : array) {
			first = comma(first);
			value(element);
		}
		closeArray();
		return this;
	}

	@Override
	public IJsonBuilder array(double... array) {
		openArray();
		boolean first = true;
		for (double element : array) {
			first = comma(first);
			value(element);
		}
		closeArray();
		return this;
	}

	@Override
	public IJsonBuilder array(boolean... array) {
		openArray();
		boolean first = true;
		for (boolean element : array) {
			first = comma(first);
			value(element);
		}
		closeArray();
		return this;
	}

	@Override
	public IJsonBuilder array(char... array) {
		openArray();
		boolean first = true;
		for (char element : array) {
			first = comma(first);
			value(element);
		}
		closeArray();
		return this;
	}

	@Override
	public IJsonBuilder array(String... array) {
		openArray();
		boolean first = true;
		for (String element : array) {
			first = comma(first);
			value(element);
		}
		closeArray();
		return this;
	}

	@Override
	public IJsonBuilder array(Object... array) {
		openArray();
		boolean first = true;
		for (Object element : array) {
			first = comma(first);
			value(element);
		}
		closeArray();
		return this;
	}

	@Override
	public IJsonBuilder array(Collection<? extends Object> array) {
		openArray();
		boolean first = true;
		for (Object element : array) {
			first = comma(first);
			value(element);
		}
		closeArray();
		return this;
	}

	@Override
	public IJsonBuilder value(Object value) {
		if (value == null) {
			json.append("null");
			return this;
		}
		if (value instanceof CharSequence) {
			return value(value.toString());
		}
		if (value instanceof Enum) {
			return value(((Enum<?>) value).name());
		}
		if (value instanceof Boolean) {
			return value(((Boolean) value).booleanValue());
		}
		if (value instanceof Byte) {
			return value(((Byte) value).byteValue());
		}
		if (value instanceof Short) {
			return value(((Short) value).shortValue());
		}
		if (value instanceof Integer) {
			return value(((Integer) value).intValue());
		}
		if (value instanceof Long) {
			return value(((Long) value).longValue());
		}
		if (value instanceof Float) {
			return value(((Float) value).floatValue());
		}
		if (value instanceof Double) {
			return value(((Double) value).doubleValue());
		}
		if (value instanceof Character) {
			return value(((Character) value).charValue());
		}
		throw new IllegalArgumentException("type not supported: " + value.getClass());
	}
}
