package com.javabi.common.json;

public interface IJsonWriter<V> {

	String toJson(V value);

}
