package com.javabi.common.json;

import java.util.Collection;
import java.util.Map;

public interface IJsonBuilder {

	String toJson();

	IJsonBuilder openObject();

	IJsonBuilder closeObject();

	IJsonBuilder openArray();

	IJsonBuilder closeArray();

	IJsonBuilder comma();

	IJsonBuilder colon();

	IJsonBuilder object(Map<String, ? extends Object> map);

	IJsonBuilder object(String name, Object value);

	IJsonBuilder value(byte value);

	IJsonBuilder value(short value);

	IJsonBuilder value(int value);

	IJsonBuilder value(long value);

	IJsonBuilder value(float value);

	IJsonBuilder value(double value);

	IJsonBuilder value(boolean value);

	IJsonBuilder value(char value);

	IJsonBuilder value(String value);

	IJsonBuilder value(Object value);

	<E extends Enum<E>> IJsonBuilder value(E value);

	IJsonBuilder array(byte... array);

	IJsonBuilder array(short... array);

	IJsonBuilder array(int... array);

	IJsonBuilder array(long... array);

	IJsonBuilder array(float... array);

	IJsonBuilder array(double... array);

	IJsonBuilder array(boolean... array);

	IJsonBuilder array(char... array);

	IJsonBuilder array(String... array);

	IJsonBuilder array(Object... array);

	IJsonBuilder array(Collection<? extends Object> array);

}
