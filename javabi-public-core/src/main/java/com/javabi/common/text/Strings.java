package com.javabi.common.text;

import java.util.List;

public class Strings {

	private static volatile IStrings instance = new StringsImpl();

	public static IStrings getStrings() {
		return instance;
	}

	public static void setStrings(IStrings strings) {
		if (strings == null) {
			throw new NullPointerException("strings");
		}
		instance = strings;
	}

	private Strings() {
		// Inaccessible constructor
	}

	public static String valueOf(Object object) {
		return instance.valueOf(object);
	}

	public static String valueOf(Throwable t) {
		return instance.valueOf(t);
	}

	public static List<String> splitOnWhitespace(String text) {
		return instance.splitOnWhitespace(text);
	}

	public static int indexOfIgnoreCase(CharSequence haystack, String needle, int fromIndex) {
		return instance.indexOfIgnoreCase(haystack, needle, fromIndex);
	}

	public static String trim(String text, int maxLength) {
		return instance.trim(text, maxLength);
	}

	public static String valueOf(byte value) {
		return instance.valueOf(value);
	}

	public static String valueOf(short value) {
		return instance.valueOf(value);
	}

	public static String valueOf(int value) {
		return instance.valueOf(value);
	}

	public static String valueOf(long value) {
		return instance.valueOf(value);
	}

	public static String trimEnd(String text, String from, boolean includeFrom) {
		return instance.trimEnd(text, from, includeFrom);
	}

	public static String trimEnd(String text, String from) {
		return instance.trimEnd(text, from);
	}

	public static int indexOfWhitespace(CharSequence text, int index) {
		return instance.indexOfWhitespace(text, index);
	}

	public static int indexOfWhitespace(CharSequence text) {
		return instance.indexOfWhitespace(text);
	}

	public static String substringEnd(String text, String from, boolean includeFrom) {
		return instance.substringEnd(text, from, includeFrom);
	}

	public static String substringEnd(String text, String from) {
		return instance.substringEnd(text, from);
	}

	public static int indexOfNonWhitespace(CharSequence text, int index) {
		return instance.indexOfNonWhitespace(text, index);
	}

	public static int toDigit(char c) {
		return instance.toDigit(c);
	}

}
