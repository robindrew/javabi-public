package com.javabi.common.text.parser;

public interface IStringParser<P> {

	P parse(String text);

}
