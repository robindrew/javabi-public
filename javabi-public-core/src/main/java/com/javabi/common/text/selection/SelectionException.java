package com.javabi.common.text.selection;

public class SelectionException extends RuntimeException {

	public SelectionException(String message) {
		super(message);
	}

}
