package com.javabi.common.text;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * A String Factory.
 */
public abstract class StringFactory {

	/** The instance. */
	private static volatile StringFactory instance = new StringFactory() {

		@Override
		public String toString(Object object) {
			return ToStringBuilder.reflectionToString(object, ToStringStyle.SIMPLE_STYLE, true);
		}
	};

	/**
	 * Returns the factory.
	 * @return the factory.
	 */
	public static StringFactory getInstance() {
		return instance;
	}

	/**
	 * Set the instance.
	 * @param factory the factory.
	 */
	public static void setInstance(StringFactory factory) {
		if (factory == null) {
			throw new NullPointerException();
		}
		instance = factory;
	}

	/**
	 * Returns the given object as a string.
	 * @param object the object.
	 * @return the string.
	 */
	public abstract String toString(Object object);
}
