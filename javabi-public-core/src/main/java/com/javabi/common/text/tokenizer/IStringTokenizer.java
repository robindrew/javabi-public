package com.javabi.common.text.tokenizer;

public interface IStringTokenizer {

	boolean hasNext();

	String next(boolean optional);

}
