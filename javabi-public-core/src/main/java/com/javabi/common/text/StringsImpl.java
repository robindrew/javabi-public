package com.javabi.common.text;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Throwables;

public class StringsImpl implements IStrings {

	/** The length of the number caches. */
	private static final int CACHE_LENGTH = 10000;
	/** The positive whole number cache. */
	private static final String[] POSITIVE_STRING_CACHE = new String[CACHE_LENGTH];
	/** The negative whole number cache. */
	private static final String[] NEGATIVE_STRING_CACHE = new String[CACHE_LENGTH];

	/**
	 * Initialise and intern String caches.
	 */
	static {
		for (int i = 0; i < CACHE_LENGTH; i++) {
			int value = i;
			POSITIVE_STRING_CACHE[i] = String.valueOf(value).intern();
			NEGATIVE_STRING_CACHE[i] = String.valueOf(-value).intern();
		}
	}

	@Override
	public final List<String> splitOnWhitespace(String text) {
		text = text.trim();

		List<String> tokens = new ArrayList<>();
		StringBuilder token = new StringBuilder();
		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			if (Character.isWhitespace(c)) {
				if (token.length() > 0) {
					tokens.add(token.toString());
				}
				token.setLength(0);
			} else {
				token.append(c);
			}
		}
		if (token.length() > 0) {
			tokens.add(token.toString());
		}
		return tokens;
	}

	/**
	 * Returns the cached string value of the given value.
	 * @param value the value.
	 * @return the string.
	 */
	@Override
	public final String valueOf(byte value) {
		return valueOf((int) value);
	}

	/**
	 * Returns the cached string value of the given value.
	 * @param value the value.
	 * @return the string.
	 */
	@Override
	public final String valueOf(short value) {
		return valueOf((int) value);
	}

	/**
	 * Returns the cached string value of the given value.
	 * @param value the value.
	 * @return the string.
	 */
	@Override
	public final String valueOf(int value) {
		if (value >= 0 && value < CACHE_LENGTH) {
			return POSITIVE_STRING_CACHE[value];
		}
		if (value <= 0 && value > -CACHE_LENGTH) {
			return NEGATIVE_STRING_CACHE[-value];
		}
		return String.valueOf(value);
	}

	/**
	 * Returns the cached string value of the given value.
	 * @param value the value.
	 * @return the string.
	 */
	@Override
	public final String valueOf(long value) {
		if (value >= 0 && value < CACHE_LENGTH) {
			return POSITIVE_STRING_CACHE[(int) value];
		}
		if (value <= 0 && value > -CACHE_LENGTH) {
			return NEGATIVE_STRING_CACHE[-(int) value];
		}
		return String.valueOf(value);
	}

	/**
	 * Returns the cached string value of the given object.
	 * @param object the object.
	 * @return the string.
	 */
	@Override
	public final String valueOf(Object object) {
		if (object == null) {
			return "null";
		}
		if (object instanceof Throwable) {
			return Throwables.getStackTraceAsString((Throwable) object);
		}
		return object.toString();
	}

	@Override
	public String valueOf(Throwable t) {
		return Throwables.getStackTraceAsString(t);
	}

	@Override
	public int indexOfIgnoreCase(CharSequence haystack, String needle, int index) {
		if (needle.length() == 0) {
			return index;
		}
		if (needle.length() > haystack.length()) {
			return -1;
		}
		int subIndex = 0;
		for (int i = index; i < haystack.length(); i++) {
			char c1 = Character.toLowerCase(haystack.charAt(i));
			char c2 = Character.toLowerCase(needle.charAt(subIndex));
			if (c1 == c2) {
				subIndex++;
				if (subIndex == needle.length()) {
					return i - subIndex + 1;
				}
			} else {
				subIndex = 0;
			}
		}
		return -1;
	}

	@Override
	public String trim(String text, int maxLength) {
		if (maxLength < 0) {
			throw new IllegalArgumentException("maxLength=" + maxLength);
		}
		if (maxLength == 0) {
			return "";
		}
		if (text == null || text.length() <= maxLength) {
			return text;
		}
		return text.substring(0, maxLength);
	}

	@Override
	public int indexOfWhitespace(CharSequence text, int index) {
		for (int i = index; i < text.length(); i++) {
			char c = text.charAt(i);
			if (Character.isWhitespace(c)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int indexOfWhitespace(CharSequence text) {
		return indexOfWhitespace(text, 0);
	}

	@Override
	public int indexOfNonWhitespace(CharSequence text, int index) {
		for (int i = index; i < text.length(); i++) {
			char c = text.charAt(i);
			if (!Character.isWhitespace(c)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public String substringEnd(String text, String from) {
		return substringEnd(text, from, true);
	}

	@Override
	public String substringEnd(String text, String from, boolean includeFrom) {
		int index = text.lastIndexOf(from);
		if (index == -1) {
			return null;
		}
		if (includeFrom) {
			return text.substring(index);
		} else {
			return text.substring(index + from.length());
		}
	}

	@Override
	public String trimEnd(String text, String from) {
		return trimEnd(text, from, true);
	}

	@Override
	public String trimEnd(String text, String from, boolean includeFrom) {
		int index = text.lastIndexOf(from);
		if (index == -1) {
			return text;
		}
		if (includeFrom) {
			return text.substring(0, index);
		} else {
			return text.substring(0, index + from.length());
		}
	}

	@Override
	public int toDigit(char c) {
		if ((c >= '0') && (c <= '9')) {
			return ((int) c) - (int) '0';
		}
		throw new IllegalArgumentException("Character is not a digit: '" + c + "'");
	}
}
