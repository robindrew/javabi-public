package com.javabi.common.text;

import static java.util.concurrent.TimeUnit.NANOSECONDS;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.binary.Hex;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.javabi.common.crypto.Base64Encoder;
import com.javabi.common.crypto.Md5Encoder;
import com.javabi.common.crypto.Sha256Encoder;
import com.javabi.common.crypto.Sha512Encoder;
import com.javabi.common.date.UnitTime;
import com.javabi.common.io.RuntimeIOException;
import com.javabi.common.lang.Numbers;

/**
 * A utility containing common string formatting methods.
 */
public class StringFormats {

	/** A kilobyte in binary notation. */
	public static final int BINARY_DIVISOR = 1024;
	/** A kilobyte in decimal notation. */
	public static final int DECIMAL_DIVISOR = 1000;

	/**
	 * Returns a percentage formatted from the given dividend and divisor.
	 * @param dividend the dividend (value).
	 * @param divisor the divisor (is percentage of)
	 * @return the formatted string.
	 */
	public static String percent(long dividend, long divisor) {

		// Shortcuts
		if (dividend == divisor) {
			return "100%";
		}
		if (dividend == 0) {
			return "0%";
		}
		if (divisor == 100) {
			return dividend + "%";
		}

		if (divisor > dividend && divisor % dividend == 0) {
			return percent(dividend, divisor, 0);
		}
		if (dividend > divisor && dividend % divisor == 0) {
			return percent(dividend, divisor, 0);
		}

		int precision = Numbers.countDigits(divisor / dividend);
		return percent(dividend, divisor, precision);
	}

	/**
	 * Returns a percentage formatted from the given dividend and divisor.
	 * @param dividend the dividend (value).
	 * @param divisor the divisor (is percentage of)
	 * @param precision the precision of the percentage.
	 * @return the formatted string.
	 */
	public static String percent(long dividend, long divisor, int precision) {
		if (dividend < 0) {
			throw new IllegalArgumentException("dividend=" + dividend);
		}
		if (divisor < 1) {
			throw new IllegalArgumentException("divisor=" + divisor);
		}
		if (precision < 0) {
			throw new IllegalArgumentException("precision=" + precision);
		}

		// Shortcuts
		if (precision == 0) {
			if (dividend == divisor) {
				return "100%";
			}
			if (dividend == 0) {
				return "0%";
			}
			if (divisor == 100) {
				return dividend + "%";
			}
		}

		long multiplier = getMultiplier(precision);
		long percent = (dividend * 100 * multiplier) / divisor;

		if (precision == 0) {
			return percent + "%";
		}
		long whole = percent / multiplier;
		long decimal = percent % multiplier;

		StringBuilder text = new StringBuilder();
		text.append(whole).append('.');
		int digits = Numbers.countDigits(decimal);
		while (digits < precision) {
			digits++;
			text.append('0');
		}
		text.append(decimal).append('%');
		return text.toString();
	}

	private static int getMultiplier(int precision) {
		int multiplier = 1;
		for (int i = 0; i < precision; i++) {
			multiplier *= 10;
		}
		return multiplier;
	}

	public static String number(Collection<?> collection) {
		return number(collection.size());
	}

	public static String number(Map<?, ?> map) {
		return number(map.size());
	}

	/**
	 * Format the given number.
	 * @param number the number.
	 * @return the formatted text.
	 */
	public static String number(Number number) {
		// Optimisation: NumberFormat is orders of magnitude slower!
		if (isWholeNumber(number)) {
			return number(number.longValue());
		}
		return NumberFormat.getInstance().format(number);
	}

	private static boolean isWholeNumber(Number number) {
		// We exclude BigInteger as it cannot have an accurate longValue()
		return number instanceof Integer || number instanceof Long || number instanceof Short || number instanceof Byte;
	}

	/**
	 * Format the given number.
	 * @param number the number.
	 * @return the formatted text.
	 */
	public static String number(long number) {
		// Optimisation: NumberFormat is orders of magnitude slower!
		if (number < 1000 && number > -1000) {
			return String.valueOf(number);
		}
		return NumberFormat.getInstance().format(number);
	}

	/**
	 * Format the given number with padding if necessary.
	 * @param number the number.
	 * @param minLength the minimum length (padding zeros as necessary)
	 * @return the formatted text.
	 */
	public static String padded(long number, int minLength) {
		String text = Long.toString(number);
		int difference = minLength - text.length();
		if (difference <= 0) {
			return text;
		}
		StringBuilder padded = new StringBuilder();
		for (int i = 0; i < difference; i++) {
			padded.append('0');
		}
		padded.append(text);
		return padded.toString();
	}

	/**
	 * Format the given number.
	 * @param number the number.
	 * @return the formatted text.
	 */
	public static String number(double number) {
		return NumberFormat.getInstance().format(number);
	}

	/**
	 * Format the given number.
	 * @param number the number.
	 * @param precision the precision.
	 * @return the formatted text.
	 */
	public static String decimal(double number, int precision) {
		return getDecimalFormat(precision).format(number);
	}

	/**
	 * Format the given number.
	 * @param number the number.
	 * @param precision the precision.
	 * @return the formatted text.
	 */
	public static String decimal(Number number, int precision) {
		return getDecimalFormat(precision).format(number);
	}

	/**
	 * Returns a new decimal format with the given precision.
	 * @param precision the precision.
	 * @return the decimal format.
	 */
	public static NumberFormat getDecimalFormat(int precision) {
		NumberFormat format = NumberFormat.getInstance();
		format.setMaximumFractionDigits(precision);
		return format;
	}

	/**
	 * Format the given bytes.
	 * @param amount the bytes.
	 * @return the formatted text.
	 */
	public static String bytes(long amount) {
		return bytes(amount, BINARY_DIVISOR);
	}

	/**
	 * Format the given bytes.
	 * @param amount the bytes.
	 * @param divisor the divisor (1024 or 1000).
	 * @return the formatted text.
	 */
	public static String bytes(long amount, int divisor) {
		if (amount < 0) {
			throw new IllegalArgumentException("amount=" + amount);
		}
		if (divisor != BINARY_DIVISOR && divisor != DECIMAL_DIVISOR) {
			throw new IllegalArgumentException("divisor=" + divisor);
		}

		// Bytes
		long bytes = amount % divisor;
		long kb = amount / divisor;
		if (kb <= 0) {
			return bytes + " bytes";
		}

		// Kilobytes
		long mb = kb / divisor;
		if (mb <= 0) {
			bytes /= 100;
			return kb + "." + bytes + " KB";
		}

		// Megabytes
		long gb = mb / divisor;
		if (gb <= 0) {
			kb %= divisor;
			kb /= 100;
			return mb + "." + kb + " MB";
		}

		// Gigabytes
		long tb = gb / divisor;
		if (tb <= 0) {
			mb %= divisor;
			mb /= 100;
			return gb + "." + mb + " GB";
		}

		// Terabytes
		long pb = tb / divisor;
		if (pb <= 0) {
			gb %= divisor;
			gb /= 100;
			return tb + "." + gb + " TB";
		}

		// Petabytes
		tb %= divisor;
		tb /= 100;
		return pb + "." + tb + " PB";
	}

	/**
	 * Format the given duration.
	 * @param duration the duration.
	 * @param unit the time unit.
	 * @return the formatted text.
	 */
	public static String time(long duration, TimeUnit unit) {
		return nanos(unit.toNanos(duration));
	}

	/**
	 * Format the given duration.
	 * @param duration the duration.
	 * @return the formatted text.
	 */
	public static String time(UnitTime duration) {
		return nanos(duration.getTime(NANOSECONDS));
	}

	/**
	 * Format the given duration in milliseconds.
	 * @param durationInMillis the duration in milliseconds.
	 * @return the formatted text.
	 */
	public static String time(long durationInMillis) {
		return nanos(durationInMillis * 1000000);
	}

	/**
	 * Format the given duration in nanoseconds.
	 * @param durationInNanos the duration in nanoseconds.
	 * @return the formatted text.
	 */
	public static String nanos(long durationInNanos) {

		// Nanos
		long nanos = durationInNanos % 1000;
		long micros = durationInNanos / 1000;
		if (micros <= 0) {
			return number(nanos) + " nanos";
		}

		// Microseconds
		long millis = micros / 1000;
		if (millis <= 0) {
			nanos %= 1000;
			nanos /= 100;
			return micros + "." + nanos + " micros";
		}

		// Milliseconds
		long seconds = millis / 1000;
		if (seconds <= 0) {
			micros %= 1000;
			micros /= 100;
			return millis + "." + micros + " millis";
		}

		// Seconds
		long minutes = seconds / 60;
		if (minutes <= 0) {
			millis %= 1000;
			millis /= 100;
			return seconds + "." + millis + " seconds";
		}

		// Minutes
		long hours = minutes / 60;
		if (hours <= 0) {
			seconds %= 60;
			seconds = (seconds * 10) / 60;
			return minutes + "." + seconds + " minutes";
		}

		// Hours
		long days = hours / 24;
		if (days <= 0) {
			minutes %= 60;
			minutes = (minutes * 10) / 60;
			return hours + "." + minutes + " hours";
		}

		// Days
		hours %= 24;
		hours = (hours * 10) / 24;
		return number(days) + "." + hours + " days";
	}

	/**
	 * Format the given number in hexadecimal (see {@link Long#toHexString(long)}).
	 * @param number the number to format.
	 * @return the formatted text.
	 */
	public static final String hex(long number) {
		return Long.toHexString(number);
	}

	/**
	 * Format the given bytes in hexadecimal (see {@link Hex#encodeHex(byte[])}).
	 * @param bytes the bytes to format.
	 * @return the formatted text.
	 */
	public static final String hex(byte[] bytes) {
		return new String(Hex.encodeHex(bytes));
	}

	/**
	 * Format the given number in binary (see {@link Long#toBinaryString(long)}).
	 * @param number the number to format.
	 * @return the formatted text.
	 */
	public static final String binary(long number) {
		return Long.toBinaryString(number);
	}

	/**
	 * Format the given number in octal (see {@link Long#toOctalString(long)}).
	 * @param number the number to format.
	 * @return the formatted text.
	 */
	public static final String octal(long number) {
		return Long.toOctalString(number);
	}

	/**
	 * Format the given bytes using the MD5 message digest.
	 * @param bytes the bytes to format.
	 * @return the formatted text.
	 */
	public static final String md5(byte[] bytes) {
		byte[] md5 = new Md5Encoder().encodeToBytes(bytes);
		return hex(md5);
	}

	/**
	 * Format the given bytes using the SHA-256 message digest.
	 * @param bytes the bytes to format.
	 * @return the formatted text.
	 */
	public static final String sha256(byte[] bytes) {
		byte[] encoded = new Sha256Encoder().encodeToBytes(bytes);
		return hex(encoded);
	}

	/**
	 * Format the given bytes using the SHA-512 message digest.
	 * @param bytes the bytes to format.
	 * @return the formatted text.
	 */
	public static final String sha512(byte[] bytes) {
		byte[] encoded = new Sha512Encoder().encodeToBytes(bytes);
		return hex(encoded);
	}

	/**
	 * Format the given bytes using Base64 encoding.
	 * @param bytes the bytes to format.
	 * @return the formatted text.
	 */
	public static final String base64(String text) {
		return new Base64Encoder().encodeToString(text);
	}

	private static URL getResource(String name) {
		URL url = Resources.getResource(name);
		if (url == null) {
			throw new IllegalArgumentException("Resource not found: " + name);
		}
		return url;
	}

	/**
	 * Read the given file to a string.
	 * @param name the resource name.
	 * @return the formatted text.
	 */
	public static final String resource(String name) {
		URL url = getResource(name);
		return resource(url);
	}

	/**
	 * Read the given URL to a string.
	 * @param url the resource url.
	 * @param charset the character set.
	 * @return the formatted text.
	 */
	public static final String resource(URL url) {
		return resource(url, Charsets.UTF_8);
	}

	/**
	 * Read the given URL to a string.
	 * @param name the resource name.
	 * @param charset the character set.
	 * @return the formatted text.
	 */
	public static final String resource(String name, Charset charset) {
		URL url = getResource(name);
		try {
			return Resources.toString(url, charset);
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}

	/**
	 * Read the given URL to a string.
	 * @param url the resource url.
	 * @param charset the character set.
	 * @return the formatted text.
	 */
	public static final String resource(URL url, Charset charset) {
		try {
			return Resources.toString(url, charset);
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}

}
