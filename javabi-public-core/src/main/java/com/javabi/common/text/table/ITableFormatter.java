package com.javabi.common.text.table;

public interface ITableFormatter {

	String formatTable(IFormattedTable table);

	void formatTable(IFormattedTable table, StringBuilder formatted);

}
