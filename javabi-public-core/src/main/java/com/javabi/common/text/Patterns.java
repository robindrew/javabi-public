package com.javabi.common.text;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Pattern;

/**
 * A Collection of {@link Pattern} related utility methods. Holds a cache of patterns.
 */
public final class Patterns {

	/** The pattern cache. */
	private static final ConcurrentMap<String, Pattern> patternCache = new ConcurrentHashMap<String, Pattern>();

	/**
	 * Returns an instance of the given pattern.
	 * @param regex the regular expression.
	 * @return the pattern.
	 */
	public static final Pattern getInstance(String regex) {
		Pattern pattern = patternCache.get(regex);
		if (pattern == null) {
			regex = regex.intern();
			pattern = Pattern.compile(regex);
			patternCache.put(regex, pattern);
		}
		return pattern;
	}

	/**
	 * Returns true if the given regular expression matches the text.
	 * @param regex the regular expression.
	 * @param text the text.
	 * @return true if the given regular expression matches the text.
	 */
	public static final boolean matches(String regex, String text) {
		Pattern pattern = getInstance(regex);
		return pattern.matcher(text).matches();
	}

	/**
	 * Inaccessible constructor.
	 */
	private Patterns() {
	}

}
