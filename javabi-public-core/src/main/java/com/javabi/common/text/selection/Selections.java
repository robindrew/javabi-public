package com.javabi.common.text.selection;

public class Selections {

	public static String extract(String text, String begin, String end) {
		Selection selection = new Selection(text);
		return selection.extract(begin, end);
	}

	public static String extract(String text, char begin, char end) {
		return extract(text, String.valueOf(begin), String.valueOf(end));
	}

	public static String extract(String text, String begin, String end, SelectionOption... options) {
		Selection selection = new Selection(text);
		return selection.extract(begin, end, options);
	}

	public static String extract(String text, char begin, char end, SelectionOption... options) {
		return extract(text, String.valueOf(begin), String.valueOf(end), options);
	}

	private Selections() {
	}

}
