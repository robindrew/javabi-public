package com.javabi.common.text.parser;

import com.javabi.common.lang.clazz.IClassResolver;

@SuppressWarnings("rawtypes")
public class ClassParser extends ObjectParser<Class> {

	private final IClassResolver resolver;

	public ClassParser(IClassResolver resolver) {
		if (resolver == null) {
			throw new NullPointerException("resolver");
		}
		this.resolver = resolver;
	}

	@Override
	protected Class parseObject(String text) {
		return resolver.resolveClass(text);
	}

}
