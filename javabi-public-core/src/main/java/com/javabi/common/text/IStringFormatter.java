package com.javabi.common.text;

public interface IStringFormatter<F> {

	String format(F object);
}
