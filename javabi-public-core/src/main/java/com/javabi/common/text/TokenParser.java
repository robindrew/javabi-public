package com.javabi.common.text;

import com.javabi.common.text.parser.IStringParser;
import com.javabi.common.text.parser.StringParserMap;

public class TokenParser {

	private final String text;
	private int index = 0;
	private StringParserMap parserMap = null;

	public TokenParser(String text) {
		if (text == null) {
			throw new NullPointerException("text");
		}
		this.text = text;
	}

	public void setParserMap(StringParserMap parserMap) {
		if (parserMap == null) {
			throw new NullPointerException("parserMap");
		}
		this.parserMap = parserMap;
	}

	public String readToken(char quote) {
		String token = readToken();
		if (token.length() < 2) {
			throw new IllegalStateException("token: '" + token + "', quote=" + quote);
		}
		if (token.charAt(0) != quote || token.charAt(token.length() - 1) != quote) {
			throw new IllegalStateException("token: '" + token + "', quote=" + quote);
		}
		return token.substring(1, token.length() - 1);
	}

	public <R> R readToken(Class<R> type) {
		if (type == null) {
			throw new NullPointerException("type");
		}
		if (parserMap == null) {
			throw new IllegalStateException("parserMap not set");
		}
		String token = readToken();
		IStringParser<R> parser = parserMap.getParser(type);
		return parser.parse(token);
	}

	public String readToken() {
		int begin = -1;
		for (; index < text.length(); index++) {
			char c = text.charAt(index);
			if (Character.isWhitespace(c)) {
				if (begin != -1) {
					break;
				}
			} else {
				if (begin == -1) {
					begin = index;
				}
			}
		}
		if (begin == -1) {
			return null;
		}
		return text.substring(begin, index);
	}

	public String readLastToken() {
		for (; index < text.length(); index++) {
			char c = text.charAt(index);
			if (!Character.isWhitespace(c)) {
				break;
			}
		}
		return text.substring(index);
	}

}
