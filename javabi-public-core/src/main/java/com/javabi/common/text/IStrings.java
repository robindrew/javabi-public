package com.javabi.common.text;

import java.util.List;

public interface IStrings {

	String valueOf(byte value);

	String valueOf(short value);

	String valueOf(int value);

	String valueOf(long value);

	String valueOf(Object value);

	String valueOf(Throwable value);

	List<String> splitOnWhitespace(String text);

	int indexOfIgnoreCase(CharSequence haystack, String needle, int fromIndex);

	String trim(String text, int maxLength);

	String trimEnd(String text, String from, boolean includeFrom);

	String trimEnd(String text, String from);

	int indexOfWhitespace(CharSequence text, int index);

	int indexOfWhitespace(CharSequence text);

	String substringEnd(String text, String from, boolean includeFrom);

	String substringEnd(String text, String from);

	int indexOfNonWhitespace(CharSequence text, int index);
	
	int toDigit(char c);
}
