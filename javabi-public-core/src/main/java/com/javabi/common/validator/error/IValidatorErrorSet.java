package com.javabi.common.validator.error;

/**
 * A Validator Error Set.
 */
public interface IValidatorErrorSet extends Iterable<IValidatorError> {

	/**
	 * Adds an error to the set.
	 * @param error to add.
	 */
	void add(IValidatorError error);

}
