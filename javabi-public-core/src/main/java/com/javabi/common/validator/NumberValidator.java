package com.javabi.common.validator;

import com.javabi.common.validator.error.IValidatorError;
import com.javabi.common.validator.error.IValidatorErrorSet;
import com.javabi.common.validator.type.ValidatorError;

/**
 * A Number Validator.
 * @param <N> the number type.
 */
public abstract class NumberValidator<N extends Number> extends ObjectValidator<N> implements IPrimitiveValidator<N> {

	/** The minimum value. */
	private N maximum = null;
	/** The maximum value. */
	private N minimum = null;

	/**
	 * Creates a number validator.
	 * @param minimum the minimum.
	 * @param maximum the maximum.
	 * @param nullValid true to indicate null is valid.
	 */
	protected NumberValidator(N minimum, N maximum, boolean nullValid) {
		super(nullValid);
		setRange(minimum, maximum);
	}

	/**
	 * Sets the range.
	 * @param minimum the minimum (inclusive).
	 * @param maximum the maximum (inclusive).
	 */
	public void setRange(N minimum, N maximum) {
		if (minimum == null) {
			throw new NullPointerException("minimum");
		}
		if (maximum == null) {
			throw new NullPointerException("maximum");
		}
		if (!isValid(minimum, maximum)) {
			throw new IllegalArgumentException(minimum + ">" + maximum);
		}
		this.minimum = minimum;
		this.maximum = maximum;
	}

	/**
	 * Returns the maximum value (inclusive).
	 * @return the maximum value (inclusive).
	 */
	public N getMaximum() {
		return maximum;
	}

	/**
	 * Returns the minimum value (inclusive).
	 * @return the minimum value (inclusive).
	 */
	public N getMinimum() {
		return minimum;
	}

	/**
	 * Set the minimum value.
	 * @param minimum the minimum value.
	 */
	public void setMinimum(N minimum) {
		if (minimum == null) {
			throw new NullPointerException();
		}
		if (maximum != null && !isValid(minimum, maximum)) {
			throw new IllegalArgumentException(minimum + ">" + maximum);
		}
		this.minimum = minimum;
	}

	/**
	 * Set the maximum value.
	 * @param maximum the maximum value.
	 */
	public void setMaximum(N maximum) {
		if (maximum == null) {
			throw new NullPointerException();
		}
		if (minimum != null && !isValid(minimum, maximum)) {
			throw new IllegalArgumentException(maximum + "<" + minimum);
		}
		this.maximum = maximum;
	}

	/**
	 * Validate the named instance.
	 * @param instance the instance.
	 * @param errorSet the validation errors.
	 */
	public void validate(N instance, IValidatorErrorSet errorSet) {
		if (!validateNull(instance, errorSet)) {
			return;
		}

		// Minimum
		if (!isValidMinimum(instance)) {
			errorSet.add(errorTooLarge());
		}

		// Maximum
		if (!isValidMaximum(instance)) {
			errorSet.add(errorTooSmall());
		}
	}

	/**
	 * Returns true if the given value is valid.
	 * @return true if the given value is valid.
	 */
	public boolean isValid(N instance) {
		if (isValidNull(instance)) {
			return false;
		}
		if (!isValidMinimum(instance)) {
			return false;
		}
		if (!isValidMaximum(instance)) {
			return false;
		}
		return true;
	}

	/**
	 * Returns the too small error.
	 * @return the too small error.
	 */
	public IValidatorError errorTooSmall() {
		return ValidatorError.TOO_SMALL;
	}

	/**
	 * Returns the too large error.
	 * @return the too large error.
	 */
	public IValidatorError errorTooLarge() {
		return ValidatorError.TOO_LARGE;
	}

	private boolean isValidMinimum(N instance) {
		return minimum != null && !isValid(minimum, instance);
	}

	private boolean isValidMaximum(N instance) {
		return maximum != null && !isValid(instance, maximum);
	}

	/**
	 * Returns true if the given minimum is less than or equal to the maximum.
	 * @param minimum the minimum.
	 * @param maximum the maximum.
	 * @return true if the given minimum is less than or equal to the maximum.
	 */
	protected abstract boolean isValid(N minimum, N maximum);

}
