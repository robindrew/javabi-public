package com.javabi.common.validator;

import com.javabi.common.validator.error.IValidatorError;
import com.javabi.common.validator.error.IValidatorErrorSet;
import com.javabi.common.validator.type.ValidatorError;

/**
 * An Object Validator.
 * @param <T> the type.
 */
public abstract class ObjectValidator<T> implements ITypeValidator<T> {

	/** Indicates if null is valid. */
	private boolean nullValid;

	/**
	 * Creates a new validator.
	 * @param nullValid true if null is valid.
	 */
	protected ObjectValidator(boolean nullValid) {
		this.nullValid = nullValid;
	}

	/**
	 * Returns true if null is valid.
	 * @return true if null is valid.
	 */
	public boolean isNullValid() {
		return nullValid;
	}

	/**
	 * Sets whether null is valid.
	 * @param nullValid true to indicate null is valid.
	 */
	public void setNullValid(boolean nullValid) {
		this.nullValid = nullValid;
	}

	/**
	 * Returns true if the given instance is valid.
	 * @param instance the instance.
	 * @return true if the given instance is valid.
	 */
	public boolean isValidNull(T instance) {
		return instance != null || nullValid;
	}

	/**
	 * Validate if the given instance is valid.
	 * @param instance the instance.
	 * @param errorSet the error set.
	 * @return true if valid.
	 */
	public boolean validateNull(T instance, IValidatorErrorSet errorSet) {
		if (!isValidNull(instance)) {
			errorSet.add(errorIsNull());
			return false;
		}
		return true;
	}

	/**
	 * Returns the is null error.
	 * @return the is null error.
	 */
	public IValidatorError errorIsNull() {
		return ValidatorError.IS_NULL;
	}
}
