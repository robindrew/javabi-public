package com.javabi.common.validator.type;

import java.util.regex.Pattern;

import com.javabi.common.text.Patterns;
import com.javabi.common.validator.ObjectValidator;
import com.javabi.common.validator.error.IValidatorError;
import com.javabi.common.validator.error.IValidatorErrorSet;

/**
 * A String Validator.
 */
public class StringValidator extends ObjectValidator<String> {

	/** The minimum length. */
	private int minimumLength = 0;
	/** The maximum length. */
	private int maximumLength = Integer.MAX_VALUE;
	/** The pattern. */
	private Pattern pattern = null;

	/**
	 * Creates a new boolean validator.
	 * @param nullValid true if null is valid.
	 */
	public StringValidator(boolean nullValid) {
		super(nullValid);
	}

	/**
	 * Creates a new boolean validator.
	 * @param nullValid true if null is valid.
	 */
	public StringValidator(boolean nullValid, int minimumLength, int maximumLength) {
		super(nullValid);
		setLength(minimumLength, maximumLength);
	}

	/**
	 * Returns the type.
	 * @return the type.
	 */
	public Class<String> getType() {
		return String.class;
	}

	/**
	 * Returns true if the given value is valid.
	 * @return true if the given value is valid.
	 */
	public boolean isValid(String instance) {
		if (!isValidNull(instance)) {
			return false;
		}
		if (!isValidMinimumLength(instance)) {
			return false;
		}
		if (!isValidMaximumLength(instance)) {
			return false;
		}
		return true;
	}

	/**
	 * Returns the pattern.
	 * @return the pattern.
	 */
	public Pattern getPattern() {
		return pattern;
	}

	/**
	 * Sets the pattern.
	 * @param pattern the pattern.
	 */
	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

	/**
	 * Sets the pattern.
	 * @param regex the regular expression.
	 */
	public void setPattern(String regex) {
		setPattern(Patterns.getInstance(regex));
	}

	/**
	 * Returns the minimum length.
	 * @return the minimum length.
	 */
	public int getMinimumLength() {
		return minimumLength;
	}

	/**
	 * Sets the minimum length.
	 * @param length the length.
	 */
	public void setMinimumLength(int length) {
		if (length < 0) {
			throw new IllegalArgumentException("length=" + length);
		}
		this.minimumLength = length;
	}

	/**
	 * Returns the maximum length.
	 * @return the maximum length.
	 */
	public int getMaximumLength() {
		return maximumLength;
	}

	/**
	 * Sets the maximum length.
	 * @param length the length.
	 */
	public void setMaximumLength(int length) {
		if (length < minimumLength) {
			throw new IllegalArgumentException("length=" + length);
		}
		this.maximumLength = length;
	}

	/**
	 * Sets the length.
	 * @param minimum the minimum length.
	 * @param maximum the maximum length
	 */
	public void setLength(int minimum, int maximum) {
		if (minimum > maximum) {
			throw new IllegalArgumentException(minimum + ">" + maximum);
		}
		if (minimum < 0) {
			throw new IllegalArgumentException("minimum=" + minimum);
		}
		this.minimumLength = minimum;
		this.maximumLength = maximum;
	}

	private boolean isValidMaximumLength(String instance) {
		return instance.length() >= minimumLength;
	}

	private boolean isValidMinimumLength(String instance) {
		return instance.length() <= maximumLength;
	}

	private boolean isValidPattern(String instance) {
		return pattern == null || pattern.matcher(instance).matches();
	}

	/**
	 * Validate the given instance.
	 * @param instance the instance.
	 * @param errorSet the error set.
	 */
	public void validate(String instance, IValidatorErrorSet errorSet) {
		if (!validateNull(instance, errorSet)) {
			return;
		}
		if (!isValidMinimumLength(instance)) {
			errorSet.add(errorTooShort());
		}
		if (!isValidMaximumLength(instance)) {
			errorSet.add(errorTooLong());
		}
		if (!isValidPattern(instance)) {
			errorSet.add(errorPatternNotMatched());
		}
	}

	/**
	 * Returns the too short error.
	 * @return the too short error.
	 */
	public IValidatorError errorTooShort() {
		return ValidatorError.TOO_SHORT;
	}

	/**
	 * Returns the too long error.
	 * @return the too long error.
	 */
	public IValidatorError errorTooLong() {
		return ValidatorError.TOO_LONG;
	}

	/**
	 * Returns the pattern not matched error.
	 * @return the pattern not matched error.
	 */
	public IValidatorError errorPatternNotMatched() {
		return ValidatorError.PATTERN_NOT_MATCHED;
	}

}