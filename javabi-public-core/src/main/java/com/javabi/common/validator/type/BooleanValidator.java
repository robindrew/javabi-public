package com.javabi.common.validator.type;

import com.javabi.common.validator.IPrimitiveValidator;
import com.javabi.common.validator.ObjectValidator;
import com.javabi.common.validator.error.IValidatorErrorSet;

/**
 * A Boolean Validator.
 */
public class BooleanValidator extends ObjectValidator<Boolean> implements IPrimitiveValidator<Boolean> {

	/**
	 * Creates a new boolean validator.
	 * @param nullValid true if null is valid.
	 */
	public BooleanValidator(boolean nullValid) {
		super(nullValid);
	}

	/**
	 * Returns the type.
	 * @return the type.
	 */
	public Class<Boolean> getType() {
		return Boolean.class;
	}

	/**
	 * Returns the primitive type.
	 * @return the primitive type.
	 */
	public Class<?> getPrimitiveType() {
		return boolean.class;
	}

	/**
	 * Returns true if the given value is valid.
	 * @return true if the given value is valid.
	 */
	public boolean isValid(Boolean instance) {
		return isValidNull(instance);
	}

	/**
	 * Validate the given instance.
	 * @param instance the instance.
	 * @param errorSet the error set.
	 */
	public void validate(Boolean instance, IValidatorErrorSet errorSet) {
		if (!validateNull(instance, errorSet)) {
			return;
		}
	}

}