package com.javabi.common.validator.type;

import java.util.EnumSet;

import com.javabi.common.validator.ObjectValidator;
import com.javabi.common.validator.error.IValidatorError;
import com.javabi.common.validator.error.IValidatorErrorSet;

/**
 * A Enum Validator.
 */
public class EnumValidator<E extends Enum<E>> extends ObjectValidator<E> {

	/** The enum class. */
	private final Class<E> enumClass;
	/** The enum set (if not assigned, all values are valid). */
	private EnumSet<E> enumSet = null;

	/**
	 * Creates a new boolean validator.
	 * @param enumClass the enum class.
	 * @param nullValid true if null is valid.
	 */
	public EnumValidator(Class<E> enumClass, boolean nullValid) {
		super(nullValid);
		this.enumClass = enumClass;
	}

	/**
	 * Returns the type.
	 * @return the type.
	 */
	public Class<E> getType() {
		return enumClass;
	}

	/**
	 * Returns true if the given value is valid.
	 * @return true if the given value is valid.
	 */
	public boolean isValid(E instance) {
		if (!isValidNull(instance)) {
			return false;
		}
		if (!isValidEnum(instance)) {
			return false;
		}
		return true;
	}

	/**
	 * Returns the enum set (can be null).
	 * @return the enum set.
	 */
	public EnumSet<E> getEnumSet() {
		return enumSet;
	}

	/**
	 * Sets the enum set.
	 * @param enumSet the enum set.
	 */
	public void setEnumSet(EnumSet<E> enumSet) {
		this.enumSet = enumSet;
	}

	private boolean isValidEnum(E instance) {
		return enumSet == null || enumSet.contains(instance);
	}

	/**
	 * Validate the given instance.
	 * @param instance the instance.
	 * @param errorSet the error set.
	 */
	public void validate(E instance, IValidatorErrorSet errorSet) {
		if (!validateNull(instance, errorSet)) {
			return;
		}
		if (!isValidEnum(instance)) {
			errorSet.add(errorInvalidEnum());
		}
	}

	/**
	 * Returns the too short error.
	 * @return the too short error.
	 */
	public IValidatorError errorInvalidEnum() {
		return ValidatorError.INVALID_ENUM;
	}

}