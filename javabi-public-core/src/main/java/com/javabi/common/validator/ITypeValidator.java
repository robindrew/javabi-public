package com.javabi.common.validator;

import com.javabi.common.validator.error.IValidatorErrorSet;

/**
 * A Type Validator.
 */
public interface ITypeValidator<T> {

	/**
	 * Returns the type.
	 * @return the type.
	 */
	Class<T> getType();

	/**
	 * Returns true if the given instance is valid.
	 * @param instance the instance.
	 * @return true if the given instance is valid.
	 */
	boolean isValid(T instance);

	/**
	 * Validate the given instance.
	 * @param instance the instance.
	 * @param errorSet the error set.
	 */
	void validate(T instance, IValidatorErrorSet errorSet);

	/**
	 * Returns true if null is valid.
	 * @return true if null is valid.
	 */
	boolean isNullValid();

}
