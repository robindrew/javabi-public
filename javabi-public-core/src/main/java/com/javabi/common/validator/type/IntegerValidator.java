package com.javabi.common.validator.type;

import com.javabi.common.validator.NumberValidator;

/**
 * An Integer Validator.
 */
public class IntegerValidator extends NumberValidator<Integer> {

	/**
	 * Creates a new integer validator.
	 * @param minimum the minimum value.
	 * @param maximum the maximum value.
	 * @param nullValid true if null is valid.
	 */
	public IntegerValidator(Integer minimum, Integer maximum, boolean nullValid) {
		super(minimum, maximum, nullValid);
	}

	@Override
	protected boolean isValid(Integer minimum, Integer maximum) {
		return minimum.intValue() <= maximum.intValue();
	}

	/**
	 * Returns the type.
	 * @return the type.
	 */
	public Class<Integer> getType() {
		return Integer.class;
	}

	/**
	 * Returns the primitive type.
	 * @return the primitive type.
	 */
	public Class<?> getPrimitiveType() {
		return int.class;
	}

}