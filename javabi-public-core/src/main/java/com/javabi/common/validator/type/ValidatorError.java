package com.javabi.common.validator.type;

import com.javabi.common.validator.error.IValidatorError;

/**
 * A Validator Error.
 */
public enum ValidatorError implements IValidatorError {

	/** The is null error. */
	IS_NULL,
	/** The too small error. */
	TOO_SMALL,
	/** The too large error. */
	TOO_LARGE,
	/** The too long error. */
	TOO_LONG,
	/** The too short error. */
	TOO_SHORT,
	/** The invalid enum error. */
	INVALID_ENUM,
	/** The pattern not matched error. */
	PATTERN_NOT_MATCHED,

}
