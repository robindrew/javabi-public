package com.javabi.common.validator;

/**
 * A Primitive Type Validator.
 * @param <T> the non-primitive type.
 */
public interface IPrimitiveValidator<T> extends ITypeValidator<T> {

	/**
	 * Returns the primitive type.
	 * @return the primitive type.
	 */
	Class<?> getPrimitiveType();

}
