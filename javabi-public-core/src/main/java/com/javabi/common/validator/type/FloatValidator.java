package com.javabi.common.validator.type;

import com.javabi.common.validator.NumberValidator;

/**
 * A Float Validator.
 */
public class FloatValidator extends NumberValidator<Float> {

	/**
	 * Creates a new float validator.
	 * @param minimum the minimum value.
	 * @param maximum the maximum value.
	 * @param nullValid true if null is valid.
	 */
	public FloatValidator(Float minimum, Float maximum, boolean nullValid) {
		super(minimum, maximum, nullValid);
	}

	@Override
	protected boolean isValid(Float minimum, Float maximum) {
		return minimum.floatValue() <= maximum.floatValue();
	}

	/**
	 * Returns the type.
	 * @return the type.
	 */
	public Class<Float> getType() {
		return Float.class;
	}

	/**
	 * Returns the primitive type.
	 * @return the primitive type.
	 */
	public Class<?> getPrimitiveType() {
		return float.class;
	}

}