package com.javabi.common.validator;

/**
 * A Validator Exception.
 */
public class ValidatorException extends RuntimeException {

	/** The serialization id. */
	private static final long serialVersionUID = 5825497266601566139L;

	/**
	 * Creates a new exception.
	 * @param message the message.
	 */
	public ValidatorException(String message) {
		super(message);
	}

	/**
	 * Creates a new exception.
	 * @param message the message.
	 * @param cause the cause.
	 */
	public ValidatorException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Creates a new exception.
	 * @param cause the cause.
	 */
	public ValidatorException(Throwable cause) {
		super(cause);
	}
}
