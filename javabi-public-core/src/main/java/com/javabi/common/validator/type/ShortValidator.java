package com.javabi.common.validator.type;

import com.javabi.common.validator.NumberValidator;

/**
 * A Short Validator.
 */
public class ShortValidator extends NumberValidator<Short> {

	/**
	 * Creates a new short validator.
	 * @param minimum the minimum value.
	 * @param maximum the maximum value.
	 * @param nullValid true if null is valid.
	 */
	public ShortValidator(Short minimum, Short maximum, boolean nullValid) {
		super(minimum, maximum, nullValid);
	}

	@Override
	protected boolean isValid(Short minimum, Short maximum) {
		return minimum.shortValue() <= maximum.shortValue();
	}

	/**
	 * Returns the type.
	 * @return the type.
	 */
	public Class<Short> getType() {
		return Short.class;
	}

	/**
	 * Returns the primitive type.
	 * @return the primitive type.
	 */
	public Class<?> getPrimitiveType() {
		return short.class;
	}

}