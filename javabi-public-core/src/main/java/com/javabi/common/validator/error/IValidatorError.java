package com.javabi.common.validator.error;

/**
 * A Validator Error.
 */
public interface IValidatorError {

	/**
	 * Returns the name of the error.
	 * @return the name of the error.
	 */
	String name();

}
