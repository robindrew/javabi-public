package com.javabi.common.validator.type;

import com.javabi.common.validator.NumberValidator;

/**
 * A Byte Validator.
 */
public class ByteValidator extends NumberValidator<Byte> {

	/**
	 * Creates a new byte validator.
	 * @param minimum the minimum value.
	 * @param maximum the maximum value.
	 * @param nullValid true if null is valid.
	 */
	public ByteValidator(Byte minimum, Byte maximum, boolean nullValid) {
		super(minimum, maximum, nullValid);
	}

	@Override
	protected boolean isValid(Byte minimum, Byte maximum) {
		return minimum.byteValue() <= maximum.byteValue();
	}

	/**
	 * Returns the type.
	 * @return the type.
	 */
	public Class<Byte> getType() {
		return Byte.class;
	}

	/**
	 * Returns the primitive type.
	 * @return the primitive type.
	 */
	public Class<?> getPrimitiveType() {
		return byte.class;
	}

}