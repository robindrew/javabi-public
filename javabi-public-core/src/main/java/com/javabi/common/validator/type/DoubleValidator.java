package com.javabi.common.validator.type;

import com.javabi.common.validator.NumberValidator;

/**
 * A Double Validator.
 */
public class DoubleValidator extends NumberValidator<Double> {

	/**
	 * Creates a new double validator.
	 * @param minimum the minimum value.
	 * @param maximum the maximum value.
	 * @param nullValid true if null is valid.
	 */
	public DoubleValidator(Double minimum, Double maximum, boolean nullValid) {
		super(minimum, maximum, nullValid);
	}

	@Override
	protected boolean isValid(Double minimum, Double maximum) {
		return minimum.doubleValue() <= maximum.doubleValue();
	}

	/**
	 * Returns the type.
	 * @return the type.
	 */
	public Class<Double> getType() {
		return Double.class;
	}

	/**
	 * Returns the primitive type.
	 * @return the primitive type.
	 */
	public Class<?> getPrimitiveType() {
		return double.class;
	}

}