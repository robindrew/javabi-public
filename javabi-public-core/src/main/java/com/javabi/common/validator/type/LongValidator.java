package com.javabi.common.validator.type;

import com.javabi.common.validator.NumberValidator;

/**
 * A Long Validator.
 */
public class LongValidator extends NumberValidator<Long> {

	/**
	 * Creates a new long validator.
	 * @param minimum the minimum value.
	 * @param maximum the maximum value.
	 * @param nullValid true if null is valid.
	 */
	public LongValidator(Long minimum, Long maximum, boolean nullValid) {
		super(minimum, maximum, nullValid);
	}

	@Override
	protected boolean isValid(Long minimum, Long maximum) {
		return minimum.longValue() <= maximum.longValue();
	}

	/**
	 * Returns the type.
	 * @return the type.
	 */
	public Class<Long> getType() {
		return Long.class;
	}

	/**
	 * Returns the primitive type.
	 * @return the primitive type.
	 */
	public Class<?> getPrimitiveType() {
		return long.class;
	}

}