package com.javabi.common.cron;

/**
 * A collection of cron scheduler factory methods.
 */
public class CronSchedulers {

	private static final ICronConfig yearly;
	private static final ICronConfig annually;
	private static final ICronConfig monthly;
	private static final ICronConfig weekly;
	private static final ICronConfig daily;
	private static final ICronConfig hourly;

	static {

		// YEARLY
		ICronConfig config = new CronConfig();
		config.addSecond(0);
		config.addMinute(0);
		config.addHour(0);
		config.addDayOfMonth(1);
		config.addMonth(1);
		yearly = new CronConfig(config);
		annually = new CronConfig(config);

		// MONTHLY
		config = new CronConfig();
		config.addSecond(0);
		config.addMinute(0);
		config.addHour(0);
		config.addDayOfMonth(1);
		monthly = new CronConfig(config);

		// WEEKLY
		config = new CronConfig();
		config.addSecond(0);
		config.addMinute(0);
		config.addHour(0);
		config.addDayOfWeek(7);
		weekly = new CronConfig(config);

		// DAILY
		config = new CronConfig();
		config.addSecond(0);
		config.addMinute(0);
		config.addHour(0);
		daily = new CronConfig(config);

		// HOURLY
		config = new CronConfig();
		config.addSecond(0);
		config.addMinute(0);
		hourly = new CronConfig(config);

	}

	/**
	 * Returns a yearly cron config.
	 * @return a yearly cron config.
	 */
	public static ICronConfig annually() {
		return annually;
	}

	/**
	 * Returns a yearly cron config.
	 * @return a yearly cron config.
	 */
	public static ICronConfig yearly() {
		return yearly;
	}

	/**
	 * Returns a monthly cron config.
	 * @return a monthly cron config.
	 */
	public static ICronConfig monthly() {
		return monthly;
	}

	/**
	 * Returns a weekly cron config.
	 * @return a weekly cron config.
	 */
	public static ICronConfig weekly() {
		return weekly;
	}

	/**
	 * Returns a daily cron config.
	 * @return a daily cron config.
	 */
	public static ICronConfig daily() {
		return daily;
	}

	/**
	 * Returns a hourly cron config.
	 * @return a hourly cron config.
	 */
	public static ICronConfig hourly() {
		return hourly;
	}

	private CronSchedulers() {
	}
}
