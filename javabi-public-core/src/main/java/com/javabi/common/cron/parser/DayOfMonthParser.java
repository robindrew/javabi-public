package com.javabi.common.cron.parser;

import static com.javabi.common.cron.ICronConfig.DAY_OF_MONTH_MAX;
import static com.javabi.common.cron.ICronConfig.DAY_OF_MONTH_MIN;

import com.javabi.common.cron.ICronConfig;

class DayOfMonthParser extends UnitParser {

	public DayOfMonthParser() {
		super(DAY_OF_MONTH_MIN, DAY_OF_MONTH_MAX);
	}

	@Override
	void addUnit(ICronConfig config, int value) {
		config.addDayOfMonth(value);
	}

}
