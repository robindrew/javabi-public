package com.javabi.common.cron;

import java.util.List;

/**
 * A cron scheduler config. It is important to note:
 * <p>
 * Months: JANUARY(1) to DECEMBER(12)
 * <p>
 * Days of Week: MONDAY(1) to SUNDAY(7)
 */
public interface ICronConfig {

	int MONTH_MIN = 1;
	int MONTH_MAX = 12;

	int DAY_OF_MONTH_MIN = 1;
	int DAY_OF_MONTH_MAX = 31;

	int DAY_OF_WEEK_MIN = 1;
	int DAY_OF_WEEK_MAX = 7;

	int HOUR_MIN = 0;
	int HOUR_MAX = 23;

	int MINUTE_MIN = 0;
	int MINUTE_MAX = 59;

	int SECOND_MIN = 0;
	int SECOND_MAX = 59;

	boolean isEmpty();

	List<Integer> getMonths();

	List<Integer> getDaysOfMonth();

	List<Integer> getDaysOfWeek();

	List<Integer> getHours();

	List<Integer> getMinutes();

	List<Integer> getSeconds();

	void addMonth(int month);

	void addDayOfMonth(int day);

	void addDayOfWeek(int day);

	void addHour(int hour);

	void addMinute(int minute);

	void addSecond(int second);

	void addMonths(int from, int to);

	void addDaysOfMonth(int from, int to);

	void addDaysOfWeek(int from, int to);

	void addHours(int from, int to);

	void addMinutes(int from, int to);

	void addSeconds(int from, int to);

	int getLowestHour();

	int getLowestMinute();

	int getLowestSecond();

}
