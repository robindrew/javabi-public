package com.javabi.common.cron;

import java.util.List;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableInt;

public abstract class CronDate {

	protected final ICronConfig config;

	protected final MutableInt second = new MutableInt();
	protected final MutableInt minute = new MutableInt();
	protected final MutableInt hour = new MutableInt();
	protected final MutableInt dayOfMonth = new MutableInt();
	protected final MutableInt month = new MutableInt();
	protected final MutableInt year = new MutableInt();

	protected CronDate(ICronConfig config) {
		if (config == null) {
			throw new NullPointerException("config");
		}
		this.config = config;
	}

	public abstract long getTimeInMillis();

	protected void roll(MutableInt currentValue, List<Integer> allowedValues, MutableBoolean force, MutableBoolean changed, int min, int max) {
		int value = currentValue.intValue();

		// For increment the value?
		if (force.isTrue()) {
			value++;
			if (value > max) {
				value = min;
				force.setValue(true);
			} else {
				force.setValue(false);
			}
			currentValue.setValue(value);
		}

		// Is it valid?
		if (allowedValues.isEmpty() || allowedValues.contains(value)) {
			return;
		}

		// Find the next value
		for (Integer allowedValue : allowedValues) {
			if (value < allowedValue) {
				currentValue.setValue(allowedValue);
				return;
			}
		}

		// Next value not found, rolling
		currentValue.setValue(allowedValues.get(0));
		force.setValue(true);
	}

	protected boolean isValidDate(int current, List<Integer> values) {
		return values.isEmpty() || values.contains(current);
	}

}
