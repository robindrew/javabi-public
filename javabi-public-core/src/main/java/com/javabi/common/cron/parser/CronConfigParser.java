package com.javabi.common.cron.parser;

import java.util.List;

import com.javabi.common.cron.CronConfig;
import com.javabi.common.cron.CronSchedulers;
import com.javabi.common.cron.ICronConfig;
import com.javabi.common.cron.ICronConfigParser;
import com.javabi.common.text.Strings;

public class CronConfigParser implements ICronConfigParser {

	private boolean secondsEnabled = false;

	public void setSeconds(boolean enabled) {
		this.secondsEnabled = enabled;
	}

	@Override
	public ICronConfig parse(String text) {

		// Handle macros
		if (text.startsWith("@")) {
			return parseMacro(text);
		}

		ICronConfig config = new CronConfig();

		List<String> tokens = Strings.splitOnWhitespace(text);
		if (tokens.size() != (secondsEnabled ? 6 : 5)) {
			throw new IllegalArgumentException("badly formatted: " + text);
		}

		// Cron does NOT normally support seconds
		// However our scheduler has optional support!
		int secondsOffset = 0;
		if (secondsEnabled) {
			new SecondParser().parse(config, tokens.get(0));
		} else {
			secondsOffset = 1;
			config.addSecond(0);
		}

		new MinuteParser().parse(config, tokens.get(1 - secondsOffset));
		new HourParser().parse(config, tokens.get(2 - secondsOffset));
		new DayOfMonthParser().parse(config, tokens.get(3 - secondsOffset));
		new MonthParser().parse(config, tokens.get(4 - secondsOffset));
		new DayOfWeekParser().parse(config, tokens.get(5 - secondsOffset));

		return config;
	}

	private ICronConfig parseMacro(String macro) {
		switch (macro) {
			case "@annually":
			case "@yearly":
				return CronSchedulers.yearly();
			case "@monthly":
				return CronSchedulers.monthly();
			case "@weekly":
				return CronSchedulers.weekly();
			case "@daily":
				return CronSchedulers.daily();
			case "@hourly":
				return CronSchedulers.hourly();
			default:
				throw new IllegalArgumentException("Macro not defined: " + macro);
		}
	}

}
