package com.javabi.common.cron;

public interface ICronConfigParser {

	ICronConfig parse(String text);

}
