package com.javabi.common.cron;

public interface ICronScheduler<D> {

	D getNextDate();

	D getNextDate(long timeInMillis);

	D getNextDate(D date);

	long getNext();

	long getNext(long timeInMillis);

}
