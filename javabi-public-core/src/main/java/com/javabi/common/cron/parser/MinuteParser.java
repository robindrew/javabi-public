package com.javabi.common.cron.parser;

import static com.javabi.common.cron.ICronConfig.MINUTE_MAX;
import static com.javabi.common.cron.ICronConfig.MINUTE_MIN;

import com.javabi.common.cron.ICronConfig;

class MinuteParser extends UnitParser {

	public MinuteParser() {
		super(MINUTE_MIN, MINUTE_MAX);
	}

	@Override
	void addUnit(ICronConfig config, int value) {
		config.addMinute(value);
	}

}
