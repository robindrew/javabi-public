package com.javabi.common.cron;

import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class CronConfig implements ICronConfig {

	private final List<Integer> months;
	private final List<Integer> daysOfMonth;
	private final List<Integer> daysOfWeek;
	private final List<Integer> hours;
	private final List<Integer> minutes;
	private final List<Integer> seconds;

	public CronConfig() {
		this.months = new ArrayList<>();
		this.daysOfMonth = new ArrayList<>();
		this.daysOfWeek = new ArrayList<>();
		this.hours = new ArrayList<>();
		this.minutes = new ArrayList<>();
		this.seconds = new ArrayList<>();
	}

	public CronConfig(ICronConfig config) {
		if (config.isEmpty()) {
			throw new IllegalArgumentException("config is empty!");
		}
		this.months = sort(config.getMonths());
		this.daysOfMonth = sort(config.getDaysOfMonth());
		this.daysOfWeek = sort(config.getDaysOfWeek());
		this.hours = sort(config.getHours());
		this.minutes = sort(config.getMinutes());
		this.seconds = sort(config.getSeconds());
	}

	private List<Integer> sort(List<Integer> list) {
		return new ArrayList<>(new TreeSet<>(list));
	}

	@Override
	public boolean isEmpty() {
		return getMonths().isEmpty() && daysOfMonth.isEmpty() && daysOfWeek.isEmpty() && hours.isEmpty() && minutes.isEmpty() && seconds.isEmpty();
	}

	@Override
	public List<Integer> getMonths() {
		return unmodifiableList(months);
	}

	@Override
	public List<Integer> getDaysOfMonth() {
		return unmodifiableList(daysOfMonth);
	}

	@Override
	public List<Integer> getDaysOfWeek() {
		return unmodifiableList(daysOfWeek);
	}

	@Override
	public List<Integer> getHours() {
		return unmodifiableList(hours);
	}

	@Override
	public List<Integer> getMinutes() {
		return unmodifiableList(minutes);
	}

	@Override
	public List<Integer> getSeconds() {
		return unmodifiableList(seconds);
	}

	@Override
	public void addMonth(int month) {
		if (month < MONTH_MIN || month > MONTH_MAX) {
			throw new IllegalArgumentException("month=" + month);
		}
		months.add(month);
	}

	@Override
	public void addDayOfMonth(int day) {
		if (day < DAY_OF_MONTH_MIN || day > DAY_OF_MONTH_MAX) {
			throw new IllegalArgumentException("day=" + day);
		}
		daysOfMonth.add(day);
	}

	@Override
	public void addDayOfWeek(int day) {
		if (day < DAY_OF_WEEK_MIN || day > DAY_OF_WEEK_MAX) {
			throw new IllegalArgumentException("day=" + day);
		}
		daysOfWeek.add(day);
	}

	@Override
	public void addHour(int hour) {
		if (hour < HOUR_MIN || hour > HOUR_MAX) {
			throw new IllegalArgumentException("hour=" + hour);
		}
		hours.add(hour);
	}

	@Override
	public void addMinute(int minute) {
		if (minute < MINUTE_MIN || minute > MINUTE_MAX) {
			throw new IllegalArgumentException("minute=" + minute);
		}
		minutes.add(minute);
	}

	@Override
	public void addSecond(int second) {
		if (second < SECOND_MIN || second > SECOND_MAX) {
			throw new IllegalArgumentException("second=" + second);
		}
		seconds.add(second);
	}

	@Override
	public void addMonths(int from, int to) {
		if (from > to) {
			throw new IllegalArgumentException("from=" + from + ", to=" + to);
		}
		for (int i = from; i <= to; i++) {
			addMonth(i);
		}
	}

	@Override
	public void addDaysOfMonth(int from, int to) {
		if (from > to) {
			throw new IllegalArgumentException("from=" + from + ", to=" + to);
		}
		for (int i = from; i <= to; i++) {
			addDayOfMonth(i);
		}
	}

	@Override
	public void addDaysOfWeek(int from, int to) {
		if (from > to) {
			throw new IllegalArgumentException("from=" + from + ", to=" + to);
		}
		for (int i = from; i <= to; i++) {
			addDayOfWeek(i);
		}
	}

	@Override
	public void addHours(int from, int to) {
		if (from > to) {
			throw new IllegalArgumentException("from=" + from + ", to=" + to);
		}
		for (int i = from; i <= to; i++) {
			addHour(i);
		}
	}

	@Override
	public void addMinutes(int from, int to) {
		if (from > to) {
			throw new IllegalArgumentException("from=" + from + ", to=" + to);
		}
		for (int i = from; i <= to; i++) {
			addMinute(i);
		}
	}

	@Override
	public void addSeconds(int from, int to) {
		if (from > to) {
			throw new IllegalArgumentException("from=" + from + ", to=" + to);
		}
		for (int i = from; i <= to; i++) {
			addSecond(i);
		}
	}

	@Override
	public int getLowestHour() {
		return hours.isEmpty() ? HOUR_MIN : hours.get(0);
	}

	@Override
	public int getLowestMinute() {
		return minutes.isEmpty() ? MINUTE_MIN : minutes.get(0);
	}

	@Override
	public int getLowestSecond() {
		return seconds.isEmpty() ? SECOND_MIN : seconds.get(0);
	}

}
