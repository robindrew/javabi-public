package com.javabi.common.cron.parser;

import static com.google.common.collect.Lists.newArrayList;
import static com.javabi.common.cron.ICronConfig.DAY_OF_WEEK_MAX;
import static com.javabi.common.cron.ICronConfig.DAY_OF_WEEK_MIN;

import java.util.List;

import com.javabi.common.cron.ICronConfig;

class DayOfWeekParser extends UnitParser {

	private static final List<String> DAYS_OF_WEEK = newArrayList("SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT");

	public DayOfWeekParser() {
		super(DAY_OF_WEEK_MIN, DAY_OF_WEEK_MAX);
	}

	@Override
	public List<String> getTextValues() {
		return DAYS_OF_WEEK;
	}

	@Override
	void addUnit(ICronConfig config, int value) {
		// Move Sunday to end of the week
		value = (value == 0) ? 7 : value;
		config.addDayOfWeek(value);
	}

}
