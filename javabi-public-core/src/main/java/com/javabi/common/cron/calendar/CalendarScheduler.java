package com.javabi.common.cron.calendar;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.javabi.common.cron.ICronScheduler;
import com.javabi.common.cron.ICronConfig;

public class CalendarScheduler implements ICronScheduler<Calendar> {

	private final ICronConfig config;

	public CalendarScheduler(ICronConfig config) {
		if (config == null) {
			throw new NullPointerException("config");
		}
		this.config = config;
	}

	@Override
	public Calendar getNextDate(long timeInMillis) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(timeInMillis);

		CalendarSchedulerDate date = new CalendarSchedulerDate(calendar, config);
		date.rollToValid();
		date.toCalendar(calendar);
		return calendar;
	}

	@Override
	public Calendar getNextDate() {
		return getNextDate(System.currentTimeMillis());
	}

	@Override
	public Calendar getNextDate(Calendar date) {
		return getNextDate(date.getTimeInMillis());
	}

	@Override
	public long getNext() {
		return getNextDate().getTimeInMillis();
	}

	@Override
	public long getNext(long timeInMillis) {
		return getNextDate(timeInMillis).getTimeInMillis();
	}

}
