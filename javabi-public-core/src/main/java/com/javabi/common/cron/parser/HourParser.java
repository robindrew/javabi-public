package com.javabi.common.cron.parser;

import static com.javabi.common.cron.ICronConfig.HOUR_MAX;
import static com.javabi.common.cron.ICronConfig.HOUR_MIN;

import com.javabi.common.cron.ICronConfig;

class HourParser extends UnitParser {

	public HourParser() {
		super(HOUR_MIN, HOUR_MAX);
	}

	@Override
	void addUnit(ICronConfig config, int value) {
		config.addHour(value);
	}

}
