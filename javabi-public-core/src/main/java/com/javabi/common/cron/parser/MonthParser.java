package com.javabi.common.cron.parser;

import static com.google.common.collect.Lists.newArrayList;
import static com.javabi.common.cron.ICronConfig.MONTH_MAX;
import static com.javabi.common.cron.ICronConfig.MONTH_MIN;

import java.util.List;

import com.javabi.common.cron.ICronConfig;

class MonthParser extends UnitParser {

	private static final List<String> MONTHS_OF_YEAR = newArrayList("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC");

	public MonthParser() {
		super(MONTH_MIN, MONTH_MAX);
	}

	@Override
	public List<String> getTextValues() {
		return MONTHS_OF_YEAR;
	}

	@Override
	void addUnit(ICronConfig config, int value) {
		config.addMonth(value);
	}

}
