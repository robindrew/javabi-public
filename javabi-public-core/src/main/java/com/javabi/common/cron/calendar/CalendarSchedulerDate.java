package com.javabi.common.cron.calendar;

import static com.javabi.common.cron.ICronConfig.DAY_OF_MONTH_MAX;
import static com.javabi.common.cron.ICronConfig.DAY_OF_MONTH_MIN;
import static com.javabi.common.cron.ICronConfig.HOUR_MAX;
import static com.javabi.common.cron.ICronConfig.HOUR_MIN;
import static com.javabi.common.cron.ICronConfig.MINUTE_MAX;
import static com.javabi.common.cron.ICronConfig.MINUTE_MIN;
import static com.javabi.common.cron.ICronConfig.MONTH_MAX;
import static com.javabi.common.cron.ICronConfig.MONTH_MIN;
import static com.javabi.common.cron.ICronConfig.SECOND_MAX;
import static com.javabi.common.cron.ICronConfig.SECOND_MIN;
import static java.util.Calendar.DATE;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.DAY_OF_WEEK;
import static java.util.Calendar.HOUR;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.SECOND;
import static java.util.Calendar.YEAR;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.mutable.MutableBoolean;

import com.javabi.common.cron.CronDate;
import com.javabi.common.cron.ICronConfig;

public class CalendarSchedulerDate extends CronDate {

	private static final int MONTH_OFFSET = 1;

	private final Calendar calendar = new GregorianCalendar();

	public CalendarSchedulerDate(Calendar calendar, ICronConfig config) {
		super(config);
		set(calendar);
	}

	@Override
	public long getTimeInMillis() {
		return get().getTimeInMillis();
	}

	public Calendar toCalendar() {
		return toCalendar(new GregorianCalendar());
	}

	public Calendar toCalendar(Calendar calendar) {
		calendar.setTimeInMillis(getTimeInMillis());
		return calendar;
	}

	protected Calendar get() {
		calendar.set(MILLISECOND, 0);
		calendar.set(SECOND, second.intValue());
		calendar.set(MINUTE, minute.intValue());
		calendar.set(HOUR, hour.intValue());
		calendar.set(DAY_OF_MONTH, dayOfMonth.intValue());
		calendar.set(MONTH, month.intValue() - MONTH_OFFSET);
		calendar.set(YEAR, year.intValue());
		return calendar;
	}

	protected void set(Calendar calendar) {
		second.setValue(calendar.get(SECOND));
		minute.setValue(calendar.get(MINUTE));
		hour.setValue(calendar.get(HOUR));
		dayOfMonth.setValue(calendar.get(DAY_OF_MONTH));
		month.setValue(calendar.get(MONTH) + MONTH_OFFSET);
		year.setValue(calendar.get(YEAR));
	}

	public boolean isValidDate() {
		Calendar date = get();
		return isValidDate(date, true);
	}

	private boolean isValidDate(Calendar date, boolean checkTime) {

		// Check the date ...
		int dayOfMonth = date.get(DAY_OF_MONTH);
		int dayOfWeek = convertDateOfWeek(date.get(DAY_OF_WEEK));
		int month = date.get(MONTH) + MONTH_OFFSET;

		if (!isValidDate(dayOfMonth, config.getDaysOfMonth())) {
			return false;
		}
		if (!isValidDate(dayOfWeek, config.getDaysOfWeek())) {
			return false;
		}
		if (!isValidDate(month, config.getMonths())) {
			return false;
		}

		// Check the time?
		if (checkTime) {
			int hour = date.get(HOUR);
			int minute = date.get(MINUTE);
			int second = date.get(SECOND);

			if (!isValidDate(hour, config.getHours())) {
				return false;
			}
			if (!isValidDate(minute, config.getMinutes())) {
				return false;
			}
			if (!isValidDate(second, config.getSeconds())) {
				return false;
			}
		}

		return true;

	}

	private int convertDateOfWeek(int day) {
		// Convert from calendar to standard day of week
		return (day == 1) ? 7 : (day - 1);
	}

	/**
	 * This method rolls to the next valid date. If it is currently valid, it will still roll. To check whether it is
	 * currently valid, call {@link #isValidDate()}
	 */
	public void rollToValid() {
		MutableBoolean force = new MutableBoolean(true);
		MutableBoolean changed = new MutableBoolean(false);

		// Roll hours, minutes & seconds
		roll(second, config.getSeconds(), force, changed, SECOND_MIN, SECOND_MAX);
		roll(minute, config.getMinutes(), force, changed, MINUTE_MIN, MINUTE_MAX);
		roll(hour, config.getHours(), force, changed, HOUR_MIN, HOUR_MAX);

		// Shorcut days - efficient!
		if (skipDays()) {
			roll(dayOfMonth, config.getDaysOfMonth(), force, changed, DAY_OF_MONTH_MIN, DAY_OF_MONTH_MAX);
			roll(month, config.getMonths(), force, changed, MONTH_MIN, MONTH_MAX);
			if (force.isTrue()) {
				year.increment();
			}
			return;
		}

		// Roll one day at a time ...
		// (inefficient over longer time periods)
		int hour = config.getLowestHour();
		int minute = config.getLowestMinute();
		int second = config.getLowestSecond();

		boolean rolled = false;
		Calendar date = get();
		while (!isValidDate(date, false) || force.isTrue()) {
			force.setValue(false);
			changed.setValue(true);

			date.set(HOUR_OF_DAY, hour);
			date.set(MINUTE, minute);
			date.set(SECOND, second);
			date.add(DATE, 1);
			rolled = true;
		}
		if (rolled) {
			set(date);
		}
	}

	private boolean skipDays() {
		if (!config.getDaysOfWeek().isEmpty()) {
			return false;
		}
		if (config.getDaysOfMonth().size() != 1) {
			return false;
		}

		// We can only skip days of month if they are valid for ALL months
		return config.getDaysOfMonth().get(0) < 28;
	}

}
