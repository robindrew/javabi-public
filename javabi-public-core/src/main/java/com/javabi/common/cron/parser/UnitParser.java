package com.javabi.common.cron.parser;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.javabi.common.cron.ICronConfig;
import com.javabi.common.lang.Empty;

abstract class UnitParser {

	private final int min;
	private final int max;

	protected UnitParser(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public List<String> getTextValues() {
		return Empty.list();
	}

	public void parse(ICronConfig config, String token) {
		for (Integer value : parseValues(token)) {
			addUnit(config, value);
		}
	}

	private Collection<Integer> parseValues(String token) {
		if (token.equals("*")) {
			return Empty.list();
		}

		Set<Integer> values = getValues(token);
		int frequency = getFrequency(token);

		if (frequency > 1) {
			Iterator<Integer> iterator = values.iterator();
			while (iterator.hasNext()) {
				int value = iterator.next();
				if (value % frequency != 0) {
					iterator.remove();
				}
			}
		}

		return values;
	}

	private int getFrequency(String token) {
		int slash = token.indexOf('/');
		if (slash != -1) {
			return Integer.parseInt(token.substring(slash + 1));
		}
		return 1;
	}

	private Set<Integer> getValues(String token) {

		// Frequency is not handled here
		int slash = token.indexOf('/');
		if (slash != -1) {
			token = token.substring(0, slash);
		}

		Set<Integer> values = new TreeSet<>();

		// CASE: All values
		if (token.equals("*")) {
			addValues(values, min, max);
			return values;
		}

		// Values can be comma separated
		for (String element : token.split(",")) {

			// Ranges require a dash
			int range = element.indexOf('-');
			if (range != -1) {
				int from = parseInt(element.substring(0, range).trim());
				int to = parseInt(element.substring(range + 1).trim());
				addValues(values, from, to);
			}

			// Basic case
			else {
				values.add(parseInt(element));
			}
		}
		return values;
	}

	private int parseInt(String value) {

		// The text values allows for months (JAN, FEB, etc)
		// and days of week (SUN, MON, etc)
		List<String> textValues = getTextValues();
		if (!textValues.isEmpty()) {
			value = value.toUpperCase();
			int index = textValues.indexOf(value);
			if (index != -1) {
				return index;
			}
		}

		// Basic case
		return Integer.parseInt(value);
	}

	private void addValues(Set<Integer> values, int min, int max) {
		for (int value = min; value <= max; value++) {
			values.add(value);
		}
	}

	abstract void addUnit(ICronConfig config, int value);

}
