package com.javabi.common.cron.parser;

import static com.javabi.common.cron.ICronConfig.SECOND_MAX;
import static com.javabi.common.cron.ICronConfig.SECOND_MIN;

import com.javabi.common.cron.ICronConfig;

class SecondParser extends UnitParser {

	public SecondParser() {
		super(SECOND_MIN, SECOND_MAX);
	}

	@Override
	void addUnit(ICronConfig config, int value) {
		config.addSecond(value);
	}

}
