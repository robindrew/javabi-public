package com.javabi.common.event;

/**
 * An Event Listener.
 */
@FunctionalInterface
public interface IEventListener<E> {

	void receiveEvent(E event);
	
}
