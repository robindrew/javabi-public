package com.javabi.common.event;

import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

public class BatchedEventListener<E> implements IEventListener<E>, IEventPublisher<List<E>>, Runnable {

	private static final int DEFAULT_PAUSE_TIME_MILLIS = 50;

	private final BlockingQueue<E> eventQueue;
	private final IEventListenerSet<List<E>> listenerSet = new EventListenerSet<>();
	private final AtomicLong pauseTimeMillis = new AtomicLong(DEFAULT_PAUSE_TIME_MILLIS);

	public BatchedEventListener(String name, BlockingQueue<E> queue) {
		if (queue == null) {
			throw new NullPointerException("queue");
		}
		this.eventQueue = queue;
	}

	public BatchedEventListener(String name) {
		this(name, new LinkedBlockingQueue<>());
	}

	public IEventListenerSet<List<E>> getListenerSet() {
		return listenerSet;
	}

	public long getPauseTimeMillis() {
		return pauseTimeMillis.get();
	}

	public void setPauseTimeMillis() {

	}

	public void run() {
		try {
			while (true) {
				if (eventQueue.isEmpty()) {

					// If the queue is empty we pause for a moment
					Thread.sleep(getPauseTimeMillis());

				} else {
					List<E> eventBatch = new ArrayList<>();
					eventQueue.drainTo(eventBatch);

					// The batch will be published to many listeners
					// protect it by making it unmodifiable
					eventBatch = unmodifiableList(eventBatch);
					publishEvent(eventBatch);
				}
			}
		} catch (InterruptedException ie) {
			handleInterrupted(ie);
		}
	}

	/**
	 * Override this method to handle the
	 * @param ie
	 */
	protected void handleInterrupted(InterruptedException ie) {
		if (ie != null) {
			ie.printStackTrace();
		}
	}

	@Override
	public void receiveEvent(E event) {
		eventQueue.add(event);
	}

	@Override
	public void publishEvent(List<E> eventBatch) {
		listenerSet.receiveEvent(eventBatch);
	}

}
