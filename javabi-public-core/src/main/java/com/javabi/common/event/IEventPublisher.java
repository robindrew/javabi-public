package com.javabi.common.event;

/**
 * An event publisher.
 */
@FunctionalInterface
public interface IEventPublisher<E> {

	void publishEvent(E event);

}
