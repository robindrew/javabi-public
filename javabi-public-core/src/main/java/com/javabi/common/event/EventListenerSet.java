package com.javabi.common.event;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import com.javabi.common.lang.ThreadSafe;

/**
 * An event listener set backed by a copy-on-write set for optimal iteration speed.
 */
@ThreadSafe
public class EventListenerSet<E> implements IEventListenerSet<E> {

	/** The listener set. */
	private final Set<IEventListener<E>> listenerSet = new CopyOnWriteArraySet<>();

	@Override
	public boolean addListener(IEventListener<E> listener) {
		if (listener == null) {
			throw new NullPointerException();
		}
		return listenerSet.add(listener);
	}

	@Override
	public boolean removeListener(IEventListener<E> listener) {
		if (listener == null) {
			throw new NullPointerException();
		}
		return listenerSet.remove(listener);
	}

	@Override
	public Iterator<IEventListener<E>> iterator() {
		return listenerSet.iterator();
	}

	@Override
	public void receiveEvent(E event) {
		if (event == null) {
			throw new NullPointerException("event");
		}
		for (IEventListener<E> listener : listenerSet) {
			listener.receiveEvent(event);
		}
	}
}
