package com.javabi.common.event;

public interface IEventListenerSet<E> extends IEventListener<E>, Iterable<IEventListener<E>> {

	/**
	 * Register the given listener.
	 * @param listener the listener.
	 * @return true if registered.
	 */
	boolean addListener(IEventListener<E> listener);

	/**
	 * Un-register the given listener.
	 * @param listener the listener.
	 * @return true if registered.
	 */
	boolean removeListener(IEventListener<E> listener);

}