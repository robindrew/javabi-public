package com.javabi.common.collect.count;

public interface ICountEntry<E> {

	E getKey();

	long getValue();
}
