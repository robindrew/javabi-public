package com.javabi.common.collect.table;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

public class ColumnSet<C> implements IColumnSet<C> {

	private final Set<C> columns;
	private final Map<C, Integer> columnToIndexMap = new HashMap<>();

	public ColumnSet(Set<C> columns) {
		if (columns.isEmpty()) {
			throw new IllegalArgumentException("columns is empty");
		}
		this.columns = ImmutableSet.copyOf(columns);
	}

	@Override
	public Iterator<C> iterator() {
		return columns.iterator();
	}

	@Override
	public int size() {
		return columns.size();
	}

	@Override
	public int indexOf(C column) {
		Integer index = columnToIndexMap.get(column);
		return (index == null ? -1 : index);
	}

	@Override
	public Set<C> toSet() {
		return columns;
	}

}
