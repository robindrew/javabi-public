package com.javabi.common.collect.table.map;

import java.util.Map;

public interface IMapRow<K, V> extends Map<K, V> {

	int columns();

	void set(K key, V value);

}
