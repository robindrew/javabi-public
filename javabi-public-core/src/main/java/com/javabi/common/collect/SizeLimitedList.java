package com.javabi.common.collect;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class SizeLimitedList<E> implements List<E> {

	private final int capacity;
	private final LinkedList<E> list = new LinkedList<E>();

	public SizeLimitedList(int capacity) {
		this.capacity = capacity;
	}

	protected void resize() {
		while (list.size() > capacity) {
			list.removeFirst();
		}
	}

	@Override
	public boolean add(E e) {
		boolean value = list.add(e);
		resize();
		return value;
	}

	@Override
	public void add(int index, E element) {
		list.add(index, element);
		resize();
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		boolean value = list.addAll(c);
		resize();
		return value;
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		boolean value = list.addAll(index, c);
		resize();
		return value;
	}

	public void addFirst(E e) {
		list.addFirst(e);
		resize();
	}

	public void addLast(E e) {
		list.addLast(e);
		resize();
	}

	@Override
	public void clear() {
		list.clear();
	}

	@Override
	public Object clone() {
		return list.clone();
	}

	@Override
	public boolean contains(Object o) {
		return list.contains(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return list.containsAll(c);
	}

	public Iterator<E> descendingIterator() {
		return list.descendingIterator();
	}

	public E element() {
		return list.element();
	}

	@Override
	public boolean equals(Object o) {
		return list.equals(o);
	}

	@Override
	public E get(int index) {
		return list.get(index);
	}

	public E getFirst() {
		return list.getFirst();
	}

	public E getLast() {
		return list.getLast();
	}

	@Override
	public int hashCode() {
		return list.hashCode();
	}

	@Override
	public int indexOf(Object o) {
		return list.indexOf(o);
	}

	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}

	@Override
	public Iterator<E> iterator() {
		return list.iterator();
	}

	@Override
	public int lastIndexOf(Object o) {
		return list.lastIndexOf(o);
	}

	@Override
	public ListIterator<E> listIterator() {
		return list.listIterator();
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		return list.listIterator(index);
	}

	public boolean offer(E e) {
		boolean value = list.offer(e);
		resize();
		return value;
	}

	public boolean offerFirst(E e) {
		boolean value = list.offerFirst(e);
		resize();
		return value;
	}

	public boolean offerLast(E e) {
		boolean value = list.offerLast(e);
		resize();
		return value;
	}

	public E peek() {
		return list.peek();
	}

	public E peekFirst() {
		return list.peekFirst();
	}

	public E peekLast() {
		return list.peekLast();
	}

	public E poll() {
		return list.poll();
	}

	public E pollFirst() {
		return list.pollFirst();
	}

	public E pollLast() {
		return list.pollLast();
	}

	public E pop() {
		return list.pop();
	}

	public void push(E e) {
		list.push(e);
		resize();
	}

	public E remove() {
		return list.remove();
	}

	@Override
	public E remove(int index) {
		return list.remove(index);
	}

	@Override
	public boolean remove(Object o) {
		return list.remove(o);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return list.removeAll(c);
	}

	public E removeFirst() {
		return list.removeFirst();
	}

	public boolean removeFirstOccurrence(Object o) {
		return list.removeFirstOccurrence(o);
	}

	public E removeLast() {
		return list.removeLast();
	}

	public boolean removeLastOccurrence(Object o) {
		return list.removeLastOccurrence(o);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return list.retainAll(c);
	}

	@Override
	public E set(int index, E element) {
		E replaced = list.set(index, element);
		resize();
		return replaced;
	}

	@Override
	public int size() {
		return list.size();
	}

	@Override
	public java.util.List<E> subList(int fromIndex, int toIndex) {
		return list.subList(fromIndex, toIndex);
	}

	@Override
	public Object[] toArray() {
		return list.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return list.toArray(a);
	}

	@Override
	public String toString() {
		return list.toString();
	}

}
