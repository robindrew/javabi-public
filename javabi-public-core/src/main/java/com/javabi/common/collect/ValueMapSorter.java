package com.javabi.common.collect;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ValueMapSorter {

	@SuppressWarnings("unchecked")
	public <K, V> Map<K, V> sort(Map<K, V> map, Comparator<Entry<K, V>> comparator) {

		Entry<K, V>[] entries = map.entrySet().toArray(new Entry[map.size()]);
		Arrays.sort(entries, comparator);

		Map<K, V> sorted = new LinkedHashMap<K, V>();
		for (Entry<K, V> entry : entries) {
			sorted.put(entry.getKey(), entry.getValue());
		}
		return sorted;
	}

}
