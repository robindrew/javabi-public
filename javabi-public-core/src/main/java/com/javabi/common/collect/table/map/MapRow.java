package com.javabi.common.collect.table.map;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapRow<K, V> implements IMapRow<K, V> {

	private final Map<K, V> map = new HashMap<K, V>();

	@Override
	public int columns() {
		return map.size();
	}

	@Override
	public String toString() {
		return map.toString();
	}

	@Override
	public void set(K key, V value) {
		put(key, value);
	}

	@Override
	public V put(K key, V value) {
		return map.put(key, value);
	}

	@Override
	public V get(Object key) {
		return map.get(key);
	}

	@Override
	public int size() {
		return columns();
	}

	@Override
	public boolean isEmpty() {
		return size() > 0;
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return map.containsValue(value);
	}

	@Override
	public V remove(Object key) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Set<K> keySet() {
		return map.keySet();
	}

	@Override
	public Collection<V> values() {
		return map.values();
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		throw new UnsupportedOperationException();
	}

}
