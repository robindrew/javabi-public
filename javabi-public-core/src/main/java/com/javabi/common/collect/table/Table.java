package com.javabi.common.collect.table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Table<C, V> implements ITable<C, V> {

	private final IColumnSet<C> columns;
	private final List<IRow<C, V>> rowList = new ArrayList<>();
	
	public Table(IColumnSet<C> columns) {
		if (columns == null) {
			throw new NullPointerException("columns");
		}
		this.columns = columns;
	}
	
	public void addRow(Collection<V> row) {
		
	}
	
	@Override
	public int columns() {
		return columns.size();
	}

	@Override
	public int rows() {
		return rowList.size();
	}

	@Override
	public IColumnSet<C> getColumns() {
		return columns;
	}

	@Override
	public List<IRow<C, V>> getRowList() {
		return rowList;
	}

	@Override
	public IRow<C, V> getRow(int rowIndex) {
		return rowList.get(rowIndex);
	}

}
