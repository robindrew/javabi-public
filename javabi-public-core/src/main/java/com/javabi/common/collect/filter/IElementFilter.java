package com.javabi.common.collect.filter;

public interface IElementFilter<E> {

	boolean removeElement(E element);

}
