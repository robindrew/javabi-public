package com.javabi.common.collect.table;

import java.util.List;

public interface ITable<C, V> {

	int columns();

	int rows();

	IColumnSet<C> getColumns();

	List<IRow<C, V>> getRowList();

	IRow<C, V> getRow(int rowIndex);

}
