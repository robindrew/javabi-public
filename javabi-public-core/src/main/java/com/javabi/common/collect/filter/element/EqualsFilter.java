package com.javabi.common.collect.filter.element;

import com.javabi.common.collect.filter.IElementFilter;

public class EqualsFilter<E> implements IElementFilter<E> {

	private final E element;

	public EqualsFilter(E element) {
		this.element = element;
	}

	public E get() {
		return element;
	}

	@Override
	public boolean removeElement(E element) {
		if (get() == element) {
			return true;
		}
		if (get() == null || element == null) {
			return false;
		}
		return get().equals(element);
	}

}
