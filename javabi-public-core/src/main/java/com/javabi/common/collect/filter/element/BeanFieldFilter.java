package com.javabi.common.collect.filter.element;

import com.javabi.common.collect.filter.IElementFilter;
import com.javabi.common.lang.reflect.field.Fields;
import com.javabi.common.lang.reflect.field.IField;
import com.javabi.common.lang.reflect.field.InstanceField;

public abstract class BeanFieldFilter<B, V> implements IElementFilter<B> {

	private final IField field;

	public BeanFieldFilter(Class<B> type, String field) {
		this.field = Fields.get(type, field);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean removeElement(B element) {
		InstanceField accessible = new InstanceField(field, element);
		V value = (V) accessible.getValue();
		return removeValue(value);
	}

	public abstract boolean removeValue(V value);

}
