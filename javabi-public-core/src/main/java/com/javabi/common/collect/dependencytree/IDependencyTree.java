package com.javabi.common.collect.dependencytree;

import java.util.Collection;

public interface IDependencyTree<N> {

	Collection<N> parents();

	void addParent(N parent);

	void removeParent(N parent);

	void addChild(N parent, N child);

	Collection<N> getParents(N node);

	Collection<N> getChildren(N node);

}
