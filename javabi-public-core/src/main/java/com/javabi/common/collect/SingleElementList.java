package com.javabi.common.collect;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class SingleElementList<E> implements List<E> {

	private boolean empty = true;
	private E element = null;

	public SingleElementList() {
	}

	public SingleElementList(E element) {
		add(element);
	}

	public int size() {
		return empty ? 0 : 1;
	}

	public boolean isEmpty() {
		return empty;
	}

	public void clear() {
		empty = true;
		element = null;
	}

	public boolean add(E element) {
		if (!empty) {
			throw new IllegalStateException("Attempt to add element to non-empty list");
		}
		this.element = element;
		this.empty = false;
		return true;
	}

	public E set(int index, E element) {
		if (empty || index != 0) {
			throw new IllegalArgumentException("index=" + index);
		}
		E replaced = this.element;
		this.element = element;
		return replaced;
	}

	public E get(int index) {
		if (empty || index != 0) {
			throw new IllegalArgumentException("index=" + index);
		}
		return element;
	}

	public int indexOf(Object object) {
		if (empty) {
			return -1;
		}
		if (element == object) {
			return 0;
		}
		if (element != null && element.equals(object)) {
			return 0;
		}
		return -1;
	}

	public boolean contains(Object object) {
		return indexOf(object) != -1;
	}

	public int lastIndexOf(Object object) {
		return indexOf(object);
	}

	public E remove(int index) {
		if (empty || index != 0) {
			throw new IllegalArgumentException("index=" + index);
		}
		E removed = element;
		element = null;
		empty = true;
		return removed;
	}

	public boolean remove(Object object) {
		if (contains(object)) {
			element = null;
			empty = true;
			return true;
		}
		return false;
	}

	public Object[] toArray() {
		if (empty) {
			return new Object[0];
		}
		return new Object[] { element };
	}

	public Iterator<E> iterator() {
		return new SingleElementIterator();
	}

	public ListIterator<E> listIterator() {
		return new SingleElementIterator();
	}

	public void add(int arg0, E arg1) {
		throw new UnsupportedOperationException("Method Not Implemented");
	}

	public boolean addAll(Collection<? extends E> arg0) {
		throw new UnsupportedOperationException("Method Not Implemented");
	}

	public boolean addAll(int arg0, Collection<? extends E> arg1) {
		throw new UnsupportedOperationException("Method Not Implemented");
	}

	public boolean containsAll(Collection<?> arg0) {
		throw new UnsupportedOperationException("Method Not Implemented");
	}

	public ListIterator<E> listIterator(int arg0) {
		throw new UnsupportedOperationException("Method Not Implemented");
	}

	public boolean removeAll(Collection<?> arg0) {
		throw new UnsupportedOperationException("Method Not Implemented");
	}

	public boolean retainAll(Collection<?> arg0) {
		throw new UnsupportedOperationException("Method Not Implemented");
	}

	public List<E> subList(int arg0, int arg1) {
		throw new UnsupportedOperationException("Method Not Implemented");
	}

	public <T> T[] toArray(T[] arg0) {
		throw new UnsupportedOperationException("Method Not Implemented");
	}

	private class SingleElementIterator implements ListIterator<E> {

		private boolean next = true;

		public void add(E arg0) {
			throw new UnsupportedOperationException("Method Not Implemented");
		}

		public boolean hasNext() {
			if (!empty && next) {
				next = false;
				return true;
			}
			return false;
		}

		public boolean hasPrevious() {
			throw new UnsupportedOperationException("Method Not Implemented");
		}

		public E next() {
			return element;
		}

		public int nextIndex() {
			throw new UnsupportedOperationException("Method Not Implemented");
		}

		public E previous() {
			throw new UnsupportedOperationException("Method Not Implemented");
		}

		public int previousIndex() {
			throw new UnsupportedOperationException("Method Not Implemented");
		}

		public void remove() {
			throw new UnsupportedOperationException("Method Not Implemented");
		}

		public void set(E arg0) {
			throw new UnsupportedOperationException("Method Not Implemented");
		}

	}

}
