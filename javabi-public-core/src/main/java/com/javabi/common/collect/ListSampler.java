package com.javabi.common.collect;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;

public class ListSampler implements IListSampler {

	private int count = 1;

	@Override
	public void setSampleCount(int count) {
		if (count < 1) {
			throw new IllegalArgumentException("size=" + count);
		}
		this.count = count;
	}

	@Override
	public <S> List<S> sample(List<S> stats) {
		if (stats.size() <= count) {
			return new ArrayList<S>(stats);
		}

		int endIndex = stats.size() - 1;
		List<S> list = new ArrayList<S>(count);

		// If only one element, get most recent
		if (count == 1) {
			list.add(stats.get(endIndex));
			return list;
		}

		// If only two elements, get first and most recent
		if (count == 2) {
			list.add(stats.get(0));
			list.add(stats.get(endIndex));
			return list;
		}

		// Over two, sample from the remaining stats
		int remainingCount = count - 1;
		int remainingSize = stats.size() - 1;
		int remainder = remainingSize / remainingCount;
		for (int i = 0; i < endIndex; i++) {
			if (i % remainder == 0) {
				list.add(stats.get(i));
				if (list.size() == remainingCount) {
					break;
				}
			}
		}

		list.add(stats.get(endIndex));
		return list;
	}

	public static void main(String[] args) {
		ListSampler sampler = new ListSampler();

		sampler.setSampleCount(3);
		System.out.println(sampler.sample(asList(1, 2, 3, 4)));
		sampler.setSampleCount(3);
		System.out.println(sampler.sample(asList(1, 2, 3, 4, 5)));
		sampler.setSampleCount(4);
		System.out.println(sampler.sample(asList(1, 2, 3, 4, 5)));
		sampler.setSampleCount(4);
		System.out.println(sampler.sample(asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)));
	}
}
