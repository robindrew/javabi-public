package com.javabi.common.collect.filter;

import java.util.Collection;
import java.util.List;

public interface ICollectionFilter<E> {

	<C extends Collection<E>> C filterCollection(C collection);

	<C extends Collection<E>> C filterCollection(C collection, List<E> toList);

	<C extends Collection<E>> C filterCollection(C collection, List<E> toList, boolean drain);

}
