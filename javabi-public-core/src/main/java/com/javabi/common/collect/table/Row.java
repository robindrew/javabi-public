package com.javabi.common.collect.table;

import java.util.Collections;
import java.util.List;

public class Row<C, V> implements IRow<C, V> {

	private final IColumnSet<C> columns;
	private final List<V> values;

	public Row(IColumnSet<C> columns, List<V> values) {
		if (columns == null) {
			throw new NullPointerException("columns");
		}
		this.columns = columns;
		this.values = values;
	}

	@Override
	public IColumnSet<C> getColumns() {
		return columns;
	}

	@Override
	public List<V> toList() {
		return Collections.unmodifiableList(values);
	}

	@Override
	public V get(int index) {
		return values.get(index);
	}

	@Override
	public V get(C column) {
		int index = columns.indexOf(column);
		return get(index);
	}

	@Override
	public int size() {
		return values.size();
	}

}
