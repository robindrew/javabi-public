package com.javabi.common.collect.table.list;

import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ArrayTable<V> implements IListTable<V> {

	private final int columns;
	private List<IListRow<V>> rows = new ArrayList<IListRow<V>>();

	public ArrayTable(int columns) {
		if (columns < 1) {
			throw new IllegalArgumentException("columns=" + columns);
		}
		this.columns = columns;
	}

	@Override
	public int rows() {
		return rows.size();
	}

	@Override
	public int columns() {
		return columns;
	}

	@Override
	public IListRow<V> row() {
		IListRow<V> row = new ListRow<V>(columns);
		rows.add(row);
		return row;
	}

	@Override
	public IListRow<V> get(int index) {
		return rows.get(index);
	}

	@Override
	public V get(int rowIndex, int columnIndex) {
		IListRow<V> row = get(rowIndex);
		return row.get(columnIndex);
	}

	@Override
	public List<IListRow<V>> getRows() {
		return unmodifiableList(rows);
	}

	@Override
	public int size() {
		return rows();
	}

	@Override
	public boolean isEmpty() {
		return rows() == 0;
	}

	@Override
	public void clear() {
		rows.clear();
	}

	@Override
	public Iterator<IListRow<V>> iterator() {
		return getRows().iterator();
	}

	@Override
	public Object[] toArray() {
		return getRows().toArray();
	}

	@Override
	public <T> T[] toArray(T[] array) {
		return getRows().toArray(array);
	}

	@Override
	public boolean contains(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean add(IListRow<V> e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(Collection<? extends IListRow<V>> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(int index, Collection<? extends IListRow<V>> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IListRow<V> set(int index, IListRow<V> element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void add(int index, IListRow<V> element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IListRow<V> remove(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int indexOf(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int lastIndexOf(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<IListRow<V>> listIterator() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<IListRow<V>> listIterator(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<IListRow<V>> subList(int fromIndex, int toIndex) {
		throw new UnsupportedOperationException();
	}

}
