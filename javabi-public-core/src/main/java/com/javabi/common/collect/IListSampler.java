package com.javabi.common.collect;

import java.util.List;

public interface IListSampler {

	void setSampleCount(int count);

	<S> List<S> sample(List<S> stats);

}
