package com.javabi.common.collect.table;

import java.util.Set;

public interface IColumnSet<C> extends Iterable<C> {

	int size();

	int indexOf(C column);

	Set<C> toSet();

}
