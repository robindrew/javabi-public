package com.javabi.common.collect;

import java.util.Collections;
import java.util.List;

import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;

public class StringListMap {

	private final List<String> list;
	private final TObjectIntMap<String> indexMap;

	public StringListMap(List<String> list) {
		this.list = list;
		this.indexMap = new TObjectIntHashMap<String>(list.size());
		for (int i = 0; i < list.size(); i++) {
			String key = list.get(i);
			indexMap.put(key, i);
		}
	}

	public List<String> getList() {
		return Collections.unmodifiableList(list);
	}

	public int indexOf(String key) {
		return indexMap.get(key);
	}

	public <E> E elementOf(String key, List<E> list) {
		int index = indexOf(key);
		if (index == -1) {
			return null;
		}
		return list.get(index);
	}

	public int size() {
		return list.size();
	}
}
