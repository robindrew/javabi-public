package com.javabi.common.collect.filter;

import java.util.ArrayList;
import java.util.List;

import com.javabi.common.collect.filter.element.EqualsFilter;

public class CollectionStrategyFilter<E> extends CollectionFilter<E> {

	private final FilterStrategy strategy;
	private final List<IElementFilter<E>> filterList = new ArrayList<IElementFilter<E>>();

	public CollectionStrategyFilter(FilterStrategy strategy) {
		this.strategy = strategy;
	}

	public CollectionStrategyFilter() {
		this(FilterStrategy.EXCLUDE);
	}

	public CollectionStrategyFilter<E> add(IElementFilter<E> filter) {
		if (filter == null) {
			throw new NullPointerException("filter");
		}
		this.filterList.add(filter);
		return this;
	}

	public CollectionStrategyFilter<E> filter(E element) {
		return add(new EqualsFilter<E>(element));
	}

	@Override
	public boolean removeElement(E element) {

		// Include strategy
		if (strategy.equals(FilterStrategy.EXCLUDE)) {
			for (IElementFilter<E> filter : filterList) {
				if (filter.removeElement(element)) {
					return true;
				}
			}
			return false;
		}

		// Exclude strategy
		else {
			for (IElementFilter<E> filter : filterList) {
				if (filter.removeElement(element)) {
					return false;
				}
			}
			return true;
		}
	}

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < 10; i++) {
			list.add(i);
		}
		list.add(null);
		System.out.println(list);
		list = new CollectionStrategyFilter<Integer>().filter(8).filter(null).filterCollection(list);
		System.out.println(list);
		list = new CollectionStrategyFilter<Integer>(FilterStrategy.INCLUDE).filter(2).filter(3).filterCollection(list);
		System.out.println(list);
	}

}
