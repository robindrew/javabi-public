package com.javabi.common.collect.dependencytree;

import static com.javabi.common.lang.Variables.notNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class DependencyTree<N> implements IDependencyTree<N> {

	private final Map<N, Set<N>> nodeToChildren = new HashMap<N, Set<N>>();
	private final Map<N, Set<N>> nodeToParents = new HashMap<N, Set<N>>();

	@Override
	public synchronized Collection<N> parents() {
		// Return a list of all nodes with no parents
		// A new list, not a view so remove can be called during iteration
		List<N> list = new ArrayList<N>();
		for (Entry<N, Set<N>> entry : nodeToParents.entrySet()) {
			Set<N> parents = entry.getValue();
			if (parents.isEmpty()) {
				list.add(entry.getKey());
			}
		}
		return list;
	}

	@Override
	public synchronized void addParent(N parent) {
		notNull("parent", parent);

		if (nodeToChildren.containsKey(parent)) {
			throw new IllegalStateException("parent already exists: " + parent);
		}
		nodeToChildren.put(parent, new LinkedHashSet<N>());
		nodeToParents.put(parent, new LinkedHashSet<N>());
	}

	@Override
	public synchronized void removeParent(N parent) {
		notNull("parent", parent);

		Set<N> parents = nodeToParents.get(parent);
		if (!parents.isEmpty()) {
			throw new IllegalStateException("node has parents: " + parent + " -> " + parents);
		}
		nodeToParents.remove(parent);

		// Remove this node from all its children
		Set<N> children = nodeToChildren.remove(parent);
		for (N child : children) {
			parents = nodeToParents.get(child);
			parents.remove(parent);
		}
	}

	@Override
	public synchronized void addChild(N parent, N child) {
		notNull("parent", parent);
		notNull("child", child);

		Set<N> children = nodeToChildren.get(parent);
		if (children == null) {
			throw new IllegalArgumentException("parent does not exist: " + parent);
		}
		if (children.contains(child)) {
			throw new IllegalArgumentException("parent already has child: " + parent + " -> " + child);
		}
		Set<N> parents = nodeToParents.get(child);
		if (parents != null && parents.isEmpty()) {
			throw new IllegalArgumentException("child already exists as parent: " + child);
		}
		if (parents == null) {
			parents = new LinkedHashSet<N>();
			nodeToParents.put(child, parents);
			nodeToChildren.put(child, new LinkedHashSet<N>());
		}
		children.add(child);
		parents.add(parent);
	}

	@Override
	public synchronized Collection<N> getParents(N node) {
		return new ArrayList<N>(nodeToParents.get(node));
	}

	@Override
	public synchronized Collection<N> getChildren(N node) {
		return new ArrayList<N>(nodeToChildren.get(node));
	}
}
