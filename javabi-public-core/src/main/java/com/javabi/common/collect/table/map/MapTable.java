package com.javabi.common.collect.table.map;

import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MapTable<K, V> implements IMapTable<K, V> {

	private final List<IMapRow<K, V>> rows = new ArrayList<IMapRow<K, V>>();

	@Override
	public int rows() {
		return rows.size();
	}

	@Override
	public IMapRow<K, V> row() {
		IMapRow<K, V> row = new MapRow<K, V>();
		rows.add(row);
		return row;
	}

	@Override
	public IMapRow<K, V> get(int index) {
		return rows.get(index);
	}

	@Override
	public V get(int rowIndex, K columnKey) {
		IMapRow<K, V> row = get(rowIndex);
		return row.get(columnKey);
	}

	@Override
	public List<IMapRow<K, V>> getRows() {
		return unmodifiableList(rows);
	}

	@Override
	public int size() {
		return rows();
	}

	@Override
	public boolean isEmpty() {
		return rows() == 0;
	}

	@Override
	public void clear() {
		rows.clear();
	}

	@Override
	public Iterator<IMapRow<K, V>> iterator() {
		return getRows().iterator();
	}

	@Override
	public Object[] toArray() {
		return getRows().toArray();
	}

	@Override
	public <T> T[] toArray(T[] array) {
		return getRows().toArray(array);
	}

	@Override
	public boolean contains(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean add(IMapRow<K, V> e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(Collection<? extends IMapRow<K, V>> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(int index, Collection<? extends IMapRow<K, V>> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IMapRow<K, V> set(int index, IMapRow<K, V> element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void add(int index, IMapRow<K, V> element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IMapRow<K, V> remove(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int indexOf(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int lastIndexOf(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<IMapRow<K, V>> listIterator() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<IMapRow<K, V>> listIterator(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<IMapRow<K, V>> subList(int fromIndex, int toIndex) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString() {
		return rows.toString();
	}

	public void sortRows(Comparator<IMapRow<K, V>> comparator) {
		Collections.sort(rows, comparator);
	}

}
