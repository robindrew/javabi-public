package com.javabi.common.collect.table.list;

import java.util.List;

public interface IListTable<V> extends List<IListRow<V>> {

	List<IListRow<V>> getRows();

	int rows();

	int columns();

	IListRow<V> row();

	IListRow<V> get(int rowIndex);

	V get(int rowIndex, int columnIndex);

}
