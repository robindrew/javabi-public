package com.javabi.common.collect.table.map;

import java.util.List;

public interface IMapTable<K, V> extends List<IMapRow<K, V>> {

	List<IMapRow<K, V>> getRows();

	int rows();

	IMapRow<K, V> row();

	@Override
	IMapRow<K, V> get(int rowIndex);

	V get(int rowIndex, K columnKey);

}
