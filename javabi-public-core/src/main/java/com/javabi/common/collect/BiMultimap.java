package com.javabi.common.collect;

import java.util.Collection;
import java.util.Set;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;

public class BiMultimap<K, V> {

	private final Multimap<K, V> keyMap;
	private final Multimap<V, K> valueMap;

	public static <K, V> BiMultimap<K, V> createBiMultimap() {
		Multimap<K, V> keyMap = HashMultimap.create();
		Multimap<V, K> valueMap = HashMultimap.create();
		return new BiMultimap<>(keyMap, valueMap);
	}

	protected BiMultimap(Multimap<K, V> keyMap, Multimap<V, K> valueMap) {
		if (keyMap == null) {
			throw new NullPointerException("keyMap");
		}
		if (valueMap == null) {
			throw new NullPointerException("valueMap");
		}
		this.keyMap = keyMap;
		this.valueMap = valueMap;
	}

	public int size() {
		return keyMap.size();
	}

	public boolean isEmpty() {
		return keyMap.isEmpty();
	}

	public Collection<V> get(K key) {
		return keyMap.get(key);
	}

	public boolean put(K key, V value) {
		boolean changed = keyMap.put(key, value);
		valueMap.put(value, key);
		return changed;
	}

	public Collection<V> removeAll(K key) {
		Collection<V> values = keyMap.removeAll(key);
		for (V value : values) {
			valueMap.remove(value, key);
		}
		return values;
	}

	public boolean containsKey(K key) {
		return keyMap.containsKey(key);
	}

	public boolean containsValue(V value) {
		return valueMap.containsKey(value);
	}

	public Set<K> keySet() {
		return ImmutableSet.copyOf(keyMap.keySet());
	}

	public Set<V> valueSet() {
		return ImmutableSet.copyOf(valueMap.keySet());
	}

}
