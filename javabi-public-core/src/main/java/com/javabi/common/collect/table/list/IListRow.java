package com.javabi.common.collect.table.list;

import java.util.List;

public interface IListRow<V> extends List<V> {

	int columns();

	void set(V value);

	void set(V value, int index);

	V get(int index);

}
