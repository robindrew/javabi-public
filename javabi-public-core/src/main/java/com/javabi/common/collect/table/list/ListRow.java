package com.javabi.common.collect.table.list;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.google.common.collect.Iterators;

public class ListRow<V> implements IListRow<V> {

	private final V[] columns;
	private int newIndex = 0;

	@SuppressWarnings("unchecked")
	public ListRow(int columns) {
		this.columns = (V[]) new Object[columns];
	}

	@Override
	public int columns() {
		return columns.length;
	}

	@Override
	public void set(V value) {
		set(value, newIndex);
		newIndex++;
	}

	@Override
	public void set(V value, int index) {
		columns[index] = value;
	}

	@Override
	public V get(int index) {
		return columns[index];
	}

	@Override
	public int size() {
		return columns();
	}

	@Override
	public boolean isEmpty() {
		return size() > 0;
	}

	@Override
	public boolean add(V value) {
		set(value);
		return true;
	}

	@Override
	public boolean contains(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Iterator<V> iterator() {
		return Iterators.forArray(columns);
	}

	@Override
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(Collection<? extends V> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(int index, Collection<? extends V> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException();
	}

	@Override
	public V set(int index, V element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void add(int index, V element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public V remove(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int indexOf(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int lastIndexOf(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<V> listIterator() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<V> listIterator(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<V> subList(int fromIndex, int toIndex) {
		throw new UnsupportedOperationException();
	}

}
