package com.javabi.common.collect.filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public abstract class CollectionFilter<E> implements ICollectionFilter<E>, IElementFilter<E> {

	@Override
	public <C extends Collection<E>> C filterCollection(C collection) {
		if (collection == null) {
			throw new NullPointerException("collection");
		}
		Iterator<E> iterator = collection.iterator();
		while (iterator.hasNext()) {
			E element = iterator.next();
			if (removeElement(element)) {
				iterator.remove();
			}
		}
		return collection;
	}

	@Override
	public <C extends Collection<E>> C filterCollection(C collection, List<E> toList, boolean drain) {
		if (collection == null) {
			throw new NullPointerException("collection");
		}
		if (toList == null) {
			throw new NullPointerException("list");
		}

		// Special case for ArrayList
		if (drain && collection instanceof ArrayList) {
			filterArrayList((List<E>) collection, toList, drain);
			return collection;
		}

		Iterator<E> iterator = collection.iterator();
		while (iterator.hasNext()) {
			E element = iterator.next();
			if (!removeElement(element)) {
				toList.add(element);
			}
			if (drain) {
				iterator.remove();
			}
		}
		return collection;
	}

	@Override
	public <C extends Collection<E>> C filterCollection(C collection, List<E> toList) {
		return filterCollection(collection, toList, false);
	}

	private List<E> filterArrayList(List<E> list, List<E> toList, boolean drain) {
		if (list == null) {
			throw new NullPointerException("list");
		}
		int size = list.size();
		for (int i = 0; i < size; i++) {
			E element = list.get(i);
			if (!removeElement(element)) {
				toList.add(element);
			}
			if (drain) {
				list.set(i, null);
			}
		}
		if (drain) {
			list.clear();
		}
		return toList;
	}

}
