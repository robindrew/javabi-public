package com.javabi.common.collect.table;

import java.util.List;

public interface IRow<C, V> {

	IColumnSet<C> getColumns();

	List<V> toList();

	V get(int index);

	V get(C column);

	int size();
	
}
