package com.javabi.common.collect.filter;

public enum FilterStrategy {

	EXCLUDE, INCLUDE;
}
