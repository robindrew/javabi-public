package com.javabi.http.parameter.parser;

import java.util.ArrayList;
import java.util.List;

import com.javabi.http.parameter.IHttpParser;
import com.javabi.http.request.IHttpRequest;

public abstract class HttpArrayParser<A> implements IHttpParser<A> {

	@Override
	public A parse(IHttpRequest request, String name, boolean optional) {

		List<String> valueList = new ArrayList<String>();
		for (int i = 0; i < Integer.MAX_VALUE; i++) {
			String key = name + i;
			String value = request.get(key);
			if (value == null) {
				break;
			}
			valueList.add(value);
		}
		int length = valueList.size();
		A array = newArray(length);

		// Empty?
		if (length == 0) {
			if (!optional) {
				throw new IllegalStateException("expected HTTP parameter '" + name + "0' not found");
			}
			return array;
		}

		// Parse
		for (int i = 0; i < length; i++) {
			String text = valueList.get(i);
			parseElement(text, array, i);
		}
		return array;
	}

	protected abstract A newArray(int length);

	protected abstract void parseElement(String text, A array, int index);
}
