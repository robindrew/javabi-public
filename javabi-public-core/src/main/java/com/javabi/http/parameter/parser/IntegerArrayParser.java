package com.javabi.http.parameter.parser;

public class IntegerArrayParser extends HttpArrayParser<int[]> {

	@Override
	protected int[] newArray(int length) {
		return new int[length];
	}

	@Override
	protected void parseElement(String text, int[] array, int index) {
		array[index] = Integer.parseInt(text);
	}

}
