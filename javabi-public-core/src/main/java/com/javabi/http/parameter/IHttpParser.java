package com.javabi.http.parameter;

import com.javabi.http.request.IHttpRequest;

public interface IHttpParser<P> {

	P parse(IHttpRequest request, String name, boolean optional);

}
