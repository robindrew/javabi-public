package com.javabi.http.parameter;

import java.util.List;
import java.util.Map;

import com.google.common.base.Throwables;
import com.google.common.collect.Maps;
import com.javabi.common.lang.reflect.field.FieldLister;
import com.javabi.common.lang.reflect.field.IField;
import com.javabi.http.page.IHttpPage;
import com.javabi.http.parameter.parser.HttpParserMap;
import com.javabi.http.request.IHttpRequest;

public class HttpParameterParser implements IHttpParameterParser {

	private final IHttpParserMap parserMap;
	private final Map<Class<?>, List<IField>> fieldMap = Maps.newHashMap();

	public HttpParameterParser(IHttpParserMap parserMap) {
		if (parserMap == null) {
			throw new NullPointerException("parserMap");
		}
		this.parserMap = parserMap;
	}

	public HttpParameterParser() {
		this.parserMap = new HttpParserMap();
	}

	public void parse(Object object, IHttpRequest request) {
		Class<?> type = object.getClass();
		List<IField> fields = getFields(type);
		for (IField field : fields) {
			parseField(object, field, request);
		}
	}

	private List<IField> getFields(Class<?> type) {
		List<IField> list = fieldMap.get(type);
		if (list != null) {
			return list;
		}
		FieldLister lister = new FieldLister();
		lister.includeAnnotation(HttpParameter.class);
		list = lister.getFieldList(type);
		fieldMap.put(type, list);
		return list;
	}

	private void parseField(Object object, IField field, IHttpRequest request) {
		HttpParameter annotation = field.getAnnotation(HttpParameter.class);

		// Name
		String name = annotation.name();
		if (name.isEmpty()) {
			name = field.getName();
		}

		// Parse
		Object parsedValue;
		try {
			parsedValue = parserMap.parse(field.getType(), request, name, annotation.optional(), annotation.collection(), annotation.element());
		} catch (Exception e) {
			throw new IllegalStateException("failed to parse field: " + field, e);
		}
		if (parsedValue == null) {
			return;
		}

		// Set
		try {
			field.set(object, parsedValue);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	public void parse(Object object, IHttpPage page) {
		parse(object, page.getRequest());
	}
}
