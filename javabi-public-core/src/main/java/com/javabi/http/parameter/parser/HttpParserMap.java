package com.javabi.http.parameter.parser;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.google.common.base.Throwables;
import com.google.common.collect.Maps;
import com.javabi.common.text.parser.IStringParser;
import com.javabi.common.text.parser.IStringParserMap;
import com.javabi.common.text.parser.StringParserMap;
import com.javabi.http.parameter.IHttpParser;
import com.javabi.http.parameter.IHttpParserMap;
import com.javabi.http.request.IHttpRequest;

public class HttpParserMap implements IHttpParserMap {

	private final Map<Class<?>, IHttpParser<?>> parserMap = Maps.newHashMap();
	private final IStringParserMap map = new StringParserMap();

	public HttpParserMap() {
		setParser(String[].class, new StringArrayParser());
		setParser(int[].class, new IntegerArrayParser());
		setParser(long[].class, new LongArrayParser());
	}

	@Override
	public <T> void setParser(Class<T> type, IHttpParser<T> parser) {
		if (type == null) {
			throw new NullPointerException("type");
		}
		if (parser == null) {
			throw new NullPointerException("parser");
		}
		parserMap.put(type, parser);
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T, E> T parse(Class<?> type, IHttpRequest request, String name, boolean optional, Class<? extends Collection> collectionType, Class<E> elementType) {

		// Advanced parsing
		IHttpParser<?> httpParser = parserMap.get(type);
		if (httpParser != null) {
			return (T) httpParser.parse(request, name, optional);
		}

		// Indexed?
		if (elementType.equals(Object.class)) {
			String value = request.get(name);
			if (value == null) {
				if (!optional) {
					throw new IllegalStateException("expected HTTP parameter '" + name + "' not found");
				}
				return null;
			}
			IStringParser<?> stringParser = map.getParser(type);
			return (T) stringParser.parse(value);
		}

		try {
			IStringParser<?> stringParser = map.getParser(elementType);
			Collection<E> list = collectionType.newInstance();
			List<String> values = request.getAll(name);
			for (String value : values) {
				E element = (E) stringParser.parse(value);
				list.add(element);
			}

			// Optional?
			if (list.isEmpty()) {
				if (!optional) {
					throw new IllegalStateException("expected HTTP parameter '" + name + "' (indexed) not found");
				}
				return null;
			}

			return (T) list;
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

}
