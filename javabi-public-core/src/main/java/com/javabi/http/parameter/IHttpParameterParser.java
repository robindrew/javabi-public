package com.javabi.http.parameter;

import com.javabi.http.page.IHttpPage;
import com.javabi.http.request.IHttpRequest;

public interface IHttpParameterParser {

	void parse(Object object, IHttpPage page);

	void parse(Object object, IHttpRequest request);

}
