package com.javabi.http.parameter.parser;

public class StringArrayParser extends HttpArrayParser<String[]> {

	@Override
	protected String[] newArray(int length) {
		return new String[length];
	}

	@Override
	protected void parseElement(String text, String[] array, int index) {
		array[index] = text;
	}

}
