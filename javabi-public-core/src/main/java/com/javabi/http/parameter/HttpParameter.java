package com.javabi.http.parameter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Collection;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface HttpParameter {

	String name() default "";

	boolean optional() default false;

	boolean hidden() default false;

	@SuppressWarnings("rawtypes")
	Class<? extends Collection> collection() default ArrayList.class;

	Class<?> element() default Object.class;

}
