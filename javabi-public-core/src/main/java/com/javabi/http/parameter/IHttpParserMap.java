package com.javabi.http.parameter;

import java.util.Collection;

import com.javabi.http.request.IHttpRequest;

public interface IHttpParserMap {

	<T> void setParser(Class<T> type, IHttpParser<T> parser);

	@SuppressWarnings("rawtypes")
	<T, E> T parse(Class<?> type, IHttpRequest request, String name, boolean optional, Class<? extends Collection> collection, Class<E> element);

}
