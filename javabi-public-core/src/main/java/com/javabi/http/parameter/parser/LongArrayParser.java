package com.javabi.http.parameter.parser;

public class LongArrayParser extends HttpArrayParser<long[]> {

	@Override
	protected long[] newArray(int length) {
		return new long[length];
	}

	@Override
	protected void parseElement(String text, long[] array, int index) {
		array[index] = Long.parseLong(text);
	}

}
