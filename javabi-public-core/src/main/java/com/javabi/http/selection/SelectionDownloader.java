package com.javabi.http.selection;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Charsets;
import com.javabi.common.io.file.Files;
import com.javabi.common.text.selection.ISelection;
import com.javabi.common.text.selection.Selection;
import com.javabi.http.content.ByteArrayContent;
import com.javabi.http.content.IContent;
import com.javabi.http.downloader.HttpUrlConnectionDownloader;
import com.javabi.http.downloader.IHttpDownloader;
import com.javabi.http.message.ContentType;
import com.javabi.http.request.HttpRequest;
import com.javabi.http.request.IHttpRequest;
import com.javabi.http.request.Method;
import com.javabi.http.response.IHttpResponse;

public class SelectionDownloader {

	private static final Logger log = LoggerFactory.getLogger(SelectionDownloader.class);

	private int retries = 3;
	private Charset charset = Charsets.UTF_8;
	private String filename = null;
	private List<String> cookies = new ArrayList<String>();
	private Method method = Method.GET;
	private String content = null;
	private ContentType type = null;

	public void setMethod(Method method) {
		if (method == null) {
			throw new NullPointerException("method");
		}
		this.method = method;
	}

	public void setContent(String content, ContentType type) {
		this.content = content;
		this.type = type;
	}

	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public void setCookies(List<String> cookies) {
		this.cookies.addAll(cookies);
	}

	public void setCookies(String... cookies) {
		setCookies(Arrays.asList(cookies));
	}

	public ISelection download(String url) {

		IContent content = null;
		int attempt = 1;
		while (content == null) {
			content = getContent(url, attempt++);
		}

		byte[] data = content.toByteArray();
		String text = new String(data, charset);

		if (filename != null) {
			Files.writeFromBytes(new File(filename), data);
		}

		return new Selection(text);
	}

	private IContent getContent(String url, int attempt) {
		try {

			IHttpRequest request = new HttpRequest();
			request.setMethod(method);
			request.setUri(url);
			if (type != null) {
				request.setContentType(type);
			}
			for (String cookie : cookies) {
				request.setCookie(cookie);
			}
			if (content != null) {
				request.setContent(new ByteArrayContent(content.getBytes()));
				request.setContentLength(request.getContent().length());
			}
			IHttpDownloader downloader = new HttpUrlConnectionDownloader();
			downloader.setConnectTimeout(5, SECONDS);
			downloader.setReadTimeout(60, SECONDS);
			downloader.setRequest(request);
			downloader.download();
			IHttpResponse response = downloader.getResponse();
			return response.getContent();

		} catch (Exception e) {
			if (attempt < retries) {
				log.warn("Download failed for url: " + url + ", attempt #" + attempt);
			} else {
				log.error("Download failed for url: " + url + ", attempt #" + attempt, e);

				IContent content = new ByteArrayContent();
				content.write(new byte[0]);
				return content;
			}
			return null;
		}
	}

}
