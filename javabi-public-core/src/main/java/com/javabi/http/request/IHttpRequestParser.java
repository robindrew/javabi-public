package com.javabi.http.request;

public interface IHttpRequestParser {

	boolean exists(String key);

	String get(String key);

	byte getByte(String key);

	int getInt(String key);

	boolean getBoolean(String key);

	<E extends Enum<E>> E getEnum(Class<E> type, String key);

	int[] getIntArray(String key);

}
