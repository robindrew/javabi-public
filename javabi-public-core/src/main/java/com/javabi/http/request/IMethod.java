package com.javabi.http.request;

public interface IMethod {

	String get();
}
