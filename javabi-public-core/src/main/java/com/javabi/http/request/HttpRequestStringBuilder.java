package com.javabi.http.request;

import static com.javabi.common.lang.Variables.notEmpty;
import static com.javabi.common.lang.Variables.notNull;
import static com.javabi.common.lang.Variables.notStartsWith;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.base.Throwables;
import com.javabi.http.message.Version;

public class HttpRequestStringBuilder {

	private static final String DEFAULT_NEW_LINE = "\n";

	private final StringBuilder request = new StringBuilder();
	private final String newLine;
	private boolean query = false;

	public HttpRequestStringBuilder(String newLine) {
		this.newLine = notEmpty("newLine", newLine);
	}

	public HttpRequestStringBuilder() {
		this(DEFAULT_NEW_LINE);
	}

	public HttpRequestStringBuilder method(Method method) {
		notNull("method", method);

		request.append(method);
		return this;
	}

	public HttpRequestStringBuilder get() {
		return method(Method.GET);
	}

	public HttpRequestStringBuilder post() {
		return method(Method.POST);
	}

	public HttpRequestStringBuilder url(String url) {
		notStartsWith("url", url, "/");

		request.append(' ');
		request.append(url);
		return this;
	}

	public HttpRequestStringBuilder url(String url, String query) {
		notStartsWith("url", url, "/");
		notNull("query", query);

		request.append(' ');
		request.append(url);
		if (!query.isEmpty()) {
			request.append('?');
			request.append(query);
			this.query = true;
		}
		return this;
	}

	public HttpRequestStringBuilder query(Map<String, String> nameToValueMap) {
		notEmpty("nameToValueMap", nameToValueMap);

		for (Entry<String, String> entry : nameToValueMap.entrySet()) {
			query(entry.getKey(), entry.getValue());
		}
		return this;

	}

	public HttpRequestStringBuilder query(String name, String value) {
		notEmpty("name", name);
		notNull("value", value);

		if (!query) {
			query = true;
			request.append('?');
		} else {
			request.append('&');
		}

		request.append(urlEncode(name));
		request.append('=');
		request.append(urlEncode(value));
		return this;
	}

	protected String urlEncode(String value) {
		try {
			return URLEncoder.encode(value, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw Throwables.propagate(e);
		}
	}

	public HttpRequestStringBuilder version(Version version) {
		notNull("version", version);

		request.append(' ');
		request.append(version.get());
		request.append(newLine);
		return this;
	}

	public HttpRequestStringBuilder version11() {
		return version(Version.HTTP_11);
	}

	public HttpRequestStringBuilder version10() {
		return version(Version.HTTP_10);
	}

	public HttpRequestStringBuilder header(String name, String value) {
		notEmpty("name", name);
		notEmpty("value", value);

		request.append(name);
		request.append(": ");
		request.append(value);
		request.append(newLine);
		return this;
	}

	public HttpRequestStringBuilder host(String header) {
		return header("Host", header);
	}

	public HttpRequestStringBuilder userAgent(String header) {
		return header("User-Agent", header);
	}

	public HttpRequestStringBuilder referer(String header) {
		return header("Referer", header);
	}

	public HttpRequestStringBuilder connection(String header) {
		return header("Connection", header);
	}

	public HttpRequestStringBuilder connectionKeepAlive() {
		return connection("Keep-Alive");
	}

	public HttpRequestStringBuilder connectionClose() {
		return connection("Close");
	}

	public HttpRequestStringBuilder accept(String header) {
		return header("Accept", header);
	}

	public HttpRequestStringBuilder acceptLanguage(String header) {
		return header("Accept-Language", header);
	}

	public HttpRequestStringBuilder acceptEncoding(String header) {
		return header("Accept-Encoding", header);
	}

	public HttpRequestStringBuilder acceptCharset(String header) {
		return header("Accept-Charset", header);
	}

	public HttpRequestStringBuilder content(String content) {
		notNull("content", content);

		request.append(newLine);
		request.append(content);
		return this;
	}

	public HttpRequestStringBuilder noContent() {
		request.append(newLine);
		return this;
	}

	@Override
	public String toString() {
		return request.toString();
	}

}
