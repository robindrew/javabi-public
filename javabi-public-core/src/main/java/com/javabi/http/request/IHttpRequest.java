package com.javabi.http.request;

import java.util.List;

import com.google.common.collect.ListMultimap;
import com.javabi.http.cookie.ICookieMap;
import com.javabi.http.message.IHttpMessage;

public interface IHttpRequest extends IHttpMessage {

	IMethod getMethod();

	String getUri();

	String getRemoteHostAddress();

	int getRemotePort();

	void setMethod(IMethod method);

	void setUri(String uri);

	String get(String key);

	ListMultimap<String, String> getAll();

	List<String> getAll(String key);

	ICookieMap getCookies();

	boolean contains(String key);

	boolean hasQuery();

	String getQuery();

	void setHost(String host);

	String getHost();

	void setContentLength(long length);

}
