package com.javabi.http.request;

public interface IHttpRequestBeanParser<B> {

	B parse(IHttpRequest request);
}
