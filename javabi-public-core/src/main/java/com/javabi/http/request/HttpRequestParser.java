package com.javabi.http.request;

public class HttpRequestParser implements IHttpRequestParser {

	private final IHttpRequest request;

	public HttpRequestParser(IHttpRequest request) {
		if (request == null) {
			throw new NullPointerException("request");
		}
		this.request = request;
	}

	@Override
	public boolean exists(String key) {
		return request.get(key) != null;
	}

	@Override
	public String get(String key) {
		String value = request.get(key);
		if (value == null) {
			throw new IllegalStateException("Mandatory HTTP parameter missing: " + key);
		}
		return value;
	}

	@Override
	public byte getByte(String key) {
		String text = get(key);
		return Byte.parseByte(text);
	}

	@Override
	public int getInt(String key) {
		String text = get(key);
		return Integer.parseInt(text);
	}

	@Override
	public boolean getBoolean(String key) {
		String text = get(key);
		return Boolean.parseBoolean(text);
	}

	@Override
	public int[] getIntArray(String key) {
		String text = get(key);
		if (text.isEmpty()) {
			return new int[0];
		}
		String[] parts = text.split(",");
		int[] array = new int[parts.length];
		for (int i = 0; i < parts.length; i++) {
			array[i] = Integer.parseInt(parts[i]);
		}
		return array;
	}

	@Override
	public <E extends Enum<E>> E getEnum(Class<E> type, String key) {
		String text = get(key);
		return Enum.valueOf(type, text);
	}

}
