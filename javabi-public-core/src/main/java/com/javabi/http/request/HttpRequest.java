package com.javabi.http.request;

import static com.javabi.common.lang.Variables.notEmpty;
import static com.javabi.common.lang.Variables.notNull;
import static java.util.Collections.unmodifiableList;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.javabi.http.cookie.CookieMapParser;
import com.javabi.http.cookie.ICookieMap;
import com.javabi.http.header.Header;
import com.javabi.http.message.HttpMessage;

public class HttpRequest extends HttpMessage implements IHttpRequest {

	private IMethod method = Method.GET;
	private String uri = "/";
	private ListMultimap<String, String> map = ArrayListMultimap.create();

	public HttpRequest() {
	}

	public HttpRequest(Map<String, String> map) {
		if (map == null) {
			throw new NullPointerException("map");
		}
		this.map = ArrayListMultimap.create(map.size(), 1);
		for (Entry<String, String> entry : map.entrySet()) {
			this.map.put(entry.getKey(), entry.getValue());
		}
	}

	public HttpRequest(Multimap<String, String> map) {
		if (map == null) {
			throw new NullPointerException("map");
		}
		this.map = ArrayListMultimap.create(map);
	}

	@Override
	public List<String> getAll(String key) {
		return unmodifiableList(map.get(key));
	}

	@Override
	public IMethod getMethod() {
		return method;
	}

	@Override
	public String getUri() {
		return uri;
	}

	@Override
	public String getQuery() {
		return null;
	}

	@Override
	public String get(String key) {
		List<String> values = map.get(key);
		if (values.isEmpty()) {
			return null;
		}
		return values.get(0);
	}

	@Override
	public ListMultimap<String, String> getAll() {
		return map;
	}

	@Override
	public void setMethod(IMethod method) {
		this.method = notNull("method", method);
	}

	@Override
	public void setUri(String uri) {
		this.uri = notEmpty("uri", uri);
	}

	@Override
	public void setHost(String host) {
		notEmpty("host", host);
		setHeader(Header.HOST, host);
	}

	@Override
	public String getHost() {
		return getHeader(Header.HOST);
	}

	@Override
	public String toString() {
		StringBuilder request = new StringBuilder();
		request.append(getMethod()).append(' ');
		request.append(getUri()).append(' ');
		request.append(getVersion().get());
		return request.toString();
	}

	@Override
	public String toString(boolean includeHeaders) {
		StringBuilder request = new StringBuilder();
		request.append(getMethod()).append(' ');
		request.append(getUri()).append(' ');
		request.append(getVersion().get()).append(NEW_LINE);
		for (Entry<String, String> header : getHeaders().entries()) {
			request.append(header.getKey()).append(": ");
			request.append(header.getValue()).append(NEW_LINE);
		}
		request.append(NEW_LINE);
		return request.toString();
	}

	@Override
	public boolean contains(String key) {
		return get(key) != null;
	}

	@Override
	public String getRemoteHostAddress() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ICookieMap getCookies() {
		String cookie = getHeader(Header.COOKIE);
		return new CookieMapParser().parseCookieMap(cookie);
	}

	@Override
	public void setCookie(String cookie) {
		addHeader(Header.COOKIE, cookie);
	}

	@Override
	public void setContentLength(long length) {
		setHeader(Header.CONTENT_LENGTH, String.valueOf(length));
	}

	@Override
	public boolean hasQuery() {
		return false;
	}

	@Override
	public int getRemotePort() {
		throw new UnsupportedOperationException();
	}

}
