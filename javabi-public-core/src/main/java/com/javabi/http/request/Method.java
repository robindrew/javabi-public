package com.javabi.http.request;

public enum Method implements IMethod {

	/** The HTTP OPTIONS method. */
	OPTIONS,
	/** The HTTP GET method. */
	GET,
	/** The HTTP HEAD method. */
	HEAD,
	/** The HTTP POST method. */
	POST,
	/** The HTTP PUT method. */
	PUT,
	/** The HTTP DELETE method. */
	DELETE,
	/** The HTTP TRACE method. */
	TRACE,
	/** The HTTP CONNECT method. */
	CONNECT;

	@Override
	public String get() {
		return name();
	}

}
