package com.javabi.http;

import static com.javabi.http.header.Header.CONTENT_ENCODING;
import static com.javabi.http.header.Header.CONTENT_LENGTH;
import static com.javabi.http.header.Header.CONTENT_TYPE;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.zip.GZIPInputStream;

import com.google.common.base.Charsets;
import com.google.common.base.Throwables;
import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import com.javabi.http.header.Header;
import com.javabi.http.message.ContentType;
import com.javabi.http.message.IHttpMessage;
import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.response.StatusCode;

public class HttpUtil {

	public static final String NEW_LINE = "\r\n";
	public static final Charset CHARSET = Charsets.ISO_8859_1;
	public static final int PORT = 80;

	public static Charset getCharset(IHttpMessage message, Charset defaultValue) {
		String type = message.getHeader(CONTENT_TYPE);
		if (type == null || type.isEmpty()) {
			return defaultValue;
		}
		int beginIndex = type.toLowerCase().indexOf("charset=");
		if (beginIndex == -1) {
			return defaultValue;
		}
		beginIndex += "charset=".length();
		int endIndex = type.indexOf(";", beginIndex);
		if (endIndex == -1) {
			endIndex = type.length();
		}
		return Charset.forName(type.substring(beginIndex, endIndex));
	}

	public static long getContentLength(IHttpMessage message) {
		String length = message.getHeader(CONTENT_LENGTH);
		if (length == null) {
			String type = message.getHeader(CONTENT_TYPE);
			return type == null ? 0 : -1;
		}
		length = length.trim();
		if (length.isEmpty()) {
			return -1;
		}
		try {
			return Long.parseLong(length);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	public static String getTextContent(IHttpMessage message) {
		if (!isTextContent(message)) {
			throw new IllegalArgumentException("Message does not contain text: " + message);
		}

		Charset charset = getCharset(message, CHARSET);
		InputStream content = message.getContent().toInputStream();
		StringWriter writer = new StringWriter();
		try {

			// Compressed?
			if (isGzipContent(message)) {
				content = new GZIPInputStream(content);
			}

			// Read!
			BufferedReader reader = new BufferedReader(new InputStreamReader(content, charset));
			CharStreams.copy(reader, writer);

		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
		return writer.toString();
	}

	public static byte[] getContent(IHttpMessage message) {
		InputStream content = message.getContent().toInputStream();
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {

			// Compressed?
			if (isGzipContent(message)) {
				content = new GZIPInputStream(content);
			}

			// Read!
			content = new BufferedInputStream(content);
			ByteStreams.copy(content, output);

		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
		return output.toByteArray();
	}

	private static boolean isGzipContent(IHttpMessage message) {
		String encoding = message.getHeader(CONTENT_ENCODING);
		return encoding != null && encoding.toLowerCase().equals("gzip");
	}

	private static boolean isTextContent(IHttpMessage message) {
		String type = message.getHeader(CONTENT_TYPE);
		return type != null && type.toLowerCase().startsWith("text/");
	}

	public static String getHost(IHttpRequest request) {
		String host = request.getHeader(Header.HOST);
		if (host == null) {
			host = getHostFromUri(request.getUri());
		}
		return host;
	}

	private static String getHostFromUri(String uri) {
		if (uri.startsWith("/")) {
			return null;
		}
		int begin = uri.indexOf("://");
		if (begin == -1) {
			return null;
		}
		String host = uri.substring(begin + 3);
		int end = host.indexOf("/");
		if (end != -1) {
			host = host.substring(0, end);
		}
		int colon = host.indexOf(":");
		if (colon != -1) {
			host = host.substring(0, colon);
		}
		return host;
	}

	public static int getPort(IHttpRequest request) {
		String uri = request.getUri();
		if (uri.startsWith("/")) {
			return PORT;
		}
		int begin = uri.indexOf("://");
		if (begin == -1) {
			return PORT;
		}
		int end = uri.indexOf("/");
		if (end != -1) {
			uri = uri.substring(0, end);
		}
		int colon = uri.indexOf(":");
		if (colon == -1) {
			return PORT;
		}
		uri = uri.substring(colon + 1);
		return Integer.parseInt(uri);
	}

	public static void setContent(IHttpResponse response, ContentType type, byte[] content) {
		setContent(response, type.getType(), content);
	}

	public static void setContent(IHttpResponse response, ContentType type, String text, Charset charset) {
		byte[] content = text.getBytes(charset);
		setContent(response, type.getType() + "; charset=" + charset.name(), content);
	}

	public static void setContent(IHttpResponse response, ContentType type, String text) {
		setContent(response, type, text, Charsets.UTF_8);
	}

	public static void setContent(IHttpResponse response, String type, byte[] content) {
		response.setHeader(Header.CONTENT_TYPE, type);
		response.setHeader(Header.CONTENT_LENGTH, String.valueOf(content.length));
		response.getContent().write(content);
	}

	public static void setText(IHttpResponse response, CharSequence text) {
		setText(response, text, Charsets.UTF_8);
	}

	public static void setText(IHttpResponse response, CharSequence text, Charset charset) {
		byte[] content = text.toString().getBytes(charset);
		setContent(response, "text/plain; charset=" + charset, content);
	}

	public static void setHtml(IHttpResponse response, CharSequence html) {
		setHtml(response, html, Charsets.UTF_8);
	}

	public static void setHtml(IHttpResponse response, CharSequence html, Charset charset) {
		String text = html.toString();
		byte[] content = text.getBytes(charset);
		setContent(response, "text/html; charset=" + charset, content);
	}

	public static void setJson(IHttpResponse response, String json) {
		setJson(response, json, Charsets.UTF_8);
	}

	public static void setJson(IHttpResponse response, CharSequence json) {
		setJson(response, json, Charsets.UTF_8);
	}

	public static void setJson(IHttpResponse response, CharSequence json, Charset charset) {
		byte[] content = json.toString().getBytes(charset);
		setContent(response, "text/x-json; charset=" + charset, content);
	}

	public static void setJson(IHttpResponse response, String json, Charset charset) {
		byte[] content = json.toString().getBytes(charset);
		setContent(response, "text/x-json; charset=" + charset, content);
	}

	public static void setXml(IHttpResponse response, String xml) {
		setXml(response, xml, Charsets.UTF_8);
	}

	public static void setXml(IHttpResponse response, CharSequence xml) {
		setXml(response, xml, Charsets.UTF_8);
	}

	public static void setXml(IHttpResponse response, CharSequence xml, Charset charset) {
		byte[] content = xml.toString().getBytes(charset);
		setContent(response, "text/xml; charset=" + charset, content);
	}

	public static void setXml(IHttpResponse response, String xml, Charset charset) {
		byte[] content = xml.toString().getBytes(charset);
		setContent(response, "text/xml; charset=" + charset, content);
	}

	public static void setJpeg(IHttpResponse response, byte[] content) {
		setContent(response, "image/jpeg", content);
	}

	public static void setPng(IHttpResponse response, byte[] content) {
		setContent(response, "image/png", content);
	}

	public static void setRedirect(IHttpResponse response, String path) {
		response.setStatus(StatusCode.FOUND);
		response.addHeader(Header.LOCATION, path);
	}

	public static void setNotFound(IHttpResponse response, String path) {
		response.setStatus(StatusCode.NOT_FOUND);
		setHtml(response, "<h1>Error 404</h1><code>Not Found: \"" + path + "\"</code>");
	}

	private HttpUtil() {
	}

}
