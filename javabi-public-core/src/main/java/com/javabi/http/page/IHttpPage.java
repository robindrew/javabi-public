package com.javabi.http.page;

import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.session.IHttpSession;

public interface IHttpPage {

	IHttpRequest getRequest();

	IHttpResponse getResponse();

	IHttpSession getSession();

	void setRequest(IHttpRequest request);

	void setResponse(IHttpResponse response);

	void setSession(IHttpSession session);

}
