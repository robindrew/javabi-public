package com.javabi.http.page;

public interface IHttpPageHandler {

	void handlePage(IHttpPage page);

}
