package com.javabi.http.page;

import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.session.IHttpSession;

public class HttpPage implements IHttpPage {

	private IHttpRequest request;
	private IHttpResponse response;
	private IHttpSession session;

	public HttpPage() {
	}

	public HttpPage(IHttpRequest request, IHttpResponse response, IHttpSession session) {
		setRequest(request);
		setResponse(response);
		setSession(session);
	}

	@Override
	public IHttpRequest getRequest() {
		return request;
	}

	@Override
	public void setRequest(IHttpRequest request) {
		if (request == null) {
			throw new NullPointerException("request");
		}
		this.request = request;
	}

	@Override
	public IHttpResponse getResponse() {
		return response;
	}

	@Override
	public void setResponse(IHttpResponse response) {
		if (response == null) {
			throw new NullPointerException("response");
		}
		this.response = response;
	}

	@Override
	public IHttpSession getSession() {
		return session;
	}

	@Override
	public void setSession(IHttpSession session) {
		if (session == null) {
			throw new NullPointerException("session");
		}
		this.session = session;
	}

}
