package com.javabi.http.content;

import static com.javabi.common.lang.Variables.notNull;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.google.common.base.Throwables;
import com.google.common.io.Files;
import com.javabi.common.io.pipe.ByteBufferPipe;
import com.javabi.common.lang.Quietly;

public class FileContent implements IContent {

	private final File file;

	public FileContent(File file) {
		this.file = notNull("file", file);
	}

	public FileContent() {
		try {
			this.file = File.createTempFile("FileContent", null);
		} catch (IOException e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public void write(InputStream input, long length) {
		OutputStream output = null;
		try {
			int bufferLength = length > 8192 ? 8192 : (int) length;
			output = new BufferedOutputStream(new FileOutputStream(file));
			new ByteBufferPipe(bufferLength).pipe(input, output, length);
		} catch (IOException e) {
			Throwables.propagate(e);
		} finally {
			Quietly.close(output);
		}
	}

	@Override
	public void write(byte[] bytes) {
		try {
			Files.write(bytes, file);
		} catch (IOException e) {
			Throwables.propagate(e);
		}
	}

	@Override
	public boolean isEmpty() {
		return !file.exists() || file.length() == 0;
	}

	@Override
	public long length() {
		if (!file.exists()) {
			return -1;
		}
		return file.length();
	}

	@Override
	public byte[] toByteArray() {
		try {
			return Files.toByteArray(file);
		} catch (IOException e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public InputStream toInputStream() {
		try {
			return new BufferedInputStream(new FileInputStream(file));
		} catch (IOException e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public OutputStream toOutputStream() {
		try {
			return new BufferedOutputStream(new FileOutputStream(file));
		} catch (IOException e) {
			throw Throwables.propagate(e);
		}
	}

}
