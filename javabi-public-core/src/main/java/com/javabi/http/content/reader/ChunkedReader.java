package com.javabi.http.content.reader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ChunkedReader implements IContentReader {

	private static final char[] HEX = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

	@Override
	public void read(InputStream input, OutputStream output) throws IOException {
		while (true) {

			// Read data length
			int length = readLength(input);
			if (length == 0) {
				break;
			}

			// Read data
			readData(input, output, length);
		}
	}

	private void readData(InputStream input, OutputStream output, int length) throws IOException {
		byte[] buffer = new byte[length > 8196 ? 8196 : length];
		while (length > 0) {
			int read = input.read(buffer, 0, buffer.length);
			if (read == -1) {
				throw new IOException("end of stream");
			}
			if (read > 0) {
				length -= read;
				output.write(buffer, 0, read);
			}
		}
		readChar(input, '\r');
		readChar(input, '\n');
	}

	private int readLength(InputStream input) throws IOException {
		int length = 0;
		while (true) {
			int read = input.read();

			// Finished?
			if (read == '\r') {
				readChar(input, '\n');
				break;
			}

			// Length
			length *= 16;
			length += toHex(read);
		}
		return length;
	}

	private void readChar(InputStream input, int expected) throws IOException {
		int read = input.read();
		if (read != expected) {
			throw new IOException("expected " + expected + ", read " + read);
		}
	}

	private int toHex(int digit) throws IOException {
		for (int i = 0; i < HEX.length; i++) {
			if (HEX[i] == digit) {
				return HEX[i];
			}
		}
		throw new IOException("unexpected hex digit=" + digit);
	}
}
