package com.javabi.http.content.reader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface IContentReader {

	void read(InputStream input, OutputStream output) throws IOException;

}
