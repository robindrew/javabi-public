package com.javabi.http.content;

import static com.javabi.common.lang.Variables.notNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.google.common.base.Throwables;

public class OutputStreamContent implements IContent {

	private final OutputStream output;

	public OutputStreamContent(OutputStream output) {
		this.output = notNull("output", output);
	}

	@Override
	public void write(InputStream input, long length) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void write(byte[] bytes) {
		try {
			output.write(bytes);
		} catch (IOException e) {
			Throwables.propagate(e);
		}
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public long length() {
		throw new UnsupportedOperationException();
	}

	@Override
	public byte[] toByteArray() {
		throw new UnsupportedOperationException();
	}

	@Override
	public InputStream toInputStream() {
		throw new UnsupportedOperationException();
	}

	@Override
	public OutputStream toOutputStream() {
		return output;
	}

}
