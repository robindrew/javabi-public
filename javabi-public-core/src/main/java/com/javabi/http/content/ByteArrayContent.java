package com.javabi.http.content;

import static com.javabi.common.lang.Variables.max;
import static com.javabi.common.lang.Variables.notNull;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import org.apache.commons.lang3.ArrayUtils;

import com.google.common.base.Charsets;
import com.javabi.common.io.IOUtil;

public class ByteArrayContent implements IContent {

	private byte[] content = null;

	public ByteArrayContent() {
	}

	public ByteArrayContent(byte[] content) {
		this.content = content;
	}

	public ByteArrayContent(String content, Charset charset) {
		this.content = content.getBytes(charset);
	}

	public ByteArrayContent(String content) {
		this(content, Charsets.UTF_8);
	}

	@Override
	public void write(byte[] content) {
		this.content = notNull("content", content);
	}

	@Override
	public void write(InputStream input, long length) {
		max("length", length, Integer.MAX_VALUE);

		// Empty
		if (length == 0) {
			content = ArrayUtils.EMPTY_BYTE_ARRAY;
			return;
		}

		// Known length?
		if (length > 0) {
			byte[] buffer = new byte[(int) length];
			content = IOUtil.readFully(input, buffer, false);
		} else {
			content = IOUtil.readFully(input);
		}
	}

	@Override
	public long length() {
		return content == null ? 0 : content.length;
	}

	@Override
	public String toString() {
		return toString(Charsets.UTF_8);
	}

	public String toString(Charset charset) {
		if (content == null) {
			return "not populated";
		}
		return new String(content, charset);
	}

	@Override
	public byte[] toByteArray() {
		if (content == null) {
			throw new IllegalStateException("not populated");
		}
		return content;
	}

	@Override
	public InputStream toInputStream() {
		return new ByteArrayInputStream(content);
	}

	@Override
	public boolean isEmpty() {
		return content == null || content.length == 0;
	}

	@Override
	public OutputStream toOutputStream() {
		throw new UnsupportedOperationException();
	}

}
