package com.javabi.http.content;

import java.io.InputStream;
import java.io.OutputStream;

public interface IContent {

	void write(InputStream input, long length);

	void write(byte[] bytes);

	long length();

	boolean isEmpty();

	byte[] toByteArray();

	InputStream toInputStream();

	OutputStream toOutputStream();

}
