package com.javabi.http.content;

import java.io.InputStream;
import java.io.OutputStream;

public class InputStreamContent implements IContent {

	private final InputStream stream;

	public InputStreamContent(InputStream stream) {
		if (stream == null) {
			throw new NullPointerException("stream");
		}
		this.stream = stream;
	}

	@Override
	public void write(InputStream input, long length) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void write(byte[] bytes) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public long length() {
		throw new UnsupportedOperationException();
	}

	@Override
	public byte[] toByteArray() {
		throw new UnsupportedOperationException();
	}

	@Override
	public InputStream toInputStream() {
		return stream;
	}

	@Override
	public OutputStream toOutputStream() {
		throw new UnsupportedOperationException();
	}

}
