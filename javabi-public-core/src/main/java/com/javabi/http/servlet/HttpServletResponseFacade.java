package com.javabi.http.servlet;

import static com.javabi.common.lang.Variables.notNull;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.google.common.base.Throwables;
import com.google.common.collect.ListMultimap;
import com.javabi.http.content.IContent;
import com.javabi.http.content.OutputStreamContent;
import com.javabi.http.header.Header;
import com.javabi.http.header.IHeader;
import com.javabi.http.message.ContentType;
import com.javabi.http.message.IVersion;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.response.IStatusCode;
import com.javabi.http.response.StatusCode;

public class HttpServletResponseFacade implements IHttpResponse {

	private final HttpServletResponse response;
	private final IContent content;
	private volatile IStatusCode code = StatusCode.NOT_FOUND;

	public HttpServletResponseFacade(HttpServletResponse response) {
		this.response = notNull("response", response);

		try {
			content = new OutputStreamContent(response.getOutputStream());
		} catch (IOException e) {
			throw Throwables.propagate(e);
		}
	}

	protected HttpServletResponse getResponse() {
		return response;
	}

	@Override
	public IVersion getVersion() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setVersion(IVersion version) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IContent getContent() {
		return content;
	}

	@Override
	public void setContent(IContent content) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setHeader(IHeader header, String value) {
		setHeader(header.get(), value);
	}

	@Override
	public void addHeader(IHeader header, String value) {
		addHeader(header.get(), value);
	}

	@Override
	public String getHeader(IHeader header) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<String> getHeaders(IHeader header) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setHeader(String header, String value) {
		response.setHeader(header, value);
	}

	@Override
	public void addHeader(String header, String value) {
		response.addHeader(header, value);
	}

	@Override
	public String getHeader(String header) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<String> getHeaders(String header) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListMultimap<String, String> getHeaders() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString(boolean includeHeaders) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IStatusCode getStatus() {
		return code;
	}

	@Override
	public String getReason() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setStatus(IStatusCode code) {
		this.code = code;
		response.setStatus(code.getCode());
	}

	@Override
	public void setStatus(IStatusCode code, String reason) {
		setStatus(code);
	}

	@Override
	public void setContentType(ContentType type) {
		setHeader(Header.CONTENT_TYPE, type.getType());
	}

	@Override
	public void setContentLength(long length) {
		setHeader(Header.CONTENT_LENGTH, String.valueOf(length));
	}

	@Override
	public String getContentType() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setCookie(String cookie) {
		throw new UnsupportedOperationException();
	}

}
