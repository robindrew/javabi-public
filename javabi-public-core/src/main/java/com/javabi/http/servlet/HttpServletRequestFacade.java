package com.javabi.http.servlet;

import static com.javabi.common.lang.Variables.notNull;
import static java.util.Collections.unmodifiableList;

import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.google.common.base.Strings;
import com.google.common.base.Throwables;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import com.javabi.http.content.ByteArrayContent;
import com.javabi.http.content.IContent;
import com.javabi.http.cookie.CookieMapParser;
import com.javabi.http.cookie.ICookieMap;
import com.javabi.http.header.Header;
import com.javabi.http.header.IHeader;
import com.javabi.http.message.ContentType;
import com.javabi.http.message.IVersion;
import com.javabi.http.request.IHttpRequest;
import com.javabi.http.request.IMethod;
import com.javabi.http.request.Method;

public class HttpServletRequestFacade implements IHttpRequest {

	private static final String URL_ENCODED = "application/x-www-form-urlencoded";

	private final HttpServletRequest request;
	private final IContent content = new ByteArrayContent();
	private volatile ListMultimap<String, String> parameterMap;
	private volatile ListMultimap<String, String> headerMap;

	public HttpServletRequestFacade(HttpServletRequest request) {
		this.request = notNull("request", request);

		// Access the input stream for the request content
		if (!URL_ENCODED.equals(getContentType())) {
			try {
				content.write(request.getInputStream(), -1);
			} catch (Exception e) {
				Throwables.propagate(e);
			}
		}
	}

	@Override
	public String getContentType() {
		String header = getHeader(Header.CONTENT_TYPE);
		if (header == null) {
			return null;
		}
		int index = header.indexOf(';');
		if (index != -1) {
			header = header.substring(0, index);
		}
		return header;
	}

	private void cacheHeaders() {
		if (headerMap != null) {
			return;
		}
		headerMap = ArrayListMultimap.create();
		Enumeration<String> names = request.getHeaderNames();
		while (names.hasMoreElements()) {
			String name = names.nextElement();
			Enumeration<String> values = request.getHeaders(name);
			while (values.hasMoreElements()) {
				String value = values.nextElement();
				headerMap.put(name, value);
			}
		}
	}

	private void cacheParameters() {
		if (parameterMap != null) {
			return;
		}
		parameterMap = ArrayListMultimap.create();
		Enumeration<String> names = request.getParameterNames();
		while (names.hasMoreElements()) {
			String name = names.nextElement();
			String[] values = request.getParameterValues(name);
			for (String value : values) {
				parameterMap.put(name, value);
			}
		}
	}

	protected HttpServletRequest getRequest() {
		return request;
	}

	@Override
	public IVersion getVersion() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setVersion(IVersion version) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IContent getContent() {
		return content;
	}

	@Override
	public void setContent(IContent content) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setHeader(IHeader header, String value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addHeader(IHeader header, String value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getHeader(IHeader header) {
		return getHeader(header.get());
	}

	@Override
	public List<String> getHeaders(IHeader header) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setHeader(String header, String value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addHeader(String header, String value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getHeader(String header) {
		return request.getHeader(header);
	}

	@Override
	public List<String> getHeaders(String header) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListMultimap<String, String> getHeaders() {
		cacheHeaders();
		return headerMap;
	}

	@Override
	public String toString(boolean includeHeaders) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IMethod getMethod() {
		return Method.valueOf(request.getMethod());
	}

	@Override
	public String getUri() {
		return request.getRequestURI();
	}

	@Override
	public void setMethod(IMethod method) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setUri(String uri) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String get(String key) {
		cacheParameters();
		List<String> values = parameterMap.get(key);
		if (values.isEmpty()) {
			return null;
		}
		return values.get(0);
	}

	@Override
	public List<String> getAll(String key) {
		cacheParameters();
		return unmodifiableList(parameterMap.get(key));
	}

	@Override
	public void setHost(String host) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getHost() {
		return getHeader(Header.HOST);
	}

	@Override
	public String getQuery() {
		return request.getQueryString();
	}

	@Override
	public boolean hasQuery() {
		String query = getQuery();
		return !Strings.isNullOrEmpty(query);
	}

	@Override
	public boolean contains(String key) {
		return get(key) != null;
	}

	@Override
	public String getRemoteHostAddress() {
		return request.getRemoteAddr();
	}

	@Override
	public String toString() {
		if (hasQuery()) {
			return getMethod() + " " + getUri() + "?" + getQuery();
		} else {
			return getMethod() + " " + getUri();
		}
	}

	@Override
	public ListMultimap<String, String> getAll() {
		cacheParameters();
		return Multimaps.unmodifiableListMultimap(parameterMap);
	}

	@Override
	public ICookieMap getCookies() {
		String cookie = getHeader(Header.COOKIE);
		return new CookieMapParser().parseCookieMap(cookie);
	}

	@Override
	public void setContentType(ContentType type) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setCookie(String cookie) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setContentLength(long length) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int getRemotePort() {
		return request.getRemotePort();
	}
}
