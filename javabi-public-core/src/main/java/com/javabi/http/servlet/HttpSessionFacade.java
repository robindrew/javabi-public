package com.javabi.http.servlet;

import static com.javabi.common.lang.Variables.notNull;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.javabi.http.session.IHttpSession;

public class HttpSessionFacade implements IHttpSession {

	private final HttpSession session;

	public HttpSessionFacade(HttpSession session) {
		this.session = notNull("session", session);
	}

	public HttpSessionFacade(HttpServletRequest request) {
		this(request.getSession());
	}

	@Override
	@SuppressWarnings("unchecked")
	public <V> V get(String key) {
		return (V) session.getAttribute(key);
	}

	@Override
	public <V> void set(String key, V value) {
		session.setAttribute(key, value);
	}

	@Override
	public boolean contains(String key) {
		return get(key) != null;
	}
}
