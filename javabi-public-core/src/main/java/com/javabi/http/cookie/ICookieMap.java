package com.javabi.http.cookie;

import java.util.Map.Entry;
import java.util.Set;

public interface ICookieMap {

	String get(String key);

	void set(String key, String value);

	void set(String key);

	boolean contains(String key);

	boolean isEmpty();

	void clear();

	Set<Entry<String, String>> entrySet();

}
