package com.javabi.http.cookie;

public class CookieMapParser implements ICookieMapParser {

	@Override
	public ICookieMap parseCookieMap(String cookie) {
		ICookieMap map = new CookieMap();
		parseCookieMap(cookie, map);
		return map;
	}

	@Override
	public void parseCookieMap(String cookie, ICookieMap map) {
		if (cookie == null || cookie.isEmpty()) {
			return;
		}

		boolean finished = false;
		int startIndex = 0;
		while (!finished) {
			int endIndex = cookie.indexOf(';', startIndex);
			if (endIndex == -1) {
				endIndex = cookie.length();
				finished = true;
			}

			String pair = cookie.substring(startIndex, endIndex);
			int equals = pair.indexOf('=');

			if (equals != -1) {
				String key = pair.substring(0, equals).trim();
				String value = pair.substring(equals + 1).trim();
				map.set(key, value);
			} else {
				String key = pair.trim();
				map.set(key);
			}

			startIndex = endIndex + 1;
		}
	}

}
