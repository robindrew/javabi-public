package com.javabi.http.cookie;

public interface ICookieMapParser {

	ICookieMap parseCookieMap(String cookie);

	void parseCookieMap(String cookie, ICookieMap map);

}