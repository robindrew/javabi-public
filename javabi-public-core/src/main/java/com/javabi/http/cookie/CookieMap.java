package com.javabi.http.cookie;

import static java.util.Collections.unmodifiableSet;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class CookieMap implements ICookieMap {

	private final Map<String, String> map = new LinkedHashMap<String, String>();

	@Override
	public String get(String key) {
		return map.get(key);
	}

	@Override
	public void set(String key, String value) {
		map.put(key, value);
	}

	@Override
	public void set(String key) {
		map.put(key, null);
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public boolean contains(String key) {
		return map.containsKey(key);
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public String toString() {
		StringBuilder cookie = new StringBuilder();
		boolean comma = false;
		for (Entry<String, String> entry : map.entrySet()) {
			if (comma) {
				cookie.append("; ");
			}
			comma = true;
			cookie.append(entry.getKey());
			cookie.append('=');
			cookie.append(entry.getValue());
		}
		return cookie.toString();
	}

	@Override
	public Set<Entry<String, String>> entrySet() {
		return unmodifiableSet(map.entrySet());
	}

}
