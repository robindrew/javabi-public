package com.javabi.http.form;

import static com.javabi.common.dependency.DependencyFactory.getDependency;
import static com.javabi.common.lang.Variables.notEmpty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.http.page.IHttpPage;
import com.javabi.http.parameter.IHttpParameterParser;
import com.javabi.http.request.Method;

public abstract class HttpForm implements IHttpForm {

	private static final Logger log = LoggerFactory.getLogger(HttpForm.class);
	private static final String FORM_NAME_PARAMETER = "form";

	private static String getName(String name) {
		if (name.endsWith("Form")) {
			return name.substring(0, name.length() - 4);
		}
		return name;
	}

	private final String name;

	public HttpForm(String name) {
		this.name = notEmpty("name", name);
	}

	public HttpForm() {
		this.name = getName(getClass().getSimpleName());
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean canHandle(IHttpPage page) {
		String formName = page.getRequest().get(FORM_NAME_PARAMETER);
		return getName().equals(formName);
	}

	@Override
	public final void handlePage(IHttpPage page) {
		if (Method.POST.equals(page.getRequest().getMethod()) && page.getRequest().hasQuery()) {
			log.warn("Request has GET parameters ... (this may affect form submissions)");
		}
		if (!alwaysHandleForm()) {
			String formName = page.getRequest().get(FORM_NAME_PARAMETER);
			if (!getName().equals(formName)) {
				return;
			}
		}

		// Parse form fields from request
		log.info("Form: '" + getName() + "'");
		populateForm(page);

		// Handle form!
		handleForm(page);
	}

	protected boolean alwaysHandleForm() {
		return false;
	}

	protected void populateForm(IHttpPage page) {
		IHttpParameterParser parser = getDependency(IHttpParameterParser.class);
		parser.parse(this, page);
	}

	protected abstract void handleForm(IHttpPage page);

}
