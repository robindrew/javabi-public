package com.javabi.http.form;

import com.javabi.http.page.IHttpPage;
import com.javabi.http.page.IHttpPageHandler;

public interface IHttpForm extends IHttpPageHandler {

	String getName();

	boolean canHandle(IHttpPage page);

}
