package com.javabi.http.session;

public interface IHttpSession {

	<V> V get(String key);

	<V> void set(String key, V value);

	boolean contains(String key);
}
