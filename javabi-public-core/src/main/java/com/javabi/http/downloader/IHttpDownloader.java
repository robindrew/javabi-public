package com.javabi.http.downloader;

import java.util.concurrent.TimeUnit;

import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;

public interface IHttpDownloader {

	void setConnectTimeout(int timeout, TimeUnit unit);

	void setReadTimeout(int timeout, TimeUnit unit);

	void setRequest(IHttpRequest request);

	void setStreaming(boolean streaming);

	void download();

	IHttpResponse getResponse();

}
