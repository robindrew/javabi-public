package com.javabi.http.downloader;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.urlconnection.HttpUrlConnectionResponseFacade;

public class HttpUrlConnectionDownloader implements IHttpDownloader {

	private static final Logger log = LoggerFactory.getLogger(HttpUrlConnectionDownloader.class);

	private static final long DEFAULT_CONNECT_TIMEOUT = TimeUnit.SECONDS.toMillis(1);
	private static final long DEFAULT_CONNECT_TIME_WARNING = TimeUnit.SECONDS.toMillis(5);

	private static final long DEFAULT_READ_TIMEOUT = TimeUnit.SECONDS.toMillis(10);
	// private static final long DEFAULT_DOWNLOAD_TIME_WARNING = TimeUnit.SECONDS.toMillis(10);

	private IHttpRequest request = null;
	private IHttpResponse response = null;

	private long connectTimeout = DEFAULT_CONNECT_TIMEOUT;
	private long connectTimeWarning = DEFAULT_CONNECT_TIME_WARNING;
	private long readTimeout = DEFAULT_READ_TIMEOUT;
	// private long downloadTimeWarning = DEFAULT_DOWNLOAD_TIME_WARNING;
	private boolean streaming = false;

	@Override
	public void setStreaming(boolean streaming) {
		this.streaming = streaming;
	}

	@Override
	public void setConnectTimeout(int timeout, TimeUnit unit) {
		if (timeout < 0) {
			throw new IllegalArgumentException("timeout=" + timeout);
		}
		this.connectTimeout = unit.toMillis(timeout);
	}

	@Override
	public void setReadTimeout(int timeout, TimeUnit unit) {
		if (timeout < 0) {
			throw new IllegalArgumentException("timeout=" + timeout);
		}
		this.readTimeout = unit.toMillis(timeout);
	}

	@Override
	public void setRequest(IHttpRequest request) {
		if (request == null) {
			throw new NullPointerException("request");
		}
		this.request = request;
	}

	@Override
	public void download() {
		if (request == null) {
			throw new IllegalStateException("request not set");
		}

		String uri = request.getUri();
		log.debug("Downloading: " + uri);
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL(uri).openConnection();
			connection.setDoOutput(true);
			connection.setConnectTimeout((int) connectTimeout);
			connection.setReadTimeout((int) readTimeout);

			connection.setRequestMethod(request.getMethod().get());
			for (Entry<String, String> header : request.getHeaders().entries()) {
				String key = header.getKey();
				String value = header.getValue();
				connection.setRequestProperty(key, value);
			}

			// Connect
			connect(connection);
			try {

				// Download
				download(connection);
			} finally {
				connection.disconnect();
			}

		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	private void connect(HttpURLConnection connection) throws IOException {
		String uri = request.getUri();
		log.debug("Connecting to: " + uri);
		NanoTimer timer = new NanoTimer();
		timer.start();
		connection.connect();
		timer.stop();
		if (timer.elapsed() > connectTimeWarning) {
			log.warn("Connected to: " + uri + " in " + timer);
		}
	}

	private void download(HttpURLConnection connection) throws IOException {
		if (request.getContent().length() > 0) {
			try (OutputStream output = new BufferedOutputStream(connection.getOutputStream())) {
				output.write(request.getContent().toByteArray());
				output.flush();
			}
		}

		String uri = request.getUri();
		log.debug("Reading from: " + uri);

		NanoTimer timer = new NanoTimer();
		timer.start();
		response = new HttpUrlConnectionResponseFacade(connection, streaming);
		timer.stop();
		log.debug("Read from: " + uri + ", type=" + response.getContentType() + " in " + timer);
	}

	@Override
	public IHttpResponse getResponse() {
		if (response == null) {
			throw new IllegalStateException("response not set");
		}
		return response;
	}

}
