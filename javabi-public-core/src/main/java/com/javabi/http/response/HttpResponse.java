package com.javabi.http.response;

import static com.javabi.common.lang.Variables.notNull;

import java.util.Map.Entry;

import com.javabi.http.header.Header;
import com.javabi.http.message.ContentType;
import com.javabi.http.message.HttpMessage;

public class HttpResponse extends HttpMessage implements IHttpResponse {

	private IStatusCode code = StatusCode.OK;
	private String reason = null;

	@Override
	public IStatusCode getStatus() {
		return code;
	}

	@Override
	public void setStatus(IStatusCode code) {
		setStatus(code, code.getReason());
	}

	@Override
	public void setStatus(IStatusCode code, String reason) {
		this.code = notNull("code", code);
		this.reason = reason;
	}

	@Override
	public String getReason() {
		return reason;
	}

	@Override
	public String toString() {
		StringBuilder request = new StringBuilder();
		request.append(getVersion().get()).append(' ');
		request.append(getStatus().getCode()).append(' ');
		request.append(getStatus().getReason());
		return request.toString();
	}

	@Override
	public String toString(boolean includeHeaders) {
		StringBuilder request = new StringBuilder();
		request.append(getVersion().get()).append(' ');
		request.append(getStatus().getCode()).append(' ');
		request.append(getStatus().getReason()).append(NEW_LINE);
		for (Entry<String, String> header : getHeaders().entries()) {
			request.append(header.getKey()).append(": ");
			request.append(header.getValue()).append(NEW_LINE);
		}
		request.append(NEW_LINE);
		return request.toString();
	}

	@Override
	public void setContentType(ContentType type) {
		setHeader(Header.CONTENT_TYPE, type.getType());
	}

	@Override
	public void setContentLength(long length) {
		setHeader(Header.CONTENT_LENGTH, String.valueOf(length));
	}

	@Override
	public String getContentType() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setCookie(String cookie) {
		addHeader(Header.SET_COOKIE, cookie);
	}

}
