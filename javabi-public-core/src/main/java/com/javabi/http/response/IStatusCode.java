package com.javabi.http.response;

public interface IStatusCode {

	int getCode();

	String getReason();

}
