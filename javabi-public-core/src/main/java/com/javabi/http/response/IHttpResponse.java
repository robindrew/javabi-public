package com.javabi.http.response;

import com.javabi.http.message.ContentType;
import com.javabi.http.message.IHttpMessage;

public interface IHttpResponse extends IHttpMessage {

	IStatusCode getStatus();

	String getReason();

	void setStatus(IStatusCode code);

	void setStatus(IStatusCode code, String reason);

	void setContentType(ContentType type);

	void setContentLength(long length);

}
