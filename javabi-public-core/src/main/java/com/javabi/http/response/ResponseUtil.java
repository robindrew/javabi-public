package com.javabi.http.response;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.google.common.base.Charsets;
import com.javabi.http.message.ContentType;

public final class ResponseUtil {

	public static void setContent(IHttpResponse response, InputStream content, ContentType type, long contentLength) {
		if (content == null) {
			throw new NullPointerException("content");
		}
		if (contentLength < 0) {
			throw new IllegalArgumentException("contentLength=" + contentLength);
		}
		response.setHeader("Content-Type", type.getType());
		response.setHeader("Content-Length", String.valueOf(contentLength));
		response.getContent().write(content, contentLength);
	}

	public static void setContent(IHttpResponse response, byte[] content, ContentType type) {
		setContent(response, new ByteArrayInputStream(content), type, content.length);
	}

	public static void setContent(IHttpResponse response, String content, ContentType type) {
		setContent(response, content.getBytes(Charsets.UTF_8), type);
	}

	public static void setHtml(IHttpResponse response, String html) {
		setContent(response, html, ContentType.TEXT_HTML);
	}

	public static void setText(IHttpResponse response, String text) {
		setContent(response, text, ContentType.TEXT_PLAIN);
	}

	public static void setXml(IHttpResponse response, String xml) {
		setContent(response, xml, ContentType.TEXT_XML);
	}

	private ResponseUtil() {
	}

}
