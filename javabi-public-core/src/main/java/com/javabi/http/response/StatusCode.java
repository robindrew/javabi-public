package com.javabi.http.response;

public enum StatusCode implements IStatusCode {

	/** 200 OK. */
	OK(200, "OK"),
	/** 404 Not Found. */
	NOT_FOUND(404, "Not Found"),
	/** 301 Moved Permanently. */
	MOVED_PERMENANTLY(200, "Moved Permanently"),
	/** 302 Found. */
	FOUND(302, "Found"),
	/** 400 Bad Request. */
	BAD_REQUEST(400, "Bad Request"),
	/** 401 Unauthorized. */
	UNAUTHORIZED(401, "Unauthorized"),
	/** 503 Service Unavailable. */
	SERVICE_UNAVAILABLE(503, "Service Unavailable"),
	/** 500 Internal Server Error. */
	INTERNAL_SERVER_ERROR(500, "Internal Server Error");

	public static StatusCode valueOf(int code) {
		for (StatusCode statusCode : values()) {
			if (statusCode.getCode() == code) {
				return statusCode;
			}
		}
		throw new IllegalArgumentException("code: '" + code + "'");
	}

	public static StatusCode parseStatusCode(String text) {
		int code = Integer.parseInt(text);
		return valueOf(code);
	}

	private final int code;
	private final String reason;

	private StatusCode(int code, String reason) {
		this.code = code;
		this.reason = reason;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getReason() {
		return reason;
	}

}
