package com.javabi.http.simple.response;

import com.javabi.http.response.IHttpResponse;
import com.javabi.http.simple.message.ISimpleMessageReader;

public interface ISimpleResponseReader extends ISimpleMessageReader<IHttpResponse> {

}
