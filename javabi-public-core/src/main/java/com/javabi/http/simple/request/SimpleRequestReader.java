package com.javabi.http.simple.request;

import java.io.IOException;
import java.io.InputStream;

import com.javabi.http.request.IHttpRequest;
import com.javabi.http.simple.message.SimpleMessageReader;

public class SimpleRequestReader extends SimpleMessageReader<IHttpRequest> implements ISimpleRequestReader {

	protected void parseFirstLine(InputStream input, IHttpRequest request) throws IOException {
		SimpleRequestLineParser parser = new SimpleRequestLineParser(request);
		String requestLine = readLine(input);
		parser.parseRequestLine(requestLine);
	}
}
