package com.javabi.http.simple.request;

import com.javabi.http.message.Version;
import com.javabi.http.request.IHttpRequest;
import com.javabi.http.request.Method;

class SimpleRequestLineParser {

	private final IHttpRequest request;

	public SimpleRequestLineParser(IHttpRequest request) {
		if (request == null) {
			throw new NullPointerException("request");
		}
		this.request = request;
	}

	public void parseRequestLine(String requestLine) {

		// Method
		int methodIndex = requestLine.indexOf(' ');
		if (methodIndex == -1) {
			throw new IllegalArgumentException("requestLine: '" + requestLine + "'");
		}
		request.setMethod(Method.valueOf(requestLine.substring(0, methodIndex)));

		// URI
		int uriIndex = requestLine.indexOf(' ', methodIndex + 1);
		if (uriIndex == -1) {
			throw new IllegalArgumentException("requestLine: '" + requestLine + "'");
		}
		request.setUri(requestLine.substring(methodIndex + 1, uriIndex));

		// Version
		request.setVersion(Version.parseVersion(requestLine.substring(uriIndex + 1)));
	}
}
