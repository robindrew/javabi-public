package com.javabi.http.simple.response;

import java.io.IOException;
import java.io.InputStream;

import com.javabi.http.response.IHttpResponse;
import com.javabi.http.simple.message.SimpleMessageReader;

public class SimpleResponseReader extends SimpleMessageReader<IHttpResponse> implements ISimpleResponseReader {

	protected void parseFirstLine(InputStream input, IHttpResponse response) throws IOException {
		SimpleResponseLineParser parser = new SimpleResponseLineParser(response);
		String requestLine = readLine(input);
		parser.parseResponseLine(requestLine);
	}
}
