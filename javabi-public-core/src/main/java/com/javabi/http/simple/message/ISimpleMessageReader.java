package com.javabi.http.simple.message;

import java.io.InputStream;

import com.javabi.http.message.IHttpMessage;

public interface ISimpleMessageReader<M extends IHttpMessage> {

	void read(InputStream input, M message);

}
