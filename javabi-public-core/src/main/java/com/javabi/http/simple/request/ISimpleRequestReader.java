package com.javabi.http.simple.request;

import com.javabi.http.request.IHttpRequest;
import com.javabi.http.simple.message.ISimpleMessageReader;

public interface ISimpleRequestReader extends ISimpleMessageReader<IHttpRequest> {

}
