package com.javabi.http.simple.response;

import com.javabi.http.message.Version;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.response.StatusCode;

class SimpleResponseLineParser {

	private final IHttpResponse response;

	public SimpleResponseLineParser(IHttpResponse response) {
		if (response == null) {
			throw new NullPointerException("response");
		}
		this.response = response;
	}

	public void parseResponseLine(String responseLine) {

		// Version
		int versionIndex = responseLine.indexOf(' ');
		if (versionIndex == -1) {
			throw new IllegalArgumentException("responseLine: '" + responseLine + "'");
		}
		response.setVersion(Version.parseVersion(responseLine.substring(0, versionIndex)));

		// Code & Reason
		int codeIndex = responseLine.indexOf(' ', versionIndex + 1);
		if (codeIndex == -1) {
			throw new IllegalArgumentException("responseLine: '" + responseLine + "'");
		}
		String code = responseLine.substring(versionIndex + 1, codeIndex);
		String reason = responseLine.substring(codeIndex + 1);
		response.setStatus(StatusCode.parseStatusCode(code), reason);
	}
}
