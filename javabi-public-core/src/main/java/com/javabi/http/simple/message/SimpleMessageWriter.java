package com.javabi.http.simple.message;

import static com.javabi.http.HttpUtil.CHARSET;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.google.common.base.Throwables;
import com.javabi.http.message.IHttpMessage;

public abstract class SimpleMessageWriter<M extends IHttpMessage> implements ISimpleMessageWriter<M> {

	public void write(OutputStream output, M message) {
		try {

			// Head
			writeHead(output, message);

			// Content
			writeContent(output, message);

		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	protected void writeContent(OutputStream output, M message) throws IOException {
		InputStream content = message.getContent().toInputStream();
		if (content == null) {
			return;
		}

		byte[] buffer = new byte[64000];
		while (true) {
			int read = content.read(buffer, 0, buffer.length);
			if (read == -1) {
				break;
			}
			output.write(buffer, 0, read);
		}
		output.flush();
	}

	protected void writeHead(OutputStream output, M message) throws IOException {
		byte[] head = toByteArray(message);
		output.write(head);
		output.flush();
	}

	protected byte[] toByteArray(M message) {
		return message.toString(true).getBytes(CHARSET);
	}

}
