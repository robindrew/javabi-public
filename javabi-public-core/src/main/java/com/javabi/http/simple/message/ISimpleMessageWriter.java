package com.javabi.http.simple.message;

import java.io.OutputStream;

import com.javabi.http.message.IHttpMessage;

public interface ISimpleMessageWriter<M extends IHttpMessage> {

	void write(OutputStream output, M message);

}
