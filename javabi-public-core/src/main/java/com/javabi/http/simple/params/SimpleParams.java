package com.javabi.http.simple.params;

import java.util.Collections;
import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

public class SimpleParams {

	private final ListMultimap<String, String> params = ArrayListMultimap.create();

	public String get(String key) {
		List<String> list = params.get(key);
		if (list == null || list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	public List<String> getAll(String key) {
		List<String> list = params.get(key);
		if (list == null) {
			return Collections.emptyList();
		}
		return Collections.unmodifiableList(list);
	}

	public void set(String key, String value) {
		if (key == null) {
			throw new NullPointerException("key");
		}
		if (value == null) {
			throw new NullPointerException("value");
		}
		params.put(key, value);
	}

}
