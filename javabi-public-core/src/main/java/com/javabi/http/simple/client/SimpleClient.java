package com.javabi.http.simple.client;

import static com.javabi.common.lang.Variables.notNull;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.lang.Quietly;
import com.javabi.common.net.Sockets;
import com.javabi.http.HttpUtil;
import com.javabi.http.client.AbstractHttpClient;
import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.simple.request.ISimpleRequestWriter;
import com.javabi.http.simple.request.SimpleRequestWriter;
import com.javabi.http.simple.response.ISimpleResponseReader;
import com.javabi.http.simple.response.SimpleResponseReader;

public class SimpleClient extends AbstractHttpClient {

	private static final Logger log = LoggerFactory.getLogger(SimpleClient.class);

	private final ISimpleRequestWriter writer;
	private final ISimpleResponseReader reader;

	public SimpleClient(ISimpleRequestWriter writer, ISimpleResponseReader reader) {
		this.writer = notNull("writer", writer);
		this.reader = notNull("reader", reader);
	}

	public SimpleClient() {
		this(new SimpleRequestWriter(), new SimpleResponseReader());
	}

	@Override
	public void send(IHttpRequest request, IHttpResponse response) {
		try {
			String host = HttpUtil.getHost(request);
			int port = HttpUtil.getPort(request);
			Socket socket = Sockets.connect(host, port);
			try {

				// Write request
				log.info("[HttpRequest]\n" + request.toString(true));
				OutputStream output = new BufferedOutputStream(socket.getOutputStream());
				writer.write(output, request);
				output.flush();

				// Read response
				InputStream input = new BufferedInputStream(socket.getInputStream());
				reader.read(input, response);
				log.info("[HttpResponse]\n" + response.toString(true));

			} finally {
				Quietly.close(socket);
			}

		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

}
