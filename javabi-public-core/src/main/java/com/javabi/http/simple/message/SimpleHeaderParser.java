package com.javabi.http.simple.message;

import com.javabi.http.message.IHttpMessage;

class SimpleHeaderParser {

	private final IHttpMessage message;

	public SimpleHeaderParser(IHttpMessage message) {
		if (message == null) {
			throw new NullPointerException("request");
		}
		this.message = message;
	}

	public void parseHeader(String header) {
		int index = header.indexOf(':');
		if (index == -1) {
			throw new IllegalArgumentException("header: '" + header + "'");
		}
		String name = header.substring(0, index).trim();
		String value = header.substring(index + 1).trim();
		message.addHeader(name, value);
	}
}
