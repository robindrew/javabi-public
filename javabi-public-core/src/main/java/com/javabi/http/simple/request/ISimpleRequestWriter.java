package com.javabi.http.simple.request;

import com.javabi.http.request.IHttpRequest;
import com.javabi.http.simple.message.ISimpleMessageWriter;

public interface ISimpleRequestWriter extends ISimpleMessageWriter<IHttpRequest> {

}
