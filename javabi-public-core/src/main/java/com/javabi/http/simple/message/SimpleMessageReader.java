package com.javabi.http.simple.message;

import java.io.IOException;
import java.io.InputStream;

import com.google.common.base.Throwables;
import com.javabi.http.HttpUtil;
import com.javabi.http.message.IHttpMessage;

public abstract class SimpleMessageReader<M extends IHttpMessage> implements ISimpleMessageReader<M> {

	public boolean isStreaming() {
		return false;
	}

	public int getBufferSize() {
		return Short.MAX_VALUE;
	}

	public void read(InputStream input, M message) {
		try {

			// First line
			parseFirstLine(input, message);

			// Headers
			parseHeaders(input, message);

			// Content
			readContent(input, message);

		} catch (IOException e) {
			throw Throwables.propagate(e);
		}
	}

	protected void readContent(InputStream input, M message) {
		long length = HttpUtil.getContentLength(message);
		message.getContent().write(input, length);
	}

	protected abstract void parseFirstLine(InputStream input, M message) throws IOException;

	protected void parseHeaders(InputStream input, M message) throws IOException {
		SimpleHeaderParser parser = new SimpleHeaderParser(message);
		while (true) {
			String header = readLine(input);
			if (header.isEmpty()) {
				break;
			}
			parser.parseHeader(header);
		}
	}

	protected String readLine(InputStream input) throws IOException {
		StringBuilder builder = new StringBuilder();
		while (true) {
			int read = input.read();
			if (read == -1) {
				break;
			}
			if (read == '\n') {
				break;
			}
			if (read == '\r') {
				continue;
			}
			builder.append((char) read);
		}
		return builder.toString();
	}

}
