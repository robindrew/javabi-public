package com.javabi.http.simple.params;

import java.net.URLDecoder;

import com.google.common.base.Throwables;

public class SimpleParamsParser {

	private final boolean decode;

	public SimpleParamsParser(boolean decode) {
		this.decode = decode;
	}

	public SimpleParams parseParams(String text) {
		SimpleParams params = new SimpleParams();
		if (!text.isEmpty()) {
			parse(text, params);
		}
		return params;
	}

	private void parse(String text, SimpleParams params) {
		String[] keyValues = text.split("&");
		for (String keyValue : keyValues) {
			parseKeyValue(keyValue, params);
		}
	}

	private void parseKeyValue(String keyValue, SimpleParams params) {
		keyValue = keyValue.trim();
		if (keyValue.isEmpty()) {
			return;
		}
		int equals = keyValue.indexOf('=');
		if (equals == -1) {
			params.set(keyValue, "");
			return;
		}
		String key = decode(keyValue.substring(0, equals));
		String value = decode(keyValue.substring(equals + 1));
		params.set(key, value);
	}

	private String decode(String text) {
		if (!decode) {
			return text;
		}
		text = text.trim();
		try {
			return URLDecoder.decode(text, "UTF-8");
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

}
