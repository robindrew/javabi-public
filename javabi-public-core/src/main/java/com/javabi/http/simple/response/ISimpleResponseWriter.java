package com.javabi.http.simple.response;

import com.javabi.http.response.IHttpResponse;
import com.javabi.http.simple.message.ISimpleMessageWriter;

public interface ISimpleResponseWriter extends ISimpleMessageWriter<IHttpResponse> {

}
