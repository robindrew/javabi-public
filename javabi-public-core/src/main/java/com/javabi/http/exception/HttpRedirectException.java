package com.javabi.http.exception;

import com.javabi.http.HttpUtil;
import com.javabi.http.response.IHttpResponse;

public class HttpRedirectException extends HttpResponseException {

	private final String path;

	public HttpRedirectException(String path) {
		super("Redirecting to: " + path);
		if (path.isEmpty()) {
			throw new IllegalArgumentException("path is empty");
		}
		this.path = path;
	}

	@Override
	public void populate(IHttpResponse response) {
		HttpUtil.setRedirect(response, path);
	}

}
