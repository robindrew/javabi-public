package com.javabi.http.exception;

import com.javabi.http.HttpUtil;
import com.javabi.http.response.IHttpResponse;

public class HttpNotFoundException extends HttpResponseException {

	private final String path;

	public HttpNotFoundException(String path) {
		super("Not found: " + path);
		if (path.isEmpty()) {
			throw new IllegalArgumentException("path is empty");
		}
		this.path = path;
	}

	@Override
	public void populate(IHttpResponse response) {
		HttpUtil.setNotFound(response, path);
	}

}
