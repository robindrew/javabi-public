package com.javabi.http.exception;

import com.javabi.http.response.IHttpResponse;

public abstract class HttpResponseException extends RuntimeException {

	public HttpResponseException(String message) {
		super(message);
	}

	public abstract void populate(IHttpResponse response);

}
