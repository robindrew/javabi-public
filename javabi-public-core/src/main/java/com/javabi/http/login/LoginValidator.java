package com.javabi.http.login;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.http.page.IHttpPage;

public class LoginValidator implements ILoginValidator {

	private static final Logger log = LoggerFactory.getLogger(LoginValidator.class);

	private String username;
	private String password;

	public void setUsername(String username) {
		if (username.isEmpty()) {
			throw new IllegalArgumentException("username is empty");
		}
		this.username = username;
	}

	public void setPassword(String password) {
		if (password.isEmpty()) {
			throw new IllegalArgumentException("password is empty");
		}
		this.password = password;
	}

	@Override
	public boolean isValid(IHttpPage page, String username, String password) {
		if (!this.username.equals(username)) {
			log.warn("[Login Failed] Invalid username: '" + username + "'");
			return false;
		}
		if (!this.password.equals(password)) {
			log.warn("[Login Failed] Invalid password for username: '" + username + "'");
			return false;
		}
		log.warn("[Login Successful] username: '" + username + "'");
		return true;
	}
}
