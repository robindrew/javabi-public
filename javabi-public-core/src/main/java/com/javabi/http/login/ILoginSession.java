package com.javabi.http.login;

import com.javabi.http.session.IHttpSession;

public interface ILoginSession extends IHttpSession {

	String getUsername();

	void setUsername(String username);

	boolean isLoggedIn();

	void logout();

}
