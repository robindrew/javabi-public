package com.javabi.http.login;

import com.javabi.http.session.IHttpSession;

public class LoginSession implements ILoginSession {

	private static final String USERNAME = "username";

	private final IHttpSession delegate;

	public LoginSession(IHttpSession delegate) {
		this.delegate = delegate;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <V> V get(String key) {
		return (V) delegate.get(key);
	}

	@Override
	public <V> void set(String key, V value) {
		delegate.set(key, value);
	}

	@Override
	public boolean contains(String key) {
		return delegate.contains(key);
	}

	@Override
	public boolean isLoggedIn() {
		return contains(USERNAME);
	}

	@Override
	public void logout() {
		set(USERNAME, null);
	}

	@Override
	public String getUsername() {
		return get(USERNAME);
	}

	@Override
	public void setUsername(String username) {
		set(USERNAME, username);
	}
}
