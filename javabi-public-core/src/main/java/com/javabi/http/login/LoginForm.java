package com.javabi.http.login;

import com.javabi.http.form.HttpForm;
import com.javabi.http.page.IHttpPage;
import com.javabi.http.parameter.HttpParameter;

public class LoginForm extends HttpForm {

	@HttpParameter
	private String username;
	@HttpParameter(hidden = true)
	private String password;

	private final ILoginValidator validator;

	public LoginForm(ILoginValidator validator) {
		super("login");
		this.validator = validator;
	}

	@Override
	protected void handleForm(IHttpPage page) {
		if (!validator.isValid(page, username, password)) {
			return;
		}

		// Login successful
		ILoginSession session = new LoginSession(page.getSession());
		session.setUsername(username);
	}

}
