package com.javabi.http.login;

import com.javabi.http.page.IHttpPage;

public interface ILoginValidator {

	boolean isValid(IHttpPage page, String username, String password);

}
