package com.javabi.http.client;

import com.javabi.common.date.UnitTime;
import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;

public interface IHttpClient {

	UnitTime getConnectTimeout();

	UnitTime getReadTimeout();

	void send(IHttpRequest request, IHttpResponse response);

}
