package com.javabi.http.client;

import static com.javabi.common.lang.Variables.notNull;

import java.util.concurrent.TimeUnit;

import com.javabi.common.date.UnitTime;

public abstract class AbstractHttpClient implements IHttpClient {

	private UnitTime connectTimeout = new UnitTime(60, TimeUnit.SECONDS);
	private UnitTime readTimeout = new UnitTime(5, TimeUnit.MINUTES);

	@Override
	public UnitTime getConnectTimeout() {
		return connectTimeout;
	}

	@Override
	public UnitTime getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(UnitTime timeout) {
		this.readTimeout = notNull("timeout", timeout);
	}

	public void setConnectTimeout(UnitTime timeout) {
		this.connectTimeout = notNull("timeout", timeout);
	}

}
