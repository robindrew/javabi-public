package com.javabi.http.message;

public enum Version implements IVersion {

	/** HTTP/1.0 */
	HTTP_10("HTTP/1.0"),
	/** HTTP/1.1 */
	HTTP_11("HTTP/1.1");

	private final String text;

	private Version(String text) {
		this.text = text;
	}

	public String get() {
		return text;
	}

	public static Version parseVersion(String text) {
		String upper = text.toUpperCase();
		for (Version version : values()) {
			if (version.get().equals(upper)) {
				return version;
			}
		}
		throw new IllegalArgumentException("text: '" + text + "'");
	}

}
