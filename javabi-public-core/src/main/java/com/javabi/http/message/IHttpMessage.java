package com.javabi.http.message;

import java.util.List;

import com.google.common.collect.ListMultimap;
import com.javabi.http.content.IContent;
import com.javabi.http.header.IHeader;

public interface IHttpMessage {

	IVersion getVersion();

	void setVersion(IVersion version);

	IContent getContent();

	void setContent(IContent content);

	void setHeader(IHeader header, String value);

	void addHeader(IHeader header, String value);

	String getHeader(IHeader header);

	List<String> getHeaders(IHeader header);

	void setHeader(String header, String value);

	void addHeader(String header, String value);

	String getHeader(String header);

	List<String> getHeaders(String header);

	ListMultimap<String, String> getHeaders();

	String toString(boolean includeHeaders);

	String getContentType();

	void setContentType(ContentType type);

	void setCookie(String cookie);

}
