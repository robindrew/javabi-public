package com.javabi.http.message;

import static com.javabi.http.message.TransferType.BINARY;
import static com.javabi.http.message.TransferType.TEXT;

public enum ContentType {

	/** text/plain */
	TEXT_PLAIN("text/plain", TEXT, "txt"),
	/** text/html */
	TEXT_HTML("text/html", TEXT, "html", "htm"),
	/** text/xml */
	TEXT_XML("text/xml", TEXT, "xml"),
	/** text/csv */
	TEXT_CSV("text/csv", TEXT, "csv"),
	/** text/css */
	TEXT_CSS("text/css", TEXT, "css"),
	/** text/rtf */
	TEXT_RTF("text/rtf", BINARY, "rtf"),

	/** application/javascript */
	APPLICATION_JAVASCRIPT("application/javascript", TEXT, "js", "javascript"),
	/** application/json */
	APPLICATION_JSON("application/json", TEXT, "json"),
	/** application/zip */
	APPLICATION_ZIP("application/zip", BINARY, "zip"),

	/** application/x-font-ttf */
	APPLICATION_TTF("application/x-font-ttf", BINARY, "ttf"),
	/** application/x-font-eot */
	APPLICATION_EOT("application/x-font-eot", BINARY, "eot"),
	/** application/x-font-woff */
	APPLICATION_WOFF("application/x-font-woff", BINARY, "woff"),
	/** application/x-font-svg */
	APPLICATION_SVG("application/x-font-svg", BINARY, "svg"),
	/** application/pdf */
	APPLICATION_PDF("application/pdf", BINARY, "pdf"),
	/** application/x-www-form-urlencoded */
	APPLICATION_FORM_ENCODED("application/x-www-form-urlencoded", BINARY, "form-urlencoded"),

	/** multipart/form-data */
	MULTIPART_FORM_DATA("multipart/form-data", BINARY, "form-data"),

	/** binary/octet-stream */
	BINARY_OCTET_STREAM("binary/octet-stream", BINARY, "octet-stream"),

	/** video/mpeg */
	VIDEO_MPEG("video/mpeg", BINARY, "mpeg"),
	/** video/mp4 */
	VIDEO_MP4("video/mp4", BINARY, "mp4"),
	/** video/raw */
	VIDEO_RAW("video/raw", BINARY, "raw"),
	/** video/quicktime */
	VIDEO_QUICKTIME("video/quicktime", BINARY, "quicktime"),

	/** image/vnd.microsoft.icon */
	IMAGE_ICO("image/x-icon", BINARY, "ico"),
	/** image/png */
	IMAGE_PNG("image/png", BINARY, "png"),
	/** image/gif */
	IMAGE_GIF("image/gif", BINARY, "gif"),
	/** image/tiff */
	IMAGE_TIFF("image/tiff", BINARY, "tiff"),
	/** image/jpeg */
	IMAGE_JPEG("image/jpeg", BINARY, "jpg", "jpeg");

	public static ContentType parseContentType(String name, ContentType defaultType) {
		name = name.toLowerCase();
		int dot = name.lastIndexOf('.');
		if (dot != -1) {
			name = name.substring(dot + 1);
		}
		for (ContentType type : values()) {
			if (type.isType(name)) {
				return type;
			}
			if (type.hasEnding(name)) {
				return type;
			}
		}
		return defaultType;
	}

	private final String type;
	private final TransferType transfer;
	private final String[] endings;

	private ContentType(String type, TransferType transfer, String... endings) {
		this.type = type;
		this.transfer = transfer;
		this.endings = endings;
	}

	private boolean isType(String name) {
		return type.equals(name);
	}

	public String getDefaultEnding() {
		return endings[0];
	}

	public boolean hasEnding(String filename) {
		for (String ending : endings) {
			if (filename.endsWith(ending)) {
				return true;
			}
		}
		return false;
	}

	public String getType() {
		return type;
	}

	public TransferType getTransferType() {
		return transfer;
	}
}
