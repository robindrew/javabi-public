package com.javabi.http.message;

public enum TransferType {

	BINARY, TEXT;
}
