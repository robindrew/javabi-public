package com.javabi.http.message;

import static com.javabi.common.lang.Variables.notNull;

import java.util.Collections;
import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.javabi.http.content.ByteArrayContent;
import com.javabi.http.content.IContent;
import com.javabi.http.header.Header;
import com.javabi.http.header.IHeader;

public abstract class HttpMessage implements IHttpMessage {

	public static final String NEW_LINE = "\r\n";

	private final ListMultimap<String, String> headerMap = ArrayListMultimap.create();
	private IVersion version = Version.HTTP_11;
	private IContent content = new ByteArrayContent();

	@Override
	public IContent getContent() {
		return content;
	}

	@Override
	public void setContent(IContent content) {
		this.content = notNull("content", content);
	}

	public final ListMultimap<String, String> getHeaders() {
		return headerMap;
	}

	public void setHeader(IHeader header, String value) {
		setHeader(header.get(), value);
	}

	public void addHeader(IHeader header, String value) {
		addHeader(header.get(), value);
	}

	public void setHeader(String name, String value) {
		if (name.isEmpty()) {
			throw new IllegalArgumentException("name is empty");
		}
		if (value == null) {
			throw new NullPointerException("value");
		}
		headerMap.removeAll(name);
		headerMap.put(name, value);
	}

	public void addHeader(String name, String value) {
		if (name.isEmpty()) {
			throw new IllegalArgumentException("name is empty");
		}
		if (value == null) {
			throw new NullPointerException("value");
		}
		headerMap.put(name, value);
	}

	@Override
	public IVersion getVersion() {
		return version;
	}

	@Override
	public String getHeader(IHeader header) {
		return getHeader(header.get());
	}

	@Override
	public List<String> getHeaders(IHeader header) {
		return getHeaders(header.get());
	}

	@Override
	public void setVersion(IVersion version) {
		this.version = notNull("version", version);
	}

	@Override
	public String getHeader(String header) {
		List<String> list = headerMap.get(header);
		if (list == null || list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public List<String> getHeaders(String header) {
		List<String> list = headerMap.get(header);
		if (list == null) {
			return Collections.emptyList();
		}
		return Collections.unmodifiableList(list);
	}

	@Override
	public String getContentType() {
		return getHeader(Header.CONTENT_TYPE);
	}

	@Override
	public void setContentType(ContentType type) {
		setHeader(Header.CONTENT_TYPE, type.getType());
	}

}
