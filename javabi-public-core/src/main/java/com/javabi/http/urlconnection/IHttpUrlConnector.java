package com.javabi.http.urlconnection;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import com.javabi.http.header.IHeader;
import com.javabi.http.message.ContentType;
import com.javabi.http.request.IMethod;

public interface IHttpUrlConnector {

	void setMethod(IMethod method);

	void setHeader(String key, Object value);

	void addHeader(String key, Object value);

	void setHeader(IHeader header, Object value);

	void addHeader(IHeader header, Object value);

	void setContent(ContentType type, byte[] content);

	void setContent(ContentType type, String content);

	void setContent(ContentType type, String content, Charset charset);

	String getHeader(String key);

	String getHeader(IHeader header);

	List<String> getHeaders(String key);

	List<String> getHeaders(IHeader header);

	Map<String, List<String>> getHeaders();

	byte[] getBinaryContent();

	String getTextContent(Charset charset);

	String getTextContent();

	void connect();

	boolean hasConnected();

}
