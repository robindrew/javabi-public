package com.javabi.http.urlconnection;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import com.google.common.base.Charsets;
import com.google.common.base.Throwables;
import com.javabi.http.header.IHeader;
import com.javabi.http.message.ContentType;
import com.javabi.http.request.IMethod;
import com.javabi.http.request.Method;

public class HttpUrlConnector implements IHttpUrlConnector {

	private final URL url;
	private final HttpURLConnection connection;
	private final AtomicBoolean connected = new AtomicBoolean(false);
	private volatile byte[] content = null;

	public HttpUrlConnector(String url) {
		try {
			this.url = new URL(url);
			this.connection = (HttpURLConnection) this.url.openConnection();
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public boolean hasConnected() {
		return connected.get();
	}

	@Override
	public void setMethod(IMethod method) {
		if (hasConnected()) {
			throw new IllegalStateException("already connected");
		}
		try {
			connection.setRequestMethod(method.get());
			if (!method.get().equals(Method.POST.get())) {
				connection.setDoOutput(false);
			}
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public void setHeader(String key, Object value) {
		if (hasConnected()) {
			throw new IllegalStateException("already connected");
		}
		connection.setRequestProperty(key, value.toString());
	}

	@Override
	public void addHeader(String key, Object value) {
		if (hasConnected()) {
			throw new IllegalStateException("already connected");
		}
		connection.addRequestProperty(key, value.toString());
	}

	@Override
	public void setHeader(IHeader header, Object value) {
		if (hasConnected()) {
			throw new IllegalStateException("already connected");
		}
		connection.setRequestProperty(header.get(), value.toString());
	}

	@Override
	public void addHeader(IHeader header, Object value) {
		if (hasConnected()) {
			throw new IllegalStateException("already connected");
		}
		connection.addRequestProperty(header.get(), value.toString());
	}

	@Override
	public void setContent(ContentType type, byte[] content) {
		if (hasConnected()) {
			throw new IllegalStateException("already connected");
		}
		connection.setDoOutput(true);
		try {
			OutputStream output = connection.getOutputStream();
			output.write(content);
			output.flush();
			connected.set(true);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public void setContent(ContentType type, String content) {
		setContent(type, content, Charsets.UTF_8);
	}

	@Override
	public void setContent(ContentType type, String content, Charset charset) {
		byte[] data = content.getBytes(charset);
		setContent(type, data);
	}

	@Override
	public String getHeader(String key) {
		if (!hasConnected()) {
			throw new IllegalStateException("not connected");
		}
		Map<String, List<String>> map = connection.getHeaderFields();
		List<String> list = map.get(key);
		if (list == null || list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public String getHeader(IHeader header) {
		return getHeader(header.get());
	}

	@Override
	public List<String> getHeaders(String key) {
		if (!hasConnected()) {
			throw new IllegalStateException("not connected");
		}
		Map<String, List<String>> map = connection.getHeaderFields();
		return map.get(key);
	}

	@Override
	public List<String> getHeaders(IHeader header) {
		return getHeaders(header.get());
	}

	@Override
	public Map<String, List<String>> getHeaders() {
		if (!hasConnected()) {
			throw new IllegalStateException("not connected");
		}
		return connection.getHeaderFields();
	}

	@Override
	public byte[] getBinaryContent() {
		if (!hasConnected()) {
			throw new IllegalStateException("not connected");
		}
		if (content != null) {
			return content;
		}
		try {
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			InputStream input = connection.getInputStream();
			while (true) {
				int read = input.read();
				if (read == -1) {
					break;
				}
				output.write(read);
			}
			content = output.toByteArray();
			return content;
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public String getTextContent(Charset charset) {
		byte[] content = getBinaryContent();
		return new String(content, charset);
	}

	@Override
	public String getTextContent() {
		return getTextContent(Charsets.UTF_8);
	}

	@Override
	public void connect() {
		if (connected.compareAndSet(false, true)) {
			try {
				// Force connection
				connection.getInputStream();
			} catch (Exception e) {
				throw Throwables.propagate(e);
			}
		}
	}

}
