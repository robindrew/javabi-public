package com.javabi.http.urlconnection;

import static java.util.Collections.emptyList;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.io.ByteStreams;
import com.javabi.http.content.ByteArrayContent;
import com.javabi.http.content.IContent;
import com.javabi.http.content.InputStreamContent;
import com.javabi.http.header.Header;
import com.javabi.http.header.IHeader;
import com.javabi.http.message.ContentType;
import com.javabi.http.message.IVersion;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.response.IStatusCode;
import com.javabi.http.response.StatusCode;

public class HttpUrlConnectionResponseFacade implements IHttpResponse {

	private final StatusCode status;
	private final Map<String, List<String>> headers;
	private final String reason;
	private final IContent content;

	public HttpUrlConnectionResponseFacade(HttpURLConnection connection, boolean streaming) throws IOException {

		// Header line
		this.status = StatusCode.valueOf(connection.getResponseCode());
		this.reason = connection.getResponseMessage();

		// Headers
		this.headers = connection.getHeaderFields();

		// Content
		if (status.equals(StatusCode.UNAUTHORIZED)) {
			this.content = new ByteArrayContent(new byte[0]);
		} else {
			if (streaming) {
				this.content = getStream(connection);
			} else {
				this.content = getByteArray(connection);
			}
		}
	}

	private IContent getStream(HttpURLConnection connection) throws IOException {
		BufferedInputStream stream = new BufferedInputStream(connection.getInputStream());
		return new InputStreamContent(stream);
	}

	private IContent getByteArray(HttpURLConnection connection) throws IOException {
		BufferedInputStream stream = new BufferedInputStream(connection.getInputStream());
		byte[] array = ByteStreams.toByteArray(stream);
		ByteArrayContent content = new ByteArrayContent();
		content.write(array);
		return content;
	}

	@Override
	public IContent getContent() {
		return content;
	}

	@Override
	public ListMultimap<String, String> getHeaders() {
		ListMultimap<String, String> map = ArrayListMultimap.create();
		for (Entry<String, List<String>> entry : headers.entrySet()) {
			String name = entry.getKey();
			if (name == null) {
				continue;
			}
			List<String> valueList = entry.getValue();
			for (String value : valueList) {
				map.put(name, value);
			}
		}
		return map;
	}

	private List<String> getHeaders(String header, boolean isLowerCase) {
		if (!isLowerCase) {
			header = header.toLowerCase();
		}
		for (Entry<String, List<String>> entry : headers.entrySet()) {
			String name = entry.getKey();
			if (name == null) {
				continue;
			}
			if (name.toLowerCase().equals(header)) {
				return entry.getValue();
			}
		}
		return emptyList();
	}

	@Override
	public List<String> getHeaders(IHeader header) {
		return getHeaders(header.getLower(), true);
	}

	@Override
	public String getHeader(IHeader header) {
		List<String> valueList = getHeaders(header);
		return valueList.isEmpty() ? null : valueList.get(0);
	}

	@Override
	public String getHeader(String header) {
		List<String> valueList = getHeaders(header, false);
		return valueList.isEmpty() ? null : valueList.get(0);
	}

	@Override
	public List<String> getHeaders(String header) {
		return getHeaders(header, false);
	}

	@Override
	public String getContentType() {
		return getHeader(Header.CONTENT_TYPE);
	}

	@Override
	public IStatusCode getStatus() {
		return status;
	}

	@Override
	public String getReason() {
		return reason;
	}

	@Override
	public IVersion getVersion() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setContent(IContent content) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setHeader(IHeader header, String value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addHeader(IHeader header, String value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setHeader(String header, String value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addHeader(String header, String value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString(boolean includeHeaders) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setStatus(IStatusCode code) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setStatus(IStatusCode code, String reason) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setContentType(ContentType type) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setContentLength(long length) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setVersion(IVersion version) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setCookie(String cookie) {
		throw new UnsupportedOperationException();
	}

}
