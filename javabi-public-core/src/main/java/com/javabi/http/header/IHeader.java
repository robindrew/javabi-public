package com.javabi.http.header;

public interface IHeader {

	String get();

	String getLower();
}
