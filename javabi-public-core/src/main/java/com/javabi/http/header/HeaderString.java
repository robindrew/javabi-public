package com.javabi.http.header;

import static com.javabi.common.lang.Variables.notEmpty;

public class HeaderString implements IHeader {

	private final String header;

	public HeaderString(String header) {
		this.header = notEmpty("header", header);
	}

	@Override
	public String get() {
		return header;
	}

	@Override
	public String getLower() {
		return header.toLowerCase();
	}
}
