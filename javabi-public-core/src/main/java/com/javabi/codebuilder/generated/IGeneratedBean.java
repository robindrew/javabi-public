package com.javabi.codebuilder.generated;

import java.io.Serializable;

/**
 * A Generated Bean.
 */
public interface IGeneratedBean extends IGeneratedObject, Serializable {

}
