package com.javabi.codebuilder.generated;

public interface IGeneratedPersistor extends IGeneratedObject {

	boolean exists();

	void create();

	void destroy();

	void clear();

	int size();

	boolean isEmpty();

}
