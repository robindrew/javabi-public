package com.javabi.codebuilder.generated;

/**
 * A Generated Enumeration.
 */
public interface IGeneratedEnum extends IGeneratedObject {

	String name();

}
