package com.javabi.codebuilder.generated;

import java.io.Serializable;

/**
 * A Generated Command.
 */
public interface IGeneratedCommand extends IGeneratedObject, Serializable {

	short getCommandId();

}
