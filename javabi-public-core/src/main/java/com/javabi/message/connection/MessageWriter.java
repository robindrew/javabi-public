package com.javabi.message.connection;

import static com.javabi.message.type.MessageType.DISCONNECT;
import static com.javabi.message.type.MessageType.IDENTIFY;
import static com.javabi.message.type.MessageType.PING;
import static com.javabi.message.type.MessageType.PUBLISH;
import static com.javabi.message.type.MessageType.SUBSCRIBE;
import static com.javabi.message.type.MessageType.UNSUBSCRIBE;

import java.util.Collection;

import com.google.common.base.Throwables;
import com.javabi.common.io.data.IDataWriter;
import com.javabi.message.IMessage;
import com.javabi.message.IMessageSerializer;
import com.javabi.message.type.ITypedMessage;
import com.javabi.message.type.MessageType;
import com.javabi.message.type.TypedMessage;

public class MessageWriter implements IMessageWriter {

	private final IDataWriter writer;
	private final IMessageSerializer serializer;

	public MessageWriter(IDataWriter writer, IMessageSerializer serializer) {
		if (writer == null) {
			throw new IllegalArgumentException("writer");
		}
		if (serializer == null) {
			throw new IllegalArgumentException("serializer");
		}
		this.writer = writer;
		this.serializer = serializer;
	}

	@Override
	public void write(ITypedMessage message) {
		try {
			synchronized (this) {
				writeMessage(message);
				writer.flush();
			}
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public void write(Collection<ITypedMessage> messages) {
		if (messages.isEmpty()) {
			return;
		}
		try {
			synchronized (this) {
				for (ITypedMessage message : messages) {
					writeMessage(message);
				}
				writer.flush();
			}
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	private void writeMessage(ITypedMessage message) throws Exception {
		if (message == null) {
			throw new NullPointerException("message");
		}
		MessageType type = message.getType();
		writer.writePositiveInt(type.ordinal());
		writeMessageContent(message);
	}

	private void writeMessageContent(ITypedMessage message) throws Exception {
		MessageType type = message.getType();
		Object content = message.getContent();
		switch (type) {
			case PUBLISH:
				serializer.writeObject(writer, (IMessage) content);
				break;
			case IDENTIFY:
			case SUBSCRIBE:
			case UNSUBSCRIBE:
				writer.writeString((String) content, false);
				break;
			case DISCONNECT:
			case PING:
				break;
			default:
				throw new IllegalStateException("Unsupported message type: " + type);
		}
	}

	@Override
	public void writeMessage(IMessage message) {
		write(new TypedMessage(PUBLISH, message));
	}

	@Override
	public void writePing() {
		write(new TypedMessage(PING));
	}

	@Override
	public void writeDisconnect() {
		write(new TypedMessage(DISCONNECT));
	}

	@Override
	public void writeSubscribe(String messageId) {
		write(new TypedMessage(SUBSCRIBE, messageId));

	}

	@Override
	public void writeUnsubscribe(String messageId) {
		write(new TypedMessage(UNSUBSCRIBE, messageId));
	}

	@Override
	public void writeIdentify(String identity) {
		write(new TypedMessage(IDENTIFY, identity));
	}

}
