package com.javabi.message.connection;

import com.javabi.message.type.ITypedMessage;

public interface IMessageReader {

	ITypedMessage read();

}
