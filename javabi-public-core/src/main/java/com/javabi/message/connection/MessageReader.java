package com.javabi.message.connection;

import java.io.IOException;

import com.google.common.base.Throwables;
import com.javabi.common.io.data.IDataReader;
import com.javabi.message.IMessageSerializer;
import com.javabi.message.type.ITypedMessage;
import com.javabi.message.type.MessageType;
import com.javabi.message.type.TypedMessage;

public class MessageReader implements IMessageReader {

	private final IDataReader reader;
	private final IMessageSerializer serializer;

	public MessageReader(IDataReader reader, IMessageSerializer serializer) {
		if (reader == null) {
			throw new IllegalArgumentException("reader");
		}
		if (serializer == null) {
			throw new IllegalArgumentException("serializer");
		}
		this.reader = reader;
		this.serializer = serializer;
	}

	@Override
	public ITypedMessage read() {
		try {
			synchronized (this) {
				return readResponse();
			}
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	private ITypedMessage readResponse() throws IOException {
		MessageType type = readMessageType();
		Object content = null;
		switch (type) {
			case IDENTIFY:
			case SUBSCRIBE:
			case UNSUBSCRIBE:
				content = reader.readString(false);
				break;
			case PUBLISH:
				content = serializer.readObject(reader);
				break;
			case PING:
			case DISCONNECT:
				break;
			default:
				throw new IllegalStateException("Type not supported: " + type);
		}
		return new TypedMessage(type, content);
	}

	private MessageType readMessageType() throws IOException {
		int ordinal = reader.readPositiveInt();
		MessageType[] values = MessageType.values();
		return values[ordinal];
	}

}
