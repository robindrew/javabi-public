package com.javabi.message.connection;

import java.util.Collection;

import com.javabi.message.IMessage;
import com.javabi.message.type.ITypedMessage;

public interface IMessageWriter {

	void write(ITypedMessage message);

	void write(Collection<ITypedMessage> messages);

	void writeIdentify(String identity);
	
	void writePing();

	void writeDisconnect();

	void writeMessage(IMessage message);

	void writeSubscribe(String messageId);

	void writeUnsubscribe(String messageId);

}
