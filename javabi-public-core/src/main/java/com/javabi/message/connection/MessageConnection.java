package com.javabi.message.connection;

import com.google.common.base.Throwables;
import com.javabi.common.io.connection.IConnection;
import com.javabi.common.io.connection.INetworkAddress;
import com.javabi.common.io.connection.SocketConnection;
import com.javabi.common.io.data.DataReader;
import com.javabi.common.io.data.DataWriter;
import com.javabi.common.io.data.IDataReader;
import com.javabi.common.io.data.IDataWriter;
import com.javabi.message.IMessageSerializer;

public class MessageConnection implements IMessageConnection {

	private final IConnection connection;
	private final IMessageWriter writer;
	private final IMessageReader reader;

	public MessageConnection(IConnection connection, IMessageSerializer serializer) {
		if (connection == null) {
			throw new NullPointerException("connection");
		}
		this.connection = connection;

		try {

			// Reader
			IDataReader dataReader = new DataReader(connection.getInput());
			this.reader = new MessageReader(dataReader, serializer);

			// Writer
			IDataWriter dataWriter = new DataWriter(connection.getOutput());
			this.writer = new MessageWriter(dataWriter, serializer);

		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	public MessageConnection(INetworkAddress address, IMessageSerializer serializer) {
		this(new SocketConnection(address), serializer);
	}

	@Override
	public IMessageWriter getWriter() {
		return writer;
	}

	@Override
	public IMessageReader getReader() {
		return reader;
	}

	public IConnection getConnection() {
		return connection;
	}

}
