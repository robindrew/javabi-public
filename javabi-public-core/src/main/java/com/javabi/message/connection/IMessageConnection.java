package com.javabi.message.connection;

public interface IMessageConnection {

	IMessageReader getReader();

	IMessageWriter getWriter();

}
