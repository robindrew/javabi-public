package com.javabi.message.publisher;

import com.javabi.message.IMessage;

public interface IMessagePublisher {

	void publish(IMessage message);

}
