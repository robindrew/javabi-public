package com.javabi.message.publisher;

import static com.javabi.message.type.MessageType.PUBLISH;

import java.net.SocketException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.io.connection.IConnection;
import com.javabi.message.IMessage;
import com.javabi.message.IMessageListener;
import com.javabi.message.IMessageSerializer;
import com.javabi.message.connection.IMessageConnection;
import com.javabi.message.connection.MessageConnection;
import com.javabi.message.subscriber.listener.IListenerSubscriber;
import com.javabi.message.type.ITypedMessage;
import com.javabi.message.type.ITypedMessagePublisher;
import com.javabi.message.type.MessageType;
import com.javabi.message.type.TypedMessage;
import com.javabi.message.type.TypedMessagePublisher;
import com.javabi.server.AbstractConnectionHandler;

public class SubscriberConnectionHandler extends AbstractConnectionHandler implements IMessageListener {

	private static final Logger log = LoggerFactory.getLogger(SubscriberConnectionHandler.class);

	private static final String UNKNOWN_IDENTITY = "unknown";

	private final IMessageConnection connection;
	private final ITypedMessagePublisher publisher;
	private final IListenerSubscriber subscriber;
	private volatile String identity = UNKNOWN_IDENTITY;

	public SubscriberConnectionHandler(IConnection connection, long number, IMessageSerializer serializer, IListenerSubscriber subscriber) {
		super(connection, number);
		if (subscriber == null) {
			throw new NullPointerException("subscriber");
		}
		this.subscriber = subscriber;
		this.connection = new MessageConnection(connection, serializer);
		this.publisher = new TypedMessagePublisher(this.connection.getWriter(), number);
	}

	@Override
	protected void handleConnection() {

		// Handle messages FROM subscriber ...
		try {
			while (publisher.isRunning()) {
				if (!handleTypedMessage()) {
					break;
				}
			}

		} finally {

			// Guarantee unsubscribe
			subscriber.unsubscribeAll(this);
		}
	}

	private boolean handleTypedMessage() {
		ITypedMessage message = connection.getReader().read();
		log.info("[Received Message] " + message);

		MessageType type = message.getType();
		switch (type) {
			case IDENTIFY:
				return handleIdentify((String) message.getContent());
			case PING:
				return handlePing();
			case DISCONNECT:
				return handleDisconnect();
			case SUBSCRIBE:
				return handleSubscribe((String) message.getContent());
			case UNSUBSCRIBE:
				return handleUnsubscribe((String) message.getContent());

			case PUBLISH:
				throw new IllegalStateException("Subscriber attempted to publish message");
			default:
				throw new IllegalStateException("Message type not supported: " + type);
		}
	}

	private boolean handleIdentify(String identity) {
		this.identity = identity;
		log.info("Subscriber Identified: " + this);
		return true;
	}

	private boolean handleDisconnect() {
		return false;
	}

	private boolean handlePing() {
		return true;
	}

	private boolean handleUnsubscribe(String messageId) {
		subscriber.unsubscribe(messageId, this);
		log.warn("Subscription Removed: " + this + ", id=" + messageId);
		return true;
	}

	private boolean handleSubscribe(String messageId) {
		subscriber.subscribe(messageId, this);
		log.warn("Subscription Added: " + this + ", id=" + messageId);
		return true;
	}

	@Override
	public void notify(IMessage message) {
		publisher.publish(new TypedMessage(PUBLISH, message));
	}

	@Override
	protected void handleError(Throwable t) {
		t = Throwables.getRootCause(t);
		if (t instanceof SocketException && "Connection reset".equals(t.getMessage())) {
			log.warn("Connection Lost: " + this);
		} else {
			log.error("Connection Error: " + this, t);
		}
		publisher.shutdown();
	}

	@Override
	public String toString() {
		return identity + ", " + super.toString();
	}

}
