package com.javabi.message.publisher;

import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.io.connection.IConnection;
import com.javabi.common.io.connection.INetworkAddress;
import com.javabi.message.IMessage;
import com.javabi.message.IMessageListener;
import com.javabi.message.IMessageSerializer;
import com.javabi.message.subscriber.listener.IListenerSubscriber;
import com.javabi.message.subscriber.listener.ListenerSubscriberMap;
import com.javabi.server.ServerControl;
import com.javabi.server.connection.IConnectionHandler;
import com.javabi.server.connection.IConnectionHandlerFactory;

public class ServerMessagePublisher extends ServerControl implements IMessagePublisher, IConnectionHandlerFactory {

	private static final Logger log = LoggerFactory.getLogger(ServerMessagePublisher.class);

	private final IListenerSubscriber subscriber = new ListenerSubscriberMap();
	private final IMessageSerializer serializer;

	public ServerMessagePublisher(String threadName, INetworkAddress address, ExecutorService service, IMessageSerializer serializer) {
		super(threadName, address, service);
		this.serializer = serializer;
	}

	public ServerMessagePublisher(String threadName, INetworkAddress address, ThreadFactory factory, IMessageSerializer serializer) {
		this(threadName, address, Executors.newCachedThreadPool(factory), serializer);
	}

	@Override
	public void publish(IMessage message) {
		Collection<IMessageListener> listeners = subscriber.getListeners(message.getMessageId());
		if (listeners.isEmpty()) {
			log.warn("No listeners to published message: " + message.getMessageId());
			return;
		}
		for (IMessageListener listener : listeners) {
			listener.notify(message);
		}
	}

	@Override
	protected IConnectionHandlerFactory getConnectionHandlerFactory() {
		return this;
	}

	@Override
	public IConnectionHandler newConnection(IConnection connection, long number) {
		return new SubscriberConnectionHandler(connection, number, serializer, subscriber);
	}
}
