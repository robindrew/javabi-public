package com.javabi.message.publisher;

import com.javabi.message.IMessage;
import com.javabi.message.IMessageListener;
import com.javabi.message.connection.IMessageConnection;

/**
 * Represents a single client connection connection in to the publisher.
 */
public class ClientMessageListener implements IMessageListener {

	private final IMessageConnection connection;

	public ClientMessageListener(IMessageConnection connection) {
		if (connection == null) {
			throw new NullPointerException("connection");
		}
		this.connection = connection;
	}

	@Override
	public void notify(IMessage message) {
		connection.getWriter().writeMessage(message);
	}

}
