package com.javabi.message.subscriber.listener;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Multimap;
import com.javabi.common.concurrent.copyonwrite.CopyOnWriteMultimap;
import com.javabi.message.IMessageListener;

public class ListenerSubscriberMap implements IListenerSubscriber {

	private static final Logger log = LoggerFactory.getLogger(ListenerSubscriberMap.class);

	private final Multimap<String, IMessageListener> idToListenerMap = new CopyOnWriteMultimap<String, IMessageListener>();

	@Override
	public Collection<IMessageListener> getListeners(String messageId) {
		return idToListenerMap.get(messageId);
	}

	@Override
	public int subscribe(String messageId, IMessageListener listener) {
		if (idToListenerMap.put(messageId, listener)) {
			log.info("[Added Subscription] " + messageId + " -> " + listener);
		}
		return idToListenerMap.get(messageId).size();
	}

	@Override
	public int unsubscribe(String messageId, IMessageListener listener) {
		for (IMessageListener element : idToListenerMap.get(messageId)) {
			if (element.equals(listener)) {
				if (idToListenerMap.remove(messageId, element)) {
					log.info("[Removed Subscription] " + messageId + " -> " + element);
				}
			}
		}
		return idToListenerMap.get(messageId).size();
	}

	@Override
	public void unsubscribeAll(String messageId) {
		idToListenerMap.removeAll(messageId);
	}

	@Override
	public Set<String> unsubscribeAll() {
		Set<String> set = idToListenerMap.keySet();
		log.info("[Removed All Subscriptions] " + set);
		idToListenerMap.clear();
		return set;
	}

	@Override
	public Set<String> unsubscribeAll(IMessageListener listener) {
		Set<String> messageIdsRemoved = new HashSet<String>();
		for (Entry<String, IMessageListener> entry : idToListenerMap.entries()) {
			String messageId = entry.getKey();
			IMessageListener element = entry.getValue();

			// Remove entry if same listener
			if (element.equals(listener)) {
				if (idToListenerMap.remove(messageId, element)) {
					log.info("[Removed Subscription] " + messageId + " -> " + element);
				}

				// No more listeners?
				if (!idToListenerMap.containsKey(messageId)) {
					messageIdsRemoved.add(messageId);
				}
			}
		}
		return messageIdsRemoved;
	}
}
