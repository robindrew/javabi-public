package com.javabi.message.subscriber.server;

import com.javabi.message.IMessageListener;

public interface IClientMessageSubscriber extends IMessageSubscriber, IMessageListener {

	void addListener(IMessageListener listener);

	void removeListener(IMessageListener listener);

}
