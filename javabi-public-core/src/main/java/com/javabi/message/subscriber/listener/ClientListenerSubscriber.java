package com.javabi.message.subscriber.listener;

import java.util.Collection;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.message.IMessage;
import com.javabi.message.IMessageListener;
import com.javabi.message.subscriber.server.IMessageSubscriber;

public class ClientListenerSubscriber implements IListenerSubscriber, IMessageListener {

	private static final Logger log = LoggerFactory.getLogger(ClientListenerSubscriber.class);

	private final IMessageSubscriber messageSubscriber;
	private final ListenerSubscriberMap listenerSubscriber = new ListenerSubscriberMap();

	public ClientListenerSubscriber(IMessageSubscriber subscriber) {
		if (subscriber == null) {
			throw new NullPointerException("subscriber");
		}
		this.messageSubscriber = subscriber;
	}

	@Override
	public void notify(IMessage message) {
		// Notify all the listeners
		String messageId = message.getMessageId();
		for (IMessageListener listener : getListeners(messageId)) {
			listener.notify(message);
		}
	}

	@Override
	public int subscribe(String messageId, IMessageListener listener) {
		log.info("[Subscribing] #" + messageId + " -> " + listener);

		// Map listeners to the message id
		int listeners = listenerSubscriber.subscribe(messageId, listener);

		// Subscribe to messages for the given id
		messageSubscriber.subscribe(messageId);
		return listeners;
	}

	@Override
	public int unsubscribe(String messageId, IMessageListener listener) {
		int listeners = listenerSubscriber.unsubscribe(messageId, listener);
		if (listeners == 0) {
			messageSubscriber.unsubscribe(messageId);
		}
		return listeners;
	}

	@Override
	public void unsubscribeAll(String messageId) {
		listenerSubscriber.unsubscribeAll(messageId);
		messageSubscriber.unsubscribe(messageId);
	}

	@Override
	public Set<String> unsubscribeAll() {
		Set<String> messageIds = listenerSubscriber.unsubscribeAll();;
		messageSubscriber.unsubscribeAll(messageIds);
		return messageIds;
	}

	@Override
	public Collection<IMessageListener> getListeners(String messageId) {
		// Delegate
		return listenerSubscriber.getListeners(messageId);
	}

	@Override
	public Set<String> unsubscribeAll(IMessageListener listener) {
		Set<String> messageIds = listenerSubscriber.unsubscribeAll(listener);
		for (String messageId : messageIds) {
			messageSubscriber.unsubscribe(messageId);
		}
		return messageIds;
	}

}
