package com.javabi.message.subscriber.server;

import java.util.Collection;

public interface IMessageSubscriber {

	void subscribe(String messageId);

	void unsubscribe(String messageId);

	void unsubscribeAll(Collection<String> messageIds);

}
