package com.javabi.message.subscriber.listener;

import java.util.Collection;
import java.util.Set;

import com.javabi.message.IMessageListener;

public interface IListenerSubscriber {

	int subscribe(String messageId, IMessageListener listener);

	int unsubscribe(String messageId, IMessageListener listener);

	Set<String> unsubscribeAll(IMessageListener listener);

	void unsubscribeAll(String messageId);

	Collection<IMessageListener> getListeners(String messageId);

	Set<String> unsubscribeAll();

}
