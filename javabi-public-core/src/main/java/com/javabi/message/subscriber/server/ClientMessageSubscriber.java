package com.javabi.message.subscriber.server;

import static com.javabi.common.lang.Variables.notEmpty;
import static com.javabi.common.lang.Variables.notNull;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.concurrent.thread.ILoopingThreadControl;
import com.javabi.common.concurrent.thread.LoopingThreadControl;
import com.javabi.common.service.IShutdownable;
import com.javabi.common.service.IStartupable;
import com.javabi.message.IMessage;
import com.javabi.message.IMessageListener;
import com.javabi.message.connection.IMessageConnection;
import com.javabi.message.type.ITypedMessage;
import com.javabi.message.type.MessageType;

public class ClientMessageSubscriber implements IClientMessageSubscriber, IStartupable, IShutdownable {

	private static final Logger log = LoggerFactory.getLogger(ClientMessageSubscriber.class);

	private final Set<IMessageListener> listenerSet = new CopyOnWriteArraySet<IMessageListener>();
	private final IMessageConnection connection;
	private final ILoopingThreadControl thread;
	private final String identity;

	public ClientMessageSubscriber(String identity, String threadName, IMessageConnection connection) {
		notNull("connection", connection);
		notEmpty("identity", identity);

		this.connection = connection;
		this.identity = identity;

		// Start!
		thread = new LoopingThreadControl(threadName, new Subscriber());
	}

	@Override
	public void startup() {
		thread.startup();

		// Identify
		connection.getWriter().writeIdentify(identity);
	}

	@Override
	public void shutdown() {
		thread.shutdown();
	}

	@Override
	public void subscribe(String messageId) {
		connection.getWriter().writeSubscribe(messageId);
	}

	@Override
	public void unsubscribe(String messageId) {
		connection.getWriter().writeUnsubscribe(messageId);
	}

	@Override
	public void unsubscribeAll(Collection<String> messageIds) {
		for (String messageId : messageIds) {
			unsubscribe(messageId);
		}
	}

	@Override
	public void addListener(IMessageListener listener) {
		this.listenerSet.add(listener);
	}

	@Override
	public void removeListener(IMessageListener listener) {
		this.listenerSet.remove(listener);
	}

	@Override
	public void notify(IMessage message) {
		log.debug("[Received Message] " + message);
		for (IMessageListener listener : listenerSet) {
			listener.notify(message);
		}
	}

	private void readMessage() {
		ITypedMessage message = connection.getReader().read();
		MessageType type = message.getType();
		switch (type) {
			case PUBLISH:
				notify((IMessage) message.getContent());
				break;
			default:
				throw new IllegalStateException("Message type not supported: " + type);
		}
	}

	private void subscriberReadMessage() {
		try {
			readMessage();
		} catch (Throwable t) {
			log.error("Fatal Error Reading Subscribed Message", t);
			throw Throwables.propagate(t);
		}
	}

	private class Subscriber implements Runnable {

		@Override
		public void run() {
			subscriberReadMessage();
		}
	}
}
