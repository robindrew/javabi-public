package com.javabi.message;

import java.io.IOException;

import com.javabi.common.io.data.IDataReader;
import com.javabi.common.io.data.IDataWriter;

public interface IMessageSerializer {

	void writeObject(IDataWriter writer, IMessage message) throws IOException;

	IMessage readObject(IDataReader reader) throws IOException;

}
