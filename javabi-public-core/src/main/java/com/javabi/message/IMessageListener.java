package com.javabi.message;

public interface IMessageListener {

	void notify(IMessage message);

}
