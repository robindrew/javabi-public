package com.javabi.message.type;

public enum MessageType {

	PING, PUBLISH, SUBSCRIBE, UNSUBSCRIBE, DISCONNECT, IDENTIFY;

}
