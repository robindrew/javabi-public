package com.javabi.message.type;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.concurrent.thread.ILoopingThreadControl;
import com.javabi.common.concurrent.thread.LoopingThreadControl;
import com.javabi.common.service.IShutdownable;
import com.javabi.common.service.ShutdownException;
import com.javabi.message.connection.IMessageWriter;

public class TypedMessagePublisher implements ITypedMessagePublisher, IShutdownable {

	private static final Logger log = LoggerFactory.getLogger(TypedMessagePublisher.class);

	private final IMessageWriter writer;
	private final BlockingQueue<ITypedMessage> publishMessageQueue = new LinkedBlockingQueue<ITypedMessage>();
	private final ILoopingThreadControl thread;

	public TypedMessagePublisher(IMessageWriter writer, long connectionNumber) {
		if (writer == null) {
			throw new NullPointerException("writer");
		}
		this.writer = writer;

		// Start!
		String name = "TypedMessagePublisher-" + connectionNumber;
		thread = new LoopingThreadControl(name, new Publisher());
		thread.startup();
	}

	@Override
	public void shutdown() {
		thread.interrupt();
		thread.shutdown();
	}

	@Override
	public void publish(ITypedMessage message) {
		try {
			publishMessageQueue.put(message);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	private void writeMessages() throws Exception {
		List<ITypedMessage> list = new ArrayList<ITypedMessage>();
		// take() needs interrupting to shutdown publisher
		ITypedMessage first = publishMessageQueue.take();
		list.add(first);
		if (!publishMessageQueue.isEmpty()) {
			publishMessageQueue.drainTo(list);
		}

		log.debug("[Publish Messages] " + list.size());
		for (ITypedMessage message : list) {
			log.debug("[Publish Message] " + message);
		}
		writer.write(list);
	}

	private void publisherWriteMessages() {
		try {
			writeMessages();

		} catch (InterruptedException ie) {
			// We interrupt take() to shutdown thread
			throw new ShutdownException();

		} catch (Throwable t) {
			t = Throwables.getRootCause(t);

			// Unexpected exception
			log.error("Fatal Error Publishing Messages", t);
			throw new ShutdownException();
		}
	}

	private class Publisher implements Runnable {

		@Override
		public void run() {
			publisherWriteMessages();
		}

	}

	@Override
	public boolean isRunning() {
		return thread.isRunning();
	}

}
