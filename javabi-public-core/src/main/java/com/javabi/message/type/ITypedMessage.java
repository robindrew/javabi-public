package com.javabi.message.type;

public interface ITypedMessage {

	MessageType getType();

	Object getContent();

}
