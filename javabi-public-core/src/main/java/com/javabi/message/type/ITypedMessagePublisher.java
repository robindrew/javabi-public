package com.javabi.message.type;

import com.javabi.common.service.IShutdownable;

public interface ITypedMessagePublisher extends IShutdownable {

	void publish(ITypedMessage message);

	boolean isRunning();
	
}
