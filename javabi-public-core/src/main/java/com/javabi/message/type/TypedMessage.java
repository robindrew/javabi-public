package com.javabi.message.type;

public class TypedMessage implements ITypedMessage {

	private final MessageType type;
	private final Object content;

	public TypedMessage(MessageType type, Object content) {
		if (type == null) {
			throw new NullPointerException("type");
		}
		this.type = type;
		this.content = content;
	}

	public TypedMessage(MessageType type) {
		if (type == null) {
			throw new NullPointerException("type");
		}
		this.type = type;
		this.content = null;
	}

	@Override
	public MessageType getType() {
		return type;
	}

	@Override
	public Object getContent() {
		return content;
	}

	@Override
	public String toString() {
		if (content == null) {
			return type.toString();
		}
		return type + "/" + content;
	}

}
