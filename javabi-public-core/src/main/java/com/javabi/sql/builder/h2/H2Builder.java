package com.javabi.sql.builder.h2;

import com.javabi.sql.builder.ISQLBuilder;
import com.javabi.sql.builder.SQLBuilder;

public class H2Builder extends SQLBuilder {

	@Override
	public ISQLBuilder value(String value) {
		if (value == null) {
			return nul();
		}
		value = escape(value);
		getSQL().append('\'');
		getSQL().append(value);
		getSQL().append('\'');
		return this;
	}

	@Override
	public ISQLBuilder columnName(String columnName) {
		getSQL().append(columnName + "_");
		return this;
	}

	private String escape(String value) {

		// Format ...
		// TODO: Improved format
		if (value.indexOf('\'') != -1) {
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < value.length(); i++) {
				char c = value.charAt(i);
				if (c == '\'') {
					builder.append('\'');
				}
				builder.append(c);
			}
			value = builder.toString();
		}
		return value;
	}

}
