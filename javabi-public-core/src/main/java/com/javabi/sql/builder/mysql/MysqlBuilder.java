package com.javabi.sql.builder.mysql;

import org.apache.commons.codec.binary.Hex;

import com.javabi.sql.builder.ISQLBuilder;
import com.javabi.sql.builder.SQLBuilder;

public class MysqlBuilder extends SQLBuilder {

	@Override
	public MysqlBuilder tableName(String databaseName, String tableName) {
		if (databaseName != null) {
			databaseName(databaseName);
			append('.');
		}
		tableName(tableName);
		return this;
	}

	@Override
	public MysqlBuilder databaseName(String databaseName) {
		if (databaseName.indexOf('.') != -1) {
			throw new IllegalStateException("databaseName is invalid: '" + databaseName + "'");
		}
		append('`');
		append(databaseName);
		append('`');
		return this;
	}

	@Override
	public MysqlBuilder tableName(String tableName) {
		if (tableName.indexOf('.') != -1) {
			throw new IllegalStateException("tableName is invalid: '" + tableName + "'");
		}
		append('`');
		append(tableName);
		append('`');
		return this;
	}

	public MysqlBuilder use(String databaseName) {
		append("USE `");
		append(databaseName);
		append('`');
		return this;
	}

	public MysqlBuilder showTables() {
		append("SHOW TABLES");
		return this;
	}

	public MysqlBuilder showDatabases() {
		append("SHOW DATABASES");
		return this;
	}

	public MysqlBuilder showColumns(String databaseName, String tableName) {
		append("SHOW COLUMNS FROM ");
		tableName(databaseName, tableName);
		return this;
	}

	public MysqlBuilder showProcessList(boolean full) {
		append("SHOW ");
		if (full) {
			append("FULL ");
		}
		append("PROCESSLIST");
		return this;
	}

	@Override
	public MysqlBuilder columnName(String columnName) {
		append('`');
		append(columnName);
		append('`');
		return this;
	}

	@Override
	public ISQLBuilder value(String value) {
		if (value == null) {
			return nul();
		}
		value = escape(value);
		getSQL().append('\'');
		getSQL().append(value);
		getSQL().append('\'');
		return this;
	}

	@Override
	public ISQLBuilder value(byte[] value) {
		if (value.length == 0) {
			getSQL().append("''");
		} else {
			char[] hex = Hex.encodeHex(value);
			getSQL().append('0').append('x');
			getSQL().append(hex);
		}
		return this;
	}

	public static String escape(String value) {

		// Format ...
		// TODO: Improved format
		if (value.indexOf('\'') != -1 || value.indexOf('\\') != -1) {
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < value.length(); i++) {
				char c = value.charAt(i);
				if (c == '\\' || c == '\'') {
					builder.append('\\');
				}
				builder.append(c);
			}
			value = builder.toString();
		}
		return value;
	}

	@Override
	public ISQLBuilder dropTable(String databaseName, String tableName) {
		append("DROP TABLE IF EXISTS ");
		tableName(databaseName, tableName);
		return this;
	}

	@Override
	public ISQLBuilder createTable(String databaseName, String tableName) {
		append("CREATE TABLE IF NOT EXISTS ");
		tableName(databaseName, tableName);
		return this;
	}

	public MysqlBuilder limit(int offset, int rows) {
		if (offset < 0) {
			throw new IllegalArgumentException("offset=" + offset);
		}
		if (rows < 1) {
			throw new IllegalArgumentException("rows=" + rows);
		}
		append(" LIMIT ");
		append(offset);
		append(',');
		append(rows);
		return this;
	}

	public MysqlBuilder insert(boolean ignore) {
		if (ignore) {
			append("INSERT IGNORE INTO ");
		} else {
			append("INSERT INTO ");
		}
		return this;
	}

	public MysqlBuilder insert(boolean ignore, String databaseName, String tableName) {
		insert(ignore);
		tableName(databaseName, tableName);
		return this;
	}

}
