package com.javabi.sql.builder;

import java.sql.Date;
import java.sql.Types;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

public class SQLBuilder implements ISQLBuilder {

	private final StringBuilder sql = new StringBuilder();

	@Override
	public int length() {
		return sql.length();
	}

	@Override
	public char charAt(int index) {
		return sql.charAt(index);
	}

	@Override
	public CharSequence subSequence(int start, int end) {
		return sql.subSequence(start, end);
	}

	@Override
	public final StringBuilder getSQL() {
		return sql;
	}

	@Override
	public final String toString() {
		return sql.toString();
	}

	@Override
	public ISQLBuilder databaseName(String databaseName) {
		getSQL().append(databaseName);
		return this;
	}

	@Override
	public ISQLBuilder columnName(String columnName) {
		getSQL().append(columnName);
		return this;
	}

	@Override
	public ISQLBuilder columnNames(Collection<String> columnNames) {
		boolean comma = false;
		for (String columnName : columnNames) {
			if (comma) {
				getSQL().append(',');
			}
			comma = true;
			columnName(columnName);
		}
		return this;
	}

	@Override
	public ISQLBuilder tableName(String tableName) {
		return append(tableName);
	}

	@Override
	public ISQLBuilder tableName(String databaseName, String tableName) {
		if (databaseName != null) {
			databaseName(databaseName);
			append('.');
		}
		tableName(tableName);
		return this;
	}

	@Override
	public ISQLBuilder append(String value) {
		getSQL().append(value);
		return this;
	}

	@Override
	public ISQLBuilder append(char value) {
		getSQL().append(value);
		return this;
	}

	@Override
	public ISQLBuilder append(int value) {
		getSQL().append(value);
		return this;
	}

	@Override
	public ISQLBuilder type(int type) {
		switch (type) {
			case Types.ARRAY:
				return append("ARRAY");
			case Types.BIGINT:
				return append("BIGINT");
			case Types.BINARY:
				return append("BINARY");
			case Types.BIT:
				return append("BIT");
			case Types.BLOB:
				return append("BLOB");
			case Types.BOOLEAN:
				return append("BOOLEAN");
			case Types.CHAR:
				return append("CHAR");
			case Types.CLOB:
				return append("CLOB");
			case Types.DATALINK:
				return append("DATALINK");
			case Types.DATE:
				return append("DATE");
			case Types.DECIMAL:
				return append("DECIMAL");
			case Types.DISTINCT:
				return append("DISTINCT");
			case Types.DOUBLE:
				return append("DOUBLE");
			case Types.FLOAT:
				return append("FLOAT");
			case Types.INTEGER:
				return append("INTEGER");
			case Types.JAVA_OBJECT:
				return append("JAVA_OBJECT");
			case Types.LONGNVARCHAR:
				return append("LONGNVARCHAR");
			case Types.LONGVARCHAR:
				return append("LONGVARCHAR");
			case Types.LONGVARBINARY:
				return append("LONGVARBINARY");
			case Types.NCHAR:
				return append("NCHAR");
			case Types.NCLOB:
				return append("NCLOB");
			case Types.NULL:
				return append("NULL");
			case Types.NUMERIC:
				return append("NUMERIC");
			case Types.NVARCHAR:
				return append("NVARCHAR");
			case Types.OTHER:
				return append("OTHER");
			case Types.REAL:
				return append("REAL");
			case Types.REF:
				return append("REF");
			case Types.ROWID:
				return append("ROWID");
			case Types.SMALLINT:
				return append("SMALLINT");
			case Types.SQLXML:
				return append("SQLXML");
			case Types.STRUCT:
				return append("STRUCT");
			case Types.TIME:
				return append("TIME");
			case Types.TIMESTAMP:
				return append("TIMESTAMP");
			case Types.TINYINT:
				return append("TINYINT");
			case Types.VARBINARY:
				return append("VARBINARY");
			case Types.VARCHAR:
				return append("VARCHAR");
			default:
				throw new IllegalArgumentException("SQL type not supported: " + type);
		}
	}

	@Override
	public ISQLBuilder value(boolean value) {
		return append(value ? "TRUE" : "FALSE");
	}

	@Override
	public ISQLBuilder value(byte value) {
		getSQL().append(value);
		return this;
	}

	@Override
	public ISQLBuilder value(short value) {
		getSQL().append(value);
		return this;
	}

	@Override
	public ISQLBuilder value(long value) {
		getSQL().append(value);
		return this;
	}

	@Override
	public ISQLBuilder value(int value) {
		getSQL().append(value);
		return this;
	}

	@Override
	public ISQLBuilder value(char value) {
		getSQL().append('\'');
		getSQL().append(value);
		getSQL().append('\'');
		return this;
	}

	@Override
	public ISQLBuilder value(float value) {
		getSQL().append(value);
		return this;
	}

	@Override
	public ISQLBuilder value(double value) {
		getSQL().append(value);
		return this;
	}

	@Override
	public ISQLBuilder value(byte[] value) {
		if (value == null) {
			return nul();
		}
		throw new UnsupportedOperationException();
	}

	@Override
	public ISQLBuilder value(String value) {
		if (value == null) {
			return nul();
		}
		getSQL().append('\'');
		getSQL().append(value);
		getSQL().append('\'');
		return this;
	}

	@Override
	public ISQLBuilder value(Object value) {
		if (value == null) {
			return nul();
		}
		if (value instanceof String) {
			return value((String) value);
		}
		if (value instanceof byte[]) {
			return value((byte[]) value);
		}
		if (value instanceof Date) {
			return value((Date) value);
		}
		if (value instanceof Boolean) {
			return value(((Boolean) value).booleanValue());
		}
		if (value instanceof Integer) {
			return value(((Integer) value).intValue());
		}
		if (value instanceof Long) {
			return value(((Long) value).intValue());
		}
		if (value instanceof Byte) {
			return value(((Byte) value).intValue());
		}
		if (value instanceof Short) {
			return value(((Short) value).intValue());
		}
		if (value instanceof Float) {
			return value(((Float) value).intValue());
		}
		if (value instanceof Double) {
			return value(((Double) value).intValue());
		}
		return value(value.toString());
	}

	@Override
	public ISQLBuilder value(Date value) {
		if (value == null) {
			return nul();
		}
		throw new UnsupportedOperationException();
	}

	@Override
	public ISQLBuilder values(Collection<Object> values) {
		boolean comma = false;
		for (Object value : values) {
			if (comma) {
				getSQL().append(',');
			}
			comma = true;
			value(value);
		}
		return this;
	}

	@Override
	public ISQLBuilder assign(String name, boolean value) {
		columnName(name);
		append('=');
		value(value);
		return this;
	}

	@Override
	public ISQLBuilder assign(String name, byte value) {
		columnName(name);
		append('=');
		value(value);
		return this;
	}

	@Override
	public ISQLBuilder assign(String name, short value) {
		columnName(name);
		append('=');
		value(value);
		return this;
	}

	@Override
	public ISQLBuilder assign(String name, long value) {
		columnName(name);
		append('=');
		value(value);
		return this;
	}

	@Override
	public ISQLBuilder assign(String name, int value) {
		columnName(name);
		append('=');
		value(value);
		return this;
	}

	@Override
	public ISQLBuilder assign(String name, char value) {
		columnName(name);
		append('=');
		value(value);
		return this;
	}

	@Override
	public ISQLBuilder assign(String name, float value) {
		columnName(name);
		append('=');
		value(value);
		return this;
	}

	@Override
	public ISQLBuilder assign(String name, double value) {
		columnName(name);
		append('=');
		value(value);
		return this;
	}

	@Override
	public ISQLBuilder assign(String name, byte[] value) {
		columnName(name);
		append('=');
		value(value);
		return this;
	}

	@Override
	public ISQLBuilder assign(String name, String value) {
		columnName(name);
		append('=');
		value(value);
		return this;
	}

	@Override
	public ISQLBuilder assign(String name, Object value) {
		columnName(name);
		append('=');
		value(value);
		return this;
	}

	@Override
	public ISQLBuilder test(String name, String test, Object value) {
		if (test.isEmpty()) {
			throw new IllegalArgumentException("test is empty");
		}
		columnName(name);
		append(test);
		value(value);
		return this;
	}

	@Override
	public ISQLBuilder assign(Map<String, Object> columnValueMap) {
		if (columnValueMap.isEmpty()) {
			throw new IllegalArgumentException("columnValueMap is empty");
		}
		return assign(columnValueMap, ",");
	}

	@Override
	public ISQLBuilder assign(Map<String, Object> columnValueMap, String separator) {
		boolean comma = false;
		for (Entry<String, Object> entry : columnValueMap.entrySet()) {
			if (comma) {
				append(separator);
			}
			comma = true;
			columnName(entry.getKey());
			append('=');
			value(entry.getValue());
		}
		return this;
	}

	@Override
	public ISQLBuilder nul() {
		return append("NULL");
	}

	@Override
	public ISQLBuilder not() {
		return append(" NOT ");
	}

	@Override
	public ISQLBuilder where() {
		return append(" WHERE ");
	}

	@Override
	public ISQLBuilder and() {
		return append(" AND ");
	}

	@Override
	public ISQLBuilder or() {
		return append(" OR ");
	}

	@Override
	public ISQLBuilder createTable() {
		return append("CREATE TABLE ");
	}

	@Override
	public ISQLBuilder dropTable() {
		return append("DROP TABLE ");
	}

	@Override
	public ISQLBuilder select() {
		return append("SELECT ");
	}

	@Override
	public ISQLBuilder insert() {
		return append("INSERT INTO ");
	}

	@Override
	public ISQLBuilder values() {
		return append(" VALUES(");
	}

	@Override
	public ISQLBuilder update() {
		return append("UPDATE ");
	}

	@Override
	public ISQLBuilder delete() {
		return append("DELETE FROM ");
	}

	@Override
	public ISQLBuilder from() {
		return append(" FROM ");
	}

	@Override
	public ISQLBuilder from(String databaseName, String tableName) {
		from();
		tableName(databaseName, tableName);
		return this;
	}

	@Override
	public ISQLBuilder set() {
		return append(" SET ");
	}

	@Override
	public ISQLBuilder key(SQLKeyType type) {
		switch (type) {
			case PRIMARY:
				return append("PRIMARY KEY");
			case UNIQUE:
				return append("UNIQUE KEY");
			case INDEX:
				return append("INDEX KEY");
			default:
				throw new IllegalArgumentException("type not supported: " + type);
		}
	}

	@Override
	public ISQLBuilder selectCount(String databaseName, String tableName) {
		append("SELECT COUNT(*) FROM ");
		tableName(databaseName, tableName);
		return this;
	}

	@Override
	public ISQLBuilder selectCountWhere(String databaseName, String tableName, Map<String, Object> whereMap) {
		if (whereMap.isEmpty()) {
			throw new IllegalArgumentException("whereMap is empty");
		}
		append("SELECT COUNT(*) FROM ");
		tableName(databaseName, tableName);
		where();
		assign(whereMap, " AND ");
		return this;
	}

	@Override
	public ISQLBuilder selectAll(String databaseName, String tableName) {
		append("SELECT * FROM ");
		tableName(databaseName, tableName);
		return this;
	}

	@Override
	public ISQLBuilder selectAll(String databaseName, String tableName, Collection<String> columnNames) {
		if (columnNames.isEmpty()) {
			throw new IllegalArgumentException("columnNames is empty");
		}
		select();
		columnNames(columnNames);
		from();
		tableName(databaseName, tableName);
		return this;
	}

	@Override
	public ISQLBuilder selectAllWhere(String databaseName, String tableName, Map<String, Object> whereMap) {
		if (whereMap.isEmpty()) {
			throw new IllegalArgumentException("whereMap is empty");
		}
		append("SELECT * FROM ");
		tableName(databaseName, tableName);
		where();
		assign(whereMap, " AND ");
		return this;
	}

	@Override
	public ISQLBuilder selectAllWhere(String databaseName, String tableName, Collection<String> columnNames, Map<String, Object> whereMap) {
		if (columnNames.isEmpty()) {
			throw new IllegalArgumentException("columnNames is empty");
		}
		if (whereMap.isEmpty()) {
			throw new IllegalArgumentException("whereMap is empty");
		}
		select();
		columnNames(columnNames);
		from();
		tableName(databaseName, tableName);
		where();
		assign(whereMap, " AND ");
		return this;
	}

	@Override
	public ISQLBuilder insert(String databaseName, String tableName, Map<String, Object> columnValueMap) {
		if (columnValueMap.isEmpty()) {
			throw new IllegalArgumentException("columnValueMap is empty");
		}
		insert();
		tableName(databaseName, tableName);
		append(' ');
		columnNames(columnValueMap.keySet());
		append(" VALUES(");
		values(columnValueMap.values());
		append(')');
		return this;
	}

	@Override
	public ISQLBuilder insert(String databaseName, String tableName, Collection<Object> values) {
		if (values.isEmpty()) {
			throw new IllegalArgumentException("values is empty");
		}
		insert();
		tableName(databaseName, tableName);
		append(" VALUES(");
		values(values);
		append(')');
		return this;
	}

	@Override
	public ISQLBuilder insert(String databaseName, String tableName) {
		insert();
		tableName(databaseName, tableName);
		append(" VALUES");
		return this;
	}

	@Override
	public ISQLBuilder update(String databaseName, String tableName) {
		update();
		tableName(databaseName, tableName);
		set();
		return this;
	}

	@Override
	public ISQLBuilder update(String databaseName, String tableName, Map<String, Object> columnValueMap) {
		if (columnValueMap.isEmpty()) {
			throw new IllegalArgumentException("columnValueMap is empty");
		}
		update();
		tableName(databaseName, tableName);
		set();
		assign(columnValueMap);
		return this;
	}

	@Override
	public ISQLBuilder update(String databaseName, String tableName, Map<String, Object> columnValueMap, Map<String, Object> whereMap) {
		if (columnValueMap.isEmpty()) {
			throw new IllegalArgumentException("columnValueMap is empty");
		}
		if (whereMap.isEmpty()) {
			throw new IllegalArgumentException("whereMap is empty");
		}
		update();
		tableName(databaseName, tableName);
		set();
		assign(columnValueMap);
		where();
		assign(whereMap, " AND ");
		return this;
	}

	@Override
	public ISQLBuilder deleteAll(String databaseName, String tableName) {
		delete();
		tableName(databaseName, tableName);
		return this;
	}

	@Override
	public ISQLBuilder deleteAllWhere(String databaseName, String tableName, Map<String, Object> whereMap) {
		if (whereMap.isEmpty()) {
			throw new IllegalArgumentException("whereMap is empty");
		}
		delete();
		tableName(databaseName, tableName);
		where();
		assign(whereMap, " AND ");
		return this;
	}

	@Override
	public ISQLBuilder dropTable(String databaseName, String tableName) {
		append("DROP TABLE ");
		tableName(databaseName, tableName);
		return this;
	}

	@Override
	public ISQLBuilder createTable(String databaseName, String tableName) {
		append("CREATE TABLE ");
		tableName(databaseName, tableName);
		return this;
	}

	@Override
	public ISQLBuilder max(String columnName) {
		append("MAX(");
		columnName(columnName);
		append(")");
		return this;
	}

	@Override
	public ISQLBuilder min(String columnName) {
		append("MIN(");
		columnName(columnName);
		append(")");
		return this;
	}

	@Override
	public ISQLBuilder orderBy() {
		append(" ORDER BY ");
		return this;
	}

	@Override
	public ISQLBuilder desc() {
		append(" DESC ");
		return this;
	}

	@Override
	public ISQLBuilder asc() {
		append(" ASC ");
		return this;
	}

	@Override
	public ISQLBuilder orderByAsc(String columnName) {
		orderBy();
		columnName(columnName);
		asc();
		return this;
	}

	@Override
	public ISQLBuilder orderByDesc(String columnName) {
		orderBy();
		columnName(columnName);
		desc();
		return this;
	}

}
