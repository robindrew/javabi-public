package com.javabi.sql.builder.hsqldb;

import org.apache.commons.codec.binary.Base64;

import com.javabi.sql.builder.ISQLBuilder;
import com.javabi.sql.builder.SQLBuilder;

public class HsqldbBuilder extends SQLBuilder {

	@Override
	public ISQLBuilder columnName(String columnName) {
		append('\"');
		append(columnName);
		append('\"');
		return this;
	}

	@Override
	public ISQLBuilder value(byte[] value) {
		getSQL().append('\'');
		if (value.length > 0) {
			String text = Base64.encodeBase64String(value);
			getSQL().append(text);
		}
		getSQL().append('\'');
		return this;
	}
}
