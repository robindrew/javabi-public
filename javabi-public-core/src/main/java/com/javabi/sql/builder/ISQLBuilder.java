package com.javabi.sql.builder;

import java.sql.Date;
import java.util.Collection;
import java.util.Map;

/**
 * An SQL Builder.
 */
public interface ISQLBuilder extends CharSequence {

	@Override
	String toString();

	StringBuilder getSQL();

	ISQLBuilder databaseName(String databaseName);

	ISQLBuilder tableName(String tableName);

	ISQLBuilder tableName(String databaseName, String tableName);

	ISQLBuilder columnName(String columnName);

	ISQLBuilder columnNames(Collection<String> columnNames);

	ISQLBuilder append(String text);

	ISQLBuilder append(char character);

	ISQLBuilder append(int number);

	ISQLBuilder type(int type);

	ISQLBuilder value(boolean value);

	ISQLBuilder value(byte value);

	ISQLBuilder value(short value);

	ISQLBuilder value(long value);

	ISQLBuilder value(int value);

	ISQLBuilder value(char value);

	ISQLBuilder value(float value);

	ISQLBuilder value(double value);

	ISQLBuilder value(byte[] value);

	ISQLBuilder value(String value);

	ISQLBuilder value(Date value);

	ISQLBuilder value(Object value);

	ISQLBuilder values(Collection<Object> values);

	ISQLBuilder assign(String name, boolean value);

	ISQLBuilder assign(String name, byte value);

	ISQLBuilder assign(String name, short value);

	ISQLBuilder assign(String name, long value);

	ISQLBuilder assign(String name, int value);

	ISQLBuilder assign(String name, char value);

	ISQLBuilder assign(String name, float value);

	ISQLBuilder assign(String name, double value);

	ISQLBuilder assign(String name, byte[] value);

	ISQLBuilder assign(String name, String value);

	ISQLBuilder assign(String name, Object value);

	ISQLBuilder test(String name, String test, Object value);

	ISQLBuilder assign(Map<String, Object> nameToValueMap);

	ISQLBuilder assign(Map<String, Object> nameToValueMap, String separator) ;

	ISQLBuilder key(SQLKeyType type);

	ISQLBuilder nul();

	ISQLBuilder not();

	ISQLBuilder where();

	ISQLBuilder from();

	ISQLBuilder from(String databaseName, String tableName);

	ISQLBuilder set();

	ISQLBuilder and();

	ISQLBuilder or();

	ISQLBuilder createTable();

	ISQLBuilder dropTable();

	ISQLBuilder select();

	ISQLBuilder insert();

	ISQLBuilder values();

	ISQLBuilder update();

	ISQLBuilder delete();

	ISQLBuilder createTable(String databaseName, String tableName);

	ISQLBuilder selectCount(String databaseName, String tableName);

	ISQLBuilder selectCountWhere(String databaseName, String tableName, Map<String, Object> whereMap);

	ISQLBuilder selectAll(String databaseName, String tableName);

	ISQLBuilder selectAll(String databaseName, String tableName, Collection<String> columnNames);

	ISQLBuilder selectAllWhere(String databaseName, String tableName, Map<String, Object> whereMap);

	ISQLBuilder selectAllWhere(String databaseName, String tableName, Collection<String> columnNames, Map<String, Object> whereMap);

	ISQLBuilder insert(String databaseName, String tableName);

	ISQLBuilder insert(String databaseName, String tableName, Map<String, Object> columnValueMap);

	ISQLBuilder insert(String databaseName, String tableName, Collection<Object> values);

	ISQLBuilder update(String databaseName, String tableName);

	ISQLBuilder update(String databaseName, String tableName, Map<String, Object> columnValueMap);

	ISQLBuilder update(String databaseName, String tableName, Map<String, Object> columnValueMap, Map<String, Object> whereMap);

	ISQLBuilder deleteAll(String databaseName, String tableName);

	ISQLBuilder deleteAllWhere(String databaseName, String tableName, Map<String, Object> whereMap);

	ISQLBuilder dropTable(String databaseName, String tableName);

	ISQLBuilder max(String columnName);

	ISQLBuilder min(String columnName);

	ISQLBuilder orderBy();

	ISQLBuilder asc();

	ISQLBuilder desc();

	ISQLBuilder orderByAsc(String columnName);

	ISQLBuilder orderByDesc(String columnName);
}
