package com.javabi.sql.builder;

/**
 * The Key Type.
 */
public enum SQLKeyType {

	/** The Primary Key. */
	PRIMARY,
	/** The Unique Key. */
	UNIQUE,
	/** The Index Key. */
	INDEX;

}
