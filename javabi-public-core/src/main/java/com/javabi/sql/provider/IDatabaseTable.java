package com.javabi.sql.provider;

public interface IDatabaseTable {

	String getTableName();

	String getDatabaseName();

	void setDatabaseName(String name);

	void setTableName(String name);

}
