package com.javabi.sql.provider.mysql;

import java.sql.Driver;

public class MysqlDriver {

	public static Class<? extends Driver> get() {
		return com.mysql.jdbc.Driver.class;
	}
}
