package com.javabi.sql.provider.hsqldb;

import com.google.common.base.Supplier;
import com.javabi.common.lang.Java;

public class HsqldbUrlBuilder implements Supplier<String> {

	private String type = "mem";
	private String database = null;

	public HsqldbUrlBuilder setDatabase(String database) {
		if (database.isEmpty()) {
			throw new IllegalArgumentException("database is empty");
		}
		this.database = database;
		return this;
	}

	public HsqldbUrlBuilder setType(String type) {
		if (type.isEmpty()) {
			throw new IllegalArgumentException("type is empty");
		}
		this.type = type;
		return this;
	}

	@Override
	public String get() {
		if (type == null) {
			throw new IllegalStateException("type not set");
		}
		if (database == null) {
			throw new IllegalStateException("database not set");
		}

		// Build URL
		StringBuilder url = new StringBuilder();
		url.append("jdbc:hsqldb");
		url.append(':').append(type);
		url.append(':').append(database);
		return url.toString();
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

}
