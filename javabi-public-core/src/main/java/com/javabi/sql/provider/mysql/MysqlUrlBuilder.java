package com.javabi.sql.provider.mysql;

import static com.javabi.common.lang.Variables.min;
import static com.javabi.common.lang.Variables.notEmpty;

import com.google.common.base.Supplier;

public class MysqlUrlBuilder implements Supplier<String> {

	private String host = "127.0.0.1";
	private int port = 3306;
	private String database = "mysql";
	private boolean jdbcCompliantTruncation = false;

	public void setJdbcCompliantTruncation(boolean jdbcCompliantTruncation) {
		this.jdbcCompliantTruncation = jdbcCompliantTruncation;
	}

	public void setHost(String host) {
		this.host = notEmpty("host", host);
	}

	public void setPort(int port) {
		this.port = min("port", port, 1);
	}

	public void setDatabase(String database) {
		this.database = notEmpty("database", database);
	}

	@Override
	public String toString() {
		return get();
	}

	@Override
	public String get() {
		StringBuilder url = new StringBuilder();
		url.append("jdbc:mysql://").append(host).append(':').append(port);
		url.append("/").append(database);
		url.append("?jdbcCompliantTruncation=").append(jdbcCompliantTruncation);
		return url.toString();
	}

}
