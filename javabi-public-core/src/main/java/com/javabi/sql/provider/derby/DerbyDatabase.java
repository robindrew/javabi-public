package com.javabi.sql.provider.derby;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Set;
import java.util.TreeSet;

import com.javabi.sql.resultset.ResultSets;

public class DerbyDatabase {

	private static final String TABLE_SCHEM = "TABLE_SCHEM";
	private static final String TABLE_SCHEM_APP = "APP";

	private static final String TABLE_NAME = "TABLE_NAME";

	public Set<String> getTableNames(Connection connection) throws SQLException {
		DatabaseMetaData meta = connection.getMetaData();
		Set<String> list = new TreeSet<String>();
		try (ResultSet set = meta.getTables(null, null, null, null)) {
			ResultSetMetaData data = set.getMetaData();
			int schemIndex = ResultSets.indexOfColumnName(data, TABLE_SCHEM);
			int nameIndex = ResultSets.indexOfColumnName(data, TABLE_NAME);
			while (set.next()) {
				String schem = set.getString(schemIndex);
				if (TABLE_SCHEM_APP.equals(schem)) {
					String name = set.getString(nameIndex);
					list.add(name);
				}
			}
		}
		return list;
	}

}
