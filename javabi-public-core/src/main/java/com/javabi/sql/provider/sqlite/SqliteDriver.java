package com.javabi.sql.provider.sqlite;

import java.sql.Driver;

import org.sqlite.JDBC;

public class SqliteDriver {

	public static Class<? extends Driver> get() {
		return JDBC.class;
	}
}
