package com.javabi.sql.provider.derby;

import java.sql.Driver;

import org.apache.derby.jdbc.EmbeddedDriver;

public class DerbyDriver {

	public static Class<? extends Driver> get() {
		return EmbeddedDriver.class;
	}
}
