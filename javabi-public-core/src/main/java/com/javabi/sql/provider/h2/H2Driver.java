package com.javabi.sql.provider.h2;

import java.sql.Driver;

public class H2Driver {

	public static Class<? extends Driver> get() {
		return org.h2.Driver.class;
	}
}
