package com.javabi.sql.provider.derby;

import java.util.List;

public interface IDerbyAdmin {

	List<String> getTableNames();

}
