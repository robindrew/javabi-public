package com.javabi.sql.provider.h2;

import java.io.File;

import com.google.common.base.Supplier;
import com.javabi.common.lang.Java;

public class H2UrlBuilder implements Supplier<String> {

	private File path = null;
	private String database = null;

	public H2UrlBuilder setPath(String path) {
		return setPath(new File(path));
	}

	public H2UrlBuilder setPath(File path) {
		if (!path.isDirectory()) {
			throw new IllegalArgumentException("path is not a directory: " + path);
		}
		this.path = path;
		return this;
	}

	public H2UrlBuilder setDatabase(String database) {
		if (database.isEmpty()) {
			throw new IllegalArgumentException("database is empty");
		}
		this.database = database;
		return this;
	}

	private String getPath() {
		String path = this.path.toString();
		if (!path.endsWith("/")) {
			return path + "/";
		}
		return path;
	}

	@Override
	public String get() {
		if (path == null) {
			throw new IllegalStateException("path not set");
		}
		if (database == null) {
			throw new IllegalStateException("database not set");
		}

		// Build URL
		StringBuilder url = new StringBuilder();
		url.append("jdbc:h2:file:");
		url.append(getPath());
		url.append(database);
		return url.toString();
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

}
