package com.javabi.sql.provider.sqlite;

import java.io.File;

import com.google.common.base.Supplier;
import com.javabi.common.lang.Java;

public class SqliteUrlBuilder implements Supplier<String> {

	private File path = null;
	private String database = null;

	public SqliteUrlBuilder setPath(String path) {
		return setPath(new File(path));
	}

	public SqliteUrlBuilder setPath(File path) {
		if (!path.isDirectory()) {
			throw new IllegalArgumentException("path is not a directory: " + path);
		}
		this.path = path;
		return this;
	}

	public SqliteUrlBuilder setDatabase(String database) {
		if (database.isEmpty()) {
			throw new IllegalArgumentException("database is empty");
		}
		this.database = database;
		return this;
	}

	private String getPath() {
		String path = this.path.toString();
		if (!path.endsWith("/")) {
			return path + "/";
		}
		return path;
	}

	@Override
	public String get() {
		if (path == null) {
			throw new IllegalStateException("path not set");
		}
		if (database == null) {
			throw new IllegalStateException("database not set");
		}

		// Build URL
		StringBuilder url = new StringBuilder();
		url.append("jdbc:sqlite:/");
		url.append(getPath());
		url.append(database);
		url.append(".db");
		return url.toString().replace('\\', '/');
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

}
