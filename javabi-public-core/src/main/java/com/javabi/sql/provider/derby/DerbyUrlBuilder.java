package com.javabi.sql.provider.derby;

import java.io.File;

import com.google.common.base.Supplier;
import com.javabi.common.lang.Java;

public class DerbyUrlBuilder implements Supplier<String> {

	private File path = null;
	private String database = null;
	private boolean create = false;

	public DerbyUrlBuilder setPath(String path) {
		return setPath(new File(path));
	}

	public DerbyUrlBuilder setPath(File path) {
		if (!path.isDirectory()) {
			throw new IllegalArgumentException("path is not a directory: " + path);
		}
		this.path = path;
		return this;
	}

	public DerbyUrlBuilder setDatabase(String database) {
		if (database.isEmpty()) {
			throw new IllegalArgumentException("database is empty");
		}
		this.database = database;
		return this;
	}

	public DerbyUrlBuilder setCreate(boolean create) {
		this.create = create;
		return this;
	}

	private String getPath() {
		if (path == null) {
			return null;
		}
		String value = path.toString();
		value = value.replace('\\', '/');
		if (!value.endsWith("/")) {
			value = value + "/";
		}
		return value;
	}

	@Override
	public String get() {
		if (database == null) {
			throw new IllegalStateException("database not set");
		}

		// Build URL
		StringBuilder url = new StringBuilder();
		url.append("jdbc:derby:");
		if (path != null) {
			url.append(getPath());
		}
		url.append(database);
		if (create) {
			url.append(";create=true");
		}
		return url.toString();
	}

	@Override
	public String toString() {
		return Java.toString(this);
	}

}
