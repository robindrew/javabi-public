package com.javabi.sql.provider.derby;

import static com.javabi.common.dependency.DependencyFactory.getDependency;

import java.util.List;

import com.javabi.sql.executor.derby.IDerbySQLExecutor;
import com.javabi.sql.resultset.list.StringListResultSetParser;

public class DerbyAdmin implements IDerbyAdmin {

	@Override
	public List<String> getTableNames() {
		IDerbySQLExecutor executor = getDependency(IDerbySQLExecutor.class);
		executor.executeQuery("SELECT tablename FROM sys.systables", new StringListResultSetParser());
		return null;
	}
}
