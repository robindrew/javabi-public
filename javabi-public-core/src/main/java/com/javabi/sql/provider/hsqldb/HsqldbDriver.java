package com.javabi.sql.provider.hsqldb;

import java.sql.Driver;

import org.hsqldb.jdbc.JDBCDriver;

public class HsqldbDriver {

	public static Class<? extends Driver> get() {
		return JDBCDriver.class;
	}
}
