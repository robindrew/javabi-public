package com.javabi.sql.resultset;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.common.collect.Table;

public class TableResultSetParser implements IResultSetParser<Table<Integer, String, Object>> {

	private final Object nullObject;

	public TableResultSetParser() {
		this("");
	}

	public TableResultSetParser(Object object) {
		if (object == null) {
			throw new NullPointerException("object is null");
		}
		this.nullObject = object;
	}

	@Override
	public Table<Integer, String, Object> parseResultSet(ResultSet set) throws SQLException {
		return ResultSets.toTable(set, nullObject);
	}

}
