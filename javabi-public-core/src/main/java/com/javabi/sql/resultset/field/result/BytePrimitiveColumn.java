package com.javabi.sql.resultset.field.result;

import java.sql.ResultSet;

import com.javabi.common.lang.reflect.field.IField;
import com.javabi.sql.resultset.field.IFieldColumn;

public class BytePrimitiveColumn implements IFieldColumn {

	@Override
	public Class<?> getType() {
		return byte.class;
	}

	@Override
	public void set(IField field, Object instance, ResultSet results, int column) throws Exception {
		byte value = results.getByte(column);
		field.setByte(instance, value);
	}

}
