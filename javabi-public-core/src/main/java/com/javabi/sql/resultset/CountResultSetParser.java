package com.javabi.sql.resultset;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CountResultSetParser implements IResultSetParser<Integer> {

	@Override
	public Integer parseResultSet(ResultSet set) throws SQLException {
		set.next();
		int count = set.getInt(1);
		return count;
	}

}
