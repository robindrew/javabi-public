package com.javabi.sql.resultset;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;

public class ResultSets {

	public static final IResultSetCellParser DEFAULT_CELL_PARSER = new DefaultResultSetCellParser();

	public static int indexOfColumnName(ResultSetMetaData metaData, String name) throws SQLException {
		int columns = metaData.getColumnCount();
		for (int i = 1; i <= columns; i++) {
			if (metaData.getColumnName(i).equals(name)) {
				return i;
			}
		}
		return -1;
	}

	public static String[] getColumnNames(ResultSetMetaData metaData) throws SQLException {
		int columns = metaData.getColumnCount();
		String[] names = new String[columns];
		for (int i = 0; i < columns; i++) {
			names[i] = metaData.getColumnName(i + 1);
		}
		return names;
	}

	public static String[] getColumnNames(ResultSet resultSet) throws SQLException {
		return getColumnNames(resultSet.getMetaData());
	}

	public static List<String> getColumnNameList(ResultSetMetaData metaData) throws SQLException {
		return Arrays.asList(getColumnNames(metaData));
	}

	public static List<String> getColumnNameList(ResultSet resultSet) throws SQLException {
		return getColumnNameList(resultSet.getMetaData());
	}

	public static List<List<Object>> toRowList(ResultSet resultSet, IResultSetCellParser cellParser) throws SQLException {
		if (resultSet == null) {
			throw new NullPointerException("resultSet");
		}
		if (cellParser == null) {
			throw new NullPointerException("cellParser");
		}

		List<List<Object>> rowList = new ArrayList<List<Object>>();
		final int columns = resultSet.getMetaData().getColumnCount();
		while (resultSet.next()) {
			Object[] row = new Object[columns];
			for (int i = 0; i < columns; i++) {
				row[i] = cellParser.parseCell(resultSet, i + 1);
			}
			rowList.add(Arrays.asList(row));
		}
		return rowList;
	}

	public static List<List<Object>> toRowList(ResultSet resultSet) throws SQLException {
		return toRowList(resultSet, DEFAULT_CELL_PARSER);
	}

	@SuppressWarnings("unchecked")
	public static Map<String, List<Object>> toColumnMap(ResultSet resultSet, IResultSetCellParser cellParser) throws SQLException {
		if (resultSet == null) {
			throw new NullPointerException("resultSet");
		}
		if (cellParser == null) {
			throw new NullPointerException("cellParser");
		}

		final int columns = resultSet.getMetaData().getColumnCount();
		List<Object>[] columnArray = new List[columns];
		for (int i = 0; i < columns; i++) {
			columnArray[i] = new ArrayList<Object>();
		}
		while (resultSet.next()) {
			for (int i = 0; i < columns; i++) {
				List<Object> column = columnArray[i];
				Object value = cellParser.parseCell(resultSet, i + 1);
				column.add(value);
			}
		}

		// Create map
		Map<String, List<Object>> columnMap = new LinkedHashMap<String, List<Object>>();
		for (int i = 0; i < columns; i++) {
			String name = resultSet.getMetaData().getColumnName(i + 1);
			List<Object> column = columnArray[i];
			columnMap.put(name, column);
		}
		return columnMap;
	}

	public static Map<String, List<Object>> toColumnMap(ResultSet resultSet) throws SQLException {
		return toColumnMap(resultSet, DEFAULT_CELL_PARSER);
	}

	public static Table<Integer, String, Object> toTable(ResultSet resultSet, Object nullObject) throws SQLException {
		if (nullObject == null) {
			throw new NullPointerException("You set nullObject to null?? seriously?");
		}
		List<String> columns = getColumnNameList(resultSet);

		Table<Integer, String, Object> table = TreeBasedTable.create();
		int row = 0;
		while (resultSet.next()) {
			for (int i = 0; i < columns.size(); i++) {
				String column = columns.get(i);
				Object value = resultSet.getObject(i + 1);
				if (value == null) {
					value = "NULL";
				}
				table.put(row, column, value);
			}
			row++;
		}

		return table;
	}
}
