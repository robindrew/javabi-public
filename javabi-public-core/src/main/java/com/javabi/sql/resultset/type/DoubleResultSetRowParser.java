package com.javabi.sql.resultset.type;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.IResultSetRowParser;

public class DoubleResultSetRowParser implements IResultSetRowParser<Double> {

	@Override
	public Double parseResultSetRow(ResultSet set) throws SQLException {
		return set.getDouble(1);
	}

}
