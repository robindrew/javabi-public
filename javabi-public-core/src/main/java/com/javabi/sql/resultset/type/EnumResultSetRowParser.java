package com.javabi.sql.resultset.type;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.IResultSetRowParser;

public class EnumResultSetRowParser<E extends Enum<E>> implements IResultSetRowParser<E> {

	private final Class<E> enumType;
	private final boolean toEnumCase;

	public EnumResultSetRowParser(Class<E> enumType, boolean toEnumCase) {
		this.enumType = enumType;
		this.toEnumCase = toEnumCase;
	}

	public EnumResultSetRowParser(Class<E> enumType) {
		this(enumType, false);
	}

	@Override
	public E parseResultSetRow(ResultSet set) throws SQLException {
		String value = set.getString(1);
		if (toEnumCase) {
			value = value.toUpperCase();
		}
		return Enum.valueOf(enumType, value);
	}

}
