package com.javabi.sql.resultset.map;

import static java.util.Collections.emptyMap;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.javabi.sql.resultset.IResultSetParser;

public class SingleRowMapParser implements IResultSetParser<Map<String, Object>> {

	@Override
	public Map<String, Object> parseResultSet(ResultSet set) throws SQLException {
		if (!set.next()) {
			return emptyMap();
		}
		ResultSetMetaData meta = set.getMetaData();
		int columns = meta.getColumnCount();

		Map<String, Object> map = new HashMap<String, Object>();
		for (int index = 1; index <= columns; index++) {
			String key = meta.getColumnName(index);
			Object value = set.getObject(index);
			map.put(key, value);
		}
		return map;
	}

}
