package com.javabi.sql.resultset.type;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.IResultSetRowParser;

public class ByteArrayResultSetRowParser implements IResultSetRowParser<byte[]> {

	@Override
	public byte[] parseResultSetRow(ResultSet set) throws SQLException {
		return set.getBytes(1);
	}

}
