package com.javabi.sql.resultset;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class ResultSetRowParser<R> implements IResultSetParser<R> {

	@Override
	public R parseResultSet(ResultSet set) throws SQLException {
		while (set.next()) {
			parseResultSetRow(set);
		}
		return getResult();
	}

	protected abstract void parseResultSetRow(ResultSet set) throws SQLException;

	protected abstract R getResult();

}
