package com.javabi.sql.resultset;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IResultSetRowParser<R> {

	R parseResultSetRow(ResultSet set) throws SQLException;

}
