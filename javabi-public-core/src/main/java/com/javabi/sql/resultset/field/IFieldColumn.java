package com.javabi.sql.resultset.field;

import java.sql.ResultSet;

import com.javabi.common.lang.reflect.field.IField;

public interface IFieldColumn {

	Class<?> getType();

	void set(IField field, Object instance, ResultSet results, int column) throws Exception;
}
