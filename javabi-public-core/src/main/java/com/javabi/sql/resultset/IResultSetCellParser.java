package com.javabi.sql.resultset;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IResultSetCellParser {

	Object parseCell(ResultSet set, int columnIndex) throws SQLException;
}
