package com.javabi.sql.resultset.row;

import java.util.LinkedHashMap;
import java.util.Map;

public class MapRow implements IRow {

	private final Map<String, Object> map;

	public MapRow(Map<String, Object> map) {
		this.map = new LinkedHashMap<String, Object>(map);
	}

	@Override
	public Object get(String columnName) {
		return map.get(columnName);
	}

	@Override
	public Map<String, Object> toMap() {
		return new LinkedHashMap<String, Object>(map);
	}

	@Override
	public String toString() {
		return map.toString();
	}

}
