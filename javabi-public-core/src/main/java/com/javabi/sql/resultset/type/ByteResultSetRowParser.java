package com.javabi.sql.resultset.type;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.IResultSetRowParser;

public class ByteResultSetRowParser implements IResultSetRowParser<Byte> {

	@Override
	public Byte parseResultSetRow(ResultSet set) throws SQLException {
		return set.getByte(1);
	}

}
