package com.javabi.sql.resultset;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CountResultSetUpdateParser extends AbstractResultSetUpdateParser<Integer> {

	@Override
	public Integer parseResultSet(ResultSet set) throws SQLException {
		return getRowCount();
	}

}
