package com.javabi.sql.resultset.type;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.IResultSetRowParser;

public class FloatResultSetRowParser implements IResultSetRowParser<Float> {

	@Override
	public Float parseResultSetRow(ResultSet set) throws SQLException {
		return set.getFloat(1);
	}

}
