package com.javabi.sql.resultset.array;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.javabi.common.primitive.PrimitiveArrayUtil;
import com.javabi.sql.resultset.ResultSetRowParser;

public class BooleanArrayResultSetParser extends ResultSetRowParser<boolean[]> {

	private final List<Boolean> list = new ArrayList<Boolean>();

	@Override
	protected void parseResultSetRow(ResultSet set) throws SQLException {
		list.add(set.getBoolean(1));
	}

	@Override
	protected boolean[] getResult() {
		return PrimitiveArrayUtil.toArray(list);
	}

}
