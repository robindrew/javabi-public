package com.javabi.sql.resultset.field.result;

import java.sql.ResultSet;

import com.javabi.common.lang.reflect.field.IField;
import com.javabi.sql.resultset.field.IFieldColumn;

public class LongPrimitiveColumn implements IFieldColumn {

	@Override
	public Class<?> getType() {
		return long.class;
	}

	@Override
	public void set(IField field, Object instance, ResultSet results, int column) throws Exception {
		long value = results.getLong(column);
		field.setLong(instance, value);
	}

}
