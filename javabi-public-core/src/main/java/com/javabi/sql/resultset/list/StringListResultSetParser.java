package com.javabi.sql.resultset.list;

import java.util.ArrayList;
import java.util.List;

import com.javabi.sql.resultset.IResultSetRowParser;
import com.javabi.sql.resultset.ResultSetCollectionParser;
import com.javabi.sql.resultset.type.StringResultSetRowParser;

public class StringListResultSetParser extends ResultSetCollectionParser<String, List<String>> {

	public StringListResultSetParser(IResultSetRowParser<String> parser, List<String> rows) {
		super(parser, rows);
	}

	public StringListResultSetParser(IResultSetRowParser<String> parser) {
		this(parser, new ArrayList<String>());
	}

	public StringListResultSetParser() {
		this(new StringResultSetRowParser(), new ArrayList<String>());
	}

}
