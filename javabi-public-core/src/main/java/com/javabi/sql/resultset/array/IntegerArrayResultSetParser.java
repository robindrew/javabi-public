package com.javabi.sql.resultset.array;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.ResultSetRowParser;

import gnu.trove.list.array.TIntArrayList;

public class IntegerArrayResultSetParser extends ResultSetRowParser<int[]> {

	private final TIntArrayList list = new TIntArrayList();

	@Override
	protected void parseResultSetRow(ResultSet set) throws SQLException {
		list.add(set.getInt(1));
	}

	@Override
	protected int[] getResult() {
		return list.toArray();
	}

}
