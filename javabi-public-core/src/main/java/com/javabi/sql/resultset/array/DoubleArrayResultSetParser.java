package com.javabi.sql.resultset.array;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.ResultSetRowParser;

import gnu.trove.list.array.TDoubleArrayList;

public class DoubleArrayResultSetParser extends ResultSetRowParser<double[]> {

	private final TDoubleArrayList list = new TDoubleArrayList();

	@Override
	protected void parseResultSetRow(ResultSet set) throws SQLException {
		list.add(set.getDouble(1));
	}

	@Override
	protected double[] getResult() {
		return list.toArray();
	}

}
