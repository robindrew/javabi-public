package com.javabi.sql.resultset;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

public class ResultSetCollectionParser<R, C extends Collection<R>> extends ResultSetRowParser<C> implements IResultSetParser<C> {

	private final IResultSetRowParser<R> parser;
	private final C rows;

	public ResultSetCollectionParser(IResultSetRowParser<R> parser, C rows) {
		if (parser == null) {
			throw new NullPointerException("parser");
		}
		if (rows == null) {
			throw new NullPointerException("rows");
		}
		this.parser = parser;
		this.rows = rows;
	}

	@Override
	protected void parseResultSetRow(ResultSet set) throws SQLException {
		R row = parser.parseResultSetRow(set);
		if (row != null) {
			rows.add(row);
		}
	}

	@Override
	protected C getResult() {
		return rows;
	}

}
