package com.javabi.sql.resultset.field.result;

import java.sql.ResultSet;

import com.javabi.common.lang.reflect.field.IField;
import com.javabi.sql.resultset.field.IFieldColumn;

public class FloatPrimitiveColumn implements IFieldColumn {

	@Override
	public Class<?> getType() {
		return float.class;
	}

	@Override
	public void set(IField field, Object instance, ResultSet results, int column) throws Exception {
		float value = results.getFloat(column);
		field.setFloat(instance, value);
	}

}
