package com.javabi.sql.resultset.row;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;

import gnu.trove.map.TObjectIntMap;

public class Row implements IRow {

	private final TObjectIntMap<String> columns;
	private final List<Object> values;

	public Row(TObjectIntMap<String> columns, List<Object> values) {
		this.columns = columns;
		this.values = values;
	}

	@Override
	public Object get(String columnName) {
		int index = columns.get(columnName);
		if (index < 1) {
			throw new IllegalArgumentException("columnName: '" + columnName + "'");
		}
		return values.get(index - 1);
	}

	@Override
	public Map<String, Object> toMap() {
		Map<String, Object> map = Maps.newLinkedHashMap();
		for (String column : columns.keySet()) {
			map.put(column, get(column));
		}
		return map;
	}

	@Override
	public String toString() {
		return toMap().toString();
	}

}
