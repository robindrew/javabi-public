package com.javabi.sql.resultset.row;

public interface IRowSet extends Iterable<IRow> {

	int size();

	IRow getRow(int index);

	boolean isEmpty();
}
