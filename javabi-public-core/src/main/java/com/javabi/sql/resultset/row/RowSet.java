package com.javabi.sql.resultset.row;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.common.collect.Lists;

import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;

public class RowSet implements IRowSet {

	private final TObjectIntMap<String> columns = new TObjectIntHashMap<String>();
	private final List<IRow> rowList = Lists.newArrayList();

	public RowSet(ResultSet set) throws SQLException {
		final ResultSetMetaData meta = set.getMetaData();
		final int columnCount = meta.getColumnCount();

		// Columns
		for (int i = 1; i <= columnCount; i++) {
			columns.put(meta.getColumnName(i), i);
		}

		while (set.next()) {
			List<Object> row = new ArrayList<Object>();
			for (int i = 1; i <= columnCount; i++) {
				row.add(set.getObject(i));
			}
			rowList.add(new Row(columns, row));
		}
	}

	@Override
	public int size() {
		return rowList.size();
	}

	@Override
	public IRow getRow(int index) {
		return rowList.get(index);
	}

	@Override
	public Iterator<IRow> iterator() {
		return rowList.iterator();
	}

	@Override
	public boolean isEmpty() {
		return rowList.isEmpty();
	}
}
