package com.javabi.sql.resultset.array;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.ResultSetRowParser;

import gnu.trove.list.array.TByteArrayList;

public class ByteArrayResultSetParser extends ResultSetRowParser<byte[]> {

	private final TByteArrayList list = new TByteArrayList();

	@Override
	protected void parseResultSetRow(ResultSet set) throws SQLException {
		list.add(set.getByte(1));
	}

	@Override
	protected byte[] getResult() {
		return list.toArray();
	}

}
