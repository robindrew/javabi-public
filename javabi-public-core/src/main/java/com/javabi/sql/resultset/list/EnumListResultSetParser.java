package com.javabi.sql.resultset.list;

import java.util.ArrayList;
import java.util.List;

import com.javabi.sql.resultset.IResultSetRowParser;
import com.javabi.sql.resultset.ResultSetCollectionParser;
import com.javabi.sql.resultset.type.EnumResultSetRowParser;

public class EnumListResultSetParser<E extends Enum<E>> extends ResultSetCollectionParser<E, List<E>> {

	public EnumListResultSetParser(IResultSetRowParser<E> parser, List<E> rows) {
		super(parser, rows);
	}

	public EnumListResultSetParser(IResultSetRowParser<E> parser) {
		this(parser, new ArrayList<E>());
	}

	public EnumListResultSetParser(Class<E> type) {
		this(new EnumResultSetRowParser<E>(type), new ArrayList<E>());
	}

	public EnumListResultSetParser(Class<E> type, boolean toEnumCase) {
		this(new EnumResultSetRowParser<E>(type, toEnumCase), new ArrayList<E>());
	}

}
