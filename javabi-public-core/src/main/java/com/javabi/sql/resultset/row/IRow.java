package com.javabi.sql.resultset.row;

import java.util.Map;

public interface IRow {

	Object get(String columnName);

	Map<String, Object> toMap();

}
