package com.javabi.sql.resultset.list;

import java.util.ArrayList;
import java.util.List;

import com.javabi.sql.resultset.IResultSetRowParser;
import com.javabi.sql.resultset.ResultSetCollectionParser;
import com.javabi.sql.resultset.type.ByteArrayResultSetRowParser;

public class ByteArrayListResultSetParser extends ResultSetCollectionParser<byte[], List<byte[]>> {

	public ByteArrayListResultSetParser(IResultSetRowParser<byte[]> parser, List<byte[]> rows) {
		super(parser, rows);
	}

	public ByteArrayListResultSetParser(IResultSetRowParser<byte[]> parser) {
		this(parser, new ArrayList<byte[]>());
	}

	public ByteArrayListResultSetParser() {
		this(new ByteArrayResultSetRowParser(), new ArrayList<byte[]>());
	}

}
