package com.javabi.sql.resultset;

public interface IResultSetUpdateParser<R> extends IResultSetParser<R> {

	int getRowCount();

	void setRowCount(int count);

}
