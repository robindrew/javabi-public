package com.javabi.sql.resultset.array;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.ResultSetRowParser;

import gnu.trove.list.array.TLongArrayList;

public class LongArrayResultSetParser extends ResultSetRowParser<long[]> {

	private final TLongArrayList list = new TLongArrayList();

	@Override
	protected void parseResultSetRow(ResultSet set) throws SQLException {
		list.add(set.getLong(1));
	}

	@Override
	protected long[] getResult() {
		return list.toArray();
	}

}
