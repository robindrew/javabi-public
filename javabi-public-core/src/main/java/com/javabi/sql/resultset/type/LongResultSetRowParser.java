package com.javabi.sql.resultset.type;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.IResultSetRowParser;

public class LongResultSetRowParser implements IResultSetRowParser<Long> {

	@Override
	public Long parseResultSetRow(ResultSet set) throws SQLException {
		return set.getLong(1);
	}

}
