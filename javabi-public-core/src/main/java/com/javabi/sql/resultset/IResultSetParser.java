package com.javabi.sql.resultset;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IResultSetParser<R> {

	R parseResultSet(ResultSet set) throws SQLException;

}
