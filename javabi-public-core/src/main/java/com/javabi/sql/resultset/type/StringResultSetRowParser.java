package com.javabi.sql.resultset.type;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.IResultSetRowParser;

public class StringResultSetRowParser implements IResultSetRowParser<String> {

	@Override
	public String parseResultSetRow(ResultSet set) throws SQLException {
		return set.getString(1);
	}

}
