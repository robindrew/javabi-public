package com.javabi.sql.resultset.map;

import static com.google.common.collect.Lists.newArrayList;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.javabi.sql.resultset.IResultSetParser;

public class MultipleRowMapParser implements IResultSetParser<List<Map<String, Object>>> {

	@Override
	public List<Map<String, Object>> parseResultSet(ResultSet set) throws SQLException {
		List<Map<String, Object>> list = newArrayList();
		while (set.next()) {
			ResultSetMetaData meta = set.getMetaData();
			int columns = meta.getColumnCount();

			Map<String, Object> map = new HashMap<String, Object>();
			for (int index = 1; index <= columns; index++) {
				String key = meta.getColumnName(index);
				Object value = set.getObject(index);
				map.put(key, value);
			}
			list.add(map);
		}
		return list;
	}
}
