package com.javabi.sql.resultset.type;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.IResultSetRowParser;

public class BooleanResultSetRowParser implements IResultSetRowParser<Boolean> {

	@Override
	public Boolean parseResultSetRow(ResultSet set) throws SQLException {
		return set.getBoolean(1);
	}

}
