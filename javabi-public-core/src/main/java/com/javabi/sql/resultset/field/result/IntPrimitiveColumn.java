package com.javabi.sql.resultset.field.result;

import java.sql.ResultSet;

import com.javabi.common.lang.reflect.field.IField;
import com.javabi.sql.resultset.field.IFieldColumn;

public class IntPrimitiveColumn implements IFieldColumn {

	@Override
	public Class<?> getType() {
		return int.class;
	}

	@Override
	public void set(IField field, Object instance, ResultSet results, int column) throws Exception {
		int value = results.getInt(column);
		field.setInt(instance, value);
	}

}
