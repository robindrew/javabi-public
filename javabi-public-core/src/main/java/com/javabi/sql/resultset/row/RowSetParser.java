package com.javabi.sql.resultset.row;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.IResultSetParser;

public class RowSetParser implements IResultSetParser<IRowSet> {

	@Override
	public IRowSet parseResultSet(ResultSet set) throws SQLException {
		return new RowSet(set);
	}

}
