package com.javabi.sql.resultset.array;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.ResultSetRowParser;

import gnu.trove.list.array.TShortArrayList;

public class ShortArrayResultSetParser extends ResultSetRowParser<short[]> {

	private final TShortArrayList list = new TShortArrayList();

	@Override
	protected void parseResultSetRow(ResultSet set) throws SQLException {
		list.add(set.getShort(1));
	}

	@Override
	protected short[] getResult() {
		return list.toArray();
	}

}
