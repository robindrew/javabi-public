package com.javabi.sql.resultset.field.result;

import java.sql.ResultSet;

import com.javabi.common.lang.reflect.field.IField;
import com.javabi.sql.resultset.field.IFieldColumn;

public class DoublePrimitiveColumn implements IFieldColumn {

	@Override
	public Class<?> getType() {
		return double.class;
	}

	@Override
	public void set(IField field, Object instance, ResultSet results, int column) throws Exception {
		double value = results.getDouble(column);
		field.setDouble(instance, value);
	}

}
