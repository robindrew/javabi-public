package com.javabi.sql.resultset;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import com.google.common.base.Throwables;
import com.google.common.io.ByteStreams;

public class DefaultResultSetCellParser implements IResultSetCellParser {

	public Object toByteArray(Blob blob) throws SQLException {
		long length = blob.length();
		if (length > Integer.MAX_VALUE) {
			return blob;
		}
		return blob.getBytes(0, (int) length);
	}

	@Override
	public Object parseCell(ResultSet set, int columnIndex) throws SQLException {
		int type = set.getMetaData().getColumnType(columnIndex);
		if (type == Types.BLOB) {
			try {
				InputStream input = set.getBinaryStream(columnIndex);
				return ByteStreams.toByteArray(input);
			} catch (IOException ioe) {
				throw Throwables.propagate(ioe);
			}
		}
		return set.getObject(columnIndex);
	}
}
