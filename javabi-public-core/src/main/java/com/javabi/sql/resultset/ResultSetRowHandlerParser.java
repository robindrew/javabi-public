package com.javabi.sql.resultset;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ResultSetRowHandlerParser<R> implements IResultSetParser<Long> {

	private final IResultSetRowParser<R> parser;
	private final IResultSetRowHandler<R> handler;

	public ResultSetRowHandlerParser(IResultSetRowParser<R> parser, IResultSetRowHandler<R> handler) {
		if (parser == null) {
			throw new NullPointerException("parser");
		}
		if (handler == null) {
			throw new NullPointerException("handler");
		}
		this.parser = parser;
		this.handler = handler;
	}

	@Override
	public Long parseResultSet(ResultSet set) throws SQLException {
		long rowCount = 0;
		while (set.next()) {
			parseResultSetRow(set);
			rowCount++;
		}
		return rowCount;
	}

	protected void parseResultSetRow(ResultSet set) throws SQLException {
		R row = parser.parseResultSetRow(set);
		handler.handleRow(row);
	}

}
