package com.javabi.sql.resultset;

public abstract class AbstractResultSetUpdateParser<R> implements IResultSetUpdateParser<R> {

	private int count = 0;

	@Override
	public void setRowCount(int count) {
		this.count = count;
	}

	@Override
	public int getRowCount() {
		return count;
	}
}
