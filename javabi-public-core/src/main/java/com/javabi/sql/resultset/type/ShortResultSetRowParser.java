package com.javabi.sql.resultset.type;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.IResultSetRowParser;

public class ShortResultSetRowParser implements IResultSetRowParser<Short> {

	@Override
	public Short parseResultSetRow(ResultSet set) throws SQLException {
		return set.getShort(1);
	}

}
