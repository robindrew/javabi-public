package com.javabi.sql.resultset.field.result;

import java.sql.ResultSet;

import com.javabi.common.lang.reflect.field.IField;
import com.javabi.sql.resultset.field.IFieldColumn;

public class StringColumn implements IFieldColumn {

	@Override
	public Class<?> getType() {
		return String.class;
	}

	@Override
	public void set(IField field, Object instance, ResultSet results, int column) throws Exception {
		String value = results.getString(column);
		field.set(instance, value);
	}

}
