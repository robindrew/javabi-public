package com.javabi.sql.resultset.array;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javabi.sql.resultset.ResultSetRowParser;

import gnu.trove.list.array.TFloatArrayList;

public class FloatArrayResultSetParser extends ResultSetRowParser<float[]> {

	private final TFloatArrayList list = new TFloatArrayList();

	@Override
	protected void parseResultSetRow(ResultSet set) throws SQLException {
		list.add(set.getFloat(1));
	}

	@Override
	protected float[] getResult() {
		return list.toArray();
	}

}
