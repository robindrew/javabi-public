package com.javabi.sql.resultset.field.result;

import java.sql.ResultSet;

import com.javabi.common.lang.reflect.field.IField;
import com.javabi.sql.resultset.field.IFieldColumn;

public class ShortPrimitiveColumn implements IFieldColumn {

	@Override
	public Class<?> getType() {
		return short.class;
	}

	@Override
	public void set(IField field, Object instance, ResultSet results, int column) throws Exception {
		short value = results.getShort(column);
		field.setShort(instance, value);
	}

}
