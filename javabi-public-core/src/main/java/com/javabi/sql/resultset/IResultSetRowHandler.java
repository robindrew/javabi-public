package com.javabi.sql.resultset;

public interface IResultSetRowHandler<R> {

	void handleRow(R row);

}
