package com.javabi.sql.resultset.field;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import com.google.common.base.Throwables;
import com.javabi.common.lang.reflect.field.IField;
import com.javabi.sql.resultset.field.result.BytePrimitiveColumn;
import com.javabi.sql.resultset.field.result.DoublePrimitiveColumn;
import com.javabi.sql.resultset.field.result.FloatPrimitiveColumn;
import com.javabi.sql.resultset.field.result.IntPrimitiveColumn;
import com.javabi.sql.resultset.field.result.LongPrimitiveColumn;
import com.javabi.sql.resultset.field.result.ShortPrimitiveColumn;
import com.javabi.sql.resultset.field.result.StringColumn;

public class FieldColumnParser {

	private final Map<Class<?>, IFieldColumn> typeToFieldMap = new HashMap<Class<?>, IFieldColumn>();

	public FieldColumnParser() {
		typeToFieldMap.put(String.class, new StringColumn());
		typeToFieldMap.put(byte.class, new BytePrimitiveColumn());
		typeToFieldMap.put(short.class, new ShortPrimitiveColumn());
		typeToFieldMap.put(int.class, new IntPrimitiveColumn());
		typeToFieldMap.put(long.class, new LongPrimitiveColumn());
		typeToFieldMap.put(float.class, new FloatPrimitiveColumn());
		typeToFieldMap.put(double.class, new DoublePrimitiveColumn());
	}

	public void parse(IField field, Object instance, ResultSet results, int column) {
		try {
			if (!field.isAccessible()) {
				field.setAccessible(true);
			}

			Class<?> type = field.getType();
			IFieldColumn result = typeToFieldMap.get(type);
			if (result == null) {
				throw new IllegalArgumentException("Type not supported: " + type);
			}
			result.set(field, instance, results, column);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

}
