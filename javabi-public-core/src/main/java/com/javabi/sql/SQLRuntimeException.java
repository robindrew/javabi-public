package com.javabi.sql;

public class SQLRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 8925280641121439109L;

	public SQLRuntimeException(Throwable cause) {
		super(cause);
	}

	public SQLRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public SQLRuntimeException(String message) {
		super(message);
	}

}
