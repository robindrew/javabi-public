package com.javabi.sql.executor.handler;

import java.sql.Connection;
import java.sql.SQLException;

public abstract class ConnectionHandler<R> extends AbstractConnectionHandler<R> {

	private final Connection connection;

	protected ConnectionHandler(IConnectionSetup setup, Connection connection) {
		super(setup);
		if (connection == null) {
			throw new NullPointerException("connection");
		}
		this.connection = connection;
	}

	public final Connection getConnection() {
		return connection;
	}

	@Override
	protected Connection openConnection() throws SQLException {
		return connection;
	}

	@Override
	protected final void closeConnection(Connection connection) {
		// Do not close!
	}
}
