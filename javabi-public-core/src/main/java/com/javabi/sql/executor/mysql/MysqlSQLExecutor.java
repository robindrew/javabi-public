package com.javabi.sql.executor.mysql;

import javax.sql.DataSource;

import com.javabi.sql.executor.SQLExecutor;

public class MysqlSQLExecutor extends SQLExecutor implements IMysqlSQLExecutor {

	public MysqlSQLExecutor(DataSource dataSource, boolean streaming) {
		super(dataSource, new MysqlConnectionSetup(streaming));
	}

}
