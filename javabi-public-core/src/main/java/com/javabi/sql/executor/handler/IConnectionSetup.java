package com.javabi.sql.executor.handler;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.javabi.sql.cache.ISQLCache;

public interface IConnectionSetup {

	int getMaxRetries();

	int getMaxSqlLength();

	long getLogThreshold();

	String trim(String query);

	boolean canRetry(Throwable error);

	Statement createStatement(Connection connection) throws SQLException;

	ISQLCache getCache();

}
