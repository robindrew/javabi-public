package com.javabi.sql.executor.hsqldb;

import java.sql.ResultSet;

import javax.sql.DataSource;

import com.javabi.sql.executor.SQLExecutor;

public class HsqldbSQLExecutor extends SQLExecutor implements IHsqldbSQLExecutor {

	public HsqldbSQLExecutor(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	protected ResultSet wrap(ResultSet set) {
		return new HsqldbResultSet(set);
	}

}
