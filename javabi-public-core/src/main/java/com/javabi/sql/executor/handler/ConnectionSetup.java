package com.javabi.sql.executor.handler;

import static com.javabi.common.dependency.DependencyFactory.getDependency;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.date.timer.NanoTimer;
import com.javabi.sql.cache.ISQLCache;

public class ConnectionSetup implements IConnectionSetup {

	private static final Logger log = LoggerFactory.getLogger(ConnectionSetup.class);

	private static final int DEFAULT_MAX_RETRIES = 3;
	private static final int DEFAULT_MAX_SQL_LENGTH = 3000;
	private static final long DEFAULT_LOG_THRESHOLD_MILLIS = 500;

	private ISQLCache cache;

	public ConnectionSetup() {
		this.cache = getDependency(ISQLCache.class);
	}

	public ConnectionSetup(ISQLCache cache) {
		if (cache == null) {
			throw new NullPointerException("cache");
		}
		this.cache = cache;
	}

	@Override
	public int getMaxRetries() {
		return DEFAULT_MAX_RETRIES;
	}

	@Override
	public int getMaxSqlLength() {
		return DEFAULT_MAX_SQL_LENGTH;
	}

	@Override
	public long getLogThreshold() {
		return DEFAULT_LOG_THRESHOLD_MILLIS;
	}

	@Override
	public String trim(String query) {
		return query;// new Trimmer(getMaxSqlLength(), true).trim(query);
	}

	@Override
	public boolean canRetry(Throwable error) {
		String message = error.getMessage();

		if (message != null) {

			// Connection closed, SQL did not execute, safe to retry ...
			if (message.equals("Already closed.")) {
				return true;
			}

			// Communication failed, hopefully did not execute, retry ...
			if (message.equals("Communications link failure")) {
				return true;
			}

			// Connection timed out ...
			if (message.startsWith("The last packet successfully")) {
				return true;
			}
		}

		// Check the cause ...
		Throwable cause = error.getCause();
		if (cause != null) {
			return canRetry(cause);
		}

		return false;
	}

	@Override
	public Statement createStatement(Connection connection) throws SQLException {

		// Create Statement
		NanoTimer timer = new NanoTimer();
		timer.start();
		Statement statement = connection.createStatement();
		timer.stop();
		if (timer.hasExceeded(1, SECONDS)) {
			log.warn("Statement took " + timer + " to create");
		}
		return statement;
	}

	@Override
	public ISQLCache getCache() {
		return cache;
	}

}
