package com.javabi.sql.executor.loader;

public class PageLimit {

	private static final int DEFAULT_MAX_SIZE = 100;

	private final int pageNumber;
	private final int pageSize;

	public PageLimit(int pageNumber, int pageSize, int maxPageSize) {
		if (pageNumber < 1) {
			throw new IllegalArgumentException("pageNumber=" + pageNumber);
		}
		if (pageSize < 1) {
			throw new IllegalArgumentException("pageSize=" + pageSize);
		}
		if (maxPageSize < 1) {
			throw new IllegalArgumentException("maxSize=" + maxPageSize);
		}
		if (pageSize > maxPageSize) {
			throw new IllegalArgumentException("pageSize=" + pageSize + ", maxPageSize=" + maxPageSize);
		}

		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
	}

	public PageLimit(int pageNumber, int pageSize) {
		this(pageNumber, pageSize, DEFAULT_MAX_SIZE);
	}

	public int getOffset() {
		return (pageNumber - 1) * pageSize;
	}

	public int getRowCount() {
		return pageSize;
	}
}
