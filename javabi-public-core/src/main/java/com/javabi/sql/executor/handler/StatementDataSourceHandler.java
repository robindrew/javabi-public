package com.javabi.sql.executor.handler;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

public abstract class StatementDataSourceHandler<R> extends ConnectionDataSourceHandler<R> implements IStatementHandler<R> {

	protected StatementDataSourceHandler(IConnectionSetup setup, DataSource dataSource) {
		super(setup, dataSource);
	}

	@Override
	public R handle(Connection connection) throws SQLException {
		try (Statement statement = getSetup().createStatement(connection)) {
			return handle(statement);
		}
	}

}
