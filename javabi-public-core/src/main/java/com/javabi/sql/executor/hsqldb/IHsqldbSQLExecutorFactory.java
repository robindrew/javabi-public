package com.javabi.sql.executor.hsqldb;

import com.javabi.sql.executor.ISQLExecutorFactory;

public interface IHsqldbSQLExecutorFactory extends ISQLExecutorFactory<IHsqldbSQLExecutor> {

}
