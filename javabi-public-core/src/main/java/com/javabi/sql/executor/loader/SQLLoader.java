package com.javabi.sql.executor.loader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Throwables;
import com.javabi.common.lang.IResource;
import com.javabi.common.lang.Resource;
import com.javabi.template.evaluator.ITemplateEvaluator;
import com.javabi.template.velocity.VelocityEvaluator;

public class SQLLoader implements ISQLLoader {

	private final Map<String, Object> parameters = new HashMap<String, Object>();
	private final ITemplateEvaluator evaluator;

	public SQLLoader(ITemplateEvaluator evaluator) {
		if (evaluator == null) {
			throw new NullPointerException("evaluator");
		}
		this.evaluator = evaluator;
	}

	public SQLLoader() {
		this(new VelocityEvaluator());
	}

	@Override
	public void set(String key, Object value) {
		if (key == null) {
			throw new NullPointerException("key");
		}
		if (value == null) {
			throw new NullPointerException("value");
		}
		parameters.put(key, value);
	}

	@Override
	public String loadSql(String name) {
		try {
			IResource resource = new Resource(name);
			String sql = evaluator.evaluate(resource.newInputStream(), parameters);
			sql = sql.trim();
			if (sql.isEmpty()) {
				throw new IllegalArgumentException("SQL is empty for resource: '" + name + "'");
			}
			return sql;
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

	@Override
	public List<String> loadSqls(String name) {
		String lines = loadSql(name);
		String[] array = lines.split("(\\r\\n)|(\\n)|(\\r)");
		List<String> list = new ArrayList<String>();
		for (String sql : array) {
			sql = sql.trim();
			if (!sql.isEmpty()) {
				list.add(sql);
			}
		}
		if (list.isEmpty()) {
			throw new IllegalArgumentException("SQL is empty for resource: '" + name + "'");
		}
		return list;
	}

}
