package com.javabi.sql.executor;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.date.timer.ITimer;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.sql.cache.ISQLCacheEntry;
import com.javabi.sql.cache.SQLCacheEntry;
import com.javabi.sql.executor.handler.ConnectionDataSourceHandler;
import com.javabi.sql.executor.handler.ConnectionSetup;
import com.javabi.sql.executor.handler.IConnectionSetup;
import com.javabi.sql.executor.handler.StatementDataSourceHandler;
import com.javabi.sql.resultset.CountResultSetUpdateParser;
import com.javabi.sql.resultset.IResultSetParser;
import com.javabi.sql.resultset.IResultSetRowHandler;
import com.javabi.sql.resultset.IResultSetRowParser;
import com.javabi.sql.resultset.IResultSetUpdateParser;
import com.javabi.sql.resultset.ResultSetRowHandlerParser;

public class SQLExecutor implements ISQLExecutor {

	private static final Logger log = LoggerFactory.getLogger(SQLExecutor.class);

	private final DataSource dataSource;
	private final IConnectionSetup setup;

	public SQLExecutor(DataSource dataSource) {
		this(dataSource, new ConnectionSetup());
	}

	public SQLExecutor(DataSource dataSource, IConnectionSetup setup) {
		if (dataSource == null) {
			throw new NullPointerException();
		}
		if (setup == null) {
			throw new NullPointerException("setup");
		}
		this.dataSource = dataSource;
		this.setup = setup;
	}

	@Override
	public DataSource getDataSource() {
		return dataSource;
	}

	@Override
	public IConnectionSetup getSetup() {
		return setup;
	}

	protected ResultSet wrap(ResultSet set) {
		return set;
	}

	@Override
	public <R> R execute(final IConnectionExecutor<R> executor) {
		return new ConnectionDataSourceHandler<R>(getSetup(), getDataSource()) {

			@Override
			public R handle(Connection connection) throws SQLException {
				return executor.executeConnection(connection);
			}
		}.execute();
	}

	@Override
	public <R> R execute(final IStatementExecutor<R> executor) {
		return new StatementDataSourceHandler<R>(getSetup(), getDataSource()) {

			@Override
			public R handle(Statement statement) throws SQLException {
				return executor.executeStatement(statement);
			}
		}.execute();
	}

	@Override
	public <R, C extends Collection<R>> C executeQuery(final CharSequence sql, final IResultSetRowParser<R> parser, final C rows) {
		return new StatementDataSourceHandler<C>(getSetup(), getDataSource()) {

			@Override
			public C handle(Statement statement) throws SQLException {
				return executeQuery(statement, sql, parser, rows);
			}
		}.execute();
	}

	private <R, C extends Collection<R>> C executeQuery(Statement statement, CharSequence sql, IResultSetRowParser<R> parser, C rows) {
		String query = sql.toString();
		ISQLCacheEntry entry = new SQLCacheEntry(query);
		try {

			// Execute
			ITimer timer = start(query, entry);
			ResultSet set = wrap(statement.executeQuery(query));
			executed(query, entry, timer, false);

			// Process
			while (set.next()) {
				R row = parser.parseResultSetRow(set);
				if (row != null) {
					rows.add(row);
				}
			}
			processed(query, entry, timer);

			// Done
			return rows;

		} catch (Throwable t) {
			throw failed(query, entry, t);
		}
	}

	@Override
	public <R> R executeQuery(final CharSequence sql, final IResultSetParser<R> parser) {
		return new StatementDataSourceHandler<R>(getSetup(), getDataSource()) {

			@Override
			public R handle(Statement statement) throws SQLException {
				return executeQuery(statement, sql, parser);
			}
		}.execute();
	}

	private <R> R executeQuery(Statement statement, CharSequence sql, IResultSetParser<R> parser) {
		String query = sql.toString();
		ISQLCacheEntry entry = new SQLCacheEntry(query);
		try {

			// Execute
			ITimer timer = start(query, entry);
			ResultSet set = wrap(statement.executeQuery(query));
			executed(query, entry, timer, false);

			// Process
			R results = parser.parseResultSet(set);
			processed(query, entry, timer);

			return results;

		} catch (Throwable t) {
			throw failed(query, entry, t);
		}
	}

	@Override
	public int executeUpdate(final CharSequence sql) {
		final CountResultSetUpdateParser parser = new CountResultSetUpdateParser();
		new StatementDataSourceHandler<Integer>(getSetup(), getDataSource()) {

			@Override
			public Integer handle(Statement statement) throws SQLException {
				return executeUpdate(statement, sql, parser, false);
			}
		}.execute();
		return parser.getRowCount();
	}

	@Override
	public <R> R executeUpdate(final CharSequence sql, final IResultSetUpdateParser<R> updateParser) {
		return new StatementDataSourceHandler<R>(getSetup(), getDataSource()) {

			@Override
			public R handle(Statement statement) throws SQLException {
				return executeUpdate(statement, sql, updateParser, true);
			}
		}.execute();
	}

	private <R> R executeUpdate(Statement statement, CharSequence sql, IResultSetUpdateParser<R> updateParser, boolean findKeys) {
		String update = sql.toString();
		ISQLCacheEntry entry = new SQLCacheEntry(update);
		try {

			// Execute
			R returnValue = null;
			ITimer timer = start(update, entry);
			int rowCount = statement.executeUpdate(update);
			updateParser.setRowCount(rowCount);
			executed(update, entry, timer, true);
			if (findKeys) {
				ResultSet keys = statement.getGeneratedKeys();
				if (keys != null) {
					returnValue = updateParser.parseResultSet(keys);
					processed(update, entry, timer);
				}
			}

			// Done
			return returnValue;

		} catch (Throwable t) {
			throw failed(update, entry, t);
		}
	}

	@Override
	public int executeCount(final CharSequence sql) {
		return new StatementDataSourceHandler<Integer>(getSetup(), getDataSource()) {

			@Override
			public Integer handle(Statement statement) throws SQLException {
				return executeCount(statement, sql);
			}
		}.execute();
	}

	private Integer executeCount(Statement statement, CharSequence sql) {
		String query = sql.toString();
		ISQLCacheEntry entry = new SQLCacheEntry(query);
		try {

			// Execute
			ITimer timer = start(query, entry);
			ResultSet set = wrap(statement.executeQuery(query));
			executed(query, entry, timer, false);

			// Process
			set.next();
			int count = set.getInt(1);
			processed(query, entry, timer);

			// Done
			return count;

		} catch (Throwable t) {
			throw failed(query, entry, t);
		}
	}

	@Override
	public boolean executeExists(final CharSequence sql) {
		return new StatementDataSourceHandler<Boolean>(getSetup(), getDataSource()) {

			@Override
			public Boolean handle(Statement statement) throws SQLException {
				return executeExists(statement, sql);
			}
		}.execute();
	}

	private Boolean executeExists(Statement statement, CharSequence sql) {
		String query = sql.toString();
		ISQLCacheEntry entry = new SQLCacheEntry(query);
		try {

			// Execute
			ITimer timer = start(query, entry);
			ResultSet set = wrap(statement.executeQuery(query));
			executed(query, entry, timer, false);

			// Process
			boolean exists = set.next();
			processed(query, entry, timer);

			// Done
			return exists;

		} catch (Throwable t) {
			throw failed(query, entry, t);
		}
	}

	@Override
	public <R> R executeQuery(final CharSequence sql, final IResultSetRowParser<R> parser) {
		return new StatementDataSourceHandler<R>(getSetup(), getDataSource()) {

			@Override
			public R handle(Statement statement) throws SQLException {
				return executeQuery(statement, sql, parser);
			}
		}.execute();
	}

	private <R> R executeQuery(Statement statement, CharSequence sql, IResultSetRowParser<R> parser) {
		String query = sql.toString();
		ISQLCacheEntry entry = new SQLCacheEntry(query);
		try {

			// Execute
			ITimer timer = start(query, entry);
			ResultSet set = wrap(statement.executeQuery(query));
			executed(query, entry, timer, false);

			// Process
			R value = null;
			if (set.next()) {
				value = parser.parseResultSetRow(set);
			}
			processed(query, entry, timer);

			// Done
			return value;

		} catch (Throwable t) {
			throw failed(query, entry, t);
		}
	}

	private RuntimeException failed(String query, ISQLCacheEntry entry, Throwable t) {
		entry.finished();
		entry.throwable(t);
		log.error("[Failed] '" + getSetup().trim(query) + "'");
		return Throwables.propagate(t);
	}

	private void processed(String query, ISQLCacheEntry entry, ITimer timer) {
		timer.stop();
		entry.processed(timer.elapsed());
		entry.finished();
		setup.getCache().put(entry);
		if (timer.hasExceeded(getSetup().getLogThreshold(), MILLISECONDS)) {
			log.info("[Processed] '" + getSetup().trim(query) + "' in " + timer);
		}
	}

	private void executed(String query, ISQLCacheEntry entry, ITimer timer, boolean finished) {
		timer.stop();
		entry.executed(timer.elapsed());
		if (finished) {
			entry.finished();
		}
		setup.getCache().put(entry);
		if (timer.hasExceeded(getSetup().getLogThreshold(), MILLISECONDS)) {
			log.info("[Executed] '" + getSetup().trim(query) + "' in " + timer);
		}
		timer.start();
	}

	private ITimer start(String query, ISQLCacheEntry entry) {
		if (log.isDebugEnabled()) {
			log.debug("[Executing] '" + getSetup().trim(query) + "'");
		}
		ITimer timer = new NanoTimer();
		entry.started();
		setup.getCache().put(entry);
		timer.start();
		return timer;
	}

	@Override
	public <R> Long executeQuery(final CharSequence sql, final IResultSetRowParser<R> rowParser, final IResultSetRowHandler<R> rowHandler) {
		IResultSetParser<Long> parser = new ResultSetRowHandlerParser<R>(rowParser, rowHandler);
		return executeQuery(sql, parser);
	}

}
