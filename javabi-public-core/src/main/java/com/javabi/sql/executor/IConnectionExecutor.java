package com.javabi.sql.executor;

import java.sql.Connection;
import java.sql.SQLException;

public interface IConnectionExecutor<R> {

	R executeConnection(Connection connection) throws SQLException;

}
