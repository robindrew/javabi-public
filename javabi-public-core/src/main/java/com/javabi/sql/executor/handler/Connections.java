package com.javabi.sql.executor.handler;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.date.timer.NanoTimer;

public final class Connections {

	private static final Logger log = LoggerFactory.getLogger(Connections.class);

	private Connections() {
	}

	public static final void close(Connection connection) {
		// Close a connection without throwing an error
		// Can throw exception "Already closed."
		try {

			// Close Connection
			NanoTimer timer = new NanoTimer();
			timer.start();
			if (!connection.isClosed()) {
				connection.close();
			}
			timer.stop();
			if (timer.hasExceeded(1, SECONDS)) {
				log.warn("Connection took " + timer + " to close");
			}

		} catch (SQLException se) {
			log.warn("Error closing connection", se);
		}
	}

}
