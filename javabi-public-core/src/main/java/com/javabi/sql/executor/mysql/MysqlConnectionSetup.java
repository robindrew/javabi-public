package com.javabi.sql.executor.mysql;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.date.timer.NanoTimer;
import com.javabi.sql.executor.handler.ConnectionSetup;

public class MysqlConnectionSetup extends ConnectionSetup {

	private static final Logger log = LoggerFactory.getLogger(MysqlConnectionSetup.class);

	private final boolean streaming;

	public MysqlConnectionSetup(boolean streaming) {
		this.streaming = streaming;
	}

	@Override
	public Statement createStatement(Connection connection) throws SQLException {

		// Create Statement
		NanoTimer timer = new NanoTimer();
		timer.start();

		final Statement statement;
		if (streaming) {
			// MySQL special case that sets the driver to stream results, minimising memory usage
			statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			statement.setFetchDirection(ResultSet.FETCH_FORWARD);
			statement.setFetchSize(Integer.MIN_VALUE);
		} else {
			// MySQL default statement loads all results in to memory for efficiency
			statement = connection.createStatement();
		}

		timer.stop();
		if (timer.hasExceeded(1, SECONDS)) {
			log.warn("Statement took " + timer + " to create");
		}
		return statement;
	}

}
