package com.javabi.sql.executor.h2;

import com.javabi.sql.executor.ISQLExecutorFactory;

public interface IH2SQLExecutorFactory extends ISQLExecutorFactory<IH2SQLExecutor> {

}
