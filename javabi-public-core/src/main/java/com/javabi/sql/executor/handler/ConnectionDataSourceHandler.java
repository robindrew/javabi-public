package com.javabi.sql.executor.handler;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.date.timer.ITimer;
import com.javabi.common.date.timer.NanoTimer;

public abstract class ConnectionDataSourceHandler<R> extends AbstractConnectionHandler<R> {

	private static final Logger log = LoggerFactory.getLogger(ConnectionDataSourceHandler.class);

	private final DataSource dataSource;

	protected ConnectionDataSourceHandler(IConnectionSetup setup, DataSource dataSource) {
		super(setup);
		if (dataSource == null) {
			throw new NullPointerException("dataSource");
		}
		this.dataSource = dataSource;
	}

	public final DataSource getDataSource() {
		return dataSource;
	}

	@Override
	protected Connection openConnection() throws SQLException {
		ITimer timer = new NanoTimer();
		timer.start();
		Connection connection = dataSource.getConnection();
		timer.stop();
		if (timer.hasExceeded(1, SECONDS)) {
			log.warn("Connection took " + timer + " to aquire");
		}
		return connection;
	}

	@Override
	protected final void closeConnection(Connection connection) {
		Connections.close(connection);
	}

}
