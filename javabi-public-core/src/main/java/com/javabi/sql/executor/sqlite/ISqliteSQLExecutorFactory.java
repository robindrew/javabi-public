package com.javabi.sql.executor.sqlite;

import com.javabi.sql.executor.ISQLExecutorFactory;

public interface ISqliteSQLExecutorFactory extends ISQLExecutorFactory<ISqliteSQLExecutor> {

}
