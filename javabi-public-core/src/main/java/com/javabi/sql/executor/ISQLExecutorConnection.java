package com.javabi.sql.executor;

import java.sql.Connection;
import java.util.Collection;

import com.javabi.sql.resultset.IResultSetParser;
import com.javabi.sql.resultset.IResultSetRowParser;

public interface ISQLExecutorConnection {

	Connection getConnection();

	<R> R execute(IStatementExecutor<R> executor);

	<R, C extends Collection<R>> C executeQuery(CharSequence sql, IResultSetRowParser<R> parser, C rows);

	<R> R executeQuery(CharSequence sql, IResultSetRowParser<R> parser);

	<R> R executeQuery(CharSequence sql, IResultSetParser<R> parser);

	int executeUpdate(CharSequence sql);

	int executeCount(CharSequence sql);

}
