package com.javabi.sql.executor.derby;

import com.javabi.sql.executor.ISQLExecutorFactory;

public interface IDerbySQLExecutorFactory extends ISQLExecutorFactory<IDerbySQLExecutor> {

}
