package com.javabi.sql.executor;

import java.sql.SQLException;
import java.sql.Statement;

public interface IStatementExecutor<R> {

	R executeStatement(Statement statement) throws SQLException;

}
