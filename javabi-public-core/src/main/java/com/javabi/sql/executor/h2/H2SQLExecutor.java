package com.javabi.sql.executor.h2;

import javax.sql.DataSource;

import com.javabi.sql.executor.SQLExecutor;

public class H2SQLExecutor extends SQLExecutor implements IH2SQLExecutor {

	public H2SQLExecutor(DataSource dataSource) {
		super(dataSource);
	}

}
