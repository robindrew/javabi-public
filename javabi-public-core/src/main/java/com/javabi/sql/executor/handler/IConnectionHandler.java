package com.javabi.sql.executor.handler;

import java.sql.Connection;
import java.sql.SQLException;

public interface IConnectionHandler<R> {

	R handle(Connection connection) throws SQLException;

}
