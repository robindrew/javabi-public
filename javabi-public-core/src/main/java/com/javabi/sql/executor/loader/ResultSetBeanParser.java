package com.javabi.sql.executor.loader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.common.base.Throwables;
import com.javabi.common.lang.reflect.field.FieldLister;
import com.javabi.common.lang.reflect.field.IField;
import com.javabi.sql.resultset.IResultSetRowParser;
import com.javabi.sql.resultset.field.FieldColumnParser;

public class ResultSetBeanParser<B> implements IResultSetRowParser<B> {

	private final Class<B> type;
	private final List<IField> fields;

	public ResultSetBeanParser(Class<B> type) {
		this.type = type;
		this.fields = new FieldLister().getFieldList(type);
	}

	@Override
	public B parseResultSetRow(ResultSet set) throws SQLException {
		try {
			B bean = type.newInstance();
			FieldColumnParser parser = new FieldColumnParser();
			int column = 1;
			for (IField field : fields) {
				parser.parse(field, bean, set, column++);
			}
			return bean;
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}
}
