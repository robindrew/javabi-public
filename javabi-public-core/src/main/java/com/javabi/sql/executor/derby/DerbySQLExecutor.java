package com.javabi.sql.executor.derby;

import javax.sql.DataSource;

import com.javabi.sql.executor.SQLExecutor;

public class DerbySQLExecutor extends SQLExecutor implements IDerbySQLExecutor {

	public DerbySQLExecutor(DataSource dataSource) {
		super(dataSource);
	}

}
