package com.javabi.sql.executor;

import java.util.List;

public interface ISQLExecutorFactory<E extends ISQLExecutor> {

	void setDefaultExecutor(E executor);

	void setExecutor(String key, E executor);

	E getExecutor(String key);

	List<E> getExecutors();

}
