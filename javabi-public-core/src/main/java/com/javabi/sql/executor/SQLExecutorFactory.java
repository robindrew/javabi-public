package com.javabi.sql.executor;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

/**
 * A Thread-safe ISQLExecutor factory.
 */
public abstract class SQLExecutorFactory<E extends ISQLExecutor> implements ISQLExecutorFactory<E> {

	private static final Logger log = LoggerFactory.getLogger(SQLExecutorFactory.class);

	public static final String DEFAULT_KEY = "default";

	private final ConcurrentMap<String, E> executorMap = new ConcurrentHashMap<String, E>();

	protected String mapKey(String key) {
		return key == null ? DEFAULT_KEY : key;
	}

	@Override
	public void setDefaultExecutor(E executor) {
		setExecutor(DEFAULT_KEY, executor);
	}

	@Override
	public void setExecutor(String key, E executor) {
		key = mapKey(key);
		this.executorMap.put(key, executor);
		log.info("[Registered Executor] " + key + " -> " + executor.getClass().getName());
	}

	@Override
	public E getExecutor(String key) {
		key = mapKey(key);
		E executor = executorMap.get(key);
		if (executor == null) {
			throw new IllegalArgumentException("No executor registered for key: " + key);
		}
		return executor;
	}

	@Override
	public List<E> getExecutors() {
		return Lists.newArrayList(executorMap.values());
	}
}
