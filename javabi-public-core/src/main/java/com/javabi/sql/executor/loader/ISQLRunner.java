package com.javabi.sql.executor.loader;

import java.util.List;

import com.javabi.sql.resultset.IResultSetParser;
import com.javabi.sql.resultset.IResultSetRowParser;

public interface ISQLRunner {

	ISQLRunner resource(String resource);

	ISQLRunner set(String key, Object value);

	int intValue();

	<R> R get(IResultSetParser<R> parser);

	<R> R get(IResultSetRowParser<R> parser);

	<R> List<R> getList(IResultSetRowParser<R> parser);

	<R> List<R> getList(Class<R> beanType);

	String getSql();

	void update();

	void updates();
}
