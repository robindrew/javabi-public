package com.javabi.sql.executor.handler;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class StatementHandler<R> extends ConnectionHandler<R> implements IStatementHandler<R>, IConnectionHandler<R> {

	protected StatementHandler(IConnectionSetup setup, Connection connection) {
		super(setup, connection);
	}

	@Override
	public R handle(Connection connection) throws SQLException {
		try (Statement statement = getSetup().createStatement(connection)) {
			return handle(statement);
		}
	}

}
