package com.javabi.sql.executor.loader;

import java.util.List;

public interface ISQLLoader {

	void set(String key, Object value);

	String loadSql(String name);

	List<String> loadSqls(String name);

}
