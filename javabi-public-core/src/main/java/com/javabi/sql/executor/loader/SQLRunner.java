package com.javabi.sql.executor.loader;

import static com.javabi.common.dependency.DependencyFactory.getDependency;

import java.util.ArrayList;
import java.util.List;

import com.javabi.sql.executor.ISQLExecutor;
import com.javabi.sql.executor.mysql.IMysqlSQLExecutorFactory;
import com.javabi.sql.resultset.IResultSetParser;
import com.javabi.sql.resultset.IResultSetRowParser;
import com.javabi.sql.resultset.type.IntegerResultSetRowParser;

public class SQLRunner implements ISQLRunner {

	private final ISQLExecutor executor;
	private final ISQLLoader loader;
	private String resource;

	public SQLRunner() {
		IMysqlSQLExecutorFactory factory = getDependency(IMysqlSQLExecutorFactory.class);
		this.executor = factory.getExecutor(null);
		this.loader = new SQLLoader();
	}

	public SQLRunner(Class<? extends ISQLExecutor> executorClass) {
		this.executor = getDependency(executorClass);
		this.loader = new SQLLoader();
	}

	@Override
	public ISQLRunner resource(String resource) {
		if (resource.isEmpty()) {
			throw new IllegalArgumentException("resource is empty");
		}
		this.resource = resource;
		return this;
	}

	@Override
	public ISQLRunner set(String key, Object value) {
		loader.set(key, value);
		return this;
	}

	@Override
	public int intValue() {
		String sql = getSql();
		IntegerResultSetRowParser parser = new IntegerResultSetRowParser();
		return executor.executeQuery(sql, parser);
	}

	@Override
	public <R> R get(IResultSetParser<R> parser) {
		String sql = getSql();
		return executor.executeQuery(sql, parser);
	}

	@Override
	public <R> R get(IResultSetRowParser<R> parser) {
		String sql = getSql();
		return executor.executeQuery(sql, parser);
	}

	@Override
	public <R> List<R> getList(IResultSetRowParser<R> parser) {
		String sql = getSql();
		List<R> list = new ArrayList<R>();
		executor.executeQuery(sql, parser, list);
		return list;
	}

	@Override
	public <R> List<R> getList(Class<R> beanType) {
		return getList(new ResultSetBeanParser<R>(beanType));
	}

	@Override
	public void update() {
		String sql = getSql();
		executor.executeUpdate(sql);
	}

	@Override
	public void updates() {
		List<String> sqls = loader.loadSqls(resource);
		for (String sql : sqls) {
			executor.executeUpdate(sql);
		}
	}

	@Override
	public String getSql() {
		return loader.loadSql(resource);
	}

}
