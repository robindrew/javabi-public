package com.javabi.sql.executor.sqlite;

import javax.sql.DataSource;

import com.javabi.sql.executor.SQLExecutor;

public class SqliteSQLExecutor extends SQLExecutor implements ISqliteSQLExecutor {

	public SqliteSQLExecutor(DataSource dataSource) {
		super(dataSource);
	}

}
