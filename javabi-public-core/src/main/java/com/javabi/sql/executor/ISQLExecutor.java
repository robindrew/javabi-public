package com.javabi.sql.executor;

import java.util.Collection;

import javax.sql.DataSource;

import com.javabi.sql.executor.handler.IConnectionSetup;
import com.javabi.sql.resultset.IResultSetParser;
import com.javabi.sql.resultset.IResultSetRowHandler;
import com.javabi.sql.resultset.IResultSetRowParser;
import com.javabi.sql.resultset.IResultSetUpdateParser;

/**
 * An SQL Executor.
 */
public interface ISQLExecutor {

	DataSource getDataSource();

	IConnectionSetup getSetup();

	<R> R execute(IConnectionExecutor<R> executor);

	<R> R execute(IStatementExecutor<R> executor);

	<R, C extends Collection<R>> C executeQuery(CharSequence sql, IResultSetRowParser<R> parser, C rows);

	<R> R executeQuery(CharSequence sql, IResultSetRowParser<R> parser);

	<R> R executeQuery(CharSequence sql, IResultSetParser<R> parser);

	<R> Long executeQuery(CharSequence sql, IResultSetRowParser<R> parser, IResultSetRowHandler<R> handler);

	int executeUpdate(CharSequence sql);

	<R> R executeUpdate(CharSequence sql, IResultSetUpdateParser<R> keyParser);

	int executeCount(CharSequence sql);

	boolean executeExists(CharSequence sql);

}
