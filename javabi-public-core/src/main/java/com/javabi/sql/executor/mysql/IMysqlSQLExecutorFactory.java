package com.javabi.sql.executor.mysql;

import com.javabi.sql.executor.ISQLExecutorFactory;

public interface IMysqlSQLExecutorFactory extends ISQLExecutorFactory<IMysqlSQLExecutor> {

}
