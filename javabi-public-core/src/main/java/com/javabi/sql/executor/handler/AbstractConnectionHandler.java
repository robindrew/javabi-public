package com.javabi.sql.executor.handler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.sql.SQLRuntimeException;

public abstract class AbstractConnectionHandler<R> implements IConnectionHandler<R> {

	private static final Logger log = LoggerFactory.getLogger(AbstractConnectionHandler.class);

	private final AtomicInteger retryCounter = new AtomicInteger(1);

	private final IConnectionSetup setup;

	public AbstractConnectionHandler(IConnectionSetup setup) {
		if (setup == null) {
			throw new NullPointerException("setup");
		}
		this.setup = setup;
	}

	public IConnectionSetup getSetup() {
		return setup;
	}

	public R execute() {
		try {
			Connection connection = openConnection();
			try {
				return handle(connection);
			} finally {
				closeConnection(connection);
			}

		} catch (SQLException se) {

			// Retry?
			int attempt = retryCounter.getAndAdd(1);
			if (attempt < getSetup().getMaxRetries() && getSetup().canRetry(se)) {
				log.warn("SQL execution failed, retry #" + attempt, se);
				return execute();
			}

			// Failed
			throw new SQLRuntimeException(se);
		}
	}

	protected abstract Connection openConnection() throws SQLException;

	protected abstract void closeConnection(Connection connection);

}
