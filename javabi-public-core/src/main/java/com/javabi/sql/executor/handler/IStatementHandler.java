package com.javabi.sql.executor.handler;

import java.sql.SQLException;
import java.sql.Statement;

public interface IStatementHandler<R> {

	R handle(Statement statement) throws SQLException;

}
