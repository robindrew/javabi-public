package com.javabi.sql;

import static com.javabi.common.lang.Java.getLineSeparator;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Table;
import com.javabi.common.text.table.FormattedTable;
import com.javabi.common.text.table.IFormattedTable;
import com.javabi.common.text.table.TableFormatter;
import com.javabi.sql.executor.SQLExecutor;
import com.javabi.sql.resultset.TableResultSetParser;

public class Jdbc {

	private static final Logger log = LoggerFactory.getLogger(Jdbc.class);

	public static String format(DataSource source, String sql, int maxCellSize) {
		SQLExecutor executor = new SQLExecutor(source);
		Table<Integer, String, Object> table = executor.executeQuery(sql, new TableResultSetParser());
		IFormattedTable formatted = FormattedTable.copyOf(table, maxCellSize);
		return new TableFormatter().formatTable(formatted);
	}

	public static void log(DataSource source, String sql, int maxCellSize) {
		String formatted = format(source, sql, maxCellSize);
		log.info("[Executed] " + sql + getLineSeparator() + formatted);
	}
}
