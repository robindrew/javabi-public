package com.javabi.sql.cache;

import java.util.List;

public interface ISQLCache {

	void put(ISQLCacheEntry entry);

	List<ISQLCacheEntry> getEntryList();

}
