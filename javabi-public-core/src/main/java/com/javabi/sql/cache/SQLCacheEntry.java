package com.javabi.sql.cache;

public class SQLCacheEntry implements ISQLCacheEntry {

	private final String sql;
	private long startTime = 0;
	private long executedTime = 0;
	private long processedTime = 0;
	private long finishTime = 0;
	private Throwable throwable = null;

	public SQLCacheEntry(String sql) {
		if (sql.isEmpty()) {
			throw new IllegalArgumentException("sql is empty");
		}
		this.sql = sql;
	}

	@Override
	public String getSql() {
		return sql;
	}

	@Override
	public long getStartTime() {
		return startTime;
	}

	@Override
	public long getExecutedTime() {
		return executedTime;
	}

	@Override
	public long getProcessedTime() {
		return processedTime;
	}

	@Override
	public long getFinishTime() {
		return finishTime;
	}

	@Override
	public Throwable getThrowable() {
		return throwable;
	}

	@Override
	public void started() {
		this.startTime = System.currentTimeMillis();
	}

	@Override
	public void executed(long nanos) {
		executedTime = nanos;
	}

	@Override
	public void processed(long nanos) {
		processedTime = nanos;
	}

	@Override
	public void finished() {
		this.finishTime = System.currentTimeMillis();
	}

	@Override
	public void throwable(Throwable throwable) {
		this.throwable = throwable;
	}

}
