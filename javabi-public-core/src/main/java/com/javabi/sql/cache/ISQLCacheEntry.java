package com.javabi.sql.cache;

public interface ISQLCacheEntry {

	String getSql();

	void started();

	void executed(long nanos);

	void processed(long nanos);

	void finished();

	void throwable(Throwable throwable);

	long getStartTime();

	long getExecutedTime();

	long getProcessedTime();

	long getFinishTime();

	Throwable getThrowable();

}
