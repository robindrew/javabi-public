package com.javabi.sql.cache;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import com.javabi.common.util.CapacityMap;

public class SQLCache implements ISQLCache {

	private final CapacityMap<ISQLCacheEntry, ISQLCacheEntry> map;

	public SQLCache(int capacity) {
		this.map = new CapacityMap<ISQLCacheEntry, ISQLCacheEntry>(capacity);
	}

	@Override
	public void put(ISQLCacheEntry entry) {
		synchronized (map) {
			map.put(entry, entry);
		}
	}

	@Override
	public List<ISQLCacheEntry> getEntryList() {
		synchronized (map) {
			return newArrayList(map.keySet());
		}
	}

}
