package com.javabi.sql.pool.simple;

import java.sql.Connection;
import java.sql.SQLException;

public interface ISimpleConnection extends Connection {

	Connection getConnection();

	boolean hasExpired(long timeoutInMillis) throws SQLException;

	void open();
}
