package com.javabi.sql.pool.simple;

import javax.sql.DataSource;

public interface ISimpleConnectionPool extends DataSource {

	int capacity();

	int active();

	int size();

	boolean isClosed();

	void close();

}
