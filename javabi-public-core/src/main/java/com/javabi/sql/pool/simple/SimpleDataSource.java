package com.javabi.sql.pool.simple;

import static java.util.concurrent.TimeUnit.MINUTES;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import javax.sql.DataSource;

import com.javabi.common.date.UnitTime;

public class SimpleDataSource implements DataSource {

	private String url = null;
	private String username = null;
	private String password = null;

	private UnitTime checkFrequency = new UnitTime(5, MINUTES);
	private UnitTime idleDisconnect = new UnitTime(5, MINUTES);
	private UnitTime reconnectLimit = new UnitTime(60, MINUTES);

	public String getUrl() {
		return url;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public void setUrl(String url) {
		if (url.isEmpty()) {
			throw new IllegalArgumentException("url is empty");
		}
		this.url = url;
	}

	public void setUsername(String username) {
		if (username.isEmpty()) {
			throw new IllegalArgumentException("username is empty");
		}
		this.username = username;
	}

	public void setPassword(String password) {
		if (password.isEmpty()) {
			throw new IllegalArgumentException("password is empty");
		}
		this.password = password;
	}

	public UnitTime getCheckFrequency() {
		return checkFrequency;
	}

	public UnitTime getIdleDisconnect() {
		return idleDisconnect;
	}

	public UnitTime getReconnectLimit() {
		return reconnectLimit;
	}

	@Override
	public Connection getConnection() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Connection getConnection(String username, String password) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PrintWriter getLogWriter() throws SQLException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setLogWriter(PrintWriter out) throws SQLException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setLoginTimeout(int seconds) throws SQLException {
		throw new UnsupportedOperationException();
	}

	@Override
	public int getLoginTimeout() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isWrapperFor(Class<?> type) throws SQLException {
		throw new UnsupportedOperationException();
	}

	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		throw new UnsupportedOperationException();
	}

}
