package com.javabi.sql.pool;

import static com.javabi.common.lang.Variables.min;
import static com.javabi.common.lang.Variables.notEmpty;
import static com.javabi.common.lang.Variables.notNull;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

import com.google.common.base.Supplier;

public class CommonsDataSourceBuilder implements Supplier<DataSource> {

	private int maxActive = 10;
	private int maxIdle = 10;
	private int minIdle = 1;
	private String url = null;
	private String username = null;
	private String password = null;

	public void setMaxActive(int maxActive) {
		this.maxActive = min("maxActive", maxActive, 1);
	}

	public void setMaxIdle(int maxIdle) {
		this.maxIdle = min("maxIdle", maxIdle, 1);
	}

	public void setMinIdle(int minIdle) {
		this.minIdle = min("minIdle", minIdle, 1);
	}

	public void setUrl(String url) {
		this.url = notEmpty("url", url);
	}

	public void setUsername(String username) {
		this.username = notEmpty("username", username);
	}

	public void setPassword(String password) {
		this.password = notNull("password", password);
	}

	@Override
	public DataSource get() {

		// Create pool
		GenericObjectPool pool = new GenericObjectPool(null);
		pool.setMaxActive(maxActive);
		pool.setMaxIdle(maxIdle);
		pool.setMinIdle(minIdle);

		// Factory
		ConnectionFactory factory = new DriverManagerConnectionFactory(url, username, password);
		new PoolableConnectionFactory(factory, pool, null, null, false, true);
		return new PoolingDataSource(pool);
	}

}
