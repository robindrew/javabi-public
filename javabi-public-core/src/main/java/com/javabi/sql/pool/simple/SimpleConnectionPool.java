package com.javabi.sql.pool.simple;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleConnectionPool implements ISimpleConnectionPool {

	private static final Logger log = LoggerFactory.getLogger(SimpleConnectionPool.class);

	private final DataSource source;
	private final Queue<ISimpleConnection> connectionQueue = new LinkedList<ISimpleConnection>();
	private final Set<ISimpleConnection> activeSet = new HashSet<ISimpleConnection>();
	private final AtomicBoolean closed = new AtomicBoolean(false);
	private final int capacity;
	private final long timeout;

	public SimpleConnectionPool(DataSource source, int capacity, long timeoutInMillis) {
		if (source == null) {
			throw new NullPointerException("source");
		}
		if (capacity < 1) {
			throw new IllegalArgumentException("capacity=" + capacity);
		}
		if (timeoutInMillis < 1000) {
			throw new IllegalArgumentException("timeoutInMillis=" + timeoutInMillis);
		}
		this.source = source;
		this.capacity = capacity;
		this.timeout = timeoutInMillis;
	}

	@Override
	public int capacity() {
		return capacity;
	}

	@Override
	public int active() {
		return activeSet.size();
	}

	public synchronized void release(ISimpleConnection connection) throws SQLException {
		if (activeSet.remove(connection)) {

			// Do not return expired connections to the pool
			if (connection.hasExpired(timeout)) {
				closeConnection(connection);
			} else {
				connectionQueue.add(connection);
			}

			notify();
		}
	}

	@Override
	public synchronized int size() {
		return activeSet.size() + connectionQueue.size();
	}

	@Override
	public boolean isClosed() {
		return closed.get();
	}

	@Override
	public void close() {
		if (closed.compareAndSet(false, true)) {
			closeConnections();
		}
	}

	private void closeConnections() {
		synchronized (this) {

			// Wake all queued connections, they will fail as closed is true
			notifyAll();

			while (!activeSet.isEmpty()) {
				log.info("Waiting for " + activeSet.size() + " active connections ...");
				try {
					wait();
				} catch (InterruptedException e) {
					log.warn("Interrupted", e);
				}
			}

			while (!connectionQueue.isEmpty()) {
				log.info("Closing " + connectionQueue.size() + " remaining connections");
				ISimpleConnection connection = connectionQueue.poll();
				if (connection != null) {
					closeConnection(connection);
				}
			}
		}
	}

	@Override
	public Connection getConnection(String username, String password) throws SQLException {
		while (true) {
			if (isClosed()) {
				throw new IllegalStateException("connection pool closed");
			}

			ISimpleConnection connection = nextConnection(username, password);
			if (connection != null) {
				connection.open();
				activeSet.add(connection);
				return connection;
			}

			// Wait ..
			log.info("Awaiting connection");
			try {
				wait();
			} catch (InterruptedException e) {
				log.warn("Interrupted", e);
			}
		}
	}

	private ISimpleConnection nextConnection(String username, String password) throws SQLException {

		// Check through available connections
		while (true) {

			ISimpleConnection connection = connectionQueue.poll();
			if (connection == null) {
				break;
			}

			if (connection.hasExpired(timeout)) {
				closeConnection(connection);
			} else {
				log.info("Existing connection: " + connection);
				return connection;
			}
		}

		// Create new connection?
		if (size() < capacity()) {
			Connection connection = newConnection(username, password);
			log.info("New connection: " + connection);
			return new SimpleConnection(connection, this);
		}

		// None available
		return null;
	}

	private void closeConnection(ISimpleConnection connection) {
		log.info("Closing connection: " + connection);
		try {
			connection.getConnection().close();
		} catch (Exception e) {
			log.warn("Error closing connection: " + connection, e);
		}
	}

	private Connection newConnection(String username, String password) throws SQLException {
		return username == null ? source.getConnection() : source.getConnection(username, password);
	}

	@Override
	public Connection getConnection() throws SQLException {
		return getConnection(null, null);
	}

	@Override
	public PrintWriter getLogWriter() throws SQLException {
		return source.getLogWriter();
	}

	@Override
	public void setLogWriter(PrintWriter out) throws SQLException {
		source.setLogWriter(out);
	}

	@Override
	public void setLoginTimeout(int seconds) throws SQLException {
		source.setLoginTimeout(seconds);
	}

	@Override
	public int getLoginTimeout() throws SQLException {
		return source.getLoginTimeout();
	}

	@Override
	public java.util.logging.Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return source.getParentLogger();
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return source.unwrap(iface);
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return source.isWrapperFor(iface);
	}

}
