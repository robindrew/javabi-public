package com.javabi.sql.pool;

import static com.javabi.common.lang.Variables.notEmpty;

import javax.sql.DataSource;

import com.google.common.base.Supplier;
import com.google.common.base.Throwables;
import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;

public class BonecpDataSourceBuilder implements Supplier<DataSource> {

	private String url = null;
	private String username = null;
	private String password = null;
	private int maxConnectionsPerPartition = 2;
	private int minConnectionsPerPartition = 1;
	private int partitionCount = 5;

	public void setUrl(String url) {
		this.url = notEmpty("url", url);
	}

	public void setUsername(String username) {
		this.username = notEmpty("username", username);
	}

	public void setPassword(String password) {
		this.password = notEmpty("password", password);
	}

	public void setMaxConnectionsPerPartition(int max) {
		this.maxConnectionsPerPartition = max;
	}

	public void setMinConnectionsPerPartition(int min) {
		this.minConnectionsPerPartition = min;
	}

	public void setPartitionCount(int partitionCount) {
		this.partitionCount = partitionCount;
	}

	@Override
	public DataSource get() {

		// Setup config
		BoneCPConfig config = new BoneCPConfig();
		config.setJdbcUrl(url);
		config.setUsername(username);
		config.setPassword(password);
		config.setMinConnectionsPerPartition(maxConnectionsPerPartition);
		config.setMaxConnectionsPerPartition(minConnectionsPerPartition);
		config.setPartitionCount(partitionCount);

		// Create data source
		try {
			BoneCP instance = new BoneCP(config);
			return new BonecpDataSource(instance);
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}

}
