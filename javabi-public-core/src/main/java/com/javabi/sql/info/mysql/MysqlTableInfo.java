package com.javabi.sql.info.mysql;

import java.util.List;

import com.javabi.sql.info.ITableInfo;

public class MysqlTableInfo implements ITableInfo {

	private final String name;

	public MysqlTableInfo(String name) {
		if (name.isEmpty()) {
			throw new IllegalArgumentException("name is empty");
		}
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<MysqlColumnInfo> getColumns() {
		return null;
	}

}
