package com.javabi.sql.info;

import java.util.List;

public interface IViewInfo {

	String getName();

	List<? extends IColumnInfo> getColumns();
}
