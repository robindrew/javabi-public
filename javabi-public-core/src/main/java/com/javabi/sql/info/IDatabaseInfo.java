package com.javabi.sql.info;

import java.util.List;

public interface IDatabaseInfo {

	String getName();

	List<? extends ITableInfo> getTables();
}
