package com.javabi.sql.info.mysql;

import java.util.List;

import com.javabi.sql.info.IDatabaseInfo;

public class MysqlDatabaseInfo implements IDatabaseInfo {

	private final String name;

	public MysqlDatabaseInfo(String name) {
		if (name.isEmpty()) {
			throw new IllegalArgumentException("name is empty");
		}
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<MysqlTableInfo> getTables() {
		return null;
	}

}
