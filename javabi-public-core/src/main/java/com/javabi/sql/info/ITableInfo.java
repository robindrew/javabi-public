package com.javabi.sql.info;

import java.util.List;

public interface ITableInfo {

	String getName();

	List<? extends IColumnInfo> getColumns();
}
