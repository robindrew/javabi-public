package com.javabi.servlet.jsp;

import static com.javabi.common.lang.Variables.notEmpty;

import org.eclipse.jetty.webapp.WebAppContext;

import com.google.common.base.Supplier;

public class WebAppContextBuilder implements Supplier<WebAppContext> {

	private String contextPath = "/";
	private String resourceBase = "src/main/resources/";

	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String path) {
		this.contextPath = notEmpty("path", path);
	}

	public String getResourceBase() {
		return resourceBase;
	}

	public void setResourceBase(String base) {
		this.resourceBase = notEmpty("base", base);
	}

	@Override
	public WebAppContext get() {
		WebAppContext context = new WebAppContext();
		context.setContextPath(contextPath);
		context.setResourceBase(resourceBase);
		context.setParentLoaderPriority(true);
		return context;
	}

}
