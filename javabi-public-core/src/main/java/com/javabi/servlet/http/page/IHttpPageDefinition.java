package com.javabi.servlet.http.page;

import com.javabi.http.page.IHttpPageHandler;

public interface IHttpPageDefinition {

	String getUri();

	Class<?> getHandlerClass();

	IHttpPageHandler newInstance();

}
