package com.javabi.servlet.http.exception;

import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.session.IHttpSession;

public interface IServletExceptionHandler {

	void execute(IHttpRequest request, IHttpResponse response, IHttpSession session, Throwable throwable);
}
