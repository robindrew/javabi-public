package com.javabi.servlet.http.servlet.resource;

import com.google.common.base.Supplier;
import com.javabi.servlet.server.IServlet;

public class ResourceServletSupplier implements Supplier<IServlet> {

	private String host;
	private String path;
	private String directory;
	private boolean cachingEnabled = false;

	public String getHost() {
		return host;
	}

	public ResourceServletSupplier setHost(String host) {
		if (host.isEmpty()) {
			throw new IllegalArgumentException("host is empty");
		}
		this.host = host;
		return this;
	}

	public String getPath() {
		return path;
	}

	public ResourceServletSupplier setPath(String path) {
		if (path.isEmpty()) {
			throw new IllegalArgumentException("path is empty");
		}
		this.path = path;
		return this;
	}

	public String getDirectory() {
		return directory;
	}

	public ResourceServletSupplier setDirectory(String directory) {
		if (directory.isEmpty()) {
			throw new IllegalArgumentException("directory is empty");
		}
		this.directory = directory;
		return this;
	}

	public boolean isCachingEnabled() {
		return cachingEnabled;
	}

	public void setCachingEnabled(boolean cachingEnabled) {
		this.cachingEnabled = cachingEnabled;
	}

	@Override
	public IServlet get() {
		if (host.isEmpty()) {
			throw new IllegalStateException("host is empty");
		}
		if (path.isEmpty()) {
			throw new IllegalStateException("path is empty");
		}
		if (directory.isEmpty()) {
			throw new IllegalStateException("directory is empty");
		}
		ResourceServlet servlet = new ResourceServlet(path, directory);
		servlet.setCachingEnabled(cachingEnabled);
		servlet.setHost(host);
		return servlet;
	}

}
