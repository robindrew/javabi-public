package com.javabi.servlet.http.page;

import com.javabi.http.page.IHttpPageHandler;

public interface IPageHandlerLocator {

	IHttpPageHandler locatePageHandler(String uri);

}
