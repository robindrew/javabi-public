package com.javabi.servlet.http.template;

import java.util.ArrayList;
import java.util.List;

import com.javabi.http.form.IHttpForm;
import com.javabi.http.page.IHttpPage;
import com.javabi.http.parameter.HttpParameterParser;

public abstract class TemplateFormPage extends TemplatePage {

	private final List<IHttpForm> formList = new ArrayList<IHttpForm>();

	public void form(IHttpForm form) {
		if (form == null) {
			throw new NullPointerException("form");
		}
		this.formList.add(form);
	}

	protected void handleForms(IHttpPage page) {
		for (IHttpForm form : formList) {
			form.handlePage(page);
		}
	}

	protected void handleParameters(IHttpPage page) {
		new HttpParameterParser().parse(this, page);
	}

}
