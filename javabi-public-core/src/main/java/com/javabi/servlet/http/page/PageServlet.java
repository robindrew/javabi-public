package com.javabi.servlet.http.page;

import com.javabi.http.exception.HttpNotFoundException;
import com.javabi.http.page.HttpPage;
import com.javabi.http.page.IHttpPage;
import com.javabi.http.page.IHttpPageHandler;
import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.session.IHttpSession;
import com.javabi.servlet.http.servlet.AbstractHttpServlet;
import com.javabi.servlet.http.servlet.IHttpServlet;
import com.javabi.servlet.server.IServlet;

public class PageServlet extends AbstractHttpServlet implements IHttpServlet, IServlet {

	private final String path;
	private final IPageHandlerLocator locator;

	public PageServlet(String path, IPageHandlerLocator locator) {
		if (locator == null) {
			throw new NullPointerException("locator");
		}
		this.path = path;
		this.locator = locator;
	}

	@Override
	public String getPath() {
		return path;
	}

	@Override
	public void handleServlet(IHttpRequest request, IHttpResponse response, IHttpSession session) {
		IHttpPage page = new HttpPage(request, response, session);

		String uri = request.getUri();
		IHttpPageHandler handler = locator.locatePageHandler(uri);
		if (handler == null) {
			throw new HttpNotFoundException(uri);
		}
		handlePage(handler, page);
	}

	protected void handlePage(IHttpPageHandler handler, IHttpPage page) {
		handler.handlePage(page);
	}

}
