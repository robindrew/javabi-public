package com.javabi.servlet.http.servlet;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.date.timer.ITimer;
import com.javabi.common.date.timer.NanoTimer;
import com.javabi.http.exception.HttpResponseException;
import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.servlet.HttpServletRequestFacade;
import com.javabi.http.servlet.HttpServletResponseFacade;
import com.javabi.http.servlet.HttpSessionFacade;
import com.javabi.http.session.IHttpSession;
import com.javabi.servlet.http.exception.HtmlExceptionHandler;
import com.javabi.servlet.http.exception.IServletExceptionHandler;

public abstract class AbstractHttpServlet extends HttpServlet implements IHttpServlet {

	private static final Logger log = LoggerFactory.getLogger(AbstractHttpServlet.class);

	private static final AtomicLong count = new AtomicLong(0);

	private volatile String host = null;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	protected boolean hasSession() {
		return true;
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		IHttpSession session = hasSession() ? new HttpSessionFacade(request) : null;
		doHttp(new HttpServletRequestFacade(request), new HttpServletResponseFacade(response), session);
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		IHttpSession session = hasSession() ? new HttpSessionFacade(request) : null;
		doHttp(new HttpServletRequestFacade(request), new HttpServletResponseFacade(response), session);
	}

	public void doHttp(IHttpRequest request, IHttpResponse response, IHttpSession session) {
		long number = count.addAndGet(1);

		ITimer timer = new NanoTimer();
		timer.start();

		// Execute
		try {
			handleServlet(request, response, session);
		}

		// Response
		catch (HttpResponseException exception) {
			exception.populate(response);
		}

		// Exception
		catch (Exception exception) {
			IServletExceptionHandler handler = getExceptionHandler();
			handler.execute(request, response, session, exception);
		}

		// Throwable
		catch (Throwable throwable) {
			throw Throwables.propagate(throwable);
		}

		timer.stop();

		// Logging...
		if (getWarnTime() <= timer.elapsed()) {
			log.warn(getClass().getSimpleName() + " #" + number + " (" + response.getStatus() + ") executed in " + timer);
		} else {
			if (getInfoTime() <= timer.elapsed()) {
				log.info(getClass().getSimpleName() + " #" + number + " (" + response.getStatus() + ") executed in " + timer);
			} else {
				if (log.isDebugEnabled()) {
					log.debug(getClass().getSimpleName() + " #" + number + " (" + response.getStatus() + ") executed in " + timer);
				}
			}
		}
	}

	protected long getInfoTime() {
		return MILLISECONDS.toNanos(500);
	}

	protected long getWarnTime() {
		return SECONDS.toNanos(1);
	}

	public IServletExceptionHandler getExceptionHandler() {
		return new HtmlExceptionHandler();
	}
}
