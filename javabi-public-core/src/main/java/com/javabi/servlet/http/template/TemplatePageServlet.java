package com.javabi.servlet.http.template;

import com.javabi.http.page.IHttpPage;
import com.javabi.http.page.IHttpPageHandler;
import com.javabi.servlet.http.page.PackagePageHandlerLocator;
import com.javabi.servlet.http.page.PageServlet;
import com.javabi.template.ITemplateLocator;

public class TemplatePageServlet extends PageServlet {

	private final ITemplateLocator locator;

	public TemplatePageServlet(String path, String packageName, String defaultPage, ITemplateLocator locator) {
		super(path, new PackagePageHandlerLocator(packageName, defaultPage));
		if (locator == null) {
			throw new NullPointerException("locator");
		}
		this.locator = locator;
	}

	@Override
	protected void handlePage(IHttpPageHandler handler, IHttpPage page) {
		if (handler instanceof TemplatePage) {
			TemplatePage template = (TemplatePage) handler;
			template.setLocator(locator);
		}
		handler.handlePage(page);
	}

}
