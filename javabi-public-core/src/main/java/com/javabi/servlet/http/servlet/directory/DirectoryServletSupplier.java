package com.javabi.servlet.http.servlet.directory;

import com.google.common.base.Supplier;
import com.javabi.servlet.server.IServlet;

public class DirectoryServletSupplier implements Supplier<IServlet> {

	private String path;
	private String directory;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	@Override
	public IServlet get() {
		if (path.isEmpty()) {
			throw new IllegalStateException("path is empty");
		}
		if (directory.isEmpty()) {
			throw new IllegalStateException("directory is empty");
		}
		return new DirectoryServlet(path, directory);
	}

}
