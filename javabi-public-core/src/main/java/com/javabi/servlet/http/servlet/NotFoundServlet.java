package com.javabi.servlet.http.servlet;

import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.response.StatusCode;
import com.javabi.http.session.IHttpSession;
import com.javabi.servlet.server.IServlet;

public class NotFoundServlet extends AbstractHttpServlet implements IHttpServlet, IServlet {

	@Override
	public void handleServlet(IHttpRequest request, IHttpResponse response, IHttpSession session) {
		response.setStatus(StatusCode.NOT_FOUND);
		response.setContentLength(0);
	}

	@Override
	public String getPath() {
		throw new IllegalStateException("No default path for NOT FOUND servlets");
	}

}
