package com.javabi.servlet.http.page;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.javabi.http.page.IHttpPageHandler;

public class PackagePageHandlerLocator implements IPageHandlerLocator {

	private static final Logger log = LoggerFactory.getLogger(PackagePageHandlerLocator.class);

	private final String rootPackageName;
	private final Map<String, IHttpPageDefinition> uriToHandlerMap = Maps.newConcurrentMap();
	private final String defaultPage;

	public PackagePageHandlerLocator(String packageName, String defaultPage) {
		if (packageName.isEmpty()) {
			throw new IllegalArgumentException("packageName is empty");
		}
		if (defaultPage.isEmpty()) {
			throw new IllegalArgumentException("defaultPage is empty");
		}
		this.rootPackageName = packageName;
		this.defaultPage = defaultPage;
	}

	@Override
	public IHttpPageHandler locatePageHandler(String uri) {
		try {
			IHttpPageDefinition handler = uriToHandlerMap.get(uri);
			if (handler == null) {
				handler = resolveHandler(uri);
				uriToHandlerMap.put(uri, handler);
			}
			return handler.newInstance();

		} catch (ClassNotFoundException e) {
			log.warn("Unable to locate page handler for uri: '" + uri + "'", e);
			return null;
		} catch (Exception e) {
			log.error("Failed to create page handler for uri: '" + uri + "'", e);
			return null;
		}
	}

	private IHttpPageDefinition resolveHandler(String uri) throws ClassNotFoundException {
		if (!uri.startsWith("/")) {
			throw new IllegalArgumentException("uri: '" + uri + "'");
		}
		if (uri.length() == 1) {
			uri = uri + defaultPage;
		}
		if (uri.endsWith("/")) {
			uri = uri.substring(0, uri.length() - 1);
		}
		uri = uri.replace('/', '.');

		String className = getClassName(uri);

		// Load Page
		String name = rootPackageName + "." + className;
		log.debug("Class Name: " + name);
		return new HttpPageDefinition(Class.forName(name), uri);
	}

	private String getClassName(String name) {
		if (name.isEmpty()) {
			return "IndexPage";
		}
		int index = name.lastIndexOf('.');
		if (index != -1) {
			name = name.substring(index + 1);
		}
		char c = name.charAt(0);
		name = Character.toUpperCase(c) + name.substring(1).toLowerCase();
		return name + "Page";
	}

}
