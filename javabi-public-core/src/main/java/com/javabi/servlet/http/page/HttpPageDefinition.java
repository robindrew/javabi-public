package com.javabi.servlet.http.page;

import com.google.common.base.Throwables;
import com.javabi.http.page.IHttpPageHandler;
import com.javabi.servlet.http.template.TemplatePage;

public class HttpPageDefinition implements IHttpPageDefinition {

	private final Class<?> handlerClass;
	private final String uri;

	public HttpPageDefinition(Class<?> handlerClass, String uri) {
		this.handlerClass = handlerClass;
		this.uri = uri;
	}

	public IHttpPageHandler newInstance() {
		final IHttpPageHandler handler;
		try {
			handler = (IHttpPageHandler) getHandlerClass().newInstance();
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
		if (handler instanceof TemplatePage) {
			TemplatePage page = (TemplatePage) handler;
			page.setDefinition(this);
		}
		return handler;
	}

	public String getUri() {
		return uri;
	}

	public Class<?> getHandlerClass() {
		return handlerClass;
	}

}
