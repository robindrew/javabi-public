package com.javabi.servlet.http.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.google.gson.Gson;
import com.javabi.http.HttpUtil;
import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.response.StatusCode;
import com.javabi.http.session.IHttpSession;

public class JsonExceptionHandler implements IServletExceptionHandler {

	private static final Logger log = LoggerFactory.getLogger(JsonExceptionHandler.class);

	@Override
	public void execute(IHttpRequest request, IHttpResponse response, IHttpSession session, Throwable throwable) {
		log.error("Internal Server Error: " + request, throwable);

		// Failure message
		String json = new Gson().toJson(Throwables.getStackTraceAsString(throwable));

		response.setStatus(StatusCode.INTERNAL_SERVER_ERROR);
		HttpUtil.setJson(response, json);
	}

}
