package com.javabi.servlet.http.servlet.directory;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.io.file.FileInput;
import com.javabi.common.io.stream.IStreamInput;
import com.javabi.http.exception.HttpNotFoundException;
import com.javabi.servlet.http.servlet.StreamInputServlet;

public class DirectoryServlet extends StreamInputServlet {

	private static final Logger log = LoggerFactory.getLogger(DirectoryServlet.class);

	public DirectoryServlet(String path, String directory) {
		super(path, directory);
	}

	private String filter(String uri) {
		return uri.substring(getPath().length() - 2);
	}

	@Override
	protected IStreamInput getInput(String uri) {
		String name = getDirectory() + filter(uri);

		File file = new File(name);
		if (!file.exists()) {
			log.warn("Resource does not exist: '" + name + "'");
			throw new HttpNotFoundException(uri);
		}
		return new FileInput(file);
	}

}
