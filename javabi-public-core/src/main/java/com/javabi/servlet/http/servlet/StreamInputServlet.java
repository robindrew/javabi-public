package com.javabi.servlet.http.servlet;

import static com.javabi.common.lang.Variables.notEmpty;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.Map;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javabi.common.collect.SoftHashMap;
import com.javabi.common.image.ImageFormat;
import com.javabi.common.io.stream.IStreamInput;
import com.javabi.http.HttpUtil;
import com.javabi.http.exception.HttpNotFoundException;
import com.javabi.http.header.Header;
import com.javabi.http.message.ContentType;
import com.javabi.http.message.TransferType;
import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.response.StatusCode;
import com.javabi.http.session.IHttpSession;
import com.javabi.image.canvas.CanvasImage;
import com.javabi.image.canvas.ICanvasImage;
import com.javabi.servlet.http.servlet.directory.DirectoryServlet;

public abstract class StreamInputServlet extends AbstractHttpServlet {

	private static final Logger log = LoggerFactory.getLogger(DirectoryServlet.class);

	private final Map<String, Object> nameToResourceMap = new SoftHashMap<String, Object>();

	private final String path;
	private final String directory;
	private boolean cachingEnabled = false;

	protected StreamInputServlet(String path, String directory) {
		this.path = notEmpty("path", path);
		this.directory = notEmpty("directory", directory);
	}

	public boolean isCachingEnabled() {
		return cachingEnabled;
	}

	public void setCachingEnabled(boolean enabled) {
		this.cachingEnabled = enabled;
	}

	public String getDirectory() {
		return directory;
	}

	@Override
	public String getPath() {
		return path;
	}

	@Override
	public void handleServlet(IHttpRequest request, IHttpResponse response, IHttpSession session) {
		String uri = request.getUri();

		// Redirect?
		if (uri.endsWith("/")) {
			log.warn("Not a resource: '" + uri + "'");
			throw new HttpNotFoundException(uri);
		}
		String key = uri;

		// Image Resize
		int width = getInt(request.get("w"));
		int height = getInt(request.get("h"));
		if (width > 0 && height > 0) {
			key = uri + "?w=" + width + "&h=" + height;
		}

		// Content Type
		ContentType type = ContentType.parseContentType(uri, ContentType.TEXT_HTML);

		// Cached?
		Object content = nameToResourceMap.get(key);
		if (content == null) {

			// Exists?
			IStreamInput input = getInput(uri);

			// Read and cache
			content = readInput(input, type.getTransferType());

			if (width > 0 && height > 0 && type.name().startsWith("IMAGE")) {
				content = resizeImage((byte[]) content, width, height, type);
			}

			if (cachingEnabled) {
				log.debug("[Caching] " + key);
				nameToResourceMap.put(key, content);
			}
		}

		// Write
		writeResource(response, content, type);
	}

	private byte[] resizeImage(byte[] content, int width, int height, ContentType type) {
		try {
			BufferedImage image = ImageIO.read(new ByteArrayInputStream(content));
			ICanvasImage canvas = new CanvasImage(image);
			canvas = canvas.resize(width, height);
			ImageFormat format = getFormat(type);
			return canvas.toByteArray(format);
		} catch (Exception e) {
			log.error("Failed to resize image: width=" + width + ", height=" + height, e);
			return content;
		}
	}

	private ImageFormat getFormat(ContentType type) {
		switch (type) {
			case IMAGE_GIF:
				return ImageFormat.GIF;
			case IMAGE_PNG:
				return ImageFormat.PNG;
			default:
				return ImageFormat.JPG;
		}
	}

	private int getInt(String value) {
		if (value == null) {
			return 0;
		}
		return Integer.parseInt(value);
	}

	private void writeResource(IHttpResponse response, Object content, ContentType type) {
		response.addHeader(Header.CACHE_CONTROL, "max-age=1000000");

		switch (type.getTransferType()) {
			case BINARY:
				response.setStatus(StatusCode.OK);
				HttpUtil.setContent(response, type, (byte[]) content);
				break;
			case TEXT:
				response.setStatus(StatusCode.OK);
				HttpUtil.setContent(response, type, (String) content);
				break;
			default:
				throw new IllegalArgumentException("type not supported: " + type);
		}
	}

	private Object readInput(IStreamInput input, TransferType type) {
		switch (type) {
			case BINARY:
				return input.readToByteArray();
			case TEXT:
				return input.readToString();
			default:
				throw new IllegalArgumentException("type not supported: " + type);
		}
	}

	protected abstract IStreamInput getInput(String uri);

}
