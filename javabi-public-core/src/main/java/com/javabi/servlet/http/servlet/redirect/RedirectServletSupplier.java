package com.javabi.servlet.http.servlet.redirect;

import com.google.common.base.Supplier;
import com.javabi.servlet.server.IServlet;

public class RedirectServletSupplier implements Supplier<IServlet> {

	private String host;
	private String from;
	private String to;

	public String getHost() {
		return host;
	}

	public RedirectServletSupplier setHost(String host) {
		if (host.isEmpty()) {
			throw new IllegalArgumentException("host is empty");
		}
		this.host = host;
		return this;
	}

	public String getFrom() {
		return from;
	}

	public RedirectServletSupplier setFrom(String from) {
		if (from.isEmpty()) {
			throw new IllegalArgumentException("from is empty");
		}
		this.from = from;
		return this;
	}

	public String getTo() {
		return to;
	}

	public RedirectServletSupplier setTo(String to) {
		if (to.isEmpty()) {
			throw new IllegalArgumentException("to is empty");
		}
		this.to = to;
		return this;
	}

	@Override
	public IServlet get() {
		if (host.isEmpty()) {
			throw new IllegalStateException("host is empty");
		}
		if (from.isEmpty()) {
			throw new IllegalStateException("from is empty");
		}
		if (to.isEmpty()) {
			throw new IllegalStateException("to is empty");
		}
		RedirectServlet servlet = new RedirectServlet(from, to);
		servlet.setHost(host);
		return servlet;
	}

}
