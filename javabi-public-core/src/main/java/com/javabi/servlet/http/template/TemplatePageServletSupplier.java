package com.javabi.servlet.http.template;

import com.google.common.base.Supplier;
import com.javabi.servlet.server.IServlet;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.TemplateType;
import com.javabi.template.simple.locator.SimpleTemplateLocatorBuilder;
import com.javabi.template.velocity.VelocityTemplateLocatorSupplier;

public class TemplatePageServletSupplier implements Supplier<IServlet> {

	private String host;
	private String path;
	private String directory;
	private String packageName;
	private String defaultPage;
	private TemplateType type = TemplateType.SIMPLE;

	public String getDefaultPage() {
		return defaultPage;
	}

	public TemplatePageServletSupplier setDefaultPage(String defaultPage) {
		if (defaultPage.isEmpty()) {
			throw new IllegalArgumentException("defaultPage is empty");
		}
		this.defaultPage = defaultPage;
		return this;
	}

	public String getHost() {
		return host;
	}

	public TemplatePageServletSupplier setHost(String host) {
		if (host.isEmpty()) {
			throw new IllegalArgumentException("host is empty");
		}
		this.host = host;
		return this;
	}

	public String getPath() {
		return path;
	}

	public TemplatePageServletSupplier setPath(String path) {
		if (path.isEmpty()) {
			throw new IllegalArgumentException("path is empty");
		}
		this.path = path;
		return this;
	}

	public String getDirectory() {
		return directory;
	}

	public TemplatePageServletSupplier setDirectory(String directory) {
		if (directory.isEmpty()) {
			throw new IllegalArgumentException("directory is empty");
		}
		this.directory = directory;
		return this;
	}

	public String getPackageName() {
		return packageName;
	}

	public TemplatePageServletSupplier setPackageName(String packageName) {
		if (packageName.isEmpty()) {
			throw new IllegalArgumentException("packageName is empty");
		}
		this.packageName = packageName;
		return this;
	}

	public TemplateType getType() {
		return type;
	}

	public TemplatePageServletSupplier setType(TemplateType type) {
		if (type == null) {
			throw new NullPointerException("type");
		}
		this.type = type;
		return this;
	}

	@Override
	public IServlet get() {
		if (host.isEmpty()) {
			throw new IllegalStateException("host is empty");
		}
		if (path.isEmpty()) {
			throw new IllegalStateException("path is empty");
		}
		if (directory.isEmpty()) {
			throw new IllegalStateException("directory is empty");
		}
		if (packageName.isEmpty()) {
			throw new IllegalStateException("packageName is empty");
		}
		ITemplateLocator locator = getTemplateLocator();
		TemplatePageServlet servlet = new TemplatePageServlet(path, packageName, defaultPage, locator);
		servlet.setHost(host);
		return servlet;
	}

	private ITemplateLocator getTemplateLocator() {
		switch (type) {
			case SIMPLE:
				return getSimpleLocator();
			case VELOCITY:
				return getVelocityLocator();
			default:
				throw new IllegalStateException("type not supported: " + type);
		}
	}

	private ITemplateLocator getVelocityLocator() {
		VelocityTemplateLocatorSupplier supplier = new VelocityTemplateLocatorSupplier();
		supplier.setPath(directory);
		return supplier.get();
	}

	private ITemplateLocator getSimpleLocator() {
		SimpleTemplateLocatorBuilder builder = new SimpleTemplateLocatorBuilder();
		builder.setPath(directory);
		return builder.get();
	}

}
