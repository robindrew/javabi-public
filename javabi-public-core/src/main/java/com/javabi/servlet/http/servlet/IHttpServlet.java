package com.javabi.servlet.http.servlet;

import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.session.IHttpSession;
import com.javabi.servlet.server.IServlet;

public interface IHttpServlet extends IServlet {

	void handleServlet(IHttpRequest request, IHttpResponse response, IHttpSession session);

}
