package com.javabi.servlet.http.servlet.resource;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.io.stream.IStreamInput;
import com.javabi.common.lang.IResource;
import com.javabi.common.lang.Resource;
import com.javabi.http.exception.HttpNotFoundException;
import com.javabi.servlet.http.servlet.StreamInputServlet;

public class ResourceServlet extends StreamInputServlet {

	private static final Logger log = LoggerFactory.getLogger(ResourceServlet.class);

	public ResourceServlet(String path, String directory) {
		super(path, directory);
	}

	@Override
	protected IStreamInput getInput(String uri) {
		String name;
		try {
			name = getDirectory() + URLDecoder.decode(uri, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw Throwables.propagate(e);
		}

		IResource resource = new Resource(name);
		if (!resource.exists()) {
			log.warn("Resource does not exist: '" + name + "'");
			throw new HttpNotFoundException(uri);
		}
		return resource;
	}

}
