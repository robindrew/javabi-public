package com.javabi.servlet.http.servlet.redirect;

import com.javabi.http.header.Header;
import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.response.StatusCode;
import com.javabi.http.session.IHttpSession;
import com.javabi.servlet.http.servlet.AbstractHttpServlet;

public class RedirectServlet extends AbstractHttpServlet {

	private final String from;
	private final String to;

	public RedirectServlet(String from, String to) {
		if (from.isEmpty()) {
			throw new IllegalArgumentException("from is empty");
		}
		if (to.isEmpty()) {
			throw new IllegalArgumentException("to is empty");
		}
		this.from = from;
		this.to = to;
	}

	@Override
	public String getPath() {
		return from;
	}

	@Override
	public void handleServlet(IHttpRequest request, IHttpResponse response, IHttpSession session) {
		response.setStatus(StatusCode.FOUND);
		response.setHeader(Header.LOCATION, to);
		response.setContentLength(0);
	}

}
