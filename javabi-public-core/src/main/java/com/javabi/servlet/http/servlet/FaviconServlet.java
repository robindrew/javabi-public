package com.javabi.servlet.http.servlet;

import static java.util.concurrent.TimeUnit.HOURS;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.javabi.common.lang.Resource;
import com.javabi.common.lang.properties.IProperty;
import com.javabi.common.lang.properties.value.StringProperty;
import com.javabi.http.content.IContent;
import com.javabi.http.header.Header;
import com.javabi.http.message.ContentType;
import com.javabi.http.request.IHttpRequest;
import com.javabi.http.response.IHttpResponse;
import com.javabi.http.response.StatusCode;
import com.javabi.http.session.IHttpSession;
import com.javabi.servlet.server.IServlet;

public class FaviconServlet extends AbstractHttpServlet implements IHttpServlet, IServlet {

	private static final Logger log = LoggerFactory.getLogger(FaviconServlet.class);

	public static final String PATH = "/favicon.ico";

	private static final IProperty<String> RESOURCE = new StringProperty("favicon.resource").defaultValue("favicon.ico");
	private static final long MAX_AGE = HOURS.toSeconds(6);

	@Override
	public String getPath() {
		return PATH;
	}

	@Override
	public void handleServlet(IHttpRequest request, IHttpResponse response, IHttpSession session) {
		if (!request.getUri().equals(PATH)) {
			response.setStatus(StatusCode.NOT_FOUND);
			return;
		}
		try {

			// Return favicon.ico
			byte[] resource = new Resource(RESOURCE.get()).readToByteArray();

			// Headers
			response.setHeader(Header.CACHE_CONTROL, "max-age=" + MAX_AGE);
			response.setContentType(ContentType.IMAGE_ICO);

			IContent content = response.getContent();
			content.write(resource);

		} catch (Throwable t) {
			t = Throwables.getRootCause(t);

			log.error("Internal Server Error", t);
			response.setStatus(StatusCode.INTERNAL_SERVER_ERROR);
		}
	}

}
