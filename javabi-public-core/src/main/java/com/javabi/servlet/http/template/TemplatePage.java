package com.javabi.servlet.http.template;

import static com.javabi.common.dependency.DependencyFactory.getDependency;

import com.javabi.http.HttpUtil;
import com.javabi.http.message.ContentType;
import com.javabi.http.page.IHttpPage;
import com.javabi.http.page.IHttpPageHandler;
import com.javabi.http.response.StatusCode;
import com.javabi.servlet.http.page.HttpPageDefinition;
import com.javabi.template.ITemplate;
import com.javabi.template.ITemplateData;
import com.javabi.template.ITemplateLocator;
import com.javabi.template.TemplateData;

public abstract class TemplatePage implements IHttpPageHandler {

	private ITemplateLocator locator;
	private HttpPageDefinition definition;

	public void setLocator(ITemplateLocator locator) {
		if (locator == null) {
			throw new NullPointerException("locator");
		}
		this.locator = locator;
	}

	public void setDefinition(HttpPageDefinition definition) {
		this.definition = definition;
	}

	public ITemplateLocator getLocator() {
		if (locator == null) {
			return getDependency(ITemplateLocator.class);
		}
		return locator;
	}

	public String getName() {
		String uri = definition.getUri();
		uri = uri.replace('.', '/');
		if (uri.startsWith("/")) {
			uri = uri.substring(1);
		}
		return uri;
	}

	@Override
	public void handlePage(IHttpPage page) {
		ITemplateLocator locator = getLocator();

		// Load Template
		String name = getName() + getExtension();
		ITemplate template = locator.getTemplate(name);

		ITemplateData variables = new TemplateData(page.getRequest().getAll());
		handleTemplate(page, variables);

		String content = template.execute(variables);

		String uri = page.getRequest().getUri();

		// Response
		page.getResponse().setStatus(StatusCode.OK);
		ContentType type = parseContentType(uri);
		HttpUtil.setContent(page.getResponse(), type, content);
	}

	private ContentType parseContentType(String uri) {

		// Remove query if one exists
		int queryIndex = uri.indexOf('?');
		if (queryIndex != -1) {
			uri = uri.substring(0, queryIndex);
		}

		// Find extension
		int dotIndex = uri.indexOf('.');
		if (dotIndex != -1) {
			uri = uri.substring(dotIndex + 1);
			return ContentType.parseContentType(uri, getContentType());
		}

		// Default
		return getContentType();
	}

	protected ContentType getContentType() {
		return ContentType.TEXT_HTML;
	}

	protected String getExtension() {
		return "." + getContentType().getDefaultEnding();
	}

	public abstract void handleTemplate(IHttpPage page, ITemplateData data);

}
