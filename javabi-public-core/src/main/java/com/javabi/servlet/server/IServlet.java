package com.javabi.servlet.server;

import javax.servlet.Servlet;

public interface IServlet extends Servlet {

	String getHost();

	String getPath();

}
